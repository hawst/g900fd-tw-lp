.class public Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.super Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppAdapter;,
        Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;,
        Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppItemViewHolder;,
        Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;
    }
.end annotation


# static fields
.field private static SCROLL_MOVE_DELAY:I


# instance fields
.field private final APPLIST_DRAG_ZONE:I

.field private final APPLIST_LONG_PRESS:I

.field private final APPLIST_RECENT_HOMEKEY:I

.field private final APPLIST_RECENT_REMOVE:I

.field private final APPLIST_TIMER_COLLAPSE:I

.field private final APPLIST_TIMER_LONG_PRESS:I

.field private final APPLIST_TIMER_MESSAGE:I

.field private final ESTIMATE_INVALID_VALUE:S

.field private final HISTORYBAR_REMAKE:I

.field private final HISTORY_TIMER_MESSAGE:I

.field private final MAX_NAME_LENGTH:I

.field private final MAX_TASKS:I

.field private final PAIREDWINDOW_DIALOG_SHOW:I

.field private final THUMBNAIL_TYPE_APP:I

.field private final THUMBNAIL_TYPE_RECENT:I

.field private final UPDATE_APPLIST_POSITION:I

.field private final UPDATE_APPLIST_POSITION_DELAY:I

.field private final UPDATE_RUNNINGAPP_LIST:I

.field private final UPDATE_RUNNINGAPP_LIST_DELAY:I

.field private final WINDOW_PORTRAIT_MODE:S

.field private doOneTimeAfterDragStarts:Z

.field private dragEnteredChildCount:I

.field private lastDegrees:I

.field private mAddEmptyItemPosition:I

.field private mAddToListItem:I

.field private mAnimationBackground:Landroid/widget/ImageView;

.field private mAnimationCapture:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimationIcon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mAppDefHeight:I

.field mAppDefWidth:I

.field mAppIconClickListener:Landroid/view/View$OnClickListener;

.field mAppIconHoverListener:Landroid/view/View$OnHoverListener;

.field private mAppIconIndex:I

.field mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

.field mAppIconTouchListener:Landroid/view/View$OnTouchListener;

.field private mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

.field mAppListDragListener:Landroid/view/View$OnDragListener;

.field private mAppListDragMode:Z

.field protected mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

.field private mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

.field private mAppListHorizontal:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field mAppListItemDragListener:Landroid/view/View$OnDragListener;

.field private mAppListPosition:I

.field private mAppListVertical:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mAppTrayBG:Landroid/widget/ImageView;

.field private mBeforeAppListPosition:I

.field private mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

.field private mBezelRecognitionArea:I

.field private mBezelUI:Z

.field private mBottomGuideline:Landroid/widget/FrameLayout;

.field private mCancelDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangedPosition:I

.field private mCloseApplist:Z

.field private mConfigChanged:Z

.field mConfirmDialog:Landroid/app/AlertDialog;

.field private mCurrentAppPosition:I

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mCurrentDownView:Landroid/view/View;

.field private mCurrentHistoryIndex:I

.field private mDelIndex:I

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field mDragHandler:Landroid/os/Handler;

.field private mEditBtn:Landroid/widget/Button;

.field private mEditBtn_h:Landroid/widget/Button;

.field private mEditLayout:Landroid/widget/RelativeLayout;

.field private mEditLayout_h:Landroid/widget/RelativeLayout;

.field private mEditTextView:Landroid/widget/EditText;

.field private mExpandAppList:Z

.field private mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mFadingAnimation:Landroid/view/animation/Animation;

.field private mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field mFlashBarEditListener:Landroid/view/View$OnClickListener;

.field mFlashBarHelpListener:Landroid/view/View$OnClickListener;

.field private mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

.field mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

.field mFlashBarOpenListener:Landroid/view/View$OnClickListener;

.field mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

.field mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mFlashBarVertical:Landroid/widget/ScrollView;

.field private mFlashingAnimation:Landroid/view/animation/Animation;

.field private mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFocusGuideline:Landroid/widget/ImageView;

.field private mGoalAnimationView:Landroid/view/View;

.field mGuidelineDragListener:Landroid/view/View$OnDragListener;

.field private mGuidelineLayout:Landroid/widget/FrameLayout;

.field private mHandlePosition:Landroid/graphics/Rect;

.field private mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

.field private mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialog:Landroid/app/Dialog;

.field private mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

.field private mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialogView:Landroid/view/View;

.field private mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

.field private mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

.field mHistoryTimerHandler:Landroid/os/Handler;

.field private mInputMethodChanged:Z

.field private mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIntent:Landroid/content/Intent;

.field private mIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mIsAvailableMove:Z

.field private mIsCheckBoxChecked:Z

.field private mIsHoverType:Z

.field private mIsInputMethodShown:Z

.field private mIsMultiWindowAppsTabExpanded:Z

.field private mIsPenWindowOnly:Z

.field private mIsPressMoveBtn:Z

.field private mIsRunningAppsTabExpanded:Z

.field private mIsTemplateItem:Z

.field private mIvt:[B

.field private mLastConfig:Landroid/content/res/Configuration;

.field private mLastHandlePosition:Landroid/graphics/Rect;

.field private mLastPostAnimationRunnable:Ljava/lang/Runnable;

.field private mLunchBlock:Z

.field private mMainFrame:Landroid/view/ViewGroup;

.field private mMakeInstanceAniRunning:Z

.field private mMoveBtn:Landroid/widget/ImageView;

.field private mMoveBtnBoundary:I

.field mMoveBtnDragListener:Landroid/view/View$OnDragListener;

.field mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mMoveBtnOverlapMargin:I

.field private mMoveBtnShadowMargin:I

.field mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mNeedUpdatePocketTrayButtonText:Z

.field private mNewInstanceIntent:Landroid/content/Intent;

.field private mOpenPocketTray:Z

.field private mPocketEditBtn:Landroid/widget/ImageView;

.field private mPocketEditItem:Landroid/widget/RelativeLayout;

.field private mPocketEditText:Landroid/widget/TextView;

.field private mPocketHelpBtn:Landroid/widget/ImageView;

.field private mPocketHelpItem:Landroid/widget/RelativeLayout;

.field private mPocketHelpText:Landroid/widget/TextView;

.field private mPocketLayout:Landroid/widget/RelativeLayout;

.field private mPocketOpenBtn:Landroid/widget/ImageButton;

.field private mPocketTemplateBtn:Landroid/widget/ImageView;

.field private mPocketTemplateItem:Landroid/widget/RelativeLayout;

.field private mPocketTemplateText:Landroid/widget/TextView;

.field private mPocketTrayBody:Landroid/widget/LinearLayout;

.field private mPocketTrayHeader:Landroid/widget/RelativeLayout;

.field private mPositionGuideline:Landroid/widget/ImageView;

.field private mPositionGuidelineShadow:I

.field private mPositionX:I

.field private mPositionY:I

.field mPreviewFocusedRect:Landroid/graphics/Rect;

.field private mRecentIcon:Landroid/widget/ImageView;

.field mRecentIconClickListener:Landroid/view/View$OnClickListener;

.field private mRecentLayout:Landroid/widget/LinearLayout;

.field private mRecentTaskInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field mResetConfirmDialog:Landroid/app/AlertDialog;

.field private mResouces:Landroid/content/res/Resources;

.field mResumedInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningAppAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppAdapter;

.field mRunningAppClickListener:Landroid/view/View$OnClickListener;

.field private mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

.field private mScrollDirection:I

.field private mShadowHeight:I

.field private mShadowWidth:I

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

.field private mStartFlashBar:Z

.field private mTemplateBtn:Landroid/widget/Button;

.field private mTemplateBtn_h:Landroid/widget/Button;

.field mTemplateDialog:Landroid/app/AlertDialog;

.field private mTemplateIconFinal:Landroid/widget/ImageView;

.field private mTemplateIconIndex:I

.field private mThbumNailCancelImageSize:I

.field private mThumbNailImageHeight:I

.field private mThumbNailImageWidth:I

.field private mThumbNailLayoutHeight:I

.field private mThumbNailLayoutPadding:I

.field private mThumbNailLayoutWidth:I

.field private mThumbnailTaskInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailType:I

.field mTimerHandler:Landroid/os/Handler;

.field mToolkitRect:Landroid/graphics/Rect;

.field mToolkitZone:I

.field private mTopGuideline:Landroid/widget/FrameLayout;

.field private mTopLayoutVertical:Landroid/widget/LinearLayout;

.field mUnableDialog:Landroid/app/AlertDialog;

.field private mWindowAppList:Landroid/view/Window;

.field mWindowDefHeight:I

.field mWindowDefWidth:I

.field private mWindowGuideline:Landroid/view/Window;

.field private mbEditmode:Z

.field private mbFromEdit:Z

.field private mbSynchronizeConfirmDialog:Z

.field private mcurDstIndex:I

.field private mcurSrcIndex:I

.field private orgIcon:Landroid/graphics/drawable/Drawable;

.field private orientationListener:Landroid/view/OrientationEventListener;

.field private templateAnimationIcon:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285
    const/16 v0, 0x32

    sput v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->SCROLL_MOVE_DELAY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p3, "trayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    const/16 v7, 0x32

    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v5, 0x0

    .line 411
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 139
    iput-short v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->ESTIMATE_INVALID_VALUE:S

    .line 140
    iput-short v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->WINDOW_PORTRAIT_MODE:S

    .line 141
    const/16 v2, 0x1388

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_TIMER_COLLAPSE:I

    .line 142
    const/16 v2, 0xc8

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_TIMER_LONG_PRESS:I

    .line 143
    const/16 v2, 0xc9

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_TIMER_MESSAGE:I

    .line 144
    const/16 v2, 0xca

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_LONG_PRESS:I

    .line 145
    const/16 v2, 0xcb

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_RECENT_HOMEKEY:I

    .line 146
    const/16 v2, 0xcc

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_RECENT_REMOVE:I

    .line 147
    const/16 v2, 0xcd

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->PAIREDWINDOW_DIALOG_SHOW:I

    .line 148
    const/16 v2, 0xce

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->UPDATE_APPLIST_POSITION:I

    .line 149
    const/16 v2, 0xcf

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->UPDATE_RUNNINGAPP_LIST:I

    .line 150
    const/16 v2, 0xc8

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->UPDATE_APPLIST_POSITION_DELAY:I

    .line 151
    const/16 v2, 0x320

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->UPDATE_RUNNINGAPP_LIST_DELAY:I

    .line 152
    const/16 v2, 0x12c

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->HISTORYBAR_REMAKE:I

    .line 153
    const/16 v2, 0x3e8

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->APPLIST_DRAG_ZONE:I

    .line 155
    const/16 v2, 0x12c

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->HISTORY_TIMER_MESSAGE:I

    .line 156
    const/16 v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->MAX_TASKS:I

    .line 157
    const/16 v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->MAX_NAME_LENGTH:I

    .line 159
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .line 167
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    .line 168
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    .line 169
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    .line 170
    const/16 v2, 0x26

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B

    .line 194
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPressMoveBtn:Z

    .line 202
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelUI:Z

    .line 203
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsRunningAppsTabExpanded:Z

    .line 204
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsMultiWindowAppsTabExpanded:Z

    .line 205
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelRecognitionArea:I

    .line 206
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLunchBlock:Z

    .line 207
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dragEnteredChildCount:I

    .line 208
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPenWindowOnly:Z

    .line 209
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z

    .line 244
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    .line 245
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    .line 248
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v7, v7, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    .line 249
    const/16 v2, 0x67

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    .line 252
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsAvailableMove:Z

    .line 253
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    .line 254
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    .line 255
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mStartFlashBar:Z

    .line 257
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    .line 258
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z

    .line 269
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationCapture:Ljava/util/List;

    .line 270
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    .line 281
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    .line 282
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I

    .line 286
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mScrollDirection:I

    .line 292
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    .line 295
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    .line 296
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I

    .line 300
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I

    .line 301
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDelIndex:I

    .line 302
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbFromEdit:Z

    .line 309
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    .line 315
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 318
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    .line 320
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailTaskInfos:Ljava/util/List;

    .line 321
    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->THUMBNAIL_TYPE_APP:I

    .line 322
    const/4 v2, 0x2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->THUMBNAIL_TYPE_RECENT:I

    .line 329
    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    .line 363
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsHoverType:Z

    .line 364
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNeedUpdatePocketTrayButtonText:Z

    .line 366
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppDefWidth:I

    .line 367
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppDefHeight:I

    .line 368
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    .line 369
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    .line 370
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mToolkitZone:I

    .line 371
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mToolkitRect:Landroid/graphics/Rect;

    .line 374
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    .line 375
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z

    .line 376
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfigChanged:Z

    .line 377
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    .line 382
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    .line 385
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentTaskInfos:Ljava/util/List;

    .line 389
    const/16 v2, 0x67

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I

    .line 392
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsCheckBoxChecked:Z

    .line 396
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->doOneTimeAfterDragStarts:Z

    .line 398
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    .line 452
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 569
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    .line 634
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryTimerHandler:Landroid/os/Handler;

    .line 1696
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$19;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    .line 1912
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$20;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$20;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    .line 2739
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    .line 2818
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$23;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$23;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 2831
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$24;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$24;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 2844
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$25;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$25;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 3420
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 3513
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$43;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$43;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 3523
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$44;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$44;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnDragListener:Landroid/view/View$OnDragListener;

    .line 3552
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRunningAppClickListener:Landroid/view/View$OnClickListener;

    .line 3580
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$46;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    .line 3618
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$47;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$47;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconHoverListener:Landroid/view/View$OnHoverListener;

    .line 3626
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 3687
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 3716
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$50;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 3736
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$51;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$51;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 3764
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    .line 3796
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    .line 3839
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$54;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    .line 3879
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$55;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$55;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHelpListener:Landroid/view/View$OnClickListener;

    .line 3939
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGuidelineDragListener:Landroid/view/View$OnDragListener;

    .line 4317
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$58;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragListener:Landroid/view/View$OnDragListener;

    .line 4621
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentIconClickListener:Landroid/view/View$OnClickListener;

    .line 413
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {v2, v3, p0, v4}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .line 414
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getFlashBarCurPosition()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    .line 415
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getFlashBarHandlePosition()I

    move-result v1

    .line 416
    .local v1, "position":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 417
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    .line 418
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020079

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    .line 419
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 421
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->helpTapAnimationInit(Landroid/content/Context;)V

    .line 423
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a001a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    .line 424
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a001b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    .line 425
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a001c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    .line 427
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a0059

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutWidth:I

    .line 428
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a005a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutHeight:I

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a005c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutPadding:I

    .line 430
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailImageWidth:I

    .line 431
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a005e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailImageHeight:I

    .line 432
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a0061

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThbumNailCancelImageSize:I

    .line 434
    new-instance v2, Landroid/content/res/Configuration;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 436
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070001

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    rsub-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    .line 438
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 439
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v2, "ResponseAxT9Info"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 440
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 443
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->initFolderResources(Landroid/content/res/Resources;)V

    .line 445
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 446
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    .line 447
    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppAdapter;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppAdapter;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRunningAppAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppAdapter;

    .line 449
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getWindowDefSize()V

    .line 450
    return-void

    .line 170
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 22
    .param p1, "splitRect"    # Landroid/graphics/Rect;

    .prologue
    .line 3176
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 3178
    .local v16, "displayMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    .line 3179
    .local v15, "display":Landroid/view/Display;
    new-instance v17, Landroid/util/DisplayMetrics;

    invoke-direct/range {v17 .. v17}, Landroid/util/DisplayMetrics;-><init>()V

    .line 3180
    .local v17, "displayMetrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 3182
    const/4 v3, 0x2

    new-array v14, v3, [F

    const/4 v3, 0x0

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    aput v4, v14, v3

    const/4 v3, 0x1

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    aput v4, v14, v3

    .line 3183
    .local v14, "dims":[F
    invoke-virtual {v15}, Landroid/view/Display;->getRotation()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getDegreesForRotation(I)F

    move-result v13

    .line 3185
    .local v13, "degrees":F
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Matrix;->reset()V

    .line 3186
    neg-float v3, v13

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 3187
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 3188
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, v14, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v14, v3

    .line 3189
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget v4, v14, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v14, v3

    .line 3191
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    const/4 v4, 0x0

    aget v4, v14, v4

    float-to-int v4, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    float-to-int v5, v5

    const/16 v6, 0x4e20

    const v7, 0x270ff

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Landroid/view/SurfaceControl;->screenshot(Landroid/graphics/Rect;IIIIZI)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 3192
    .local v20, "screenCapturedTemp":Landroid/graphics/Bitmap;
    move-object/from16 v0, v17

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 3193
    .local v19, "screenCaptured":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3195
    .local v11, "canvasTemp":Landroid/graphics/Canvas;
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3196
    invoke-virtual {v11, v13}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3197
    const/4 v3, 0x0

    aget v3, v14, v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/4 v4, 0x1

    aget v4, v14, v4

    neg-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3198
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 3199
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 3200
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 3202
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    const/4 v4, 0x1

    if-lt v3, v4, :cond_0

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 3203
    :cond_0
    const/4 v12, 0x0

    .line 3216
    :goto_0
    return-object v12

    .line 3205
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 3206
    .local v12, "cropBitmap":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3207
    .local v10, "canvasFinal":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/Rect;

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v21

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3208
    .local v21, "srcRectForCrop":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3209
    .local v18, "dstRectForCrop":Landroid/graphics/Rect;
    const/4 v3, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 3211
    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Bitmap;->recycle()V

    .line 3212
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->recycle()V

    .line 3213
    invoke-virtual {v11}, Landroid/graphics/Canvas;->release()V

    .line 3214
    invoke-virtual {v10}, Landroid/graphics/Canvas;->release()V

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    return p1
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationRecentIcon()V

    return-void
.end method

.method static synthetic access$10000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$10100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->isLaunchingBlockedItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z

    return p1
.end method

.method static synthetic access$10200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$10202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$10300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$10400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    return v0
.end method

.method static synthetic access$10402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    return p1
.end method

.method static synthetic access$10500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updatePocketTrayLayout(IZZ)V

    return-void
.end method

.method static synthetic access$10602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNeedUpdatePocketTrayButtonText:Z

    return p1
.end method

.method static synthetic access$10700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailType:I

    return v0
.end method

.method static synthetic access$11000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$11100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$11200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$11300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$11400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    return-object v0
.end method

.method static synthetic access$11500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z

    return v0
.end method

.method static synthetic access$11502(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z

    return p1
.end method

.method static synthetic access$11600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    return v0
.end method

.method static synthetic access$11700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar(Z)V

    return-void
.end method

.method static synthetic access$11800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$11900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dragEnteredChildCount:I

    return v0
.end method

.method static synthetic access$11902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dragEnteredChildCount:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V

    return-void
.end method

.method static synthetic access$12002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    return p1
.end method

.method static synthetic access$12100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutWidth:I

    return v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateRunningAppList()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntents:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZZZ)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getRunningTaskCnt(ZZZ)I

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailTaskInfos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarDisappear()V

    return-void
.end method

.method static synthetic access$2702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMakeInstanceAniRunning:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailNew()V

    return-void
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/OrientationEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orientationListener:Landroid/view/OrientationEventListener;

    return-object v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/OrientationEventListener;)Landroid/view/OrientationEventListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/view/OrientationEventListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orientationListener:Landroid/view/OrientationEventListener;

    return-object p1
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->lastDegrees:I

    return v0
.end method

.method static synthetic access$3702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->lastDegrees:I

    return p1
.end method

.method static synthetic access$3802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsCheckBoxChecked:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    return v0
.end method

.method static synthetic access$3902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setAppListHandlePosition(I)V

    return-void
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    return v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->findeCurIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$4202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$4300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I

    return v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I

    return p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$4602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z

    return p1
.end method

.method static synthetic access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mStartFlashBar:Z

    return v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddEmptyItemPosition:I

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;III)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changlistItemFromEditList(III)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/DragEvent;

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I

    return v0
.end method

.method static synthetic access$5402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAddToListItem:I

    return p1
.end method

.method static synthetic access$5500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->changelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->doOneTimeAfterDragStarts:Z

    return v0
.end method

.method static synthetic access$5602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->doOneTimeAfterDragStarts:Z

    return p1
.end method

.method static synthetic access$5700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->addAndChangelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mScrollDirection:I

    return v0
.end method

.method static synthetic access$5900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/HorizontalScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    return v0
.end method

.method static synthetic access$6000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    return p1
.end method

.method static synthetic access$6100()I
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->SCROLL_MOVE_DELAY:I

    return v0
.end method

.method static synthetic access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->showTraybarHelpPopup()V

    return-void
.end method

.method static synthetic access$6400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGoalAnimationView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I

    return v0
.end method

.method static synthetic access$6902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V

    return-void
.end method

.method static synthetic access$7000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->prepareAnimationIcon()V

    return-void
.end method

.method static synthetic access$7100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->templateAnimationIcon:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$7202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->templateAnimationIcon:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationTemplateStart()V

    return-void
.end method

.method static synthetic access$7500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->openFlashBar()V

    return-void
.end method

.method static synthetic access$7600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDelIndex:I

    return v0
.end method

.method static synthetic access$7700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbFromEdit:Z

    return v0
.end method

.method static synthetic access$7800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissResetConfirmDialog()V

    return-void
.end method

.method static synthetic access$7900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationTemplateIconAppear()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$8000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationTemplateIconMove()V

    return-void
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$8100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->endAnimationIcon()V

    return-void
.end method

.method static synthetic access$8200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationTemplateIcon()V

    return-void
.end method

.method static synthetic access$8300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$8402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPressMoveBtn:Z

    return p1
.end method

.method static synthetic access$8500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setImageToMoveButton(IZ)V

    return-void
.end method

.method static synthetic access$8600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsAvailableMove:Z

    return v0
.end method

.method static synthetic access$8602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsAvailableMove:Z

    return p1
.end method

.method static synthetic access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I

    return v0
.end method

.method static synthetic access$8702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I

    return p1
.end method

.method static synthetic access$8800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZZ)V

    return-void
.end method

.method static synthetic access$8900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I

    return v0
.end method

.method static synthetic access$8902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationRecentIconAppearByHomeKey()V

    return-void
.end method

.method static synthetic access$9000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$9100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$9200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    return v0
.end method

.method static synthetic access$9202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    return p1
.end method

.method static synthetic access$9300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V

    return-void
.end method

.method static synthetic access$9400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLunchBlock:Z

    return v0
.end method

.method static synthetic access$9402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLunchBlock:Z

    return p1
.end method

.method static synthetic access$9500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPenWindowOnly:Z

    return v0
.end method

.method static synthetic access$9502(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 136
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPenWindowOnly:Z

    return p1
.end method

.method static synthetic access$9600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowWidth:I

    return v0
.end method

.method static synthetic access$9602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowWidth:I

    return p1
.end method

.method static synthetic access$9700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowHeight:I

    return v0
.end method

.method static synthetic access$9702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;
    .param p1, "x1"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowHeight:I

    return p1
.end method

.method static synthetic access$9800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelUI:Z

    return v0
.end method

.method static synthetic access$9900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private addAndChangelistItem(II)Z
    .locals 7
    .param p1, "dstIndex"    # I
    .param p2, "editIndex"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1940
    const-string v0, "AppListGridWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addAndChangelistItem src="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dst=%d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1942
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->moveToAppListItem(II)V

    .line 1943
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 1944
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    .line 1945
    return v6
.end method

.method private animationHistoryBarDisappear()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v4, 0x0

    .line 1124
    const/4 v2, 0x0

    .line 1125
    .local v2, "translateAnimation":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v2, v4, v3, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1126
    .restart local v2    # "translateAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1127
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1128
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1129
    new-instance v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1148
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1149
    .local v0, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1150
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1152
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1153
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1154
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1155
    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 1157
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1158
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1159
    return-void
.end method

.method private animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "animation"    # Z

    .prologue
    .line 1190
    move-object v11, p1

    .line 1191
    .local v11, "sendIntent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 1192
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .end local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1194
    .restart local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1195
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1197
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1198
    .local v9, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1199
    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1201
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1202
    .local v10, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1203
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1205
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;

    invoke-direct {v1, p0, v11}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$12;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1226
    if-eqz p2, :cond_0

    .line 1227
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1228
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1232
    :goto_0
    return-void

    .line 1230
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, v11}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private animationHistoryBarThumbNailNew()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1f4

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1091
    const/4 v0, 0x0

    .line 1092
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .end local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1094
    .restart local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1095
    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1097
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1098
    .local v9, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1099
    invoke-virtual {v9, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1101
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1103
    .local v10, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1105
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$10;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1119
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1120
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1121
    return-void
.end method

.method private animationHistoryBarUI()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v6, 0x0

    .line 1163
    const/4 v3, 0x0

    .line 1164
    .local v3, "translateAnimation":Landroid/view/animation/Animation;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    packed-switch v4, :pswitch_data_0

    .line 1174
    :goto_0
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1175
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1177
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1178
    .local v0, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1179
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1181
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1182
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1183
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1185
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1186
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1187
    return-void

    .line 1166
    .end local v0    # "alphaAnimation1":Landroid/view/animation/Animation;
    .end local v1    # "animationSet":Landroid/view/animation/AnimationSet;
    :pswitch_0
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-direct {v3, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1167
    .restart local v3    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0

    .line 1169
    :pswitch_1
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1170
    .local v2, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1171
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v3, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v3    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0

    .line 1164
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationRecentIcon()V
    .locals 3

    .prologue
    .line 3368
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3369
    .local v0, "bubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$41;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$41;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3377
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3378
    return-void
.end method

.method private animationRecentIconAppearByHomeKey()V
    .locals 0

    .prologue
    .line 3365
    return-void
.end method

.method private animationTemplateIcon()V
    .locals 3

    .prologue
    .line 3340
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 3341
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateScrollPositionTemplate()V

    .line 3342
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3343
    .local v0, "bubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$40;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$40;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3353
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->templateAnimationIcon:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 3354
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->templateAnimationIcon:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3356
    :cond_0
    return-void
.end method

.method private animationTemplateIconAppear()V
    .locals 7

    .prologue
    .line 3263
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 3264
    .local v4, "templateSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 3265
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3266
    .local v1, "animationIcon":Landroid/widget/ImageView;
    move v2, v3

    .line 3267
    .local v2, "count":I
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3269
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v6, 0x7f04003a

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3270
    .local v0, "animation":Landroid/view/animation/Animation;
    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow$38;

    invoke-direct {v5, p0, v1, v2, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$38;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/ImageView;II)V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3281
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3264
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3283
    .end local v0    # "animation":Landroid/view/animation/Animation;
    .end local v1    # "animationIcon":Landroid/widget/ImageView;
    .end local v2    # "count":I
    :cond_0
    return-void
.end method

.method private animationTemplateIconMove()V
    .locals 15

    .prologue
    .line 3286
    const/4 v4, 0x0

    .line 3287
    .local v4, "distanceX":F
    const/4 v8, 0x0

    .line 3289
    .local v8, "distanceY":F
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v14

    .line 3290
    .local v14, "templateSize":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    if-ge v13, v14, :cond_1

    .line 3291
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v1, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 3292
    .local v10, "animationIcon":Landroid/widget/ImageView;
    move v12, v13

    .line 3293
    .local v12, "count":I
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    packed-switch v1, :pswitch_data_0

    .line 3303
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 3304
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v10}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v8, v1

    .line 3307
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 3311
    .local v0, "translateAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3313
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e4ccccd    # 0.2f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 3314
    .local v9, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 3315
    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3317
    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v11, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 3318
    .local v11, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 3319
    invoke-virtual {v11, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 3321
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$39;

    invoke-direct {v1, p0, v10, v12, v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$39;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/ImageView;II)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3334
    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3290
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 3296
    .end local v0    # "translateAnimation":Landroid/view/animation/Animation;
    .end local v9    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v11    # "animationSet":Landroid/view/animation/AnimationSet;
    :pswitch_0
    invoke-virtual {v10}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    neg-int v1, v1

    int-to-float v4, v1

    .line 3297
    goto :goto_1

    .line 3300
    :pswitch_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    invoke-virtual {v10}, Landroid/widget/ImageView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v4, v1

    goto :goto_1

    .line 3337
    .end local v10    # "animationIcon":Landroid/widget/ImageView;
    .end local v12    # "count":I
    :cond_1
    return-void

    .line 3293
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationTemplateStart()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 3220
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    .line 3221
    .local v7, "templateSize":I
    const/4 v6, 0x0

    .line 3222
    .local v6, "splitRect":Landroid/graphics/Rect;
    const/4 v3, 0x0

    .line 3224
    .local v3, "captureImage":Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 3225
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v7, :cond_0

    .line 3226
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationCapture:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3227
    .local v1, "animationCapture":Landroid/widget/ImageView;
    move v4, v5

    .line 3228
    .local v4, "count":I
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-direct {v2, v8}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3232
    .local v2, "animationCaptureMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 3233
    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 3234
    if-nez v3, :cond_1

    .line 3235
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->endAnimationIcon()V

    .line 3236
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationTemplateIcon()V

    .line 3260
    .end local v1    # "animationCapture":Landroid/widget/ImageView;
    .end local v2    # "animationCaptureMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "count":I
    :cond_0
    return-void

    .line 3240
    .restart local v1    # "animationCapture":Landroid/widget/ImageView;
    .restart local v2    # "animationCaptureMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v4    # "count":I
    :cond_1
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3241
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v8, v9, v10, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3243
    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3245
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3247
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f040035

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3248
    .local v0, "animation":Landroid/view/animation/Animation;
    new-instance v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow$37;

    invoke-direct {v8, p0, v1, v4, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$37;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/ImageView;II)V

    invoke-virtual {v0, v8}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3258
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3225
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private cancelCollapseTimer()V
    .locals 2

    .prologue
    const/16 v1, 0xc9

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 567
    :cond_0
    return-void
.end method

.method private changelistItem(II)Z
    .locals 4
    .param p1, "srcIndex"    # I
    .param p2, "dstIndex"    # I

    .prologue
    .line 1950
    if-ne p1, p2, :cond_0

    .line 1951
    const/4 v0, 0x0

    .line 1996
    :goto_0
    return v0

    .line 1953
    :cond_0
    const-string v0, "AppListGridWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChangeItem src=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") dst=( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1955
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p2, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->reorderApplist(II)V

    .line 1956
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelUI:Z

    if-eqz v0, :cond_1

    .line 1989
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 1990
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    .line 1995
    :goto_1
    iput p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    .line 1996
    const/4 v0, 0x1

    goto :goto_0

    .line 1992
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 1993
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    goto :goto_1
.end method

.method private changlistItemFromEditList(III)Z
    .locals 4
    .param p1, "appListIndex"    # I
    .param p2, "editListIndex"    # I
    .param p3, "action"    # I

    .prologue
    const/4 v3, 0x1

    .line 2023
    const-string v0, "AppListGridWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changlistItemFromEditList action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2024
    const-string v0, "AppListGridWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changlistItemFromEditList app="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " edit ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2026
    const/4 v0, 0x6

    if-ne p3, v0, :cond_1

    .line 2028
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2031
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->checkAppListLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2032
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 2033
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    .line 2046
    :cond_0
    :goto_0
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    .line 2047
    return v3

    .line 2036
    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragMode:Z

    if-ne v0, v3, :cond_0

    .line 2038
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1, p2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2041
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 2042
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    goto :goto_0
.end method

.method private closeFlashBar()V
    .locals 1

    .prologue
    .line 2680
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar(Z)V

    .line 2681
    return-void
.end method

.method private closeFlashBar(Z)V
    .locals 6
    .param p1, "requestPreview"    # Z

    .prologue
    const/4 v5, 0x0

    .line 2683
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    if-nez v2, :cond_0

    .line 2712
    :goto_0
    return-void

    .line 2686
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 2687
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v4, 0x7f08001a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2688
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 2690
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2691
    .local v1, "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v2, -0x2710

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2692
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2695
    .end local v1    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V

    .line 2697
    const/4 v0, 0x0

    .line 2699
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v3, 0x66

    if-ne v2, v3, :cond_2

    .line 2700
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040036

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2708
    :goto_1
    invoke-direct {p0, v0, v5, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V

    .line 2709
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/4 v3, 0x1

    invoke-direct {p0, v2, v5, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updatePocketTrayLayout(IZZ)V

    .line 2711
    invoke-direct {p0, v5, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V

    goto :goto_0

    .line 2701
    :cond_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v3, 0x65

    if-ne v2, v3, :cond_3

    .line 2702
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040039

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 2703
    :cond_3
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v3, 0x68

    if-ne v2, v3, :cond_4

    .line 2704
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040038

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 2706
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040037

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1
.end method

.method private decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v5, 0x0

    const/16 v12, 0x68

    const/16 v11, 0x67

    const/4 v10, 0x1

    const/4 v9, 0x3

    .line 1856
    if-nez p1, :cond_1

    move v0, v5

    .line 1909
    :cond_0
    :goto_0
    return v0

    .line 1859
    :cond_1
    const/4 v3, -0x1

    .line 1860
    .local v3, "rawEnd":I
    const/4 v4, 0x0

    .line 1862
    .local v4, "scroll":I
    const/4 v0, 0x0

    .line 1863
    .local v0, "bRet":Z
    const/4 v6, 0x2

    new-array v1, v6, [I

    .line 1864
    .local v1, "mOffset":[I
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1865
    aget v6, v1, v10

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v7

    float-to-int v7, v7

    add-int v3, v6, v7

    .line 1867
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I

    const/4 v7, -0x1

    if-ne v6, v7, :cond_2

    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v6, :cond_5

    .line 1868
    :cond_2
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-eq v6, v11, :cond_3

    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v6, v12, :cond_7

    .line 1869
    :cond_3
    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    if-eqz v6, :cond_6

    .line 1870
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    add-int/lit8 v6, v6, -0x28

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v7

    sub-int/2addr v6, v7

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getEditAreaHeight()I

    move-result v7

    sub-int v4, v6, v7

    .line 1880
    :goto_1
    add-int/lit8 v6, v3, 0x64

    if-le v6, v4, :cond_9

    .line 1881
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-eq v6, v11, :cond_4

    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v6, v12, :cond_8

    .line 1882
    :cond_4
    const/4 v6, 0x4

    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mScrollDirection:I

    .line 1886
    :goto_2
    const/4 v0, 0x1

    .line 1901
    :cond_5
    :goto_3
    if-eqz v0, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1902
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 1903
    .local v2, "msg":Landroid/os/Message;
    const/4 v6, 0x5

    sput v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->SCROLL_MOVE_DELAY:I

    .line 1904
    iput v9, v2, Landroid/os/Message;->what:I

    .line 1905
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1906
    iput-boolean v10, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->doOneTimeAfterDragStarts:Z

    move v0, v5

    .line 1907
    goto :goto_0

    .line 1872
    .end local v2    # "msg":Landroid/os/Message;
    :cond_6
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    add-int/lit8 v6, v6, -0x28

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v7

    sub-int v4, v6, v7

    goto :goto_1

    .line 1875
    :cond_7
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    add-int/lit8 v6, v6, -0x28

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0049

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    sub-int v4, v6, v7

    goto :goto_1

    .line 1884
    :cond_8
    iput v10, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mScrollDirection:I

    goto :goto_2

    .line 1887
    :cond_9
    const/16 v6, 0x64

    if-ge v3, v6, :cond_c

    .line 1888
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-eq v6, v11, :cond_a

    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v6, v12, :cond_b

    .line 1889
    :cond_a
    iput v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mScrollDirection:I

    .line 1893
    :goto_4
    const/4 v0, 0x1

    goto :goto_3

    .line 1891
    :cond_b
    const/4 v6, 0x2

    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mScrollDirection:I

    goto :goto_4

    .line 1895
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1896
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_3
.end method

.method private dismissHelpPopupWindowTraybar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1393
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1394
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1395
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1396
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 1398
    :cond_0
    return-void
.end method

.method private dismissHistoryWindow(Z)V
    .locals 3
    .param p1, "reDraw"    # Z

    .prologue
    const/4 v2, 0x0

    .line 614
    if-nez p1, :cond_0

    .line 615
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    .line 616
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    .line 618
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMakeInstanceAniRunning:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 619
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMakeInstanceAniRunning:Z

    .line 620
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 621
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_3

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 625
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v0, :cond_2

    .line 626
    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 628
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_3

    .line 629
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 632
    :cond_3
    return-void
.end method

.method private dismissResetConfirmDialog()V
    .locals 1

    .prologue
    .line 3133
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    .line 3134
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3135
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    .line 3138
    :cond_0
    return-void
.end method

.method private endAnimationIcon()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 3405
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3406
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3407
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 3408
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3409
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3407
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3411
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3413
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3414
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3415
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3416
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3417
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3418
    return-void
.end method

.method private findHistoryBarPosisition(I)I
    .locals 6
    .param p1, "cnt"    # I

    .prologue
    .line 1064
    const/4 v0, 0x0

    .line 1065
    .local v0, "barHeight":I
    const/4 v3, 0x0

    .line 1066
    .local v3, "top":I
    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutHeight:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutPadding:I

    add-int v1, v4, v5

    .line 1067
    .local v1, "thumbNailsHeight":I
    mul-int v2, v1, p1

    .line 1069
    .local v2, "thumbNailsLayoutHeight":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailTaskInfos:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v5

    if-lt v4, v5, :cond_1

    .line 1070
    move v0, v2

    .line 1071
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    .line 1077
    :goto_0
    if-gez v3, :cond_2

    .line 1078
    const/4 v3, 0x0

    .line 1087
    :cond_0
    :goto_1
    return v3

    .line 1073
    :cond_1
    add-int/lit8 v4, p1, 0x1

    mul-int v0, v1, v4

    .line 1074
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    goto :goto_0

    .line 1080
    :cond_2
    add-int v4, v3, v0

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getHeight()I

    move-result v5

    if-le v4, v5, :cond_0

    .line 1081
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    sub-int v3, v4, v0

    .line 1082
    if-gez v3, :cond_0

    .line 1083
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private findeCurIndex(Landroid/view/View;)I
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 2051
    instance-of v3, p1, Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    move v1, v4

    .line 2060
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v2, p1

    .line 2054
    check-cast v2, Landroid/widget/LinearLayout;

    .line 2055
    .local v2, "item":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v0

    .line 2056
    .local v0, "appListSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 2057
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    check-cast v3, Landroid/widget/LinearLayout;

    if-eq v2, v3, :cond_0

    .line 2056
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v4

    .line 2060
    goto :goto_0
.end method

.method private getAppListHandlePosition()I
    .locals 5

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 4261
    const/4 v1, 0x0

    .line 4262
    .local v1, "position":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 4264
    .local v0, "moveBtnWidth":I
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    packed-switch v2, :pswitch_data_0

    .line 4306
    :goto_0
    return v1

    .line 4266
    :pswitch_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4267
    div-int/lit8 v2, v0, 0x2

    if-gt v1, v2, :cond_0

    .line 4268
    const/4 v1, 0x0

    goto :goto_0

    .line 4269
    :cond_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_1

    .line 4270
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    sub-int v1, v2, v0

    goto :goto_0

    .line 4272
    :cond_1
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 4274
    goto :goto_0

    .line 4276
    :pswitch_1
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4277
    div-int/lit8 v2, v0, 0x2

    if-gt v1, v2, :cond_2

    .line 4278
    const/4 v1, 0x0

    goto :goto_0

    .line 4279
    :cond_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_3

    .line 4280
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    sub-int v1, v2, v0

    goto :goto_0

    .line 4282
    :cond_3
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 4284
    goto :goto_0

    .line 4286
    :pswitch_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4287
    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_4

    .line 4288
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    goto :goto_0

    .line 4289
    :cond_4
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_5

    .line 4290
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 4292
    :cond_5
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 4294
    goto :goto_0

    .line 4296
    :pswitch_3
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4297
    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_6

    .line 4298
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    goto/16 :goto_0

    .line 4299
    :cond_6
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_7

    .line 4300
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnBoundary:I

    sub-int v1, v2, v3

    goto/16 :goto_0

    .line 4302
    :cond_7
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    goto/16 :goto_0

    .line 4264
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getCurrentAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 716
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v1, 0x67

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_1

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    .line 719
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    goto :goto_0
.end method

.method private getDegreesForRotation(I)F
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1413
    packed-switch p1, :pswitch_data_0

    .line 1421
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1415
    :pswitch_0
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0

    .line 1417
    :pswitch_1
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 1419
    :pswitch_2
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    .line 1413
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getEditAreaHeight()I
    .locals 2

    .prologue
    .line 4565
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 4566
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 4568
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method private getFlashBarCurPosition()I
    .locals 4

    .prologue
    .line 2071
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v2, "FlashBarPosition"

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2072
    .local v1, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v2, "FlashBarPosition"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 2073
    .local v0, "flashBarState":I
    const/16 v2, 0x65

    if-lt v0, v2, :cond_0

    const/16 v2, 0x68

    if-le v0, v2, :cond_1

    .line 2074
    :cond_0
    const/16 v0, 0x67

    .line 2075
    :cond_1
    return v0
.end method

.method private getFlashBarHandlePosition()I
    .locals 3

    .prologue
    .line 2085
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarHandlePosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2091
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarHandlePosition"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getRunningTaskCnt(ZZZ)I
    .locals 10
    .param p1, "resume"    # Z
    .param p2, "withScale"    # Z
    .param p3, "showHidden"    # Z

    .prologue
    const/16 v9, 0x800

    .line 4493
    const/4 v1, 0x0

    .line 4494
    .local v1, "cnt":I
    const/4 v5, 0x0

    .line 4496
    .local v5, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mActivityManager:Landroid/app/ActivityManager;

    const/16 v8, 0x64

    invoke-virtual {v7, v8}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v5

    .line 4498
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getSelectedTaskAffinity()Ljava/lang/String;

    move-result-object v3

    .line 4499
    .local v3, "selectedAffinity":Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 4500
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 4501
    .local v4, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 4502
    .local v0, "baseRunningAffinity":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v6

    .line 4503
    .local v6, "topRunningAffinity":Ljava/lang/String;
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz p3, :cond_0

    .line 4504
    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 4505
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_2

    if-eqz p2, :cond_0

    .line 4506
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4508
    :cond_3
    if-eqz v6, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4509
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7, v9}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz p2, :cond_0

    .line 4510
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4516
    .end local v0    # "baseRunningAffinity":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "topRunningAffinity":Ljava/lang/String;
    :cond_5
    return v1
.end method

.method private getWindowDefSize()V
    .locals 5

    .prologue
    const v4, 0x3f266666    # 0.65f

    .line 4726
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 4727
    .local v1, "displaySize":Landroid/graphics/Point;
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 4728
    .local v0, "display":Landroid/view/Display;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 4730
    :cond_0
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    .line 4731
    iget v2, v1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    .line 4732
    return-void
.end method

.method private helpTapAnimationInit(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 2810
    const v0, 0x7f040050

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShowAnimation:Landroid/view/animation/Animation;

    .line 2811
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShowAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2812
    const v0, 0x7f04004e

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    .line 2813
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2814
    const v0, 0x7f04004f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    .line 2815
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2816
    return-void
.end method

.method private initPocketTrayLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 4587
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0038

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    .line 4588
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0039

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    .line 4589
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f003c

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTrayBody:Landroid/widget/LinearLayout;

    .line 4590
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f003b

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    .line 4591
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f003e

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    .line 4592
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0041

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    .line 4593
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0044

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    .line 4594
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f003d

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditItem:Landroid/widget/RelativeLayout;

    .line 4595
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0040

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;

    .line 4596
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0043

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    .line 4597
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f003f

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    .line 4598
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0042

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateText:Landroid/widget/TextView;

    .line 4599
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0045

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpText:Landroid/widget/TextView;

    .line 4600
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 4601
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v0, :cond_1

    .line 4602
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4606
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4607
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4608
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditItem:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4609
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4610
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHelpListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4611
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    invoke-direct {p0, v0, v3, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updatePocketTrayLayout(IZZ)V

    .line 4613
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4614
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4616
    :cond_0
    return-void

    .line 4604
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private initRecentLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 4572
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4573
    .local v1, "recentLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0010

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 4574
    .local v0, "recentLabel":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0011

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentIcon:Landroid/widget/ImageView;

    .line 4575
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v4, 0x7f0a001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 4577
    .local v2, "topPadding":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 4579
    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 4580
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 4581
    const-string v3, "Recent"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4582
    const/16 v3, 0x7f

    invoke-static {v3, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 4583
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4584
    return-void
.end method

.method private isLaunchingBlockedItem(I)Z
    .locals 1
    .param p1, "curDstIndex"    # I

    .prologue
    .line 4489
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->isLaunchingBlockedItem(I)Z

    move-result v0

    return v0
.end method

.method private isPortrait()Z
    .locals 2

    .prologue
    .line 401
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeFlashBarLayout(Z)V
    .locals 7
    .param p1, "bAni"    # Z

    .prologue
    const v6, 0x7f080001

    const/16 v5, 0x11

    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1426
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    .line 1427
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    .line 1428
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    .line 1430
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0034

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    .line 1431
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0035

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    .line 1433
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0013

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    .line 1434
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0017

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    .line 1435
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0015

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    .line 1436
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    .line 1437
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0016

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;

    .line 1438
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001a

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;

    .line 1439
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f000f

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    .line 1441
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0037

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    .line 1442
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0022

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001b

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    .line 1447
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->setScrimColor(I)V

    .line 1448
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->setVisibility(I)V

    .line 1450
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setVerticalScrollBarEnabled(Z)V

    .line 1451
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setVisibility(I)V

    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1488
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setVisibility(I)V

    .line 1492
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->initRecentLayout()V

    .line 1493
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->initPocketTrayLayout()V

    .line 1495
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setGravity(I)V

    .line 1496
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setGravity(I)V

    .line 1497
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v0, :cond_1

    .line 1498
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1499
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1506
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1507
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1508
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1509
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1510
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1512
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1513
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1515
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1516
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1517
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1518
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1520
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1521
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1524
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1525
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1527
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f000e

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    .line 1528
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1529
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1530
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1531
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1533
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1534
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1536
    if-eqz p1, :cond_0

    .line 1537
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getFlashBarState()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1538
    invoke-direct {p0, v3, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startFlashBarAnimation(ZZ)V

    .line 1544
    :cond_0
    :goto_1
    return-void

    .line 1502
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1503
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1540
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarState(Z)V

    .line 1541
    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startFlashBarAnimation(ZZ)V

    goto :goto_1
.end method

.method private makeGuideLineLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 3894
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f008d

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    .line 3895
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f008e

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    .line 3896
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0091

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    .line 3897
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0094

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    .line 3898
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0095

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;

    .line 3899
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0096

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFocusGuideline:Landroid/widget/ImageView;

    .line 3900
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0097

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    .line 3901
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f0098

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3902
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f0099

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3903
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009a

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3904
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009b

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3905
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009c

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3906
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009d

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3907
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009e

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3908
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009f

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 3909
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f00a0

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    .line 3910
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 3911
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 3912
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 3913
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3914
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFocusGuideline:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3915
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3916
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3917
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0056

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    .line 3919
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGuidelineDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 3921
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 3922
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 3923
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3924
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 3925
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FlashBarService/GuideLine "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 3926
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3927
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3928
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3930
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$56;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$56;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3937
    return-void
.end method

.method private makeHistoryBarDialog(I)V
    .locals 34
    .param p1, "type"    # I

    .prologue
    .line 723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    const-string v32, "layout_inflater"

    invoke-virtual/range {v31 .. v32}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/LayoutInflater;

    .line 725
    .local v11, "layoutInflater":Landroid/view/LayoutInflater;
    const/16 v28, 0x0

    .line 726
    .local v28, "thumbNailCnt":I
    const/4 v8, 0x0

    .line 727
    .local v8, "hasThumb":Z
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailType:I

    .line 729
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mActivityManager:Landroid/app/ActivityManager;

    move-object/from16 v31, v0

    const/16 v32, 0x64

    invoke-virtual/range {v31 .. v32}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v24

    .line 731
    .local v24, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_3

    .line 732
    const/high16 v31, 0x7f030000

    const/16 v32, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    .line 736
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0001

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ScrollView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    .line 737
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 759
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0002

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0004

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0005

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    .line 762
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0007

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    .line 763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0006

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

    .line 765
    new-instance v31, Landroid/app/Dialog;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-direct/range {v31 .. v32}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    .line 766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    invoke-virtual/range {v31 .. v32}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 769
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v30

    .line 770
    .local v30, "w":Landroid/view/Window;
    const/high16 v31, 0x10000

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->addFlags(I)V

    .line 771
    const/16 v31, 0x8

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->addFlags(I)V

    .line 772
    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 773
    const/16 v31, -0x3

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->setFormat(I)V

    .line 774
    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->setDimAmount(F)V

    .line 776
    invoke-virtual/range {v30 .. v30}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v18

    .line 777
    .local v18, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v31, 0x3e8

    move/from16 v0, v31

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 778
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    iput-object v0, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 780
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->clear()V

    .line 781
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I

    move/from16 v31, v0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    .line 782
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/FlashBarService/AppListController;->getSelectedTaskAffinity()Ljava/lang/String;

    move-result-object v21

    .line 784
    .local v21, "selectedAffinity":Ljava/lang/String;
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v31

    add-int/lit8 v9, v31, -0x1

    .local v9, "i":I
    :goto_1
    if-ltz v9, :cond_6

    .line 785
    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 786
    .local v23, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v31, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v20

    .line 787
    .local v20, "runningAffinity":Ljava/lang/String;
    if-eqz v21, :cond_0

    if-eqz v20, :cond_0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_1

    :cond_0
    const/16 v31, 0x2

    move/from16 v0, p1

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    .line 788
    :cond_1
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 784
    :cond_2
    :goto_2
    add-int/lit8 v9, v9, -0x1

    goto :goto_1

    .line 734
    .end local v9    # "i":I
    .end local v18    # "p":Landroid/view/WindowManager$LayoutParams;
    .end local v20    # "runningAffinity":Ljava/lang/String;
    .end local v21    # "selectedAffinity":Ljava/lang/String;
    .end local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v30    # "w":Landroid/view/Window;
    :cond_3
    const v31, 0x7f030001

    const/16 v32, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogView:Landroid/view/View;

    goto/16 :goto_0

    .line 791
    .restart local v9    # "i":I
    .restart local v18    # "p":Landroid/view/WindowManager$LayoutParams;
    .restart local v20    # "runningAffinity":Ljava/lang/String;
    .restart local v21    # "selectedAffinity":Ljava/lang/String;
    .restart local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v30    # "w":Landroid/view/Window;
    :cond_4
    move-object/from16 v0, v23

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->userId:I

    move/from16 v31, v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_2

    .line 796
    :try_start_0
    move-object/from16 v0, v23

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move/from16 v26, v0

    .line 797
    .local v26, "taskid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mActivityManager:Landroid/app/ActivityManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getTaskThumbnail(I)Landroid/app/ActivityManager$TaskThumbnail;

    move-result-object v25

    .line 798
    .local v25, "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    if-eqz v25, :cond_7

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/app/ActivityManager$TaskThumbnail;->mainThumbnail:Landroid/graphics/Bitmap;

    move-object/from16 v27, v0

    .line 800
    .local v27, "thumb":Landroid/graphics/Bitmap;
    :goto_3
    if-nez v27, :cond_5

    .line 801
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    check-cast v6, Landroid/graphics/drawable/BitmapDrawable;

    .line 802
    .local v6, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v27

    .line 805
    .end local v6    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_5
    if-eqz v27, :cond_2

    .line 806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-lt v0, v1, :cond_8

    .line 1003
    .end local v20    # "runningAffinity":Ljava/lang/String;
    .end local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v25    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .end local v26    # "taskid":I
    .end local v27    # "thumb":Landroid/graphics/Bitmap;
    :cond_6
    if-eqz v8, :cond_c

    .line 1004
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v22

    .line 1005
    .local v22, "size":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v31

    move/from16 v0, v22

    move/from16 v1, v31

    if-lt v0, v1, :cond_b

    .line 1006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1007
    const/4 v9, 0x0

    :goto_4
    move/from16 v0, v22

    if-ge v9, v0, :cond_b

    .line 1008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/RelativeLayout;

    .line 1009
    .local v19, "r":Landroid/widget/RelativeLayout;
    new-instance v12, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual/range {v19 .. v19}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-direct {v12, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1010
    .local v12, "layoutMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutHeight:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutPadding:I

    move/from16 v32, v0

    add-int v31, v31, v32

    sub-int v32, v22, v9

    add-int/lit8 v32, v32, -0x1

    mul-int v15, v31, v32

    .line 1011
    .local v15, "margin":I
    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v12, v0, v15, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1012
    new-instance v31, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v31

    invoke-direct {v0, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1007
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 798
    .end local v12    # "layoutMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v15    # "margin":I
    .end local v19    # "r":Landroid/widget/RelativeLayout;
    .end local v22    # "size":I
    .restart local v20    # "runningAffinity":Ljava/lang/String;
    .restart local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v25    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .restart local v26    # "taskid":I
    :cond_7
    const/16 v27, 0x0

    goto/16 :goto_3

    .line 808
    .restart local v27    # "thumb":Landroid/graphics/Bitmap;
    :cond_8
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 810
    new-instance v5, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-direct {v5, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 811
    .local v5, "AppThumbnailLayout":Landroid/widget/RelativeLayout;
    new-instance v13, Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutWidth:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutHeight:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-direct {v13, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 812
    .local v13, "layoutParam":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v5, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 813
    const v31, 0x7f020073

    move/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 815
    new-instance v4, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-direct {v4, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 816
    .local v4, "AppThumbnailImageView":Landroid/widget/ImageView;
    sget-object v31, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 817
    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 818
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailImageWidth:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailImageHeight:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 819
    .local v14, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v31, 0xd

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 820
    invoke-virtual {v5, v4, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 822
    new-instance v10, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-direct {v10, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 823
    .local v10, "imageView":Landroid/widget/ImageView;
    const v31, 0x7f020076

    move/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 824
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThbumNailCancelImageSize:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThbumNailCancelImageSize:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 825
    .restart local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v31, v0

    const v32, 0x7f0a0062

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v17, v0

    .line 826
    .local v17, "minusIconTopMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v31, v0

    const v32, 0x7f0a0063

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v16, v0

    .line 827
    .local v16, "minusIconRightMargin":I
    move/from16 v0, v17

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 828
    move/from16 v0, v16

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 829
    const/16 v31, 0xb

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 830
    const/16 v31, 0xa

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 831
    invoke-virtual {v10, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 832
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 833
    new-instance v31, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 844
    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 846
    if-nez v8, :cond_a

    .line 847
    const/4 v8, 0x1

    .line 848
    const/16 v31, 0x2

    move/from16 v0, p1

    move/from16 v1, v31

    if-eq v0, v1, :cond_a

    .line 849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    move/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v31

    if-nez v31, :cond_9

    .line 851
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 852
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    new-instance v32, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 958
    :cond_a
    new-instance v31, Lcom/sec/android/app/FlashBarService/AppListGridWindow$8;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 972
    new-instance v31, Lcom/sec/android/app/FlashBarService/AppListGridWindow$9;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 991
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v28

    .line 992
    new-instance v29, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v31

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 993
    .local v29, "thumbnailMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutHeight:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutPadding:I

    move/from16 v32, v0

    add-int v31, v31, v32

    mul-int v15, v31, v28

    .line 994
    .restart local v15    # "margin":I
    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 995
    new-instance v31, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 997
    .end local v4    # "AppThumbnailImageView":Landroid/widget/ImageView;
    .end local v5    # "AppThumbnailLayout":Landroid/widget/RelativeLayout;
    .end local v10    # "imageView":Landroid/widget/ImageView;
    .end local v13    # "layoutParam":Landroid/view/ViewGroup$LayoutParams;
    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v15    # "margin":I
    .end local v16    # "minusIconRightMargin":I
    .end local v17    # "minusIconTopMargin":I
    .end local v25    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .end local v26    # "taskid":I
    .end local v27    # "thumb":Landroid/graphics/Bitmap;
    .end local v29    # "thumbnailMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    :catch_0
    move-exception v7

    .line 998
    .local v7, "e":Ljava/lang/SecurityException;
    invoke-virtual {v7}, Ljava/lang/SecurityException;->printStackTrace()V

    goto/16 :goto_2

    .line 1015
    .end local v7    # "e":Ljava/lang/SecurityException;
    .end local v20    # "runningAffinity":Ljava/lang/String;
    .end local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v22    # "size":I
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->showHistoryBarUi(II)V

    .line 1016
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V

    .line 1020
    .end local v22    # "size":I
    :goto_5
    return-void

    .line 1018
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V

    goto :goto_5
.end method

.method private makeTraybarHelpPopupLayout()V
    .locals 14

    .prologue
    .line 1236
    :try_start_0
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const-string v13, "layout_inflater"

    invoke-virtual {v12, v13}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 1237
    .local v6, "layoutInflater":Landroid/view/LayoutInflater;
    const v12, 0x7f030019

    const/4 v13, 0x0

    invoke-virtual {v6, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 1238
    .local v11, "trayBarHelpPopupView":Landroid/view/View;
    if-nez v11, :cond_0

    .line 1376
    .end local v6    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v11    # "trayBarHelpPopupView":Landroid/view/View;
    :goto_0
    return-void

    .line 1242
    .restart local v6    # "layoutInflater":Landroid/view/LayoutInflater;
    .restart local v11    # "trayBarHelpPopupView":Landroid/view/View;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHelpPopupWindowTraybar()V

    .line 1243
    new-instance v12, Landroid/widget/PopupWindow;

    invoke-direct {v12, v11}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 1244
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    iget v13, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 1245
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    iget v13, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1246
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1247
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 1248
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    new-instance v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow$13;

    invoke-direct {v13, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$13;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v12, v13}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1258
    const v12, 0x7f0f00bc

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1259
    .local v0, "bottomPicker":Landroid/widget/ImageView;
    const v12, 0x7f0f00b0

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 1260
    .local v9, "rightPicker":Landroid/widget/ImageView;
    const v12, 0x7f0f00be

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1261
    .local v7, "leftPicker":Landroid/widget/ImageView;
    const v12, 0x7f0f00bd

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 1262
    .local v10, "topPicker":Landroid/widget/ImageView;
    const v12, 0x7f0f00b3

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1264
    .local v2, "contents":Landroid/widget/TextView;
    new-instance v12, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;

    invoke-direct {v12, p0, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/TextView;)V

    invoke-virtual {v11, v12}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1308
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v12}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/Display;->getRotation()I

    move-result v12

    iput v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->lastDegrees:I

    .line 1309
    new-instance v12, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-direct {v12, p0, v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Context;)V

    iput-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orientationListener:Landroid/view/OrientationEventListener;

    .line 1322
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v12}, Landroid/view/OrientationEventListener;->enable()V

    .line 1324
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v12}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    .line 1325
    .local v4, "display":Landroid/view/Display;
    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v3

    .line 1326
    .local v3, "degrees":I
    packed-switch v3, :pswitch_data_0

    .line 1341
    :goto_1
    const v12, 0x7f0f00ad

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1342
    .local v1, "checkBox":Landroid/widget/CheckBox;
    iget-boolean v12, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsCheckBoxChecked:Z

    invoke-virtual {v1, v12}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1343
    new-instance v12, Lcom/sec/android/app/FlashBarService/AppListGridWindow$16;

    invoke-direct {v12, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$16;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v12}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1349
    const v12, 0x7f0f00ae

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 1350
    .local v5, "helpButton":Landroid/widget/Button;
    new-instance v12, Lcom/sec/android/app/FlashBarService/AppListGridWindow$17;

    invoke-direct {v12, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$17;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1365
    const v12, 0x7f0f00af

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 1366
    .local v8, "okButton":Landroid/widget/Button;
    new-instance v12, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;

    invoke-direct {v12, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v8, v12}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1374
    .end local v0    # "bottomPicker":Landroid/widget/ImageView;
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    .end local v2    # "contents":Landroid/widget/TextView;
    .end local v3    # "degrees":I
    .end local v4    # "display":Landroid/view/Display;
    .end local v5    # "helpButton":Landroid/widget/Button;
    .end local v6    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v7    # "leftPicker":Landroid/widget/ImageView;
    .end local v8    # "okButton":Landroid/widget/Button;
    .end local v9    # "rightPicker":Landroid/widget/ImageView;
    .end local v10    # "topPicker":Landroid/widget/ImageView;
    .end local v11    # "trayBarHelpPopupView":Landroid/view/View;
    :catch_0
    move-exception v12

    goto/16 :goto_0

    .line 1328
    .restart local v0    # "bottomPicker":Landroid/widget/ImageView;
    .restart local v2    # "contents":Landroid/widget/TextView;
    .restart local v3    # "degrees":I
    .restart local v4    # "display":Landroid/view/Display;
    .restart local v6    # "layoutInflater":Landroid/view/LayoutInflater;
    .restart local v7    # "leftPicker":Landroid/widget/ImageView;
    .restart local v9    # "rightPicker":Landroid/widget/ImageView;
    .restart local v10    # "topPicker":Landroid/widget/ImageView;
    .restart local v11    # "trayBarHelpPopupView":Landroid/view/View;
    :pswitch_0
    const/4 v12, 0x0

    invoke-virtual {v0, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1331
    :pswitch_1
    const/4 v12, 0x0

    invoke-virtual {v9, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1334
    :pswitch_2
    const/4 v12, 0x0

    invoke-virtual {v10, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1337
    :pswitch_3
    const/4 v12, 0x0

    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1326
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private openFlashBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2653
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    if-eqz v1, :cond_0

    .line 2677
    :goto_0
    return-void

    .line 2656
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    .line 2657
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080019

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2658
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 2661
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    .line 2663
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 2664
    const/4 v0, 0x0

    .line 2666
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x66

    if-ne v1, v2, :cond_2

    .line 2667
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2675
    :goto_1
    invoke-direct {p0, v0, v4, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V

    .line 2676
    invoke-direct {p0, v4, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V

    goto :goto_0

    .line 2668
    :cond_2
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_3

    .line 2669
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003f

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 2670
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_4

    .line 2671
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 2673
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1
.end method

.method private prepareAnimationIcon()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3381
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 3382
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    rsub-int/lit8 v1, v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 3383
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    rsub-int/lit8 v1, v1, 0x0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 3384
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 3385
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3386
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v1, :cond_0

    .line 3387
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 3388
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3390
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3391
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3392
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 3394
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->isPortrait()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3395
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const v2, 0x7f02007b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3399
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 3400
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 3401
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3402
    return-void

    .line 3397
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const v2, 0x7f02007c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private saveFlashBarCurPosition(I)V
    .locals 3
    .param p1, "flashBarPosition"    # I

    .prologue
    .line 2065
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarPosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2066
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarPosition"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 2067
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2068
    return-void
.end method

.method private saveFlashBarHandlePosition(I)V
    .locals 3
    .param p1, "handlePosition"    # I

    .prologue
    .line 2079
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarHandlePosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2080
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarHandlePosition"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 2081
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2082
    return-void
.end method

.method private setAppListHandlePosition(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/high16 v7, 0x425c0000    # 55.0f

    const/high16 v3, 0x42340000    # 45.0f

    const/4 v6, 0x0

    const/high16 v5, 0x42480000    # 50.0f

    const/high16 v4, 0x42c80000    # 100.0f

    .line 4169
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v2, 0x7f0a0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 4170
    .local v0, "moveBtnWidth":I
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    packed-switch v1, :pswitch_data_0

    .line 4258
    :goto_0
    return-void

    .line 4172
    :pswitch_0
    div-int/lit8 v1, v0, 0x2

    if-gt p1, v1, :cond_0

    .line 4173
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 4182
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->saveFlashBarHandlePosition(I)V

    goto :goto_0

    .line 4174
    :cond_0
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    if-le v1, v2, :cond_1

    .line 4175
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 4177
    :cond_1
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_2

    .line 4178
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 4180
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 4185
    :pswitch_1
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_3

    .line 4186
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->bottom:I

    .line 4195
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->saveFlashBarHandlePosition(I)V

    goto :goto_0

    .line 4187
    :cond_3
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    if-le v1, v2, :cond_4

    .line 4188
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 4190
    :cond_4
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_5

    .line 4191
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 4193
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 4198
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfigChanged:Z

    if-eqz v1, :cond_7

    .line 4199
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_6

    .line 4200
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 4204
    :goto_3
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfigChanged:Z

    .line 4225
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->saveFlashBarHandlePosition(I)V

    goto/16 :goto_0

    .line 4202
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_3

    .line 4205
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z

    if-eqz v1, :cond_a

    .line 4206
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_9

    .line 4207
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 4211
    :cond_8
    :goto_5
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z

    goto :goto_4

    .line 4208
    :cond_9
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    if-nez v1, :cond_8

    .line 4209
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_5

    .line 4213
    :cond_a
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_b

    .line 4214
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->left:I

    .line 4223
    :goto_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_4

    .line 4215
    :cond_b
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    if-le v1, v2, :cond_c

    .line 4216
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_6

    .line 4218
    :cond_c
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_d

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_d

    .line 4219
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_6

    .line 4221
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_6

    .line 4228
    :pswitch_3
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfigChanged:Z

    if-eqz v1, :cond_f

    .line 4229
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_e

    .line 4230
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 4234
    :goto_7
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfigChanged:Z

    .line 4255
    :goto_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->saveFlashBarHandlePosition(I)V

    goto/16 :goto_0

    .line 4232
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_7

    .line 4235
    :cond_f
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z

    if-eqz v1, :cond_12

    .line 4236
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_11

    .line 4237
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 4241
    :cond_10
    :goto_9
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z

    goto :goto_8

    .line 4238
    :cond_11
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    if-nez v1, :cond_10

    .line 4239
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_9

    .line 4243
    :cond_12
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_13

    .line 4244
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->right:I

    .line 4253
    :goto_a
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_8

    .line 4245
    :cond_13
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    if-le v1, v2, :cond_14

    .line 4246
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_a

    .line 4248
    :cond_14
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_15

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_15

    .line 4249
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_a

    .line 4251
    :cond_15
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_a

    .line 4170
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V
    .locals 5
    .param p1, "ani"    # Landroid/view/animation/Animation;
    .param p2, "expand"    # Z
    .param p3, "finishtray"    # Z

    .prologue
    const/16 v4, 0xca

    const/16 v3, 0x8

    .line 2742
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    if-nez v1, :cond_0

    .line 2808
    :goto_0
    return-void

    .line 2745
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, p2}, Lcom/sec/android/app/FlashBarService/AppListController;->setMWTrayOpenState(Z)V

    .line 2747
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 2748
    .local v0, "currentAni":Landroid/view/animation/Animation;
    if-eqz v0, :cond_1

    .line 2749
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2750
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 2752
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 2753
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2757
    :cond_1
    if-eqz p2, :cond_3

    .line 2758
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 2759
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2761
    :cond_2
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZLandroid/view/animation/Animation;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    .line 2786
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2789
    :cond_3
    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    .line 2791
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2792
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2795
    :cond_4
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;

    invoke-direct {v1, p0, p3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$22;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V

    invoke-virtual {p1, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2806
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private setImageToMoveButton(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "expand"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2097
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPressMoveBtn:Z

    .line 2098
    .local v0, "press":Z
    packed-switch p1, :pswitch_data_0

    .line 2160
    :goto_0
    return-void

    .line 2100
    :pswitch_0
    if-ne p2, v1, :cond_1

    .line 2101
    if-ne v0, v1, :cond_0

    .line 2102
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f02010c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2104
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f020109

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2107
    :cond_1
    if-ne v0, v1, :cond_2

    .line 2108
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f02010b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2110
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f02010a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2115
    :pswitch_1
    if-ne p2, v1, :cond_4

    .line 2116
    if-ne v0, v1, :cond_3

    .line 2117
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2119
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b5

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2122
    :cond_4
    if-ne v0, v1, :cond_5

    .line 2123
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b7

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2125
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b6

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2130
    :pswitch_2
    if-ne p2, v1, :cond_7

    .line 2131
    if-ne v0, v1, :cond_6

    .line 2132
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200eb

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2134
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200e8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2137
    :cond_7
    if-ne v0, v1, :cond_8

    .line 2138
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200ea

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2140
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200e9

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2145
    :pswitch_3
    if-ne p2, v1, :cond_a

    .line 2146
    if-ne v0, v1, :cond_9

    .line 2147
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f020100

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2149
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200fd

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2152
    :cond_a
    if-ne v0, v1, :cond_b

    .line 2153
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200ff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2155
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200fe

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2098
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showHistoryBarUi(II)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "cnt"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    .line 1023
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v6, :cond_1

    .line 1024
    const/4 v0, 0x0

    .line 1025
    .local v0, "gravity":I
    const/4 v3, 0x0

    .local v3, "x":I
    const/4 v4, 0x0

    .line 1026
    .local v4, "y":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v2, v6, 0xa

    .line 1027
    .local v2, "padding":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    packed-switch v6, :pswitch_data_0

    .line 1049
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1050
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1051
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1052
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1053
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v7

    invoke-virtual {v6, v5, v5, v5, v7}, Landroid/widget/ScrollView;->setPaddingRelative(IIII)V

    .line 1054
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 1055
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1057
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 1058
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarUI()V

    .line 1060
    .end local v0    # "gravity":I
    .end local v1    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "padding":I
    .end local v3    # "x":I
    .end local v4    # "y":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V

    .line 1061
    return-void

    .line 1029
    .restart local v0    # "gravity":I
    .restart local v2    # "padding":I
    .restart local v3    # "x":I
    .restart local v4    # "y":I
    :pswitch_0
    const/16 v0, 0x33

    .line 1030
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getWidth()I

    move-result v6

    add-int v3, v6, v2

    .line 1031
    if-ne p1, v7, :cond_2

    move v4, v5

    .line 1032
    :goto_1
    goto :goto_0

    .line 1031
    :cond_2
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->findHistoryBarPosisition(I)I

    move-result v4

    goto :goto_1

    .line 1034
    :pswitch_1
    const/16 v0, 0x35

    .line 1035
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getWidth()I

    move-result v6

    add-int v3, v6, v2

    .line 1036
    if-ne p1, v7, :cond_3

    move v4, v5

    .line 1037
    :goto_2
    goto :goto_0

    .line 1036
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->findHistoryBarPosisition(I)I

    move-result v4

    goto :goto_2

    .line 1039
    :pswitch_2
    const/16 v0, 0x33

    .line 1040
    if-ne p1, v7, :cond_4

    move v3, v5

    .line 1041
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    .line 1042
    goto :goto_0

    .line 1040
    :cond_4
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    goto :goto_3

    .line 1044
    :pswitch_3
    const/16 v0, 0x53

    .line 1045
    if-ne p1, v7, :cond_5

    move v3, v5

    .line 1046
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    goto :goto_0

    .line 1045
    :cond_5
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    goto :goto_4

    .line 1027
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showTraybarHelpPopup()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1381
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1390
    :cond_0
    :goto_0
    return-void

    .line 1385
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V

    .line 1386
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeTraybarHelpPopupLayout()V

    .line 1387
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1388
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    const/16 v2, 0x33

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method private startCollapseTimer()V
    .locals 4

    .prologue
    const/16 v2, 0xc9

    .line 556
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 557
    .local v0, "msg":Landroid/os/Message;
    iput v2, v0, Landroid/os/Message;->what:I

    .line 558
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 559
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 560
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-nez v1, :cond_1

    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v1, :cond_1

    .line 561
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 562
    :cond_1
    return-void
.end method

.method private startFlashBarAnimation(ZZ)V
    .locals 5
    .param p1, "bStart"    # Z
    .param p2, "bExpand"    # Z

    .prologue
    const/16 v4, 0x68

    const/16 v3, 0x66

    const/16 v2, 0x65

    .line 2715
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mStartFlashBar:Z

    .line 2716
    const/4 v0, 0x0

    .line 2717
    .local v0, "ani":Landroid/view/animation/Animation;
    if-eqz p1, :cond_3

    .line 2718
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v1, v3, :cond_0

    .line 2719
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040045

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2736
    :goto_0
    if-nez p1, :cond_7

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v0, p2, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V

    .line 2737
    return-void

    .line 2720
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v1, v2, :cond_1

    .line 2721
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2722
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v1, v4, :cond_2

    .line 2723
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2725
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040049

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2727
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v1, v3, :cond_4

    .line 2728
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2729
    :cond_4
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v1, v2, :cond_5

    .line 2730
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040034

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2731
    :cond_5
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    if-ne v1, v4, :cond_6

    .line 2732
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040033

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2734
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040031

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 2736
    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private updateAppListPosition(IZZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "sticky"    # Z
    .param p3, "expand"    # Z

    .prologue
    .line 2236
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZZ)V

    .line 2237
    return-void
.end method

.method private updateAppListPosition(IZZZ)V
    .locals 23
    .param p1, "position"    # I
    .param p2, "sticky"    # Z
    .param p3, "expand"    # Z
    .param p4, "savePosition"    # Z

    .prologue
    .line 2240
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    const/16 v19, 0x1

    move/from16 v0, p3

    move/from16 v1, v19

    if-ne v0, v1, :cond_0

    .line 2241
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateRunningAppList()V

    .line 2243
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v14

    .line 2244
    .local v14, "l":Landroid/view/WindowManager$LayoutParams;
    new-instance v4, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v4, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2245
    .local v4, "appListMarginHorizontal":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v5, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2246
    .local v5, "appListMarginVertical":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v16, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2247
    .local v16, "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-direct {v6, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2249
    .local v6, "appTrayBGLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2250
    .local v10, "editLayout":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2252
    .local v11, "editLayout_h":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a0018

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v17, v0

    .line 2253
    .local v17, "moveBtnWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a0019

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v15, v0

    .line 2254
    .local v15, "moveBtnHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0a001d

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v7, v0

    .line 2255
    .local v7, "applistHeight":I
    move v13, v7

    .line 2256
    .local v13, "flashBarHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0a0019

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v19, v19, v20

    add-int v13, v13, v19

    .line 2259
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a0020

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v18, v0

    .line 2260
    .local v18, "scrollerMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a0053

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v8, v0

    .line 2262
    .local v8, "applistSidePadding":I
    const/4 v9, 0x0

    .line 2263
    .local v9, "applistTopPadding":I
    const/16 v19, 0x67

    move/from16 v0, p1

    move/from16 v1, v19

    if-ne v0, v1, :cond_3

    const/4 v12, 0x0

    .line 2265
    .local v12, "editMargin":I
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 2551
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setImageToMoveButton(IZ)V

    .line 2552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2553
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v20

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2554
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v14}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2556
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    .line 2558
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 2559
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    .line 2560
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    .line 2561
    const/16 v19, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V

    .line 2562
    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updatePocketTrayLayout(IZZ)V

    .line 2564
    if-eqz p3, :cond_1

    .line 2565
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->setMWTrayOpenState(Z)V

    .line 2567
    :cond_1
    if-eqz p4, :cond_2

    .line 2568
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->saveFlashBarCurPosition(I)V

    .line 2570
    :cond_2
    return-void

    .line 2263
    .end local v12    # "editMargin":I
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    sub-int v12, v15, v19

    goto/16 :goto_0

    .line 2267
    .restart local v12    # "editMargin":I
    :pswitch_0
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 2268
    const/16 v19, 0x1

    move/from16 v0, p3

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    .line 2269
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2270
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v19

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2271
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2272
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2273
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v7, v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2274
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f08001a

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2277
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2300
    :goto_2
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v11, v0, v1, v2, v15}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2302
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2303
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2305
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2306
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a0049

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2307
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2309
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->isPortrait()Z

    move-result v19

    if-eqz v19, :cond_6

    .line 2310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f020107

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2313
    :goto_3
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2314
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2315
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2317
    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2318
    move-object/from16 v0, v16

    iput v15, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2279
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2280
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2281
    move/from16 v0, v17

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2282
    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2283
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2284
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f080019

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2287
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2290
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I

    move/from16 v19, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v20

    sub-int v19, v19, v20

    div-int/lit8 v20, v17, 0x2

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2291
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    move/from16 v19, v0

    div-int/lit8 v20, v17, 0x2

    sub-int v20, v13, v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2292
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2293
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2294
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v7, v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2295
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2312
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f020108

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 2323
    :pswitch_1
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_8

    .line 2324
    const/16 v19, 0x1

    move/from16 v0, p3

    move/from16 v1, v19

    if-ne v0, v1, :cond_7

    .line 2325
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2326
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    sub-int v19, v19, v13

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2327
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2328
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2329
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2330
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f08001a

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2358
    :goto_4
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v11, v0, v15, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2363
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v4, v0, v15, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2364
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a0049

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2365
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v20

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2367
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->isPortrait()Z

    move-result v19

    if-eqz v19, :cond_9

    .line 2368
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200b3

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2371
    :goto_5
    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v20, v15, v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2372
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2373
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2375
    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2376
    move-object/from16 v0, v16

    iput v15, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2378
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2336
    :cond_7
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2337
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    sub-int v19, v19, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2338
    move/from16 v0, v17

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2339
    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2340
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2341
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f080019

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 2348
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I

    move/from16 v19, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v20

    sub-int v19, v19, v20

    div-int/lit8 v20, v17, 0x2

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2349
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    move/from16 v19, v0

    div-int/lit8 v20, v15, 0x2

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2350
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2351
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2352
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2353
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2355
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 2370
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200b4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    .line 2381
    :pswitch_2
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_c

    .line 2382
    const/16 v19, 0x1

    move/from16 v0, p3

    move/from16 v1, v19

    if-ne v0, v1, :cond_b

    .line 2383
    const/16 v19, 0x0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2384
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v19

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2385
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2386
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2387
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2388
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v7, v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200e8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f08001a

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2392
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2393
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    .line 2394
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2428
    :cond_a
    :goto_6
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v10, v0, v1, v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_d

    .line 2431
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2435
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_e

    .line 2438
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a001f

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v9, v0

    .line 2443
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getEditAreaHeight()I

    move-result v20

    sub-int v19, v19, v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    sub-int v19, v19, v9

    move/from16 v0, v19

    iput v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2447
    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v5, v0, v9, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2450
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v8, v1, v8, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200e6

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2454
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2455
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2456
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2458
    move-object/from16 v0, v16

    iput v15, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2459
    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2461
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2398
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    rsub-int/lit8 v19, v19, 0x0

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2399
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v19

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v20

    add-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2400
    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2401
    move/from16 v0, v17

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2402
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2403
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f080019

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2405
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    .line 2408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2409
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_6

    .line 2413
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I

    move/from16 v19, v0

    div-int/lit8 v20, v17, 0x2

    sub-int v20, v13, v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2414
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    add-int v19, v19, v20

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v20

    sub-int v19, v19, v20

    div-int/lit8 v20, v17, 0x2

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2415
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2416
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2417
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2418
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v7, v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2419
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200e8

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2421
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2422
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_a

    .line 2423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2424
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x3

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_6

    .line 2433
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_7

    .line 2440
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a001e

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v9, v0

    goto/16 :goto_8

    .line 2464
    :pswitch_3
    const/16 v19, 0x1

    move/from16 v0, p2

    move/from16 v1, v19

    if-ne v0, v1, :cond_11

    .line 2465
    const/16 v19, 0x1

    move/from16 v0, p3

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    .line 2466
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    sub-int v19, v19, v13

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2467
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v19

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2468
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2469
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2470
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2471
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200fd

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f08001a

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2475
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_f

    .line 2477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x5

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2515
    :cond_f
    :goto_9
    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v10, v12, v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_12

    .line 2518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2522
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2524
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v19, v0

    const/16 v20, 0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_13

    .line 2525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a001f

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v9, v0

    .line 2530
    :goto_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getEditAreaHeight()I

    move-result v20

    sub-int v19, v19, v20

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    sub-int v19, v19, v9

    move/from16 v0, v19

    iput v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2534
    add-int v19, v15, v18

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v5, v0, v9, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2536
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    new-instance v20, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v20

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2537
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v21

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v8, v1, v8, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2540
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200fb

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2541
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    sub-int v19, v15, v19

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2542
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2543
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2545
    move-object/from16 v0, v16

    iput v15, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2546
    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2548
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2483
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    move/from16 v19, v0

    sub-int v19, v19, v15

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2484
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v19

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v20

    add-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2485
    iput v15, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2486
    move/from16 v0, v17

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2487
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2488
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x7f080019

    invoke-virtual/range {v20 .. v21}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2490
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2491
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_f

    .line 2493
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x5

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_9

    .line 2498
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I

    move/from16 v19, v0

    div-int/lit8 v20, v15, 0x2

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2499
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    add-int v19, v19, v20

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v20

    sub-int v19, v19, v20

    div-int/lit8 v20, v17, 0x2

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2500
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2501
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    move/from16 v19, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2502
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getAppListHandlePosition()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2503
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, v16

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2504
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const v20, 0x7f0200fd

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2505
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2506
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    if-eqz v19, :cond_f

    .line 2508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x5

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2510
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x4

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_9

    .line 2520
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_a

    .line 2527
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0a001e

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v9, v0

    goto/16 :goto_b

    .line 2265
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updatePocketTrayButtonText()V
    .locals 3

    .prologue
    .line 2573
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNeedUpdatePocketTrayButtonText:Z

    if-nez v0, :cond_1

    .line 2589
    :cond_0
    :goto_0
    return-void

    .line 2577
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 2582
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v0, :cond_2

    const v0, 0x7f080002

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2583
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2584
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2586
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 2587
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 2588
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0

    .line 2582
    :cond_2
    const v0, 0x7f080001

    goto :goto_1
.end method

.method private updatePocketTrayLayout(IZZ)V
    .locals 11
    .param p1, "position"    # I
    .param p2, "isStarting"    # Z
    .param p3, "forceClose"    # Z

    .prologue
    const v9, 0x7f02003a

    const/4 v10, 0x0

    .line 2593
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-nez v7, :cond_0

    .line 2650
    :goto_0
    return-void

    .line 2597
    :cond_0
    new-instance v4, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2598
    .local v4, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2600
    .local v5, "scrollLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0019

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v0, v7

    .line 2601
    .local v0, "moveBtnHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a006b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v1, v7

    .line 2602
    .local v1, "openButtonHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0068

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v2, v7

    .line 2603
    .local v2, "openPocketTrayHeght":I
    iget v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2604
    .local v6, "scrollViewHeight":I
    move v3, v6

    .line 2606
    .local v3, "pocketTopMargin":I
    if-eqz p3, :cond_1

    .line 2607
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v8

    add-int/2addr v8, v1

    sub-int v3, v7, v8

    .line 2608
    move v6, v3

    .line 2609
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v7, v9}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 2610
    iput-boolean v10, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    .line 2630
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 2640
    :goto_2
    iput v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2641
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2642
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2644
    if-nez p2, :cond_5

    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    if-nez v7, :cond_5

    .line 2645
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTrayBody:Landroid/widget/LinearLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 2612
    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    if-eqz v7, :cond_3

    .line 2613
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    add-int v8, v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v9

    add-int/2addr v8, v9

    sub-int v3, v7, v8

    .line 2614
    if-eqz p2, :cond_2

    .line 2615
    add-int v6, v3, v2

    .line 2619
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    const v8, 0x7f02003b

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 2617
    :cond_2
    move v6, v3

    goto :goto_3

    .line 2621
    :cond_3
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v8

    add-int/2addr v8, v1

    sub-int v6, v7, v8

    .line 2622
    if-eqz p2, :cond_4

    .line 2623
    sub-int v3, v6, v2

    .line 2627
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v7, v9}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 2625
    :cond_4
    move v3, v6

    goto :goto_4

    .line 2632
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0200ec

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 2633
    invoke-virtual {v4, v10, v3, v10, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 2636
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    const v8, 0x7f020101

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 2637
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnOverlapMargin:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtnShadowMargin:I

    add-int/2addr v7, v8

    sub-int v7, v0, v7

    invoke-virtual {v4, v7, v3, v10, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 2647
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updatePocketTrayButtonText()V

    .line 2648
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTrayBody:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 2630
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateRunningAppList()V
    .locals 0

    .prologue
    .line 553
    return-void
.end method

.method private updateScrollPositionTemplate()V
    .locals 2

    .prologue
    .line 4554
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$60;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$60;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 4562
    return-void
.end method

.method private updateSplitGuidePosition(ZZ)V
    .locals 12
    .param p1, "expand"    # Z
    .param p2, "requestPreview"    # Z

    .prologue
    .line 2163
    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v7, :cond_1

    .line 2233
    :cond_0
    :goto_0
    return-void

    .line 2166
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    if-eqz v7, :cond_0

    .line 2170
    if-eqz p1, :cond_6

    .line 2171
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2172
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    rsub-int/lit8 v7, v7, 0x0

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2173
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    rsub-int/lit8 v7, v7, 0x0

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2174
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2175
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2176
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v7, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2177
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2178
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2180
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 2181
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect()Landroid/graphics/Rect;

    move-result-object v5

    .line 2183
    .local v5, "rect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/16 v8, 0xc

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 2184
    .local v0, "bottomRect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 2185
    .local v4, "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v7

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2186
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v7

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2187
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2188
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2189
    .local v2, "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, v0, Landroid/graphics/Rect;->left:I

    iget v8, v0, Landroid/graphics/Rect;->top:I

    iget v9, v0, Landroid/graphics/Rect;->right:I

    iget v10, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    iget v11, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    invoke-virtual {v2, v7, v8, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2192
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2194
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 2195
    .local v6, "topRect":Landroid/graphics/Rect;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 2196
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v7

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2197
    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v7

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    iput v7, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2198
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2199
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    .end local v2    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-direct {v2, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2200
    .restart local v2    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    iget v7, v6, Landroid/graphics/Rect;->left:I

    iget v8, v6, Landroid/graphics/Rect;->top:I

    iget v9, v6, Landroid/graphics/Rect;->right:I

    iget v10, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    iget v10, v6, Landroid/graphics/Rect;->bottom:I

    iget v11, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I

    mul-int/lit8 v11, v11, 0x2

    add-int/2addr v10, v11

    invoke-virtual {v2, v7, v8, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2203
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2205
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2206
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2207
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v3

    .line 2209
    .local v3, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    if-nez v7, :cond_4

    .line 2210
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2211
    if-eqz v3, :cond_3

    const/4 v7, 0x2

    invoke-virtual {v3, v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-nez v7, :cond_4

    .line 2212
    :cond_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2216
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v5}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 2217
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2218
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v8, v8, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 2231
    .end local v0    # "bottomRect":Landroid/graphics/Rect;
    .end local v1    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v3    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v4    # "params":Landroid/view/ViewGroup$LayoutParams;
    .end local v5    # "rect":Landroid/graphics/Rect;
    .end local v6    # "topRect":Landroid/graphics/Rect;
    :cond_5
    :goto_1
    if-eqz p2, :cond_0

    .line 2232
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v7, p1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->requestSplitPreview(Z)V

    goto/16 :goto_0

    .line 2220
    :cond_6
    if-eqz p2, :cond_5

    .line 2221
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 2222
    .restart local v1    # "l":Landroid/view/WindowManager$LayoutParams;
    const/4 v7, 0x0

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    const/4 v7, 0x0

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v7, 0x0

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v7, 0x0

    iput v7, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2223
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v7, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2224
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-interface {v7, v8, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2225
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2227
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2228
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public animationRecentIconByHomeKey()V
    .locals 0

    .prologue
    .line 3362
    return-void
.end method

.method public canCloseWindow()Z
    .locals 1

    .prologue
    .line 4675
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4676
    const/4 v0, 0x0

    .line 4678
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cancelAllMessages()V
    .locals 6

    .prologue
    const/16 v5, 0xcd

    const/16 v4, 0xcc

    const/16 v3, 0xcb

    const/16 v2, 0xca

    const/16 v1, 0xc9

    .line 4520
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4521
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4523
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4524
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4526
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4527
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 4529
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4530
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 4532
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4533
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4535
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4536
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 4538
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4539
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4541
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4544
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4547
    :goto_0
    return-void

    .line 4545
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public checkAppListLayout()Z
    .locals 2

    .prologue
    .line 4412
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 4413
    const/4 v0, 0x1

    .line 4414
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closePocketTray()V
    .locals 2

    .prologue
    .line 3761
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 3762
    return-void
.end method

.method public createConfirmDialog(IZ)V
    .locals 10
    .param p1, "index"    # I
    .param p2, "bFromEdit"    # Z

    .prologue
    const/4 v6, 0x1

    .line 3035
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    if-eqz v3, :cond_0

    .line 3087
    :goto_0
    return-void

    .line 3037
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    .line 3038
    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbFromEdit:Z

    .line 3039
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDelIndex:I

    .line 3040
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080015

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080017

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDelIndex:I

    invoke-virtual {v8, v9, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppTitle(IZ)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080012

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow$30;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$30;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow$29;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$29;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3068
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    .line 3069
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3070
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3071
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3072
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3073
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3074
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3075
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow$32;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$32;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto/16 :goto_0
.end method

.method public createGestureHelp()V
    .locals 0

    .prologue
    .line 4744
    return-void
.end method

.method public createResetConfirmDialog()V
    .locals 6

    .prologue
    .line 3090
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    if-eqz v3, :cond_0

    .line 3130
    :goto_0
    return-void

    .line 3092
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    .line 3093
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f08004a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f08004b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080012

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow$33;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$33;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3111
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    .line 3112
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3113
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3114
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3115
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3116
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3117
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3118
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow$35;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$35;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    goto :goto_0
.end method

.method public createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "defaultTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 2926
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 2927
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030010

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 2928
    .local v0, "createDialogView":Landroid/view/View;
    const v5, 0x7f0f0085

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    .line 2929
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2930
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5, p2}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 2931
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->selectAll()V

    .line 2932
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    const/16 v6, 0x4000

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 2933
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    .line 2934
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    const-string v6, "disablePrediction=true;disableEmoticonInput=true;disableVoiceInput=true;noMicrophoneKey=true;inputType=DisableOCR"

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 2935
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;

    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow$26;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$26;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2964
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080013

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080011

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$28;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080012

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow$27;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$27;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 3009
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    .line 3010
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3011
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 3012
    .local v4, "w":Landroid/view/Window;
    const v5, 0x10200

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 3014
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 3015
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 3016
    .local v3, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x7d8

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3017
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    iput-object v5, v3, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3018
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 3019
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V

    .line 3020
    const/16 v5, 0x15

    invoke-virtual {v4, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 3021
    return-void
.end method

.method public createUnableDialog(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 3149
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateFlashBarState(ZZ)Z

    .line 3150
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080013

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow$36;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$36;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3159
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    .line 3160
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3161
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3162
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3163
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3164
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3165
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3166
    return-void
.end method

.method public dismissConfirmDialog()V
    .locals 1

    .prologue
    .line 3141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbSynchronizeConfirmDialog:Z

    .line 3142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3143
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3144
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    .line 3146
    :cond_0
    return-void
.end method

.method public dismissGestureOverlayHelp()V
    .locals 0

    .prologue
    .line 4747
    return-void
.end method

.method public dismissTemplateDialog()V
    .locals 2

    .prologue
    .line 3024
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3025
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 3026
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 3027
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 3029
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 3030
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    .line 3032
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method

.method public dismissUnableDialog()V
    .locals 1

    .prologue
    .line 3169
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3170
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mUnableDialog:Landroid/app/AlertDialog;

    .line 3173
    :cond_0
    return-void
.end method

.method public dismissViewPagerAppList()V
    .locals 0

    .prologue
    .line 4741
    return-void
.end method

.method public getFlashBarEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 677
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 678
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 680
    :cond_0
    :goto_0
    return v0

    .line 678
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getFlashBarState()Z
    .locals 1

    .prologue
    .line 670
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 671
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarState()Z

    move-result v0

    .line 673
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGuidelineWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method public getItemIndex(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3563
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getCount()I

    move-result v0

    .line 3564
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 3565
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 3566
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 3567
    const v3, 0x7f0f0059

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 3577
    .end local v1    # "i":I
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    :goto_1
    return v1

    .line 3569
    .restart local v1    # "i":I
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_1
    const v3, 0x7f0f005c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3571
    const v3, 0x7f0f005b

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3573
    const v3, 0x7f0f005a

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3564
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3577
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getRunningAppItemIndex(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3539
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getCount()I

    move-result v0

    .line 3540
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 3541
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 3542
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 3543
    const v3, 0x7f0f00e6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 3549
    .end local v1    # "i":I
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    :goto_1
    return v1

    .line 3545
    .restart local v1    # "i":I
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_1
    const v3, 0x7f0f00e5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3540
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3549
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public hideAppEditList()V
    .locals 1

    .prologue
    .line 4419
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->hideAppEditList(Z)V

    .line 4420
    return-void
.end method

.method public hideAppEditList(Z)V
    .locals 3
    .param p1, "bForceCloseAppList"    # Z

    .prologue
    .line 4432
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbAnimating:Z

    if-nez v1, :cond_0

    .line 4433
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 4434
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCloseApplist:Z

    .line 4437
    const/4 v0, 0x0

    .line 4438
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x67

    if-ne v1, v2, :cond_1

    .line 4439
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002f

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4446
    :goto_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$59;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4482
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getMainFrame()Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4483
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4485
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_0
    return-void

    .line 4440
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_2

    .line 4441
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040030

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 4443
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public hideAppEditPost()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4424
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCloseApplist:Z

    if-eqz v0, :cond_0

    .line 4425
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateFlashBarState(ZZ)Z

    .line 4430
    :goto_0
    return-void

    .line 4428
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V

    goto :goto_0
.end method

.method public hideTraybarHelpPopup()V
    .locals 1

    .prologue
    .line 1401
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsCheckBoxChecked:Z

    .line 1402
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHelpPopupWindowTraybar()V

    .line 1403
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V

    .line 1404
    return-void
.end method

.method public isPocketTrayOpen()Z
    .locals 1

    .prologue
    .line 3757
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z

    return v0
.end method

.method public makeRecentApp()V
    .locals 0

    .prologue
    .line 4619
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v5, 0x7f080002

    const v3, 0x7f080001

    const/4 v4, 0x1

    .line 2859
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    .line 2863
    :cond_0
    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 2864
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNeedUpdatePocketTrayButtonText:Z

    .line 2866
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v1, :cond_a

    .line 2867
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_1

    .line 2868
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2870
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 2871
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2873
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 2874
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2888
    :cond_3
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateText:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpText:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 2889
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080003

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2890
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080004

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2893
    :cond_4
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 2894
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2896
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 2897
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2898
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    .line 2899
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    .line 2901
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mStartFlashBar:Z

    if-eqz v1, :cond_5

    .line 2902
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V

    .line 2903
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    invoke-direct {p0, v1, v4, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V

    .line 2906
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    if-eqz v1, :cond_6

    .line 2907
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2909
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x64

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mChangedPosition:I

    .line 2910
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getFlashBarEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_8

    .line 2911
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mConfigChanged:Z

    .line 2912
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x67

    if-eq v1, v2, :cond_7

    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_8

    .line 2913
    :cond_7
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setAppListHandlePosition(I)V

    .line 2914
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    invoke-direct {p0, v1, v4, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V

    .line 2918
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_9

    .line 2919
    invoke-direct {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V

    .line 2922
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getWindowDefSize()V

    .line 2923
    return-void

    .line 2878
    .end local v0    # "fullscreen":Landroid/graphics/Point;
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_b

    .line 2879
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2881
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    if-eqz v1, :cond_c

    .line 2882
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2884
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 2885
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 404
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->releaseAppListBitmap(Ljava/util/List;)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->releaseAppListBitmap(Ljava/util/List;)V

    .line 406
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 407
    return-void
.end method

.method public pkgManagerList(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 4134
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->pkgManagerList(Landroid/content/Intent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4135
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 4136
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v0, :cond_0

    .line 4137
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 4140
    :cond_0
    return-void
.end method

.method public prepareClosing()V
    .locals 0

    .prologue
    .line 4682
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissUnableDialog()V

    .line 4683
    return-void
.end method

.method public reviveMultiWindowTray()V
    .locals 0

    .prologue
    .line 4735
    return-void
.end method

.method public rotateTraybarHelpPopup()V
    .locals 0

    .prologue
    .line 1407
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHelpPopupWindowTraybar()V

    .line 1408
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V

    .line 1409
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->showTraybarHelpPopup()V

    .line 1410
    return-void
.end method

.method public setAppEditListWindow(Landroid/view/Window;)V
    .locals 3
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 4311
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 4312
    .local v0, "more":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1, p1, v2, v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Z)V

    .line 4313
    return-void

    .line 4311
    .end local v0    # "more":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFlashBarState(Z)V
    .locals 1
    .param p1, "enableState"    # Z

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 665
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setFlashBarState(Z)V

    .line 667
    :cond_0
    return-void
.end method

.method public setGuidelineWindow(Landroid/view/Window;)V
    .locals 0
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;

    .line 509
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeGuideLineLayout()V

    .line 510
    return-void
.end method

.method public setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 5
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 477
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    .line 478
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 480
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 482
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 483
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 484
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I

    .line 485
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I

    .line 487
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    .line 489
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeFlashBarLayout(Z)V

    .line 490
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 491
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    .line 494
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 504
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateRunningAppList()V

    .line 505
    return-void
.end method

.method public showFlashBar(ZZ)V
    .locals 7
    .param p1, "show"    # Z
    .param p2, "expand"    # Z

    .prologue
    const/16 v6, 0x67

    const/16 v5, 0x8

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 685
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 686
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarState(Z)V

    .line 687
    const/4 p1, 0x0

    .line 691
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V

    .line 693
    if-eqz p1, :cond_4

    .line 694
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v2, :cond_1

    .line 695
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    invoke-virtual {v2, v3, v4, v6, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 696
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    invoke-virtual {v2, v3, v4, v6, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 698
    :cond_1
    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startFlashBarAnimation(ZZ)V

    .line 699
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 700
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v4, 0x7f080019

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 701
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 711
    :cond_2
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    .line 712
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    move v0, v1

    :cond_3
    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V

    .line 713
    return-void

    .line 704
    :cond_4
    invoke-direct {p0, v0, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startFlashBarAnimation(ZZ)V

    .line 705
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    .line 706
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v4, 0x7f08001a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 707
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public showHistoryBarDialog(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4687
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4688
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-ne v3, v7, :cond_2

    .line 4689
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsHoverType:Z

    if-eqz v3, :cond_0

    .line 4723
    :goto_0
    return-void

    .line 4693
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    .line 4694
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 4697
    :cond_1
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    if-ne v3, p1, :cond_2

    .line 4698
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    .line 4699
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I

    goto :goto_0

    .line 4704
    :cond_2
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 4705
    .local v1, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 4706
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 4707
    .local v0, "loc":[I
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 4708
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    packed-switch v3, :pswitch_data_0

    .line 4720
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 4721
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsHoverType:Z

    .line 4722
    invoke-direct {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V

    goto :goto_0

    .line 4712
    :pswitch_0
    aget v3, v0, v7

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    goto :goto_1

    .line 4716
    :pswitch_1
    aget v3, v0, v6

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I

    goto :goto_1

    .line 4708
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public showViewPagerTray(Ljava/lang/String;I)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "taskId"    # I

    .prologue
    .line 4738
    return-void
.end method

.method public updateAppListRelayout(Z)V
    .locals 3
    .param p1, "bClear"    # Z

    .prologue
    .line 4156
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mStartFlashBar:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    if-eqz v0, :cond_0

    .line 4157
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V

    .line 4158
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V

    .line 4162
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 4163
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 4165
    return-void
.end method

.method public updateFlashBarState(ZZ)Z
    .locals 4
    .param p1, "expand"    # Z
    .param p2, "pressedHelpBtn"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 518
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 519
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 520
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 529
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z

    if-ne v2, p1, :cond_2

    move v0, v1

    .line 540
    :goto_0
    return v0

    .line 522
    :cond_1
    if-ne p1, v0, :cond_0

    if-ne p2, v0, :cond_0

    .line 523
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 524
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 531
    :cond_2
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z

    if-eqz v2, :cond_3

    if-nez p2, :cond_3

    move v0, v1

    .line 532
    goto :goto_0

    .line 534
    :cond_3
    if-eqz p1, :cond_4

    .line 535
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->openFlashBar()V

    goto :goto_0

    .line 537
    :cond_4
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V

    goto :goto_0
.end method

.method public userSwitched()V
    .locals 2

    .prologue
    .line 4660
    const-string v0, "AppListGridWindow"

    const-string v1, "userSwitched"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4661
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowAppList:Landroid/view/Window;

    if-nez v0, :cond_0

    .line 4672
    :goto_0
    return-void

    .line 4666
    :cond_0
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4670
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 4671
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    goto :goto_0

    .line 4667
    :catch_0
    move-exception v0

    goto :goto_1
.end method
