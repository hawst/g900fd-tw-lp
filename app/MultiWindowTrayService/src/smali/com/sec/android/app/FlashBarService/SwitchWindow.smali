.class public Lcom/sec/android/app/FlashBarService/SwitchWindow;
.super Ljava/lang/Object;
.source "SwitchWindow.java"

# interfaces
.implements Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;
    }
.end annotation


# instance fields
.field private mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

.field private final mContext:Landroid/content/Context;

.field mHandler:Landroid/os/Handler;

.field private mHideRequested:Z

.field private mLp:Landroid/view/WindowManager$LayoutParams;

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mScreenshotLoaderTask:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Void;",
            "Ljava/lang/Void;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

.field private mViewVisible:Z

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    .line 33
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHandler:Landroid/os/Handler;

    .line 42
    new-instance v1, Lcom/sec/android/app/FlashBarService/SwitchWindow$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SwitchWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/SwitchWindow;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 51
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    .line 54
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mContext:Landroid/content/Context;

    .line 56
    new-instance v1, Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

    .line 57
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->setAnimationFinishedCallback(Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;)V

    .line 59
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 60
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mContext:Landroid/content/Context;

    const-string v2, "multiwindow_facade"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 63
    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    const/16 v2, 0x8a4

    const v3, 0x1000500

    const/4 v4, -0x3

    invoke-direct {v1, v2, v3, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(III)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mLp:Landroid/view/WindowManager$LayoutParams;

    .line 69
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mLp:Landroid/view/WindowManager$LayoutParams;

    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x12

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 71
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mLp:Landroid/view/WindowManager$LayoutParams;

    const-string v2, "SwitchWindow"

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 73
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 74
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 75
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 76
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->loadScreenshots()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHideRequested:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/FlashBarService/SwitchWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHideRequested:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/sec/android/app/FlashBarService/SwitchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mViewVisible:Z

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/FlashBarService/SwitchWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mViewVisible:Z

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Landroid/view/WindowManager$LayoutParams;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mLp:Landroid/view/WindowManager$LayoutParams;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/SwitchWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    return-object v0
.end method

.method static getDegreesForRotation(I)F
    .locals 1
    .param p0, "value"    # I

    .prologue
    .line 210
    packed-switch p0, :pswitch_data_0

    .line 218
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 212
    :pswitch_0
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    .line 214
    :pswitch_1
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 216
    :pswitch_2
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private loadScreenshots()Landroid/graphics/Bitmap;
    .locals 18

    .prologue
    .line 171
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFocusedStackLayer()I

    move-result v17

    .line 173
    .local v17, "topLayer":I
    new-instance v13, Landroid/util/DisplayMetrics;

    invoke-direct {v13}, Landroid/util/DisplayMetrics;-><init>()V

    .line 174
    .local v13, "displayMetrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v12

    .line 176
    .local v12, "display":Landroid/view/Display;
    invoke-virtual {v12, v13}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 178
    invoke-virtual {v12}, Landroid/view/Display;->getRotation()I

    move-result v1

    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->getDegreesForRotation(I)F

    move-result v10

    .line 179
    .local v10, "degrees":F
    const/4 v1, 0x0

    cmpl-float v1, v10, v1

    if-lez v1, :cond_2

    const/4 v15, 0x1

    .line 180
    .local v15, "requiresRotation":Z
    :goto_0
    const/4 v1, 0x2

    new-array v11, v1, [F

    const/4 v1, 0x0

    iget v2, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    aput v2, v11, v1

    const/4 v1, 0x1

    iget v2, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    aput v2, v11, v1

    .line 181
    .local v11, "dims":[F
    if-eqz v15, :cond_0

    .line 183
    new-instance v14, Landroid/graphics/Matrix;

    invoke-direct {v14}, Landroid/graphics/Matrix;-><init>()V

    .line 184
    .local v14, "matrix":Landroid/graphics/Matrix;
    neg-float v1, v10

    invoke-virtual {v14, v1}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 185
    invoke-virtual {v14, v11}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 186
    const/4 v1, 0x0

    const/4 v2, 0x0

    aget v2, v11, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    aput v2, v11, v1

    .line 187
    const/4 v1, 0x1

    const/4 v2, 0x1

    aget v2, v11, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    aput v2, v11, v1

    .line 190
    .end local v14    # "matrix":Landroid/graphics/Matrix;
    :cond_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    const/4 v2, 0x0

    aget v2, v11, v2

    float-to-int v2, v2

    const/4 v3, 0x1

    aget v3, v11, v3

    float-to-int v3, v3

    const/16 v4, 0x4e20

    add-int/lit8 v5, v17, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v1 .. v7}, Landroid/view/SurfaceControl;->screenshot(Landroid/graphics/Rect;IIIIZI)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 192
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v15, :cond_1

    .line 194
    iget v1, v13, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v13, Landroid/util/DisplayMetrics;->heightPixels:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v16

    .line 196
    .local v16, "ss":Landroid/graphics/Bitmap;
    new-instance v9, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v9, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 198
    .local v9, "c":Landroid/graphics/Canvas;
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {v9, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 199
    const/high16 v1, 0x43b40000    # 360.0f

    sub-float/2addr v1, v10

    invoke-virtual {v9, v1}, Landroid/graphics/Canvas;->rotate(F)V

    .line 200
    const/4 v1, 0x0

    aget v1, v11, v1

    neg-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    const/4 v2, 0x1

    aget v2, v11, v2

    neg-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    invoke-virtual {v9, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 201
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v9, v8, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 203
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    .line 204
    move-object/from16 v8, v16

    .line 206
    .end local v9    # "c":Landroid/graphics/Canvas;
    .end local v16    # "ss":Landroid/graphics/Bitmap;
    :cond_1
    return-object v8

    .line 179
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    .end local v11    # "dims":[F
    .end local v15    # "requiresRotation":Z
    :cond_2
    const/4 v15, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_1

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHideRequested:Z

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mViewVisible:Z

    if-eqz v0, :cond_0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->hide()V

    goto :goto_0
.end method

.method public onFirstAnimationBegin()V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;->onShow()V

    .line 154
    :cond_0
    return-void
.end method

.method public onLastAnimationFinished()V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/SwitchWindow$3;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SwitchWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/SwitchWindow;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 137
    return-void
.end method

.method public onLastAnimationFinishedReal()V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/SwitchWindow$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SwitchWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/SwitchWindow;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 168
    return-void
.end method

.method public onSwitch(II)V
    .locals 1
    .param p1, "app1"    # I
    .param p2, "app2"    # I

    .prologue
    .line 141
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchWindow$4;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/android/app/FlashBarService/SwitchWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/SwitchWindow;II)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 147
    return-void
.end method

.method setCallback(Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;)V
    .locals 0
    .param p1, "c"    # Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mCallback:Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;

    .line 80
    return-void
.end method

.method public setFocusedZone(I)V
    .locals 1
    .param p1, "focusedZone"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/SwitchView;->setFocusedZone(I)V

    .line 223
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    invoke-virtual {v0}, Landroid/os/AsyncTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-eq v0, v1, :cond_0

    .line 115
    :goto_0
    return-void

    .line 87
    :cond_0
    new-instance v0, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SwitchWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/SwitchWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    .line 113
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mSwitchView:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->updateActiveZones()V

    .line 114
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mScreenshotLoaderTask:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public unregisterReceiver()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 228
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 231
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchWindow;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 233
    :cond_0
    return-void

    .line 229
    :catch_0
    move-exception v0

    goto :goto_0
.end method
