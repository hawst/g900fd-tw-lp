.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1686
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x2

    const/4 v2, -0x1

    const/4 v4, 0x1

    .line 1689
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v6

    .line 1690
    .local v6, "childCnt":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v6, :cond_2

    .line 1691
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/RelativeLayout;

    .line 1692
    .local v12, "thumnail":Landroid/widget/RelativeLayout;
    invoke-virtual {v12, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1693
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    if-le v0, v2, :cond_2

    .line 1694
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v0, :cond_1

    .line 1695
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v0, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v10

    .line 1697
    .local v10, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v10, :cond_0

    invoke-virtual {v10, v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1698
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect()Landroid/graphics/Rect;

    move-result-object v11

    .line 1699
    .local v11, "rect":Landroid/graphics/Rect;
    if-eqz v11, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v11}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1700
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v11}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1701
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v1, v1, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 1705
    .end local v10    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v11    # "rect":Landroid/graphics/Rect;
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getComponentInfo(I)Landroid/content/pm/ComponentInfo;

    move-result-object v9

    .line 1706
    .local v9, "info":Landroid/content/pm/ComponentInfo;
    const-string v0, "LegacyAppListWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "App icon clicked :: name = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v9, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1707
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1708
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v5, v3, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivity(IIIZI)V

    .line 1723
    .end local v9    # "info":Landroid/content/pm/ComponentInfo;
    .end local v12    # "thumnail":Landroid/widget/RelativeLayout;
    :cond_2
    :goto_1
    return-void

    .line 1710
    .restart local v9    # "info":Landroid/content/pm/ComponentInfo;
    .restart local v12    # "thumnail":Landroid/widget/RelativeLayout;
    :cond_3
    new-instance v13, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-direct {v13, v5}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 1711
    .local v13, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    .line 1712
    .local v7, "displayFrame":Landroid/graphics/Rect;
    invoke-virtual {v13, v7}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 1713
    const/16 v0, 0x800

    invoke-virtual {v13, v0, v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 1714
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v4, v13}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->moveOnlySpecificTaskToFront(ILandroid/os/Bundle;ILcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1716
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v2, "TRAY"

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskInfos:Ljava/util/List;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/sec/android/app/FlashBarService/LoggingHelper;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1690
    .end local v7    # "displayFrame":Landroid/graphics/Rect;
    .end local v9    # "info":Landroid/content/pm/ComponentInfo;
    .end local v13    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_0
.end method
