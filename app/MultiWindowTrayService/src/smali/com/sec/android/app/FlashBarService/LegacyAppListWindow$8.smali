.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 699
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSystemUiVisibilityChange(I)V
    .locals 4
    .param p1, "visibility"    # I

    .prologue
    .line 701
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissTemplateDialog()V

    .line 702
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iput p1, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSystemUiVisibility:I

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListOverlayHelpLocation()V

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mStartFlashBar:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 707
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    .line 708
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 710
    :cond_1
    return-void
.end method
