.class public Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppListEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppListEditAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 470
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 466
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->context:Landroid/content/Context;

    .line 471
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->context:Landroid/content/Context;

    .line 472
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 473
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 488
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030004

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 490
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;-><init>()V

    .line 492
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->iconView:Landroid/widget/ImageView;

    .line 493
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    .line 494
    const v2, 0x7f0f005a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->removeIconView:Landroid/widget/ImageView;

    .line 496
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 497
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 499
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 500
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getUnableAppCnt()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 531
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    .line 504
    if-nez p2, :cond_0

    .line 505
    invoke-virtual {p0, p3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->createView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 506
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 507
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Recycled child has parent"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 509
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 510
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Recycled child has parent"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 513
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;

    .line 515
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v3

    invoke-virtual {v3, p1, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v2

    .line 516
    .local v2, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 517
    .local v1, "label":Ljava/lang/CharSequence;
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 519
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 520
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v5, :cond_2

    .line 521
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->removeIconView:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 526
    :goto_0
    return-object p2

    .line 523
    :cond_2
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$ViewHolder;->removeIconView:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
