.class public Lcom/sec/android/app/FlashBarService/FolderIconHelper;
.super Ljava/lang/Object;
.source "FolderIconHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;
    }
.end annotation


# static fields
.field private static CENTER_ALIGN_VAL:I

.field private static DEBUG:Z

.field private static final FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

.field private static final FOLDER_ICON_PAINT:Landroid/graphics/Paint;

.field private static RIGHT_ALIGN_VAL:I

.field private static mResources:Landroid/content/res/Resources;

.field private static mTmpCord:[F

.field private static sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

.field private static sDebugColors:[I

.field private static sLandAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

.field private static sPortAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 21
    sput v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->CENTER_ALIGN_VAL:I

    .line 22
    sput v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->RIGHT_ALIGN_VAL:I

    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mResources:Landroid/content/res/Resources;

    .line 34
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    .line 35
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    .line 36
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 37
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 38
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 39
    const/4 v0, 0x2

    new-array v0, v0, [F

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mTmpCord:[F

    .line 100
    sput-boolean v1, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->DEBUG:Z

    .line 101
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sDebugColors:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        -0xffff01
        -0x10000
        -0xff01
    .end array-data
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->CENTER_ALIGN_VAL:I

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->RIGHT_ALIGN_VAL:I

    return v0
.end method

.method private static createDarkenColorFilter(F)Landroid/graphics/ColorFilter;
    .locals 4
    .param p0, "mult"    # F

    .prologue
    .line 168
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    invoke-static {v3, p0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result p0

    .line 170
    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v2, p0

    float-to-int v0, v2

    .line 171
    .local v0, "alpha":I
    rsub-int v0, v0, 0xff

    .line 172
    const/16 v2, 0xff

    invoke-static {v2, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    .line 173
    .local v1, "color":I
    new-instance v2, Landroid/graphics/LightingColorFilter;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    return-object v2
.end method

.method static createFolderIconNoPlate(Landroid/graphics/Bitmap;Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "folderIcon"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->createFolderIconNoPlate(Landroid/graphics/Bitmap;Ljava/util/List;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static createFolderIconNoPlate(Landroid/graphics/Bitmap;Ljava/util/List;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "folderIcon"    # Landroid/graphics/Bitmap;
    .param p2, "hiddenItem"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sget-object v4, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 62
    .local v0, "b":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 64
    .local v1, "childCount":I
    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-virtual {v3, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 68
    const/4 v2, 0x0

    .line 70
    .local v2, "tempBitmap":Landroid/graphics/Bitmap;
    if-eqz p2, :cond_0

    .line 71
    move-object v2, p2

    .line 72
    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget-object p2, v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->EMPTY_BITMAP:Landroid/graphics/Bitmap;

    .line 75
    :cond_0
    invoke-static {p1, v1}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->drawIcons(Ljava/util/List;I)V

    .line 77
    if-eqz v2, :cond_1

    .line 78
    move-object p2, v2

    .line 80
    :cond_1
    return-object v0
.end method

.method static createFolderIconWithPlate(Landroid/graphics/Bitmap;Ljava/util/List;)Landroid/graphics/Bitmap;
    .locals 6
    .param p0, "folderIcon"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p1, "info":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v5, 0x0

    .line 84
    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->OPEN_FOLDER_BITMAP:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 87
    .local v0, "b":Landroid/graphics/Bitmap;
    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 93
    invoke-static {p0, p1}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->createFolderIconNoPlate(Landroid/graphics/Bitmap;Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 94
    .local v1, "miniIcons":Landroid/graphics/Bitmap;
    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-virtual {v2, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 95
    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v2, v1, v5, v5, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 97
    return-object v0
.end method

.method private static drawIcons(Ljava/util/List;I)V
    .locals 12
    .param p1, "childCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .local p0, "folderItem":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x4

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v1, 0x0

    .line 104
    if-nez p1, :cond_1

    .line 149
    :cond_0
    return-void

    .line 106
    :cond_1
    if-le p1, v0, :cond_2

    move p1, v0

    .line 107
    :cond_2
    add-int/lit8 v6, p1, -0x1

    .line 109
    .local v6, "configIndex":I
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->DEBUG:Z

    if-eqz v0, :cond_3

    .line 110
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    const v2, 0x4400ff00    # 515.9844f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    # getter for: Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->FOLDER_ICON_RECT:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->access$000(Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;)Landroid/graphics/Rect;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 113
    :cond_3
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_4

    .line 114
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v10, :cond_7

    .line 115
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0a0097

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_HEIGHT:I

    iput v3, v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    .line 123
    :cond_4
    :goto_0
    add-int/lit8 v8, p1, -0x1

    .local v8, "i":I
    :goto_1
    if-ltz v8, :cond_0

    .line 124
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->mIconPositions:[[[F

    aget-object v0, v0, v6

    aget-object v7, v0, v8

    .line 125
    .local v7, "cord":[F
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->save()I

    .line 127
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    aget v2, v7, v11

    aget v3, v7, v10

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 128
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v2, v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_SCALE_FACTOR:F

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v3, v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_SCALE_FACTOR:F

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->scale(FF)V

    .line 131
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v0, :cond_5

    .line 132
    int-to-float v0, v8

    const v2, 0x3dcccccd    # 0.1f

    mul-float/2addr v0, v2

    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->setDarkenPaintMultiplier(F)V

    .line 134
    :cond_5
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->DEBUG:Z

    if-eqz v0, :cond_6

    .line 135
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sDebugColors:[I

    aget v2, v2, v8

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v2, v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    int-to-float v3, v2

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v2, v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_HEIGHT:I

    int-to-float v4, v2

    sget-object v5, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 139
    :cond_6
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_8

    .line 140
    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v3, v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    sget-object v4, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v4, v4, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_HEIGHT:I

    invoke-static {v0, v3, v4, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v2, v0, v1, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 147
    :goto_2
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    .line 123
    add-int/lit8 v8, v8, -0x1

    goto :goto_1

    .line 118
    .end local v7    # "cord":[F
    .end local v8    # "i":I
    :cond_7
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0098

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    .line 119
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0a0099

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_HEIGHT:I

    goto/16 :goto_0

    .line 143
    .restart local v7    # "cord":[F
    .restart local v8    # "i":I
    :cond_8
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f0a0148

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 144
    .local v9, "shadowOfIconY":I
    sget-object v2, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_CANVAS:Landroid/graphics/Canvas;

    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v3, 0x3

    invoke-static {v0, v3, v11, v9}, Lcom/samsung/samm/spenscrap/ClipImageLibrary;->makeImageShadow(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v3, v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_WIDTH:I

    sget-object v4, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    iget v4, v4, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;->APP_ICON_HEIGHT:I

    invoke-static {v0, v3, v4, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v3, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-virtual {v2, v0, v1, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_2
.end method

.method public static initFolderResources(Landroid/content/res/Resources;)V
    .locals 2
    .param p0, "r"    # Landroid/content/res/Resources;

    .prologue
    .line 228
    sput-object p0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->mResources:Landroid/content/res/Resources;

    .line 229
    sget v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->CENTER_ALIGN_VAL:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 230
    const v0, 0x7f0a009b

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->CENTER_ALIGN_VAL:I

    .line 231
    const v0, 0x7f0a009c

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->RIGHT_ALIGN_VAL:I

    .line 234
    :cond_0
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 235
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sPortAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    if-nez v0, :cond_1

    .line 236
    new-instance v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sPortAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    .line 238
    :cond_1
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sPortAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    .line 245
    :goto_0
    return-void

    .line 240
    :cond_2
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sLandAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    if-nez v0, :cond_3

    .line 241
    new-instance v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;-><init>(Landroid/content/res/Resources;)V

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sLandAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    .line 243
    :cond_3
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sLandAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    sput-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->sCurrentAppIconData:Lcom/sec/android/app/FlashBarService/FolderIconHelper$AppIconData;

    goto :goto_0
.end method

.method private static setDarkenPaintMultiplier(F)V
    .locals 1
    .param p0, "mult"    # F

    .prologue
    .line 159
    sget-object v0, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->FOLDER_ICON_PAINT:Landroid/graphics/Paint;

    invoke-static {p0, v0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->setDarkenPaintMultiplier(FLandroid/graphics/Paint;)V

    .line 160
    return-void
.end method

.method private static setDarkenPaintMultiplier(FLandroid/graphics/Paint;)V
    .locals 1
    .param p0, "mult"    # F
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 163
    invoke-static {p0}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->createDarkenColorFilter(F)Landroid/graphics/ColorFilter;

    move-result-object v0

    .line 164
    .local v0, "filter":Landroid/graphics/ColorFilter;
    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 165
    return-void
.end method
