.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1186
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v6, 0x1f4

    const/16 v5, 0xd1

    const/16 v2, 0xd0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1189
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1221
    :cond_0
    :goto_0
    :pswitch_0
    return v3

    .line 1191
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1192
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto :goto_0

    .line 1195
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerScrollHoverMargin:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 1196
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setHoverIcon(I)V

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1199
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1200
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1202
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMark:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerScrollHoverMargin:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 1203
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setHoverIcon(I)V

    .line 1204
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1205
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1206
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1207
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1210
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setHoverIcon(I)V

    goto :goto_0

    .line 1214
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveLeft:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1215
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mViewPagerMoveRight:Z
    invoke-static {v0, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1216
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1217
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1218
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$19;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setHoverIcon(I)V

    goto/16 :goto_0

    .line 1189
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
