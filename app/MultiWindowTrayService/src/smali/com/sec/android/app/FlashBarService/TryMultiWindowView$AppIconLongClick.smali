.class final Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;
.super Ljava/lang/Object;
.source "TryMultiWindowView.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AppIconLongClick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V
    .locals 0

    .prologue
    .line 519
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Lcom/sec/android/app/FlashBarService/TryMultiWindowView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowView$1;

    .prologue
    .line 519
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 522
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 523
    .local v2, "tag":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppTag1:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 524
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$202(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 525
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z
    invoke-static {v3, v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$302(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 526
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z
    invoke-static {v3, v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$402(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 527
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 534
    :goto_0
    const-string v3, ""

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 535
    .local v0, "data":Landroid/content/ClipData;
    new-instance v1, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v1, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 536
    .local v1, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v0, v1, p1, v6}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 537
    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 539
    return v7

    .line 529
    .end local v0    # "data":Landroid/content/ClipData;
    .end local v1    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z
    invoke-static {v3, v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$202(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 530
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$302(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 531
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)[B

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    goto :goto_0
.end method
