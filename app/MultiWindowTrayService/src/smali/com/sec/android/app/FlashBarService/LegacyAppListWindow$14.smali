.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$14;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createViewPagerTray()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 990
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotion(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0x9

    const/4 v4, 0x0

    .line 993
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_4

    .line 998
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_2

    .line 999
    const/4 v1, 0x0

    .line 1000
    .local v1, "vscroll":F
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .line 1005
    .local v0, "hscroll":F
    :goto_0
    cmpl-float v2, v0, v4

    if-nez v2, :cond_0

    cmpl-float v2, v1, v4

    if-eqz v2, :cond_4

    .line 1006
    :cond_0
    cmpl-float v2, v0, v4

    if-gtz v2, :cond_1

    cmpl-float v2, v1, v4

    if-lez v2, :cond_3

    .line 1007
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->moveViewPagerRight()V

    .line 1011
    :goto_1
    const/4 v2, 0x1

    .line 1014
    .end local v0    # "hscroll":F
    .end local v1    # "vscroll":F
    :goto_2
    return v2

    .line 1002
    :cond_2
    invoke-virtual {p2, v5}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    neg-float v1, v2

    .line 1003
    .restart local v1    # "vscroll":F
    const/16 v2, 0xa

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    .restart local v0    # "hscroll":F
    goto :goto_0

    .line 1009
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->moveViewPagerLeft()V

    goto :goto_1

    .line 1014
    .end local v0    # "hscroll":F
    .end local v1    # "vscroll":F
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method
