.class Lcom/sec/android/app/FlashBarService/SmartWindow$10;
.super Landroid/os/Handler;
.source "SmartWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V
    .locals 0

    .prologue
    .line 983
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 985
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_1

    .line 986
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mScrollDirection:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3300(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 996
    :goto_0
    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3400()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 997
    # -= operator for: Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3420(I)I

    .line 999
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3400()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1001
    :cond_1
    return-void

    .line 988
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/widget/ScrollView;

    move-result-object v0

    const/16 v1, -0xa

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_0

    .line 991
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$10;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/widget/ScrollView;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_0

    .line 986
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
