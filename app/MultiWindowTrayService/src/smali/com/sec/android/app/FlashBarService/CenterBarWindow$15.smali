.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1800
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0xcc

    .line 1802
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1803
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->cancelHideButtonTimer()V

    .line 1804
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1805
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 1809
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setCenterBarPoint(ILandroid/graphics/Point;)V

    .line 1810
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 1811
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1812
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1813
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 1815
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-static {v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1816
    return-void

    .line 1807
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Point;->x:I

    goto :goto_0
.end method
