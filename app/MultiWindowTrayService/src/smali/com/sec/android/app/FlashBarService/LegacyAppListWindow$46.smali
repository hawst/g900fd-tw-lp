.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 3622
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v5, 0x0

    .line 3625
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 3626
    .local v2, "title":Ljava/lang/String;
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    const/4 v6, -0x2

    if-ne v4, v6, :cond_3

    :cond_1
    const/4 v3, 0x1

    .line 3629
    .local v3, "titleChanged":Z
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResumedInfos:Ljava/util/List;

    invoke-virtual {v6, v7, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addTemplate(Ljava/util/List;Ljava/lang/String;Z)I

    move-result v6

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v4, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 3630
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTemplateIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    if-gez v4, :cond_4

    .line 3660
    :goto_2
    return-void

    .line 3625
    .end local v2    # "title":Ljava/lang/String;
    .end local v3    # "titleChanged":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .restart local v2    # "title":Ljava/lang/String;
    :cond_3
    move v3, v5

    .line 3626
    goto :goto_1

    .line 3633
    .restart local v3    # "titleChanged":Z
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->prepareAnimationIcon()V
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 3634
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v6, v5}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v6

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v4, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/Point;)Landroid/graphics/Point;

    .line 3636
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3637
    .local v0, "animationIcon1Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3639
    .local v1, "animationIcon2Margin":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->isPortrait()Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 3640
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v7

    iget v7, v7, Landroid/graphics/Point;->y:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v0, v4, v6, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3641
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Point;->y:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v1, v4, v6, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3654
    :goto_3
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon1:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v4

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3655
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAnimationIcon2:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v4

    new-instance v5, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v5, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3657
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationTemplateStart()V
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 3658
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissTemplateDialog()V

    .line 3659
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->openFlashBar()V
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_2

    .line 3643
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_3

    .line 3645
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    add-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v0, v4, v6, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3646
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    add-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v1, v4, v6, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_3

    .line 3649
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v0, v4, v6, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3650
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    add-int/2addr v4, v6

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mOffsetForAnim:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$9900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    add-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$46;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v1, v4, v6, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto/16 :goto_3

    .line 3643
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
