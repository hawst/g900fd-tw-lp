.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateIconAppear()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

.field final synthetic val$animationIcon:Landroid/widget/ImageView;

.field final synthetic val$count:I

.field final synthetic val$templateSize:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/ImageView;II)V
    .locals 0

    .prologue
    .line 3695
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->val$animationIcon:Landroid/widget/ImageView;

    iput p3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->val$count:I

    iput p4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->val$templateSize:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3699
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->val$animationIcon:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3700
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->val$count:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->val$templateSize:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 3702
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateIconMove()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$9300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 3704
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3696
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3697
    return-void
.end method
