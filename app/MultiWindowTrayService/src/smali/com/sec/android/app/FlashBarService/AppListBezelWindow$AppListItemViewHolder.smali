.class final Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "AppListItemViewHolder"
.end annotation


# instance fields
.field badgeIconView:Landroid/widget/ImageView;

.field contentView:Landroid/widget/LinearLayout;

.field iconView:Landroid/widget/ImageView;

.field labelView:Landroid/widget/TextView;

.field removeIconView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "appClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    return-void
.end method

.method public setOnDragListener(Landroid/view/View$OnDragListener;)V
    .locals 1
    .param p1, "appListItemDragListener"    # Landroid/view/View$OnDragListener;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 258
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 1
    .param p1, "appTouchListener"    # Landroid/view/View$OnTouchListener;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 248
    return-void
.end method
