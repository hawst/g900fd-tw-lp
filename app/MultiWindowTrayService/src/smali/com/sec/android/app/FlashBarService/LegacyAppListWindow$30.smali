.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

.field final synthetic val$sendIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1979
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->val$sendIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v1, 0x8

    .line 1989
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1990
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1991
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1992
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1993
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailNew()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 1994
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1986
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1982
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMakeInstanceAniRunning:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 1983
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$30;->val$sendIntent:Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1984
    return-void
.end method
