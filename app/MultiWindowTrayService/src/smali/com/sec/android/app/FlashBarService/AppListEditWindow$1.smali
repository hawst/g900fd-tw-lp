.class Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;
.super Ljava/lang/Object;
.source "AppListEditWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListEditWindow;->makeAppListEditLayout(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 199
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->hideAppEditList()V

    .line 200
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 201
    const-string v3, "market://search?q=multiwindow"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 202
    .local v2, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v1, v3, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 203
    .local v1, "goToMarket":Landroid/content/Intent;
    const v3, 0x30008000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 205
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$100(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :goto_0
    return-void

    .line 206
    :catch_0
    move-exception v0

    .line 207
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$100(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x104047a

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
