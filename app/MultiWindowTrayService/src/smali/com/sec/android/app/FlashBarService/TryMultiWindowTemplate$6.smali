.class Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;
.super Ljava/lang/Object;
.source "TryMultiWindowTemplate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 399
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup3Parent:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 401
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 402
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppPaired:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->drs:Ljava/util/List;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;Ljava/util/List;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 403
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppPaired:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer2:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 406
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->trytemplateAnimationTemplate()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTemplateBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    return-void
.end method
