.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

.field final synthetic val$ani:Landroid/view/animation/Animation;

.field final synthetic val$expand:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZLandroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2761
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->val$expand:Z

    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->val$ani:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 2764
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 2765
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2768
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->val$expand:Z

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2772
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->val$ani:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2782
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMainFrame:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->val$ani:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2783
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$21;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 2784
    return-void

    .line 2769
    :catch_0
    move-exception v0

    .line 2770
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AppListGridWindow"

    const-string v2, "Exception in mWindowAppList"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
