.class public Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.super Landroid/app/SallyService;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;
    }
.end annotation


# static fields
.field public static GUI_RECENT_UI:Z

.field public static GUI_SCALE_PREVIEW:Z

.field public static GUI_TRAYBAR_UI:Z

.field private static mIsMlActivated:Z


# instance fields
.field private final DEBUG:Z

.field final DUMMY_NOTIFICATION_ID:I

.field mAirButtonStateReceiver:Landroid/content/BroadcastReceiver;

.field private mAlarmStateListener:Landroid/content/BroadcastReceiver;

.field private mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

.field mBackKeyReceiver:Landroid/content/BroadcastReceiver;

.field mCenterBarFocusDisplayReceiver:Landroid/content/BroadcastReceiver;

.field private mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

.field private mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

.field private mCropedRectData:Landroid/graphics/RectF;

.field private mFocusZoneInfo:I

.field final mForegroundToken:Landroid/os/IBinder;

.field private mHandler:Landroid/os/Handler;

.field mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

.field private mIsFactoryMode:Z

.field mIsForeground:Z

.field mKeyguardUnlockReceiver:Landroid/content/BroadcastReceiver;

.field mKnoxSettingProperty:Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;

.field mKnoxSetupCompleteReceiver:Landroid/content/BroadcastReceiver;

.field mMlReceiver:Landroid/content/BroadcastReceiver;

.field mMultiUserReceiver:Landroid/content/BroadcastReceiver;

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field mMultiWindowServiceCallback:Landroid/sec/multiwindow/impl/IMultiWindowServiceCallback$Stub;

.field mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field private mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

.field private mPhone:Landroid/telephony/TelephonyManager;

.field private mPhone2:Landroid/telephony/TelephonyManager;

.field mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

.field private mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

.field private mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

.field mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

.field private mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

.field private mRemoveViewWindowAppList:Z

.field private mRemoveViewWindowCenterBar:Z

.field private mRemoveViewWindowGuideline:Z

.field mSealedReceiver:Landroid/content/BroadcastReceiver;

.field private mShowCenterbarButton:Z

.field private mShowGestureOverlayHelp:Z

.field private mShowViewPager:Z

.field private mShowViewPagerIntent:Landroid/content/Intent;

.field private mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

.field mStatusBarAnimatingReceiver:Landroid/content/BroadcastReceiver;

.field mStatusBarExpandReceiver:Landroid/content/BroadcastReceiver;

.field mStopDragModeReceiver:Landroid/content/BroadcastReceiver;

.field private mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

.field private mSwitchWindowActive:Z

.field mThemeChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mWindowAppList:Landroid/view/Window;

.field private mWindowCenterBar:Landroid/view/Window;

.field private mWindowDim:Landroid/view/Window;

.field private mWindowEditList:Landroid/view/Window;

.field private mWindowExtraCenterBar:Landroid/view/Window;

.field private mWindowGlow:Landroid/view/Window;

.field private mWindowGuideline:Landroid/view/Window;

.field private mWindowPopupView:Landroid/view/Window;

.field private mWindowPopupViewEdit:Landroid/view/Window;

.field private mWindowRecentGuideLine:Landroid/view/Window;

.field private mWindowRecents:Landroid/view/Window;

.field private mWindowSmart:Landroid/view/Window;

.field private mWindowSmartEdit:Landroid/view/Window;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 109
    sput-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsMlActivated:Z

    .line 134
    sput-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    .line 135
    sput-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    .line 136
    sput-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_TRAYBAR_UI:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/SallyService;-><init>()V

    .line 67
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->DEBUG:Z

    .line 79
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .line 80
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .line 81
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    .line 82
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .line 83
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .line 84
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    .line 85
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 88
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    .line 89
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    .line 91
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowExtraCenterBar:Landroid/view/Window;

    .line 92
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    .line 93
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    .line 94
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    .line 95
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    .line 96
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    .line 97
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupViewEdit:Landroid/view/Window;

    .line 98
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    .line 99
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone2:Landroid/telephony/TelephonyManager;

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    .line 106
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsFactoryMode:Z

    .line 107
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHandler:Landroid/os/Handler;

    .line 108
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCropedRectData:Landroid/graphics/RectF;

    .line 120
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSettingProperty:Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;

    .line 127
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowViewPager:Z

    .line 128
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowGestureOverlayHelp:Z

    .line 131
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mForegroundToken:Landroid/os/IBinder;

    .line 139
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowAppList:Z

    .line 140
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z

    .line 141
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z

    .line 143
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowCenterbarButton:Z

    .line 147
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 189
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$2;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    .line 207
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$3;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mBackKeyReceiver:Landroid/content/BroadcastReceiver;

    .line 218
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$4;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarFocusDisplayReceiver:Landroid/content/BroadcastReceiver;

    .line 236
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$5;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 256
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$6;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMlReceiver:Landroid/content/BroadcastReceiver;

    .line 278
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSealedReceiver:Landroid/content/BroadcastReceiver;

    .line 306
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$8;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAirButtonStateReceiver:Landroid/content/BroadcastReceiver;

    .line 318
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$9;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarExpandReceiver:Landroid/content/BroadcastReceiver;

    .line 337
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$10;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarAnimatingReceiver:Landroid/content/BroadcastReceiver;

    .line 356
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKeyguardUnlockReceiver:Landroid/content/BroadcastReceiver;

    .line 365
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$12;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiUserReceiver:Landroid/content/BroadcastReceiver;

    .line 375
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$13;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStopDragModeReceiver:Landroid/content/BroadcastReceiver;

    .line 389
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$14;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mThemeChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 403
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$15;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSetupCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 421
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$16;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$16;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    .line 711
    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->DUMMY_NOTIFICATION_ID:I

    .line 712
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsForeground:Z

    .line 1518
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$24;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowServiceCallback:Landroid/sec/multiwindow/impl/IMultiWindowServiceCallback$Stub;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SmartWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindowActive:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowAppList:Z

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowAppList:Z

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z

    return p1
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Landroid/view/Window;)Landroid/view/Window;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Landroid/view/Window;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowCenterbarButton:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowCenterbarButton:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Landroid/view/Window;)Landroid/view/Window;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Landroid/view/Window;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeCenterBarWindow(I)V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowViewPagerIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mFocusZoneInfo:I

    return v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mFocusZoneInfo:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SwitchWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Landroid/view/Window;)Landroid/view/Window;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p1, "x1"    # Landroid/view/Window;

    .prologue
    .line 63
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    return-object p1
.end method

.method static synthetic access$902(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 63
    sput-boolean p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsMlActivated:Z

    return p0
.end method

.method private initAlarmEventListener()V
    .locals 2

    .prologue
    .line 1394
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1395
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1396
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT_FROM_ALARM"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1398
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    .line 1419
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1421
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private initCloseSystemDialogListener()V
    .locals 2

    .prologue
    .line 1424
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    if-nez v1, :cond_0

    .line 1425
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1426
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1428
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$23;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$23;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    .line 1440
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1442
    .end local v0    # "filter":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private initTelephonyEventListener()V
    .locals 4

    .prologue
    .line 1380
    const-string v1, "phone"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 1383
    :try_start_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    .line 1384
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 1385
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone2:Landroid/telephony/TelephonyManager;

    if-eqz v1, :cond_0

    .line 1386
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone2:Landroid/telephony/TelephonyManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    const/16 v3, 0x20

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1391
    :cond_0
    :goto_0
    return-void

    .line 1388
    :catch_0
    move-exception v0

    .line 1389
    .local v0, "e":Ljava/lang/SecurityException;
    const-string v1, "MultiWindowTrayService"

    const-string v2, "initTelephonyEventListener : Doesn\'t have the permission READ_PHONE_STATE."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private isCompleteUserSetup()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 989
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "user_setup_complete"

    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFactoryMode()Z
    .locals 5

    .prologue
    .line 1310
    const/4 v0, 0x0

    .line 1312
    .local v0, "isFactoryMode":Z
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "phone"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 1314
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-nez v0, :cond_0

    .line 1315
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "SHOULD_SHUT_DOWN"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1316
    const-string v2, "MultiWindowTrayService"

    const-string v3, "Factory mode is enabled by Case #1"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1317
    const/4 v0, 0x1

    .line 1322
    :cond_0
    if-nez v0, :cond_1

    .line 1323
    const-string v2, "999999999999999"

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1324
    const-string v2, "MultiWindowTrayService"

    const-string v3, "Factory mode is enabled by Case #2"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1325
    const/4 v0, 0x1

    .line 1330
    :cond_1
    if-nez v0, :cond_2

    .line 1331
    invoke-static {}, Landroid/os/FactoryTest;->isFactoryMode()Z

    move-result v0

    .line 1332
    const-string v2, "MultiWindowTrayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Factory mode is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1333
    if-eqz v0, :cond_2

    .line 1334
    const-string v2, "MultiWindowTrayService"

    const-string v3, "Factory mode is enabled by Case #3"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1338
    :cond_2
    return v0
.end method

.method private isKidsModeRunning()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 994
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "kids_home_mode"

    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isMlActivated()Z
    .locals 1

    .prologue
    .line 1301
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsMlActivated:Z

    return v0
.end method

.method private isOtaRegistrationMode()Z
    .locals 1

    .prologue
    .line 1272
    const/4 v0, 0x0

    return v0
.end method

.method public static isUsaFeature()Z
    .locals 2

    .prologue
    .line 984
    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 985
    .local v0, "countryCode":Ljava/lang/String;
    const-string v1, "USA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isVzwSetupRunning()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 976
    const-string v2, "ro.csc.sales_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 977
    .local v0, "SalesCode":Ljava/lang/String;
    const-string v2, "VZW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 978
    const-string v2, "persist.sys.vzw_setup_running"

    invoke-static {v2, v1}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 980
    :cond_0
    return v1
.end method

.method private makeAppListWindow()V
    .locals 2

    .prologue
    .line 1012
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    .line 1013
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1015
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$19;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$19;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1030
    :cond_0
    return-void
.end method

.method private makeCenterBarWindow(I)V
    .locals 6
    .param p1, "arrageMode"    # I

    .prologue
    const/4 v5, 0x0

    .line 1033
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1034
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03000d

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1035
    .local v0, "centerBarView":Landroid/view/View;
    const/16 v2, 0x898

    .line 1037
    .local v2, "windowType":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-nez v3, :cond_0

    .line 1038
    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .line 1041
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    if-nez v3, :cond_1

    .line 1042
    invoke-virtual {p0, v0, v5, v5, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    .line 1043
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setView(Landroid/view/View;)V

    .line 1044
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setAppListWindow(Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;)V

    .line 1045
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setWindow(Landroid/view/Window;I)V

    .line 1048
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1049
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1069
    :cond_2
    return-void
.end method

.method private makeGuidelineWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1072
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1073
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030015

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1074
    .local v0, "guidelineView":Landroid/view/View;
    const/16 v2, 0x89b

    .line 1075
    .local v2, "windowType":I
    invoke-virtual {p0, v0, v5, v5, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    .line 1076
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    if-eqz v3, :cond_0

    .line 1077
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setGuidelineWindow(Landroid/view/Window;)V

    .line 1080
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1081
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1095
    :cond_1
    return-void
.end method

.method private unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V
    .locals 1
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 555
    if-eqz p1, :cond_0

    .line 557
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 561
    :cond_0
    :goto_0
    return-void

    .line 558
    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public LaunchAppToSideSync(I)V
    .locals 1
    .param p1, "displayId"    # I

    .prologue
    .line 1576
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$26;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$26;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;I)V

    invoke-static {v0}, Landroid/os/AsyncTask;->execute(Ljava/lang/Runnable;)V

    .line 1590
    return-void
.end method

.method public canShow()Z
    .locals 1

    .prologue
    .line 1276
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isMultiWindowSettingEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isMlActivated()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1277
    :cond_0
    const/4 v0, 0x0

    .line 1280
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public forceSetMWTrayOpenState(Z)V
    .locals 4
    .param p1, "open"    # Z

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "multi_window_expanded"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1658
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v0, p1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setMultiWindowTrayOpenState(Z)V

    .line 1659
    return-void

    .line 1655
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCenterBarPoint()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 1641
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1642
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getCenterBarPoint()Landroid/graphics/Point;

    move-result-object v0

    .line 1645
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDimWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    return-object v0
.end method

.method public getFocusedZoneInfo()I
    .locals 1

    .prologue
    .line 1483
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1484
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getFocusedZoneInfo()I

    move-result v0

    .line 1486
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideDragAndDropHelpDialog()V
    .locals 2

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1611
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideDragAndDropHelpDialog()V

    .line 1612
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->removeDragAndDrop()V

    .line 1613
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1615
    :cond_0
    return-void
.end method

.method public hideSwitchWindow()V
    .locals 1

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    if-eqz v0, :cond_0

    .line 1619
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->hide()V

    .line 1621
    :cond_0
    return-void
.end method

.method public initCenterBarButtonAnim()V
    .locals 1

    .prologue
    .line 1599
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1600
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1602
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->initCenterBarButtonAnim()V

    .line 1607
    :cond_0
    :goto_0
    return-void

    .line 1604
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowCenterbarButton:Z

    goto :goto_0
.end method

.method public initCenterBarIfNeed()V
    .locals 1

    .prologue
    .line 1593
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1594
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->initCenterBarIfNeed()V

    .line 1596
    :cond_0
    return-void
.end method

.method public isKnoxMode()Z
    .locals 2

    .prologue
    .line 1662
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    const/16 v1, 0x64

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isMultiWindowSettingEnabled()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1284
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "multi_window_enabled"

    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 1285
    .local v0, "enable":Z
    :goto_0
    const-string v1, "MultiWindowTrayService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MultiWindow enabled "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1286
    return v0

    .line 1284
    .end local v0    # "enable":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRecentsWindowShowing()Z
    .locals 1

    .prologue
    .line 1476
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    if-eqz v0, :cond_0

    .line 1477
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->isWindowShowing()Z

    move-result v0

    .line 1479
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSideSyncConnected(I)Z
    .locals 6
    .param p1, "inputDisplayId"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1491
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "sidesync_source_connect"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 1492
    .local v1, "sideSyncState":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "KMS_SERVICE_CONNECTED"

    invoke-static {v4, v5, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1494
    .local v0, "kmsState":I
    if-eq v1, v3, :cond_0

    const/4 v4, 0x2

    if-eq v1, v4, :cond_0

    if-ne v0, v3, :cond_1

    .line 1497
    :cond_0
    if-nez p1, :cond_1

    move v2, v3

    .line 1502
    :cond_1
    return v2
.end method

.method public isSwitchWindowActive()Z
    .locals 1

    .prologue
    .line 1631
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindowActive:Z

    return v0
.end method

.method public makeAppListEditWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1098
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1099
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030003

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1101
    .local v0, "appEditView":Landroid/view/View;
    const/16 v2, 0x89a

    .line 1102
    .local v2, "windowType":I
    invoke-virtual {p0, v0, v5, v5, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    .line 1103
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setAppEditListWindow(Landroid/view/Window;)V

    .line 1104
    return-void
.end method

.method public makePopupViewWindow(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "displayRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v5, 0x0

    .line 1172
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1173
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f03001f

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1175
    .local v1, "popupViewWindow":Landroid/view/View;
    const/16 v2, 0x7d2

    .line 1177
    .local v2, "windowType":I
    invoke-virtual {p0, v1, v5, v5, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    .line 1178
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    invoke-virtual {v3, v4, p1, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setPopupViewWindow(Landroid/view/Window;Landroid/graphics/Rect;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    .line 1179
    return-void
.end method

.method public makeRecentApp()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1445
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    if-eqz v0, :cond_0

    .line 1446
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    .line 1447
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->makeRecentApp()V

    .line 1449
    :cond_0
    return-void
.end method

.method public makeRecentGuideLineWindow(Lcom/sec/android/app/FlashBarService/AppListController;)V
    .locals 6
    .param p1, "appListController"    # Lcom/sec/android/app/FlashBarService/AppListController;

    .prologue
    const/4 v5, 0x0

    .line 1182
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    if-nez v3, :cond_0

    .line 1183
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1184
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030021

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1186
    .local v1, "recentGuideLine":Landroid/view/View;
    const/16 v2, 0x7e0

    .line 1187
    .local v2, "windowType":I
    invoke-virtual {p0, v1, v5, v5, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    .line 1188
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    invoke-virtual {v3, v4, p1}, Lcom/sec/android/app/FlashBarService/GuideLine;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/AppListController;)V

    .line 1190
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    .end local v1    # "recentGuideLine":Landroid/view/View;
    .end local v2    # "windowType":I
    :cond_0
    return-void
.end method

.method public makeRecentsWindow()V
    .locals 1

    .prologue
    .line 1452
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    if-eqz v0, :cond_1

    .line 1453
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    if-nez v0, :cond_0

    .line 1454
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->getWindow()Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    .line 1456
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    if-eqz v0, :cond_1

    .line 1457
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->openRecentsPanel()V

    .line 1462
    :goto_0
    return-void

    .line 1461
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    goto :goto_0
.end method

.method public makeSmartEditWindow()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1113
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1114
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030026

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1116
    .local v2, "smartEditView":Landroid/view/View;
    const/16 v3, 0x89a

    .line 1117
    .local v3, "windowType":I
    invoke-virtual {p0, v2, v6, v6, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    .line 1118
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1119
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 1120
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    invoke-virtual {v4, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1121
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->setSmartEditWindow(Landroid/view/Window;)V

    .line 1122
    return-void
.end method

.method public makeSmartWindow(Landroid/graphics/Rect;)V
    .locals 10
    .param p1, "displayRect"    # Landroid/graphics/Rect;

    .prologue
    const/4 v9, 0x0

    const/4 v2, -0x1

    .line 1153
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 1154
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f030025

    const/4 v3, 0x0

    invoke-virtual {v6, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 1156
    .local v8, "smartWindowView":Landroid/view/View;
    const/16 v4, 0x7d2

    .line 1158
    .local v4, "windowType":I
    new-instance v1, Landroid/view/View;

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1159
    .local v1, "dimView":Landroid/view/View;
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1160
    const/16 v5, 0x1a

    move-object v0, p0

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;IIII)Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    .line 1163
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 1164
    .local v7, "l":Landroid/view/WindowManager$LayoutParams;
    const v0, 0x3f19999a    # 0.6f

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    .line 1165
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v2, v7}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1167
    invoke-virtual {p0, v8, v9, v9, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    .line 1168
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    invoke-virtual {v0, v2, p1, p0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->setSmartWindow(Landroid/view/Window;Landroid/graphics/Rect;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    .line 1169
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1666
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowViewPager:Z

    if-eqz v0, :cond_0

    .line 1667
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$27;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$27;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1673
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowViewPager:Z

    .line 1675
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowGestureOverlayHelp:Z

    if-eqz v0, :cond_1

    .line 1676
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$28;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$28;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1682
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowGestureOverlayHelp:Z

    .line 1684
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 999
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1001
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    if-eqz v0, :cond_1

    .line 1002
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1003
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    if-eqz v0, :cond_2

    .line 1004
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1005
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    if-eqz v0, :cond_3

    .line 1006
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1007
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    if-eqz v0, :cond_4

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1009
    :cond_4
    return-void
.end method

.method public onCreate()V
    .locals 71

    .prologue
    .line 438
    invoke-super/range {p0 .. p0}, Landroid/app/SallyService;->onCreate()V

    .line 439
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 441
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isFactoryMode()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsFactoryMode:Z

    .line 442
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsFactoryMode:Z

    if-eqz v2, :cond_0

    .line 552
    :goto_0
    return-void

    .line 446
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f090000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    .line 447
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportRecentUI(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    .line 448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportTrayBarUI(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_TRAYBAR_UI:Z

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    const-string v3, "multiwindow_facade"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 451
    new-instance v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 452
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    invoke-static {v2, v0, v3, v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->create(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;ILcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .line 453
    new-instance v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .line 454
    new-instance v2, Lcom/sec/android/app/FlashBarService/SmartWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0, v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    .line 455
    new-instance v2, Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0, v4, v6}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Lcom/sec/android/app/FlashBarService/AppListController;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .line 456
    new-instance v2, Lcom/sec/android/app/FlashBarService/RecentsWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-direct {v2, v3, v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .line 457
    new-instance v2, Lcom/sec/android/app/FlashBarService/GuideLine;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/FlashBarService/GuideLine;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    .line 459
    const/16 v70, 0x899

    .line 461
    .local v70, "windowType":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v68

    .line 462
    .local v68, "l":Landroid/view/WindowManager$LayoutParams;
    move/from16 v0, v70

    move-object/from16 v1, v68

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 463
    move-object/from16 v0, v68

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v2, v2, 0x8

    move-object/from16 v0, v68

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 464
    move-object/from16 v0, v68

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    or-int/lit8 v2, v2, 0x40

    move-object/from16 v0, v68

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 465
    move-object/from16 v0, v68

    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x4

    move-object/from16 v0, v68

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 466
    const/4 v2, 0x1

    move-object/from16 v0, v68

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 467
    const/4 v2, 0x1

    move-object/from16 v0, v68

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 468
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MultiWindowTrayService/AppListWindow "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v68

    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 469
    const/16 v2, 0x33

    move-object/from16 v0, v68

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 470
    const/4 v2, 0x1

    move-object/from16 v0, v68

    iput-boolean v2, v0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    .line 472
    const v2, 0x7f030002

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->setContentView(I)V

    .line 474
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initTelephonyEventListener()V

    .line 475
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initAlarmEventListener()V

    .line 476
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initCloseSystemDialogListener()V

    .line 478
    new-instance v5, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v5, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 479
    .local v5, "pkgMngFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 480
    const-string v2, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 481
    const-string v2, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 482
    const-string v2, "package"

    invoke-virtual {v5, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 483
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 486
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 487
    .local v9, "sdFilter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v9, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 488
    const-string v2, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v9, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 489
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    sget-object v8, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v6, p0

    invoke-virtual/range {v6 .. v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 491
    new-instance v13, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.intent.action.HOME_RESUME"

    invoke-direct {v13, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 492
    .local v13, "homeResumeReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    sget-object v12, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object/from16 v10, p0

    invoke-virtual/range {v10 .. v15}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 494
    new-instance v17, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.intent.action.BACK_KEY"

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 495
    .local v17, "backKeyReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mBackKeyReceiver:Landroid/content/BroadcastReceiver;

    sget-object v16, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object/from16 v14, p0

    invoke-virtual/range {v14 .. v19}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 497
    new-instance v21, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.action.NOTIFY_FOCUS_WINDOWS"

    move-object/from16 v0, v21

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 498
    .local v21, "centerBarfocusFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarFocusDisplayReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v19, v0

    sget-object v20, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v18, p0

    invoke-virtual/range {v18 .. v23}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 500
    new-instance v25, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    move-object/from16 v0, v25

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 501
    .local v25, "multiwindowStatusFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v23, v0

    sget-object v24, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v22, p0

    invoke-virtual/range {v22 .. v27}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 503
    new-instance v29, Landroid/content/IntentFilter;

    const-string v2, "com.samsung.android.mirrorlink.ML_STATE"

    move-object/from16 v0, v29

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 504
    .local v29, "mlIntentFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMlReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v27, v0

    sget-object v28, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v30, 0x0

    const/16 v31, 0x0

    move-object/from16 v26, p0

    invoke-virtual/range {v26 .. v31}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 507
    new-instance v69, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.multiwindow.SEALED_MODE"

    move-object/from16 v0, v69

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 508
    .local v69, "sealedIntentFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSealedReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, v69

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 509
    new-instance v2, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSettingProperty:Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;

    .line 512
    new-instance v33, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.intent.action.AIR_BUTTON"

    move-object/from16 v0, v33

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 513
    .local v33, "airButtonIntentFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAirButtonStateReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v31, v0

    sget-object v32, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v34, 0x0

    const/16 v35, 0x0

    move-object/from16 v30, p0

    invoke-virtual/range {v30 .. v35}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 515
    new-instance v37, Landroid/content/IntentFilter;

    const-string v2, "com.android.systemui.statusbar.EXPANDED"

    move-object/from16 v0, v37

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 516
    .local v37, "statusBarExpandReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarExpandReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v35, v0

    sget-object v36, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v34, p0

    invoke-virtual/range {v34 .. v39}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 518
    new-instance v41, Landroid/content/IntentFilter;

    const-string v2, "com.android.systemui.statusbar.ANIMATING"

    move-object/from16 v0, v41

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 519
    .local v41, "statusBarAnimatingReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarAnimatingReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v39, v0

    sget-object v40, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v42, 0x0

    const/16 v43, 0x0

    move-object/from16 v38, p0

    invoke-virtual/range {v38 .. v43}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 521
    new-instance v45, Landroid/content/IntentFilter;

    const-string v2, "com.android.internal.policy.impl.Keyguard.Unlock"

    move-object/from16 v0, v45

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 522
    .local v45, "keyguardUnlockReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKeyguardUnlockReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v43, v0

    sget-object v44, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v46, 0x0

    const/16 v47, 0x0

    move-object/from16 v42, p0

    invoke-virtual/range {v42 .. v47}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 524
    new-instance v49, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.USER_SWITCHED"

    move-object/from16 v0, v49

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 525
    .local v49, "multiUserReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiUserReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v47, v0

    sget-object v48, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v50, 0x0

    const/16 v51, 0x0

    move-object/from16 v46, p0

    invoke-virtual/range {v46 .. v51}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 527
    new-instance v53, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.action.NOTIFY_STOP_DRAG_MODE"

    move-object/from16 v0, v53

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 528
    .local v53, "stopDragModeReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStopDragModeReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v51, v0

    sget-object v52, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v54, 0x0

    const/16 v55, 0x0

    move-object/from16 v50, p0

    invoke-virtual/range {v50 .. v55}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 530
    new-instance v57, Landroid/content/IntentFilter;

    const-string v2, "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

    move-object/from16 v0, v57

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 531
    .local v57, "themeChangedReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mThemeChangedReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v55, v0

    sget-object v56, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v58, 0x0

    const/16 v59, 0x0

    move-object/from16 v54, p0

    invoke-virtual/range {v54 .. v59}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 533
    new-instance v61, Landroid/content/IntentFilter;

    const-string v2, "com.sec.knox.SETUP_COMPLETE"

    move-object/from16 v0, v61

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 534
    .local v61, "knoxSetupCompleteReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSetupCompleteReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v59, v0

    sget-object v60, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v62, 0x0

    const/16 v63, 0x0

    move-object/from16 v58, p0

    invoke-virtual/range {v58 .. v63}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 536
    new-instance v65, Landroid/content/IntentFilter;

    const-string v2, "com.android.systemui.recent.RECENTSPANEL_OPEN"

    move-object/from16 v0, v65

    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 537
    .local v65, "recentsAppStartReceiver":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v63, v0

    sget-object v64, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    const/16 v66, 0x0

    const/16 v67, 0x0

    move-object/from16 v62, p0

    invoke-virtual/range {v62 .. v67}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->registerReceiverAsUser(Landroid/content/BroadcastReceiver;Landroid/os/UserHandle;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    .line 539
    new-instance v2, Lcom/sec/android/app/FlashBarService/SwitchWindow;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/app/FlashBarService/SwitchWindow;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .line 540
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    new-instance v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$17;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$17;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->setCallback(Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 565
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 566
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->onDestroy()V

    .line 569
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    if-eqz v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->cancelAllMessages()V

    .line 571
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->onDestroy()V

    .line 574
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    if-eqz v0, :cond_2

    .line 575
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->setCallback(Lcom/sec/android/app/FlashBarService/SwitchWindow$SwitchWindowCallbacks;)V

    .line 576
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->unregisterReceiver()V

    .line 577
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    .line 580
    :cond_2
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 583
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPkgManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 585
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 586
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHomeResumeReceiver:Landroid/content/BroadcastReceiver;

    .line 588
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mBackKeyReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 589
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mBackKeyReceiver:Landroid/content/BroadcastReceiver;

    .line 591
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarFocusDisplayReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 592
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarFocusDisplayReceiver:Landroid/content/BroadcastReceiver;

    .line 594
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 595
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 597
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMlReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 598
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMlReceiver:Landroid/content/BroadcastReceiver;

    .line 601
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSealedReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 602
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSealedReceiver:Landroid/content/BroadcastReceiver;

    .line 603
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSettingProperty:Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->onDestroy()V

    .line 604
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSettingProperty:Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;

    .line 607
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAirButtonStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 608
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAirButtonStateReceiver:Landroid/content/BroadcastReceiver;

    .line 610
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 611
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAlarmStateListener:Landroid/content/BroadcastReceiver;

    .line 613
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 614
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCloseSystemDialogListener:Landroid/content/BroadcastReceiver;

    .line 616
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiUserReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 617
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiUserReceiver:Landroid/content/BroadcastReceiver;

    .line 619
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStopDragModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 620
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStopDragModeReceiver:Landroid/content/BroadcastReceiver;

    .line 622
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mThemeChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 623
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mThemeChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSetupCompleteReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 626
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKnoxSetupCompleteReceiver:Landroid/content/BroadcastReceiver;

    .line 628
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 629
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsAppStartReceiver:Landroid/content/BroadcastReceiver;

    .line 631
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    if-eqz v0, :cond_4

    .line 632
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 633
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone2:Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_3

    .line 634
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone2:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 636
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    # invokes: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->dispose()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;->access$1200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;)V

    .line 637
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone:Landroid/telephony/TelephonyManager;

    .line 638
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPhone2:Landroid/telephony/TelephonyManager;

    .line 639
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMyPhoneStateListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$MyPhoneStateListener;

    .line 642
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarExpandReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 643
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarExpandReceiver:Landroid/content/BroadcastReceiver;

    .line 645
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarAnimatingReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 646
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mStatusBarAnimatingReceiver:Landroid/content/BroadcastReceiver;

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKeyguardUnlockReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->unregisterReceiverSafe(Landroid/content/BroadcastReceiver;)V

    .line 649
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mKeyguardUnlockReceiver:Landroid/content/BroadcastReceiver;

    .line 651
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    if-eqz v0, :cond_5

    .line 652
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 653
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 654
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    .line 661
    :cond_5
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    if-eqz v0, :cond_6

    .line 662
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 663
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 664
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;

    .line 671
    :cond_6
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowExtraCenterBar:Landroid/view/Window;

    if-eqz v0, :cond_8

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowExtraCenterBar:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowExtraCenterBar:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 673
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowExtraCenterBar:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 675
    :cond_7
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowExtraCenterBar:Landroid/view/Window;

    .line 678
    :cond_8
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    if-eqz v0, :cond_9

    .line 679
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 681
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    .line 688
    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    if-eqz v0, :cond_b

    .line 689
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 690
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 692
    :cond_a
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    .line 695
    :cond_b
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    if-eqz v0, :cond_d

    .line 696
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 697
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 699
    :cond_c
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    .line 702
    :cond_d
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    if-eqz v0, :cond_f

    .line 703
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 704
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 706
    :cond_e
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecentGuideLine:Landroid/view/Window;

    .line 708
    :cond_f
    invoke-super {p0}, Landroid/app/SallyService;->onDestroy()V

    .line 709
    return-void

    .line 656
    :cond_10
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowAppList:Z

    .line 657
    const-string v0, "MultiWindowTrayService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Warning : need removeView after attached, mRemoveViewWindowAppList="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowAppList:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 666
    :cond_11
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z

    .line 667
    const-string v0, "MultiWindowTrayService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Warning : need removeView after attached, mRemoveViewWindowCenterBar="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 683
    :cond_12
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z

    .line 684
    const-string v0, "MultiWindowTrayService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Warning : need removeView after attached, mRemoveViewWindowGuideline="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method public onRining()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1342
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    if-eqz v0, :cond_0

    .line 1343
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0, v1, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 1344
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->hideAppEditList()V

    .line 1347
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    .line 1348
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    if-eqz v0, :cond_1

    .line 1349
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->hideSmartEditWindow()V

    .line 1351
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeSmartWindow()V

    .line 1352
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    if-eqz v0, :cond_2

    .line 1353
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->hidePopupViewEditWindow()V

    .line 1355
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewWindow()V

    .line 1356
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 19
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 745
    const-string v14, "MultiWindowTrayService"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "mIsFactoryMode : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsFactoryMode:Z

    move/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  isOtaRegistrationMode : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isOtaRegistrationMode()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "  isMultiWindowSettingEnabled : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isMultiWindowSettingEnabled()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " isVzwSetupRunning : "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isVzwSetupRunning()Z

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 751
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsFactoryMode:Z

    if-nez v14, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isOtaRegistrationMode()Z

    move-result v14

    if-nez v14, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isVzwSetupRunning()Z

    move-result v14

    if-nez v14, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isCompleteUserSetup()Z

    move-result v14

    if-eqz v14, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isKidsModeRunning()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 752
    :cond_0
    const/4 v14, 0x2

    .line 963
    :goto_0
    return v14

    .line 759
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/high16 v15, 0x7f070000

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    .line 760
    .local v12, "trayType":I
    const/4 v4, 0x0

    .line 761
    .local v4, "alwaysNotSticky":Z
    if-nez v12, :cond_2

    .line 762
    const/4 v4, 0x1

    .line 765
    :cond_2
    const/4 v11, 0x2

    .line 766
    .local v11, "result":I
    if-eqz p1, :cond_5

    const-string v14, "com.sec.android.action.MULTIWINDOW_SMART_WINDOW_LAUNCH"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 767
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 768
    .local v5, "cropImageInfo":Landroid/os/Bundle;
    const-string v14, "cropRect"

    invoke-virtual {v5, v14}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    .line 769
    .local v6, "cropImageRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCropedRectData:Landroid/graphics/RectF;

    invoke-virtual {v14, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 770
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeSmartWindow()V

    .line 771
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeSmartWindow(Landroid/graphics/Rect;)V

    .line 963
    .end local v5    # "cropImageInfo":Landroid/os/Bundle;
    .end local v6    # "cropImageRect":Landroid/graphics/Rect;
    :cond_3
    :goto_1
    if-eqz v4, :cond_4

    const/4 v11, 0x2

    .end local v11    # "result":I
    :cond_4
    move v14, v11

    goto :goto_0

    .line 772
    .restart local v11    # "result":I
    :cond_5
    if-eqz p1, :cond_6

    const-string v14, "com.sec.android.action.MULTIWINDOW_POPUP_VIEW_WINDOW_LAUNCH"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 773
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    .line 774
    .restart local v5    # "cropImageInfo":Landroid/os/Bundle;
    const-string v14, "cropRect"

    invoke-virtual {v5, v14}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Landroid/graphics/Rect;

    .line 775
    .restart local v6    # "cropImageRect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCropedRectData:Landroid/graphics/RectF;

    invoke-virtual {v14, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 776
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewWindow()V

    .line 777
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makePopupViewWindow(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 779
    .end local v5    # "cropImageInfo":Landroid/os/Bundle;
    .end local v6    # "cropImageRect":Landroid/graphics/Rect;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->canShow()Z

    move-result v14

    if-nez v14, :cond_b

    .line 780
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    if-eqz v14, :cond_7

    .line 781
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getFlashBarEnabled()Z

    move-result v14

    if-eqz v14, :cond_7

    .line 782
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    .line 786
    :cond_7
    const/4 v3, 0x1

    .line 787
    .local v3, "allOff":Z
    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v14

    if-eqz v14, :cond_9

    .line 789
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v14

    invoke-interface {v14}, Landroid/app/IActivityManager;->getRunningUserIds()[I

    move-result-object v13

    .line 790
    .local v13, "userIds":[I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_2
    array-length v14, v13

    if-ge v9, v14, :cond_9

    .line 791
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "multi_window_enabled"

    const/16 v16, 0x0

    aget v17, v13, v9

    invoke-static/range {v14 .. v17}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    .line 795
    .local v7, "enable":I
    const/4 v14, 0x1

    if-ne v7, v14, :cond_8

    .line 796
    const/4 v3, 0x0

    .line 790
    :cond_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    .line 800
    .end local v7    # "enable":I
    .end local v9    # "i":I
    .end local v13    # "userIds":[I
    :catch_0
    move-exception v14

    .line 804
    :cond_9
    if-eqz v3, :cond_a

    .line 806
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mHandler:Landroid/os/Handler;

    new-instance v15, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$18;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$18;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    invoke-virtual {v14, v15}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 813
    :cond_a
    const/4 v14, 0x2

    goto/16 :goto_0

    .line 816
    .end local v3    # "allOff":Z
    :cond_b
    if-eqz p1, :cond_d

    const-string v14, "com.sec.android.multiwindow.ui.center_bar_display"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_d

    .line 817
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 818
    if-eqz v4, :cond_c

    const/4 v14, 0x2

    goto/16 :goto_0

    :cond_c
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 821
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    if-eqz v14, :cond_27

    .line 822
    if-eqz p1, :cond_11

    const-string v14, "com.sec.android.multiwindow.close.traybar"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_11

    .line 823
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->canCloseWindow()Z

    move-result v14

    if-eqz v14, :cond_e

    .line 824
    sget-boolean v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v14, :cond_f

    const-string v14, "com.sec.android.multiwindow.extra.forceclose"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-nez v14, :cond_f

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->isPocketTrayOpen()Z

    move-result v14

    if-eqz v14, :cond_f

    .line 827
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->closePocketTray()V

    .line 832
    :cond_e
    :goto_3
    if-eqz v4, :cond_10

    const/4 v14, 0x2

    goto/16 :goto_0

    .line 829
    :cond_f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto :goto_3

    .line 832
    :cond_10
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 835
    :cond_11
    if-eqz p1, :cond_15

    const-string v14, "com.sec.android.multiwindow.open.traybar"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_15

    .line 837
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getFlashBarEnabled()Z

    move-result v14

    if-nez v14, :cond_12

    .line 838
    const-string v14, "com.sec.android.multiwindow.extra.expand"

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_13

    .line 839
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    .line 844
    :cond_12
    :goto_4
    if-eqz v4, :cond_14

    const/4 v14, 0x2

    goto/16 :goto_0

    .line 841
    :cond_13
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto :goto_4

    .line 844
    :cond_14
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 847
    :cond_15
    if-eqz p1, :cond_17

    const-string v14, "com.sec.android.multiwindow.add.recentapp"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_17

    .line 848
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeRecentApp()V

    .line 849
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->animationRecentIconByHomeKey()V

    .line 850
    if-eqz v4, :cond_16

    const/4 v14, 0x2

    goto/16 :goto_0

    :cond_16
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 853
    :cond_17
    const/4 v8, -0x1

    .line 854
    .local v8, "forced":I
    if-eqz p1, :cond_18

    .line 855
    const-string v14, "com.sec.android.multiwindow.ui.forceshow"

    const/4 v15, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 858
    :cond_18
    if-lez v8, :cond_1b

    .line 859
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getFlashBarEnabled()Z

    move-result v14

    if-eqz v14, :cond_19

    if-eqz v12, :cond_1a

    .line 860
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 861
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    .line 906
    :cond_1a
    :goto_5
    const/4 v14, 0x2

    goto/16 :goto_0

    .line 863
    :cond_1b
    if-nez v8, :cond_1c

    .line 864
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 865
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto :goto_5

    .line 867
    :cond_1c
    sget-boolean v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v14, :cond_23

    .line 868
    if-eqz p1, :cond_1d

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    if-eqz v14, :cond_1d

    const-string v14, "com.sec.android.action.RECENT_LONG_PRESS_LAUNCH"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1d

    .line 869
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    const-string v14, "com.sec.android.multiwindow.extra.intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Landroid/content/Intent;

    const-string v16, "com.sec.android.multiwindow.extra.taskId"

    const/16 v17, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    const-string v17, "com.sec.android.multiwindow.extra.support_multiwindow"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/sec/android/app/FlashBarService/GuideLine;->show(Landroid/content/Intent;IZ)V

    goto :goto_5

    .line 872
    :cond_1d
    if-eqz p1, :cond_1e

    const-string v14, "recentUI.multiwindow"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_1e

    .line 873
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const-string v15, "recentUI.multiwindow.package"

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "recentUI.multiwindow.taskId"

    const/16 v17, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showViewPagerTray(Ljava/lang/String;I)V

    goto/16 :goto_5

    .line 874
    :cond_1e
    if-eqz p1, :cond_20

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.sec.android.multiwindow.first.split"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_1f

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.sec.android.multiwindow.recentkey.start"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_20

    .line 876
    :cond_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 877
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_5

    .line 878
    :cond_20
    if-eqz p1, :cond_21

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.sec.android.multiwindow.gesture.overlayHelp"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_21

    .line 879
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->createGestureHelp()V

    goto/16 :goto_5

    .line 880
    :cond_21
    sget-boolean v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_TRAYBAR_UI:Z

    if-eqz v14, :cond_1a

    .line 881
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v14, v14, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AppListController;->isMultiWindowTrayOpen()Z

    move-result v14

    if-eqz v14, :cond_22

    .line 882
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto/16 :goto_5

    .line 884
    :cond_22
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 885
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_5

    .line 888
    :cond_23
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getFlashBarEnabled()Z

    move-result v14

    if-eqz v14, :cond_26

    .line 889
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/high16 v15, 0x7f070000

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_25

    .line 890
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    iget-object v14, v14, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AppListController;->isMultiWindowTrayOpen()Z

    move-result v14

    if-eqz v14, :cond_24

    .line 891
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    goto/16 :goto_5

    .line 893
    :cond_24
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 894
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_5

    .line 897
    :cond_25
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 898
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_5

    .line 901
    :cond_26
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 902
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_5

    .line 909
    .end local v8    # "forced":I
    :cond_27
    invoke-super/range {p0 .. p3}, Landroid/app/SallyService;->onStartCommand(Landroid/content/Intent;II)I

    move-result v11

    .line 912
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->forceSetMWTrayOpenState(Z)V

    .line 914
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeAppListWindow()V

    .line 915
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeGuidelineWindow()V

    .line 916
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeCenterBarWindow(I)V

    .line 917
    sget-boolean v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v14, :cond_28

    .line 918
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getAppListController()Lcom/sec/android/app/FlashBarService/AppListController;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeRecentGuideLineWindow(Lcom/sec/android/app/FlashBarService/AppListController;)V

    .line 921
    :cond_28
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v14

    const-string v15, "multi_window_flashbar_shown"

    const/16 v16, -0x1

    const/16 v17, -0x2

    invoke-static/range {v14 .. v17}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_29

    const/4 v10, 0x1

    .line 923
    .local v10, "isFirst":Z
    :goto_6
    sget-boolean v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v14, :cond_2f

    .line 924
    if-eqz p1, :cond_3

    .line 925
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    if-eqz v14, :cond_2a

    const-string v14, "com.sec.android.action.RECENT_LONG_PRESS_LAUNCH"

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2a

    .line 926
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentGuideLine:Lcom/sec/android/app/FlashBarService/GuideLine;

    const-string v14, "com.sec.android.multiwindow.extra.intent"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v14

    check-cast v14, Landroid/content/Intent;

    const-string v16, "com.sec.android.multiwindow.extra.taskId"

    const/16 v17, -0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v16

    const-string v17, "com.sec.android.multiwindow.extra.support_multiwindow"

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v17

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v15, v14, v0, v1}, Lcom/sec/android/app/FlashBarService/GuideLine;->show(Landroid/content/Intent;IZ)V

    goto/16 :goto_1

    .line 921
    .end local v10    # "isFirst":Z
    :cond_29
    const/4 v10, 0x0

    goto :goto_6

    .line 929
    .restart local v10    # "isFirst":Z
    :cond_2a
    const-string v14, "recentUI.multiwindow"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_2b

    .line 930
    new-instance v14, Landroid/content/Intent;

    move-object/from16 v0, p1

    invoke-direct {v14, v0}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowViewPagerIntent:Landroid/content/Intent;

    .line 931
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowViewPager:Z

    goto/16 :goto_1

    .line 932
    :cond_2b
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.sec.android.multiwindow.first.split"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_2c

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.sec.android.multiwindow.recentkey.start"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2d

    .line 934
    :cond_2c
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->reviveMultiWindowTray()V

    goto/16 :goto_1

    .line 935
    :cond_2d
    if-eqz p1, :cond_2e

    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    const-string v15, "com.sec.android.multiwindow.gesture.overlayHelp"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2e

    .line 936
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowGestureOverlayHelp:Z

    goto/16 :goto_1

    .line 937
    :cond_2e
    sget-boolean v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_TRAYBAR_UI:Z

    if-eqz v14, :cond_3

    .line 938
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 939
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x1

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_1

    .line 942
    :cond_2f
    if-eqz p1, :cond_30

    const-string v14, "com.sec.android.multiwindow.ui.forceshow"

    const/4 v15, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    if-gtz v14, :cond_31

    :cond_30
    if-eqz v10, :cond_32

    .line 943
    :cond_31
    const-string v14, "MultiWindowTrayService"

    const-string v15, "Force launch MW cursor"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 945
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_1

    .line 947
    :cond_32
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const/high16 v15, 0x7f070000

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_35

    .line 948
    if-nez p1, :cond_34

    .line 950
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v14}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->reviveMultiWindowTray()V

    .line 951
    if-eqz v4, :cond_33

    const/4 v14, 0x2

    goto/16 :goto_0

    :cond_33
    const/4 v14, 0x1

    goto/16 :goto_0

    .line 953
    :cond_34
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 954
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_1

    .line 957
    :cond_35
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->setFlashBarState(Z)V

    .line 958
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    goto/16 :goto_1
.end method

.method public redrawCenterBar()V
    .locals 2

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1636
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setImageToImageView(Z)V

    .line 1638
    :cond_0
    return-void
.end method

.method public removeAppListEditWindow()V
    .locals 1

    .prologue
    .line 1106
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1107
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowEditList:Landroid/view/Window;

    .line 1110
    :cond_0
    return-void
.end method

.method public removePopupViewEditWindow()V
    .locals 1

    .prologue
    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupViewEdit:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupViewEdit:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1148
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupViewEdit:Landroid/view/Window;

    .line 1150
    :cond_0
    return-void
.end method

.method public removePopupViewWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1210
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1211
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1212
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1213
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowPopupView:Landroid/view/Window;

    .line 1214
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    .line 1216
    :cond_0
    return-void
.end method

.method public removeRecentsWindow()V
    .locals 2

    .prologue
    .line 1465
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1466
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->refreshWindow(I)V

    .line 1468
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    if-eqz v0, :cond_1

    .line 1469
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->closeRecentsPanel()V

    .line 1470
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->removeWindow()V

    .line 1471
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowRecents:Landroid/view/Window;

    .line 1473
    :cond_1
    return-void
.end method

.method public removeSmartEditWindow()V
    .locals 1

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    .line 1143
    :cond_0
    return-void
.end method

.method public removeSmartWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1197
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1199
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1200
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmart:Landroid/view/Window;

    .line 1201
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowDim:Landroid/view/Window;

    .line 1202
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1204
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowSmartEdit:Landroid/view/Window;

    .line 1207
    :cond_0
    return-void
.end method

.method public setGlowEffect(IZ)V
    .locals 14
    .param p1, "pos"    # I
    .param p2, "needEffect"    # Z

    .prologue
    .line 1219
    if-nez p2, :cond_1

    .line 1220
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    if-eqz v12, :cond_0

    .line 1221
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    invoke-virtual {p0, v12}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 1222
    const/4 v12, 0x0

    iput-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    .line 1269
    :cond_0
    :goto_0
    return-void

    .line 1225
    :cond_1
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    if-nez v12, :cond_0

    .line 1226
    const/16 v11, 0x7ea

    .line 1227
    .local v11, "windowType":I
    new-instance v5, Landroid/widget/ImageView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-direct {v5, v12}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1228
    .local v5, "glowEffectView":Landroid/widget/ImageView;
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    .line 1230
    .local v9, "paint":Landroid/graphics/Paint;
    const/4 v4, 0x0

    .line 1231
    .local v4, "glowEffectDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    .line 1232
    .local v2, "edgeEffectDrawable":Landroid/graphics/drawable/Drawable;
    const/4 v3, 0x0

    .line 1233
    .local v3, "glowEffectBmp":Landroid/graphics/Bitmap;
    const/4 v1, 0x0

    .line 1235
    .local v1, "edgeEffectBmp":Landroid/graphics/Bitmap;
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a006f

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v10, v12

    .line 1236
    .local v10, "width":I
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a0070

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v6, v12

    .line 1237
    .local v6, "height":I
    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v10, v6, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1238
    .local v8, "outputBmp":Landroid/graphics/Bitmap;
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1240
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {p0, v5, v10, v6, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v12

    iput-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    .line 1241
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    invoke-virtual {v12}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 1243
    .local v7, "lp":Landroid/view/WindowManager$LayoutParams;
    packed-switch p1, :pswitch_data_0

    .line 1265
    :goto_1
    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1267
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGlow:Landroid/view/Window;

    invoke-virtual {v13}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v13

    invoke-interface {v12, v13, v7}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1245
    :pswitch_0
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f02012d

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1246
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f02012a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v12, v4

    .line 1247
    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v12, v2

    .line 1248
    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1250
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    sub-int v12, v10, v12

    int-to-float v12, v12

    const/4 v13, 0x0

    invoke-virtual {v0, v3, v12, v13, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1251
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    sub-int v12, v10, v12

    int-to-float v12, v12

    const/4 v13, 0x0

    invoke-virtual {v0, v1, v12, v13, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1252
    const/4 v12, 0x5

    iput v12, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto :goto_1

    .line 1255
    :pswitch_1
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f02012c

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1256
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f020129

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v12, v4

    .line 1257
    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    move-object v12, v2

    .line 1258
    check-cast v12, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1260
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v0, v3, v12, v13, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1261
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v0, v1, v12, v13, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1262
    const/4 v12, 0x3

    iput v12, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto/16 :goto_1

    .line 1243
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public showSwitchWindow()V
    .locals 1

    .prologue
    .line 1624
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    if-eqz v0, :cond_0

    .line 1625
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindow:Lcom/sec/android/app/FlashBarService/SwitchWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SwitchWindow;->show()V

    .line 1626
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSwitchWindowActive:Z

    .line 1628
    :cond_0
    return-void
.end method

.method public updateCenterBarPoint(Landroid/graphics/Point;)V
    .locals 1
    .param p1, "centerBarPoint"    # Landroid/graphics/Point;

    .prologue
    .line 1649
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    if-eqz v0, :cond_0

    .line 1650
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->updateCenterBarPoint(Landroid/graphics/Point;)V

    .line 1652
    :cond_0
    return-void
.end method

.method public updateForeground()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 714
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 716
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsForeground:Z

    if-nez v1, :cond_2

    .line 717
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsForeground:Z

    .line 718
    const-string v1, "MultiWindowTrayService"

    const-string v2, "startForeground()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 721
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x1

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 740
    :cond_2
    :goto_0
    return-void

    .line 722
    :catch_0
    move-exception v0

    .line 723
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "MultiWindowTrayService"

    const-string v2, "cannot set process foregorund"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 724
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 728
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsForeground:Z

    if-eqz v1, :cond_2

    .line 729
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mIsForeground:Z

    .line 730
    const-string v1, "MultiWindowTrayService"

    const-string v2, "stopForeground()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 733
    :try_start_1
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mForegroundToken:Landroid/os/IBinder;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/app/IActivityManager;->setProcessForeground(Landroid/os/IBinder;IZ)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 734
    :catch_1
    move-exception v0

    .line 735
    .restart local v0    # "e":Landroid/os/RemoteException;
    const-string v1, "MultiWindowTrayService"

    const-string v2, "cannot set OFF process foregorund"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 736
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
