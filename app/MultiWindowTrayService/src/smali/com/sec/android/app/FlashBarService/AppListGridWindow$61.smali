.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 4621
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4624
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 4625
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    .line 4626
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 4627
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4629
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 4630
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I
    invoke-static {v2, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$12002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4631
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v3, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentHistoryIndex:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 4655
    :goto_0
    return-void

    .line 4636
    :cond_1
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 4637
    .local v1, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 4638
    new-array v0, v6, [I

    .line 4639
    .local v0, "loc":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 4640
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 4652
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v4}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 4653
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V
    invoke-static {v2, v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V

    .line 4654
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 4645
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    aget v3, v0, v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$12100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$12002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    goto :goto_1

    .line 4649
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    aget v3, v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbNailLayoutWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$12100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentAppPosition:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$12002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    goto :goto_1

    .line 4640
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
