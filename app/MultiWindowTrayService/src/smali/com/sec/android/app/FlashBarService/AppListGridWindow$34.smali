.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createResetConfirmDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3096
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3099
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->resetMultiWindowTray()V

    .line 3100
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 3101
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissResetConfirmDialog()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3102
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateAppListEditContents()V

    .line 3103
    return-void
.end method
