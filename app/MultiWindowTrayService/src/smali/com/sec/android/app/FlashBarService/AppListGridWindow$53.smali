.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3796
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f080002

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 3800
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-boolean v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbAnimating:Z

    if-nez v3, :cond_2

    .line 3801
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 3802
    const-string v3, "AppListGridWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onClick : mExpandAppList = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3835
    :goto_0
    return-void

    .line 3806
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3807
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v3, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3808
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 3809
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 3810
    .local v0, "dim":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 3811
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 3813
    .local v1, "paddingBottom":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 3814
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 3815
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 3816
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 3817
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3818
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 3819
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3820
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3821
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 3822
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3823
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketEditBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f020031

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 3825
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeAppListEditWindow()V

    .line 3826
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v4

    invoke-virtual {v3, v4, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 3827
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3828
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 3829
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 3834
    .end local v0    # "dim":Landroid/graphics/drawable/Drawable;
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingTop":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissHistoryWindow(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V

    goto/16 :goto_0

    .line 3831
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->hideAppEditList(Z)V

    .line 3832
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$53;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_1
.end method
