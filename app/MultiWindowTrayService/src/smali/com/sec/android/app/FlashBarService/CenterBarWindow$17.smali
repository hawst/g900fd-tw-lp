.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1838
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 1840
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1841
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isRecentsWindowShowing()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1842
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIvt:[B
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 1844
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->forceHideInputMethod()V
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 1845
    const/4 v1, 0x0

    .line 1846
    .local v1, "targetTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v4

    const/16 v5, 0x64

    const/4 v6, 0x2

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v3

    .line 1847
    .local v3, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1848
    .local v2, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v4, v2, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    iget-object v4, v2, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v4}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 1850
    move-object v1, v2

    .line 1854
    .end local v2    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_1
    if-eqz v1, :cond_2

    .line 1855
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActivityManager:Landroid/app/ActivityManager;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/app/ActivityManager;

    move-result-object v4

    iget v5, v1, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    invoke-virtual {v4, v5, v7}, Landroid/app/ActivityManager;->removeTask(II)Z

    .line 1858
    :cond_2
    sget-boolean v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v4, :cond_3

    .line 1859
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17$1;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;)V

    const-wide/16 v6, 0x190

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1867
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "targetTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v3    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_3
    return-void
.end method
