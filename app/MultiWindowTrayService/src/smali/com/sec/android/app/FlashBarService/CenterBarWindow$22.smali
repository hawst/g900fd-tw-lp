.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animateCenterBarClose(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 2192
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 2199
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 2200
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    .line 2201
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3202(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 2202
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonCloseAnimating:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 2203
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 2196
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 2194
    return-void
.end method
