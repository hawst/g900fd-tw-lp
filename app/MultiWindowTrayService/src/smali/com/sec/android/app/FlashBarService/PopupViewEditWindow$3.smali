.class Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;
.super Ljava/lang/Object;
.source "PopupViewEditWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)V
    .locals 0

    .prologue
    .line 330
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 332
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 333
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 365
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v2, 0x1

    return v2

    .line 335
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditDragIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$600(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)I

    move-result v2

    if-eq v2, v6, :cond_0

    .line 336
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/ImageView;

    move-result-object v2

    const v3, 0x7f02007f

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 341
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->updateSmartEditChanged()V

    .line 342
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$702(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;Z)Z

    .line 343
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditDragIndex:I
    invoke-static {v2, v6}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$602(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;I)I

    goto :goto_0

    .line 347
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mAppListDragIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$800(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)I

    move-result v2

    if-eq v2, v6, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$700(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 348
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$1000(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/os/SystemVibrator;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mIvt:[B
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$900(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$1000(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/os/SystemVibrator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 349
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$1100(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mAppListDragIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$800(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->moveToSmartEditItem(I)V

    .line 350
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->updateSmartEditChanged()V

    .line 351
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$1100(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 352
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mScrollHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 353
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mScrollHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 355
    :cond_1
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 356
    .local v1, "msg":Landroid/os/Message;
    iput v5, v1, Landroid/os/Message;->what:I

    .line 357
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mScrollHandler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 359
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v2, v5}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$702(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;Z)Z

    .line 360
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mAppListDragIndex:I
    invoke-static {v2, v6}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$802(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;I)I

    goto/16 :goto_0

    .line 333
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
