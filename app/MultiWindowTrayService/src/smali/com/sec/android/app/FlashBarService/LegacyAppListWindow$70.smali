.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4277
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v11, 0x68

    const/16 v10, 0x67

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 4279
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0019

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v2, v7

    .line 4280
    .local v2, "moveBtnHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0071

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v3, v7

    .line 4281
    .local v3, "moveBtnMoveMargin":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 4373
    :cond_0
    :goto_0
    return v6

    .line 4284
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->enablePocketButtons(Z)V
    invoke-static {v7, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 4285
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4286
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPressMoveBtn:Z
    invoke-static {v7, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4287
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setImageToMoveButton(IZ)V
    invoke-static {v5, v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZ)V

    .line 4288
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isRecentsWindowShowing()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 4289
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    .line 4291
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPrevExpandAppList:Z
    invoke-static {v5, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto :goto_0

    .line 4294
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4295
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v8

    float-to-int v8, v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4296
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    float-to-int v8, v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4297
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    .line 4298
    .local v1, "left":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    sub-int v4, v7, v8

    .line 4300
    .local v4, "right":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    packed-switch v7, :pswitch_data_1

    .line 4313
    :cond_2
    :goto_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 4314
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionY:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setAppListHandlePosition(I)V
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    .line 4315
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v7, v8, v5, v9, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZZ)V

    :goto_2
    move v6, v5

    .line 4332
    goto/16 :goto_0

    .line 4302
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    add-int v8, v2, v3

    if-lt v7, v8, :cond_2

    .line 4303
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v7, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto :goto_1

    .line 4306
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    sub-int/2addr v8, v2

    sub-int/2addr v8, v3

    if-gt v7, v8, :cond_2

    .line 4307
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v7, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$902(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto :goto_1

    .line 4317
    :cond_3
    if-ge v1, v4, :cond_5

    .line 4318
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    if-ne v7, v11, :cond_4

    .line 4319
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I
    invoke-static {v7, v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4320
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v9

    invoke-virtual {v7, v8, v9, v10, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 4322
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v7, v10, v6, v8, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZZ)V

    .line 4330
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideTextPadding()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_2

    .line 4324
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    if-ne v7, v10, :cond_6

    .line 4325
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mBeforeAppListPosition:I
    invoke-static {v7, v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4326
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v9

    invoke-virtual {v7, v8, v9, v11, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 4328
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v7, v11, v6, v8, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZZ)V

    goto :goto_3

    .line 4335
    .end local v1    # "left":I
    .end local v4    # "right":I
    :pswitch_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->enablePocketButtons(Z)V
    invoke-static {v7, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 4336
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPressMoveBtn:Z
    invoke-static {v7, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4337
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setImageToMoveButton(IZ)V
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZ)V

    .line 4338
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/widget/ImageView;->playSoundEffect(I)V

    .line 4339
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-ne v7, v5, :cond_b

    .line 4340
    const/4 v0, 0x0

    .line 4341
    .local v0, "hide":Z
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsAvailableMove:Z
    invoke-static {v7, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4342
    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v7, :cond_9

    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v7, :cond_9

    .line 4343
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPrevExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$11900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-nez v7, :cond_8

    move v0, v5

    .line 4356
    :cond_7
    :goto_4
    if-eqz v0, :cond_a

    .line 4357
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v7, v8, v5, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    .line 4361
    :goto_5
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v7, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v8}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v8

    invoke-virtual {v5, v7, v8}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 4362
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 4370
    .end local v0    # "hide":Z
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    invoke-virtual {v5, v7, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->setGlowEffect(IZ)V

    goto/16 :goto_0

    .restart local v0    # "hide":Z
    :cond_8
    move v0, v6

    .line 4343
    goto :goto_4

    .line 4345
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    packed-switch v7, :pswitch_data_2

    goto :goto_4

    .line 4347
    :pswitch_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    if-gt v7, v2, :cond_7

    .line 4348
    const/4 v0, 0x1

    goto :goto_4

    .line 4351
    :pswitch_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    sub-int/2addr v8, v2

    if-lt v7, v8, :cond_7

    .line 4352
    const/4 v0, 0x1

    goto :goto_4

    .line 4359
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v7, v8, v5, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    goto :goto_5

    .line 4364
    .end local v0    # "hide":Z
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 4365
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->closeFlashBar()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_6

    .line 4367
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$70;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->openFlashBar()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_6

    .line 4281
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 4300
    :pswitch_data_1
    .packed-switch 0x67
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 4345
    :pswitch_data_2
    .packed-switch 0x67
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
