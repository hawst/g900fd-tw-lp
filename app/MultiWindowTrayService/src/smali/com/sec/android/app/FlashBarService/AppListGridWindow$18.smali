.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeTraybarHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1366
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1368
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1369
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "do_not_show_help_popup_traybar"

    const/4 v2, 0x1

    const/4 v3, -0x2

    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$18;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->hideTraybarHelpPopup()V

    .line 1372
    return-void
.end method
