.class public Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewPagerAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    .line 6019
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 6020
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->context:Landroid/content/Context;

    .line 6021
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 6022
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeViewPagerAppList()I

    move-result v0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {p1, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 6023
    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    if-nez v0, :cond_1

    .line 6024
    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {p1, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 6025
    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedHelpPage:Z
    invoke-static {p1, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 6030
    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 6031
    return-void

    .line 6027
    :cond_1
    const/4 v0, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedHelpPage:Z
    invoke-static {p1, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto :goto_0
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 6089
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/widget/FrameLayout;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 6090
    return-void
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 6034
    const/4 v0, 0x0

    .line 6035
    .local v0, "appCnt":I
    const/4 v1, 0x0

    .line 6036
    .local v1, "pageCnt":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v2, :cond_2

    .line 6037
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListCnt(I)I

    move-result v0

    .line 6038
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    div-int v1, v0, v2

    .line 6039
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowItemNum:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    rem-int v2, v0, v2

    if-lez v2, :cond_0

    .line 6040
    add-int/lit8 v1, v1, 0x1

    .line 6042
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    if-lez v2, :cond_1

    .line 6043
    add-int/lit8 v1, v1, 0x1

    .line 6045
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPageCount:I
    invoke-static {v2, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    move v2, v1

    .line 6048
    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 7
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 6058
    const/4 v2, 0x0

    .line 6059
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastConfig:Landroid/content/res/Configuration;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 6060
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030006

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 6064
    :goto_0
    const v3, 0x7f0f005d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 6065
    .local v0, "gridView":Landroid/widget/GridView;
    const v3, 0x7f0f005e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 6066
    .local v1, "helpText":Landroid/widget/TextView;
    if-nez p2, :cond_3

    .line 6067
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    if-lez v3, :cond_2

    .line 6068
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedHelpPage:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 6069
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 6083
    :goto_1
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    invoke-virtual {p1, v2, v5}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 6084
    return-object v2

    .line 6062
    .end local v0    # "gridView":Landroid/widget/GridView;
    .end local v1    # "helpText":Landroid/widget/TextView;
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030007

    invoke-virtual {v3, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 6071
    .restart local v0    # "gridView":Landroid/widget/GridView;
    .restart local v1    # "helpText":Landroid/widget/TextView;
    :cond_1
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {v3, v4, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 6072
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View$OnKeyListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_1

    .line 6075
    :cond_2
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$GridViewAdapter;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {v3, v4, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$GridViewAdapter;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 6076
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLegacyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View$OnKeyListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_1

    .line 6079
    :cond_3
    new-instance v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$GridViewAdapter;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {v3, v4, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$GridViewAdapter;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 6080
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLegacyAppListWindowKeyListener:Landroid/view/View$OnKeyListener;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View$OnKeyListener;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_1
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 6053
    check-cast p2, Landroid/widget/FrameLayout;

    .end local p2    # "object":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
