.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;
.super Landroid/content/BroadcastReceiver;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 6093
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 6096
    const-string v0, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6097
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissResetConfirmDialog()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$10400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 6098
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissTemplateDialog()V

    .line 6099
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissConfirmDialog()V

    .line 6100
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissUnableDialog()V

    .line 6101
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$95;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissViewPagerAppList()V

    .line 6103
    :cond_0
    return-void
.end method
