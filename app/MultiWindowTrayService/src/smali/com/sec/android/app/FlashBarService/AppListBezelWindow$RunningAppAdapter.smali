.class public Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RunningAppAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 2078
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2074
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->context:Landroid/content/Context;

    .line 2079
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->context:Landroid/content/Context;

    .line 2080
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 2081
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2096
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030024

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2098
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;-><init>()V

    .line 2100
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;
    const v2, 0x7f0f00e7

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->labelView:Landroid/widget/TextView;

    .line 2101
    const v2, 0x7f0f00e6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->iconView:Landroid/widget/ImageView;

    .line 2102
    const v2, 0x7f0f00e5

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->tumbnailView:Landroid/widget/ImageView;

    .line 2103
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2105
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2106
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 2084
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getRunningAppCnt()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2090
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getRunningAppByIndex(I)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 2131
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 2110
    if-nez p2, :cond_0

    .line 2111
    invoke-virtual {p0, p3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->createView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2112
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2113
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Recycled child has parent"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2115
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2116
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Recycled child has parent"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2119
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;

    .line 2121
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getRunningAppByIndex(I)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v1

    .line 2122
    .local v1, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2123
    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->tumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2124
    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2126
    return-object p2
.end method
