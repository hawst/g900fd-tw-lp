.class public abstract Lcom/sec/android/app/FlashBarService/recent/BaseContainer;
.super Landroid/widget/RelativeLayout;
.source "BaseContainer.java"

# interfaces
.implements Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;
.implements Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;,
        Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;
    }
.end annotation


# static fields
.field protected static BOTTOM_STACK_BORDER:F

.field protected static CLOSE_ANIMATION:I

.field protected static MAX_STACK_APPS:I

.field protected static ROTATE_SPEED:F

.field protected static SHADOW_ANIMATION_DURATION:I

.field protected static STACK_ANIMATION_DURATION:I

.field protected static TOP_STACK_Z_DIFF:F

.field protected static Z_DIFF:F

.field protected static mAnimationScrollStep:I

.field protected static mAnimationScrollStepForSizeOne:I

.field protected static mBeginRotateDownDistance:F

.field protected static mBeginRotateUpDistance:F

.field protected static mBottomStackVisiblePart:F

.field protected static mMaxRotateAngle:I

.field protected static mTopStackDistance:F

.field protected static mTopStackStartPosition:F


# instance fields
.field private Z_ANIMATION_DURATION:J

.field animators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation
.end field

.field bottomStackIndex:I

.field doMove:Z

.field dx:F

.field dy:F

.field gd:Landroid/view/GestureDetector;

.field mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

.field private mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

.field protected mBottomStackReleaseDistance:F

.field private mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

.field mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

.field protected mFingerOnDisplay:Z

.field private mLinearLayout:Landroid/widget/LinearLayout;

.field private mNumItemsInOneScreenful:I

.field private mPagingTouchSlop:I

.field private mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

.field private mRecycledViews:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mScrollInterpolator:Landroid/view/animation/Interpolator;

.field protected mScrolling:Z

.field mShadowInterpolator:Landroid/view/animation/Interpolator;

.field protected mSwipe:Z

.field protected mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

.field protected mTopStackReleaseDistance:F

.field mover:Ljava/lang/Runnable;

.field rotated_angle:F

.field rotation_restorer:Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

.field scroll_type:I

.field scroller:Landroid/widget/Scroller;

.field should_clean_rotation:Z

.field should_send_cancel:Z

.field protected target:Landroid/view/View;

.field topStackIndex:I

.field views:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xc8

    .line 50
    const v0, 0x3e99999a    # 0.3f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->ROTATE_SPEED:F

    .line 51
    sput v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->STACK_ANIMATION_DURATION:I

    .line 52
    sput v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->CLOSE_ANIMATION:I

    .line 53
    const/16 v0, 0x12

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->MAX_STACK_APPS:I

    .line 54
    const v0, 0x3dae147b    # 0.085f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_DIFF:F

    .line 55
    const v0, 0x3ccccccd    # 0.025f

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->TOP_STACK_Z_DIFF:F

    .line 56
    const/16 v0, 0x12c

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->SHADOW_ANIMATION_DURATION:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 117
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 118
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, -0x1

    const/high16 v4, 0x3fc00000    # 1.5f

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 129
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_ANIMATION_DURATION:J

    .line 58
    const/high16 v0, 0x435c0000    # 220.0f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackReleaseDistance:F

    .line 60
    const v0, 0x43fd8000    # 507.0f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBottomStackReleaseDistance:F

    .line 83
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->dx:F

    .line 84
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->dy:F

    .line 85
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doMove:Z

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    .line 90
    iput v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    .line 91
    const v0, 0x7fffffff

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->target:Landroid/view/View;

    .line 93
    iput v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroll_type:I

    .line 96
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_send_cancel:Z

    .line 97
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    .line 98
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    .line 99
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    .line 101
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    .line 103
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    .line 105
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollInterpolator:Landroid/view/animation/Interpolator;

    .line 106
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40400000    # 3.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mShadowInterpolator:Landroid/view/animation/Interpolator;

    .line 334
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotation_restorer:Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    .line 623
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mover:Ljava/lang/Runnable;

    .line 131
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a011c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBottomStackVisiblePart:F

    .line 132
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mMaxRotateAngle:I

    .line 133
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0119

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackStartPosition:F

    .line 134
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a011a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackDistance:F

    .line 135
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0117

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateDownDistance:F

    .line 136
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateUpDistance:F

    .line 138
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPagingTouchSlop:I

    .line 139
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a011d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBottomStackReleaseDistance:F

    .line 140
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a011b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackReleaseDistance:F

    .line 141
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a011e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAnimationScrollStep:I

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a011f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAnimationScrollStepForSizeOne:I

    .line 144
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->gd:Landroid/view/GestureDetector;

    .line 207
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->gd:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    .line 208
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    .line 210
    const/4 v0, 0x1

    invoke-static {p1, p2, p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->create(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;Z)Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    .line 212
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mRecycledViews:Ljava/util/HashSet;

    .line 214
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    .prologue
    .line 48
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPagingTouchSlop:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->update()V

    return-void
.end method

.method private addToRecycledViews(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 759
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mRecycledViews:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 760
    return-void
.end method

.method private setOverScrollEffectPadding(II)V
    .locals 0
    .param p1, "leftPadding"    # I
    .param p2, "i"    # I

    .prologue
    .line 1044
    return-void
.end method

.method private shouldReleaseFromBottomStack()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 579
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 589
    :goto_0
    return v0

    .line 582
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBottomStackReleaseDistance:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    move v0, v2

    .line 584
    goto :goto_0

    .line 586
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    if-nez v0, :cond_2

    move v0, v2

    .line 587
    goto :goto_0

    :cond_2
    move v0, v1

    .line 589
    goto :goto_0
.end method

.method private update()V
    .locals 21

    .prologue
    .line 763
    const-string v19, "MIK"

    const-string v20, "update()"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getChildCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_0

    .line 765
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v17

    .line 766
    .local v17, "v":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->addToRecycledViews(Landroid/view/View;)V

    .line 767
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->recycleView(Landroid/view/View;)V

    .line 764
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 769
    .end local v17    # "v":Landroid/view/View;
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v16

    .line 770
    .local v16, "transitioner":Landroid/animation/LayoutTransition;
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 772
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->removeAllViews()V

    .line 774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v12

    .line 776
    .local v12, "pm":Landroid/content/pm/PackageManager;
    new-instance v19, Landroid/content/Intent;

    const-string v20, "android.intent.action.MAIN"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v20, "android.intent.category.HOME"

    invoke-virtual/range {v19 .. v20}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v19

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v0, v12, v1}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v7

    .line 782
    .local v7, "homeInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mRecycledViews:Ljava/util/HashSet;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .line 783
    .local v13, "recycledViews":Ljava/util/Iterator;, "Ljava/util/Iterator<Landroid/view/View;>;"
    const/4 v8, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->getCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v8, v0, :cond_5

    .line 784
    const/4 v11, 0x0

    .line 785
    .local v11, "old":Landroid/view/View;
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_1

    .line 786
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    .end local v11    # "old":Landroid/view/View;
    check-cast v11, Landroid/view/View;

    .line 787
    .restart local v11    # "old":Landroid/view/View;
    invoke-interface {v13}, Ljava/util/Iterator;->remove()V

    .line 788
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/view/View;->setVisibility(I)V

    .line 790
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v8, v11, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v18

    .line 792
    .local v18, "view":Landroid/view/View;
    new-instance v10, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$4;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$4;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    .line 802
    .local v10, "noOpListener":Landroid/view/View$OnTouchListener;
    new-instance v9, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$5;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v9, v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$5;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;Landroid/view/View;)V

    .line 811
    .local v9, "launchAppListener":Landroid/view/View$OnClickListener;
    const v19, 0x7f0f00df

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 814
    .local v4, "big":Landroid/widget/RelativeLayout;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 815
    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 816
    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 818
    const v19, 0x7f0f00e2

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    .line 819
    .local v14, "thumbnail":Landroid/view/View;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->setClickable(Z)V

    .line 820
    invoke-virtual {v14, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 821
    invoke-virtual {v14, v10}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 825
    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setSoundEffectsEnabled(Z)V

    .line 827
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    .line 829
    .local v6, "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    iget-object v15, v6, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    .line 830
    .local v15, "thumbnailView":Landroid/view/View;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/view/View;->setClickable(Z)V

    .line 834
    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setFocusable(Z)V

    .line 835
    new-instance v19, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$6;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$6;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 845
    new-instance v19, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$7;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$7;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 861
    const v19, 0x7f0f005c

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 862
    .local v3, "appTitle":Landroid/view/View;
    if-eqz v3, :cond_2

    .line 863
    const-string v19, " "

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 865
    :cond_2
    const v19, 0x7f0f00e1

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 866
    .local v5, "deleteView":Landroid/view/View;
    if-eqz v5, :cond_3

    .line 867
    new-instance v19, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$8;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$8;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;Landroid/view/View;)V

    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 873
    :cond_3
    const/16 v19, 0x0

    invoke-virtual/range {v18 .. v19}, Landroid/view/View;->setAlpha(F)V

    .line 874
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->addView(Landroid/view/View;)V

    .line 876
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    move-object/from16 v19, v0

    if-eqz v19, :cond_4

    .line 877
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->addViewCallback(Landroid/view/View;)V

    .line 783
    :cond_4
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1

    .line 880
    .end local v3    # "appTitle":Landroid/view/View;
    .end local v4    # "big":Landroid/widget/RelativeLayout;
    .end local v5    # "deleteView":Landroid/view/View;
    .end local v6    # "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    .end local v9    # "launchAppListener":Landroid/view/View$OnClickListener;
    .end local v10    # "noOpListener":Landroid/view/View$OnTouchListener;
    .end local v11    # "old":Landroid/view/View;
    .end local v14    # "thumbnail":Landroid/view/View;
    .end local v15    # "thumbnailView":Landroid/view/View;
    .end local v18    # "view":Landroid/view/View;
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 882
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 883
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 123
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    .line 124
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 125
    invoke-super {p0, p1, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;I)V

    .line 126
    return-void
.end method

.method public canChildBeDismissed(Landroid/view/View;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 891
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v6, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 892
    .local v2, "index":I
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    .line 893
    .local v0, "h":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->taskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 894
    .local v4, "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 896
    .local v3, "pm":Landroid/content/pm/PackageManager;
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.MAIN"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "android.intent.category.HOME"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6, v3, v5}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 898
    .local v1, "homeInfo":Landroid/content/pm/ActivityInfo;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    if-le v2, v6, :cond_1

    iget v6, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    if-ge v2, v6, :cond_1

    if-eqz v1, :cond_0

    iget-object v6, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v7

    iget-object v7, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    const/4 v5, 0x1

    :cond_1
    return v5
.end method

.method protected createBottomStackInAnimation()Landroid/view/animation/Animation;
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 720
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    return-object v0
.end method

.method protected abstract createBottomStackInAnimationInternal(Landroid/view/View;)Landroid/view/animation/Animation;
.end method

.method protected createBottomStackOutAnimation()Landroid/view/animation/Animation;
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 735
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    return-object v0
.end method

.method protected createTopStackInAnimation()Landroid/view/animation/Animation;
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 713
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    return-object v0
.end method

.method protected createTopStackOutAnimation()Landroid/view/animation/Animation;
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 728
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    return-object v0
.end method

.method public dismissChild(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    .line 903
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 649
    const/4 v1, 0x0

    .line 650
    .local v1, "res":Z
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 651
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrolling:Z

    if-nez v2, :cond_4

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipe:Z

    if-nez v2, :cond_4

    .line 652
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_1

    .line 653
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 654
    .local v0, "ev2":Landroid/view/MotionEvent;
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 655
    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 656
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 698
    .end local v0    # "ev2":Landroid/view/MotionEvent;
    :cond_0
    :goto_0
    return v3

    .line 659
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 660
    if-eqz v1, :cond_2

    .line 661
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipe:Z

    .line 663
    :cond_2
    if-nez v1, :cond_0

    .line 664
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->gd:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    .line 665
    if-eqz v1, :cond_3

    .line 666
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrolling:Z

    goto :goto_0

    .line 668
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 672
    :cond_4
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 673
    .restart local v0    # "ev2":Landroid/view/MotionEvent;
    invoke-virtual {v0, v5}, Landroid/view/MotionEvent;->setAction(I)V

    .line 674
    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 675
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrolling:Z

    if-eqz v2, :cond_6

    .line 676
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 678
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->isFinished()Z

    move-result v2

    if-nez v2, :cond_5

    .line 679
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v2, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 683
    :cond_5
    :pswitch_1
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrolling:Z

    .line 684
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mFingerOnDisplay:Z

    .line 685
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->restoreRotation()V

    .line 688
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->gd:Landroid/view/GestureDetector;

    invoke-virtual {v2, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 690
    :cond_6
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipe:Z

    if-eqz v2, :cond_0

    .line 691
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v3, :cond_7

    .line 692
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipe:Z

    .line 694
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    invoke-virtual {v2, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 676
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method doScroll(F)Z
    .locals 1
    .param p1, "d"    # F

    .prologue
    .line 342
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(FI)Z

    move-result v0

    return v0
.end method

.method doScroll(FI)Z
    .locals 25
    .param p1, "d"    # F
    .param p2, "index"    # I

    .prologue
    .line 346
    const-string v21, "MIK"

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const-string v23, "doScroll:d="

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    move-object/from16 v0, v22

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 347
    const/4 v4, 0x0

    .line 348
    .local v4, "b":Z
    const/4 v8, 0x0

    .line 350
    .local v8, "isForceTopStackAnimation":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-nez v21, :cond_0

    .line 351
    const-string v21, "MIK"

    const-string v22, "finish scroll"

    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const/16 v21, 0x0

    .line 565
    :goto_0
    return v21

    .line 355
    :cond_0
    const/16 v21, 0x0

    cmpl-float v21, p1, v21

    if-lez v21, :cond_b

    .line 356
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-lez v21, :cond_5

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    move/from16 v21, v0

    if-eqz v21, :cond_5

    .line 357
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    .line 358
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->restoreRotation()V

    .line 394
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-lez v21, :cond_2

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 396
    .local v18, "v":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v10, v21, v22

    .line 397
    .local v10, "rpos":F
    const/high16 v21, 0x3f800000    # 1.0f

    const/high16 v22, 0x40200000    # 2.5f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollInterpolator:Landroid/view/animation/Interpolator;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v23

    mul-float v22, v22, v23

    add-float v6, v21, v22

    .line 398
    .local v6, "g":F
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    mul-float v22, p1, v6

    add-float v21, v21, v22

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateDownDistance:F

    const/high16 v23, 0x3f800000    # 1.0f

    add-float v22, v22, v23

    cmpl-float v21, v21, v22

    if-lez v21, :cond_2

    .line 399
    sget v21, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateDownDistance:F

    const/high16 v22, 0x3f800000    # 1.0f

    add-float v21, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v22

    sub-float v21, v21, v22

    div-float p1, v21, v6

    .line 470
    .end local v6    # "g":F
    .end local v10    # "rpos":F
    .end local v18    # "v":Landroid/view/View;
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v17, v21, 0x1

    .line 471
    .local v17, "startIndex":I
    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 472
    .local v5, "endIndex":I
    const/16 v21, -0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-eq v0, v1, :cond_3

    .line 473
    const/16 v21, 0x0

    cmpg-float v21, p1, v21

    if-gez v21, :cond_16

    .line 474
    move/from16 v17, p2

    .line 483
    :cond_3
    :goto_3
    move/from16 v7, v17

    .local v7, "i":I
    :goto_4
    if-ge v7, v5, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v7, v0, :cond_18

    .line 484
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 485
    .restart local v18    # "v":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v10, v21, v22

    .line 486
    .restart local v10    # "rpos":F
    const/high16 v21, 0x3f800000    # 1.0f

    const/high16 v22, 0x40200000    # 2.5f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollInterpolator:Landroid/view/animation/Interpolator;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v23

    mul-float v22, v22, v23

    add-float v21, v21, v22

    mul-float v21, v21, p1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setNewPosition(Landroid/view/View;F)Z

    move-result v9

    .line 488
    .local v9, "res":Z
    if-nez v4, :cond_4

    if-eqz v9, :cond_17

    :cond_4
    const/4 v4, 0x1

    .line 483
    :goto_5
    add-int/lit8 v7, v7, 0x1

    goto :goto_4

    .line 359
    .end local v5    # "endIndex":I
    .end local v7    # "i":I
    .end local v9    # "res":Z
    .end local v10    # "rpos":F
    .end local v17    # "startIndex":I
    .end local v18    # "v":Landroid/view/View;
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-lez v21, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateDownDistance:F

    cmpl-float v21, v21, v22

    if-lez v21, :cond_9

    .line 363
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->ROTATE_SPEED:F

    sub-float v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    .line 364
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    .line 365
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mFingerOnDisplay:Z

    move/from16 v21, v0

    if-eqz v21, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpg-float v21, v21, v22

    if-gez v21, :cond_6

    .line 366
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotation_restorer:Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 368
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    move-result v21

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mMaxRotateAngle:I

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    cmpl-float v21, v21, v22

    if-lez v21, :cond_7

    .line 369
    sget v21, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mMaxRotateAngle:I

    move/from16 v0, v21

    neg-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    .line 370
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 372
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v7, v21, 0x1

    .restart local v7    # "i":I
    :goto_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_8

    .line 375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 376
    .restart local v18    # "v":Landroid/view/View;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setRotation(Landroid/view/View;F)V

    .line 372
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 378
    .end local v18    # "v":Landroid/view/View;
    :cond_8
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 379
    .end local v7    # "i":I
    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->shouldReleaseFromTopStack()Z

    move-result v21

    if-eqz v21, :cond_1

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 385
    .restart local v18    # "v":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->createTopStackOutAnimation()Landroid/view/animation/Animation;

    move-result-object v15

    .line 386
    .local v15, "set":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    .line 387
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    const/16 v22, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-eq v0, v1, :cond_a

    .line 388
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/View;

    .line 389
    .local v20, "v2":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->optShow(Landroid/view/View;)V

    .line 391
    .end local v20    # "v2":Landroid/view/View;
    :cond_a
    const v21, 0x7f0f00e1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x1

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setEnabled(Z)V

    .line 392
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_1

    .line 402
    .end local v15    # "set":Landroid/view/animation/Animation;
    .end local v18    # "v":Landroid/view/View;
    :cond_b
    const/16 v21, 0x0

    cmpg-float v21, p1, v21

    if-gez v21, :cond_15

    .line 403
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpg-float v21, v21, v22

    if-gez v21, :cond_d

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    move/from16 v21, v0

    if-eqz v21, :cond_d

    .line 404
    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    .line 405
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->restoreRotation()V

    .line 458
    :cond_c
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-lez v21, :cond_2

    .line 459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 460
    .restart local v18    # "v":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    div-float v10, v21, v22

    .line 461
    .restart local v10    # "rpos":F
    const/high16 v21, 0x3f800000    # 1.0f

    const/high16 v22, 0x40200000    # 2.5f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollInterpolator:Landroid/view/animation/Interpolator;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-interface {v0, v10}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v23

    mul-float v22, v22, v23

    add-float v6, v21, v22

    .line 462
    .restart local v6    # "g":F
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    mul-float v22, p1, v6

    add-float v21, v21, v22

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateUpDistance:F

    const/high16 v23, 0x3f800000    # 1.0f

    sub-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-gez v21, :cond_2

    .line 463
    sget v21, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateUpDistance:F

    const/high16 v22, 0x3f800000    # 1.0f

    sub-float v21, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v22

    sub-float v21, v21, v22

    div-float p1, v21, v6

    goto/16 :goto_2

    .line 406
    .end local v6    # "g":F
    .end local v10    # "rpos":F
    .end local v18    # "v":Landroid/view/View;
    :cond_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    if-lez v21, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mBeginRotateUpDistance:F

    cmpg-float v21, v21, v22

    if-gez v21, :cond_13

    .line 410
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    if-gez v21, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    const/16 v22, 0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_12

    .line 411
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->ROTATE_SPEED:F

    add-float v21, v21, v22

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    .line 412
    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->should_clean_rotation:Z

    .line 413
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mFingerOnDisplay:Z

    move/from16 v21, v0

    if-eqz v21, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    const/16 v22, 0x0

    cmpl-float v21, v21, v22

    if-lez v21, :cond_f

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotation_restorer:Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 417
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Math;->abs(F)F

    move-result v21

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mMaxRotateAngle:I

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    cmpl-float v21, v21, v22

    if-lez v21, :cond_10

    .line 418
    sget v21, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mMaxRotateAngle:I

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    .line 420
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 422
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v7, v21, 0x1

    .restart local v7    # "i":I
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_11

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 424
    .restart local v18    # "v":Landroid/view/View;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotated_angle:F

    move/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setRotation(Landroid/view/View;F)V

    .line 422
    add-int/lit8 v7, v7, 0x1

    goto :goto_8

    .line 427
    .end local v18    # "v":Landroid/view/View;
    :cond_11
    const/16 v21, 0x0

    goto/16 :goto_0

    .line 429
    .end local v7    # "i":I
    :cond_12
    const/4 v8, 0x1

    goto/16 :goto_7

    .line 432
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->shouldReleaseFromBottomStack()Z

    move-result v21

    if-eqz v21, :cond_c

    .line 435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 436
    .restart local v18    # "v":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->optShow(Landroid/view/View;)V

    .line 437
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->createBottomStackOutAnimation()Landroid/view/animation/Animation;

    move-result-object v15

    .line 438
    .restart local v15    # "set":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 440
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v7, v21, 0x1

    .restart local v7    # "i":I
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_14

    .line 441
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/View;

    .line 442
    .local v19, "v1":Landroid/view/View;
    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    sub-int v22, v22, v7

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_DIFF:F

    mul-float v22, v22, v23

    sub-float v11, v21, v22

    .line 444
    .local v11, "scaleCurrent":F
    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    sub-int v22, v22, v7

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_DIFF:F

    mul-float v22, v22, v23

    sub-float v12, v21, v22

    .line 445
    .local v12, "scaleNew":F
    const-string v21, "scaleX"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v11, v22, v23

    const/16 v23, 0x1

    aput v12, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    .line 447
    .local v13, "scalex_anim":Landroid/animation/ObjectAnimator;
    const-string v21, "scaleY"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v11, v22, v23

    const/16 v23, 0x1

    aput v12, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v14

    .line 449
    .local v14, "scaley_anim":Landroid/animation/ObjectAnimator;
    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    .line 450
    .local v16, "set1":Landroid/animation/AnimatorSet;
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v13, v21, v22

    const/16 v22, 0x1

    aput-object v14, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 451
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_ANIMATION_DURATION:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 452
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->stopAnimators(Landroid/view/View;)V

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    invoke-virtual/range {v16 .. v16}, Landroid/animation/AnimatorSet;->start()V

    .line 440
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_9

    .line 456
    .end local v11    # "scaleCurrent":F
    .end local v12    # "scaleNew":F
    .end local v13    # "scalex_anim":Landroid/animation/ObjectAnimator;
    .end local v14    # "scaley_anim":Landroid/animation/ObjectAnimator;
    .end local v16    # "set1":Landroid/animation/AnimatorSet;
    .end local v19    # "v1":Landroid/view/View;
    :cond_14
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_7

    .line 467
    .end local v7    # "i":I
    .end local v15    # "set":Landroid/view/animation/Animation;
    .end local v18    # "v":Landroid/view/View;
    :cond_15
    const/16 v21, 0x1

    goto/16 :goto_0

    .line 476
    .restart local v5    # "endIndex":I
    .restart local v17    # "startIndex":I
    :cond_16
    move/from16 v5, p2

    .line 477
    move/from16 v0, v17

    if-ne v0, v5, :cond_3

    if-lez v17, :cond_3

    .line 478
    add-int/lit8 v17, v17, -0x1

    goto/16 :goto_3

    .line 488
    .restart local v7    # "i":I
    .restart local v9    # "res":Z
    .restart local v10    # "rpos":F
    .restart local v18    # "v":Landroid/view/View;
    :cond_17
    const/4 v4, 0x0

    goto/16 :goto_5

    .line 491
    .end local v9    # "res":Z
    .end local v10    # "rpos":F
    .end local v18    # "v":Landroid/view/View;
    :cond_18
    const/16 v21, -0x1

    move/from16 v0, p2

    move/from16 v1, v21

    if-ne v0, v1, :cond_1e

    .line 492
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v22

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_1b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackStartPosition:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, 0x1

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sget v24, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackDistance:F

    mul-float v23, v23, v24

    add-float v22, v22, v23

    cmpg-float v21, v21, v22

    if-lez v21, :cond_19

    if-eqz v8, :cond_1b

    .line 495
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, 0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 496
    .restart local v18    # "v":Landroid/view/View;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    if-lez v21, :cond_1a

    .line 497
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/view/View;

    .line 498
    .restart local v20    # "v2":Landroid/view/View;
    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->optHide(Landroid/view/View;)V

    .line 500
    .end local v20    # "v2":Landroid/view/View;
    :cond_1a
    const v21, 0x7f0f00e1

    move-object/from16 v0, v18

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v21

    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setEnabled(Z)V

    .line 501
    sget v21, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackStartPosition:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackDistance:F

    mul-float v22, v22, v23

    add-float v21, v21, v22

    const/high16 v22, 0x3f800000    # 1.0f

    add-float v21, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setScrollPosition(Landroid/view/View;F)V

    .line 503
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->stopAnimators(Landroid/view/View;)V

    .line 504
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->createTopStackInAnimation()Landroid/view/animation/Animation;

    move-result-object v15

    .line 505
    .restart local v15    # "set":Landroid/view/animation/Animation;
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 508
    .end local v15    # "set":Landroid/view/animation/Animation;
    .end local v18    # "v":Landroid/view/View;
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v21, v21, -0x1

    if-ltz v21, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->BOTTOM_STACK_BORDER:F

    sub-float v22, v22, v23

    cmpl-float v21, v21, v22

    if-lez v21, :cond_1d

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    invoke-virtual/range {v21 .. v22}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 512
    .restart local v18    # "v":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sget v22, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->BOTTOM_STACK_BORDER:F

    sub-float v21, v21, v22

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setScrollPosition(Landroid/view/View;F)V

    .line 513
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->createBottomStackInAnimation()Landroid/view/animation/Animation;

    move-result-object v15

    .line 515
    .restart local v15    # "set":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    move/from16 v21, v0

    add-int/lit8 v7, v21, 0x1

    :goto_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v7, v0, :cond_1c

    .line 516
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/View;

    .line 517
    .restart local v19    # "v1":Landroid/view/View;
    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    sub-int v22, v22, v7

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_DIFF:F

    mul-float v22, v22, v23

    sub-float v11, v21, v22

    .line 518
    .restart local v11    # "scaleCurrent":F
    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    add-int/lit8 v22, v22, -0x1

    sub-int v22, v22, v7

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_DIFF:F

    mul-float v22, v22, v23

    sub-float v12, v21, v22

    .line 519
    .restart local v12    # "scaleNew":F
    const-string v21, "scaleX"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v11, v22, v23

    const/16 v23, 0x1

    aput v12, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    .line 521
    .restart local v13    # "scalex_anim":Landroid/animation/ObjectAnimator;
    const-string v21, "scaleY"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v11, v22, v23

    const/16 v23, 0x1

    aput v12, v22, v23

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v14

    .line 523
    .restart local v14    # "scaley_anim":Landroid/animation/ObjectAnimator;
    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    .line 524
    .restart local v16    # "set1":Landroid/animation/AnimatorSet;
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v13, v21, v22

    const/16 v22, 0x1

    aput-object v14, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 525
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_ANIMATION_DURATION:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 526
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->stopAnimators(Landroid/view/View;)V

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 528
    invoke-virtual/range {v16 .. v16}, Landroid/animation/AnimatorSet;->start()V

    .line 515
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_a

    .line 530
    .end local v11    # "scaleCurrent":F
    .end local v12    # "scaleNew":F
    .end local v13    # "scalex_anim":Landroid/animation/ObjectAnimator;
    .end local v14    # "scaley_anim":Landroid/animation/ObjectAnimator;
    .end local v16    # "set1":Landroid/animation/AnimatorSet;
    .end local v19    # "v1":Landroid/view/View;
    :cond_1c
    new-instance v21, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$2;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$2;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;Landroid/view/View;)V

    move-object/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 545
    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .end local v15    # "set":Landroid/view/animation/Animation;
    .end local v18    # "v":Landroid/view/View;
    :cond_1d
    move/from16 v21, v4

    .line 565
    goto/16 :goto_0

    .line 549
    :cond_1e
    move/from16 v7, v17

    :goto_b
    if-ge v7, v5, :cond_1d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v21

    move/from16 v0, v21

    if-ge v7, v0, :cond_1d

    .line 550
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Landroid/view/View;

    .line 551
    .restart local v18    # "v":Landroid/view/View;
    invoke-virtual/range {v18 .. v18}, Landroid/view/View;->getScaleX()F

    move-result v11

    .line 552
    .restart local v11    # "scaleCurrent":F
    const/high16 v21, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    move/from16 v22, v0

    sub-int v22, v22, v7

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    sget v23, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_DIFF:F

    mul-float v22, v22, v23

    sub-float v12, v21, v22

    .line 553
    .restart local v12    # "scaleNew":F
    const-string v21, "scaleX"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v11, v22, v23

    const/16 v23, 0x1

    aput v12, v22, v23

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v13

    .line 555
    .restart local v13    # "scalex_anim":Landroid/animation/ObjectAnimator;
    const-string v21, "scaleY"

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput v11, v22, v23

    const/16 v23, 0x1

    aput v12, v22, v23

    move-object/from16 v0, v18

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v14

    .line 557
    .restart local v14    # "scaley_anim":Landroid/animation/ObjectAnimator;
    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    .line 558
    .restart local v16    # "set1":Landroid/animation/AnimatorSet;
    const/16 v21, 0x2

    move/from16 v0, v21

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object v13, v21, v22

    const/16 v22, 0x1

    aput-object v14, v21, v22

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 559
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->Z_ANIMATION_DURATION:J

    move-wide/from16 v22, v0

    move-object/from16 v0, v16

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 560
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->stopAnimators(Landroid/view/View;)V

    .line 561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 562
    invoke-virtual/range {v16 .. v16}, Landroid/animation/AnimatorSet;->start()V

    .line 549
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_b
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v10, 0x0

    .line 972
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 974
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    if-eqz v0, :cond_2

    .line 975
    iget v13, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPaddingLeft:I

    .line 976
    .local v13, "paddingLeft":I
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->isPaddingOffsetRequired()Z

    move-result v12

    .line 977
    .local v12, "offsetRequired":Z
    if-eqz v12, :cond_0

    .line 978
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getLeftPaddingOffset()I

    move-result v0

    add-int/2addr v13, v0

    .line 981
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollX:I

    add-int v2, v0, v13

    .line 982
    .local v2, "left":I
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mRight:I

    add-int/2addr v0, v2

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mLeft:I

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPaddingRight:I

    sub-int/2addr v0, v1

    sub-int v3, v0, v13

    .line 983
    .local v3, "right":I
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollY:I

    invoke-virtual {p0, v12}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getFadeTop(Z)I

    move-result v1

    add-int v4, v0, v1

    .line 984
    .local v4, "top":I
    invoke-virtual {p0, v12}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getFadeHeight(Z)I

    move-result v0

    add-int v5, v4, v0

    .line 986
    .local v5, "bottom":I
    if-eqz v12, :cond_1

    .line 987
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getRightPaddingOffset()I

    move-result v0

    add-int/2addr v3, v0

    .line 988
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getBottomPaddingOffset()I

    move-result v0

    add-int/2addr v5, v0

    .line 990
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    iget v6, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollX:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrollY:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getTopFadingEdgeStrength()F

    move-result v8

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getBottomFadingEdgeStrength()F

    move-result v9

    move-object v1, p1

    move v11, v10

    invoke-virtual/range {v0 .. v11}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->drawCallback(Landroid/graphics/Canvas;IIIIIIFFFF)V

    .line 994
    .end local v2    # "left":I
    .end local v3    # "right":I
    .end local v4    # "top":I
    .end local v5    # "bottom":I
    .end local v12    # "offsetRequired":Z
    .end local v13    # "paddingLeft":I
    :cond_2
    return-void
.end method

.method public getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;
    .locals 7
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 938
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollX()I

    move-result v6

    int-to-float v6, v6

    add-float v3, v5, v6

    .line 939
    .local v3, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollY()I

    move-result v6

    int-to-float v6, v6

    add-float v4, v5, v6

    .line 940
    .local v4, "y":F
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 941
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v5

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    cmpg-float v5, v3, v5

    if-gez v5, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v5

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getY()F

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    cmpg-float v5, v4, v5

    if-gez v5, :cond_0

    .line 954
    .end local v2    # "v":Landroid/view/View;
    :goto_0
    return-object v2

    .line 946
    .restart local v2    # "v":Landroid/view/View;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 947
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 948
    .local v1, "item":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v5

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    cmpg-float v5, v3, v5

    if-gez v5, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v5

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v5

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    cmpg-float v5, v4, v5

    if-gez v5, :cond_1

    move-object v2, v1

    .line 951
    goto :goto_0

    .line 946
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 954
    .end local v1    # "item":Landroid/view/View;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getChildContentView(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 967
    const v0, 0x7f0f00de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getHorizontalFadingEdgeLength()I
    .locals 1

    .prologue
    .line 1007
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    if-eqz v0, :cond_0

    .line 1008
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->getHorizontalFadingEdgeLengthCallback()I

    move-result v0

    .line 1010
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/RelativeLayout;->getHorizontalFadingEdgeLength()I

    move-result v0

    goto :goto_0
.end method

.method public getNextChild(I)Landroid/view/View;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 958
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 959
    .local v0, "size":I
    const/4 v1, 0x1

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    if-gt v0, v1, :cond_0

    .line 960
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 962
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_0
.end method

.method protected abstract getScrollDistance(FF)F
.end method

.method protected abstract getScrollPosition(Landroid/view/View;)F
.end method

.method protected abstract getScrollSize()I
.end method

.method protected abstract getScrollVelocity(FF)F
.end method

.method public getVerticalFadingEdgeLength()I
    .locals 1

    .prologue
    .line 998
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    if-eqz v0, :cond_0

    .line 999
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->getVerticalFadingEdgeLengthCallback()I

    move-result v0

    .line 1001
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/RelativeLayout;->getVerticalFadingEdgeLength()I

    move-result v0

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 4

    .prologue
    .line 1026
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    if-eqz v0, :cond_0

    .line 1027
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPerformanceHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->isHardwareAccelerated()Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->onAttachedToWindowCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;Landroid/widget/LinearLayout;Z)V

    .line 1030
    :cond_0
    return-void
.end method

.method public onBeginDrag(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v0, 0x1

    .line 930
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipe:Z

    .line 931
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->requestDisallowInterceptTouchEvent(Z)V

    .line 932
    return-void
.end method

.method public onChildDismissed(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 906
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->addToRecycledViews(Landroid/view/View;)V

    .line 907
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 908
    .local v0, "index":I
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    if-ge v0, v3, :cond_0

    .line 909
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 910
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    if-gt v0, v3, :cond_0

    .line 911
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    .line 915
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v1

    .line 916
    .local v1, "pos":F
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->removeView(Landroid/view/View;)V

    .line 917
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 918
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 919
    .local v2, "size":I
    const/4 v3, 0x1

    if-lt v0, v3, :cond_2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    if-gt v2, v3, :cond_2

    .line 920
    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;

    sget v4, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAnimationScrollStep:I

    invoke-direct {v3, p0, v0, v1, v4}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;IFI)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->post(Ljava/lang/Runnable;)Z

    .line 924
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    invoke-interface {v3, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;->handleSwipe(Ljava/lang/Object;)V

    .line 925
    return-void

    .line 921
    :cond_2
    if-eq v0, v2, :cond_1

    if-eqz v2, :cond_1

    .line 922
    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;

    sget v4, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAnimationScrollStep:I

    neg-int v4, v4

    invoke-direct {v3, p0, v0, v1, v4}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;IFI)V

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 1034
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1035
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 1036
    .local v0, "densityScale":F
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    invoke-virtual {v2, v0}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->setDensityScale(F)V

    .line 1037
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    int-to-float v1, v2

    .line 1039
    .local v1, "pagingTouchSlop":F
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    invoke-virtual {v2, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->setPagingTouchSlop(F)V

    .line 1040
    return-void
.end method

.method public onDragCancelled(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 935
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 1016
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 1017
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setScrollbarFadingEnabled(Z)V

    .line 1019
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0116

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1021
    .local v0, "leftPadding":I
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setOverScrollEffectPadding(II)V

    .line 1022
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1048
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 1049
    const-string v0, "MIK"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1050
    return-void
.end method

.method public onTasksLoaded()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 1085
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 1086
    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1087
    .local v4, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v5

    int-to-float v5, v5

    sget v6, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->BOTTOM_STACK_BORDER:F

    sub-float/2addr v5, v6

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setScrollPosition(Landroid/view/View;F)V

    .line 1088
    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->createBottomStackInAnimationInternal(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1089
    .local v0, "a":Landroid/view/animation/Animation;
    invoke-virtual {v0, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1090
    invoke-virtual {v4, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1091
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 1092
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 1093
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1094
    .local v3, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollSize()I

    move-result v5

    int-to-float v5, v5

    sget v6, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->BOTTOM_STACK_BORDER:F

    sub-float/2addr v5, v6

    invoke-virtual {p0, v3, v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setScrollPosition(Landroid/view/View;F)V

    .line 1095
    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->createBottomStackInAnimationInternal(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v1

    .line 1096
    .local v1, "aa":Landroid/view/animation/Animation;
    invoke-virtual {v1, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1097
    invoke-virtual {v3, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1098
    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    add-int/lit8 v5, v5, -0x1

    iput v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 1092
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1100
    .end local v1    # "aa":Landroid/view/animation/Animation;
    .end local v3    # "v":Landroid/view/View;
    :cond_0
    iput v7, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    .line 1101
    new-instance v5, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;

    sget v6, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackStartPosition:F

    invoke-direct {v5, p0, v7, v6, v8}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;IFI)V

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->run()V

    .line 1102
    const/4 v2, 0x0

    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 1103
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 1104
    .restart local v3    # "v":Landroid/view/View;
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v3, v5}, Landroid/view/View;->setAlpha(F)V

    .line 1102
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1107
    .end local v0    # "a":Landroid/view/animation/Animation;
    .end local v2    # "i":I
    .end local v3    # "v":Landroid/view/View;
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 1054
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onVisibilityChanged(Landroid/view/View;I)V

    .line 1056
    if-nez p2, :cond_0

    if-ne p1, p0, :cond_0

    .line 1057
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$9;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->post(Ljava/lang/Runnable;)Z

    .line 1063
    :cond_0
    return-void
.end method

.method protected abstract optHide(Landroid/view/View;)V
.end method

.method protected abstract optShow(Landroid/view/View;)V
.end method

.method public removeAllViews()V
    .locals 1

    .prologue
    .line 747
    invoke-super {p0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    .line 748
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 749
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    .line 750
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->bottomStackIndex:I

    .line 751
    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 887
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->dismissChild(Landroid/view/View;)V

    .line 888
    return-void
.end method

.method restoreRotation()V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotation_restorer:Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 338
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->rotation_restorer:Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;->init()Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRestoreRotationRunnable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->post(Ljava/lang/Runnable;)Z

    .line 339
    return-void
.end method

.method public setAdapter(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;)V
    .locals 3
    .param p1, "adapter"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    .prologue
    .line 1066
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    .line 1067
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    new-instance v2, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$10;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$10;-><init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1077
    const/4 v1, 0x3

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mNumItemsInOneScreenful:I

    .line 1079
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mNumItemsInOneScreenful:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 1080
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-virtual {v1, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->createView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->addToRecycledViews(Landroid/view/View;)V

    .line 1079
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1082
    :cond_0
    return-void
.end method

.method public setCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    .prologue
    .line 1120
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    .line 1121
    return-void
.end method

.method public setLayoutTransition(Landroid/animation/LayoutTransition;)V
    .locals 0
    .param p1, "transition"    # Landroid/animation/LayoutTransition;

    .prologue
    .line 1117
    return-void
.end method

.method public setMinSwipeAlpha(F)V
    .locals 1
    .param p1, "minAlpha"    # F

    .prologue
    .line 755
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->setMinAlpha(F)V

    .line 756
    return-void
.end method

.method setNewPosition(Landroid/view/View;F)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "d"    # F

    .prologue
    .line 617
    const/4 v0, 0x1

    .line 618
    .local v0, "ret":Z
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v1

    add-float/2addr p2, v1

    .line 619
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->setScrollPosition(Landroid/view/View;F)V

    .line 620
    return v0
.end method

.method protected abstract setRotation(Landroid/view/View;F)V
.end method

.method protected abstract setScrollPosition(Landroid/view/View;F)V
.end method

.method protected shouldReleaseFromTopStack()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 593
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 611
    :goto_0
    return v0

    .line 596
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    add-int/lit8 v0, v0, 0x1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mTopStackReleaseDistance:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_1

    move v0, v2

    .line 606
    goto :goto_0

    .line 607
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->topStackIndex:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_2

    move v0, v2

    .line 609
    goto :goto_0

    :cond_2
    move v0, v1

    .line 611
    goto :goto_0
.end method

.method protected stopAnimators(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 569
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 570
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .line 571
    .local v0, "a":Landroid/animation/Animator;
    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 572
    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 574
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->animators:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    .end local v0    # "a":Landroid/animation/Animator;
    :cond_1
    return-void
.end method
