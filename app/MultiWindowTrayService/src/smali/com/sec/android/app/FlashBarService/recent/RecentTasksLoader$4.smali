.class Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;
.super Landroid/os/AsyncTask;
.source "RecentTasksLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->loadThumbnailsAndIconsInBackground(Ljava/util/concurrent/BlockingQueue;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

.field final synthetic val$tasksWaitingForThumbnails:Ljava/util/concurrent/BlockingQueue;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Ljava/util/concurrent/BlockingQueue;)V
    .locals 0

    .prologue
    .line 563
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->val$tasksWaitingForThumbnails:Ljava/util/concurrent/BlockingQueue;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 563
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 579
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    .line 580
    .local v1, "origPri":I
    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    .line 583
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 602
    :goto_1
    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    .line 603
    const/4 v3, 0x0

    return-object v3

    .line 586
    :cond_0
    const/4 v2, 0x0

    .line 587
    .local v2, "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    :goto_2
    if-nez v2, :cond_1

    .line 589
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->val$tasksWaitingForThumbnails:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v3}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 593
    :cond_1
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->isNull()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 594
    new-array v3, v5, [Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    aput-object v2, v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->publishProgress([Ljava/lang/Object;)V

    goto :goto_1

    .line 597
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {v3, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->loadThumbnailAndIcon(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V

    .line 599
    new-array v3, v5, [Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    aput-object v2, v3, v4

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->publishProgress([Ljava/lang/Object;)V

    goto :goto_0

    .line 590
    :catch_0
    move-exception v3

    goto :goto_2
.end method

.method protected varargs onProgressUpdate([Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V
    .locals 3
    .param p1, "values"    # [Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 566
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 567
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 568
    .local v0, "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->isNull()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    sget-object v2, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;->LOADED:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    # setter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mState:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$902(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$State;

    .line 576
    .end local v0    # "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    :cond_0
    :goto_0
    return-void

    .line 571
    .restart local v0    # "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 572
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onTaskThumbnailLoaded(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V

    goto :goto_0
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 563
    check-cast p1, [Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$4;->onProgressUpdate([Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V

    return-void
.end method
