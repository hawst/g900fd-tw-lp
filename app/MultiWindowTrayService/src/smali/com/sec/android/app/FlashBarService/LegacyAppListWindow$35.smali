.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

.field final synthetic val$bStart:Z

.field final synthetic val$finishtray:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZ)V
    .locals 0

    .prologue
    .line 3294
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->val$finishtray:Z

    iput-boolean p3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->val$bStart:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 5
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v4, 0x0

    .line 3296
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3297
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    .line 3298
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListOverlayHelp:Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;)Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;

    .line 3300
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v1, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 3301
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 3302
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    if-lez v1, :cond_1

    .line 3303
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 3305
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 3306
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->val$finishtray:Z

    if-nez v1, :cond_2

    .line 3307
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->val$bStart:Z

    if-eqz v1, :cond_3

    .line 3308
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 3318
    :cond_2
    :goto_0
    return-void

    .line 3315
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$35;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    const/4 v3, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3319
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3320
    return-void
.end method
