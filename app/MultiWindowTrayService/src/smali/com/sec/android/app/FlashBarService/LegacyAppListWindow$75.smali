.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4480
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 4482
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4484
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v5, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4485
    const-string v5, "LegacyAppListWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "App icon long clicked :: appIconIndex = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mbAnimating = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", mbEditmode = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4488
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/sec/android/app/FlashBarService/AppListController;->isPenWindowOnly(I)Z

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4490
    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "text/uri-list"

    aput-object v5, v4, v6

    .line 4491
    .local v4, "strs":[Ljava/lang/String;
    new-instance v3, Landroid/content/ClipData$Item;

    const-string v5, ""

    invoke-direct {v3, v5}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 4492
    .local v3, "item":Landroid/content/ClipData$Item;
    new-instance v0, Landroid/content/ClipData;

    const-string v5, "appIcon"

    invoke-direct {v0, v5, v4, v3}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 4494
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v5

    const/4 v8, -0x1

    if-le v5, v8, :cond_1

    .line 4495
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSupportMultiInstance:Z

    const/16 v10, 0x64

    invoke-virtual {v5, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v2

    .line 4496
    .local v2, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    move v5, v6

    .line 4530
    .end local v2    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :goto_0
    return v5

    .line 4499
    .restart local v2    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_0
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v8, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 4500
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntents:Ljava/util/List;
    invoke-static {v5, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Ljava/util/List;)Ljava/util/List;

    .line 4501
    const-string v5, "LegacyAppListWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "App icon long clicked :: packageName: = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4502
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 4505
    if-eqz v0, :cond_1

    .line 4507
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$75;Landroid/view/View;)V

    .line 4527
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v5, 0x0

    invoke-virtual {p1, v0, v1, v5, v6}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    .end local v2    # "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_1
    move v5, v7

    .line 4530
    goto :goto_0
.end method
