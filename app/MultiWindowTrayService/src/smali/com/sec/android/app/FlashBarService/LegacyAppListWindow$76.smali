.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4534
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v11, 0x64

    const/16 v10, 0xca

    .line 4536
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 4564
    :cond_0
    :goto_0
    const/4 v6, 0x0

    return v6

    .line 4539
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 4540
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v6, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;)Landroid/view/View;

    .line 4541
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v7

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13002(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 4542
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v3

    .line 4543
    .local v3, "mainFrameAnim":Landroid/view/animation/Animation;
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v3}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 4544
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v8, 0xc8

    invoke-virtual {v6, v10, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 4546
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->applyShadePressEffect(Landroid/view/MotionEvent;Landroid/view/View;I)V
    invoke-static {v6, p2, p1, v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/MotionEvent;Landroid/view/View;I)V

    goto :goto_0

    .line 4549
    .end local v3    # "mainFrameAnim":Landroid/view/animation/Animation;
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    .line 4550
    .local v4, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    .line 4551
    .local v5, "y":F
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/MotionEvent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    sub-float v6, v4, v6

    float-to-int v0, v6

    .line 4552
    .local v0, "deltaX":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/MotionEvent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    sub-float v6, v5, v6

    float-to-int v1, v6

    .line 4553
    .local v1, "deltaY":I
    mul-int v6, v0, v0

    mul-int v7, v1, v1

    add-int v2, v6, v7

    .line 4554
    .local v2, "distance":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragZone:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    if-le v2, v6, :cond_0

    .line 4555
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 4560
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    .end local v2    # "distance":I
    .end local v4    # "x":F
    .end local v5    # "y":F
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->applyShadePressEffect(Landroid/view/MotionEvent;Landroid/view/View;I)V
    invoke-static {v6, p2, p1, v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/MotionEvent;Landroid/view/View;I)V

    .line 4561
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$76;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v6, v10}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 4536
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
