.class public Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;
.super Ljava/lang/Object;
.source "RecentsHelper.java"


# instance fields
.field private final attrs:[Ljava/lang/String;

.field private mHandles:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mInited:Z

.field private mProgram:I

.field private mTextureHandle:I

.field private mTextureHeight:I

.field private mTextureWidth:I

.field private textureSpaceCounter:F

.field private final uniforms:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "u_MVPMatrix"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->uniforms:[Ljava/lang/String;

    .line 42
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "a_Position"

    aput-object v1, v0, v4

    const-string v1, "a_Color"

    aput-object v1, v0, v2

    const/4 v1, 0x2

    const-string v2, "a_TexCoordinate"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->attrs:[Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mHandles:Ljava/util/HashMap;

    .line 75
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mInited:Z

    .line 76
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    .line 77
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHandle:I

    .line 78
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureWidth:I

    .line 79
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHeight:I

    .line 174
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->textureSpaceCounter:F

    return-void
.end method

.method public static deleteTexture(I)V
    .locals 3
    .param p0, "handle"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 205
    new-array v0, v2, [I

    aput p0, v0, v1

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 206
    return-void
.end method

.method public static getFloatFromResource(Landroid/content/res/Resources;I)F
    .locals 2
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "id"    # I

    .prologue
    .line 232
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 233
    .local v0, "t":Landroid/util/TypedValue;
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 234
    invoke-virtual {v0}, Landroid/util/TypedValue;->getFloat()F

    move-result v1

    return v1
.end method

.method public static loadShader(ILjava/lang/String;)I
    .locals 4
    .param p0, "type"    # I
    .param p1, "shaderCode"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 130
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 133
    .local v1, "shader":I
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 134
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 137
    const/4 v2, 0x1

    new-array v0, v2, [I

    .line 138
    .local v0, "compileStatus":[I
    const v2, 0x8b81

    invoke-static {v1, v2, v0, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 141
    aget v2, v0, v3

    if-nez v2, :cond_0

    .line 143
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 144
    const/4 v1, 0x0

    .line 147
    :cond_0
    if-nez v1, :cond_1

    .line 148
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Shader compilation failed!"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 151
    :cond_1
    return v1
.end method

.method public static loadTexture(Landroid/content/Context;I)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 165
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 166
    .local v2, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v3, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 169
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/graphics/Bitmap;)I

    move-result v1

    .line 170
    .local v1, "id":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 171
    return v1
.end method

.method public static loadTexture(Landroid/graphics/Bitmap;)I
    .locals 5
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/16 v4, 0x2601

    const/4 v1, 0x1

    const/16 v3, 0xde1

    const/4 v2, 0x0

    .line 176
    new-array v0, v1, [I

    .line 178
    .local v0, "textureHandle":[I
    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 180
    aget v1, v0, v2

    if-eqz v1, :cond_0

    .line 183
    aget v1, v0, v2

    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 186
    const/16 v1, 0x2801

    invoke-static {v3, v1, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 187
    const/16 v1, 0x2800

    invoke-static {v3, v1, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 190
    invoke-static {v3, v2, p0, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 196
    :cond_0
    aget v1, v0, v2

    if-nez v1, :cond_1

    .line 198
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Error loading texture."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 201
    :cond_1
    aget v1, v0, v2

    return v1
.end method

.method public static loadToBuffer([F)Ljava/nio/FloatBuffer;
    .locals 3
    .param p0, "data"    # [F

    .prologue
    .line 209
    array-length v1, p0

    mul-int/lit8 v1, v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 211
    .local v0, "buffer":Ljava/nio/FloatBuffer;
    invoke-virtual {v0, p0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 212
    return-object v0
.end method


# virtual methods
.method protected finalize()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 238
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHandle:I

    if-eq v0, v1, :cond_0

    .line 239
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHandle:I

    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->deleteTexture(I)V

    .line 240
    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHandle:I

    .line 242
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 243
    return-void
.end method

.method public getLocation(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mHandles:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getProgram()I
    .locals 2

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mInited:Z

    if-eqz v0, :cond_0

    .line 121
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    return v0

    .line 123
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Helper should be inited first!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTextureHandle()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHandle:I

    return v0
.end method

.method public getTextureHeight()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHeight:I

    return v0
.end method

.method public getTextureWidth()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureWidth:I

    return v0
.end method

.method public init(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v11, -0x1

    .line 82
    const v9, 0x8b31

    const-string v10, "attribute vec4 a_Position;attribute vec4 a_Color;attribute vec2 a_TexCoordinate;uniform mat4 u_MVPMatrix;varying vec4 v_Color;varying vec2 v_TexCoordinate;void main() {v_Color = a_Color;v_TexCoordinate = a_TexCoordinate;gl_Position = u_MVPMatrix * a_Position;}"

    invoke-static {v9, v10}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadShader(ILjava/lang/String;)I

    move-result v8

    .line 83
    .local v8, "vertexShader":I
    const v9, 0x8b30

    const-string v10, "precision highp float;varying vec4 v_Color;varying vec2 v_TexCoordinate;uniform sampler2D u_Texture;void main() {vec4 tex_color = texture2D(u_Texture, v_TexCoordinate);tex_color.a = tex_color.a * v_Color.a;gl_FragColor = tex_color;}"

    invoke-static {v9, v10}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadShader(ILjava/lang/String;)I

    move-result v2

    .line 85
    .local v2, "fragmentShader":I
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    .line 86
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    invoke-static {v9, v8}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 87
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    invoke-static {v9, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 88
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    invoke-static {v9}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 91
    new-array v5, v13, [I

    .line 92
    .local v5, "linkStatus":[I
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    const v10, 0x8b82

    invoke-static {v9, v10, v5, v12}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 95
    aget v9, v5, v12

    if-nez v9, :cond_0

    .line 97
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    invoke-static {v9}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 98
    iput v11, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    .line 101
    :cond_0
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    if-ne v9, v11, :cond_1

    .line 102
    new-instance v9, Ljava/lang/RuntimeException;

    const-string v10, "Program linking failed!"

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->uniforms:[Ljava/lang/String;

    .local v1, "arr$":[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_2

    aget-object v7, v1, v3

    .line 106
    .local v7, "u":Ljava/lang/String;
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    invoke-static {v9, v7}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v6

    .line 107
    .local v6, "loc":I
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mHandles:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v7, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 109
    .end local v6    # "loc":I
    .end local v7    # "u":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->attrs:[Ljava/lang/String;

    array-length v4, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_3

    aget-object v0, v1, v3

    .line 110
    .local v0, "a":Ljava/lang/String;
    iget v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mProgram:I

    invoke-static {v9, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v6

    .line 111
    .restart local v6    # "loc":I
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mHandles:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v0, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 114
    .end local v0    # "a":Ljava/lang/String;
    .end local v6    # "loc":I
    :cond_3
    const v9, 0x7f020059

    invoke-virtual {p0, p1, v9}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTextureInternal(Landroid/content/Context;I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHandle:I

    .line 116
    iput-boolean v13, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mInited:Z

    .line 117
    return-void
.end method

.method public loadTextureInternal(Landroid/content/Context;I)I
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resId"    # I

    .prologue
    .line 155
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 156
    .local v1, "options":Landroid/graphics/BitmapFactory$Options;
    const/4 v2, 0x0

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 158
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, p2, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 159
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureWidth:I

    .line 160
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->mTextureHeight:I

    .line 161
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/graphics/Bitmap;)I

    move-result v2

    return v2
.end method
