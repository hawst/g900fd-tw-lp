.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1824
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1826
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->changeTaskToFull(I)V

    .line 1827
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1828
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->cancelHideButtonTimer()V

    .line 1829
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->forceHideInputMethod()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 1830
    return-void
.end method
