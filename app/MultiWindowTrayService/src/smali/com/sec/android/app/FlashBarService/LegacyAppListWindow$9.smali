.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;
.super Landroid/os/Handler;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 777
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 779
    iget v5, p1, Landroid/os/Message;->what:I

    sparse-switch v5, :sswitch_data_0

    .line 830
    :goto_0
    return-void

    .line 781
    :sswitch_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v5, v6, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    goto :goto_0

    .line 785
    :sswitch_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/View;

    move-result-object v6

    invoke-interface {v5, v6}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    goto :goto_0

    .line 789
    :sswitch_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationRecentIconAppearByHomeKey()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_0

    .line 793
    :sswitch_3
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeRecentApp()V

    .line 794
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationRecentIcon()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$2900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto :goto_0

    .line 798
    :sswitch_4
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailType:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V

    goto :goto_0

    .line 802
    :sswitch_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAvailableTemplateText()Ljava/lang/CharSequence;

    move-result-object v7

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/EditText;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v6, v7, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 806
    :sswitch_6
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v6

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 807
    :catch_0
    move-exception v2

    .line 808
    .local v2, "e":Ljava/lang/Exception;
    const-string v5, "LegacyAppListWindow"

    const-string v6, "Exception in mInputMethodChangedReceiver."

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 812
    .end local v2    # "e":Ljava/lang/Exception;
    :sswitch_7
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 813
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v5, "visible"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 814
    .local v4, "visible":Z
    const-string v5, "focusVisible"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 815
    .local v3, "focusVisible":Z
    const-string v5, "animating"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 816
    .local v0, "animating":Z
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v5, v4, v3, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    goto/16 :goto_0

    .line 819
    .end local v0    # "animating":Z
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v3    # "focusVisible":Z
    .end local v4    # "visible":Z
    :sswitch_8
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->moveViewPagerLeft()V

    goto/16 :goto_0

    .line 822
    :sswitch_9
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->moveViewPagerRight()V

    goto/16 :goto_0

    .line 825
    :sswitch_a
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    goto/16 :goto_0

    .line 779
    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0xca -> :sswitch_1
        0xcb -> :sswitch_2
        0xcc -> :sswitch_3
        0xcd -> :sswitch_5
        0xce -> :sswitch_6
        0xcf -> :sswitch_7
        0xd0 -> :sswitch_8
        0xd1 -> :sswitch_9
        0x12c -> :sswitch_4
        0x12d -> :sswitch_a
    .end sparse-switch
.end method
