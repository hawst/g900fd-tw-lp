.class Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;
.super Ljava/lang/Object;
.source "AppListEditWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V
    .locals 0

    .prologue
    .line 575
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x1

    .line 577
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 578
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 605
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v1, 0x1

    return v1

    .line 580
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$500(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)I

    move-result v1

    if-eq v1, v4, :cond_0

    .line 581
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/ImageView;

    move-result-object v1

    const v2, 0x7f02007f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 587
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 588
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$602(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Z)Z

    .line 589
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditDragIndex:I
    invoke-static {v1, v4}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$502(Lcom/sec/android/app/FlashBarService/AppListEditWindow;I)I

    goto :goto_0

    .line 593
    :pswitch_3
    const-string v1, "AppListEdit"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "drag dropped on app edit list index="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)I

    move-result v1

    if-eq v1, v4, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 595
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$900(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/os/SystemVibrator;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mIvt:[B
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)[B

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$900(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/os/SystemVibrator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 596
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->moveToEditListItem(I)V

    .line 597
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 598
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$1100(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/widget/GridView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mEditListAdapter:Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$1000(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/AppListEditWindow$AppListEditAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->smoothScrollToPosition(I)V

    .line 599
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v1, v5}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$602(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Z)Z

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mAppListDragIndex:I
    invoke-static {v1, v4}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$702(Lcom/sec/android/app/FlashBarService/AppListEditWindow;I)I

    goto/16 :goto_0

    .line 578
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
