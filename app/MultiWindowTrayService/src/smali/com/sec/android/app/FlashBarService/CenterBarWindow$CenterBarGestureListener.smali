.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "CenterBarWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CenterBarGestureListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1492
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Lcom/sec/android/app/FlashBarService/CenterBarWindow$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow$1;

    .prologue
    .line 1492
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1495
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDoubleTabbed:Z
    invoke-static {v0, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1496
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 1497
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setCenterBarPoint(ILandroid/graphics/Point;)V

    .line 1498
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1499
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 1500
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1501
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1503
    :cond_0
    return v4
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v5, 0xcd

    .line 1508
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportStyleTransition(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1535
    :cond_0
    :goto_0
    return-void

    .line 1511
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDoubleTabbed:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1514
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1517
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->isEnableMakePenWindow()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1521
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIvt:[B
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 1522
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 1524
    .local v1, "touchP":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1525
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1526
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->changeTaskToCascade(Landroid/graphics/Point;IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1529
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z

    .line 1530
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideGuideCenterBar()V

    .line 1532
    move-object v0, p1

    .line 1533
    .local v0, "event":Landroid/view/MotionEvent;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v4, 0xce

    invoke-static {v3, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto/16 :goto_0
.end method
