.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarDisappear()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 1129
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 1140
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMakeInstanceAniRunning:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 1141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 1143
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1145
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1134
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1132
    return-void
.end method
