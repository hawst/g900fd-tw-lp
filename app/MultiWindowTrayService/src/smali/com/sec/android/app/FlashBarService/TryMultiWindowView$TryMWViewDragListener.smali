.class Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;
.super Ljava/lang/Object;
.source "TryMultiWindowView.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TryMWViewDragListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V
    .locals 0

    .prologue
    .line 548
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 14
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 552
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    .line 553
    .local v2, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v11

    float-to-int v0, v11

    .line 554
    .local v0, "X":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v11

    float-to-int v1, v11

    .line 556
    .local v1, "Y":I
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    div-int/lit8 v9, v11, 0x2

    .line 557
    .local v9, "tryMWViewContainerHeightCenter":I
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    div-int/lit8 v10, v11, 0x2

    .line 558
    .local v10, "tryMWViewContainerWidthCenter":I
    packed-switch v2, :pswitch_data_0

    .line 878
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v11, 0x1

    return v11

    .line 567
    :pswitch_1
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1DrawnSuccess:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_3

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppListPosition:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/16 v12, 0x68

    if-ne v11, v12, :cond_7

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00ac

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v12, v12

    sub-int/2addr v11, v12

    if-ge v0, v11, :cond_3

    .line 570
    :goto_1
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app1DragArea:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 571
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp1ToggleForVibrate:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 572
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp1ToggleForVibrate:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1202(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 573
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)[B

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v13

    invoke-virtual {v13}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 575
    :cond_1
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-eqz v11, :cond_b

    .line 576
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_9

    .line 577
    if-le v0, v10, :cond_8

    .line 578
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 580
    .local v5, "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 581
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 582
    iput v10, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 583
    iput v10, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 584
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 585
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 586
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 587
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    .line 630
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_2
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp1:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 631
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-nez v11, :cond_3

    .line 632
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 633
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 634
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 635
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 636
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 640
    :cond_3
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mAppListPosition:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/16 v12, 0x68

    if-ne v11, v12, :cond_c

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a00ac

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v12

    float-to-int v12, v12

    sub-int/2addr v11, v12

    if-ge v0, v11, :cond_0

    .line 643
    :goto_3
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app2DragArea:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2502(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 644
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp2ToggleForVibrate:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 645
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mApp2ToggleForVibrate:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 646
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mIvt:[B
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)[B

    move-result-object v12

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/os/SystemVibrator;

    move-result-object v13

    invoke-virtual {v13}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v13

    invoke-virtual {v11, v12, v13}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 648
    :cond_4
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_5

    .line 649
    if-le v1, v9, :cond_d

    .line 650
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 652
    .restart local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    iput v9, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 653
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 654
    iput v9, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 655
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 656
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 657
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 658
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 659
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    .line 676
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_5
    :goto_4
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_6

    .line 677
    if-le v0, v10, :cond_e

    .line 678
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 680
    .restart local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 681
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 682
    iput v10, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 683
    iput v10, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 684
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 685
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 686
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 687
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    .line 703
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_6
    :goto_5
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 704
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-nez v11, :cond_0

    .line 705
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 706
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 707
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 708
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 709
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 567
    :cond_7
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00ac

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v11, v11

    if-lt v0, v11, :cond_3

    goto/16 :goto_1

    .line 590
    :cond_8
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 592
    .local v3, "l":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 593
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 594
    const/4 v11, 0x0

    iput v11, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 595
    iput v10, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 596
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 597
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 598
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    goto/16 :goto_2

    .line 601
    .end local v3    # "l":Landroid/widget/FrameLayout$LayoutParams;
    :cond_9
    if-le v1, v9, :cond_a

    .line 602
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 604
    .restart local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    iput v9, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 605
    iput v9, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 606
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 607
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 608
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 609
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    goto/16 :goto_2

    .line 612
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_a
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 614
    .restart local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 615
    iput v9, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 616
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 617
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 618
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 619
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    goto/16 :goto_2

    .line 624
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_b
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 625
    .restart local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 626
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 627
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 628
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 640
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_c
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mResource:Landroid/content/res/Resources;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00ac

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v11, v11

    if-lt v0, v11, :cond_0

    goto/16 :goto_3

    .line 662
    :cond_d
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 664
    .restart local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 665
    const/4 v11, 0x0

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 666
    iput v9, v5, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 667
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v11

    iput v11, v5, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 668
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 669
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 670
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 671
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    goto/16 :goto_4

    .line 690
    .end local v5    # "mTryMWViewEditParams":Landroid/widget/FrameLayout$LayoutParams;
    :cond_e
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 692
    .restart local v3    # "l":Landroid/widget/FrameLayout$LayoutParams;
    const/4 v11, 0x0

    iput v11, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 693
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 694
    const/4 v11, 0x0

    iput v11, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 695
    iput v10, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 696
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 697
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 698
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;I)I

    goto/16 :goto_5

    .line 715
    .end local v3    # "l":Landroid/widget/FrameLayout$LayoutParams;
    :pswitch_2
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewEdit:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isFirstTipShown:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_11

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1DrawnSuccess:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 717
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app1DragArea:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_1e

    .line 718
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1DrawnSuccess:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$802(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 719
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-eqz v11, :cond_1d

    .line 720
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_1b

    .line 721
    if-le v1, v9, :cond_1a

    .line 722
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 723
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 724
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 745
    :goto_6
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp1:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 746
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 747
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 748
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup2:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/view/animation/Animation;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 750
    :cond_f
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 751
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 752
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 753
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-nez v11, :cond_10

    .line 754
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 755
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 769
    :cond_10
    :goto_7
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 772
    :cond_11
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isSecondTipShown:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_19

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_19

    .line 773
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->app2DragArea:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_21

    .line 774
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp2Drawn:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3602(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 775
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/4 v12, 0x2

    if-ne v11, v12, :cond_14

    .line 776
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 777
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_12

    .line 778
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 780
    :cond_12
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout$LayoutParams;

    .line 782
    .local v8, "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v8, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 783
    iput v10, v8, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 784
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 786
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 788
    .local v4, "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 789
    iput v10, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 790
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 792
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 793
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout$LayoutParams;

    .line 795
    .local v7, "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v7, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 796
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    iput v11, v7, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 797
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 799
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout$LayoutParams;

    .line 801
    .local v6, "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainer:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    invoke-virtual {v11}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v11

    iput v11, v6, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 802
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewContainerWidthCenter:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    iput v11, v6, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 803
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    invoke-virtual {v11, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 805
    .end local v6    # "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v7    # "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_13
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x4

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 806
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 808
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrow:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f0200a5

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 810
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitArrowOverlay:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f0200a6

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 813
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setMarginsForViewInLandMode()V
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V

    .line 816
    .end local v4    # "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    .end local v8    # "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_14
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mOrientation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_15

    .line 817
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControl:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 818
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewSplitControlLand:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 819
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->setMarginsForViewInPortMode()V
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)V

    .line 822
    :cond_15
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-eqz v11, :cond_16

    .line 823
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 824
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 827
    :cond_16
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    if-ne v11, v12, :cond_1f

    .line 828
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 829
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020085

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 830
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 831
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 838
    :goto_8
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp3:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 839
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_17

    .line 840
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_UP:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    if-ne v11, v12, :cond_20

    .line 841
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 847
    :cond_17
    :goto_9
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWFeatureLevel:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_18

    .line 848
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPopUp3:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/view/animation/Animation;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 850
    :cond_18
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 851
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 852
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 853
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 854
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-nez v11, :cond_19

    .line 855
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 856
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 870
    :cond_19
    :goto_a
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 871
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWApp1:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$5000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_0

    .line 726
    :cond_1a
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 727
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 728
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 731
    :cond_1b
    if-le v0, v10, :cond_1c

    .line 732
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 733
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 734
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 736
    :cond_1c
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/FrameLayout;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 737
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 738
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 742
    :cond_1d
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_6

    .line 758
    :cond_1e
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    const/4 v12, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->isApp1Drawn:Z
    invoke-static {v11, v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$402(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;Z)Z

    .line 759
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 760
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 761
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 762
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup1:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/view/animation/Animation;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 763
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp1:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 764
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-nez v11, :cond_10

    .line 765
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 766
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_7

    .line 833
    :cond_1f
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020085

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 834
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHome:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 835
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const v12, 0x7f020084

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 836
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewApp2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_8

    .line 842
    :cond_20
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mFrameLocation:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->FRAME_LOCATION_DOWN:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)I

    move-result v12

    if-ne v11, v12, :cond_17

    .line 843
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    .line 859
    :cond_21
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewDragApp2:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 860
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mMWDialogPopup2:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/view/animation/Animation;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 861
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewFlashBar:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 862
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 863
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/LinearLayout;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 864
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    iget-boolean v11, v11, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->GUI_SCALE_PREVIEW:Z

    if-nez v11, :cond_19

    .line 865
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandle:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 866
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowView$TryMWViewDragListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowView;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->mTryMWViewHandleClose:Landroid/widget/ImageView;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowView;->access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowView;)Landroid/widget/ImageView;

    move-result-object v11

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_a

    .line 558
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
