.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->onForegroundActivitiesChanged(IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

.field final synthetic val$foregroundActivities:Z

.field final synthetic val$pid:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;ZI)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->val$foregroundActivities:Z

    iput p3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->val$pid:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/16 v3, 0xcf

    const/4 v5, 0x1

    .line 529
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v1, :cond_5

    .line 532
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListViewPagerTrayShow:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 533
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v0

    .line 536
    .local v0, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v1, :cond_1

    .line 537
    if-eqz v0, :cond_6

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v1

    if-nez v1, :cond_6

    .line 541
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 544
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->closeFlashBar()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 551
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 553
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 555
    :cond_2
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v1

    if-ne v1, v5, :cond_4

    .line 556
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/util/SparseArray;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserId:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    .line 557
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 558
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initCenterBarButtonAnim()V

    .line 560
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/util/SparseArray;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mUserId:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 564
    .end local v0    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_4
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->val$foregroundActivities:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->val$pid:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedTaskId:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->checkPackageName(ILjava/lang/String;I)Z
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ILjava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 567
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v2, 0x12d

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 568
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mShowViewPagerTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 569
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createViewPagerTray()V

    .line 572
    :cond_5
    return-void

    .line 545
    .restart local v0    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 548
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFocusVisible:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v1, v2, v3, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    goto/16 :goto_0
.end method
