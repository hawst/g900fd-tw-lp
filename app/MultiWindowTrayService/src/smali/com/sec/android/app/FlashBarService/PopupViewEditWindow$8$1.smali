.class Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;
.super Landroid/view/View$DragShadowBuilder;
.source "PopupViewEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 456
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;->this$1:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 464
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;->this$1:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/ImageView;

    move-result-object v0

    .line 465
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 466
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 458
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;->this$1:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 459
    .local v1, "ShadowWidth":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;->this$1:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getHeight()I

    move-result v0

    .line 460
    .local v0, "ShadowHeight":I
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Point;->set(II)V

    .line 461
    div-int/lit8 v2, v1, 0x2

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 462
    return-void
.end method
