.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;
.super Landroid/view/View$DragShadowBuilder;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 3653
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v3, 0x7f0f0059

    .line 3661
    const/4 v0, 0x0

    .line 3662
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBezelUI:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 3663
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 3667
    :goto_0
    if-eqz v0, :cond_0

    .line 3668
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 3670
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurSrcIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3671
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3672
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 3673
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->isLaunchingBlockedItem(I)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3674
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3675
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLunchBlock:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3678
    :cond_1
    return-void

    .line 3665
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getCurrentAppList()Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 3655
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3656
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowHeight:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3657
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 3658
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$48;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 3659
    return-void
.end method
