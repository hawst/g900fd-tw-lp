.class public Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "PopupViewWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewPagerAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Landroid/content/Context;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 825
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 826
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->context:Landroid/content/Context;

    .line 827
    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 828
    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 829
    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makePopupViewPagerAppList()V

    .line 831
    :cond_0
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 873
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    check-cast p3, Landroid/widget/FrameLayout;

    .end local p3    # "object":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 874
    return-void
.end method

.method public getCount()I
    .locals 4

    .prologue
    const/16 v3, 0x67

    .line 834
    const/4 v0, 0x0

    .line 835
    .local v0, "cnt":I
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 836
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListCnt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v2

    div-int v0, v1, v2

    .line 837
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListCnt(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v2

    rem-int/2addr v1, v2

    if-lez v1, :cond_0

    .line 838
    add-int/lit8 v0, v0, 0x1

    .line 840
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPageCount:I
    invoke-static {v1, v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1902(Lcom/sec/android/app/FlashBarService/PopupViewWindow;I)I

    move v1, v0

    .line 843
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 857
    const/4 v1, 0x0

    .line 858
    .local v1, "v":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1800(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 859
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030008

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 863
    :goto_0
    const v2, 0x7f0f005d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 864
    .local v0, "gridView":Landroid/widget/GridView;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWindowKeyListener:Landroid/view/View$OnKeyListener;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/View$OnKeyListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 865
    new-instance v2, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {v2, v3, p2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;I)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 866
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 867
    invoke-virtual {v0}, Landroid/widget/GridView;->requestFocus()Z

    .line 868
    return-object v1

    .line 861
    .end local v0    # "gridView":Landroid/widget/GridView;
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030009

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 852
    check-cast p2, Landroid/widget/FrameLayout;

    .end local p2    # "object":Ljava/lang/Object;
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
