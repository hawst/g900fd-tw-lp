.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

.field final synthetic val$ani:Landroid/view/animation/Animation;

.field final synthetic val$expand:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZLandroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 3247
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->val$expand:Z

    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->val$ani:Landroid/view/animation/Animation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 3250
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    .line 3251
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3254
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v2

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->val$expand:Z

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListPosition(IZZ)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 3258
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuidePosition(ZZ)V
    invoke-static {v1, v5, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZ)V

    .line 3259
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v1, v5, v6, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 3261
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->val$ani:Landroid/view/animation/Animation;

    new-instance v2, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;-><init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3273
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMainFrame:Landroid/view/ViewGroup;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/ViewGroup;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->val$ani:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3274
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 3275
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->setSemiTransStatusMode(Z)V

    .line 3276
    return-void

    .line 3255
    :catch_0
    move-exception v0

    .line 3256
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
