.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;
.super Landroid/widget/BaseAdapter;
.source "TryMultiWindowBezelTemplate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GridAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mTryMWBezelAppListIcons:[I

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 613
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 589
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->mTryMWBezelAppListIcons:[I

    .line 614
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->mContext:Landroid/content/Context;

    .line 615
    return-void

    .line 589
    nop

    :array_0
    .array-data 4
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
        0x7f02008b
        0x7f02008c
        0x7f02008d
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
        0x7f02008b
        0x7f02008c
        0x7f02008d
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
    .end array-data
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 619
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->mTryMWBezelAppListIcons:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->mTryMWBezelAppListIcons:[I

    rem-int/lit8 v1, p1, 0x5

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 629
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 635
    if-nez p2, :cond_1

    .line 636
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 637
    .local v0, "imageView":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIconSize:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIconSize:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 638
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 639
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 644
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->mTryMWBezelAppListIcons:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 646
    if-nez p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCheckPairedCreated:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 647
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPairedbubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 648
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->drs:Ljava/util/List;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;Ljava/util/List;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 650
    :cond_0
    return-object v0

    .end local v0    # "imageView":Landroid/widget/ImageView;
    :cond_1
    move-object v0, p2

    .line 641
    check-cast v0, Landroid/widget/ImageView;

    .restart local v0    # "imageView":Landroid/widget/ImageView;
    goto :goto_0
.end method
