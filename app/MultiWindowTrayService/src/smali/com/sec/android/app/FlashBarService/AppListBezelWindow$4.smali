.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;
.super Landroid/content/BroadcastReceiver;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 565
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "action":Ljava/lang/String;
    const-string v2, "ResponseAxT9Info"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 568
    const-string v2, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 570
    .local v1, "isInputMethodShown":Z
    const-string v2, "AxT9IME.isMovableKeypad"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 583
    .end local v1    # "isInputMethodShown":Z
    :cond_0
    :goto_0
    return-void

    .line 573
    .restart local v1    # "isInputMethodShown":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getFlashBarEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 576
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 577
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 578
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    const/16 v3, 0x67

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    const/16 v3, 0x68

    if-ne v2, v3, :cond_0

    .line 580
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0xce

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
