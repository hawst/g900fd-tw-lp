.class final Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;
.super Ljava/lang/Object;
.source "TryMultiWindowTray.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MoveRightHandle"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V
    .locals 0

    .prologue
    .line 601
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;

    .prologue
    .line 601
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const v5, 0x7f0a00c1

    const/4 v6, 0x1

    const v4, 0x7f0a00c0

    .line 606
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    int-to-float v0, v1

    .line 608
    .local v0, "Y":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 643
    :cond_0
    :goto_0
    :pswitch_0
    return v6

    .line 613
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200fe

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 614
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 615
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayLongClick;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-direct {v2, v3, v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 616
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x4

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v1

    div-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x6

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 617
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 618
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup4:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 619
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->r:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0xdac

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 627
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveClicked:Z
    invoke-static {v1, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1702(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    .line 628
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMoveHeightRate:F
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1802(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;F)F

    .line 629
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ff

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 630
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_2

    .line 631
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setY(F)V

    .line 637
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->rightHandleMovementStarted:Z
    invoke-static {v1, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    goto/16 :goto_0

    .line 632
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    .line 633
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mDisplayHeight:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setY(F)V

    goto :goto_1

    .line 635
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveRightHandle;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    int-to-float v2, v2

    sub-float v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setY(F)V

    goto :goto_1

    .line 608
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
