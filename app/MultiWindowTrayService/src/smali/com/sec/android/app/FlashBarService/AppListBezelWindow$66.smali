.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4212
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const v7, 0x7f080002

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 4216
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-boolean v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbAnimating:Z

    if-nez v3, :cond_2

    .line 4222
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 4223
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v3, v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4224
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 4225
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 4226
    .local v0, "dim":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0029

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 4227
    .local v2, "paddingTop":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v1, v3

    .line 4229
    .local v1, "paddingBottom":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4230
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v5, v0, v5, v5}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 4231
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4232
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6, v2, v6, v1}, Landroid/widget/Button;->setPadding(IIII)V

    .line 4233
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4234
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 4235
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4236
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4237
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 4238
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 4239
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const v4, 0x7f020031

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 4241
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 4242
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$12900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 4244
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeAppListEditWindow()V

    .line 4245
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    invoke-virtual {v3, v4, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 4246
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4247
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4248
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateAppListForScrollView(Ljava/util/List;Z)V

    .line 4249
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/support/v4/widget/DrawerLayoutMW;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(I)V

    .line 4252
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 4253
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 4254
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 4255
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 4256
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 4257
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 4263
    .end local v0    # "dim":Landroid/graphics/drawable/Drawable;
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingTop":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)V

    .line 4264
    return-void

    .line 4259
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideAppEditList(Z)V

    .line 4260
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 4261
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$13000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/support/v4/widget/DrawerLayoutMW;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(I)V

    goto :goto_0
.end method
