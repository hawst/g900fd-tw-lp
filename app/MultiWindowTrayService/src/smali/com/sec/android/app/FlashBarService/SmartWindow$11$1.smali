.class Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;
.super Landroid/view/View$DragShadowBuilder;
.source "SmartWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow$11;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow$11;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 1071
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1079
    const/4 v0, 0x0

    .line 1080
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0059

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1081
    if-eqz v0, :cond_0

    .line 1082
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 1084
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 1085
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->this$1:Lcom/sec/android/app/FlashBarService/SmartWindow$11;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 1086
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 1073
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 1074
    .local v1, "shadowWidth":I
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 1075
    .local v0, "shadowHeight":I
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Point;->set(II)V

    .line 1076
    div-int/lit8 v2, v1, 0x2

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 1077
    return-void
.end method
