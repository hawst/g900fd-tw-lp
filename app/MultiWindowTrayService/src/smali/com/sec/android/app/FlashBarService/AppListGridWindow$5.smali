.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 737
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v1, 0x12c

    .line 742
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 755
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 746
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 747
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 750
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHistoryTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 751
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$5;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 742
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
