.class Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;
.super Landroid/view/View$DragShadowBuilder;
.source "SmartClipDragDrop.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ShadowBuilder"
.end annotation


# instance fields
.field final SCALE_RATE:F

.field private mDragBitmap:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2, "view"    # Landroid/view/View;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    .line 108
    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 104
    const v0, 0x3f4ccccd    # 0.8f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->SCALE_RATE:F

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 109
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 110
    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 10
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v9, 0x0

    const v8, 0x3f4ccccd    # 0.8f

    const/4 v7, 0x0

    .line 122
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_0

    .line 124
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 125
    .local v4, "paint":Landroid/graphics/Paint;
    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 126
    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 127
    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_BORDER_LINE_THICK:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$000()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 130
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 131
    .local v3, "imgW":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 132
    .local v2, "imgH":I
    new-instance v0, Landroid/graphics/Rect;

    add-int/lit8 v5, v3, -0x1

    add-int/lit8 v6, v2, -0x1

    invoke-direct {v0, v9, v9, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 133
    .local v0, "borderRect":Landroid/graphics/Rect;
    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_BORDER_LINE_THICK:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$000()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->DRAG_SHADOW_BORDER_LINE_THICK:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$000()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->inset(II)V

    .line 134
    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    .line 135
    .local v1, "imagePath":Landroid/graphics/Path;
    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 136
    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 137
    iget v5, v0, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 138
    iget v5, v0, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 139
    iget v5, v0, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget v6, v0, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 142
    invoke-virtual {p1, v8, v8}, Landroid/graphics/Canvas;->scale(FF)V

    .line 143
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v5, v7, v7, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 144
    invoke-virtual {p1, v1, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 146
    .end local v0    # "borderRect":Landroid/graphics/Rect;
    .end local v1    # "imagePath":Landroid/graphics/Path;
    .end local v2    # "imgH":I
    .end local v3    # "imgW":I
    .end local v4    # "paint":Landroid/graphics/Paint;
    :cond_0
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    const v3, 0x3f4ccccd    # 0.8f

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 115
    .local v1, "width":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$ShadowBuilder;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 116
    .local v0, "height":I
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Point;->set(II)V

    .line 117
    div-int/lit8 v2, v1, 0x2

    div-int/lit8 v3, v0, 0x2

    invoke-virtual {p2, v2, v3}, Landroid/graphics/Point;->set(II)V

    .line 119
    .end local v0    # "height":I
    .end local v1    # "width":I
    :cond_0
    return-void
.end method
