.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 150
    :try_start_0
    const-string v8, "MultiWindowTrayService"

    const-string v9, "mPkgManagerReceiver : onReceive"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const-string v8, "android.intent.extra.user_handle"

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->getSendingUserId()I

    move-result v9

    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 152
    .local v7, "userHandle":I
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v8

    if-eq v7, v8, :cond_1

    .line 186
    .end local v7    # "userHandle":I
    :cond_0
    :goto_0
    return-void

    .line 155
    .restart local v7    # "userHandle":I
    :cond_1
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v8

    if-eqz v8, :cond_2

    .line 156
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->pkgManagerList(Landroid/content/Intent;)V

    .line 157
    :cond_2
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SmartWindow;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->pkgManagerList(Landroid/content/Intent;)V

    .line 158
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    move-result-object v8

    if-eqz v8, :cond_3

    .line 159
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    move-result-object v8

    invoke-virtual {v8, p2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->pkgManagerList(Landroid/content/Intent;)V

    .line 162
    :cond_3
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "actionStr":Ljava/lang/String;
    const-string v8, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_4

    const-string v8, "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 171
    :cond_4
    const-string v8, "android.intent.extra.changed_package_list"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 172
    .local v5, "pkgList":[Ljava/lang/String;
    if-eqz v5, :cond_0

    array-length v8, v5

    if-eqz v8, :cond_0

    .line 177
    const-string v8, "android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 179
    .local v2, "available":Z
    if-eqz v2, :cond_0

    .line 180
    move-object v1, v5

    .local v1, "arr$":[Ljava/lang/String;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_1
    if-ge v3, v4, :cond_0

    aget-object v6, v1, v3

    .line 181
    .local v6, "pkgName":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$1;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateInstalledPackage(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 185
    .end local v0    # "actionStr":Ljava/lang/String;
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "available":Z
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    .end local v5    # "pkgList":[Ljava/lang/String;
    .end local v6    # "pkgName":Ljava/lang/String;
    .end local v7    # "userHandle":I
    :catch_0
    move-exception v8

    goto :goto_0
.end method
