.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 358
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getFlashBarEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateAppListRelayout(Z)V

    .line 360
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$11;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->showFlashBar(ZZ)V

    .line 362
    :cond_0
    return-void
.end method
