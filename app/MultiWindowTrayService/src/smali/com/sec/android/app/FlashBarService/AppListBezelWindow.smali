.class public Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.super Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;,
        Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;,
        Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppItemViewHolder;,
        Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListItemViewHolder;
    }
.end annotation


# static fields
.field private static SCROLL_MOVE_DELAY:I


# instance fields
.field private final APPLIST_DRAG_ZONE:I

.field private final APPLIST_LONG_PRESS:I

.field private final APPLIST_RECENT_HOMEKEY:I

.field private final APPLIST_RECENT_REMOVE:I

.field private final APPLIST_TIMER_COLLAPSE:I

.field private final APPLIST_TIMER_LONG_PRESS:I

.field private final APPLIST_TIMER_MESSAGE:I

.field private final ESTIMATE_INVALID_VALUE:S

.field private final HISTORYBAR_REMAKE:I

.field private final HISTORY_TIMER_MESSAGE:I

.field private final MAX_NAME_LENGTH:I

.field private final MAX_TASKS:I

.field private final PAIREDWINDOW_DIALOG_SHOW:I

.field private final THUMBNAIL_TYPE_APP:I

.field private final THUMBNAIL_TYPE_RECENT:I

.field private final UPDATE_APPLIST_POSITION:I

.field private final UPDATE_APPLIST_POSITION_DELAY:I

.field private final UPDATE_RUNNINGAPP_LIST:I

.field private final UPDATE_RUNNINGAPP_LIST_DELAY:I

.field private final WINDOW_PORTRAIT_MODE:S

.field private doOneTimeAfterDragStarts:Z

.field private dragEnteredChildCount:I

.field private lastDegrees:I

.field private mAddEmptyItemPosition:I

.field private mAddToListItem:I

.field private mAnimationBackground:Landroid/widget/ImageView;

.field private mAnimationCapture:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimationIcon:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field mAppDefHeight:I

.field mAppDefWidth:I

.field mAppIconClickListener:Landroid/view/View$OnClickListener;

.field mAppIconHoverListener:Landroid/view/View$OnHoverListener;

.field private mAppIconIndex:I

.field mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

.field mAppIconTouchListener:Landroid/view/View$OnTouchListener;

.field private mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

.field mAppListDragListener:Landroid/view/View$OnDragListener;

.field private mAppListDragMode:Z

.field private mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

.field private mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

.field private mAppListHorizontal:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field mAppListItemDragListener:Landroid/view/View$OnDragListener;

.field private mAppListPosition:I

.field private mAppListVertical:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mAppTrayBG:Landroid/widget/ImageView;

.field private mBeforeAppListPosition:I

.field private mBezelDrawer:Landroid/widget/FrameLayout;

.field private mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

.field private mBezelRecognitionArea:I

.field private mBezelUI:Z

.field private mBottomGuideline:Landroid/widget/FrameLayout;

.field private mCancelDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangedPosition:I

.field private mCloseApplist:Z

.field private mConfigChanged:Z

.field mConfirmDialog:Landroid/app/AlertDialog;

.field private mCurrentAppPosition:I

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mCurrentDownView:Landroid/view/View;

.field private mCurrentHistoryIndex:I

.field private mDelIndex:I

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field mDragHandler:Landroid/os/Handler;

.field private mEditBtn:Landroid/widget/Button;

.field private mEditBtn_h:Landroid/widget/Button;

.field private mEditLayout:Landroid/widget/RelativeLayout;

.field private mEditLayout_h:Landroid/widget/RelativeLayout;

.field private mEditTextView:Landroid/widget/EditText;

.field private mExpandAppList:Z

.field private mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mFadingAnimation:Landroid/view/animation/Animation;

.field private mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field mFlashBarEditListener:Landroid/view/View$OnClickListener;

.field mFlashBarHelpListener:Landroid/view/View$OnClickListener;

.field private mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

.field mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

.field mFlashBarOpenListener:Landroid/view/View$OnClickListener;

.field mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

.field mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

.field private mFlashBarVertical:Landroid/widget/ScrollView;

.field private mFlashingAnimation:Landroid/view/animation/Animation;

.field private mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mFocusGuideline:Landroid/widget/ImageView;

.field private mGoalAnimationView:Landroid/view/View;

.field mGuidelineDragListener:Landroid/view/View$OnDragListener;

.field private mGuidelineLayout:Landroid/widget/FrameLayout;

.field private mHandlePosition:Landroid/graphics/Rect;

.field private mHandler:Landroid/os/Handler;

.field private mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

.field private mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

.field private mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialog:Landroid/app/Dialog;

.field private mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

.field private mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

.field private mHistoryBarDialogView:Landroid/view/View;

.field private mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

.field private mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

.field mHistoryTimerHandler:Landroid/os/Handler;

.field private mInputMethodChanged:Z

.field private mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIntent:Landroid/content/Intent;

.field private mIntents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field

.field private mIsAvailableMove:Z

.field private mIsBezelOpened:Z

.field private mIsBezelSliding:Z

.field private mIsCheckBoxChecked:Z

.field private mIsHoverType:Z

.field private mIsInputMethodShown:Z

.field private mIsMultiWindowAppsTabExpanded:Z

.field private mIsPenWindowOnly:Z

.field private mIsPressMoveBtn:Z

.field private mIsRunningAppsTabExpanded:Z

.field private mIsTemplateItem:Z

.field private mIsUsingHelpPopupOpened:Z

.field private mIvt:[B

.field private mLastConfig:Landroid/content/res/Configuration;

.field private mLastDrawerSlideOffset:F

.field private mLastHandlePosition:Landroid/graphics/Rect;

.field private mLastMultiWindowTypeArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLastPostAnimationRunnable:Ljava/lang/Runnable;

.field private mLunchBlock:Z

.field private mMainFrame:Landroid/view/ViewGroup;

.field private mMakeInstanceAniRunning:Z

.field private mMoveBtn:Landroid/widget/ImageView;

.field private mMoveBtnBoundary:I

.field mMoveBtnDragListener:Landroid/view/View$OnDragListener;

.field mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

.field private mMoveBtnOverlapMargin:I

.field private mMoveBtnShadowMargin:I

.field mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

.field private mMultiWindowAppsTab:Landroid/widget/TextView;

.field private mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

.field private mNeedUpdatePocketTrayButtonText:Z

.field private mNewInstanceIntent:Landroid/content/Intent;

.field private mOnlyEditBtn:Landroid/widget/Button;

.field private mOpenPocketTray:Z

.field private mPocketEditBtn:Landroid/widget/ImageView;

.field private mPocketEditItem:Landroid/widget/RelativeLayout;

.field private mPocketEditText:Landroid/widget/TextView;

.field private mPocketHelpBtn:Landroid/widget/ImageView;

.field private mPocketHelpItem:Landroid/widget/RelativeLayout;

.field private mPocketHelpText:Landroid/widget/TextView;

.field private mPocketLayout:Landroid/widget/RelativeLayout;

.field private mPocketOpenBtn:Landroid/widget/ImageButton;

.field private mPocketTemplateBtn:Landroid/widget/ImageView;

.field private mPocketTemplateItem:Landroid/widget/RelativeLayout;

.field private mPocketTemplateText:Landroid/widget/TextView;

.field private mPocketTrayBody:Landroid/widget/LinearLayout;

.field private mPocketTrayHeader:Landroid/widget/RelativeLayout;

.field private mPositionGuideline:Landroid/widget/ImageView;

.field private mPositionX:I

.field private mPositionY:I

.field private mRecentIcon:Landroid/widget/ImageView;

.field mRecentIconClickListener:Landroid/view/View$OnClickListener;

.field private mRecentLayout:Landroid/widget/LinearLayout;

.field private mRecentTaskInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field mResetConfirmDialog:Landroid/app/AlertDialog;

.field private mResouces:Landroid/content/res/Resources;

.field mResumedInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mRunningAppAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;

.field mRunningAppClickListener:Landroid/view/View$OnClickListener;

.field private mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

.field private mRunningAppsTab:Landroid/widget/TextView;

.field private mScrollDirection:I

.field private mShadowHeight:I

.field private mShadowWidth:I

.field private mShowAnimation:Landroid/view/animation/Animation;

.field private mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field private mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

.field private mStartFlashBar:Z

.field private mStatusBarUpdate:Z

.field private mTemplateBtn:Landroid/widget/Button;

.field private mTemplateBtn_h:Landroid/widget/Button;

.field mTemplateDialog:Landroid/app/AlertDialog;

.field private mTemplateIconFinal:Landroid/widget/ImageView;

.field private mTemplateIconIndex:I

.field private mThbumNailCancelImageSize:I

.field private mThumbNailImageHeight:I

.field private mThumbNailImageWidth:I

.field private mThumbNailLayoutHeight:I

.field private mThumbNailLayoutPadding:I

.field private mThumbNailLayoutWidth:I

.field private mThumbnailTaskInfos:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbnailType:I

.field mTimerHandler:Landroid/os/Handler;

.field mToolkitRect:Landroid/graphics/Rect;

.field mToolkitZone:I

.field private mTopGuideline:Landroid/widget/FrameLayout;

.field private mTopLayoutVertical:Landroid/widget/LinearLayout;

.field mUnableDialog:Landroid/app/AlertDialog;

.field private mUserId:I

.field private mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

.field private mWindowAppList:Landroid/view/Window;

.field mWindowDefHeight:I

.field mWindowDefWidth:I

.field private mWindowGuideline:Landroid/view/Window;

.field private mbEditmode:Z

.field private mbFromEdit:Z

.field private mbSynchronizeConfirmDialog:Z

.field private mcurDstIndex:I

.field private mcurSrcIndex:I

.field private misAvailableHelpHub:Z

.field private orgIcon:Landroid/graphics/drawable/Drawable;

.field private orientationListener:Landroid/view/OrientationEventListener;

.field private templateAnimationIcon:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->SCROLL_MOVE_DELAY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p3, "trayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    const/16 v9, 0x32

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 483
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 145
    iput-short v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->ESTIMATE_INVALID_VALUE:S

    .line 146
    iput-short v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->WINDOW_PORTRAIT_MODE:S

    .line 147
    const/16 v4, 0x1388

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_TIMER_COLLAPSE:I

    .line 148
    const/16 v4, 0xc8

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_TIMER_LONG_PRESS:I

    .line 149
    const/16 v4, 0xc9

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_TIMER_MESSAGE:I

    .line 150
    const/16 v4, 0xca

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_LONG_PRESS:I

    .line 151
    const/16 v4, 0xcb

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_RECENT_HOMEKEY:I

    .line 152
    const/16 v4, 0xcc

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_RECENT_REMOVE:I

    .line 153
    const/16 v4, 0xcd

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->PAIREDWINDOW_DIALOG_SHOW:I

    .line 154
    const/16 v4, 0xce

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->UPDATE_APPLIST_POSITION:I

    .line 155
    const/16 v4, 0xcf

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->UPDATE_RUNNINGAPP_LIST:I

    .line 156
    const/16 v4, 0xc8

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->UPDATE_APPLIST_POSITION_DELAY:I

    .line 157
    const/16 v4, 0x320

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->UPDATE_RUNNINGAPP_LIST_DELAY:I

    .line 158
    const/16 v4, 0x12c

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->HISTORYBAR_REMAKE:I

    .line 159
    const/16 v4, 0x3e8

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->APPLIST_DRAG_ZONE:I

    .line 161
    const/16 v4, 0x12c

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->HISTORY_TIMER_MESSAGE:I

    .line 162
    const/16 v4, 0x64

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->MAX_TASKS:I

    .line 163
    const/16 v4, 0x64

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->MAX_NAME_LENGTH:I

    .line 165
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .line 173
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    .line 174
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    .line 175
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    .line 176
    const/16 v4, 0x26

    new-array v4, v4, [B

    fill-array-data v4, :array_0

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B

    .line 200
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPressMoveBtn:Z

    .line 201
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z

    .line 214
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelUI:Z

    .line 215
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsRunningAppsTabExpanded:Z

    .line 216
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsMultiWindowAppsTabExpanded:Z

    .line 217
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    .line 218
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    .line 219
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z

    .line 221
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLunchBlock:Z

    .line 222
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dragEnteredChildCount:I

    .line 223
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPenWindowOnly:Z

    .line 224
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z

    .line 225
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z

    .line 226
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$1;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    .line 272
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    .line 273
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    .line 276
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v9, v9, v9, v9}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    .line 277
    const/16 v4, 0x68

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    .line 280
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsAvailableMove:Z

    .line 281
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z

    .line 282
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    .line 283
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStartFlashBar:Z

    .line 285
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    .line 286
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z

    .line 297
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationCapture:Ljava/util/List;

    .line 298
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    .line 307
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    .line 308
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I

    .line 312
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I

    .line 318
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    .line 321
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    .line 322
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I

    .line 326
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I

    .line 327
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDelIndex:I

    .line 328
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbFromEdit:Z

    .line 335
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    .line 341
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 342
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    .line 345
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    .line 347
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;

    .line 348
    iput v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->THUMBNAIL_TYPE_APP:I

    .line 349
    const/4 v4, 0x2

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->THUMBNAIL_TYPE_RECENT:I

    .line 356
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    .line 380
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    .line 391
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsHoverType:Z

    .line 392
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNeedUpdatePocketTrayButtonText:Z

    .line 394
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppDefWidth:I

    .line 395
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppDefHeight:I

    .line 396
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    .line 397
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    .line 398
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mToolkitZone:I

    .line 399
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mToolkitRect:Landroid/graphics/Rect;

    .line 402
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    .line 403
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z

    .line 404
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfigChanged:Z

    .line 405
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    .line 410
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4, v5, v5, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    .line 413
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentTaskInfos:Ljava/util/List;

    .line 417
    const/16 v4, 0x67

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I

    .line 420
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsCheckBoxChecked:Z

    .line 421
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsUsingHelpPopupOpened:Z

    .line 424
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;

    .line 425
    iput v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserId:I

    .line 429
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->doOneTimeAfterDragStarts:Z

    .line 530
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    .line 552
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$3;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

    .line 562
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 694
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    .line 761
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$9;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryTimerHandler:Landroid/os/Handler;

    .line 2136
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$32;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    .line 2348
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    .line 3125
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    .line 3230
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$35;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$35;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 3243
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$36;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$36;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 3256
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$37;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$37;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    .line 3840
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$55;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

    .line 3933
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$56;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$56;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 3943
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$57;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$57;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnDragListener:Landroid/view/View$OnDragListener;

    .line 3972
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$58;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$58;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppClickListener:Landroid/view/View$OnClickListener;

    .line 4000
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$59;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$59;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    .line 4034
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$60;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$60;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconHoverListener:Landroid/view/View$OnHoverListener;

    .line 4042
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 4103
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 4132
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    .line 4152
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$64;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$64;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 4180
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$65;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    .line 4212
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$66;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    .line 4268
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$67;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    .line 4329
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$68;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$68;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHelpListener:Landroid/view/View$OnClickListener;

    .line 4388
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$70;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineDragListener:Landroid/view/View$OnDragListener;

    .line 4732
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$71;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragListener:Landroid/view/View$OnDragListener;

    .line 5128
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentIconClickListener:Landroid/view/View$OnClickListener;

    .line 485
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {v4, v5, p0, v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    .line 486
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getFlashBarCurPosition()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    .line 487
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getFlashBarHandlePosition()I

    move-result v1

    .line 488
    .local v1, "position":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    invoke-virtual {v4, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 489
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    .line 490
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020079

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    .line 493
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->helpTapAnimationInit(Landroid/content/Context;)V

    .line 495
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a001a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    .line 496
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a001b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    .line 497
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a001c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    .line 499
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a0059

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutWidth:I

    .line 500
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a005a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutHeight:I

    .line 501
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a005c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutPadding:I

    .line 502
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a005d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailImageWidth:I

    .line 503
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a005e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailImageHeight:I

    .line 504
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v5, 0x7f0a0061

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThbumNailCancelImageSize:I

    .line 506
    new-instance v4, Landroid/content/res/Configuration;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 508
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    rsub-int/lit8 v4, v4, 0x64

    iput v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    .line 510
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 511
    .local v0, "intentFilter":Landroid/content/IntentFilter;
    const-string v4, "ResponseAxT9Info"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 512
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 515
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->initFolderResources(Landroid/content/res/Resources;)V

    .line 517
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    .line 518
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    .line 519
    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$RunningAppAdapter;

    .line 521
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getWindowDefSize()V

    .line 523
    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "com.sec.android.action.NOTIFY_MULTIWINDOW_STATUS"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 524
    .local v2, "statusFilter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 526
    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "android.intent.action.USER_STOPPED"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 527
    .local v3, "userStoppedFilter":Landroid/content/IntentFilter;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 528
    return-void

    .line 176
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method private GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;
    .locals 22
    .param p1, "splitRect"    # Landroid/graphics/Rect;

    .prologue
    .line 3604
    new-instance v16, Landroid/graphics/Matrix;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Matrix;-><init>()V

    .line 3606
    .local v16, "displayMatrix":Landroid/graphics/Matrix;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v15

    .line 3607
    .local v15, "display":Landroid/view/Display;
    new-instance v17, Landroid/util/DisplayMetrics;

    invoke-direct/range {v17 .. v17}, Landroid/util/DisplayMetrics;-><init>()V

    .line 3608
    .local v17, "displayMetrics":Landroid/util/DisplayMetrics;
    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    .line 3610
    const/4 v3, 0x2

    new-array v14, v3, [F

    const/4 v3, 0x0

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    aput v4, v14, v3

    const/4 v3, 0x1

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    aput v4, v14, v3

    .line 3611
    .local v14, "dims":[F
    invoke-virtual {v15}, Landroid/view/Display;->getRotation()I

    move-result v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getDegreesForRotation(I)F

    move-result v13

    .line 3613
    .local v13, "degrees":F
    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Matrix;->reset()V

    .line 3614
    neg-float v3, v13

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->preRotate(F)Z

    .line 3615
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 3616
    const/4 v3, 0x0

    const/4 v4, 0x0

    aget v4, v14, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v14, v3

    .line 3617
    const/4 v3, 0x1

    const/4 v4, 0x1

    aget v4, v14, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    aput v4, v14, v3

    .line 3619
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    const/4 v4, 0x0

    aget v4, v14, v4

    float-to-int v4, v4

    const/4 v5, 0x1

    aget v5, v14, v5

    float-to-int v5, v5

    const/16 v6, 0x4e20

    const v7, 0x270ff

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v3 .. v9}, Landroid/view/SurfaceControl;->screenshot(Landroid/graphics/Rect;IIIIZI)Landroid/graphics/Bitmap;

    move-result-object v20

    .line 3620
    .local v20, "screenCapturedTemp":Landroid/graphics/Bitmap;
    move-object/from16 v0, v17

    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v17

    iget v4, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v19

    .line 3621
    .local v19, "screenCaptured":Landroid/graphics/Bitmap;
    new-instance v11, Landroid/graphics/Canvas;

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3623
    .local v11, "canvasTemp":Landroid/graphics/Canvas;
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3624
    invoke-virtual {v11, v13}, Landroid/graphics/Canvas;->rotate(F)V

    .line 3625
    const/4 v3, 0x0

    aget v3, v14, v3

    neg-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    const/4 v4, 0x1

    aget v4, v14, v4

    neg-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {v11, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 3626
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v11, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 3627
    const/4 v3, 0x0

    invoke-virtual {v11, v3}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 3628
    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 3632
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    const/4 v4, 0x1

    if-lt v3, v4, :cond_0

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    const/4 v4, 0x1

    if-ge v3, v4, :cond_1

    .line 3633
    :cond_0
    const/4 v12, 0x0

    .line 3641
    :goto_0
    return-object v12

    .line 3635
    :cond_1
    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v12

    .line 3636
    .local v12, "cropBitmap":Landroid/graphics/Bitmap;
    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 3637
    .local v10, "canvasFinal":Landroid/graphics/Canvas;
    new-instance v21, Landroid/graphics/Rect;

    move-object/from16 v0, p1

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p1

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v21

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3638
    .local v21, "srcRectForCrop":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p1

    iget v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p1

    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3639
    .local v18, "dstRectForCrop":Landroid/graphics/Rect;
    const/4 v3, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    move-object/from16 v2, v18

    invoke-virtual {v10, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->showTraybarHelpPopup()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->showUsingHelpPopup()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z

    return v0
.end method

.method static synthetic access$10000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsAvailableMove:Z

    return v0
.end method

.method static synthetic access$10002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsAvailableMove:Z

    return p1
.end method

.method static synthetic access$1002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z

    return p1
.end method

.method static synthetic access$10100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I

    return v0
.end method

.method static synthetic access$10102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I

    return p1
.end method

.method static synthetic access$10200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I

    return v0
.end method

.method static synthetic access$10202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I

    return p1
.end method

.method static synthetic access$10300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setAppListHandlePosition(I)V

    return-void
.end method

.method static synthetic access$10400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I

    return v0
.end method

.method static synthetic access$10402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBeforeAppListPosition:I

    return p1
.end method

.method static synthetic access$10500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$10700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$10800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZ)V

    return-void
.end method

.method static synthetic access$10900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    return v0
.end method

.method static synthetic access$10902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z

    return v0
.end method

.method static synthetic access$11000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLunchBlock:Z

    return v0
.end method

.method static synthetic access$11002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLunchBlock:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z

    return p1
.end method

.method static synthetic access$11100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPenWindowOnly:Z

    return v0
.end method

.method static synthetic access$11102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPenWindowOnly:Z

    return p1
.end method

.method static synthetic access$11200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowWidth:I

    return v0
.end method

.method static synthetic access$11202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowWidth:I

    return p1
.end method

.method static synthetic access$11300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowHeight:I

    return v0
.end method

.method static synthetic access$11302(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowHeight:I

    return p1
.end method

.method static synthetic access$11400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelUI:Z

    return v0
.end method

.method static synthetic access$11500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getCurrentAppList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$11700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->isLaunchingBlockedItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$11800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$11802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$11900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    return v0
.end method

.method static synthetic access$12000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    return v0
.end method

.method static synthetic access$12002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    return p1
.end method

.method static synthetic access$1202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    return p1
.end method

.method static synthetic access$12102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNeedUpdatePocketTrayButtonText:Z

    return p1
.end method

.method static synthetic access$12200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$12300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$12400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$12500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$12600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$12700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$12800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$12900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V

    return-void
.end method

.method static synthetic access$13000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/support/v4/widget/DrawerLayoutMW;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    return-object v0
.end method

.method static synthetic access$13100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$13200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$13300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$13400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$13500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$13600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$13700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z

    return v0
.end method

.method static synthetic access$13702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsTemplateItem:Z

    return p1
.end method

.method static synthetic access$13800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$13900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dragEnteredChildCount:I

    return v0
.end method

.method static synthetic access$13902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dragEnteredChildCount:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    return-object v0
.end method

.method static synthetic access$14000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z

    return v0
.end method

.method static synthetic access$14102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    return p1
.end method

.method static synthetic access$14200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutWidth:I

    return v0
.end method

.method static synthetic access$14300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationRecentIconAppearByHomeKey()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserId:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationRecentIcon()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailType:I

    return v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    return v0
.end method

.method static synthetic access$2402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateRunningAppList()V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V

    return-void
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V

    return-void
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I

    return v0
.end method

.method static synthetic access$2902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastMultiWindowTypeArray:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$3002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$3102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntents:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;ZZZ)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getRunningTaskCnt(ZZZ)I

    move-result v0

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Ljava/util/List;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findCandidateZone(Ljava/util/List;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarDisappear()V

    return-void
.end method

.method static synthetic access$3902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMakeInstanceAniRunning:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNewInstanceIntent:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic access$4002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNewInstanceIntent:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailNew()V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/OrientationEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    return-object v0
.end method

.method static synthetic access$4402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/OrientationEventListener;)Landroid/view/OrientationEventListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/view/OrientationEventListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    return-object p1
.end method

.method static synthetic access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$4700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->lastDegrees:I

    return v0
.end method

.method static synthetic access$4802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->lastDegrees:I

    return p1
.end method

.method static synthetic access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsCheckBoxChecked:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastDrawerSlideOffset:F

    return v0
.end method

.method static synthetic access$5102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # F

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastDrawerSlideOffset:F

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    return v0
.end method

.method static synthetic access$5300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    return v0
.end method

.method static synthetic access$5400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updatePocketTrayLayout(IZZ)V

    return-void
.end method

.method static synthetic access$5500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsUsingHelpPopupOpened:Z

    return v0
.end method

.method static synthetic access$5600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    return v0
.end method

.method static synthetic access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    return p1
.end method

.method static synthetic access$5700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 142
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findeCurIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$5800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$5802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orgIcon:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$5900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I

    return v0
.end method

.method static synthetic access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I

    return p1
.end method

.method static synthetic access$602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    return p1
.end method

.method static synthetic access$6100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$6202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z

    return p1
.end method

.method static synthetic access$6300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I

    return v0
.end method

.method static synthetic access$6702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddEmptyItemPosition:I

    return p1
.end method

.method static synthetic access$6800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;III)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changlistItemFromEditList(III)Z

    move-result v0

    return v0
.end method

.method static synthetic access$6900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/DragEvent;

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I

    return v0
.end method

.method static synthetic access$7002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAddToListItem:I

    return p1
.end method

.method static synthetic access$702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z

    return p1
.end method

.method static synthetic access$7100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->changelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->doOneTimeAfterDragStarts:Z

    return v0
.end method

.method static synthetic access$7202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->doOneTimeAfterDragStarts:Z

    return p1
.end method

.method static synthetic access$7300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->addAndChangelistItem(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$7400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I

    return v0
.end method

.method static synthetic access$7500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/HorizontalScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$7600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$7700()I
    .locals 1

    .prologue
    .line 142
    sget v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->SCROLL_MOVE_DELAY:I

    return v0
.end method

.method static synthetic access$7800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGoalAnimationView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    return v0
.end method

.method static synthetic access$8000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$8100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconIndex:I

    return v0
.end method

.method static synthetic access$8102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I

    .prologue
    .line 142
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconIndex:I

    return p1
.end method

.method static synthetic access$8200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->prepareAnimationIcon()V

    return-void
.end method

.method static synthetic access$8300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$8400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->templateAnimationIcon:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$8402(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->templateAnimationIcon:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    return-object v0
.end method

.method static synthetic access$8600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateStart()V

    return-void
.end method

.method static synthetic access$8700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->openFlashBar()V

    return-void
.end method

.method static synthetic access$8800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDelIndex:I

    return v0
.end method

.method static synthetic access$8900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbFromEdit:Z

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStartFlashBar:Z

    return v0
.end method

.method static synthetic access$9002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    return p1
.end method

.method static synthetic access$9100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissResetConfirmDialog()V

    return-void
.end method

.method static synthetic access$9200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateIconAppear()V

    return-void
.end method

.method static synthetic access$9300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateIconMove()V

    return-void
.end method

.method static synthetic access$9400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->endAnimationIcon()V

    return-void
.end method

.method static synthetic access$9500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateIcon()V

    return-void
.end method

.method static synthetic access$9600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$9702(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPressMoveBtn:Z

    return p1
.end method

.method static synthetic access$9800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z

    return v0
.end method

.method static synthetic access$9802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 142
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z

    return p1
.end method

.method static synthetic access$9900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setImageToMoveButton(IZ)V

    return-void
.end method

.method private addAndChangelistItem(II)Z
    .locals 7
    .param p1, "dstIndex"    # I
    .param p2, "editIndex"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2376
    const-string v0, "AppListBezelWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "addAndChangelistItem src="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "dst=%d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2378
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->moveToAppListItem(II)V

    .line 2379
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 2380
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    .line 2381
    return v6
.end method

.method private animationHistoryBarDisappear()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x12c

    const/4 v4, 0x0

    .line 1273
    const/4 v2, 0x0

    .line 1274
    .local v2, "translateAnimation":Landroid/view/animation/Animation;
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    .end local v2    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-direct {v2, v4, v3, v4, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1275
    .restart local v2    # "translateAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1276
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1277
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1278
    new-instance v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$16;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$16;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1297
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1298
    .local v0, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1299
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1301
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1302
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v1, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1303
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1304
    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/AnimationSet;->setStartOffset(J)V

    .line 1306
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1307
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1308
    return-void
.end method

.method private animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    .locals 12
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "animation"    # Z

    .prologue
    .line 1338
    move-object v11, p1

    .line 1339
    .local v11, "sendIntent":Landroid/content/Intent;
    const/4 v0, 0x0

    .line 1340
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .end local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1342
    .restart local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1343
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1345
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1346
    .local v9, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1347
    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1349
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1350
    .local v10, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1351
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1353
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;

    invoke-direct {v1, p0, v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1374
    if-eqz p2, :cond_0

    .line 1375
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1376
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1380
    :goto_0
    return-void

    .line 1378
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, v11}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private animationHistoryBarThumbNailNew()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1f4

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v5, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1240
    const/4 v0, 0x0

    .line 1241
    .local v0, "scaleAnimation":Landroid/view/animation/Animation;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .end local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    move v3, v1

    move v4, v2

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 1243
    .restart local v0    # "scaleAnimation":Landroid/view/animation/Animation;
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1244
    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1246
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1247
    .local v9, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1248
    invoke-virtual {v9, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1250
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v10, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1252
    .local v10, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1254
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$15;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$15;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1268
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1269
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v10}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1270
    return-void
.end method

.method private animationHistoryBarUI()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x12c

    const/4 v6, 0x0

    .line 1311
    const/4 v3, 0x0

    .line 1312
    .local v3, "translateAnimation":Landroid/view/animation/Animation;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    packed-switch v4, :pswitch_data_0

    .line 1322
    :goto_0
    invoke-virtual {v3, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1323
    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1325
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1326
    .local v0, "alphaAnimation1":Landroid/view/animation/Animation;
    new-instance v4, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1327
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1329
    new-instance v1, Landroid/view/animation/AnimationSet;

    const/4 v4, 0x0

    invoke-direct {v1, v4}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 1330
    .local v1, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v1, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1331
    invoke-virtual {v1, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 1333
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 1334
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1335
    return-void

    .line 1314
    .end local v0    # "alphaAnimation1":Landroid/view/animation/Animation;
    .end local v1    # "animationSet":Landroid/view/animation/AnimationSet;
    :pswitch_0
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-direct {v3, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1315
    .restart local v3    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0

    .line 1317
    :pswitch_1
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1318
    .local v2, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1319
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    .end local v3    # "translateAnimation":Landroid/view/animation/Animation;
    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v3, v4, v6, v6, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v3    # "translateAnimation":Landroid/view/animation/Animation;
    goto :goto_0

    .line 1312
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationRecentIcon()V
    .locals 3

    .prologue
    .line 3793
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3794
    .local v0, "bubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$54;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$54;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3802
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentIcon:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3803
    return-void
.end method

.method private animationRecentIconAppearByHomeKey()V
    .locals 0

    .prologue
    .line 3790
    return-void
.end method

.method private animationTemplateIcon()V
    .locals 3

    .prologue
    .line 3765
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 3766
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateScrollPositionTemplate()V

    .line 3767
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3768
    .local v0, "bubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$53;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$53;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3778
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->templateAnimationIcon:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 3779
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->templateAnimationIcon:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3781
    :cond_0
    return-void
.end method

.method private animationTemplateIconAppear()V
    .locals 7

    .prologue
    .line 3688
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    .line 3689
    .local v4, "templateSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 3690
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3691
    .local v1, "animationIcon":Landroid/widget/ImageView;
    move v2, v3

    .line 3692
    .local v2, "count":I
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3694
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v6, 0x7f04003a

    invoke-static {v5, v6}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3695
    .local v0, "animation":Landroid/view/animation/Animation;
    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;

    invoke-direct {v5, p0, v1, v2, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$51;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/ImageView;II)V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3706
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3689
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 3708
    .end local v0    # "animation":Landroid/view/animation/Animation;
    .end local v1    # "animationIcon":Landroid/widget/ImageView;
    .end local v2    # "count":I
    :cond_0
    return-void
.end method

.method private animationTemplateIconMove()V
    .locals 15

    .prologue
    .line 3711
    const/4 v4, 0x0

    .line 3712
    .local v4, "distanceX":F
    const/4 v8, 0x0

    .line 3714
    .local v8, "distanceY":F
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v14

    .line 3715
    .local v14, "templateSize":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_0
    if-ge v13, v14, :cond_1

    .line 3716
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v1, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 3717
    .local v10, "animationIcon":Landroid/widget/ImageView;
    move v12, v13

    .line 3718
    .local v12, "count":I
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    packed-switch v1, :pswitch_data_0

    .line 3728
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v1

    if-eqz v1, :cond_0

    .line 3729
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconIndex:I

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v10}, Landroid/widget/ImageView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v8, v1

    .line 3732
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 3736
    .local v0, "translateAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3738
    new-instance v9, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e4ccccd    # 0.2f

    invoke-direct {v9, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 3739
    .local v9, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 3740
    const-wide/16 v2, 0x1f4

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 3742
    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v11, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 3743
    .local v11, "animationSet":Landroid/view/animation/AnimationSet;
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 3744
    invoke-virtual {v11, v9}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 3746
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$52;

    invoke-direct {v1, p0, v10, v12, v14}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$52;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/ImageView;II)V

    invoke-virtual {v11, v1}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3759
    invoke-virtual {v10, v11}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3715
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 3721
    .end local v0    # "translateAnimation":Landroid/view/animation/Animation;
    .end local v9    # "alphaAnimation":Landroid/view/animation/Animation;
    .end local v11    # "animationSet":Landroid/view/animation/AnimationSet;
    :pswitch_0
    invoke-virtual {v10}, Landroid/widget/ImageView;->getLeft()I

    move-result v1

    neg-int v1, v1

    int-to-float v4, v1

    .line 3722
    goto :goto_1

    .line 3725
    :pswitch_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    invoke-virtual {v10}, Landroid/widget/ImageView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v4, v1

    goto :goto_1

    .line 3762
    .end local v10    # "animationIcon":Landroid/widget/ImageView;
    .end local v12    # "count":I
    :cond_1
    return-void

    .line 3718
    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private animationTemplateStart()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 3645
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v7

    .line 3646
    .local v7, "templateSize":I
    const/4 v6, 0x0

    .line 3647
    .local v6, "splitRect":Landroid/graphics/Rect;
    const/4 v3, 0x0

    .line 3649
    .local v3, "captureImage":Landroid/graphics/Bitmap;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListController;->updateWindowRects()V

    .line 3650
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v7, :cond_0

    .line 3651
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationCapture:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 3652
    .local v1, "animationCapture":Landroid/widget/ImageView;
    move v4, v5

    .line 3653
    .local v4, "count":I
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    invoke-direct {v2, v8}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3657
    .local v2, "animationCaptureMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResumedInfos:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v8

    invoke-virtual {v9, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getRectByZone(I)Landroid/graphics/Rect;

    move-result-object v6

    .line 3658
    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->GetCaptureImage(Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 3659
    if-nez v3, :cond_1

    .line 3660
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->endAnimationIcon()V

    .line 3661
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationTemplateIcon()V

    .line 3685
    .end local v1    # "animationCapture":Landroid/widget/ImageView;
    .end local v2    # "animationCaptureMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v4    # "count":I
    :cond_0
    return-void

    .line 3665
    .restart local v1    # "animationCapture":Landroid/widget/ImageView;
    .restart local v2    # "animationCaptureMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .restart local v4    # "count":I
    :cond_1
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 3666
    iget v8, v6, Landroid/graphics/Rect;->left:I

    iget v9, v6, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v8, v9, v10, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 3668
    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3670
    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3672
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f040035

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3673
    .local v0, "animation":Landroid/view/animation/Animation;
    new-instance v8, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$50;

    invoke-direct {v8, p0, v1, v4, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$50;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/ImageView;II)V

    invoke-virtual {v0, v8}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3683
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3650
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private cancelCollapseTimer()V
    .locals 2

    .prologue
    const/16 v1, 0xc9

    .line 690
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 692
    :cond_0
    return-void
.end method

.method private changelistItem(II)Z
    .locals 4
    .param p1, "srcIndex"    # I
    .param p2, "dstIndex"    # I

    .prologue
    .line 2386
    if-ne p1, p2, :cond_0

    .line 2387
    const/4 v0, 0x0

    .line 2432
    :goto_0
    return v0

    .line 2389
    :cond_0
    const-string v0, "AppListBezelWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ChangeItem src=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") dst=( "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2391
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p2, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->reorderApplist(II)V

    .line 2392
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelUI:Z

    if-eqz v0, :cond_1

    .line 2425
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 2426
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    .line 2431
    :goto_1
    iput p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    .line 2432
    const/4 v0, 0x1

    goto :goto_0

    .line 2428
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 2429
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    goto :goto_1
.end method

.method private changlistItemFromEditList(III)Z
    .locals 4
    .param p1, "appListIndex"    # I
    .param p2, "editListIndex"    # I
    .param p3, "action"    # I

    .prologue
    const/4 v3, 0x1

    .line 2459
    const-string v0, "AppListBezelWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changlistItemFromEditList action="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2460
    const-string v0, "AppListBezelWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changlistItemFromEditList app="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " edit ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2462
    const/4 v0, 0x6

    if-ne p3, v0, :cond_1

    .line 2464
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2467
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->checkAppListLayout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2468
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 2469
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    .line 2482
    :cond_0
    :goto_0
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    .line 2483
    return v3

    .line 2472
    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragMode:Z

    if-ne v0, v3, :cond_0

    .line 2474
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1, p2, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2477
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 2478
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->invalidateViews()V

    goto :goto_0
.end method

.method private closeFlashBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3070
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mExpandAppList:Z

    if-nez v2, :cond_0

    .line 3098
    :goto_0
    return-void

    .line 3074
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 3075
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v4, 0x7f08001a

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3076
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    .line 3078
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3079
    .local v1, "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    const/16 v2, -0x2710

    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 3080
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3083
    .end local v1    # "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    invoke-direct {p0, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V

    .line 3085
    const/4 v0, 0x0

    .line 3087
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v3, 0x66

    if-ne v2, v3, :cond_2

    .line 3088
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040036

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3096
    :goto_1
    invoke-direct {p0, v0, v5, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V

    goto :goto_0

    .line 3089
    :cond_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v3, 0x65

    if-ne v2, v3, :cond_3

    .line 3090
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040039

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 3091
    :cond_3
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v3, 0x68

    if-ne v2, v3, :cond_4

    .line 3092
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040038

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 3094
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f040037

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1
.end method

.method private decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 2289
    if-nez p1, :cond_1

    .line 2290
    const/4 v0, 0x0

    .line 2345
    :cond_0
    :goto_0
    return v0

    .line 2292
    :cond_1
    const/4 v3, -0x1

    .line 2293
    .local v3, "rawEnd":I
    const/4 v4, 0x0

    .line 2295
    .local v4, "scroll":I
    const/4 v0, 0x0

    .line 2296
    .local v0, "bRet":Z
    const/4 v7, 0x2

    new-array v1, v7, [I

    .line 2297
    .local v1, "mOffset":[I
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2298
    const/4 v7, 0x1

    aget v7, v1, v7

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v8

    float-to-int v8, v8

    add-int v3, v7, v8

    .line 2300
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0146

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v6, v7

    .line 2301
    .local v6, "scrollMargin":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0147

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v5, v7

    .line 2303
    .local v5, "scrollAdjustment":I
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_2

    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v7, :cond_5

    .line 2304
    :cond_2
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v8, 0x67

    if-eq v7, v8, :cond_3

    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v8, 0x68

    if-ne v7, v8, :cond_7

    .line 2305
    :cond_3
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    if-eqz v7, :cond_6

    .line 2306
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    sub-int/2addr v7, v5

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getEditAreaHeight()I

    move-result v8

    sub-int v4, v7, v8

    .line 2314
    :goto_1
    const-string v7, "AppListBezelWindow"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Drag decideScrollMove event="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", rawEnd = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " scroll = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2316
    add-int v7, v3, v6

    if-le v7, v4, :cond_9

    .line 2317
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v8, 0x67

    if-eq v7, v8, :cond_4

    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v8, 0x68

    if-ne v7, v8, :cond_8

    .line 2318
    :cond_4
    const/4 v7, 0x4

    iput v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I

    .line 2322
    :goto_2
    const/4 v0, 0x1

    .line 2337
    :cond_5
    :goto_3
    if-eqz v0, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2338
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 2339
    .local v2, "msg":Landroid/os/Message;
    const/4 v7, 0x5

    sput v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->SCROLL_MOVE_DELAY:I

    .line 2340
    const/4 v7, 0x3

    iput v7, v2, Landroid/os/Message;->what:I

    .line 2341
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2342
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->doOneTimeAfterDragStarts:Z

    .line 2343
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 2308
    .end local v2    # "msg":Landroid/os/Message;
    :cond_6
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    sub-int/2addr v7, v5

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v8

    sub-int v4, v7, v8

    goto :goto_1

    .line 2311
    :cond_7
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    sub-int/2addr v7, v5

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v9, 0x7f0a0049

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    sub-int v4, v7, v8

    goto :goto_1

    .line 2320
    :cond_8
    const/4 v7, 0x1

    iput v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I

    goto :goto_2

    .line 2323
    :cond_9
    if-ge v3, v6, :cond_c

    .line 2324
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v8, 0x67

    if-eq v7, v8, :cond_a

    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v8, 0x68

    if-ne v7, v8, :cond_b

    .line 2325
    :cond_a
    const/4 v7, 0x3

    iput v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I

    .line 2329
    :goto_4
    const/4 v0, 0x1

    goto :goto_3

    .line 2327
    :cond_b
    const/4 v7, 0x2

    iput v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I

    goto :goto_4

    .line 2331
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2332
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_3
.end method

.method private dismissHelpPopupWindowTraybar()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1694
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1695
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1696
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1697
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 1699
    :cond_0
    return-void
.end method

.method private dismissHelpPopupWindowUsing()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1702
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 1703
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsUsingHelpPopupOpened:Z

    .line 1704
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1705
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1706
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    .line 1708
    :cond_0
    return-void
.end method

.method private dismissHistoryWindow(Z)V
    .locals 3
    .param p1, "reDraw"    # Z

    .prologue
    const/4 v2, 0x0

    .line 743
    if-nez p1, :cond_0

    .line 744
    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    .line 745
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    .line 747
    :cond_0
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMakeInstanceAniRunning:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNewInstanceIntent:Landroid/content/Intent;

    if-eqz v0, :cond_1

    .line 748
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMakeInstanceAniRunning:Z

    .line 749
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNewInstanceIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    .line 750
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNewInstanceIntent:Landroid/content/Intent;

    .line 752
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 753
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 754
    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 755
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_2

    .line 759
    :cond_2
    return-void
.end method

.method private dismissResetConfirmDialog()V
    .locals 1

    .prologue
    .line 3561
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    .line 3562
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3563
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3564
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    .line 3566
    :cond_0
    return-void
.end method

.method private endAnimationIcon()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 3825
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3826
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3827
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 3828
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3829
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3827
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3831
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 3833
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3834
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3835
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3836
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3837
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3838
    return-void
.end method

.method private findCandidateZone(Ljava/util/List;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 845
    .local p1, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I

    invoke-virtual {v8, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 846
    .local v5, "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v4, 0x0

    .line 847
    .local v4, "obscuredZone":I
    const/4 v0, 0x0

    .line 848
    .local v0, "candidateZone":I
    const/4 v2, 0x0

    .line 849
    .local v2, "lvl1Zone":I
    const/4 v3, 0x0

    .line 850
    .local v3, "lvl2Zone":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 851
    .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v8, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v9, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 854
    iget-object v8, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    .line 855
    .local v7, "taskZone":I
    or-int/2addr v4, v7

    .line 856
    const/4 v8, 0x3

    if-eq v7, v8, :cond_1

    const/16 v8, 0xc

    if-ne v7, v8, :cond_2

    .line 858
    :cond_1
    move v2, v7

    goto :goto_0

    .line 859
    :cond_2
    const/4 v8, 0x1

    if-eq v7, v8, :cond_3

    const/4 v8, 0x2

    if-eq v7, v8, :cond_3

    const/4 v8, 0x4

    if-eq v7, v8, :cond_3

    const/16 v8, 0x8

    if-ne v7, v8, :cond_0

    .line 863
    :cond_3
    move v3, v7

    goto :goto_0

    .line 867
    .end local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v7    # "taskZone":I
    :cond_4
    if-eqz v4, :cond_b

    .line 868
    const/16 v8, 0xf

    if-ne v4, v8, :cond_7

    if-eqz v2, :cond_7

    .line 869
    const/4 v8, 0x3

    if-ne v2, v8, :cond_6

    .line 870
    const/4 v0, 0x2

    .line 899
    :cond_5
    :goto_1
    return v0

    .line 872
    :cond_6
    const/16 v0, 0x8

    goto :goto_1

    .line 875
    :cond_7
    xor-int/lit8 v0, v4, 0xf

    .line 876
    if-eqz v0, :cond_5

    const/16 v8, 0xf

    if-eq v0, v8, :cond_5

    const/4 v8, 0x3

    if-eq v0, v8, :cond_5

    const/16 v8, 0xc

    if-eq v0, v8, :cond_5

    const/4 v8, 0x1

    if-eq v0, v8, :cond_5

    const/4 v8, 0x2

    if-eq v0, v8, :cond_5

    const/4 v8, 0x4

    if-eq v0, v8, :cond_5

    const/16 v8, 0x8

    if-eq v0, v8, :cond_5

    .line 884
    const/4 v8, 0x1

    if-ne v3, v8, :cond_8

    .line 885
    const/4 v0, 0x2

    goto :goto_1

    .line 886
    :cond_8
    const/4 v8, 0x2

    if-ne v3, v8, :cond_9

    .line 887
    const/4 v0, 0x1

    goto :goto_1

    .line 888
    :cond_9
    const/4 v8, 0x4

    if-ne v3, v8, :cond_a

    .line 889
    const/16 v0, 0x8

    goto :goto_1

    .line 890
    :cond_a
    const/16 v8, 0x8

    if-ne v3, v8, :cond_5

    .line 891
    const/4 v0, 0x4

    goto :goto_1

    .line 896
    :cond_b
    const/16 v0, 0xc

    goto :goto_1
.end method

.method private findHistoryBarPosisition(I)I
    .locals 8
    .param p1, "cnt"    # I

    .prologue
    const/4 v5, 0x0

    .line 1213
    const/4 v0, 0x0

    .line 1214
    .local v0, "barHeight":I
    const/4 v4, 0x0

    .line 1215
    .local v4, "top":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutHeight:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutPadding:I

    add-int v2, v6, v7

    .line 1216
    .local v2, "thumbNailsHeight":I
    mul-int v3, v2, p1

    .line 1218
    .local v3, "thumbNailsLayoutHeight":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v7

    if-lt v6, v7, :cond_1

    .line 1219
    move v0, v3

    .line 1220
    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    .line 1225
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v7, 0x1050010

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1226
    .local v1, "statusBarHeight":I
    if-gez v4, :cond_3

    .line 1227
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSystemUiVisibility:I

    if-nez v6, :cond_2

    move v4, v1

    .line 1236
    :cond_0
    :goto_1
    return v4

    .line 1222
    .end local v1    # "statusBarHeight":I
    :cond_1
    add-int/lit8 v6, p1, 0x1

    mul-int v0, v2, v6

    .line 1223
    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    goto :goto_0

    .restart local v1    # "statusBarHeight":I
    :cond_2
    move v4, v5

    .line 1227
    goto :goto_1

    .line 1229
    :cond_3
    add-int v6, v4, v0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v7}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    if-le v6, v7, :cond_0

    .line 1230
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    sub-int v4, v6, v0

    .line 1231
    if-gez v4, :cond_0

    .line 1232
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSystemUiVisibility:I

    if-nez v6, :cond_4

    move v4, v1

    :goto_2
    goto :goto_1

    :cond_4
    move v4, v5

    goto :goto_2
.end method

.method private findeCurIndex(Landroid/view/View;)I
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 2487
    instance-of v3, p1, Landroid/widget/LinearLayout;

    if-nez v3, :cond_1

    move v1, v4

    .line 2496
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v2, p1

    .line 2490
    check-cast v2, Landroid/widget/LinearLayout;

    .line 2491
    .local v2, "item":Landroid/widget/LinearLayout;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v0

    .line 2492
    .local v0, "appListSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 2493
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    check-cast v3, Landroid/widget/LinearLayout;

    if-eq v2, v3, :cond_0

    .line 2492
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v1, v4

    .line 2496
    goto :goto_0
.end method

.method private getAppListHandlePosition()I
    .locals 5

    .prologue
    const/high16 v4, 0x42c80000    # 100.0f

    .line 4676
    const/4 v1, 0x0

    .line 4677
    .local v1, "position":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v3, 0x7f0a0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 4679
    .local v0, "moveBtnWidth":I
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    packed-switch v2, :pswitch_data_0

    .line 4721
    :goto_0
    return v1

    .line 4681
    :pswitch_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4682
    div-int/lit8 v2, v0, 0x2

    if-gt v1, v2, :cond_0

    .line 4683
    const/4 v1, 0x0

    goto :goto_0

    .line 4684
    :cond_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_1

    .line 4685
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    sub-int v1, v2, v0

    goto :goto_0

    .line 4687
    :cond_1
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 4689
    goto :goto_0

    .line 4691
    :pswitch_1
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4692
    div-int/lit8 v2, v0, 0x2

    if-gt v1, v2, :cond_2

    .line 4693
    const/4 v1, 0x0

    goto :goto_0

    .line 4694
    :cond_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_3

    .line 4695
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    sub-int v1, v2, v0

    goto :goto_0

    .line 4697
    :cond_3
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 4699
    goto :goto_0

    .line 4701
    :pswitch_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4702
    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_4

    .line 4703
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    goto :goto_0

    .line 4704
    :cond_4
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_5

    .line 4705
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    sub-int v1, v2, v3

    goto :goto_0

    .line 4707
    :cond_5
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    .line 4709
    goto :goto_0

    .line 4711
    :pswitch_3
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    float-to-int v1, v2

    .line 4712
    div-int/lit8 v2, v0, 0x2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_6

    .line 4713
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    goto/16 :goto_0

    .line 4714
    :cond_6
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_7

    .line 4715
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnBoundary:I

    sub-int v1, v2, v3

    goto/16 :goto_0

    .line 4717
    :cond_7
    div-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    goto/16 :goto_0

    .line 4679
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getCurrentAppList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation

    .prologue
    .line 838
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v1, 0x67

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v1, 0x68

    if-ne v0, v1, :cond_1

    .line 839
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    .line 841
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    goto :goto_0
.end method

.method private getDegreesForRotation(I)F
    .locals 1
    .param p1, "value"    # I

    .prologue
    .line 1738
    packed-switch p1, :pswitch_data_0

    .line 1746
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1740
    :pswitch_0
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_0

    .line 1742
    :pswitch_1
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 1744
    :pswitch_2
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_0

    .line 1738
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getEditAreaHeight()I
    .locals 2

    .prologue
    .line 5006
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 5007
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 5009
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method private getFlashBarCurPosition()I
    .locals 1

    .prologue
    .line 2512
    const/16 v0, 0x68

    return v0
.end method

.method private getFlashBarHandlePosition()I
    .locals 3

    .prologue
    .line 2522
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarHandlePosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2528
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarHandlePosition"

    const/16 v2, 0x32

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method private getRunningTaskCnt(ZZZ)I
    .locals 11
    .param p1, "resume"    # Z
    .param p2, "withScale"    # Z
    .param p3, "showHidden"    # Z

    .prologue
    const/16 v10, 0x800

    .line 4922
    const/4 v1, 0x0

    .line 4923
    .local v1, "cnt":I
    const/4 v5, 0x0

    .line 4925
    .local v5, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v8, 0x64

    const/4 v9, 0x1

    invoke-virtual {v7, v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v5

    .line 4927
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListController;->getSelectedTaskAffinity()Ljava/lang/String;

    move-result-object v3

    .line 4928
    .local v3, "selectedAffinity":Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 4929
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 4930
    .local v4, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 4931
    .local v0, "baseRunningAffinity":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v8, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v6

    .line 4932
    .local v6, "topRunningAffinity":Ljava/lang/String;
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_1

    if-eqz p3, :cond_0

    .line 4933
    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 4934
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_2

    if-eqz p2, :cond_0

    .line 4935
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4937
    :cond_3
    if-eqz v6, :cond_0

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 4938
    iget-object v7, v4, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v7, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_4

    if-eqz p2, :cond_0

    .line 4939
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4945
    .end local v0    # "baseRunningAffinity":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "topRunningAffinity":Ljava/lang/String;
    :cond_5
    return v1
.end method

.method private getWindowDefSize()V
    .locals 5

    .prologue
    const v4, 0x3f266666    # 0.65f

    .line 5288
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 5289
    .local v1, "displaySize":Landroid/graphics/Point;
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    move-result-object v0

    .line 5290
    .local v0, "display":Landroid/view/Display;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 5292
    :cond_0
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefWidth:I

    .line 5293
    iget v2, v1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v2, v4

    add-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowDefHeight:I

    .line 5294
    return-void
.end method

.method private helpTapAnimationInit(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 3222
    const v0, 0x7f040050

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShowAnimation:Landroid/view/animation/Animation;

    .line 3223
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShowAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShowAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3224
    const v0, 0x7f04004e

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    .line 3225
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFadingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFadingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3226
    const v0, 0x7f04004f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    .line 3227
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashingAnimation:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashingAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3228
    return-void
.end method

.method private initPocketTrayLayout()V
    .locals 13

    .prologue
    const/4 v8, 0x0

    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 5028
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v7, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 5029
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0025

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    .line 5030
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0026

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    .line 5031
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0029

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTrayBody:Landroid/widget/LinearLayout;

    .line 5032
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0028

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    .line 5033
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f002b

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    .line 5034
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f002e

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    .line 5035
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0031

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    .line 5036
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f002a

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditItem:Landroid/widget/RelativeLayout;

    .line 5037
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f002d

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;

    .line 5038
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0030

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    .line 5039
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f002c

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    .line 5040
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f002f

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    .line 5041
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v9, 0x7f0f0032

    invoke-virtual {v7, v9}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    .line 5042
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v7, :cond_4

    .line 5043
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v7, :cond_5

    .line 5044
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f080002

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5048
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5049
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5050
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditItem:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5051
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateItem:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5052
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHelpListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5054
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    invoke-virtual {v7, v12}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 5055
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 5057
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const-string v9, "user"

    invoke-virtual {v7, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/UserManager;

    .line 5058
    .local v6, "userManager":Landroid/os/UserManager;
    if-eqz v6, :cond_6

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/os/UserManager;->getUserInfo(I)Landroid/content/pm/UserInfo;

    move-result-object v3

    .line 5060
    .local v3, "info":Landroid/content/pm/UserInfo;
    :goto_1
    if-eqz v3, :cond_3

    .line 5061
    iput-boolean v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z

    .line 5062
    const-string v5, "com.samsung.helphub"

    .line 5063
    .local v5, "packageName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    const/16 v9, 0x80

    iget v10, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v7, v9, v10}, Landroid/content/pm/PackageManager;->getInstalledPackages(II)Ljava/util/List;

    move-result-object v1

    .line 5066
    .local v1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 5067
    .local v0, "appInfo":Landroid/content/pm/PackageInfo;
    iget-object v7, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 5068
    iput-boolean v12, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z

    .line 5073
    .end local v0    # "appInfo":Landroid/content/pm/PackageInfo;
    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isKnoxMode()Z

    move-result v7

    if-nez v7, :cond_7

    .line 5074
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v11}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5118
    .end local v1    # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_3
    :goto_2
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-direct {p0, v7, v11, v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updatePocketTrayLayout(IZZ)V

    .line 5120
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080003

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5121
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f080004

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5123
    .end local v3    # "info":Landroid/content/pm/UserInfo;
    .end local v6    # "userManager":Landroid/os/UserManager;
    :cond_4
    return-void

    .line 5046
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f080001

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .restart local v6    # "userManager":Landroid/os/UserManager;
    :cond_6
    move-object v3, v8

    .line 5058
    goto :goto_1

    .line 5076
    .restart local v1    # "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "info":Landroid/content/pm/UserInfo;
    .restart local v5    # "packageName":Ljava/lang/String;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    const/16 v9, 0x8

    invoke-virtual {v7, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 5077
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpItem:Landroid/widget/RelativeLayout;

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private initRecentLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 5013
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5014
    .local v1, "recentLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0010

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 5015
    .local v0, "recentLabel":Landroid/widget/TextView;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v4, 0x7f0f0011

    invoke-virtual {v3, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentIcon:Landroid/widget/ImageView;

    .line 5016
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v4, 0x7f0a001e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v2, v3

    .line 5018
    .local v2, "topPadding":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 5020
    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 5021
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 5022
    const-string v3, "Recent"

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5023
    const/16 v3, 0x7f

    invoke-static {v3, v5, v5, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 5024
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentIcon:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5025
    return-void
.end method

.method private isLaunchingBlockedItem(I)Z
    .locals 1
    .param p1, "curDstIndex"    # I

    .prologue
    .line 4918
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->isLaunchingBlockedItem(I)Z

    move-result v0

    return v0
.end method

.method private isPortrait()Z
    .locals 2

    .prologue
    .line 432
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeFlashBarLayout(Z)V
    .locals 7
    .param p1, "bAni"    # Z

    .prologue
    const v6, 0x7f080001

    const/16 v5, 0x11

    const/4 v4, 0x1

    const/16 v2, 0x8

    const/4 v3, 0x0

    .line 1751
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastDrawerSlideOffset:F

    .line 1752
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0008

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    .line 1753
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f000d

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    .line 1754
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1755
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001e

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    .line 1757
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0009

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    .line 1759
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001f

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0020

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    .line 1762
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0013

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    .line 1763
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0017

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    .line 1764
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0015

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    .line 1765
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0019

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    .line 1766
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0016

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;

    .line 1767
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001a

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;

    .line 1768
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f000f

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRecentLayout:Landroid/widget/LinearLayout;

    .line 1770
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0024

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    .line 1771
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0022

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    .line 1772
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0021

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppsTab:Landroid/widget/TextView;

    .line 1773
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f0023

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowAppsTab:Landroid/widget/TextView;

    .line 1775
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001d

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    .line 1776
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f001b

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayoutMW;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    .line 1777
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/DrawerLayoutMW;->setScrimColor(I)V

    .line 1778
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a013b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    .line 1780
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerListener(Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;)V

    .line 1890
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setVerticalScrollBarEnabled(Z)V

    .line 1891
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setVisibility(I)V

    .line 1892
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1928
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setVisibility(I)V

    .line 1929
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppsTab:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1932
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->initRecentLayout()V

    .line 1933
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->initPocketTrayLayout()V

    .line 1935
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setGravity(I)V

    .line 1936
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setGravity(I)V

    .line 1937
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v0, :cond_1

    .line 1938
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1939
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1946
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1947
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1948
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1949
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1950
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1952
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1953
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarEditListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1955
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1956
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTemplateListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1957
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1958
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1960
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1961
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1964
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1965
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1967
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    const v1, 0x7f0f000e

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    .line 1968
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1969
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1970
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1971
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1973
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1974
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1976
    if-eqz p1, :cond_0

    .line 1977
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getFlashBarState()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1978
    invoke-direct {p0, v3, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startFlashBarAnimation(ZZ)V

    .line 1984
    :cond_0
    :goto_1
    return-void

    .line 1942
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1943
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1980
    :cond_2
    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setFlashBarState(Z)V

    .line 1981
    invoke-direct {p0, v4, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startFlashBarAnimation(ZZ)V

    goto :goto_1
.end method

.method private makeGuideLineLayout()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 4344
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f008d

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    .line 4345
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f008e

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    .line 4346
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0091

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    .line 4347
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0094

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    .line 4348
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0095

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;

    .line 4349
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0096

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFocusGuideline:Landroid/widget/ImageView;

    .line 4350
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f0097

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    .line 4351
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f0098

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4352
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f0099

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4353
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009a

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4354
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationCapture:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009b

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4355
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009c

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4356
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009d

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4357
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009e

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4358
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationIcon:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v3, 0x7f0f009f

    invoke-virtual {v1, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 4359
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    const v2, 0x7f0f00a0

    invoke-virtual {v1, v2}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    .line 4360
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4361
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBottomGuideline:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4362
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    invoke-virtual {v1, v4}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4363
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionGuideline:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4364
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFocusGuideline:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4365
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4366
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateIconFinal:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4368
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 4370
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 4371
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 4372
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 4373
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 4374
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FlashBarService/GuideLine "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 4375
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 4376
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4377
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 4379
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$69;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$69;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4386
    return-void
.end method

.method private makeHistoryBarDialog(I)V
    .locals 34
    .param p1, "type"    # I

    .prologue
    .line 903
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    const-string v32, "layout_inflater"

    invoke-virtual/range {v31 .. v32}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/view/LayoutInflater;

    .line 905
    .local v11, "layoutInflater":Landroid/view/LayoutInflater;
    const/16 v28, 0x0

    .line 906
    .local v28, "thumbNailCnt":I
    const/4 v8, 0x0

    .line 907
    .local v8, "hasThumb":Z
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailType:I

    .line 909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v31, v0

    const/16 v32, 0x64

    const/16 v33, 0x1

    invoke-virtual/range {v31 .. v33}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v24

    .line 911
    .local v24, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v31, v0

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_3

    .line 912
    const/high16 v31, 0x7f030000

    const/16 v32, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    .line 916
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0001

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ScrollView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    .line 917
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$10;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$10;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0002

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogLayout:Landroid/widget/RelativeLayout;

    .line 940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0004

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    .line 941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0005

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    .line 942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0007

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    .line 943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v31, v0

    const v32, 0x7f0f0006

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

    .line 945
    new-instance v31, Landroid/app/Dialog;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v32, v0

    invoke-direct/range {v31 .. v32}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    .line 946
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    invoke-virtual/range {v31 .. v32}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 947
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 949
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v30

    .line 950
    .local v30, "w":Landroid/view/Window;
    const/high16 v31, 0x10000

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->addFlags(I)V

    .line 951
    const/16 v31, 0x8

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->addFlags(I)V

    .line 952
    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 953
    const/16 v31, -0x3

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->setFormat(I)V

    .line 954
    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Landroid/view/Window;->setDimAmount(F)V

    .line 956
    invoke-virtual/range {v30 .. v30}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v18

    .line 957
    .local v18, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v31, 0x3e8

    move/from16 v0, v31

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 958
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    iput-object v0, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 960
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->clear()V

    .line 961
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I

    move/from16 v31, v0

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    .line 962
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/FlashBarService/AppListController;->getSelectedTaskAffinity()Ljava/lang/String;

    move-result-object v21

    .line 964
    .local v21, "selectedAffinity":Ljava/lang/String;
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v31

    add-int/lit8 v9, v31, -0x1

    .local v9, "i":I
    :goto_1
    if-ltz v9, :cond_6

    .line 965
    move-object/from16 v0, v24

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 966
    .local v23, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v31, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/FlashBarService/AppListController;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v20

    .line 967
    .local v20, "runningAffinity":Ljava/lang/String;
    if-eqz v21, :cond_0

    if-eqz v20, :cond_0

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_1

    :cond_0
    const/16 v31, 0x2

    move/from16 v0, p1

    move/from16 v1, v31

    if-ne v0, v1, :cond_2

    .line 968
    :cond_1
    move-object/from16 v0, v23

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 964
    :cond_2
    :goto_2
    add-int/lit8 v9, v9, -0x1

    goto :goto_1

    .line 914
    .end local v9    # "i":I
    .end local v18    # "p":Landroid/view/WindowManager$LayoutParams;
    .end local v20    # "runningAffinity":Ljava/lang/String;
    .end local v21    # "selectedAffinity":Ljava/lang/String;
    .end local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v30    # "w":Landroid/view/Window;
    :cond_3
    const v31, 0x7f030001

    const/16 v32, 0x0

    move/from16 v0, v31

    move-object/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogView:Landroid/view/View;

    goto/16 :goto_0

    .line 971
    .restart local v9    # "i":I
    .restart local v18    # "p":Landroid/view/WindowManager$LayoutParams;
    .restart local v20    # "runningAffinity":Ljava/lang/String;
    .restart local v21    # "selectedAffinity":Ljava/lang/String;
    .restart local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v30    # "w":Landroid/view/Window;
    :cond_4
    move-object/from16 v0, v23

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->userId:I

    move/from16 v31, v0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_2

    .line 976
    :try_start_0
    move-object/from16 v0, v23

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move/from16 v26, v0

    .line 977
    .local v26, "taskid":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mActivityManager:Landroid/app/ActivityManager;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/app/ActivityManager;->getTaskThumbnail(I)Landroid/app/ActivityManager$TaskThumbnail;

    move-result-object v25

    .line 978
    .local v25, "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    if-eqz v25, :cond_7

    move-object/from16 v0, v25

    iget-object v0, v0, Landroid/app/ActivityManager$TaskThumbnail;->mainThumbnail:Landroid/graphics/Bitmap;

    move-object/from16 v27, v0

    .line 980
    .local v27, "thumb":Landroid/graphics/Bitmap;
    :goto_3
    if-nez v27, :cond_5

    .line 981
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    check-cast v6, Landroid/graphics/drawable/BitmapDrawable;

    .line 982
    .local v6, "drawable":Landroid/graphics/drawable/BitmapDrawable;
    invoke-virtual {v6}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v27

    .line 985
    .end local v6    # "drawable":Landroid/graphics/drawable/BitmapDrawable;
    :cond_5
    if-eqz v27, :cond_2

    .line 986
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Ljava/util/List;->size()I

    move-result v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v32

    move/from16 v0, v31

    move/from16 v1, v32

    if-lt v0, v1, :cond_8

    .line 1152
    .end local v20    # "runningAffinity":Ljava/lang/String;
    .end local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v25    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .end local v26    # "taskid":I
    .end local v27    # "thumb":Landroid/graphics/Bitmap;
    :cond_6
    if-eqz v8, :cond_c

    .line 1153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v22

    .line 1154
    .local v22, "size":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v31

    move/from16 v0, v22

    move/from16 v1, v31

    if-lt v0, v1, :cond_b

    .line 1155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1156
    const/4 v9, 0x0

    :goto_4
    move/from16 v0, v22

    if-ge v9, v0, :cond_b

    .line 1157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v19

    check-cast v19, Landroid/widget/RelativeLayout;

    .line 1158
    .local v19, "r":Landroid/widget/RelativeLayout;
    new-instance v12, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual/range {v19 .. v19}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-direct {v12, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1159
    .local v12, "layoutMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutHeight:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutPadding:I

    move/from16 v32, v0

    add-int v31, v31, v32

    sub-int v32, v22, v9

    add-int/lit8 v32, v32, -0x1

    mul-int v15, v31, v32

    .line 1160
    .local v15, "margin":I
    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v12, v0, v15, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1161
    new-instance v31, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v31

    invoke-direct {v0, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1156
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 978
    .end local v12    # "layoutMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    .end local v15    # "margin":I
    .end local v19    # "r":Landroid/widget/RelativeLayout;
    .end local v22    # "size":I
    .restart local v20    # "runningAffinity":Ljava/lang/String;
    .restart local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v25    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .restart local v26    # "taskid":I
    :cond_7
    const/16 v27, 0x0

    goto/16 :goto_3

    .line 988
    .restart local v27    # "thumb":Landroid/graphics/Bitmap;
    :cond_8
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailTaskInfos:Ljava/util/List;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 990
    new-instance v5, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-direct {v5, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 991
    .local v5, "AppThumbnailLayout":Landroid/widget/RelativeLayout;
    new-instance v13, Landroid/view/ViewGroup$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutWidth:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutHeight:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-direct {v13, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 992
    .local v13, "layoutParam":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v5, v13}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 993
    const v31, 0x7f020073

    move/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 995
    new-instance v4, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-direct {v4, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 996
    .local v4, "AppThumbnailImageView":Landroid/widget/ImageView;
    sget-object v31, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 997
    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 998
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailImageWidth:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailImageHeight:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 999
    .local v14, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    const/16 v31, 0xd

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1000
    invoke-virtual {v5, v4, v14}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1002
    new-instance v10, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-direct {v10, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1003
    .local v10, "imageView":Landroid/widget/ImageView;
    const v31, 0x7f020076

    move/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1004
    new-instance v14, Landroid/widget/RelativeLayout$LayoutParams;

    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThbumNailCancelImageSize:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThbumNailCancelImageSize:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-direct {v14, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1005
    .restart local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v31, v0

    const v32, 0x7f0a0062

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v17, v0

    .line 1006
    .local v17, "minusIconTopMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v31, v0

    const v32, 0x7f0a0063

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v31

    move/from16 v0, v31

    float-to-int v0, v0

    move/from16 v16, v0

    .line 1007
    .local v16, "minusIconRightMargin":I
    move/from16 v0, v17

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1008
    move/from16 v0, v16

    iput v0, v14, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1009
    const/16 v31, 0x9

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1010
    const/16 v31, 0xa

    move/from16 v0, v31

    invoke-virtual {v14, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1011
    invoke-virtual {v10, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1012
    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1013
    new-instance v31, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$11;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    move-object/from16 v0, v31

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1024
    invoke-virtual {v5, v10}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1026
    if-nez v8, :cond_a

    .line 1027
    const/4 v8, 0x1

    .line 1028
    const/16 v31, 0x2

    move/from16 v0, p1

    move/from16 v1, v31

    if-eq v0, v1, :cond_a

    .line 1029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbIcon:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    move/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getIconByIndex(I)Landroid/graphics/drawable/Drawable;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1030
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v31

    if-nez v31, :cond_9

    .line 1031
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1032
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    new-instance v32, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1107
    :cond_a
    new-instance v31, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$13;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1121
    new-instance v31, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$14;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$14;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogThumbNailLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v28

    .line 1141
    new-instance v29, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v31

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1142
    .local v29, "thumbnailMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutHeight:I

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutPadding:I

    move/from16 v32, v0

    add-int v31, v31, v32

    mul-int v15, v31, v28

    .line 1143
    .restart local v15    # "margin":I
    const/16 v31, 0x0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v0, v29

    move/from16 v1, v31

    move/from16 v2, v32

    move/from16 v3, v33

    invoke-virtual {v0, v1, v15, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1144
    new-instance v31, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v31

    invoke-virtual {v5, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 1146
    .end local v4    # "AppThumbnailImageView":Landroid/widget/ImageView;
    .end local v5    # "AppThumbnailLayout":Landroid/widget/RelativeLayout;
    .end local v10    # "imageView":Landroid/widget/ImageView;
    .end local v13    # "layoutParam":Landroid/view/ViewGroup$LayoutParams;
    .end local v14    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v15    # "margin":I
    .end local v16    # "minusIconRightMargin":I
    .end local v17    # "minusIconTopMargin":I
    .end local v25    # "taskThumbnail":Landroid/app/ActivityManager$TaskThumbnail;
    .end local v26    # "taskid":I
    .end local v27    # "thumb":Landroid/graphics/Bitmap;
    .end local v29    # "thumbnailMargin":Landroid/view/ViewGroup$MarginLayoutParams;
    :catch_0
    move-exception v7

    .line 1147
    .local v7, "e":Ljava/lang/SecurityException;
    invoke-virtual {v7}, Ljava/lang/SecurityException;->printStackTrace()V

    goto/16 :goto_2

    .line 1164
    .end local v7    # "e":Ljava/lang/SecurityException;
    .end local v20    # "runningAffinity":Ljava/lang/String;
    .end local v23    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v22    # "size":I
    :cond_b
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v28

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->showHistoryBarUi(II)V

    .line 1165
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V

    .line 1169
    .end local v22    # "size":I
    :goto_5
    return-void

    .line 1167
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V

    goto :goto_5
.end method

.method private makeTraybarHelpPopupLayout()V
    .locals 12

    .prologue
    .line 1487
    :try_start_0
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 1488
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v9, 0x7f030017

    const/4 v10, 0x0

    invoke-virtual {v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 1490
    .local v8, "trayBarHelpPopupView":Landroid/view/View;
    if-nez v8, :cond_0

    .line 1614
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v8    # "trayBarHelpPopupView":Landroid/view/View;
    :goto_0
    return-void

    .line 1493
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .restart local v8    # "trayBarHelpPopupView":Landroid/view/View;
    :cond_0
    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->measure(II)V

    .line 1494
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    .line 1495
    .local v4, "mPopupWidth":I
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 1497
    .local v3, "mPopupHeight":I
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHelpPopupWindowTraybar()V

    .line 1498
    new-instance v9, Landroid/widget/PopupWindow;

    invoke-direct {v9, v8}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 1499
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v9, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 1500
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v9, v3}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1501
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1502
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 1503
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    new-instance v10, Landroid/graphics/drawable/ColorDrawable;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1504
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v9}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 1505
    const-string v9, "ro.csc.sales_code"

    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1506
    .local v6, "salesCode":Ljava/lang/String;
    const-string v9, "VZW"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1507
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    const/4 v10, 0x1

    const v11, 0x3f19999a    # 0.6f

    invoke-virtual {v9, v10, v11}, Landroid/widget/PopupWindow;->setDimBehind(ZF)V

    .line 1509
    :cond_1
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    new-instance v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$24;

    invoke-direct {v10, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$24;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v9, v10}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1518
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v9}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v9

    if-eqz v9, :cond_2

    .line 1519
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v9}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;

    invoke-direct {v10, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1537
    :cond_2
    new-instance v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;

    invoke-direct {v9, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v8, v9}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1548
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/Display;->getRotation()I

    move-result v9

    iput v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->lastDegrees:I

    .line 1549
    new-instance v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-direct {v9, p0, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    .line 1567
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v9}, Landroid/view/OrientationEventListener;->enable()V

    .line 1569
    const-string v9, "ATT"

    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1570
    const v9, 0x7f0f00ac

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1571
    .local v7, "textView":Landroid/widget/TextView;
    const v9, 0x7f08004f

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(I)V

    .line 1574
    .end local v7    # "textView":Landroid/widget/TextView;
    :cond_3
    const v9, 0x7f0f00ad

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 1575
    .local v0, "checkBox":Landroid/widget/CheckBox;
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsCheckBoxChecked:Z

    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1576
    new-instance v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$28;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$28;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v9}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1582
    const v9, 0x7f0f00ae

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 1583
    .local v1, "helpButton":Landroid/widget/Button;
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->misAvailableHelpHub:Z

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isKnoxMode()Z

    move-result v9

    if-nez v9, :cond_5

    .line 1584
    :cond_4
    new-instance v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$29;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$29;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1602
    :goto_1
    const v9, 0x7f0f00af

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 1603
    .local v5, "okButton":Landroid/widget/Button;
    new-instance v9, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$30;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$30;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v9}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1612
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v1    # "helpButton":Landroid/widget/Button;
    .end local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v3    # "mPopupHeight":I
    .end local v4    # "mPopupWidth":I
    .end local v5    # "okButton":Landroid/widget/Button;
    .end local v6    # "salesCode":Ljava/lang/String;
    .end local v8    # "trayBarHelpPopupView":Landroid/view/View;
    :catch_0
    move-exception v9

    goto/16 :goto_0

    .line 1599
    .restart local v0    # "checkBox":Landroid/widget/CheckBox;
    .restart local v1    # "helpButton":Landroid/widget/Button;
    .restart local v2    # "layoutInflater":Landroid/view/LayoutInflater;
    .restart local v3    # "mPopupHeight":I
    .restart local v4    # "mPopupWidth":I
    .restart local v6    # "salesCode":Ljava/lang/String;
    .restart local v8    # "trayBarHelpPopupView":Landroid/view/View;
    :cond_5
    const/16 v9, 0x8

    invoke-virtual {v1, v9}, Landroid/widget/Button;->setVisibility(I)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method private makeUsingHelpPopupLayout()V
    .locals 9

    .prologue
    .line 1384
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 1385
    .local v4, "layoutInflater":Landroid/view/LayoutInflater;
    const v6, 0x7f03001a

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1386
    .local v0, "bezelHelpPopupView":Landroid/view/View;
    if-nez v0, :cond_0

    .line 1482
    .end local v0    # "bezelHelpPopupView":Landroid/view/View;
    .end local v4    # "layoutInflater":Landroid/view/LayoutInflater;
    :goto_0
    return-void

    .line 1390
    .restart local v0    # "bezelHelpPopupView":Landroid/view/View;
    .restart local v4    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHelpPopupWindowUsing()V

    .line 1391
    new-instance v6, Landroid/widget/PopupWindow;

    invoke-direct {v6, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    .line 1392
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 1393
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 1394
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    .line 1395
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    .line 1396
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v7}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1397
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    new-instance v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$18;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$18;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v6, v7}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 1408
    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$19;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$19;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v0, v6}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1421
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Display;->getRotation()I

    move-result v6

    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->lastDegrees:I

    .line 1422
    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$20;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-direct {v6, p0, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$20;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    .line 1440
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v6}, Landroid/view/OrientationEventListener;->enable()V

    .line 1442
    const v6, 0x7f0f00ad

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 1443
    .local v1, "checkBox":Landroid/widget/CheckBox;
    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsCheckBoxChecked:Z

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1444
    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$21;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$21;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v6}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1450
    const v6, 0x7f0f00ae

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1452
    .local v3, "helpButton":Landroid/widget/Button;
    :try_start_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const-string v7, "com.samsung.helphub"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 1453
    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1471
    :goto_1
    const v6, 0x7f0f00af

    :try_start_2
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    .line 1472
    .local v5, "okButton":Landroid/widget/Button;
    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$23;

    invoke-direct {v6, p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$23;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1480
    .end local v0    # "bezelHelpPopupView":Landroid/view/View;
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    .end local v3    # "helpButton":Landroid/widget/Button;
    .end local v4    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v5    # "okButton":Landroid/widget/Button;
    :catch_0
    move-exception v6

    goto/16 :goto_0

    .line 1467
    .restart local v0    # "bezelHelpPopupView":Landroid/view/View;
    .restart local v1    # "checkBox":Landroid/widget/CheckBox;
    .restart local v3    # "helpButton":Landroid/widget/Button;
    .restart local v4    # "layoutInflater":Landroid/view/LayoutInflater;
    :catch_1
    move-exception v2

    .line 1468
    .local v2, "e":Ljava/lang/Exception;
    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method private openFlashBar()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 3054
    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 3055
    const/4 v0, 0x0

    .line 3057
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x66

    if-ne v1, v2, :cond_0

    .line 3058
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3066
    :goto_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V

    .line 3067
    return-void

    .line 3059
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_1

    .line 3060
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003f

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3061
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_2

    .line 3062
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3064
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04003d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method private prepareAnimationIcon()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3806
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 3807
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v5, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 3808
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 3809
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 3810
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 3811
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3812
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 3814
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->isPortrait()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3815
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const v2, 0x7f02007b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 3819
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const/16 v2, 0x5a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 3820
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 3821
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3822
    return-void

    .line 3817
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAnimationBackground:Landroid/widget/ImageView;

    const v2, 0x7f02007c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private saveFlashBarCurPosition(I)V
    .locals 3
    .param p1, "flashBarPosition"    # I

    .prologue
    .line 2501
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarPosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2502
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarPosition"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 2503
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2504
    return-void
.end method

.method private saveFlashBarHandlePosition(I)V
    .locals 3
    .param p1, "handlePosition"    # I

    .prologue
    .line 2516
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    const-string v1, "FlashBarHandlePosition"

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 2517
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v1, "FlashBarHandlePosition"

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 2518
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2519
    return-void
.end method

.method private setAppListHandlePosition(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/high16 v7, 0x425c0000    # 55.0f

    const/high16 v3, 0x42340000    # 45.0f

    const/4 v6, 0x0

    const/high16 v5, 0x42480000    # 50.0f

    const/high16 v4, 0x42c80000    # 100.0f

    .line 4584
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v2, 0x7f0a0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    .line 4585
    .local v0, "moveBtnWidth":I
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    packed-switch v1, :pswitch_data_0

    .line 4673
    :goto_0
    return-void

    .line 4587
    :pswitch_0
    div-int/lit8 v1, v0, 0x2

    if-gt p1, v1, :cond_0

    .line 4588
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->top:I

    .line 4597
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->saveFlashBarHandlePosition(I)V

    goto :goto_0

    .line 4589
    :cond_0
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    if-le v1, v2, :cond_1

    .line 4590
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 4592
    :cond_1
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_2

    .line 4593
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 4595
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    goto :goto_1

    .line 4600
    :pswitch_1
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_3

    .line 4601
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->bottom:I

    .line 4610
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->saveFlashBarHandlePosition(I)V

    goto :goto_0

    .line 4602
    :cond_3
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    if-le v1, v2, :cond_4

    .line 4603
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 4605
    :cond_4
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_5

    .line 4606
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 4608
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    .line 4613
    :pswitch_2
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfigChanged:Z

    if-eqz v1, :cond_7

    .line 4614
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_6

    .line 4615
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 4619
    :goto_3
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfigChanged:Z

    .line 4640
    :goto_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->saveFlashBarHandlePosition(I)V

    goto/16 :goto_0

    .line 4617
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_3

    .line 4620
    :cond_7
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z

    if-eqz v1, :cond_a

    .line 4621
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_9

    .line 4622
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 4626
    :cond_8
    :goto_5
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z

    goto :goto_4

    .line 4623
    :cond_9
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    if-nez v1, :cond_8

    .line 4624
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_5

    .line 4628
    :cond_a
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_b

    .line 4629
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->left:I

    .line 4638
    :goto_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_4

    .line 4630
    :cond_b
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    if-le v1, v2, :cond_c

    .line 4631
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_6

    .line 4633
    :cond_c
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_d

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_d

    .line 4634
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_6

    .line 4636
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    goto :goto_6

    .line 4643
    :pswitch_3
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfigChanged:Z

    if-eqz v1, :cond_f

    .line 4644
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_e

    .line 4645
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 4649
    :goto_7
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfigChanged:Z

    .line 4670
    :goto_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->saveFlashBarHandlePosition(I)V

    goto/16 :goto_0

    .line 4647
    :cond_e
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_7

    .line 4650
    :cond_f
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z

    if-eqz v1, :cond_12

    .line 4651
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_11

    .line 4652
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    int-to-float v3, v0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 4656
    :cond_10
    :goto_9
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChanged:Z

    goto :goto_8

    .line 4653
    :cond_11
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    if-nez v1, :cond_10

    .line 4654
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_9

    .line 4658
    :cond_12
    div-int/lit8 v1, v0, 0x2

    sub-int v1, p1, v1

    if-gtz v1, :cond_13

    .line 4659
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iput v6, v1, Landroid/graphics/Rect;->right:I

    .line 4668
    :goto_a
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastHandlePosition:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_8

    .line 4660
    :cond_13
    div-int/lit8 v1, v0, 0x2

    add-int/2addr v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    if-le v1, v2, :cond_14

    .line 4661
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x64

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_a

    .line 4663
    :cond_14
    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_15

    int-to-float v1, p1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v4

    cmpg-float v1, v1, v7

    if-gez v1, :cond_15

    .line 4664
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    const/16 v2, 0x32

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_a

    .line 4666
    :cond_15
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandlePosition:Landroid/graphics/Rect;

    int-to-float v2, p1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v4

    float-to-int v2, v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    goto :goto_a

    .line 4585
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V
    .locals 5
    .param p1, "ani"    # Landroid/view/animation/Animation;
    .param p2, "expand"    # Z
    .param p3, "finishtray"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 3128
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    if-nez v1, :cond_1

    .line 3220
    :cond_0
    :goto_0
    return-void

    .line 3131
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 3132
    .local v0, "currentAni":Landroid/view/animation/Animation;
    if-eqz v0, :cond_2

    .line 3133
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3134
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 3136
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-ne v1, v3, :cond_2

    .line 3140
    :cond_2
    if-eqz p2, :cond_7

    .line 3141
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 3142
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3173
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-direct {p0, v1, v2, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZ)V

    .line 3175
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    if-eqz v1, :cond_4

    .line 3176
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->hideDragAndDropHelpDialog()V

    .line 3178
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "do_not_show_help_popup_traybar"

    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isKnoxMode()Z

    move-result v1

    if-nez v1, :cond_6

    .line 3184
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$34;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$34;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 3191
    :cond_6
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->openBezelUI()V

    goto :goto_0

    .line 3214
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideTraybarHelpPopup()V

    .line 3215
    if-nez p3, :cond_0

    .line 3217
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeBezelUI()V

    goto :goto_0
.end method

.method private setImageToMoveButton(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "expand"    # Z

    .prologue
    const/4 v1, 0x1

    .line 2533
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsPressMoveBtn:Z

    .line 2534
    .local v0, "press":Z
    packed-switch p1, :pswitch_data_0

    .line 2596
    :goto_0
    return-void

    .line 2536
    :pswitch_0
    if-ne p2, v1, :cond_1

    .line 2537
    if-ne v0, v1, :cond_0

    .line 2538
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f02010c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2540
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f020109

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2543
    :cond_1
    if-ne v0, v1, :cond_2

    .line 2544
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f02010b

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2546
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f02010a

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2551
    :pswitch_1
    if-ne p2, v1, :cond_4

    .line 2552
    if-ne v0, v1, :cond_3

    .line 2553
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2555
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b5

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2558
    :cond_4
    if-ne v0, v1, :cond_5

    .line 2559
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b7

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2561
    :cond_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200b6

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2566
    :pswitch_2
    if-ne p2, v1, :cond_7

    .line 2567
    if-ne v0, v1, :cond_6

    .line 2568
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200eb

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2570
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200e8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2573
    :cond_7
    if-ne v0, v1, :cond_8

    .line 2574
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200ea

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2576
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200e9

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 2581
    :pswitch_3
    if-ne p2, v1, :cond_a

    .line 2582
    if-ne v0, v1, :cond_9

    .line 2583
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f020100

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2585
    :cond_9
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200fd

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2588
    :cond_a
    if-ne v0, v1, :cond_b

    .line 2589
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200ff

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2591
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const v2, 0x7f0200fe

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2534
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private showHistoryBarUi(II)V
    .locals 8
    .param p1, "type"    # I
    .param p2, "cnt"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x0

    .line 1172
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v6, :cond_1

    .line 1173
    const/4 v0, 0x0

    .line 1174
    .local v0, "gravity":I
    const/4 v3, 0x0

    .local v3, "x":I
    const/4 v4, 0x0

    .line 1175
    .local v4, "y":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v2, v6, 0xa

    .line 1176
    .local v2, "padding":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    packed-switch v6, :pswitch_data_0

    .line 1198
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 1199
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1200
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1201
    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1202
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialogScrollView:Landroid/widget/ScrollView;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v7

    invoke-virtual {v6, v5, v5, v5, v7}, Landroid/widget/ScrollView;->setPaddingRelative(IIII)V

    .line 1203
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getVisibility()I

    move-result v5

    if-nez v5, :cond_0

    .line 1204
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1206
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->show()V

    .line 1207
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarUI()V

    .line 1209
    .end local v0    # "gravity":I
    .end local v1    # "l":Landroid/view/WindowManager$LayoutParams;
    .end local v2    # "padding":I
    .end local v3    # "x":I
    .end local v4    # "y":I
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V

    .line 1210
    return-void

    .line 1178
    .restart local v0    # "gravity":I
    .restart local v2    # "padding":I
    .restart local v3    # "x":I
    .restart local v4    # "y":I
    :pswitch_0
    const/16 v0, 0x33

    .line 1179
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getWidth()I

    move-result v6

    add-int v3, v6, v2

    .line 1180
    if-ne p1, v7, :cond_2

    move v4, v5

    .line 1181
    :goto_1
    goto :goto_0

    .line 1180
    :cond_2
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findHistoryBarPosisition(I)I

    move-result v4

    goto :goto_1

    .line 1183
    :pswitch_1
    const/16 v0, 0x35

    .line 1184
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v6}, Landroid/widget/ScrollView;->getWidth()I

    move-result v6

    add-int v3, v6, v2

    .line 1185
    if-ne p1, v7, :cond_3

    move v4, v5

    .line 1186
    :goto_2
    goto :goto_0

    .line 1185
    :cond_3
    invoke-direct {p0, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findHistoryBarPosisition(I)I

    move-result v4

    goto :goto_2

    .line 1188
    :pswitch_2
    const/16 v0, 0x33

    .line 1189
    if-ne p1, v7, :cond_4

    move v3, v5

    .line 1190
    :goto_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    .line 1191
    goto :goto_0

    .line 1189
    :cond_4
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    goto :goto_3

    .line 1193
    :pswitch_3
    const/16 v0, 0x53

    .line 1194
    if-ne p1, v7, :cond_5

    move v3, v5

    .line 1195
    :goto_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v6}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    goto :goto_0

    .line 1194
    :cond_5
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    goto :goto_4

    .line 1176
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private showTraybarHelpPopup()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1651
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    if-eqz v2, :cond_1

    .line 1653
    const-string v2, "AppListBezelWindow"

    const-string v3, "cacncel showTrayBarHelpPopup() / Reason : mIsBezelOpend == true"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1654
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1655
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1691
    :cond_0
    :goto_0
    return-void

    .line 1660
    :cond_1
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v1

    .line 1661
    .local v1, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getSealedState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1665
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V

    .line 1666
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeTraybarHelpPopupLayout()V

    .line 1668
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_5

    .line 1669
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_6

    .line 1673
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    if-eqz v2, :cond_3

    .line 1674
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v2}, Landroid/view/OrientationEventListener;->disable()V

    .line 1675
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    .line 1677
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1678
    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    .line 1679
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1680
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1682
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-static {v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v3

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1687
    :cond_5
    :goto_1
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 1688
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 1689
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    goto :goto_0

    .line 1684
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    const/16 v4, 0x33

    invoke-virtual {v2, v3, v4, v5, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_1
.end method

.method private showUsingHelpPopup()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 1618
    const-string v1, "ro.csc.sales_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1619
    .local v0, "salesCode":Ljava/lang/String;
    const-string v1, "ATT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1648
    :cond_0
    :goto_0
    return-void

    .line 1622
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "do_not_show_help_popup_using"

    const/4 v3, -0x2

    invoke-static {v1, v2, v4, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eq v1, v7, :cond_0

    .line 1626
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1629
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsUsingHelpPopupOpened:Z

    .line 1630
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V

    .line 1631
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeUsingHelpPopupLayout()V

    .line 1632
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_0

    .line 1633
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1634
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    if-eqz v1, :cond_2

    .line 1635
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v1}, Landroid/view/OrientationEventListener;->disable()V

    .line 1636
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->orientationListener:Landroid/view/OrientationEventListener;

    .line 1638
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 1639
    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    .line 1640
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1641
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 1643
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-static {v2, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 1645
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowUsing:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    const/16 v3, 0x33

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_0
.end method

.method private startCollapseTimer()V
    .locals 4

    .prologue
    const/16 v2, 0xc9

    .line 681
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 682
    .local v0, "msg":Landroid/os/Message;
    iput v2, v0, Landroid/os/Message;->what:I

    .line 683
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 684
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 685
    :cond_0
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-nez v1, :cond_1

    .line 686
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 687
    :cond_1
    return-void
.end method

.method private startFlashBarAnimation(ZZ)V
    .locals 5
    .param p1, "bStart"    # Z
    .param p2, "bExpand"    # Z

    .prologue
    const/16 v4, 0x68

    const/16 v3, 0x66

    const/16 v2, 0x65

    .line 3101
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStartFlashBar:Z

    .line 3102
    const/4 v0, 0x0

    .line 3103
    .local v0, "ani":Landroid/view/animation/Animation;
    if-eqz p1, :cond_3

    .line 3104
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v1, v3, :cond_0

    .line 3105
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040045

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3122
    :goto_0
    if-nez p1, :cond_7

    const/4 v1, 0x1

    :goto_1
    invoke-direct {p0, v0, p2, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setFlashBarAnimation(Landroid/view/animation/Animation;ZZ)V

    .line 3123
    return-void

    .line 3106
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v1, v2, :cond_1

    .line 3107
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3108
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v1, v4, :cond_2

    .line 3109
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3111
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040049

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3113
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v1, v3, :cond_4

    .line 3114
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002d

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3115
    :cond_4
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v1, v2, :cond_5

    .line 3116
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040034

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3117
    :cond_5
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v1, v4, :cond_6

    .line 3118
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040033

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3120
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040031

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 3122
    :cond_7
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private unregisterListeners()V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 436
    const/16 v5, 0xa

    new-array v4, v5, [Landroid/view/View;

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    aput-object v7, v4, v5

    const/4 v5, 0x2

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    aput-object v7, v4, v5

    const/4 v5, 0x3

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    aput-object v7, v4, v5

    const/4 v5, 0x4

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn:Landroid/widget/Button;

    aput-object v7, v4, v5

    const/4 v5, 0x5

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateBtn_h:Landroid/widget/Button;

    aput-object v7, v4, v5

    const/4 v5, 0x6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    aput-object v7, v4, v5

    const/4 v5, 0x7

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    aput-object v7, v4, v5

    const/16 v5, 0x8

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;

    aput-object v7, v4, v5

    const/16 v7, 0x9

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    :goto_0
    aput-object v5, v4, v7

    .line 449
    .local v4, "views":[Landroid/view/View;
    move-object v0, v4

    .local v0, "arr$":[Landroid/view/View;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    .line 450
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 451
    invoke-virtual {v3, v6}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 452
    invoke-virtual {v3, v6}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 453
    invoke-virtual {v3, v6}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 454
    invoke-virtual {v3, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 449
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .end local v0    # "arr$":[Landroid/view/View;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "views":[Landroid/view/View;
    :cond_1
    move-object v5, v6

    .line 436
    goto :goto_0

    .line 457
    .restart local v0    # "arr$":[Landroid/view/View;
    .restart local v1    # "i$":I
    .restart local v2    # "len$":I
    .restart local v4    # "views":[Landroid/view/View;
    :cond_2
    return-void
.end method

.method private updateAppListPosition(IZZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "sticky"    # Z
    .param p3, "expand"    # Z

    .prologue
    .line 2599
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V

    .line 2600
    return-void
.end method

.method private updateAppListPosition(IZZZ)V
    .locals 24
    .param p1, "position"    # I
    .param p2, "sticky"    # Z
    .param p3, "expand"    # Z
    .param p4, "savePosition"    # Z

    .prologue
    .line 2603
    const/16 v20, 0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    const/16 v20, 0x1

    move/from16 v0, p3

    move/from16 v1, v20

    if-ne v0, v1, :cond_0

    .line 2604
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateRunningAppList()V

    .line 2606
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v14

    .line 2607
    .local v14, "l":Landroid/view/WindowManager$LayoutParams;
    new-instance v4, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/HorizontalScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v4, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2608
    .local v4, "appListMarginHorizontal":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v5, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2609
    .local v5, "appListMarginVertical":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v17, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2610
    .local v17, "moveBtnLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-direct {v6, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2612
    .local v6, "appTrayBGLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2613
    .local v10, "editLayout":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2615
    .local v11, "editLayout_h":Landroid/widget/RelativeLayout$LayoutParams;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a0018

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v18, v0

    .line 2616
    .local v18, "moveBtnWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a0019

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v16, v0

    .line 2617
    .local v16, "moveBtnHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0023

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v7, v0

    .line 2618
    .local v7, "applistHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0019

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v20, v20, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v22, v0

    add-int v21, v21, v22

    sub-int v13, v20, v21

    .line 2622
    .local v13, "flashBarHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a0020

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v19, v0

    .line 2623
    .local v19, "scrollerMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a0053

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v8, v0

    .line 2625
    .local v8, "applistSidePadding":I
    const/4 v9, 0x0

    .line 2626
    .local v9, "applistTopPadding":I
    const/16 v20, 0x67

    move/from16 v0, p1

    move/from16 v1, v20

    if-ne v0, v1, :cond_2

    const/4 v12, 0x0

    .line 2628
    .local v12, "editMargin":I
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 2937
    :goto_1
    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setImageToMoveButton(IZ)V

    .line 2938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v21

    invoke-direct {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-interface {v0, v1, v14}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/view/View;->setVisibility(I)V

    .line 2944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->updateForeground()V

    .line 2945
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    .line 2947
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V

    .line 2948
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p1

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updatePocketTrayLayout(IZZ)V

    .line 2950
    if-eqz p4, :cond_1

    .line 2951
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->saveFlashBarCurPosition(I)V

    .line 2953
    :cond_1
    return-void

    .line 2626
    .end local v12    # "editMargin":I
    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    sub-int v12, v16, v20

    goto/16 :goto_0

    .line 2630
    .restart local v12    # "editMargin":I
    :pswitch_0
    const/16 v20, 0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    .line 2631
    const/16 v20, 0x1

    move/from16 v0, p3

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    .line 2632
    const/16 v20, 0x0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2633
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v20

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2634
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2635
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2636
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v20, v7, v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2637
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2638
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f08001a

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2639
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2640
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2663
    :goto_2
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v16

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2664
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2665
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2666
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2668
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2669
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v21, v0

    const v22, 0x7f0a0049

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2670
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2672
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->isPortrait()Z

    move-result v20

    if-eqz v20, :cond_5

    .line 2673
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f020107

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2676
    :goto_3
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2677
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2678
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2680
    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2681
    move/from16 v0, v16

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2683
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2642
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2643
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2644
    move/from16 v0, v18

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2645
    move/from16 v0, v16

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2646
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2647
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f080019

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2649
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2650
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2653
    :cond_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I

    move/from16 v20, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v21

    sub-int v20, v20, v21

    div-int/lit8 v21, v18, 0x2

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2654
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I

    move/from16 v20, v0

    div-int/lit8 v21, v18, 0x2

    sub-int v21, v13, v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2655
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2656
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2657
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v20, v7, v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2658
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2659
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2660
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 2675
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f020108

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_3

    .line 2686
    :pswitch_1
    const/16 v20, 0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_7

    .line 2687
    const/16 v20, 0x1

    move/from16 v0, p3

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    .line 2688
    const/16 v20, 0x0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2689
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    sub-int v20, v20, v13

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2690
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2691
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2692
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2693
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2694
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f08001a

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2695
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2696
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2721
    :goto_4
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v16

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v11, v0, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2722
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2723
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2724
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2726
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v16

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2727
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v21, v0

    const v22, 0x7f0a0049

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v4, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2728
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v21

    invoke-direct {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2730
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->isPortrait()Z

    move-result v20

    if-eqz v20, :cond_8

    .line 2731
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200b3

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2734
    :goto_5
    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v22, v0

    add-int v21, v21, v22

    sub-int v21, v16, v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2735
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2736
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2738
    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2739
    move/from16 v0, v16

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2741
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2699
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2700
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    sub-int v20, v20, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2701
    move/from16 v0, v18

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2702
    move/from16 v0, v16

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2703
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2704
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2705
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f080019

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2706
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2707
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 2711
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I

    move/from16 v20, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v21

    sub-int v20, v20, v21

    div-int/lit8 v21, v18, 0x2

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2712
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I

    move/from16 v20, v0

    div-int/lit8 v21, v16, 0x2

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2713
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2714
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2715
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2716
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2717
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 2718
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 2733
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200b4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_5

    .line 2744
    :pswitch_2
    const/16 v20, 0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_c

    .line 2745
    const/16 v20, 0x1

    move/from16 v0, p3

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    .line 2746
    const/16 v20, 0x0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2747
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v20

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2748
    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2749
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2750
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2751
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v20, v7, v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2752
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200e8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2753
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f08001a

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2754
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2755
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2756
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_9

    .line 2757
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2758
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2761
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 2762
    .local v15, "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    const/16 v20, 0x3

    move/from16 v0, v20

    iput v0, v15, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    .line 2763
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2796
    .end local v15    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_a
    :goto_6
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v10, v0, v1, v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_d

    .line 2799
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2803
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2805
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_e

    .line 2806
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a001f

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v9, v0

    .line 2811
    :goto_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getEditAreaHeight()I

    move-result v21

    sub-int v20, v20, v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    sub-int v20, v20, v9

    move/from16 v0, v20

    iput v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2815
    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v0, v20

    move/from16 v1, v19

    move/from16 v2, v21

    invoke-virtual {v5, v0, v9, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2817
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    add-int v21, v8, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v23

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v8, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2821
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->isPortrait()Z

    move-result v20

    if-eqz v20, :cond_f

    .line 2822
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200e6

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2825
    :goto_9
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2826
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2827
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2829
    move/from16 v0, v16

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2830
    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2832
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2766
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    rsub-int/lit8 v20, v20, 0x0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2767
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v20

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v21

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2768
    move/from16 v0, v16

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2769
    move/from16 v0, v18

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2770
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2771
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2772
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f080019

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2773
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2775
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 2776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2777
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_6

    .line 2781
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I

    move/from16 v20, v0

    div-int/lit8 v21, v18, 0x2

    sub-int v21, v13, v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2782
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    add-int v20, v20, v21

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v21

    sub-int v20, v20, v21

    div-int/lit8 v21, v18, 0x2

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2783
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2784
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2785
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2786
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v20, v7, v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200e8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2788
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2789
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2790
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_a

    .line 2791
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2792
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_6

    .line 2801
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_7

    .line 2808
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a001e

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v9, v0

    goto/16 :goto_8

    .line 2824
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200e7

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_9

    .line 2835
    :pswitch_3
    const/16 v20, 0x1

    move/from16 v0, p2

    move/from16 v1, v20

    if-ne v0, v1, :cond_15

    .line 2836
    const/16 v20, 0x1

    move/from16 v0, p3

    move/from16 v1, v20

    if-ne v0, v1, :cond_14

    .line 2838
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z

    move/from16 v20, v0

    if-nez v20, :cond_13

    .line 2839
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    move/from16 v20, v0

    if-eqz v20, :cond_12

    .line 2840
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    sub-int v20, v20, v7

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2841
    iput v7, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2849
    :goto_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v20

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2850
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2851
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2852
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2853
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200fd

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2854
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f08001a

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2855
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2856
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2857
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_10

    .line 2858
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2859
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x5

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2860
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2861
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2864
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;

    .line 2865
    .restart local v15    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    const/16 v20, 0x5

    move/from16 v0, v20

    iput v0, v15, Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;->gravity:I

    .line 2866
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2900
    .end local v15    # "lp":Landroid/support/v4/widget/DrawerLayoutMW$LayoutParams;
    :cond_11
    :goto_b
    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v10, v12, v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2902
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_16

    .line 2903
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2907
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2909
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 2910
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a001f

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v9, v0

    .line 2915
    :goto_d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getEditAreaHeight()I

    move-result v21

    sub-int v20, v20, v21

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    sub-int v20, v20, v9

    move/from16 v0, v20

    iput v0, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2919
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v5, v0, v9, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2921
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    new-instance v21, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2922
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v21

    add-int v22, v8, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v23

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 2926
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200f8

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2927
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    sub-int v20, v16, v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v6, v0, v1, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2928
    iput v7, v6, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2929
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v6, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2931
    move/from16 v0, v16

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2932
    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2934
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    goto/16 :goto_1

    .line 2843
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    move/from16 v21, v0

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2844
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    goto/16 :goto_a

    .line 2847
    :cond_13
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z

    goto/16 :goto_a

    .line 2868
    :cond_14
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    move/from16 v20, v0

    sub-int v20, v20, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2869
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v20

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v21

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2870
    move/from16 v0, v16

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2871
    move/from16 v0, v18

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2872
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2873
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2874
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f080019

    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2875
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2876
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2877
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_11

    .line 2878
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2879
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x5

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setGravity(I)V

    goto/16 :goto_b

    .line 2883
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionX:I

    move/from16 v20, v0

    div-int/lit8 v21, v16, 0x2

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2884
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPositionY:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    add-int v20, v20, v21

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v21

    sub-int v20, v20, v21

    div-int/lit8 v21, v18, 0x2

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2885
    iput v13, v14, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2886
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v21

    sub-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v14, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2887
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getAppListHandlePosition()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2888
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2889
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const v21, 0x7f0200fd

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2890
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ScrollView;->setVisibility(I)V

    .line 2891
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppTrayBG:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2892
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    if-eqz v20, :cond_11

    .line 2893
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2894
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x5

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 2895
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 2896
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout_h:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_b

    .line 2905
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_c

    .line 2912
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    move-object/from16 v20, v0

    const v21, 0x7f0a001e

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v9, v0

    goto/16 :goto_d

    .line 2628
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updatePocketTrayButtonText()V
    .locals 5

    .prologue
    const v1, 0x7f080002

    const v2, 0x7f080001

    .line 2956
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNeedUpdatePocketTrayButtonText:Z

    if-nez v0, :cond_1

    .line 2975
    :cond_0
    :goto_0
    return-void

    .line 2960
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 2965
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2966
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    if-eqz v0, :cond_2

    .line 2967
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v4, :cond_4

    :goto_2
    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2969
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080003

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2970
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080004

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2972
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 2973
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    .line 2974
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpBtn:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2965
    goto :goto_1

    :cond_4
    move v1, v2

    .line 2967
    goto :goto_2
.end method

.method private updatePocketTrayLayout(IZZ)V
    .locals 11
    .param p1, "position"    # I
    .param p2, "isStarting"    # Z
    .param p3, "forceClose"    # Z

    .prologue
    const v9, 0x7f02003a

    const/4 v10, 0x0

    .line 2979
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-nez v7, :cond_0

    .line 3043
    :goto_0
    return-void

    .line 2983
    :cond_0
    new-instance v4, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v4, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2984
    .local v4, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v7}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2986
    .local v5, "scrollLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0019

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v0, v7

    .line 2987
    .local v0, "moveBtnHeight":I
    const/4 v1, 0x0

    .line 2993
    .local v1, "openButtonHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a006b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v1, v7

    .line 2995
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    const v8, 0x7f0a0068

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v2, v7

    .line 2996
    .local v2, "openPocketTrayHeght":I
    iget v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2997
    .local v6, "scrollViewHeight":I
    move v3, v6

    .line 2999
    .local v3, "pocketTopMargin":I
    if-eqz p3, :cond_1

    .line 3000
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v8

    add-int/2addr v8, v1

    sub-int v3, v7, v8

    .line 3001
    move v6, v3

    .line 3002
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v7, v9}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 3003
    iput-boolean v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    .line 3023
    :goto_1
    packed-switch p1, :pswitch_data_0

    .line 3033
    :goto_2
    iput v6, v5, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 3034
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v8, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3035
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3037
    if-nez p2, :cond_5

    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    if-nez v7, :cond_5

    .line 3038
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTrayBody:Landroid/widget/LinearLayout;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0

    .line 3005
    :cond_1
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    if-eqz v7, :cond_3

    .line 3006
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    add-int v8, v1, v2

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v9

    add-int/2addr v8, v9

    sub-int v3, v7, v8

    .line 3007
    if-eqz p2, :cond_2

    .line 3008
    add-int v6, v3, v2

    .line 3012
    :goto_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    const v8, 0x7f02003b

    invoke-virtual {v7, v8}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 3010
    :cond_2
    move v6, v3

    goto :goto_3

    .line 3014
    :cond_3
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getApplistIndicatorSize()I

    move-result v8

    add-int/2addr v8, v1

    sub-int v6, v7, v8

    .line 3015
    if-eqz p2, :cond_4

    .line 3016
    sub-int v3, v6, v2

    .line 3020
    :goto_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v7, v9}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 3018
    :cond_4
    move v3, v6

    goto :goto_4

    .line 3025
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0200ec

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 3026
    invoke-virtual {v4, v10, v3, v10, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 3029
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    const v8, 0x7f0200f9

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 3030
    iget v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I

    add-int/2addr v7, v8

    sub-int v7, v0, v7

    invoke-virtual {v4, v7, v3, v10, v10}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    .line 3040
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updatePocketTrayButtonText()V

    .line 3041
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTrayBody:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 3023
    nop

    :pswitch_data_0
    .packed-switch 0x67
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateRunningAppList()V
    .locals 0

    .prologue
    .line 678
    return-void
.end method

.method private updateScrollPositionTemplate()V
    .locals 4

    .prologue
    .line 4990
    const-string v0, "AppListBezelWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ScrollView total height : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4991
    const-string v0, "AppListBezelWindow"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current ScrollY : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    invoke-virtual {v2}, Landroid/widget/ScrollView;->getScrollY()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4993
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;

    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$73;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$73;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->post(Ljava/lang/Runnable;)Z

    .line 5003
    return-void
.end method


# virtual methods
.method public animationRecentIconByHomeKey()V
    .locals 0

    .prologue
    .line 3787
    return-void
.end method

.method public canCloseWindow()Z
    .locals 1

    .prologue
    .line 5184
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5185
    const/4 v0, 0x0

    .line 5187
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public cancelAllMessages()V
    .locals 6

    .prologue
    const/16 v5, 0xcd

    const/16 v4, 0xcc

    const/16 v3, 0xcb

    const/16 v2, 0xca

    const/16 v1, 0xc9

    .line 4949
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4950
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4952
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4953
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 4955
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4956
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 4958
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 4959
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 4961
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4962
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4964
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 4965
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 4967
    :cond_5
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 4968
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0xce

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 4970
    :cond_6
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastPostAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4973
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4975
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 4979
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowStatusReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 4983
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserStoppedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 4986
    :goto_2
    return-void

    .line 4984
    :catch_0
    move-exception v0

    goto :goto_2

    .line 4980
    :catch_1
    move-exception v0

    goto :goto_1

    .line 4976
    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public checkAppListLayout()Z
    .locals 2

    .prologue
    .line 4826
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 4827
    const/4 v0, 0x1

    .line 4828
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public closeBezelUI()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 5195
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideUsingHelpPopup()V

    .line 5196
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerLockMode(I)V

    .line 5197
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayoutMW;->closeDrawer(Landroid/view/View;)V

    .line 5198
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updatePocketTrayLayout(IZZ)V

    .line 5199
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v1, 0xcf

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 5200
    return-void
.end method

.method public closePocketTray()V
    .locals 2

    .prologue
    .line 4177
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarOpenListener:Landroid/view/View$OnClickListener;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTrayHeader:Landroid/widget/RelativeLayout;

    invoke-interface {v0, v1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 4178
    return-void
.end method

.method public createConfirmDialog(IZ)V
    .locals 10
    .param p1, "index"    # I
    .param p2, "bFromEdit"    # Z

    .prologue
    const/4 v6, 0x1

    .line 3450
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    if-eqz v3, :cond_0

    .line 3505
    :goto_0
    return-void

    .line 3452
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    .line 3453
    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbFromEdit:Z

    .line 3454
    iput p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDelIndex:I

    .line 3455
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080015

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080017

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget v9, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDelIndex:I

    invoke-virtual {v8, v9, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppTitle(IZ)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$42;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080012

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$41;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$41;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3476
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    .line 3477
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3478
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3479
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3480
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3481
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3482
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3483
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$43;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$43;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 3499
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$44;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$44;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_0
.end method

.method public createGestureHelp()V
    .locals 0

    .prologue
    .line 5307
    return-void
.end method

.method public createResetConfirmDialog()V
    .locals 6

    .prologue
    .line 3508
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    if-eqz v3, :cond_0

    .line 3558
    :goto_0
    return-void

    .line 3510
    :cond_0
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    .line 3511
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f08004a

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f08004b

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$46;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$46;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080012

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$45;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$45;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3529
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    .line 3530
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3531
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3532
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3533
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3534
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3535
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3536
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$47;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$47;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 3552
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResetConfirmDialog:Landroid/app/AlertDialog;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$48;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$48;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    goto/16 :goto_0
.end method

.method public createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 8
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "defaultTitle"    # Ljava/lang/CharSequence;

    .prologue
    .line 3344
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 3345
    .local v2, "layoutInflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030010

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 3346
    .local v0, "createDialogView":Landroid/view/View;
    const v5, 0x7f0f0085

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    .line 3347
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3348
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5, p2}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 3349
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->selectAll()V

    .line 3350
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    const/16 v6, 0x4000

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setInputType(I)V

    .line 3351
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->requestFocus()Z

    .line 3352
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    const-string v6, "disablePrediction=true;disableEmoticonInput=true;disableVoiceInput=true;noMicrophoneKey=true;inputType=DisableOCR"

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 3353
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;

    new-instance v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 3382
    new-instance v5, Landroid/app/AlertDialog$Builder;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080013

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080011

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$40;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$40;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v7, 0x7f080012

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$39;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$39;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 3424
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    .line 3425
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3426
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    .line 3427
    .local v4, "w":Landroid/view/Window;
    const v5, 0x10200

    invoke-virtual {v4, v5}, Landroid/view/Window;->addFlags(I)V

    .line 3429
    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Landroid/view/Window;->clearFlags(I)V

    .line 3430
    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 3431
    .local v3, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v5, 0x7d8

    iput v5, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3432
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v5

    iput-object v5, v3, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3433
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    .line 3434
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V

    .line 3435
    const/16 v5, 0x15

    invoke-virtual {v4, v5}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 3436
    return-void
.end method

.method public createUnableDialog(Ljava/lang/CharSequence;)V
    .locals 6
    .param p1, "message"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 3577
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateFlashBarState(ZZ)Z

    .line 3578
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const/4 v5, 0x5

    invoke-direct {v3, v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080013

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v5, 0x7f080011

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$49;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$49;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 3587
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    .line 3588
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    .line 3589
    .local v2, "w":Landroid/view/Window;
    const/16 v3, 0x300

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 3590
    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 3591
    .local v1, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3592
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMainFrame:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v1, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3593
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 3594
    return-void
.end method

.method public dismissConfirmDialog()V
    .locals 1

    .prologue
    .line 3569
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbSynchronizeConfirmDialog:Z

    .line 3570
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3571
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3572
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfirmDialog:Landroid/app/AlertDialog;

    .line 3574
    :cond_0
    return-void
.end method

.method public dismissGestureOverlayHelp()V
    .locals 0

    .prologue
    .line 5310
    return-void
.end method

.method public dismissTemplateDialog()V
    .locals 2

    .prologue
    .line 3439
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 3440
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 3441
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 3442
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 3444
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 3445
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    .line 3447
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    return-void
.end method

.method public dismissUnableDialog()V
    .locals 1

    .prologue
    .line 3597
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3598
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3599
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUnableDialog:Landroid/app/AlertDialog;

    .line 3601
    :cond_0
    return-void
.end method

.method public dismissViewPagerAppList()V
    .locals 0

    .prologue
    .line 5304
    return-void
.end method

.method public getFlashBarEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 804
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 805
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    .line 807
    :cond_0
    :goto_0
    return v0

    .line 805
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getFlashBarState()Z
    .locals 1

    .prologue
    .line 797
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarState()Z

    move-result v0

    .line 800
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGuidelineWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    return-object v0
.end method

.method public getItemIndex(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3983
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getCount()I

    move-result v0

    .line 3984
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 3985
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 3986
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 3987
    const v3, 0x7f0f0059

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 3997
    .end local v1    # "i":I
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    :goto_1
    return v1

    .line 3989
    .restart local v1    # "i":I
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_1
    const v3, 0x7f0f005c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3991
    const v3, 0x7f0f005b

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3993
    const v3, 0x7f0f005a

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3984
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3997
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getRunningAppItemIndex(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3959
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getCount()I

    move-result v0

    .line 3960
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 3961
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mRunningAppGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 3962
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 3963
    const v3, 0x7f0f00e6

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 3969
    .end local v1    # "i":I
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    :goto_1
    return v1

    .line 3965
    .restart local v1    # "i":I
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_1
    const v3, 0x7f0f00e5

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 3960
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3969
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public hideAppEditList()V
    .locals 1

    .prologue
    .line 4833
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideAppEditList(Z)V

    .line 4834
    return-void
.end method

.method public hideAppEditList(Z)V
    .locals 3
    .param p1, "bForceCloseAppList"    # Z

    .prologue
    .line 4846
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbAnimating:Z

    if-nez v1, :cond_0

    .line 4847
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 4848
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCloseApplist:Z

    .line 4851
    const/4 v0, 0x0

    .line 4852
    .local v0, "ani":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x67

    if-ne v1, v2, :cond_1

    .line 4853
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002f

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 4860
    :goto_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$72;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 4910
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissResetConfirmDialog()V

    .line 4911
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getMainFrame()Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 4914
    .end local v0    # "ani":Landroid/view/animation/Animation;
    :cond_0
    return-void

    .line 4854
    .restart local v0    # "ani":Landroid/view/animation/Animation;
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_2

    .line 4855
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040030

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    .line 4857
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04002e

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0
.end method

.method public hideAppEditPost()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4838
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCloseApplist:Z

    if-eqz v0, :cond_0

    .line 4839
    invoke-virtual {p0, v1, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateFlashBarState(ZZ)Z

    .line 4844
    :goto_0
    return-void

    .line 4842
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V

    goto :goto_0
.end method

.method public hideTraybarHelpPopup()V
    .locals 1

    .prologue
    .line 1711
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsCheckBoxChecked:Z

    .line 1712
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHelpPopupWindowTraybar()V

    .line 1713
    return-void
.end method

.method public hideUsingHelpPopup()V
    .locals 1

    .prologue
    .line 1716
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsCheckBoxChecked:Z

    .line 1717
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHelpPopupWindowUsing()V

    .line 1718
    return-void
.end method

.method public isPocketTrayOpen()Z
    .locals 1

    .prologue
    .line 4173
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOpenPocketTray:Z

    return v0
.end method

.method public makeRecentApp()V
    .locals 0

    .prologue
    .line 5126
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 6
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const v5, 0x7f080002

    const v3, 0x7f080001

    const/4 v4, 0x1

    .line 3271
    iget-object v1, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/content/res/Configuration;->fontScale:F

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_5

    .line 3273
    :cond_0
    const-string v1, "AppListBezelWindow"

    const-string v2, "onConfigurationChanged: update text"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3275
    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 3276
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNeedUpdatePocketTrayButtonText:Z

    .line 3278
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v1, :cond_a

    .line 3279
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_1

    .line 3280
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3282
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    if-eqz v1, :cond_2

    .line 3283
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3285
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 3286
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3288
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_4

    .line 3289
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3306
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    .line 3307
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketTemplateText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080003

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3308
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketHelpText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v3, 0x7f080004

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3311
    :cond_5
    new-instance v1, Landroid/content/res/Configuration;

    invoke-direct {v1, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 3312
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListController;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3314
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 3315
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 3316
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    .line 3317
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    .line 3319
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStartFlashBar:Z

    if-eqz v1, :cond_6

    .line 3320
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-direct {p0, v1, v4, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZ)V

    .line 3321
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->refreshBezelUI()V

    .line 3324
    :cond_6
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    if-eqz v1, :cond_7

    .line 3325
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 3327
    :cond_7
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070001

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    rsub-int/lit8 v1, v1, 0x64

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mChangedPosition:I

    .line 3328
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getFlashBarEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsInputMethodShown:Z

    if-eqz v1, :cond_8

    .line 3329
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mConfigChanged:Z

    .line 3330
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x67

    if-eq v1, v2, :cond_8

    iget v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    const/16 v2, 0x68

    if-ne v1, v2, :cond_8

    .line 3336
    :cond_8
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v1, :cond_9

    .line 3337
    invoke-direct {p0, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V

    .line 3340
    :cond_9
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getWindowDefSize()V

    .line 3341
    return-void

    .line 3293
    .end local v0    # "fullscreen":Landroid/graphics/Point;
    :cond_a
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_b

    .line 3294
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3296
    :cond_b
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    if-eqz v1, :cond_c

    .line 3297
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditBtn_h:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3299
    :cond_c
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    if-eqz v1, :cond_d

    .line 3300
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketEditText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3302
    :cond_d
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    if-eqz v1, :cond_4

    .line 3303
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mOnlyEditBtn:Landroid/widget/Button;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/16 v3, 0xcf

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 464
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/DrawerLayoutMW;->setDrawerListener(Landroid/support/v4/widget/DrawerLayoutMW$DrawerListener;)V

    .line 469
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->releaseAppListBitmap(Ljava/util/List;)V

    .line 470
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->releaseAppListBitmap(Ljava/util/List;)V

    .line 471
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    .line 472
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    .line 473
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 474
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 476
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->unregisterListeners()V

    .line 478
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 479
    return-void
.end method

.method public openBezelUI()V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 5203
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z

    if-eqz v3, :cond_0

    .line 5204
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-direct {p0, v3, v4, v4, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V

    .line 5205
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 5208
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 5209
    .local v2, "l":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0023

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v0, v3

    .line 5210
    .local v0, "applistHeight":I
    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 5211
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 5212
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5214
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateRunningAppList()V

    .line 5216
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v1

    .line 5217
    .local v1, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getSealedState()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5226
    :goto_0
    return-void

    .line 5221
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelDrawerLayout:Landroid/support/v4/widget/DrawerLayoutMW;

    new-instance v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$75;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$75;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    const-wide/16 v6, 0x64

    invoke-virtual {v3, v4, v6, v7}, Landroid/support/v4/widget/DrawerLayoutMW;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public pkgManagerList(Landroid/content/Intent;)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 4538
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v2, p1, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->pkgManagerList(Landroid/content/Intent;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4539
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 4540
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v2, :cond_0

    .line 4541
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 4544
    :cond_0
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4545
    const-string v2, "android.intent.extra.REPLACING"

    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 4546
    .local v0, "bReplace":Z
    if-nez v0, :cond_1

    .line 4547
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v1

    .line 4548
    .local v1, "packageName":Ljava/lang/String;
    const-string v2, "com.samsung.helphub"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 4550
    const-string v2, "AppListBezelWindow"

    const-string v3, "act= android.intent.action.PACKAGE_REMOVE "

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 4551
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->initPocketTrayLayout()V

    .line 4555
    .end local v0    # "bReplace":Z
    .end local v1    # "packageName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public prepareClosing()V
    .locals 0

    .prologue
    .line 5191
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissUnableDialog()V

    .line 5192
    return-void
.end method

.method public refreshBezelUI()V
    .locals 5

    .prologue
    const v4, 0x7f0a0023

    const/16 v3, 0x68

    .line 5229
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 5230
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v0, v2

    .line 5231
    .local v0, "applistHeight":I
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    if-eqz v2, :cond_1

    .line 5232
    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 5233
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v2, v3, :cond_0

    .line 5234
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 5242
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 5243
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 5245
    return-void

    .line 5237
    :cond_1
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 5238
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    if-ne v2, v3, :cond_0

    .line 5239
    iget v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_0
.end method

.method public reviveMultiWindowTray()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5297
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z

    .line 5298
    iget v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-direct {p0, v0, v1, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZ)V

    .line 5299
    return-void
.end method

.method public rotateTraybarHelpPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1721
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHelpPopupWindowTraybar()V

    .line 1722
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V

    .line 1723
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1724
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1726
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1727
    return-void
.end method

.method public rotateUsingHelpPopup()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 1730
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHelpPopupWindowUsing()V

    .line 1731
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1732
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1734
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1735
    return-void
.end method

.method public setAppEditListWindow(Landroid/view/Window;)V
    .locals 3
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 4726
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mPocketLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 4727
    .local v0, "more":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1, p1, v2, v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Z)V

    .line 4728
    return-void

    .line 4726
    .end local v0    # "more":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFlashBarState(Z)V
    .locals 1
    .param p1, "enableState"    # Z

    .prologue
    .line 791
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    .line 792
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setFlashBarState(Z)V

    .line 794
    :cond_0
    return-void
.end method

.method public setGuidelineWindow(Landroid/view/Window;)V
    .locals 0
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 645
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowGuideline:Landroid/view/Window;

    .line 646
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeGuideLineLayout()V

    .line 647
    return-void
.end method

.method public setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 6
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 587
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    .line 588
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 590
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 592
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 593
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 594
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I

    .line 595
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I

    .line 597
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    .line 599
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeFlashBarLayout(Z)V

    .line 600
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListHorizontal:Ljava/util/List;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 601
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListVertical:Ljava/util/List;

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZI)V

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getGlobalSystemUiVisibility()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSystemUiVisibility:I

    .line 606
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$5;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 611
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 625
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 641
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateRunningAppList()V

    .line 642
    return-void
.end method

.method public showFlashBar(ZZ)V
    .locals 4
    .param p1, "show"    # Z
    .param p2, "expand"    # Z

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 811
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V

    .line 812
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelUI:Z

    if-eqz v0, :cond_1

    .line 813
    if-eqz p1, :cond_0

    .line 814
    invoke-direct {p0, v1, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startFlashBarAnimation(ZZ)V

    .line 835
    :cond_0
    :goto_0
    return-void

    .line 817
    :cond_1
    if-eqz p1, :cond_2

    .line 819
    invoke-direct {p0, v1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startFlashBarAnimation(ZZ)V

    .line 820
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080019

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 822
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 827
    :cond_2
    invoke-direct {p0, v2, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startFlashBarAnimation(ZZ)V

    .line 828
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f08001a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 830
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method

.method public showHistoryBarDialog(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 5249
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    invoke-virtual {v3, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 5250
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-ne v3, v7, :cond_2

    .line 5251
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsHoverType:Z

    if-eqz v3, :cond_0

    .line 5285
    :goto_0
    return-void

    .line 5255
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    .line 5256
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_1

    .line 5259
    :cond_1
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    if-ne v3, p1, :cond_2

    .line 5260
    iput v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    .line 5261
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I

    goto :goto_0

    .line 5266
    :cond_2
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 5267
    .local v1, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 5268
    const/4 v3, 0x2

    new-array v0, v3, [I

    .line 5269
    .local v0, "loc":[I
    invoke-virtual {v2, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 5270
    iget v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I

    packed-switch v3, :pswitch_data_0

    .line 5282
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v5}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 5283
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsHoverType:Z

    .line 5284
    invoke-direct {p0, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V

    goto :goto_0

    .line 5274
    :pswitch_0
    aget v3, v0, v7

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    goto :goto_1

    .line 5278
    :pswitch_1
    aget v3, v0, v6

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I

    goto :goto_1

    .line 5270
    nop

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public showViewPagerTray(Ljava/lang/String;I)V
    .locals 0
    .param p1, "packageName"    # Ljava/lang/String;
    .param p2, "taskId"    # I

    .prologue
    .line 5301
    return-void
.end method

.method public updateAppListRelayout(Z)V
    .locals 2
    .param p1, "bClear"    # Z

    .prologue
    .line 4577
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;->notifyDataSetChanged()V

    .line 4578
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListAdapter:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$AppListAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 4580
    return-void
.end method

.method public updateFlashBarState(ZZ)Z
    .locals 1
    .param p1, "expand"    # Z
    .param p2, "pressedHelpBtn"    # Z

    .prologue
    .line 656
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 657
    const/4 v0, 0x0

    .line 665
    :goto_0
    return v0

    .line 659
    :cond_0
    if-eqz p1, :cond_1

    .line 660
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->openFlashBar()V

    .line 665
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 662
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeFlashBar()V

    goto :goto_1
.end method

.method public userSwitched()V
    .locals 2

    .prologue
    .line 5167
    const-string v0, "AppListBezelWindow"

    const-string v1, "userSwitched"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5168
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;

    if-nez v0, :cond_0

    .line 5181
    :goto_0
    return-void

    .line 5170
    :cond_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mUserId:I

    .line 5174
    const-wide/16 v0, 0x3e8

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 5178
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 5179
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 5180
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->initPocketTrayLayout()V

    goto :goto_0

    .line 5175
    :catch_0
    move-exception v0

    goto :goto_1
.end method
