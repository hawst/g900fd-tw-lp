.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 5128
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 5131
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/app/Dialog;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-ne v2, v5, :cond_1

    .line 5132
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/app/Dialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    .line 5133
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 5136
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 5137
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I
    invoke-static {v2, v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$14102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 5138
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v3, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentHistoryIndex:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$10902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 5162
    :goto_0
    return-void

    .line 5143
    :cond_1
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 5144
    .local v1, "outMetrics":Landroid/util/DisplayMetrics;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 5145
    new-array v0, v6, [I

    .line 5146
    .local v0, "loc":[I
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 5147
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 5159
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIvt:[B
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)[B

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v4}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 5160
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V
    invoke-static {v2, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)V

    .line 5161
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_0

    .line 5152
    :pswitch_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    aget v3, v0, v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$14200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$14102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    goto :goto_1

    .line 5156
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    aget v3, v0, v4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$74;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbNailLayoutWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$14200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentAppPosition:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$14102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    goto :goto_1

    .line 5147
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
