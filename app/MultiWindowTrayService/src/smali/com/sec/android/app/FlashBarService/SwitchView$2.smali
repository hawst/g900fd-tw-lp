.class Lcom/sec/android/app/FlashBarService/SwitchView$2;
.super Ljava/lang/Object;
.source "SwitchView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SwitchView;->animateZoom(FJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private canceled:Z

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

.field final synthetic val$set:Landroid/animation/AnimatorSet;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/animation/AnimatorSet;)V
    .locals 1

    .prologue
    .line 425
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->val$set:Landroid/animation/AnimatorSet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 427
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->canceled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 475
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$600(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MIK"

    const-string v1, "animateZoom: onAnimationCancel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->canceled:Z

    .line 477
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$600(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MIK"

    const-string v1, "animateZoom: onAnimationEnd"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1002(Lcom/sec/android/app/FlashBarService/SwitchView;Lcom/sec/android/app/FlashBarService/SwitchView$State;)Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 449
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->canceled:Z

    if-eqz v0, :cond_2

    .line 471
    :cond_1
    :goto_0
    return-void

    .line 450
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$200(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$202(Lcom/sec/android/app/FlashBarService/SwitchView;Z)Z

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # invokes: Lcom/sec/android/app/FlashBarService/SwitchView;->triggerEndCallback()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$400(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    .line 453
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mNeedsHideAnimation:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1100(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # invokes: Lcom/sec/android/app/FlashBarService/SwitchView;->animateTransparentHiding()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1200(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    .line 459
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 460
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    new-instance v1, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SwitchView$2$1;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView$2;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 456
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # invokes: Lcom/sec/android/app/FlashBarService/SwitchView;->triggerRealEndCallback()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$500(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    goto :goto_1
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 481
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 430
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$600(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "animateZoom: onAnimationStart: mCurrentZoomAnimation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$700(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$700(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/Animator;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$700(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/Animator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$700(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/Animator;->isStarted()Z

    move-result v1

    if-nez v1, :cond_3

    .line 433
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$600(Lcom/sec/android/app/FlashBarService/SwitchView;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "MIK"

    const-string v2, "onAnimationStart: calling cancel"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$700(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/Animator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/Animator;->cancel()V

    .line 436
    :cond_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    .line 437
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderIn(I)V

    .line 438
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$900(Lcom/sec/android/app/FlashBarService/SwitchView;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 439
    .local v0, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->bringChildToFront(Landroid/view/View;)V

    .line 440
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->requestLayout()V

    .line 442
    .end local v0    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$2;->val$set:Landroid/animation/AnimatorSet;

    # setter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$702(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/animation/Animator;)Landroid/animation/Animator;

    .line 443
    return-void
.end method
