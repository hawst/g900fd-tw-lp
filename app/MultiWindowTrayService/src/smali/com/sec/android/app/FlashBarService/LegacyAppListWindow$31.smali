.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 2113
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 2115
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 2116
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 2257
    const-string v7, "DragDrop"

    const-string v8, "Unknown action type received by OnDragListener."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2260
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    const/4 v7, 0x1

    return v7

    .line 2118
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v8, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;)I

    move-result v8

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-nez v7, :cond_0

    .line 2120
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v5, v7

    check-cast v5, Landroid/widget/ImageView;

    .line 2122
    .local v5, "orgView":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2123
    const v7, 0x7f02007f

    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2124
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v8, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v9}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 2126
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 2127
    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v7, :cond_1

    sget-boolean v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v7, :cond_2

    .line 2129
    :cond_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 2130
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    const/4 v7, 0x0

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2131
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v7

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2132
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2133
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2134
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2135
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v8

    invoke-interface {v7, v8, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2136
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 2138
    .end local v3    # "l":Landroid/view/WindowManager$LayoutParams;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->cancelCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_0

    .line 2143
    .end local v5    # "orgView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 2146
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 2147
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 2148
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v7

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2149
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2150
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    const/4 v10, 0x6

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z

    .line 2151
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddEmptyItemPosition:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    goto/16 :goto_0

    .line 2156
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2157
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v7, p1, p2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2161
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v8, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;)I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2164
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 2165
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_5

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2167
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2168
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    if-ge v7, v8, :cond_4

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    if-eq v7, v8, :cond_4

    .line 2169
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z

    .line 2182
    :cond_3
    :goto_1
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v1, v7

    check-cast v1, Landroid/widget/ImageView;

    .line 2183
    .local v1, "blankView":Landroid/widget/ImageView;
    const v7, 0x7f02007f

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 2172
    .end local v1    # "blankView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v8

    if-ne v7, v8, :cond_3

    .line 2173
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z

    goto :goto_1

    .line 2177
    :cond_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changelistItem(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;II)Z

    .line 2178
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    goto :goto_1

    .line 2189
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->checkAppListLayout()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2190
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 2191
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2192
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setStartDragFromEdit(Z)V

    .line 2193
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2194
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2197
    :cond_6
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_7

    .line 2198
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v9

    const/4 v10, 0x0

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeApplistItem(IIZ)V

    .line 2199
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2200
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateAppListRelayout(Z)V

    .line 2203
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->findeCurIndex(Landroid/view/View;)I
    invoke-static {v7, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/view/View;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    if-ne v7, v8, :cond_9

    .line 2204
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v4, v7

    check-cast v4, Landroid/widget/ImageView;

    .line 2205
    .local v4, "org":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v7, :cond_8

    .line 2206
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2207
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2209
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2210
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2213
    .end local v4    # "org":Landroid/widget/ImageView;
    :cond_9
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2214
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 2219
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_b

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_a

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 2220
    :cond_a
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7602(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 2221
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v1, v7

    check-cast v1, Landroid/widget/ImageView;

    .line 2222
    .restart local v1    # "blankView":Landroid/widget/ImageView;
    const v7, 0x7f02007f

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2224
    .end local v1    # "blankView":Landroid/widget/ImageView;
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2227
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->changlistItemFromEditList(III)Z
    invoke-static {v7, v8, v9, v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;III)Z

    .line 2228
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5202(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2229
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5102(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    goto/16 :goto_0

    .line 2233
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_5
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListDragMode:Z
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 2235
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v7

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getStartDragFromEdit()Z

    move-result v7

    if-eqz v7, :cond_c

    .line 2236
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->getItemIndexFromEditList()I

    move-result v9

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->addAndChangelistItem(II)Z
    invoke-static {v7, v8, v9}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;II)Z

    .line 2237
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    sget-object v8, Lcom/sec/android/app/FlashBarService/AppListController;->mDefaultIvt:[B

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v9, v9, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v9}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 2238
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->setItemIndexFromAppList(I)V

    .line 2239
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAddToListItem:I
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 2240
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 2242
    :cond_c
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_d

    .line 2243
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mResources:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f02007f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2244
    .local v2, "curIcon":Landroid/graphics/drawable/Drawable;
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v7, 0x7f0f0059

    invoke-virtual {p1, v7}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    move-object v6, v7

    check-cast v6, Landroid/widget/ImageView;

    .line 2246
    .local v6, "tmpiv":Landroid/widget/ImageView;
    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-ne v7, v2, :cond_d

    .line 2247
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2248
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v8, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v8}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 2251
    .end local v2    # "curIcon":Landroid/graphics/drawable/Drawable;
    .end local v6    # "tmpiv":Landroid/widget/ImageView;
    :cond_d
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 2252
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 2253
    :cond_e
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_0

    .line 2116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
