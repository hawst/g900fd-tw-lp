.class final Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;
.super Ljava/lang/Object;
.source "TryMultiWindowBezel.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AppIconLongClick"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;

    .prologue
    .line 512
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 515
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 516
    .local v2, "tag":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppTag1:Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 517
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1002(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 518
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isFirstTipShown:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 519
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$402(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 526
    :goto_0
    const-string v3, ""

    const-string v4, ""

    invoke-static {v3, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v0

    .line 527
    .local v0, "data":Landroid/content/ClipData;
    new-instance v1, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v1, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 528
    .local v1, "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    invoke-virtual {p1, v0, v1, p1, v5}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 529
    invoke-virtual {p1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 531
    return v6

    .line 521
    .end local v0    # "data":Landroid/content/ClipData;
    .end local v1    # "shadowBuilder":Landroid/view/View$DragShadowBuilder;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app1DragArea:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 522
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1002(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    .line 524
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isFirstTipShown:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z

    goto :goto_0
.end method
