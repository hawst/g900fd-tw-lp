.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4401
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, -0x1

    const/4 v6, 0x0

    .line 4403
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getCurrentAppList()Ljava/util/List;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v3, v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4302(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 4405
    const-string v3, "LegacyAppListWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "App icon clicked :: appIconIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mbAnimating = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", mbEditmode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4408
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    if-le v3, v7, :cond_0

    .line 4409
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mTopLayoutVertical:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mCurrentHistoryIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 4410
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->dismissHistoryWindow(Z)V
    invoke-static {v3, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$3300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)V

    .line 4414
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbEditmode:Z
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$500(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mbAnimating:Z

    if-eqz v3, :cond_3

    .line 4415
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->isTemplate(IZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 4416
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createConfirmDialog(IZ)V

    .line 4442
    :cond_2
    :goto_0
    return-void

    .line 4421
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v3

    if-le v3, v7, :cond_2

    .line 4422
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-nez v3, :cond_5

    .line 4423
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v1

    .line 4425
    .local v1, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v1, :cond_4

    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_5

    .line 4427
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect()Landroid/graphics/Rect;

    move-result-object v2

    .line 4428
    .local v2, "rect":Landroid/graphics/Rect;
    if-eqz v2, :cond_5

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 4429
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 4430
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v4, v4, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    invoke-virtual {v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 4434
    .end local v1    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v2    # "rect":Landroid/graphics/Rect;
    :cond_5
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getComponentInfo(I)Landroid/content/pm/ComponentInfo;

    move-result-object v0

    .line 4435
    .local v0, "info":Landroid/content/pm/ComponentInfo;
    const-string v3, "LegacyAppListWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "App icon clicked :: name = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4436
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 4437
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivity(I)V

    goto/16 :goto_0

    .line 4439
    :cond_6
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$73;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(I)V

    goto/16 :goto_0
.end method
