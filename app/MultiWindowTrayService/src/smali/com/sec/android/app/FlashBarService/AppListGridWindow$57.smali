.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3939
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 19
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 3941
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 3942
    .local v4, "l":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v1

    .line 3943
    .local v1, "action":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v13

    float-to-int v11, v13

    .line 3944
    .local v11, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v13

    float-to-int v12, v13

    .line 3945
    .local v12, "y":I
    const/4 v5, 0x0

    .line 3946
    .local v5, "left":I
    const/4 v10, 0x0

    .line 3947
    .local v10, "top":I
    const/4 v9, 0x0

    .line 3948
    .local v9, "right":I
    const/4 v3, 0x0

    .line 3951
    .local v3, "bottom":I
    const/4 v13, 0x6

    if-eq v1, v13, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v13

    if-nez v13, :cond_0

    .line 3952
    const/4 v13, 0x0

    .line 4098
    :goto_0
    return v13

    .line 3955
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mLunchBlock:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 3956
    const/4 v13, 0x1

    goto :goto_0

    .line 3959
    :cond_1
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 3961
    .local v8, "rect":Landroid/graphics/Rect;
    packed-switch v1, :pswitch_data_0

    .line 4098
    :cond_2
    :goto_1
    const/4 v13, 0x1

    goto :goto_0

    .line 3963
    :pswitch_0
    sget-boolean v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v13, :cond_7

    .line 3964
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPenWindowOnly:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 3965
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    div-int/lit8 v13, v13, 0x2

    sub-int v5, v11, v13

    .line 3966
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    div-int/lit8 v13, v13, 0x2

    sub-int v10, v12, v13

    .line 3967
    if-gez v10, :cond_3

    const/4 v10, 0x0

    .line 3968
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    add-int v9, v5, v13

    .line 3969
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    add-int v3, v10, v13

    .line 3970
    new-instance v8, Landroid/graphics/Rect;

    .end local v8    # "rect":Landroid/graphics/Rect;
    invoke-direct {v8, v5, v10, v9, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 3979
    .restart local v8    # "rect":Landroid/graphics/Rect;
    :goto_2
    if-eqz v8, :cond_4

    .line 3980
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 3981
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    iget v14, v8, Landroid/graphics/Rect;->left:I

    iget v15, v8, Landroid/graphics/Rect;->top:I

    iget v0, v8, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v13 .. v17}, Landroid/widget/ImageView;->layout(IIII)V

    .line 3983
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_1

    .line 3972
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v14

    invoke-virtual {v13, v14}, Lcom/sec/android/app/FlashBarService/AppListController;->isTemplateItem(I)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 3973
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v14, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11502(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3974
    new-instance v8, Landroid/graphics/Rect;

    .end local v8    # "rect":Landroid/graphics/Rect;
    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v16

    move/from16 v0, v16

    invoke-direct {v8, v13, v14, v15, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v8    # "rect":Landroid/graphics/Rect;
    goto :goto_2

    .line 3976
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v13, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v8

    goto :goto_2

    .line 3987
    :cond_7
    :pswitch_1
    sget-boolean v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v13, :cond_b

    .line 3989
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPenWindowOnly:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v13

    if-eqz v13, :cond_a

    .line 3990
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    div-int/lit8 v13, v13, 0x2

    sub-int v5, v11, v13

    .line 3991
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    div-int/lit8 v13, v13, 0x2

    sub-int v10, v12, v13

    .line 3992
    if-gez v10, :cond_8

    const/4 v10, 0x0

    .line 3993
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    add-int v9, v5, v13

    .line 3994
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    add-int v3, v10, v13

    .line 3995
    new-instance v8, Landroid/graphics/Rect;

    .end local v8    # "rect":Landroid/graphics/Rect;
    invoke-direct {v8, v5, v10, v9, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4001
    .restart local v8    # "rect":Landroid/graphics/Rect;
    :cond_9
    :goto_3
    if-eqz v8, :cond_2

    .line 4002
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4003
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    iget v14, v8, Landroid/graphics/Rect;->left:I

    iget v15, v8, Landroid/graphics/Rect;->top:I

    iget v0, v8, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    iget v0, v8, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    invoke-virtual/range {v13 .. v17}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_1

    .line 3997
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v13

    if-nez v13, :cond_9

    .line 3998
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v13, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v8

    goto :goto_3

    .line 4008
    :cond_b
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v13, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v8

    .line 4009
    if-eqz v8, :cond_c

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v13, v8}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_c

    .line 4010
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    invoke-virtual {v13, v8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 4011
    sget-boolean v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v13, :cond_c

    .line 4012
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v14, v14, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v14, v14, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 4013
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v6

    .line 4015
    .local v6, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v6, :cond_c

    const/4 v13, 0x2

    invoke-virtual {v6, v13}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v13

    if-eqz v13, :cond_c

    invoke-virtual {v6}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v13

    if-nez v13, :cond_c

    .line 4018
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    const/4 v14, 0x3

    if-ne v13, v14, :cond_d

    .line 4019
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4020
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4028
    .end local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_c
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4029
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v14, v14, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v15

    add-int/2addr v14, v15

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v16

    add-int v15, v15, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v17, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I
    invoke-static/range {v17 .. v17}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v17

    add-int v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v18, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuidelineShadow:I
    invoke-static/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v18

    add-int v17, v17, v18

    invoke-virtual/range {v13 .. v17}, Landroid/widget/ImageView;->layout(IIII)V

    .line 4033
    const/4 v13, 0x5

    if-ne v1, v13, :cond_2

    .line 4034
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v14, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar(Z)V
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)V

    goto/16 :goto_1

    .line 4022
    .restart local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4023
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 4039
    .end local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v14, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11502(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 4040
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 4041
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4042
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4043
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4044
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 4049
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v14, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsTemplateItem:Z
    invoke-static {v13, v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$11502(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 4050
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTopGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4051
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBottomGuideline:Landroid/widget/FrameLayout;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/FrameLayout;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 4052
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 4053
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$5000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v13

    const/4 v14, 0x4

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 4055
    const/4 v13, 0x0

    iput v13, v4, Landroid/view/WindowManager$LayoutParams;->x:I

    const/4 v13, 0x0

    iput v13, v4, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v13, 0x0

    iput v13, v4, Landroid/view/WindowManager$LayoutParams;->width:I

    const/4 v13, 0x0

    iput v13, v4, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 4056
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13, v4}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 4057
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v14

    invoke-virtual {v14}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v14

    invoke-interface {v13, v14, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 4058
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 4061
    const-string v13, "AppListGridWindow"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "App icon long clicked :: appIconIndex = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ",  mcurDstIndex = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4062
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPenWindowOnly:Z
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v13

    if-eqz v13, :cond_10

    .line 4063
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v13

    if-eqz v13, :cond_f

    .line 4064
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    div-int/lit8 v13, v13, 0x2

    sub-int v5, v11, v13

    .line 4065
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    div-int/lit8 v13, v13, 0x2

    sub-int v10, v12, v13

    .line 4066
    if-gez v10, :cond_e

    const/4 v10, 0x0

    .line 4067
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getApplistIndicatorSize()I

    move-result v13

    add-int/2addr v10, v13

    .line 4068
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefWidth:I

    add-int v9, v5, v13

    .line 4069
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowDefHeight:I

    add-int v3, v10, v13

    .line 4070
    new-instance v8, Landroid/graphics/Rect;

    .end local v8    # "rect":Landroid/graphics/Rect;
    invoke-direct {v8, v5, v10, v9, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4072
    .restart local v8    # "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v14

    invoke-virtual {v13, v14}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 4073
    .local v7, "r":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    invoke-virtual {v7, v13}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 4074
    .local v2, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v14, v14, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v15, 0x7f08002c

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aput-object v2, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v13, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v13

    invoke-virtual {v13}, Landroid/widget/Toast;->show()V

    .line 4076
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v14

    invoke-virtual {v13, v14, v8}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(ILandroid/graphics/Rect;)V

    .line 4083
    .end local v2    # "appName":Ljava/lang/String;
    .end local v7    # "r":Landroid/content/pm/ResolveInfo;
    :cond_f
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/Window;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v13

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/view/View;->setVisibility(I)V

    .line 4085
    sget-boolean v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v13, :cond_2

    .line 4086
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v6

    .line 4088
    .restart local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v6, :cond_2

    const/4 v13, 0x2

    invoke-virtual {v6, v13}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 4089
    invoke-virtual {v6}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v13

    if-nez v13, :cond_11

    .line 4090
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v14, 0x1

    const/4 v15, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateSplitGuidePosition(ZZ)V
    invoke-static {v13, v14, v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZZ)V

    goto/16 :goto_1

    .line 4079
    .end local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_10
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mcurDstIndex:I
    invoke-static {v14}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v14

    invoke-virtual {v13, v14, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(III)V

    goto :goto_5

    .line 4092
    .restart local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_11
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$57;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v13, v13, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->requestSplitPreview(Z)V

    goto/16 :goto_1

    .line 3961
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
