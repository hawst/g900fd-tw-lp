.class Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;
.super Ljava/lang/Object;
.source "SmartClipDragDrop.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->initDragDrop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    .line 331
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    .line 332
    move-object v0, p1

    .line 333
    .local v0, "getView":Landroid/view/View;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v1, v3

    .line 334
    .local v1, "x":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v2, v3

    .line 335
    .local v2, "y":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$300(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/HandlerThread;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$300(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/HandlerThread;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getState()Ljava/lang/Thread$State;

    move-result-object v3

    sget-object v4, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v3, v4, :cond_0

    .line 336
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$300(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/HandlerThread;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/HandlerThread;->start()V

    .line 337
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mThread:Landroid/os/HandlerThread;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$300(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/HandlerThread;

    move-result-object v4

    invoke-virtual {v4}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v4

    # setter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceLooper:Landroid/os/Looper;
    invoke-static {v3, v4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$402(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/os/Looper;)Landroid/os/Looper;

    .line 338
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    new-instance v4, Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceLooper:Landroid/os/Looper;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$400(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    # setter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceHandler:Landroid/os/Handler;
    invoke-static {v3, v4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$502(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/os/Handler;)Landroid/os/Handler;

    .line 339
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$500(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5$1;-><init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;Landroid/view/View;II)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 348
    .end local v0    # "getView":Landroid/view/View;
    .end local v1    # "x":I
    .end local v2    # "y":I
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    if-ne v3, v6, :cond_1

    .line 349
    const-string v3, "SmartClipDragDrop"

    const-string v4, "onTouch : ACTION_UP : stopService()"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mServiceHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$500(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 351
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$5;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopService()V

    .line 353
    :cond_1
    return v6
.end method
