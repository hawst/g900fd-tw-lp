.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeTraybarHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 1537
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 1540
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a013e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 1541
    .local v1, "helpPopupWidth":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a013f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v0, v4

    .line 1542
    .local v0, "helpPopupHeight":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, v1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0140

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v2, v4

    .line 1543
    .local v2, "xOffset":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    int-to-float v4, v4

    int-to-float v5, v0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0141

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-int v3, v4

    .line 1544
    .local v3, "yOffset":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$26;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v6

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 1545
    return-void
.end method
