.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;)V
    .locals 0

    .prologue
    .line 3261
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3262
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3263
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 3265
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->hideDragAndDropHelpDialog()V

    .line 3266
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3267
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createAppListOverlayHelp()V

    .line 3271
    :goto_0
    return-void

    .line 3269
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$34;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->showTraybarHelpPopup()V

    goto :goto_0
.end method
