.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3687
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v8, 0xca

    .line 3689
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 3712
    :cond_0
    :goto_0
    const/4 v5, 0x0

    return v5

    .line 3692
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 3693
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v5, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$802(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/View;)Landroid/view/View;

    .line 3694
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v6

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10202(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 3695
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v6, 0xc8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 3698
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 3699
    .local v3, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 3700
    .local v4, "y":F
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float v5, v3, v5

    float-to-int v0, v5

    .line 3701
    .local v0, "deltaX":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float v5, v4, v5

    float-to-int v1, v5

    .line 3702
    .local v1, "deltaY":I
    mul-int v5, v0, v0

    mul-int v6, v1, v1

    add-int v2, v5, v6

    .line 3703
    .local v2, "distance":I
    const/16 v5, 0x3e8

    if-le v2, v5, :cond_0

    .line 3704
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 3709
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    .end local v2    # "distance":I
    .end local v3    # "x":F
    .end local v4    # "y":F
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$49;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 3689
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
