.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;
.super Landroid/os/Handler;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 694
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 696
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 739
    :cond_0
    :goto_0
    return-void

    .line 698
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateFlashBarState(ZZ)Z

    goto :goto_0

    .line 702
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    goto :goto_0

    .line 706
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationRecentIconAppearByHomeKey()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_0

    .line 710
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeRecentApp()V

    .line 711
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationRecentIcon()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_0

    .line 715
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mThumbnailType:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)V

    goto :goto_0

    .line 719
    :sswitch_5
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAvailableTemplateText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 725
    :sswitch_6
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 726
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->closeBezelUI()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 728
    :catch_0
    move-exception v0

    .line 729
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AppListBezelWindow"

    const-string v2, "Exception in mInputMethodChangedReceiver."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 733
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListRelayout(Z)V

    .line 734
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateRunningAppList()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_0

    .line 696
    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0xca -> :sswitch_1
        0xcb -> :sswitch_2
        0xcc -> :sswitch_3
        0xcd -> :sswitch_5
        0xce -> :sswitch_6
        0xcf -> :sswitch_7
        0x12c -> :sswitch_4
    .end sparse-switch
.end method
