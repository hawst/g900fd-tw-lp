.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setPopupViewWindow(Landroid/view/Window;Landroid/graphics/Rect;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 281
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 282
    return-void
.end method

.method public onPageSelected(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const v6, 0x3f99999a    # 1.2f

    .line 284
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 285
    .local v2, "prevIcon":Landroid/widget/ImageView;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 286
    const/16 v3, 0x66

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 288
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 289
    .local v0, "curIcon":Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 290
    .local v1, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v1, v6, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 291
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    div-float/2addr v3, v7

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    div-float/2addr v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 293
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 294
    const/16 v3, 0xff

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 295
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I
    invoke-static {v3, p1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$402(Lcom/sec/android/app/FlashBarService/PopupViewWindow;I)I

    .line 296
    return-void
.end method
