.class Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;
.super Ljava/lang/Object;
.source "TryMultiWindowTray.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CloseTrayOnClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V
    .locals 0

    .prologue
    .line 574
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
    .param p2, "x1"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;

    .prologue
    .line 574
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    .line 578
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayClosed:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$202(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    .line 579
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 580
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRight:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 582
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup3:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRightMove:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 586
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # invokes: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->setHandleToCenter()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)V

    .line 587
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightMove:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v0

    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayLongClick;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$CloseTrayOnClickListener;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 588
    return-void
.end method
