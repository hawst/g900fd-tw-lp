.class public abstract Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
.super Ljava/lang/Object;
.source "AbstractAppListWindow.java"


# instance fields
.field mActivityManager:Landroid/app/ActivityManager;

.field mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

.field mContext:Landroid/content/Context;

.field mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

.field protected mSupportMultiInstance:Z

.field public mSystemUiVisibility:I

.field mVibrator:Landroid/os/SystemVibrator;

.field mWindowManager:Landroid/view/WindowManager;

.field public mbAnimating:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p3, "trayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mSupportMultiInstance:Z

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    .line 75
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mbAnimating:Z

    .line 78
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 79
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    .line 80
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 81
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mActivityManager:Landroid/app/ActivityManager;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    const-string v1, "multiwindow_facade"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 85
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListController;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportMultiInstance(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mSupportMultiInstance:Z

    .line 87
    const-string v0, "statusbar"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    .line 88
    return-void
.end method

.method public static create(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;ILcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p2, "trayType"    # I
    .param p3, "mMultiwindowTrayInfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    .line 37
    packed-switch p2, :pswitch_data_0

    .line 45
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    :goto_0
    return-object v0

    .line 39
    :pswitch_0
    new-instance v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    goto :goto_0

    .line 41
    :pswitch_1
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    goto :goto_0

    .line 43
    :pswitch_2
    new-instance v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {v0, p0, p1, p3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public abstract animationRecentIconByHomeKey()V
.end method

.method public abstract canCloseWindow()Z
.end method

.method public abstract cancelAllMessages()V
.end method

.method public abstract closePocketTray()V
.end method

.method public abstract createConfirmDialog(IZ)V
.end method

.method public abstract createGestureHelp()V
.end method

.method public abstract createResetConfirmDialog()V
.end method

.method public abstract dismissConfirmDialog()V
.end method

.method public abstract dismissGestureOverlayHelp()V
.end method

.method public abstract dismissTemplateDialog()V
.end method

.method public abstract dismissUnableDialog()V
.end method

.method public abstract dismissViewPagerAppList()V
.end method

.method public getAppListController()Lcom/sec/android/app/FlashBarService/AppListController;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    return-object v0
.end method

.method public getApplistIndicatorSize()I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 126
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mSystemUiVisibility:I

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method public abstract getFlashBarEnabled()Z
.end method

.method public abstract getGuidelineWindow()Landroid/view/Window;
.end method

.method public abstract hideAppEditList()V
.end method

.method public abstract hideAppEditList(Z)V
.end method

.method public abstract hideTraybarHelpPopup()V
.end method

.method public abstract isPocketTrayOpen()Z
.end method

.method public abstract makeRecentApp()V
.end method

.method public abstract onConfigurationChanged(Landroid/content/res/Configuration;)V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract pkgManagerList(Landroid/content/Intent;)V
.end method

.method public abstract prepareClosing()V
.end method

.method public abstract reviveMultiWindowTray()V
.end method

.method public abstract setAppEditListWindow(Landroid/view/Window;)V
.end method

.method public abstract setFlashBarState(Z)V
.end method

.method public abstract setGuidelineWindow(Landroid/view/Window;)V
.end method

.method public abstract setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
.end method

.method public abstract showFlashBar(ZZ)V
.end method

.method public abstract showHistoryBarDialog(I)V
.end method

.method public abstract showViewPagerTray(Ljava/lang/String;I)V
.end method

.method public abstract updateAppListRelayout(Z)V
.end method

.method public abstract updateFlashBarState(ZZ)Z
.end method

.method public abstract userSwitched()V
.end method
