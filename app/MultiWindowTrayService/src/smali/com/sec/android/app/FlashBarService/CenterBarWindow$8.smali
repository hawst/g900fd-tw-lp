.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->initCenterBarButtonAnim()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1139
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v2, 0xc9

    .line 1144
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v1, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showButtonPopupWindow(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)V

    .line 1145
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_0

    .line 1146
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1147
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1151
    :goto_0
    return-void

    .line 1149
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startHideButtonTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    goto :goto_0
.end method
