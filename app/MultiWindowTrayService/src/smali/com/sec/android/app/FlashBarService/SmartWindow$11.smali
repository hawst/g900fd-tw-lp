.class Lcom/sec/android/app/FlashBarService/SmartWindow$11;
.super Ljava/lang/Object;
.source "SmartWindow.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V
    .locals 0

    .prologue
    .line 1056
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1059
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1091
    :cond_0
    :goto_0
    return v6

    .line 1062
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getPositionForView(Landroid/view/View;)I

    move-result v5

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I
    invoke-static {v4, v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 1064
    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "text/uri-list"

    aput-object v4, v3, v7

    .line 1065
    .local v3, "strs":[Ljava/lang/String;
    new-instance v2, Landroid/content/ClipData$Item;

    const-string v4, ""

    invoke-direct {v2, v4}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 1066
    .local v2, "item":Landroid/content/ClipData$Item;
    new-instance v0, Landroid/content/ClipData;

    const-string v4, "appIcon"

    invoke-direct {v0, v4, v3, v2}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 1068
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$11;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppIconIndex:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v4

    const/4 v5, -0x1

    if-le v4, v5, :cond_0

    .line 1069
    if-eqz v0, :cond_0

    .line 1071
    new-instance v1, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow$11$1;-><init>(Lcom/sec/android/app/FlashBarService/SmartWindow$11;Landroid/view/View;)V

    .line 1088
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    const/4 v4, 0x0

    invoke-virtual {p1, v0, v1, v4, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    goto :goto_0
.end method
