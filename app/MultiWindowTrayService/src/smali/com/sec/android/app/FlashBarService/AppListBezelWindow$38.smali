.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 3353
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    const/4 v3, 0x1

    const/4 v1, -0x1

    const/16 v6, 0x64

    const/4 v5, 0x0

    .line 3356
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-le v0, v6, :cond_1

    .line 3357
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080027

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 3358
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/EditText;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3359
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/EditText;

    move-result-object v0

    invoke-interface {p1, v5, v6}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 3360
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 3371
    :cond_0
    :goto_0
    return-void

    .line 3362
    :cond_1
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 3363
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 3364
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 3367
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 3368
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$38;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTemplateDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 3375
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 3379
    return-void
.end method
