.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 280
    const-string v2, "closeTray"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 281
    .local v0, "closeTray":I
    const-string v2, "updateCenterBar"

    invoke-virtual {p2, v2, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 283
    .local v1, "updateCenterBar":I
    const-string v2, "MultiWindowTrayService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSealedReceiver : closeTray="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " updateCenterBar="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    if-ne v0, v5, :cond_0

    .line 285
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 287
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v2

    invoke-virtual {v2, v6, v6}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 290
    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 291
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 293
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v2

    invoke-virtual {v2, v5, v5}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 296
    :cond_1
    if-ne v1, v5, :cond_2

    .line 297
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 298
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkSealedFixedWindow(Z)V

    .line 299
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$7;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getCenterBarPoint()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->updateCenterBarPoint(Landroid/graphics/Point;)V

    .line 302
    :cond_2
    return-void
.end method
