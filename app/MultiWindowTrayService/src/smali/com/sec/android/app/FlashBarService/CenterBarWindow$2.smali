.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;
.super Landroid/content/BroadcastReceiver;
.source "CenterBarWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 377
    const-string v2, "com.sec.android.action.ARRANGE_CONTROLL_BAR"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 378
    const-string v2, "com.sec.android.extra.CONTROL_BAR_POS"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 379
    .local v0, "extra":Ljava/lang/Object;
    if-eqz v0, :cond_0

    instance-of v2, v0, Landroid/graphics/Point;

    if-eqz v2, :cond_0

    move-object v1, v0

    .line 380
    check-cast v1, Landroid/graphics/Point;

    .line 381
    .local v1, "refreshCenterbarPos":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget v3, v1, Landroid/graphics/Point;->x:I

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$002(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 382
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget v3, v1, Landroid/graphics/Point;->y:I

    # setter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I

    .line 383
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setCenterBarPoint()V
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 388
    .end local v0    # "extra":Ljava/lang/Object;
    .end local v1    # "refreshCenterbarPos":Landroid/graphics/Point;
    :cond_0
    return-void
.end method
