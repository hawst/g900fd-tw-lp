.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$8;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$8;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 308
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$8;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowAppList:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$800(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 309
    const-string v1, "isShow"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 310
    .local v0, "state":Z
    const-string v1, "MultiWindowTrayService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "airButton isShow="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    if-eqz v0, :cond_0

    .line 312
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$8;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->hideAppEditList()V

    .line 315
    .end local v0    # "state":Z
    :cond_0
    return-void
.end method
