.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;
.super Ljava/lang/Object;
.source "TryMultiWindowBezelTemplate.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->startPocketOpenAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder3:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup3:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateCue3:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 391
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 385
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 382
    return-void
.end method
