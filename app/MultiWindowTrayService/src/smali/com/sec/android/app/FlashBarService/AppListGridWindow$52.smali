.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3764
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 3769
    const/4 v0, 0x0

    .line 3770
    .local v0, "anim":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3771
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f04004a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3772
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3777
    :goto_0
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3791
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPocketLayout:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3792
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3793
    return-void

    .line 3774
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f040032

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 3775
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$52;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mOpenPocketTray:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$10402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    goto :goto_0
.end method
