.class public Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
.super Landroid/widget/FrameLayout;
.source "RecentsPanelView.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;,
        Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;,
        Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    }
.end annotation


# instance fields
.field private mAnimateIconOfFirstTask:Z

.field private mFitThumbnailToXY:Z

.field private mHighEndGfx:Z

.field private mItemHeight:I

.field private mItemWidth:I

.field private mListAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

.field private mPopup:Landroid/widget/PopupMenu;

.field private mRecentItemLayoutId:I

.field private mRecentTaskDescriptions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;"
        }
    .end annotation
.end field

.field private mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

.field private mRecentsContainer:Landroid/view/View;

.field private mRecentsScrim:Landroid/view/View;

.field private mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

.field private mShowing:Z

.field private mWaitingForWindowAnimation:Z

.field private mWaitingToShow:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 195
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v2, 0x0

    .line 199
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .line 200
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateValuesFromResources()V

    .line 202
    sget-object v1, Lcom/sec/android/app/FlashBarService/R$styleable;->RecentsPanelView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 205
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentItemLayoutId:I

    .line 206
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    .line 207
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 208
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentItemLayoutId:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mItemWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .prologue
    .line 67
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mItemHeight:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/drawable/Drawable;ZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    .param p2, "x2"    # Landroid/graphics/drawable/Drawable;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateIcon(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/drawable/Drawable;ZZ)V

    return-void
.end method

.method private createCustomAnimations(Landroid/animation/LayoutTransition;)V
    .locals 4
    .param p1, "transitioner"    # Landroid/animation/LayoutTransition;

    .prologue
    .line 412
    const-wide/16 v0, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 413
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 414
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/animation/LayoutTransition;->setAnimator(ILandroid/animation/Animator;)V

    .line 415
    return-void
.end method

.method private pointInside(IILandroid/view/View;)Z
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 225
    invoke-virtual {p3}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 226
    .local v1, "l":I
    invoke-virtual {p3}, Landroid/view/View;->getRight()I

    move-result v2

    .line 227
    .local v2, "r":I
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v3

    .line 228
    .local v3, "t":I
    invoke-virtual {p3}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 229
    .local v0, "b":I
    if-lt p1, v1, :cond_0

    if-ge p1, v2, :cond_0

    if-lt p2, v3, :cond_0

    if-ge p2, v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private refreshRecentTasksList(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "firstScreenful"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 553
    .local p1, "recentTasksList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 554
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onTasksLoaded(Ljava/util/ArrayList;Z)V

    .line 558
    :goto_0
    return-void

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->loadTasksInBackground()V

    goto :goto_0
.end method

.method private showIfReady()V
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mWaitingToShow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->showImpl(Z)V

    .line 263
    :cond_0
    return-void
.end method

.method private showImpl(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 275
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mShowing:Z

    .line 277
    if-eqz p1, :cond_2

    .line 279
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 282
    .local v0, "noApps":Z
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 283
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setFocusable(Z)V

    .line 284
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setFocusableInTouchMode(Z)V

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->requestFocus()Z

    .line 293
    .end local v0    # "noApps":Z
    :cond_1
    :goto_0
    return-void

    .line 287
    :cond_2
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mWaitingToShow:Z

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mPopup:Landroid/widget/PopupMenu;

    if-eqz v1, :cond_1

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mPopup:Landroid/widget/PopupMenu;

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->dismiss()V

    goto :goto_0
.end method

.method private updateIcon(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/drawable/Drawable;ZZ)V
    .locals 3
    .param p1, "h"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    .param p2, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p3, "show"    # Z
    .param p4, "anim"    # Z

    .prologue
    .line 418
    if-eqz p2, :cond_1

    .line 419
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 420
    if-eqz p3, :cond_1

    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 421
    if-eqz p4, :cond_0

    .line 422
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->iconView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    const v2, 0x7f040051

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 425
    :cond_0
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->iconView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 428
    :cond_1
    return-void
.end method

.method private updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V
    .locals 3
    .param p1, "h"    # Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    .param p2, "thumbnail"    # Landroid/graphics/Bitmap;
    .param p3, "show"    # Z
    .param p4, "anim"    # Z

    .prologue
    .line 431
    if-eqz p2, :cond_4

    .line 435
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 439
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailViewImageBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailViewImageBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailViewImageBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 442
    :cond_0
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 444
    :cond_1
    if-eqz p3, :cond_3

    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    .line 445
    if-eqz p4, :cond_2

    .line 446
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    const v2, 0x7f040051

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 449
    :cond_2
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 451
    :cond_3
    iput-object p2, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailViewImageBitmap:Landroid/graphics/Bitmap;

    .line 453
    :cond_4
    return-void
.end method

.method private updateUiElements()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 577
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 580
    .local v0, "items":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    if-lez v0, :cond_1

    move v4, v3

    :goto_1
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 583
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 586
    .local v1, "numRecentApps":I
    :goto_2
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f0c0000

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v3

    invoke-virtual {v4, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 588
    .local v2, "recentAppsAccessibilityDescription":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 589
    return-void

    .end local v0    # "items":I
    .end local v1    # "numRecentApps":I
    .end local v2    # "recentAppsAccessibilityDescription":Ljava/lang/String;
    :cond_0
    move v0, v3

    .line 577
    goto :goto_0

    .line 580
    .restart local v0    # "items":I
    :cond_1
    const/16 v4, 0x8

    goto :goto_1

    :cond_2
    move v1, v3

    .line 583
    goto :goto_2
.end method


# virtual methods
.method public clearRecentTasksList()V
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->cancelLoadingThumbnailsAndIcons(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)V

    .line 529
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onTaskLoadingCancelled()V

    .line 531
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {v0, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->cancelLoadingThumbnailsAndIcons(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)V

    .line 304
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->dismiss()V

    .line 305
    return-void
.end method

.method public dismissAndGoBack()V
    .locals 0

    .prologue
    .line 308
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->dismiss()V

    .line 309
    return-void
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 347
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v0, v2

    .line 348
    .local v0, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v1, v2

    .line 349
    .local v1, "y":I
    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->getWidth()I

    move-result v2

    if-ge v0, v2, :cond_0

    if-ltz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 350
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    .line 352
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public handleOnClick(Ljava/lang/Object;)V
    .locals 6
    .param p1, "view"    # Ljava/lang/Object;

    .prologue
    .line 605
    const/4 v0, 0x0

    .line 606
    .local v0, "ad":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    instance-of v3, p1, Landroid/view/View;

    if-eqz v3, :cond_2

    .line 607
    check-cast p1, Landroid/view/View;

    .end local p1    # "view":Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    iget-object v0, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->taskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 611
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    .line 613
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->show(Z)V

    .line 614
    if-eqz v0, :cond_1

    .line 615
    iget v3, v0, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->taskId:I

    if-ltz v3, :cond_3

    .line 617
    const-string v3, "multiwindow_facade"

    invoke-virtual {v1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 628
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->dismissAndGoBack()V

    .line 630
    :cond_1
    return-void

    .line 608
    .end local v1    # "context":Landroid/content/Context;
    .restart local p1    # "view":Ljava/lang/Object;
    :cond_2
    instance-of v3, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    if-eqz v3, :cond_0

    .line 609
    check-cast p1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .end local p1    # "view":Ljava/lang/Object;
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getTaskDescription()Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    move-result-object v0

    goto :goto_0

    .line 620
    .restart local v1    # "context":Landroid/content/Context;
    :cond_3
    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->intent:Landroid/content/Intent;

    .line 621
    .local v2, "intent":Landroid/content/Intent;
    const v3, 0x10104000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 624
    const-string v3, "MultiwindowRecentsPanelView"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting activity "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    const/4 v3, 0x0

    new-instance v4, Landroid/os/UserHandle;

    const/4 v5, -0x2

    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/Bundle;Landroid/os/UserHandle;)V

    goto :goto_1
.end method

.method public handleSwipe(Ljava/lang/Object;)V
    .locals 7
    .param p1, "view"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    .line 637
    const/4 v0, 0x0

    .line 638
    .local v0, "ad":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    instance-of v2, p1, Landroid/view/View;

    if-eqz v2, :cond_2

    move-object v2, p1

    .line 639
    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    iget-object v0, v2, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->taskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 643
    :cond_0
    :goto_0
    if-nez v0, :cond_3

    .line 644
    const-string v2, "MultiwindowRecentsPanelView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Not able to find activity description for swiped task; view="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 673
    :cond_1
    :goto_1
    return-void

    .line 640
    :cond_2
    instance-of v2, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    if-eqz v2, :cond_0

    move-object v2, p1

    .line 641
    check-cast v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getTaskDescription()Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    move-result-object v0

    goto :goto_0

    .line 647
    :cond_3
    const-string v2, "MultiwindowRecentsPanelView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Jettison "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    .line 651
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 656
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 657
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->dismissAndGoBack()V

    .line 662
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/ActivityManager;

    .line 664
    .local v1, "am":Landroid/app/ActivityManager;
    if-eqz v1, :cond_1

    .line 665
    iget v2, v0, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->persistentTaskId:I

    invoke-virtual {v1, v2, v5}, Landroid/app/ActivityManager;->removeTask(II)Z

    .line 668
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    const v3, 0x7f08003f

    new-array v4, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 670
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->sendAccessibilityEvent(I)V

    .line 671
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public isInContentArea(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 233
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->pointInside(IILandroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 234
    const/4 v0, 0x1

    .line 236
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 312
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 315
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    .line 316
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mShowing:Z

    if-eqz v1, :cond_1

    .line 317
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 318
    .local v0, "transitioner":Landroid/animation/LayoutTransition;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 319
    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->createCustomAnimations(Landroid/animation/LayoutTransition;)V

    .line 325
    .end local v0    # "transitioner":Landroid/animation/LayoutTransition;
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 322
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->clearRecentTasksList()V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 328
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 331
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 377
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 379
    const v1, 0x7f0f00db

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    .line 380
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    invoke-direct {v1, p0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mListAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    .line 381
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    instance-of v1, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;

    if-eqz v1, :cond_1

    .line 382
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;

    .line 384
    .local v0, "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mListAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-interface {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;->setAdapter(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;)V

    .line 385
    invoke-interface {v0, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;->setCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;)V

    .line 390
    const v1, 0x7f0f00d9

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsScrim:Landroid/view/View;

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsScrim:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 393
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mHighEndGfx:Z

    .line 394
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mHighEndGfx:Z

    if-nez v1, :cond_2

    .line 395
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsScrim:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 401
    :cond_0
    :goto_0
    return-void

    .line 387
    .end local v0    # "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "missing Recents[Horizontal]ScrollView"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 396
    .restart local v0    # "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsScrim:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    instance-of v1, v1, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    .line 398
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsScrim:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    sget-object v2, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setTileModeY(Landroid/graphics/Shader$TileMode;)V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 684
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mPopup:Landroid/widget/PopupMenu;

    if-eqz v0, :cond_0

    .line 685
    const/4 v0, 0x1

    .line 687
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 633
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    invoke-virtual {p0, p2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->handleOnClick(Ljava/lang/Object;)V

    .line 634
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 339
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 340
    return-void
.end method

.method public onTaskLoadingCancelled()V
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 536
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mListAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->notifyDataSetInvalidated()V

    .line 539
    :cond_0
    return-void
.end method

.method onTaskThumbnailLoaded(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V
    .locals 8
    .param p1, "td"    # Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 456
    monitor-enter p1

    .line 457
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    if-eqz v6, :cond_2

    .line 458
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    instance-of v6, v6, Landroid/view/ViewGroup;

    if-eqz v6, :cond_1

    .line 459
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v2, Landroid/view/ViewGroup;

    .line 461
    .local v2, "container":Landroid/view/ViewGroup;
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 462
    .local v1, "childCnt":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_2

    .line 463
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 464
    .local v5, "v":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v6

    instance-of v6, v6, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    if-eqz v6, :cond_0

    .line 465
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    .line 466
    .local v3, "h":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    iget-boolean v6, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->loadedThumbnailAndIcon:Z

    if-nez v6, :cond_0

    iget-object v6, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->taskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    if-ne v6, p1, :cond_0

    .line 471
    const/4 v0, 0x0

    .line 472
    .local v0, "animateShow":Z
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct {p0, v3, v6, v7, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateIcon(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/drawable/Drawable;ZZ)V

    .line 473
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct {p0, v3, v6, v7, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V

    .line 474
    const/4 v6, 0x1

    iput-boolean v6, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->loadedThumbnailAndIcon:Z

    .line 462
    .end local v0    # "animateShow":Z
    .end local v3    # "h":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 478
    .end local v1    # "childCnt":I
    .end local v2    # "container":Landroid/view/ViewGroup;
    .end local v4    # "i":I
    .end local v5    # "v":Landroid/view/View;
    :cond_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    instance-of v6, v6, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;

    if-eqz v6, :cond_2

    .line 479
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v6, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->updateIcon(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V

    .line 480
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v6, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;

    invoke-virtual {v6, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLSurfaceView;->updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)V

    .line 483
    :cond_2
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->showIfReady()V

    .line 485
    return-void

    .line 483
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public onTasksLoaded()V
    .locals 2

    .prologue
    .line 569
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    instance-of v1, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;

    if-eqz v1, :cond_0

    .line 570
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;

    .line 572
    .local v0, "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    invoke-interface {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;->onTasksLoaded()V

    .line 574
    .end local v0    # "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    :cond_0
    return-void
.end method

.method public onTasksLoaded(Ljava/util/ArrayList;Z)V
    .locals 1
    .param p2, "firstScreenful"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 561
    .local p1, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 562
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    .line 566
    :goto_0
    return-void

    .line 564
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public onUiHidden()V
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mShowing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 297
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 298
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->clearRecentTasksList()V

    .line 300
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 512
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onWindowFocusChanged(Z)V

    .line 514
    if-nez p1, :cond_0

    .line 515
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->dismissAndGoBack()V

    .line 517
    :cond_0
    return-void
.end method

.method public refreshViews()V
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mListAdapter:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->notifyDataSetInvalidated()V

    .line 543
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateUiElements()V

    .line 544
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->showIfReady()V

    .line 545
    return-void
.end method

.method public setMinSwipeAlpha(F)V
    .locals 2
    .param p1, "minAlpha"    # F

    .prologue
    .line 404
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    instance-of v1, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;

    if-eqz v1, :cond_0

    .line 405
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsContainer:Landroid/view/View;

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;

    .line 407
    .local v0, "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    invoke-interface {v0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;->setMinSwipeAlpha(F)V

    .line 409
    .end local v0    # "scrollView":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$RecentsScrollView;
    :cond_0
    return-void
.end method

.method public setRecentsWindow(Lcom/sec/android/app/FlashBarService/RecentsWindow;)V
    .locals 0
    .param p1, "rw"    # Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentsWindow:Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .line 212
    return-void
.end method

.method public show(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    const/4 v1, 0x0

    .line 241
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->show(ZLjava/util/ArrayList;ZZ)V

    .line 242
    return-void
.end method

.method public show(ZLjava/util/ArrayList;ZZ)V
    .locals 1
    .param p1, "show"    # Z
    .param p3, "firstScreenful"    # Z
    .param p4, "animateIconOfFirstTask"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;ZZ)V"
        }
    .end annotation

    .prologue
    .line 246
    .local p2, "recentTaskDescriptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    iput-boolean p4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mAnimateIconOfFirstTask:Z

    .line 247
    iput-boolean p4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mWaitingForWindowAnimation:Z

    .line 248
    if-eqz p1, :cond_0

    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mWaitingToShow:Z

    .line 250
    invoke-direct {p0, p2, p3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->refreshRecentTasksList(Ljava/util/ArrayList;Z)V

    .line 251
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->showIfReady()V

    .line 255
    :goto_0
    return-void

    .line 253
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->showImpl(Z)V

    goto :goto_0
.end method

.method public final updateValuesFromResources()V
    .locals 2

    .prologue
    .line 368
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 369
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0a0108

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mItemWidth:I

    .line 370
    const v1, 0x7f0a0109

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mItemHeight:I

    .line 371
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mFitThumbnailToXY:Z

    .line 373
    return-void
.end method
