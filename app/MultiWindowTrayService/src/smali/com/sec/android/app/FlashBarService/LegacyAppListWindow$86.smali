.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 4912
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 26
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 4914
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getAction()I

    move-result v5

    .line 4918
    .local v5, "action":I
    const/16 v20, 0x6

    move/from16 v0, v20

    if-eq v5, v0, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v20

    if-nez v20, :cond_0

    .line 4919
    const/16 v20, 0x0

    .line 5103
    :goto_0
    return v20

    .line 4922
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v20

    if-eqz v20, :cond_1

    .line 4923
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v11

    .line 4924
    .local v11, "label":Ljava/lang/CharSequence;
    const-string v20, "appIcon"

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    const-string v20, "historybar"

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_1

    .line 4925
    const/16 v20, 0x0

    goto :goto_0

    .line 4929
    .end local v11    # "label":Ljava/lang/CharSequence;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mLunchBlock:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 4930
    const/16 v20, 0x1

    goto :goto_0

    .line 4933
    :cond_2
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getX()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v18, v0

    .line 4934
    .local v18, "x":I
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getY()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v19, v0

    .line 4935
    .local v19, "y":I
    const/4 v12, 0x0

    .line 4936
    .local v12, "left":I
    const/16 v16, 0x0

    .line 4937
    .local v16, "top":I
    const/4 v15, 0x0

    .line 4938
    .local v15, "right":I
    const/4 v7, 0x0

    .line 4939
    .local v7, "bottom":I
    const/4 v14, 0x0

    .line 4941
    .local v14, "rect":Landroid/graphics/Rect;
    packed-switch v5, :pswitch_data_0

    .line 5103
    :cond_3
    :goto_1
    const/16 v20, 0x1

    goto :goto_0

    .line 4943
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v20

    if-eqz v20, :cond_4

    .line 4944
    invoke-virtual/range {p2 .. p2}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v11

    .line 4945
    .restart local v11    # "label":Ljava/lang/CharSequence;
    const-string v20, "appIcon"

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 4946
    const/16 v20, 0x1

    goto :goto_0

    .line 4950
    .end local v11    # "label":Ljava/lang/CharSequence;
    :cond_4
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v20

    if-eqz v20, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 4951
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v12, v18, v20

    .line 4952
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v16, v19, v20

    .line 4953
    if-gez v16, :cond_5

    const/16 v16, 0x0

    .line 4954
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    add-int v15, v12, v20

    .line 4955
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    move/from16 v20, v0

    add-int v7, v16, v20

    .line 4956
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "rect":Landroid/graphics/Rect;
    move/from16 v0, v16

    invoke-direct {v14, v12, v0, v15, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4966
    .restart local v14    # "rect":Landroid/graphics/Rect;
    :goto_2
    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v20, :cond_6

    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v20, :cond_9

    .line 4967
    :cond_6
    if-eqz v14, :cond_3

    .line 4968
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_1

    .line 4958
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/AppListController;->isTemplateItem(I)Z

    move-result v20

    if-eqz v20, :cond_8

    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v20, :cond_8

    .line 4959
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4960
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "rect":Landroid/graphics/Rect;
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v14    # "rect":Landroid/graphics/Rect;
    goto :goto_2

    .line 4962
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v14

    goto/16 :goto_2

    .line 4971
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v22

    add-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v23

    add-int v22, v22, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v24

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v25

    add-int v24, v24, v25

    invoke-virtual/range {v20 .. v24}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_1

    .line 4979
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v20

    if-eqz v20, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_d

    .line 4980
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v12, v18, v20

    .line 4981
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v16, v19, v20

    .line 4983
    if-gez v16, :cond_a

    const/16 v16, 0x0

    .line 4984
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    add-int/lit16 v0, v0, 0xc8

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    add-int/lit16 v12, v0, 0xc8

    .line 4985
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    add-int v15, v12, v20

    .line 4986
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    move/from16 v20, v0

    add-int v7, v16, v20

    .line 4987
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "rect":Landroid/graphics/Rect;
    move/from16 v0, v16

    invoke-direct {v14, v12, v0, v15, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 4996
    .restart local v14    # "rect":Landroid/graphics/Rect;
    :goto_3
    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v20, :cond_c

    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v20, :cond_f

    .line 4997
    :cond_c
    if-eqz v14, :cond_3

    .line 4998
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    invoke-virtual/range {v20 .. v24}, Landroid/widget/ImageView;->layout(IIII)V

    .line 4999
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 4989
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v20

    if-eqz v20, :cond_e

    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v20, :cond_e

    .line 4990
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "rect":Landroid/graphics/Rect;
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayWidth:I
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDisplayHeight:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v23

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-direct {v14, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v14    # "rect":Landroid/graphics/Rect;
    goto :goto_3

    .line 4992
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentGuideRect(II)Landroid/graphics/Rect;

    move-result-object v14

    goto/16 :goto_3

    .line 5002
    :cond_f
    if-eqz v14, :cond_10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_10

    .line 5003
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 5005
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/android/app/FlashBarService/AppListController;->mPreviewFullAppZone:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setPreviewFullAppZone(I)V

    .line 5006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v21

    const/16 v22, 0x1

    const/16 v23, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static/range {v20 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 5007
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v22

    add-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v23 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v23

    add-int v22, v22, v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v24 .. v24}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v24

    add-int v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPreviewFocusedRect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSplitGuidelineShadow:I
    invoke-static/range {v25 .. v25}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v25

    add-int v24, v24, v25

    invoke-virtual/range {v20 .. v24}, Landroid/widget/ImageView;->layout(IIII)V

    goto/16 :goto_1

    .line 5015
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 5016
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mGuidelineLayout:Landroid/widget/FrameLayout;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/FrameLayout;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 5017
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static/range {v20 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 5018
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v20

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 5019
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 5024
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsTemplateItem:Z
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$14802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 5025
    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v20, :cond_11

    .line 5026
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    move-object/from16 v20, v0

    sget-object v21, Lcom/sec/android/app/FlashBarService/AppListController;->mAppListItemDropIvt:[B

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mVibrator:Landroid/os/SystemVibrator;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v22

    invoke-virtual/range {v20 .. v22}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 5028
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSideSyncDropGuideline:Lcom/sec/android/app/FlashBarService/GuideLineImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$6900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/sec/android/app/FlashBarService/GuideLineImageView;

    move-result-object v20

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/GuideLineImageView;->setVisibility(I)V

    .line 5029
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mPositionGuideline:Landroid/widget/ImageView;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ImageView;

    move-result-object v20

    const/16 v21, 0x4

    invoke-virtual/range {v20 .. v21}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 5030
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mExpandAppList:Z
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateSplitGuideVisibility(ZZZ)V
    invoke-static/range {v20 .. v23}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$1100(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;ZZZ)V

    .line 5031
    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v20, :cond_12

    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v20, :cond_13

    .line 5032
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->hideGuidelineWindow()V
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    .line 5035
    :cond_13
    const-string v20, "LegacyAppListWindow"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "mGuidelineDragListener ACTION_DROP : appIconIndex="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, "  mcurDstIndex="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5036
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIsPenWindowOnly:Z
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$12800(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Z

    move-result v20

    if-eqz v20, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v20

    if-eqz v20, :cond_17

    .line 5037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v20

    if-eqz v20, :cond_16

    .line 5038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v12, v18, v20

    .line 5039
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v16, v19, v20

    .line 5041
    if-gez v16, :cond_14

    const/16 v16, 0x0

    .line 5042
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    add-int/lit16 v0, v0, 0xc8

    move/from16 v20, v0

    move/from16 v0, v20

    if-ge v12, v0, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    move/from16 v0, v20

    neg-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    add-int/lit16 v12, v0, 0xc8

    .line 5043
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->getApplistIndicatorSize()I

    move-result v20

    add-int v16, v16, v20

    .line 5044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefWidth:I

    move/from16 v20, v0

    add-int v15, v12, v20

    .line 5045
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mWindowDefHeight:I

    move/from16 v20, v0

    add-int v7, v16, v20

    .line 5046
    new-instance v14, Landroid/graphics/Rect;

    .end local v14    # "rect":Landroid/graphics/Rect;
    move/from16 v0, v16

    invoke-direct {v14, v12, v0, v15, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 5048
    .restart local v14    # "rect":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v13

    .line 5049
    .local v13, "r":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 5050
    .local v6, "appName":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v21, v0

    const v22, 0x7f08002c

    const/16 v23, 0x1

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v6, v23, v24

    invoke-virtual/range {v21 .. v23}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v21

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/widget/Toast;->show()V

    .line 5052
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v14}, Lcom/sec/android/app/FlashBarService/AppListController;->startFreeStyleActivity(ILandroid/graphics/Rect;)V

    .line 5097
    .end local v6    # "appName":Ljava/lang/String;
    .end local v13    # "r":Landroid/content/pm/ResolveInfo;
    :cond_16
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    .line 5098
    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v20, :cond_3

    .line 5099
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->startCollapseTimer()V
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5600(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V

    goto/16 :goto_1

    .line 5055
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v20

    if-eqz v20, :cond_20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/app/Dialog;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/app/Dialog;->isShowing()Z

    move-result v20

    if-eqz v20, :cond_20

    .line 5056
    const/4 v9, 0x1

    .line 5057
    .local v9, "expand":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFrontActivityMultiWindowStyle(I)Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-result-object v10

    .line 5058
    .local v10, "frontActivityMultiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    if-eqz v10, :cond_18

    const/16 v20, 0x2

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v20

    if-eqz v20, :cond_18

    const/high16 v20, 0x200000

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v20

    if-nez v20, :cond_18

    const/16 v20, 0x1000

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v20

    if-nez v20, :cond_18

    .line 5061
    const/4 v9, 0x0

    .line 5062
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    .line 5065
    :cond_18
    new-instance v17, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-direct/range {v17 .. v17}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>()V

    .line 5067
    .local v17, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->getCurrentZone(II)I

    move-result v8

    .line 5068
    .local v8, "currentZone":I
    const/16 v20, 0xf

    move/from16 v0, v20

    if-eq v8, v0, :cond_19

    if-nez v8, :cond_1b

    .line 5070
    :cond_19
    const/16 v20, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 5076
    :cond_1a
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v9, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updateFlashBarState(ZZ)Z

    .line 5077
    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 5079
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_1d

    .line 5080
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v21

    const/16 v22, 0x0

    const/16 v23, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    move-object/from16 v2, v22

    move/from16 v3, v23

    move-object/from16 v4, v17

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->moveOnlySpecificTaskToFront(ILandroid/os/Bundle;ILcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 5081
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const-string v21, "TRAY"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v22 .. v22}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v20 .. v22}, Lcom/sec/android/app/FlashBarService/LoggingHelper;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 5082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mThumbnailTaskId:I
    invoke-static/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4702(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)I

    goto/16 :goto_1

    .line 5071
    :cond_1b
    sget-boolean v20, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v20, :cond_1c

    const/16 v20, 0xf

    move/from16 v0, v20

    if-eq v8, v0, :cond_1a

    .line 5072
    :cond_1c
    const/16 v20, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 5073
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 5084
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v20

    if-gtz v20, :cond_1e

    .line 5085
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Intent;->getFlags()I

    move-result v21

    const v22, -0x8000001

    and-int v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 5087
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Intent;->getFlags()I

    move-result v20

    const/high16 v21, 0x8000000

    and-int v20, v20, v21

    if-eqz v20, :cond_1f

    .line 5088
    const/high16 v20, 0x80000

    const/16 v21, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 5090
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v20 .. v20}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 5091
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 5095
    .end local v8    # "currentZone":I
    .end local v9    # "expand":Z
    .end local v10    # "frontActivityMultiWindowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v17    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$86;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mcurDstIndex:I
    invoke-static/range {v21 .. v21}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v21

    move-object/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivity(III)V

    goto/16 :goto_4

    .line 4941
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
