.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 1032
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1037
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v10

    if-nez v10, :cond_1

    .line 1103
    :cond_0
    :goto_0
    return-void

    .line 1040
    :cond_1
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v10

    const/4 v11, -0x1

    if-le v10, v11, :cond_0

    .line 1041
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-boolean v12, v12, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mSupportMultiInstance:Z

    const/16 v13, 0x64

    invoke-virtual {v10, v11, v12, v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v3

    .line 1042
    .local v3, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1045
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1046
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntents:Ljava/util/List;
    invoke-static {v10, v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Ljava/util/List;)Ljava/util/List;

    .line 1049
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1050
    .local v1, "displayFrame":Landroid/graphics/Rect;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    const/16 v12, 0x80

    invoke-virtual {v10, v11, v12}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 1051
    .local v0, "aInfo":Landroid/content/pm/ActivityInfo;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v10, v0}, Lcom/sec/android/app/FlashBarService/AppListController;->isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1052
    new-instance v4, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v10, 0x2

    invoke-direct {v4, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 1053
    .local v4, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v10, 0x800

    const/4 v11, 0x1

    invoke-virtual {v4, v10, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 1054
    invoke-virtual {v4, v1}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 1055
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10, v4}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1056
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v11

    const/4 v12, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static {v10, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 1058
    .end local v4    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 1059
    new-instance v9, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v10, 0x1

    invoke-direct {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 1060
    .local v9, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v11, 0x64

    const/4 v12, 0x1

    invoke-virtual {v10, v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v7

    .line 1061
    .local v7, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v6, 0x0

    .line 1063
    .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v7, :cond_9

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1064
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1065
    .local v8, "visibleTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v10

    if-nez v10, :cond_3

    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v11, 0x400

    invoke-virtual {v10, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v10

    if-nez v10, :cond_3

    .line 1067
    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v10

    if-nez v10, :cond_4

    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v10

    const/4 v11, 0x1

    if-eq v10, v11, :cond_3

    .line 1071
    :cond_4
    move-object v6, v8

    .line 1075
    .end local v8    # "visibleTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_5
    if-nez v6, :cond_6

    .line 1076
    const/4 v10, 0x0

    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1079
    .restart local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v11, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getRunningTaskCnt(ZZZ)I
    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;ZZZ)I

    move-result v5

    .line 1080
    .local v5, "runningCnt":I
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v10, v10, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v10

    if-lt v5, v10, :cond_7

    .line 1081
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getFlags()I

    move-result v11

    const v12, -0x8000001

    and-int/2addr v11, v12

    invoke-virtual {v10, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1084
    :cond_7
    iget-object v10, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1085
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->findCandidateZone(Ljava/util/List;)I
    invoke-static {v10, v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Ljava/util/List;)I

    move-result v10

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1086
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1092
    :goto_1
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v11

    const/4 v12, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static {v10, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 1088
    :cond_8
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 1089
    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1090
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    goto :goto_1

    .line 1094
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "runningCnt":I
    :cond_9
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 1095
    const/16 v10, 0xf

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1096
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1097
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$12;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/Intent;

    move-result-object v11

    const/4 v12, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static {v10, v11, v12}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0
.end method
