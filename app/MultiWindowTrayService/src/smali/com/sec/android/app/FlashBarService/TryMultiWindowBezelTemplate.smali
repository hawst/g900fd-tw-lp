.class public Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;
.super Landroid/app/Activity;
.source "TryMultiWindowBezelTemplate.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;
    }
.end annotation


# instance fields
.field drs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mAnimationTemplateIconMakeRunnable:Ljava/lang/Runnable;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mCheckPairedCreated:Z

.field mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mFadeInAnimation:Landroid/view/animation/Animation;

.field private mFinishRunnable:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mInvalidToast:Landroid/widget/Toast;

.field private mOrientation:I

.field private mPairedbubbleAnimation:Landroid/view/animation/Animation;

.field mPocketBezelTemplateCreateBtnListener:Landroid/view/View$OnClickListener;

.field private mPocketBtnClicked:Z

.field mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

.field private mResource:Landroid/content/res/Resources;

.field private mTryMWBezelAppIcon1:Landroid/widget/ImageView;

.field private mTryMWBezelAppIcon2:Landroid/widget/ImageView;

.field private mTryMWBezelAppIconSize:I

.field private mTryMWBezelAppViewer:Landroid/widget/LinearLayout;

.field private mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateApp:Landroid/widget/ImageView;

.field private mTryMWBezelTemplateContainer:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateCreateBtn:Landroid/widget/ImageView;

.field private mTryMWBezelTemplateEditBtn:Landroid/widget/ImageView;

.field private mTryMWBezelTemplateFocusFrame:Landroid/widget/LinearLayout;

.field private mTryMWBezelTemplateFocusFrame1:Landroid/widget/ImageView;

.field private mTryMWBezelTemplateFocusFrame2:Landroid/widget/ImageView;

.field private mTryMWBezelTemplateGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

.field private mTryMWBezelTemplateGridView:Landroid/widget/GridView;

.field private mTryMWBezelTemplateHelpBtn:Landroid/widget/ImageView;

.field private mTryMWBezelTemplateHelpPopup1:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHelpPopup2:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHelpPopup3:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHelpPopup4:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHelpPopupHolder2:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHelpPopupHolder3:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateHome:Landroid/widget/ImageView;

.field private mTryMWBezelTemplatePocketOpenBtn:Landroid/widget/RelativeLayout;

.field private mTryMWBezelTemplateToggleBtn:Landroid/widget/ImageView;

.field private mTryMWTemplateBezelHelperWidthCenter:I

.field private mTryMWTemplateCue2:Landroid/widget/ImageView;

.field private mTryMWTemplateCue3:Landroid/widget/ImageView;

.field private mWindowManager:Landroid/view/WindowManager;

.field private openButtonHeight:I

.field private openPocketTrayHeght:I

.field private pocketTopMargin:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 48
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppViewer:Landroid/widget/LinearLayout;

    .line 49
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    .line 50
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBtnClicked:Z

    .line 51
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCheckPairedCreated:Z

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mOrientation:I

    .line 63
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

    .line 64
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridView:Landroid/widget/GridView;

    .line 65
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 66
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateContainer:Landroid/widget/RelativeLayout;

    .line 67
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;

    .line 68
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder2:Landroid/widget/RelativeLayout;

    .line 69
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder3:Landroid/widget/RelativeLayout;

    .line 70
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup1:Landroid/widget/RelativeLayout;

    .line 71
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup2:Landroid/widget/RelativeLayout;

    .line 72
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup3:Landroid/widget/RelativeLayout;

    .line 73
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup4:Landroid/widget/RelativeLayout;

    .line 74
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplatePocketOpenBtn:Landroid/widget/RelativeLayout;

    .line 75
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    .line 90
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mInvalidToast:Landroid/widget/Toast;

    .line 91
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 92
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mHandler:Landroid/os/Handler;

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->drs:Ljava/util/List;

    .line 346
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$2;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

    .line 364
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$3;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBezelTemplateCreateBtnListener:Landroid/view/View$OnClickListener;

    .line 518
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$7;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mAnimationTemplateIconMakeRunnable:Ljava/lang/Runnable;

    .line 578
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$9;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mFinishRunnable:Ljava/lang/Runnable;

    .line 585
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIcon1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIcon2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateCreateBtn:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateCue3:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mAnimationTemplateIconMakeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->animationTemplateIconMake()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIconSize:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCheckPairedCreated:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPairedbubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateCue2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBtnClicked:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->updatePocketTrayLayout(IZZ)V

    return-void
.end method

.method private animationTemplateIconMake()V
    .locals 4

    .prologue
    .line 537
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCheckPairedCreated:Z

    .line 538
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;->notifyDataSetChanged()V

    .line 539
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 541
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup4:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 542
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup4:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 543
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mFinishRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 544
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPairedbubbleAnimation:Landroid/view/animation/Animation;

    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$8;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 555
    return-void
.end method

.method private generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 526
    .local p1, "drs":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/drawable/Drawable;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 527
    .local v3, "size":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 528
    .local v2, "info":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 529
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 528
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 532
    :cond_0
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-static {v4, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->createFolderIconWithPlate(Landroid/graphics/Bitmap;Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 533
    .local v0, "buttonBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v4
.end method

.method private getSizeOfStatusBar()I
    .locals 3

    .prologue
    .line 420
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 421
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 422
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 423
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method private init()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 109
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBtnClicked:Z

    .line 110
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    .line 112
    new-instance v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

    invoke-direct {v2, p0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

    .line 113
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mOrientation:I

    .line 114
    const v2, 0x7f0f012a

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/GridView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridView:Landroid/widget/GridView;

    .line 115
    const v2, 0x7f0f0129

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/widget/DrawerLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 117
    const v2, 0x7f0f012c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder1:Landroid/widget/RelativeLayout;

    .line 118
    const v2, 0x7f0f012e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder2:Landroid/widget/RelativeLayout;

    .line 119
    const v2, 0x7f0f0130

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder3:Landroid/widget/RelativeLayout;

    .line 120
    const v2, 0x7f0f012d

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup1:Landroid/widget/RelativeLayout;

    .line 121
    const v2, 0x7f0f012f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup2:Landroid/widget/RelativeLayout;

    .line 122
    const v2, 0x7f0f0131

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup3:Landroid/widget/RelativeLayout;

    .line 123
    const v2, 0x7f0f0132

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup4:Landroid/widget/RelativeLayout;

    .line 125
    const v2, 0x7f0f0111

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateCue2:Landroid/widget/ImageView;

    .line 126
    const v2, 0x7f0f0114

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateCue3:Landroid/widget/ImageView;

    .line 128
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridView:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$GridAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 130
    const v2, 0x7f0f0026

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplatePocketOpenBtn:Landroid/widget/RelativeLayout;

    .line 131
    const v2, 0x7f0f0122

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateContainer:Landroid/widget/RelativeLayout;

    .line 132
    const v2, 0x7f0f012b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    .line 134
    const v2, 0x7f0f0124

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHome:Landroid/widget/ImageView;

    .line 135
    const v2, 0x7f0f0125

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateApp:Landroid/widget/ImageView;

    .line 136
    const v2, 0x7f0f0100

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame1:Landroid/widget/ImageView;

    .line 137
    const v2, 0x7f0f0101

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame2:Landroid/widget/ImageView;

    .line 138
    const v2, 0x7f0f0028

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateToggleBtn:Landroid/widget/ImageView;

    .line 139
    const v2, 0x7f0f002e

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateCreateBtn:Landroid/widget/ImageView;

    .line 140
    const v2, 0x7f0f002b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateEditBtn:Landroid/widget/ImageView;

    .line 141
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateEditBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 142
    const v2, 0x7f0f0031

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpBtn:Landroid/widget/ImageView;

    .line 143
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 146
    const v2, 0x7f0f0126

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIcon1:Landroid/widget/ImageView;

    .line 147
    const v2, 0x7f0f0127

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIcon2:Landroid/widget/ImageView;

    .line 149
    const v2, 0x7f0f00ff

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame:Landroid/widget/LinearLayout;

    .line 151
    const v2, 0x7f0f0123

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppViewer:Landroid/widget/LinearLayout;

    .line 153
    const v2, 0x7f040024

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 154
    const v2, 0x7f040052

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 155
    const v2, 0x7f04003b

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPairedbubbleAnimation:Landroid/view/animation/Animation;

    .line 157
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopup1:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 159
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 160
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridView:Landroid/widget/GridView;

    invoke-virtual {v2, p0}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 162
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateToggleBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplatePocketOpenBtn:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateCreateBtn:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBezelTemplateCreateBtnListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f0a006b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->openButtonHeight:I

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f0a0068

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->openPocketTrayHeght:I

    .line 168
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    const v3, 0x7f0a0158

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIconSize:I

    .line 170
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v2, v4}, Landroid/support/v4/widget/DrawerLayout;->setFocusableInTouchMode(Z)V

    .line 172
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->setDrawerLayoutListener()V

    .line 174
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020086

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 176
    .local v0, "drawable1":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020087

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 178
    .local v1, "drawable2":Landroid/graphics/drawable/Drawable;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->drs:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->drs:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mResource:Landroid/content/res/Resources;

    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->initFolderResources(Landroid/content/res/Resources;)V

    .line 181
    return-void
.end method

.method private makeScreenForLandscape()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 262
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 263
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 265
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 266
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 267
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 269
    :cond_0
    iget v7, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    .line 270
    iget v7, v0, Landroid/graphics/Point;->y:I

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    .line 272
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 274
    .local v5, "mTryMWBezelTemplateHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 275
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 276
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 278
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 280
    .local v2, "mTryMWBezelTemplateApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 281
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 282
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 286
    .local v4, "mTryMWBezelTemplateFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getSizeOfStatusBar()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 287
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 288
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame1:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 290
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 292
    .local v3, "mTryMWBezelTemplateFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getSizeOfStatusBar()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 293
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 294
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBtnClicked:Z

    if-eqz v7, :cond_1

    .line 297
    invoke-direct {p0, v9, v9, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->updatePocketTrayLayout(IZZ)V

    .line 311
    :goto_0
    return-void

    .line 299
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 300
    .local v1, "displaySizeTemp":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 301
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 303
    :cond_2
    iget v7, v1, Landroid/graphics/Point;->y:I

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    .line 304
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->openButtonHeight:I

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->pocketTopMargin:I

    .line 305
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 307
    .local v6, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->pocketTopMargin:I

    invoke-virtual {v6, v9, v7, v9, v9}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 308
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private makeScreenForPortrait()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 209
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 210
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v7, v8}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 211
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 213
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 214
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 215
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 217
    :cond_0
    iget v7, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    .line 218
    iget v7, v0, Landroid/graphics/Point;->y:I

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    .line 220
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 222
    .local v5, "mTryMWBezelTemplateHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 223
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 224
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 228
    .local v2, "mTryMWBezelTemplateApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 229
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 230
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 232
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame1:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 234
    .local v4, "mTryMWBezelTemplateFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 235
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 236
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame1:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 238
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 240
    .local v3, "mTryMWBezelTemplateFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWTemplateBezelHelperWidthCenter:I

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 241
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v7, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 242
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mPocketBtnClicked:Z

    if-eqz v7, :cond_1

    .line 244
    invoke-direct {p0, v9, v9, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->updatePocketTrayLayout(IZZ)V

    .line 258
    :goto_0
    return-void

    .line 246
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 247
    .local v1, "displaySizeTemp":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 248
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 250
    :cond_2
    iget v7, v1, Landroid/graphics/Point;->y:I

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    .line 251
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->openButtonHeight:I

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->pocketTopMargin:I

    .line 252
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    .local v6, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->pocketTopMargin:I

    invoke-virtual {v6, v9, v7, v9, v9}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 255
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v8, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v8, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private setDrawerLayoutListener()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setScrimColor(I)V

    .line 315
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDrawerBezelTemplateLayout:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$1;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 344
    return-void
.end method

.method private updatePocketTrayLayout(IZZ)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "isStarting"    # Z
    .param p3, "forceClose"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 397
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    .local v1, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 401
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 402
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 404
    :cond_0
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    .line 405
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->openButtonHeight:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->openPocketTrayHeght:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->pocketTopMargin:I

    .line 406
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->pocketTopMargin:I

    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 407
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 409
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateToggleBtn:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 410
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplatePocketOpenBtn:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 411
    if-eqz p3, :cond_1

    .line 412
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateToggleBtn:Landroid/widget/ImageView;

    const v3, 0x7f0200d3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 416
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpPopupHolder2:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 417
    return-void

    .line 414
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateToggleBtn:Landroid/widget/ImageView;

    const v3, 0x7f0200d5

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public invalidTouch()V
    .locals 2

    .prologue
    .line 572
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mInvalidToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 573
    const v0, 0x7f08003a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mInvalidToast:Landroid/widget/Toast;

    .line 575
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mInvalidToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 576
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 185
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 186
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mOrientation:I

    .line 187
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 188
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->makeScreenForPortrait()V

    .line 190
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 191
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->makeScreenForLandscape()V

    .line 193
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 100
    .local v1, "w":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 101
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 102
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 104
    const v2, 0x7f03002c

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->setContentView(I)V

    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->init()V

    .line 106
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 198
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 199
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 200
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->makeScreenForPortrait()V

    .line 202
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 203
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->makeScreenForLandscape()V

    .line 205
    :cond_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 559
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 568
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 561
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateGridView:Landroid/widget/GridView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateEditBtn:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelTemplateHelpBtn:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->invalidTouch()V

    goto :goto_0

    .line 559
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public startPocketOpenAnimation()V
    .locals 2

    .prologue
    .line 376
    const/4 v0, 0x0

    .line 377
    .local v0, "mPocketOpenAnimation":Landroid/view/animation/Animation;
    const v1, 0x7f04004a

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 380
    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$4;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 393
    return-void
.end method

.method public trytemplateAnimationTemplate()V
    .locals 28

    .prologue
    .line 428
    const/4 v6, 0x0

    .line 429
    .local v6, "distance1X":F
    const/4 v15, 0x0

    .line 430
    .local v15, "distance2X":F
    const/4 v10, 0x0

    .line 431
    .local v10, "distance1Y":F
    const/16 v19, 0x0

    .line 433
    .local v19, "distance2Y":F
    const/16 v26, 0x0

    .line 434
    .local v26, "mDisplayAppIconX":I
    const/16 v27, 0x0

    .line 435
    .local v27, "mDisplayAppIconY":I
    const/16 v24, 0x0

    .line 436
    .local v24, "mDisplayApp2IconX":I
    const/16 v25, 0x0

    .line 438
    .local v25, "mDisplayApp2IconY":I
    new-instance v23, Landroid/graphics/Point;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Point;-><init>()V

    .line 439
    .local v23, "displaySize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 440
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 442
    :cond_0
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    .line 443
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    .line 445
    const/high16 v10, -0x3d380000    # -100.0f

    .line 446
    const/high16 v19, -0x3d380000    # -100.0f

    .line 447
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    add-int/lit16 v3, v3, 0xc8

    int-to-float v6, v3

    .line 448
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    add-int/lit16 v3, v3, 0xc8

    int-to-float v15, v3

    .line 450
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 451
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    div-int/lit8 v26, v3, 0x2

    .line 452
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    div-int/lit8 v24, v3, 0x2

    .line 453
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    div-int/lit8 v27, v3, 0x4

    .line 454
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v25, v3, 0x3

    .line 456
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 457
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    div-int/lit8 v26, v3, 0x4

    .line 458
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v24, v3, 0x3

    .line 459
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    div-int/lit8 v27, v3, 0x2

    .line 460
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mDisplayHeight:I

    div-int/lit8 v25, v3, 0x2

    .line 463
    :cond_2
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x0

    move/from16 v0, v26

    int-to-float v4, v0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move/from16 v0, v27

    int-to-float v8, v0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 466
    .local v2, "translateAnimation1":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 467
    new-instance v3, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 469
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    const/4 v12, 0x0

    move/from16 v0, v24

    int-to-float v13, v0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v11 .. v19}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 472
    .local v11, "translateAnimation2":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v11, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 473
    new-instance v3, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    invoke-virtual {v11, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 475
    new-instance v20, Landroid/view/animation/AlphaAnimation;

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3e4ccccd    # 0.2f

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 476
    .local v20, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 477
    const-wide/16 v4, 0x1f4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 479
    new-instance v21, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 480
    .local v21, "animationSet1":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 481
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 483
    new-instance v22, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 484
    .local v22, "animationSet2":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 485
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 487
    new-instance v3, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$5;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$5;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 500
    new-instance v3, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$6;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate$6;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 514
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIcon1:Landroid/widget/ImageView;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 515
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezelTemplate;->mTryMWBezelAppIcon2:Landroid/widget/ImageView;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 516
    return-void
.end method
