.class public Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;
.super Ljava/lang/Object;
.source "RecentsScrollViewPerformanceHelper.java"


# instance fields
.field private mFadingEdgeLength:I

.field private mIsVertical:Z

.field private mScrollView:Landroid/view/View;

.field private mSoftwareRendered:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;Z)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "scrollView"    # Landroid/view/View;
    .param p4, "isVertical"    # Z

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mSoftwareRendered:Z

    .line 58
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mScrollView:Landroid/view/View;

    .line 59
    sget-object v1, Lcom/android/internal/R$styleable;->View:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 60
    .local v0, "a":Landroid/content/res/TypedArray;
    const/16 v1, 0x19

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledFadingEdgeLength()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mFadingEdgeLength:I

    .line 62
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 63
    iput-boolean p4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mIsVertical:Z

    .line 64
    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;Z)Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "scrollView"    # Landroid/view/View;
    .param p3, "isVertical"    # Z

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 49
    .local v0, "isTablet":Z
    if-nez v0, :cond_0

    .line 50
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;Z)V

    .line 52
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addViewCallback(Landroid/view/View;)V
    .locals 3
    .param p1, "newLinearLayoutChild"    # Landroid/view/View;

    .prologue
    .line 77
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mSoftwareRendered:Z

    if-eqz v1, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    .line 80
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->labelView:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    .line 81
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->buildDrawingCache()V

    .line 83
    .end local v0    # "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    :cond_0
    return-void
.end method

.method public drawCallback(Landroid/graphics/Canvas;IIIIIIFFFF)V
    .locals 21
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "left"    # I
    .param p3, "right"    # I
    .param p4, "top"    # I
    .param p5, "bottom"    # I
    .param p6, "scrollX"    # I
    .param p7, "scrollY"    # I
    .param p8, "topFadingEdgeStrength"    # F
    .param p9, "bottomFadingEdgeStrength"    # F
    .param p10, "leftFadingEdgeStrength"    # F
    .param p11, "rightFadingEdgeStrength"    # F

    .prologue
    .line 90
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mSoftwareRendered:Z

    if-eqz v2, :cond_0

    .line 92
    :cond_0
    new-instance v18, Landroid/graphics/Paint;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Paint;-><init>()V

    .line 93
    .local v18, "p":Landroid/graphics/Paint;
    new-instance v17, Landroid/graphics/Matrix;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Matrix;-><init>()V

    .line 96
    .local v17, "matrix":Landroid/graphics/Matrix;
    new-instance v1, Landroid/graphics/LinearGradient;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, -0x34000000    # -3.3554432E7f

    const/4 v7, 0x0

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v1 .. v8}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 99
    .local v1, "fade":Landroid/graphics/Shader;
    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 102
    const/4 v13, 0x0

    .line 103
    .local v13, "drawTop":Z
    const/4 v10, 0x0

    .line 104
    .local v10, "drawBottom":Z
    const/4 v11, 0x0

    .line 105
    .local v11, "drawLeft":Z
    const/4 v12, 0x0

    .line 107
    .local v12, "drawRight":Z
    const/16 v20, 0x0

    .line 108
    .local v20, "topFadeStrength":F
    const/4 v9, 0x0

    .line 109
    .local v9, "bottomFadeStrength":F
    const/4 v15, 0x0

    .line 110
    .local v15, "leftFadeStrength":F
    const/16 v19, 0x0

    .line 112
    .local v19, "rightFadeStrength":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mFadingEdgeLength:I

    int-to-float v14, v2

    .line 113
    .local v14, "fadeHeight":F
    float-to-int v0, v14

    move/from16 v16, v0

    .line 117
    .local v16, "length":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mIsVertical:Z

    if-eqz v2, :cond_1

    add-int v2, p4, v16

    sub-int v3, p5, v16

    if-le v2, v3, :cond_1

    .line 118
    sub-int v2, p5, p4

    div-int/lit8 v16, v2, 0x2

    .line 122
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mIsVertical:Z

    if-nez v2, :cond_2

    add-int v2, p2, v16

    sub-int v3, p3, v16

    if-le v2, v3, :cond_2

    .line 123
    sub-int v2, p3, p2

    div-int/lit8 v16, v2, 0x2

    .line 126
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 127
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    move/from16 v0, p8

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v20

    .line 128
    mul-float v2, v20, v14

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_9

    const/4 v13, 0x1

    .line 129
    :goto_0
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    move/from16 v0, p9

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v9

    .line 130
    mul-float v2, v9, v14

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_a

    const/4 v10, 0x1

    .line 133
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mIsVertical:Z

    if-nez v2, :cond_4

    .line 134
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    move/from16 v0, p10

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v15

    .line 135
    mul-float v2, v15, v14

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_b

    const/4 v11, 0x1

    .line 136
    :goto_2
    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    move/from16 v0, p11

    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v19

    .line 137
    mul-float v2, v19, v14

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_c

    const/4 v12, 0x1

    .line 140
    :cond_4
    :goto_3
    if-eqz v13, :cond_5

    .line 141
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v3, v14, v20

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 142
    move/from16 v0, p2

    int-to-float v2, v0

    move/from16 v0, p4

    int-to-float v3, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 143
    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 144
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p4

    int-to-float v4, v0

    move/from16 v0, p3

    int-to-float v5, v0

    add-int v2, p4, v16

    int-to-float v6, v2

    move-object/from16 v2, p1

    move-object/from16 v7, v18

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 147
    :cond_5
    if-eqz v10, :cond_6

    .line 148
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v3, v14, v9

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 149
    const/high16 v2, 0x43340000    # 180.0f

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 150
    move/from16 v0, p2

    int-to-float v2, v0

    move/from16 v0, p5

    int-to-float v3, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 151
    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 152
    move/from16 v0, p2

    int-to-float v3, v0

    sub-int v2, p5, v16

    int-to-float v4, v2

    move/from16 v0, p3

    int-to-float v5, v0

    move/from16 v0, p5

    int-to-float v6, v0

    move-object/from16 v2, p1

    move-object/from16 v7, v18

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 155
    :cond_6
    if-eqz v11, :cond_7

    .line 156
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v3, v14, v15

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 157
    const/high16 v2, -0x3d4c0000    # -90.0f

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 158
    move/from16 v0, p2

    int-to-float v2, v0

    move/from16 v0, p4

    int-to-float v3, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 159
    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 160
    move/from16 v0, p2

    int-to-float v3, v0

    move/from16 v0, p4

    int-to-float v4, v0

    add-int v2, p2, v16

    int-to-float v5, v2

    move/from16 v0, p5

    int-to-float v6, v0

    move-object/from16 v2, p1

    move-object/from16 v7, v18

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 163
    :cond_7
    if-eqz v12, :cond_8

    .line 164
    const/high16 v2, 0x3f800000    # 1.0f

    mul-float v3, v14, v19

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 165
    const/high16 v2, 0x42b40000    # 90.0f

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 166
    move/from16 v0, p3

    int-to-float v2, v0

    move/from16 v0, p4

    int-to-float v3, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 167
    move-object/from16 v0, v17

    invoke-virtual {v1, v0}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 168
    sub-int v2, p3, v16

    int-to-float v3, v2

    move/from16 v0, p4

    int-to-float v4, v0

    move/from16 v0, p3

    int-to-float v5, v0

    move/from16 v0, p5

    int-to-float v6, v0

    move-object/from16 v2, p1

    move-object/from16 v7, v18

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 171
    :cond_8
    return-void

    .line 128
    :cond_9
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 130
    :cond_a
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 135
    :cond_b
    const/4 v11, 0x0

    goto/16 :goto_2

    .line 137
    :cond_c
    const/4 v12, 0x0

    goto/16 :goto_3
.end method

.method public getHorizontalFadingEdgeLengthCallback()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mFadingEdgeLength:I

    return v0
.end method

.method public getVerticalFadingEdgeLengthCallback()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mFadingEdgeLength:I

    return v0
.end method

.method public onAttachedToWindowCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;Landroid/widget/LinearLayout;Z)V
    .locals 2
    .param p1, "callback"    # Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;
    .param p2, "layout"    # Landroid/widget/LinearLayout;
    .param p3, "hardwareAccelerated"    # Z

    .prologue
    const/4 v1, 0x0

    .line 68
    if-nez p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mSoftwareRendered:Z

    .line 69
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mSoftwareRendered:Z

    if-eqz v0, :cond_0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mScrollView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVerticalFadingEdgeEnabled(Z)V

    .line 72
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsScrollViewPerformanceHelper;->mScrollView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setHorizontalFadingEdgeEnabled(Z)V

    .line 74
    return-void

    :cond_1
    move v0, v1

    .line 68
    goto :goto_0
.end method
