.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4103
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v8, 0xca

    .line 4105
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 4128
    :cond_0
    :goto_0
    const/4 v5, 0x0

    return v5

    .line 4108
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 4109
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v5, p1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/View;)Landroid/view/View;

    .line 4110
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v6

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 4111
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v6, 0xc8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 4114
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 4115
    .local v3, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 4116
    .local v4, "y":F
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float v5, v3, v5

    float-to-int v0, v5

    .line 4117
    .local v0, "deltaX":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float v5, v4, v5

    float-to-int v1, v5

    .line 4118
    .local v1, "deltaY":I
    mul-int v5, v0, v0

    mul-int v6, v1, v1

    add-int v2, v5, v6

    .line 4119
    .local v2, "distance":I
    const/16 v5, 0x3e8

    if-le v2, v5, :cond_0

    .line 4120
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 4125
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    .end local v2    # "distance":I
    .end local v3    # "x":F
    .end local v4    # "y":F
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$62;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 4105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
