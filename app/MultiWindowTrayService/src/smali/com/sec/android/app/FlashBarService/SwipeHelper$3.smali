.class Lcom/sec/android/app/FlashBarService/SwipeHelper$3;
.super Ljava/lang/Object;
.source "SwipeHelper.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SwipeHelper;->dismissChild(Landroid/view/View;F)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SwipeHelper;

.field final synthetic val$animView:Landroid/view/View;

.field final synthetic val$canAnimViewBeDismissed:Z


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwipeHelper;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->this$0:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    iput-boolean p2, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->val$canAnimViewBeDismissed:Z

    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->val$animView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->val$canAnimViewBeDismissed:Z

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->val$animView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->this$0:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->val$animView:Landroid/view/View;

    # invokes: Lcom/sec/android/app/FlashBarService/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->access$400(Lcom/sec/android/app/FlashBarService/SwipeHelper;Landroid/view/View;)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;->val$animView:Landroid/view/View;

    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;)V

    .line 301
    return-void
.end method
