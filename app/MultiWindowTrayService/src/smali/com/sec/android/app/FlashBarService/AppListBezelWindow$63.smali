.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 4132
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 4134
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 4148
    :cond_0
    :goto_0
    return v1

    .line 4138
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 4139
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4140
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)V

    goto :goto_0

    .line 4145
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$63;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_0

    .line 4134
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
