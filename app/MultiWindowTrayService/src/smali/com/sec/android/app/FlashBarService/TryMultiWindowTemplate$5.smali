.class Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;
.super Ljava/lang/Object;
.source "TryMultiWindowTemplate.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 371
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 372
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 373
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 375
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    iget v3, v0, Landroid/graphics/Point;->x:I

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;I)I

    .line 376
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    iget v3, v0, Landroid/graphics/Point;->y:I

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;I)I

    .line 377
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)I

    move-result v4

    sub-int/2addr v3, v4

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1202(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;I)I

    .line 378
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 380
    .local v1, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)I

    move-result v2

    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 381
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v2

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 382
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 384
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTray:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 385
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandle:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 386
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandleClosed:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 387
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 388
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup1:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 389
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 390
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 391
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 392
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 393
    return-void
.end method
