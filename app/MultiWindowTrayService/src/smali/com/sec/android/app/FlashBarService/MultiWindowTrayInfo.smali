.class public Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
.super Ljava/lang/Object;
.source "MultiWindowTrayInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;,
        Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;,
        Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;
    }
.end annotation


# instance fields
.field private final DEBUG:Z

.field private final ESTIMATE_INVALID_VALUE:S

.field private final FREQUENTLY_USED_APP_COUNT:I

.field private final MAXIMUM_TEMPLATE_NUM:I

.field private final MAX_TASKS:I

.field private final PREF_KEY_FILE_SAVE:Ljava/lang/String;

.field private final PREF_KEY_FILE_SAVE_POPUP:Ljava/lang/String;

.field private final PREF_KEY_NUM_APP:Ljava/lang/String;

.field private final PREF_KEY_NUM_APP_POPUP:Ljava/lang/String;

.field private final PREF_KEY_NUM_TEMPLATE:Ljava/lang/String;

.field private final SEPARATOR:Ljava/lang/String;

.field private mActivatedLayoutList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

.field private mContext:Landroid/content/Context;

.field private mFlashBarAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFlashBarDelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mFlashBarListFileName:Ljava/lang/String;

.field private mFrequentlyViewPagerAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mIsTabletConcept:Z

.field private mIsUseDefaultTheme:Z

.field private mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

.field private mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

.field private mPenWindowOnlyList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPixelOfShadow:I

.field private mPopupAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupDelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPopupListFileName:Ljava/lang/String;

.field private mPopupViewPagerAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mRemovedFromDelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mRemovedFromPopupDelList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mRemovingTask:I

.field private mRunningAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private mShadowAlpha:I

.field private mShadowOfIconY:I

.field private mSupportExampleTemplate:Z

.field private mSupportLegacyTheme:Z

.field private mSupportMultiInstance:Z

.field private mTemplateList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private mThemeChanged:Z

.field private mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

.field private mViewPagerAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation
.end field

.field private prefixTemplate:Ljava/lang/String;

.field private final prefixTemplate_:Ljava/lang/String;

.field private themeBackground:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->DEBUG:Z

    .line 67
    iput-short v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->ESTIMATE_INVALID_VALUE:S

    .line 68
    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 70
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    .line 71
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    .line 72
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    .line 73
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    .line 74
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    .line 75
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    .line 76
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    .line 77
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromDelList:Ljava/util/List;

    .line 78
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromPopupDelList:Ljava/util/List;

    .line 79
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRunningAppList:Ljava/util/List;

    .line 80
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPenWindowOnlyList:Ljava/util/List;

    .line 86
    const-string v3, "flash_bar_list"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarListFileName:Ljava/lang/String;

    .line 87
    const-string v3, "popup_list"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupListFileName:Ljava/lang/String;

    .line 88
    const-string v3, "KEY_FILE_SAVE"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->PREF_KEY_FILE_SAVE:Ljava/lang/String;

    .line 89
    const-string v3, "KEY_NUM_APP"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->PREF_KEY_NUM_APP:Ljava/lang/String;

    .line 90
    const-string v3, "KEY_FILE_SAVE_POPUP"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->PREF_KEY_FILE_SAVE_POPUP:Ljava/lang/String;

    .line 91
    const-string v3, "KEY_NUM_APP_POPUP"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->PREF_KEY_NUM_APP_POPUP:Ljava/lang/String;

    .line 92
    const-string v3, "KEY_NUM_TEMPLATE"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->PREF_KEY_NUM_TEMPLATE:Ljava/lang/String;

    .line 93
    const-string v3, "Paired window "

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    .line 94
    const-string v3, "Pairedwindow_no"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate_:Ljava/lang/String;

    .line 95
    const-string v3, "#/#"

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->SEPARATOR:Ljava/lang/String;

    .line 101
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportMultiInstance:Z

    .line 102
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportExampleTemplate:Z

    .line 105
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    .line 107
    const/16 v3, 0x64

    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->MAX_TASKS:I

    .line 108
    const/16 v3, 0xa

    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->MAXIMUM_TEMPLATE_NUM:I

    .line 109
    const/16 v3, 0x8

    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->FREQUENTLY_USED_APP_COUNT:I

    .line 110
    iput v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    .line 112
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsTabletConcept:Z

    .line 116
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportLegacyTheme:Z

    .line 118
    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    .line 119
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    .line 120
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeChanged:Z

    .line 127
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    .line 139
    const-string v3, "MultiWindowTrayInfo"

    const-string v4, "FlashBarInfo"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    .line 141
    invoke-static {}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->getInstance()Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateSupportedAppList()V

    .line 144
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportMultiInstance(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportMultiInstance:Z

    .line 150
    new-instance v3, Lcom/samsung/android/theme/SThemeManager;

    invoke-direct {v3, p1}, Lcom/samsung/android/theme/SThemeManager;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    .line 151
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v3, v5}, Lcom/samsung/android/theme/SThemeManager;->getVersionFromFeature(I)Ljava/lang/String;

    move-result-object v1

    .line 152
    .local v1, "themeVersion":Ljava/lang/String;
    const-string v3, "0"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 153
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    .line 154
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportLegacyTheme:Z

    .line 166
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f070000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 167
    .local v2, "trayType":I
    if-eq v2, v6, :cond_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_4

    .line 169
    :cond_0
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsTabletConcept:Z

    .line 173
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0148

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mShadowOfIconY:I

    .line 174
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mShadowAlpha:I

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f07000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPixelOfShadow:I

    .line 176
    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    .line 177
    new-instance v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    .line 178
    return-void

    .line 156
    .end local v2    # "trayType":I
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "current_sec_theme_package"

    invoke-static {v3, v4}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "themePackageName":Ljava/lang/String;
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportLegacyTheme:Z

    .line 159
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 160
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    goto :goto_0

    .line 162
    :cond_3
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    goto :goto_0

    .line 171
    .end local v0    # "themePackageName":Ljava/lang/String;
    .restart local v2    # "trayType":I
    :cond_4
    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsTabletConcept:Z

    goto :goto_1
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .param p1, "x1"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method private addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z
    .locals 5
    .param p1, "launchItem"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .prologue
    const/4 v2, 0x0

    .line 869
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 870
    .local v1, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 871
    const-string v3, "MultiWindowTrayInfo"

    const-string v4, "addwithcheckduplicate  duplicated item return false!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 876
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_0
    return v2

    .line 875
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 876
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private addwithcheckduplicateForPenWindow(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z
    .locals 5
    .param p1, "launchItem"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .prologue
    const/4 v2, 0x0

    .line 880
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 881
    .local v1, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 882
    const-string v3, "MultiWindowTrayInfo"

    const-string v4, "addwithcheckduplicateForPenWindow duplicated item return false!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 887
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_0
    return v2

    .line 886
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v3, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 887
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private addwithcheckduplicateForPenWindowOnly(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z
    .locals 5
    .param p1, "launchItem"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .prologue
    const/4 v2, 0x0

    .line 891
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPenWindowOnlyList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 892
    .local v1, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 893
    const-string v3, "MultiWindowTrayInfo"

    const-string v4, "addwithcheckduplicateForPenWindowOnly duplicated item return false!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_0
    return v2

    .line 897
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPenWindowOnlyList:Ljava/util/List;

    invoke-interface {v3, v2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 898
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private addwithcheckduplicateForTemplate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z
    .locals 4
    .param p1, "launchItem"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .prologue
    .line 859
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 860
    .local v1, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 861
    const-string v2, "MultiWindowTrayInfo"

    const-string v3, "addwithcheckduplicateForTemplate  duplicated item return false!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 862
    const/4 v2, 0x0

    .line 865
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_0
    return v2

    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    move-result v2

    goto :goto_0
.end method

.method private findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 7
    .param p1, "className"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 1942
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 1943
    .local v0, "appListSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 1944
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1945
    .local v4, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v2

    .line 1946
    .local v2, "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v6, :cond_1

    .line 1943
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1950
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1951
    .local v1, "appResolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1969
    .end local v1    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v4    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_1
    return-object v1

    .line 1956
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 1957
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v0, :cond_5

    .line 1958
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1959
    .restart local v4    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v2

    .line 1960
    .restart local v2    # "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v6, :cond_4

    .line 1957
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1964
    :cond_4
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1965
    .restart local v1    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_1

    .line 1969
    .end local v1    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v4    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Landroid/graphics/drawable/BitmapDrawable;"
        }
    .end annotation

    .prologue
    .line 2135
    .local p1, "drs":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/drawable/Drawable;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    .line 2136
    .local v7, "size":I
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2137
    .local v4, "info":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v7, :cond_2

    .line 2138
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 2139
    sget-boolean v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v8, :cond_1

    .line 2140
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    .line 2141
    .local v5, "orgIcon":Landroid/graphics/Bitmap;
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {p0, v5, v8, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->createShadowIcon(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 2142
    .local v6, "shadowBitmap":Landroid/graphics/Bitmap;
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2137
    .end local v5    # "orgIcon":Landroid/graphics/Bitmap;
    .end local v6    # "shadowBitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2144
    :cond_1
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v8}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2149
    :cond_2
    const/4 v8, 0x0

    invoke-interface {v4, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    invoke-static {v8, v4}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->createFolderIconWithPlate(Landroid/graphics/Bitmap;Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2150
    .local v1, "buttonBitmap":Landroid/graphics/Bitmap;
    sget-boolean v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v8, :cond_3

    .line 2151
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 2152
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    .line 2156
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_3
    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-direct {v8, v9, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v8
.end method

.method private getClassName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "taskInfo"    # Landroid/app/ActivityManager$RunningTaskInfo;

    .prologue
    .line 2102
    iget-object v0, p1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "iconId"    # I
    .param p3, "dpi"    # I

    .prologue
    .line 934
    :try_start_0
    invoke-virtual {p1, p2, p3}, Landroid/content/res/Resources;->getDrawableForDensity(II)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 938
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v0

    .line 935
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :catch_0
    move-exception v1

    .line 936
    .local v1, "e":Landroid/content/res/Resources$NotFoundException;
    const/4 v0, 0x0

    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    goto :goto_0
.end method

.method private getPackageName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "taskInfo"    # Landroid/app/ActivityManager$RunningTaskInfo;

    .prologue
    .line 2098
    iget-object v0, p1, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 1023
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getLauncherLargeIconDensity()I

    move-result v2

    .line 1024
    .local v2, "dpi":I
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v7, :cond_3

    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1027
    .local v0, "cinfo":Landroid/content/pm/ComponentInfo;
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 1029
    .local v5, "pm":Landroid/content/pm/PackageManager;
    const/4 v6, 0x0

    .line 1031
    .local v6, "resources":Landroid/content/res/Resources;
    :try_start_0
    iget-object v7, v0, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v5, v7}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 1036
    :goto_1
    const/4 v1, 0x0

    .line 1037
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v6, :cond_1

    .line 1038
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v7, :cond_1

    .line 1040
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportLegacyTheme:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    if-nez v7, :cond_0

    .line 1041
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolveTheme(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1049
    :cond_0
    if-nez v1, :cond_1

    .line 1050
    iget-object v7, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v7}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v4

    .line 1051
    .local v4, "iconId":I
    if-eqz v4, :cond_1

    .line 1052
    invoke-direct {p0, v6, v4, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1057
    .end local v4    # "iconId":I
    :cond_1
    if-nez v1, :cond_2

    .line 1058
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x1080093

    invoke-direct {p0, v7, v8, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1061
    :cond_2
    return-object v1

    .line 1024
    .end local v0    # "cinfo":Landroid/content/pm/ComponentInfo;
    .end local v1    # "d":Landroid/graphics/drawable/Drawable;
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "resources":Landroid/content/res/Resources;
    :cond_3
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto :goto_0

    .line 1032
    .restart local v0    # "cinfo":Landroid/content/pm/ComponentInfo;
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    .restart local v6    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v3

    .line 1033
    .local v3, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v6, 0x0

    goto :goto_1
.end method

.method private loadIconForResolveTheme(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 22
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 943
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getLauncherLargeIconDensity()I

    move-result v8

    .line 944
    .local v8, "dpi":I
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    if-eqz v18, :cond_6

    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 947
    .local v6, "cinfo":Landroid/content/pm/ComponentInfo;
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v15

    .line 949
    .local v15, "pm":Landroid/content/pm/PackageManager;
    const/16 v16, 0x0

    .line 951
    .local v16, "resources":Landroid/content/res/Resources;
    :try_start_0
    iget-object v0, v6, Landroid/content/pm/ComponentInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v16

    .line 956
    :goto_1
    const/4 v7, 0x0

    .line 959
    .local v7, "d":Landroid/graphics/drawable/Drawable;
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/theme/SThemeManager;->getPackageIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 964
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeChanged:Z

    move/from16 v18, v0

    if-eqz v18, :cond_1

    .line 965
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_0

    .line 966
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->recycle()V

    .line 967
    const/16 v18, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    .line 969
    :cond_0
    const/16 v18, 0x0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeChanged:Z

    .line 972
    :cond_1
    const/4 v11, 0x0

    .line 973
    .local v11, "icon":Landroid/graphics/drawable/Drawable;
    if-nez v7, :cond_5

    .line 974
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/content/pm/ActivityInfo;->icon:I

    move/from16 v18, v0

    if-eqz v18, :cond_7

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    :goto_3
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/pm/PackageManager;->getCSCPackageItemIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 977
    if-nez v11, :cond_2

    .line 978
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/pm/ActivityInfo;->getIconResource()I

    move-result v12

    .line 979
    .local v12, "iconId":I
    if-eqz v12, :cond_2

    .line 980
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1, v12, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFullResIcon(Landroid/content/res/Resources;II)Landroid/graphics/drawable/Drawable;

    move-result-object v11

    .line 984
    .end local v12    # "iconId":I
    :cond_2
    if-eqz v11, :cond_5

    .line 985
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v18

    if-eqz v18, :cond_4

    .line 987
    :cond_3
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    move-object/from16 v18, v0

    const-string v19, "theme_app_3rd_party_icon"

    invoke-virtual/range {v18 .. v19}, Lcom/samsung/android/theme/SThemeManager;->getItemDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 988
    .local v4, "bg":Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    .end local v4    # "bg":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;
    :try_end_2
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 994
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_5

    .line 995
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    .line 996
    .local v17, "width":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    .line 998
    .local v10, "height":I
    new-instance v14, Landroid/graphics/Paint;

    invoke-direct {v14}, Landroid/graphics/Paint;-><init>()V

    .line 999
    .local v14, "p":Landroid/graphics/Paint;
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1000
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 1001
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v14, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 1003
    sget-object v18, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-static {v0, v10, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .local v3, "b":Landroid/graphics/Bitmap;
    move-object/from16 v18, v11

    .line 1004
    check-cast v18, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v13

    .line 1005
    .local v13, "in_bit":Landroid/graphics/Bitmap;
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1006
    .local v5, "canvas":Landroid/graphics/Canvas;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->themeBackground:Landroid/graphics/Bitmap;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v5, v0, v1, v2, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1007
    invoke-virtual {v5}, Landroid/graphics/Canvas;->save()I

    .line 1008
    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v18, v0

    const/high16 v19, 0x40000000    # 2.0f

    div-float v18, v18, v19

    int-to-float v0, v10

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1009
    const/high16 v18, 0x3f400000    # 0.75f

    const/high16 v19, 0x3f400000    # 0.75f

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v5, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    .line 1010
    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v13, v0, v10, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v18

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    const/high16 v20, 0x40000000    # 2.0f

    div-float v19, v19, v20

    neg-int v0, v10

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x40000000    # 2.0f

    div-float v20, v20, v21

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v5, v0, v1, v2, v14}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1011
    invoke-virtual {v5}, Landroid/graphics/Canvas;->restore()V

    .line 1013
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    .end local v7    # "d":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v7, v0, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 1018
    .end local v3    # "b":Landroid/graphics/Bitmap;
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "height":I
    .end local v13    # "in_bit":Landroid/graphics/Bitmap;
    .end local v14    # "p":Landroid/graphics/Paint;
    .end local v17    # "width":I
    .restart local v7    # "d":Landroid/graphics/drawable/Drawable;
    :cond_5
    return-object v7

    .line 944
    .end local v6    # "cinfo":Landroid/content/pm/ComponentInfo;
    .end local v7    # "d":Landroid/graphics/drawable/Drawable;
    .end local v11    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v15    # "pm":Landroid/content/pm/PackageManager;
    .end local v16    # "resources":Landroid/content/res/Resources;
    :cond_6
    move-object/from16 v0, p1

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    goto/16 :goto_0

    .line 952
    .restart local v6    # "cinfo":Landroid/content/pm/ComponentInfo;
    .restart local v15    # "pm":Landroid/content/pm/PackageManager;
    .restart local v16    # "resources":Landroid/content/res/Resources;
    :catch_0
    move-exception v9

    .line 953
    .local v9, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 960
    .end local v9    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v7    # "d":Landroid/graphics/drawable/Drawable;
    :catch_1
    move-exception v9

    .line 961
    .local v9, "e":Landroid/content/res/Resources$NotFoundException;
    const-string v18, "MultiWindowTrayInfo"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "loadAppIconBitmap( "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ) failed! "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 974
    .end local v9    # "e":Landroid/content/res/Resources$NotFoundException;
    .restart local v11    # "icon":Landroid/graphics/drawable/Drawable;
    :cond_7
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    goto/16 :goto_3

    .line 989
    :catch_2
    move-exception v9

    .line 990
    .restart local v9    # "e":Landroid/content/res/Resources$NotFoundException;
    const-string v18, "MultiWindowTrayInfo"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "3rd_party_icon_menu  failed! "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4
.end method

.method private makeExampleTemplate(Lcom/sec/android/app/FlashBarService/MultiUserPreferences;)I
    .locals 1
    .param p1, "prefs"    # Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    .prologue
    .line 2094
    const/4 v0, 0x0

    return v0
.end method

.method private parseActivityInfoMetaData(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "activityMetaData"    # Landroid/os/Bundle;
    .param p2, "metaData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 338
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 339
    .local v0, "empty":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 340
    .local v1, "style":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 341
    new-instance v0, Ljava/util/ArrayList;

    .end local v0    # "empty":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "\\|"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 343
    :cond_0
    return-object v0
.end method

.method private queryResolveInfo(Ljava/lang/String;)Ljava/util/List;
    .locals 9
    .param p1, "pkgName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1897
    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1898
    .local v2, "launcherIntent":Landroid/content/Intent;
    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1899
    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1900
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    const/16 v7, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v8

    invoke-virtual {v6, v2, v7, v8}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v3

    .line 1901
    .local v3, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1903
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 1904
    .local v4, "r":Landroid/content/pm/ResolveInfo;
    const/4 v0, 0x0

    .line 1905
    .local v0, "addInfo":Landroid/content/pm/ResolveInfo;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1907
    .end local v0    # "addInfo":Landroid/content/pm/ResolveInfo;
    .end local v4    # "r":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-object v5
.end method


# virtual methods
.method public addInstalledPackage(Ljava/lang/String;Z)Z
    .locals 16
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "forSmart"    # Z

    .prologue
    .line 1655
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-nez v13, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    if-nez v13, :cond_0

    .line 1656
    const/4 v13, 0x0

    .line 1773
    :goto_0
    return v13

    .line 1658
    :cond_0
    const/4 v11, 0x0

    .line 1659
    .local v11, "resolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateSupportedAppList()V

    .line 1661
    if-eqz p2, :cond_6

    .line 1662
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1663
    .local v4, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 1664
    iget-object v13, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 1667
    const/4 v13, 0x1

    goto :goto_0

    .line 1670
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1671
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 1672
    iget-object v13, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 1675
    const/4 v13, 0x1

    goto :goto_0

    .line 1678
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromPopupDelList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1679
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 1680
    iget-object v13, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 1681
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v13, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1682
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromPopupDelList:Ljava/util/List;

    invoke-interface {v13, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1683
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 1687
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .restart local v3    # "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1688
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 1689
    iget-object v13, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 1691
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 1694
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_8
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1695
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 1696
    iget-object v13, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 1698
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 1702
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_a
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromDelList:Ljava/util/List;

    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1703
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v11

    .line 1704
    iget-object v13, v11, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 1705
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v13, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1706
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromDelList:Ljava/util/List;

    invoke-interface {v13, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1707
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 1708
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 1713
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_c
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    .line 1715
    .local v8, "pm":Landroid/content/pm/PackageManager;
    new-instance v5, Landroid/content/Intent;

    const-string v13, "android.intent.action.MAIN"

    invoke-direct {v5, v13}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1716
    .local v5, "launcherIntent":Landroid/content/Intent;
    const-string v13, "android.intent.category.LAUNCHER"

    invoke-virtual {v5, v13}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1717
    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1718
    const/16 v13, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v14

    invoke-virtual {v8, v5, v13, v14}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v6

    .line 1720
    .local v6, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_d
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_1b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/pm/ResolveInfo;

    .line 1721
    .local v10, "r":Landroid/content/pm/ResolveInfo;
    const/4 v1, 0x0

    .line 1722
    .local v1, "addInfo":Landroid/content/pm/ResolveInfo;
    const/4 v12, 0x0

    .line 1725
    .local v12, "smartAddInfo":Landroid/content/pm/ResolveInfo;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v14, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 1726
    move-object v12, v10

    .line 1728
    :cond_e
    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v13, :cond_16

    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v13, v13, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v13, :cond_16

    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v13, v13, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v13, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1729
    .local v7, "metaDataBundle":Landroid/os/Bundle;
    :goto_1
    if-eqz v7, :cond_17

    const-string v13, "com.samsung.android.sdk.multiwindow.enable"

    invoke-virtual {v7, v13}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v13

    if-eqz v13, :cond_17

    .line 1730
    move-object v1, v10

    .line 1746
    .end local v7    # "metaDataBundle":Landroid/os/Bundle;
    :cond_f
    :goto_2
    if-eqz p2, :cond_10

    if-eqz v12, :cond_10

    .line 1747
    const-string v13, "MultiWindowTrayInfo"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "generateFlashBarList() : loading penwidnow app (activityInfo="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1748
    new-instance v9, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v12}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 1749
    .local v9, "popupAppitem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForPenWindow(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 1750
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 1753
    .end local v9    # "popupAppitem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_10
    if-nez p2, :cond_11

    if-eqz v1, :cond_11

    .line 1754
    const-string v13, "MultiWindowTrayInfo"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "generateFlashBarList() : loading mw app (activityInfo="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1755
    new-instance v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 1756
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 1757
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 1761
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_11
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsTabletConcept:Z

    if-nez v13, :cond_12

    sget-boolean v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v13, :cond_13

    :cond_12
    if-eqz v12, :cond_13

    if-nez v1, :cond_13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-static {v13}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v13

    if-eqz v13, :cond_13

    .line 1762
    new-instance v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v12}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 1763
    .restart local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 1764
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForPenWindowOnly(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 1767
    .end local v4    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_13
    if-eqz p2, :cond_14

    if-nez v12, :cond_15

    :cond_14
    if-nez p2, :cond_d

    if-eqz v1, :cond_d

    .line 1768
    :cond_15
    const/4 v13, 0x1

    goto/16 :goto_0

    .line 1728
    :cond_16
    :try_start_1
    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v13, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    goto/16 :goto_1

    .line 1731
    .restart local v7    # "metaDataBundle":Landroid/os/Bundle;
    :cond_17
    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    if-eqz v13, :cond_19

    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    const-string v14, "android.intent.category.MULTIWINDOW_LAUNCHER"

    invoke-virtual {v13, v14}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_18

    iget-object v13, v10, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    const-string v14, "com.sec.android.intent.category.MULTIWINDOW_LAUNCHER"

    invoke-virtual {v13, v14}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_19

    .line 1732
    :cond_18
    move-object v1, v10

    goto/16 :goto_2

    .line 1733
    :cond_19
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v14, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportApp(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_f

    .line 1734
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v14, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportPackageList(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_1a

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v14, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v14, v14, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v13, v14}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportComponentList(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v13

    if-eqz v13, :cond_d

    .line 1738
    :cond_1a
    move-object v1, v10

    goto/16 :goto_2

    .line 1740
    .end local v7    # "metaDataBundle":Landroid/os/Bundle;
    :catch_0
    move-exception v2

    .line 1741
    .local v2, "e":Ljava/lang/Exception;
    const/4 v1, 0x0

    .line 1742
    const-string v13, "MultiWindowTrayInfo"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "generateFlashBarList() : exception occurs! while loading mw app (activityInfo="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget-object v15, v10, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ")"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1743
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    .line 1773
    .end local v1    # "addInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "e":Ljava/lang/Exception;
    .end local v10    # "r":Landroid/content/pm/ResolveInfo;
    .end local v12    # "smartAddInfo":Landroid/content/pm/ResolveInfo;
    :cond_1b
    const/4 v13, 0x0

    goto/16 :goto_0
.end method

.method public addTemplate(Ljava/util/List;Ljava/lang/String;Z)I
    .locals 23
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "titleChanged"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;",
            "Ljava/lang/String;",
            "Z)I"
        }
    .end annotation

    .prologue
    .line 1973
    .local p1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 1975
    .local v11, "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1976
    .local v16, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getClassName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v14

    .line 1977
    .local v14, "r":Landroid/content/pm/ResolveInfo;
    if-nez v14, :cond_0

    .line 1978
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPackageName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfoByPackage(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v14

    .line 1980
    :cond_0
    if-nez v14, :cond_1

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    move-object/from16 v18, v0

    if-eqz v18, :cond_1

    .line 1981
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v14

    .line 1982
    if-nez v14, :cond_1

    .line 1983
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->queryResolveInfo(Ljava/lang/String;)Ljava/util/List;

    move-result-object v10

    .line 1984
    .local v10, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v18

    if-lez v18, :cond_1

    .line 1985
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    .end local v14    # "r":Landroid/content/pm/ResolveInfo;
    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 1990
    .end local v10    # "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v14    # "r":Landroid/content/pm/ResolveInfo;
    :cond_1
    if-eqz v14, :cond_4

    .line 1991
    const-string v18, "MultiWindowTrayInfo"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "addTemplate r = "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1992
    const/4 v13, 0x0

    .line 1993
    .local v13, "pos":I
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/util/Pair;

    .line 1994
    .local v12, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v19

    iget-object v0, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Ljava/lang/Integer;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    move-result v18

    move/from16 v0, v19

    move/from16 v1, v18

    if-le v0, v1, :cond_2

    .line 1995
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 1998
    .end local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_3
    new-instance v18, Landroid/util/Pair;

    move-object/from16 v0, v16

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v0, v14, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v18

    invoke-interface {v11, v13, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 2000
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v13    # "pos":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f080020

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2004
    .end local v14    # "r":Landroid/content/pm/ResolveInfo;
    .end local v16    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_5
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-le v0, v1, :cond_e

    .line 2005
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 2006
    .local v17, "tmpList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    move-object/from16 v0, v17

    invoke-interface {v0, v11}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2007
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v18

    if-ge v5, v0, :cond_a

    .line 2008
    const/16 v18, 0x0

    invoke-interface/range {v17 .. v18}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/util/Pair;

    .line 2009
    .restart local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/util/Pair;

    .line 2010
    .local v9, "item":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v19, v0

    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 2011
    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    if-eqz v18, :cond_8

    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 2012
    .local v3, "applicationMetaData":Landroid/os/Bundle;
    :goto_3
    if-eqz v3, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v19, v0

    iget-object v0, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v18, v0

    check-cast v18, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupporMultiInstance(Landroid/content/pm/ActivityInfo;)Z

    move-result v18

    if-nez v18, :cond_6

    .line 2013
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f080020

    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    .line 2014
    const/16 v18, -0x1

    .line 2040
    .end local v3    # "applicationMetaData":Landroid/os/Bundle;
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v9    # "item":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    .end local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    .end local v17    # "tmpList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    :goto_4
    return v18

    .line 2011
    .restart local v5    # "i":I
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v9    # "item":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    .restart local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    .restart local v17    # "tmpList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    .line 2007
    .end local v9    # "item":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 2020
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_a
    new-instance v8, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    if-nez p3, :cond_b

    const/16 v18, 0x1

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, v18

    invoke-direct {v8, v0, v11, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Ljava/util/List;Ljava/lang/String;Z)V

    .line 2021
    .local v8, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForTemplate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    move-result v18

    if-nez v18, :cond_c

    .line 2022
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    const v20, 0x7f08002d

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    aput-object p2, v21, v22

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    const/16 v20, 0x1

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    .line 2023
    const/16 v18, -0x2

    goto :goto_4

    .line 2020
    .end local v8    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_b
    const/16 v18, 0x0

    goto :goto_5

    .line 2026
    .restart local v8    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_c
    const-string v18, "MultiWindowTrayInfo"

    const-string v19, "Add new template!!"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2027
    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v4

    .line 2028
    .local v4, "flashbarList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    const/4 v15, 0x0

    .line 2030
    .local v15, "resolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .restart local v6    # "i$":Ljava/util/Iterator;
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/util/Pair;

    .line 2031
    .restart local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v15, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v15    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    check-cast v15, Landroid/content/pm/ResolveInfo;

    .line 2032
    .restart local v15    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    const-string v18, "MultiWindowTrayInfo"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "Stored Package : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v15, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 2035
    .end local v12    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 2036
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    move-object/from16 v18, v0

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v19

    invoke-interface/range {v18 .. v19}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2037
    const/16 v18, 0x0

    goto/16 :goto_4

    .line 2039
    .end local v4    # "flashbarList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v5    # "i":I
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v8    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v15    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v17    # "tmpList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    :cond_e
    const-string v18, "MultiWindowTrayInfo"

    const-string v19, "template add fail. current running app is null or only have 1"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2040
    const/16 v18, -0x1

    goto/16 :goto_4
.end method

.method public canAddTemplate()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2086
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0xa

    if-lt v1, v2, :cond_0

    .line 2087
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    const v3, 0x7f080024

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2090
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public changeApplistItem(IIZ)V
    .locals 3
    .param p1, "app"    # I
    .param p2, "edit"    # I
    .param p3, "bAdd"    # Z

    .prologue
    const/4 v2, -0x1

    .line 1560
    if-le p1, v2, :cond_0

    if-le p2, v2, :cond_0

    .line 1562
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1564
    .local v1, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz p3, :cond_1

    .line 1565
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v2, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1573
    .end local v1    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    :goto_0
    return-void

    .line 1567
    .restart local v1    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1569
    .end local v1    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :catch_0
    move-exception v0

    .line 1570
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public changeSmartWindowItem(IIZ)V
    .locals 3
    .param p1, "app"    # I
    .param p2, "edit"    # I
    .param p3, "bAdd"    # Z

    .prologue
    const/4 v2, -0x1

    .line 1576
    if-le p1, v2, :cond_0

    if-le p2, v2, :cond_0

    .line 1578
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1580
    .local v1, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz p3, :cond_1

    .line 1581
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v2, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1585
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    invoke-interface {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;->notifyAppListSizeChanged()V

    .line 1590
    .end local v1    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    :goto_1
    return-void

    .line 1583
    .restart local v1    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1586
    .end local v1    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :catch_0
    move-exception v0

    .line 1587
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public createShadowIcon(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 10
    .param p1, "icon"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 2331
    if-nez p1, :cond_0

    .line 2332
    const/4 v0, 0x0

    .line 2361
    :goto_0
    return-object v0

    .line 2334
    :cond_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 2335
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget v2, v7, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 2336
    .local v2, "density":I
    iget v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPixelOfShadow:I

    div-int/lit16 v8, v2, 0xa0

    mul-int v6, v7, v8

    .line 2338
    .local v6, "shadowPixel":I
    add-int v7, p3, v6

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2339
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2340
    .local v1, "canvas":Landroid/graphics/Canvas;
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2341
    .local v5, "shadowPaint":Landroid/graphics/Paint;
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    .line 2342
    .local v3, "m":Landroid/graphics/Matrix;
    const v7, 0x3f733333    # 0.95f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v3, v7, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 2343
    int-to-float v7, p2

    int-to-float v8, p2

    const v9, 0x3f733333    # 0.95f

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    int-to-float v8, v6

    invoke-virtual {v3, v7, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 2344
    new-instance v7, Landroid/graphics/LightingColorFilter;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 2345
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 2346
    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2347
    iget v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mShadowAlpha:I

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2356
    invoke-virtual {v1, p1, v3, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 2357
    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v1, p1, v7, v8, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2358
    invoke-virtual {v1}, Landroid/graphics/Canvas;->release()V

    .line 2359
    const/4 v1, 0x0

    .line 2361
    goto :goto_0
.end method

.method public findResolveInfoByPackage(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 7
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 1911
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 1912
    .local v0, "appListSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 1913
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1914
    .local v4, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v2

    .line 1915
    .local v2, "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v6, :cond_1

    .line 1912
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1919
    :cond_1
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1920
    .local v1, "appResolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1938
    .end local v1    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v4    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_1
    return-object v1

    .line 1925
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 1926
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v0, :cond_5

    .line 1927
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1928
    .restart local v4    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v2

    .line 1929
    .restart local v2    # "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v6, :cond_4

    .line 1926
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1933
    :cond_4
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    .line 1934
    .restart local v1    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    goto :goto_1

    .line 1938
    .end local v1    # "appResolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v2    # "flashBarApps":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v4    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_5
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public generateFlashBarList()V
    .locals 39

    .prologue
    .line 347
    const-string v36, "MultiWindowTrayInfo"

    const-string v37, "generateFlashBarList"

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->clear()V

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->clear()V

    .line 351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->clear()V

    .line 352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->clear()V

    .line 353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->clear()V

    .line 354
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPenWindowOnlyList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->clear()V

    .line 356
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v30

    .line 357
    .local v30, "pm":Landroid/content/pm/PackageManager;
    const-string v36, "package"

    invoke-static/range {v36 .. v36}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v36

    invoke-static/range {v36 .. v36}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v24

    .line 359
    .local v24, "mIPm":Landroid/content/pm/IPackageManager;
    new-instance v20, Landroid/content/Intent;

    const-string v36, "android.intent.action.MAIN"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 360
    .local v20, "launcherIntent":Landroid/content/Intent;
    const-string v36, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 362
    const/16 v36, 0x40

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v37

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    move/from16 v2, v36

    move/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v21

    .line 364
    .local v21, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_a

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Landroid/content/pm/ResolveInfo;

    .line 365
    .local v34, "r":Landroid/content/pm/ResolveInfo;
    const/4 v5, 0x0

    .line 367
    .local v5, "addInfo":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    if-eqz v36, :cond_0

    .line 369
    :try_start_0
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v36, v0

    const/16 v37, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v38

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 370
    .local v8, "applicationInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v36, Landroid/content/ComponentName;

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v38, v0

    invoke-direct/range {v36 .. v38}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v37, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v38

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    move-result-object v4

    .line 371
    .local v4, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v8, :cond_4

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v36, v0

    if-eqz v36, :cond_4

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v25, v0

    .line 373
    .local v25, "metaDataBundle":Landroid/os/Bundle;
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v36

    if-eqz v36, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-virtual {v0, v4}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z

    move-result v36

    if-eqz v36, :cond_1

    .line 374
    new-instance v31, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 375
    .local v31, "popupAppitem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForPenWindow(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 378
    .end local v31    # "popupAppitem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1
    const/16 v35, 0x0

    .line 379
    .local v35, "style":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v36, v0

    if-eqz v36, :cond_2

    .line 380
    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v36, v0

    const-string v37, "com.sec.android.multiwindow.activity.STYLE"

    move-object/from16 v0, p0

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->parseActivityInfoMetaData(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v35

    .line 382
    :cond_2
    move-object/from16 v0, v34

    iput-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 383
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iput-object v8, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 384
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v36, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v37, v0

    invoke-virtual/range {v36 .. v37}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isHideAppList(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_5

    .line 385
    const-string v36, "MultiWindowTrayInfo"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "generateFlashBarList() : SKIP package="

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 401
    .end local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v25    # "metaDataBundle":Landroid/os/Bundle;
    .end local v35    # "style":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v13

    .line 402
    .local v13, "e":Ljava/lang/Exception;
    const/4 v5, 0x0

    .line 403
    const-string v36, "MultiWindowTrayInfo"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "generateFlashBarList() : exception occurs! while loading mw app (activityInfo="

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ")"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 404
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    .line 407
    .end local v13    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_2
    if-eqz v5, :cond_0

    .line 408
    const-string v36, "MultiWindowTrayInfo"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "generateFlashBarList() : loading mw app (activityInfo="

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ")"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    new-instance v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 410
    .local v18, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    goto/16 :goto_0

    .line 371
    .end local v18    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .restart local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .restart local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_4
    :try_start_1
    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v25, v0

    goto/16 :goto_1

    .line 387
    .restart local v25    # "metaDataBundle":Landroid/os/Bundle;
    .restart local v35    # "style":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    if-eqz v35, :cond_6

    const-string v36, "hideAppFromMultiWindowList"

    invoke-virtual/range {v35 .. v36}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_6

    .line 388
    const-string v36, "MultiWindowTrayInfo"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "generateFlashBarList() : SKIP WINDOW_STYLE_HIDE_APP_FROM_MULTIWINDOWLIST activity="

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 390
    :cond_6
    if-eqz v25, :cond_8

    const-string v36, "com.samsung.android.sdk.multiwindow.enable"

    move-object/from16 v0, v25

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v36

    if-nez v36, :cond_7

    const-string v36, "com.sec.android.support.multiwindow"

    move-object/from16 v0, v25

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_8

    .line 393
    :cond_7
    move-object/from16 v5, v34

    goto/16 :goto_2

    .line 394
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v36, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v37, v0

    invoke-virtual/range {v36 .. v37}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportApp(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_3

    .line 395
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v36, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v37, v0

    invoke-virtual/range {v36 .. v37}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportPackageList(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v36, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v37, v0

    invoke-virtual/range {v36 .. v37}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportComponentList(Ljava/lang/String;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v36

    if-eqz v36, :cond_0

    .line 399
    :cond_9
    move-object/from16 v5, v34

    goto/16 :goto_2

    .line 415
    .end local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v5    # "addInfo":Landroid/content/pm/ResolveInfo;
    .end local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v25    # "metaDataBundle":Landroid/os/Bundle;
    .end local v34    # "r":Landroid/content/pm/ResolveInfo;
    .end local v35    # "style":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_a
    new-instance v20, Landroid/content/Intent;

    .end local v20    # "launcherIntent":Landroid/content/Intent;
    const-string v36, "android.intent.action.MAIN"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 416
    .restart local v20    # "launcherIntent":Landroid/content/Intent;
    const-string v36, "com.sec.android.intent.category.MULTIWINDOW_LAUNCHER"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 417
    const/16 v36, 0x40

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v37

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    move/from16 v2, v36

    move/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v21

    .line 419
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_b
    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_d

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Landroid/content/pm/ResolveInfo;

    .line 420
    .restart local v34    # "r":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    if-eqz v36, :cond_b

    .line 421
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-boolean v0, v0, Landroid/content/pm/ActivityInfo;->exported:Z

    move/from16 v36, v0

    if-eqz v36, :cond_b

    .line 425
    :try_start_2
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v36, v0

    const/16 v37, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v38

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 426
    .restart local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v36, Landroid/content/ComponentName;

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v38, v0

    invoke-direct/range {v36 .. v38}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v37, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v38

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    move-result-object v4

    .line 427
    .restart local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v8, :cond_c

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v36, v0

    if-eqz v36, :cond_c

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v25, v0

    .line 428
    .restart local v25    # "metaDataBundle":Landroid/os/Bundle;
    :goto_4
    move-object/from16 v0, v34

    iput-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 429
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iput-object v8, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 430
    if-eqz v25, :cond_b

    const-string v36, "com.samsung.android.sdk.multiwindow.enable"

    move-object/from16 v0, v25

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_b

    .line 431
    new-instance v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 432
    .restart local v18    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_3

    .line 434
    .end local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v18    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v25    # "metaDataBundle":Landroid/os/Bundle;
    :catch_1
    move-exception v13

    .line 435
    .restart local v13    # "e":Ljava/lang/Exception;
    const-string v36, "MultiWindowTrayInfo"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "generateFlashBarList() : exception occurs! while loading mw app (activityInfo="

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ")"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    .line 427
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .restart local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_c
    :try_start_3
    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v25, v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_4

    .line 441
    .end local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v34    # "r":Landroid/content/pm/ResolveInfo;
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v36

    if-eqz v36, :cond_11

    .line 442
    new-instance v20, Landroid/content/Intent;

    .end local v20    # "launcherIntent":Landroid/content/Intent;
    const-string v36, "android.intent.action.MAIN"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 443
    .restart local v20    # "launcherIntent":Landroid/content/Intent;
    const-string v36, "com.samsung.android.intent.category.PENWINDOW_LAUNCHER"

    move-object/from16 v0, v20

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 444
    const/16 v36, 0x40

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v37

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    move/from16 v2, v36

    move/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivitiesAsUser(Landroid/content/Intent;II)Ljava/util/List;

    move-result-object v21

    .line 446
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_e
    :goto_5
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_11

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v34

    check-cast v34, Landroid/content/pm/ResolveInfo;

    .line 447
    .restart local v34    # "r":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    if-eqz v36, :cond_e

    .line 448
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-boolean v0, v0, Landroid/content/pm/ActivityInfo;->exported:Z

    move/from16 v36, v0

    if-eqz v36, :cond_e

    .line 452
    :try_start_4
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v36, v0

    const/16 v37, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v38

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getApplicationInfo(Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    .line 453
    .restart local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    new-instance v36, Landroid/content/ComponentName;

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v37, v0

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v38, v0

    invoke-direct/range {v36 .. v38}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v37, 0xc0

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v38

    move-object/from16 v0, v24

    move-object/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-interface {v0, v1, v2, v3}, Landroid/content/pm/IPackageManager;->getActivityInfo(Landroid/content/ComponentName;II)Landroid/content/pm/ActivityInfo;

    move-result-object v4

    .line 454
    .restart local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v8, :cond_10

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v36, v0

    if-eqz v36, :cond_10

    iget-object v0, v8, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v25, v0

    .line 455
    .restart local v25    # "metaDataBundle":Landroid/os/Bundle;
    :goto_6
    move-object/from16 v0, v34

    iput-object v4, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 456
    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iput-object v8, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 457
    if-eqz v25, :cond_e

    const-string v36, "com.samsung.android.sdk.multiwindow.enable"

    move-object/from16 v0, v25

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v36

    if-nez v36, :cond_f

    const-string v36, "com.samsung.android.sdk.multiwindow.penwindow.enable"

    move-object/from16 v0, v25

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v36

    if-eqz v36, :cond_e

    .line 458
    :cond_f
    new-instance v31, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v34

    invoke-direct {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    .line 459
    .restart local v31    # "popupAppitem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForPenWindow(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_5

    .line 461
    .end local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v25    # "metaDataBundle":Landroid/os/Bundle;
    .end local v31    # "popupAppitem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :catch_2
    move-exception v13

    .line 462
    .restart local v13    # "e":Ljava/lang/Exception;
    const-string v36, "MultiWindowTrayInfo"

    new-instance v37, Ljava/lang/StringBuilder;

    invoke-direct/range {v37 .. v37}, Ljava/lang/StringBuilder;-><init>()V

    const-string v38, "generateFlashBarList() : exception occurs! while loading mw app (activityInfo="

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    move-object/from16 v0, v34

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v38, v0

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v37

    const-string v38, ")"

    invoke-virtual/range {v37 .. v38}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v37

    invoke-virtual/range {v37 .. v37}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v36 .. v37}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_5

    .line 454
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .restart local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    :cond_10
    :try_start_5
    iget-object v0, v4, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    move-object/from16 v25, v0
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_6

    .line 470
    .end local v4    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v8    # "applicationInfo":Landroid/content/pm/ApplicationInfo;
    .end local v34    # "r":Landroid/content/pm/ResolveInfo;
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-virtual/range {v36 .. v36}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v36

    const v37, 0x1070067

    invoke-virtual/range {v36 .. v37}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v12

    .line 471
    .local v12, "defaultSpenAppList":[Ljava/lang/String;
    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    .line 472
    .local v29, "orderedPopupAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;>;"
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 473
    .local v26, "nonDefaultAppList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_7
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_12

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 474
    .local v6, "app":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, v26

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 476
    .end local v6    # "app":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_12
    move-object v9, v12

    .local v9, "arr$":[Ljava/lang/String;
    array-length v0, v9

    move/from16 v22, v0

    .local v22, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    move/from16 v16, v15

    .end local v15    # "i$":I
    .local v16, "i$":I
    :goto_8
    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_15

    aget-object v28, v9, v16

    .line 477
    .local v28, "orderedApp":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .end local v16    # "i$":I
    .local v15, "i$":Ljava/util/Iterator;
    :cond_13
    :goto_9
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_14

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 478
    .restart local v6    # "app":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v36

    if-eqz v36, :cond_13

    .line 479
    move-object/from16 v0, v29

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 480
    move-object/from16 v0, v26

    invoke-interface {v0, v6}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_9

    .line 476
    .end local v6    # "app":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_14
    add-int/lit8 v15, v16, 0x1

    .local v15, "i$":I
    move/from16 v16, v15

    .end local v15    # "i$":I
    .restart local v16    # "i$":I
    goto :goto_8

    .line 484
    .end local v28    # "orderedApp":Ljava/lang/String;
    :cond_15
    invoke-interface/range {v26 .. v26}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .end local v16    # "i$":I
    .local v15, "i$":Ljava/util/Iterator;
    :goto_a
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_16

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 485
    .local v27, "notDefaultApp":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, v29

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 488
    .end local v27    # "notDefaultApp":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_16
    new-instance v36, Ljava/util/ArrayList;

    move-object/from16 v0, v36

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, v36

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    .line 491
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsTabletConcept:Z

    move/from16 v36, v0

    if-nez v36, :cond_17

    sget-boolean v36, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v36, :cond_19

    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v36, v0

    invoke-static/range {v36 .. v36}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v36

    if-eqz v36, :cond_19

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_18
    :goto_b
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v36

    if-eqz v36, :cond_19

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 493
    .restart local v6    # "app":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    move-result v36

    if-eqz v36, :cond_18

    .line 494
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForPenWindowOnly(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    goto :goto_b

    .line 499
    .end local v6    # "app":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_19
    new-instance v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$1;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 506
    .local v10, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-static {v0, v10}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v23

    .line 509
    .local v23, "listSize":I
    const/16 v17, 0x0

    .line 510
    .local v17, "index":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_c
    move/from16 v0, v23

    if-ge v14, v0, :cond_1c

    .line 511
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 512
    .restart local v18    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v36, v0

    if-eqz v36, :cond_1a

    .line 513
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v36, v0

    const-string v37, "google"

    invoke-virtual/range {v36 .. v37}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v36

    if-eqz v36, :cond_1b

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    move/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 515
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    add-int/lit8 v37, v23, -0x1

    move-object/from16 v0, v36

    move/from16 v1, v37

    move-object/from16 v2, v18

    invoke-interface {v0, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 510
    :cond_1a
    :goto_d
    add-int/lit8 v14, v14, 0x1

    goto :goto_c

    .line 517
    :cond_1b
    add-int/lit8 v17, v17, 0x1

    goto :goto_d

    .line 522
    .end local v18    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1c
    new-instance v32, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarListFileName:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    const/16 v38, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    move/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 523
    .local v32, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v36, "KEY_FILE_SAVE"

    const/16 v37, 0x0

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v36

    if-nez v36, :cond_1e

    .line 524
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeExampleTemplate(Lcom/sec/android/app/FlashBarService/MultiUserPreferences;)I

    move-result v11

    .line 525
    .local v11, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v7

    .line 526
    .local v7, "appListSize":I
    const/4 v14, 0x0

    :goto_e
    if-ge v14, v7, :cond_1d

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 528
    .local v19, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v36, v0

    add-int v37, v14, v11

    add-int/lit8 v37, v37, 0x1

    move-object/from16 v0, v32

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 526
    add-int/lit8 v14, v14, 0x1

    goto :goto_e

    .line 530
    .end local v19    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1d
    invoke-virtual/range {v32 .. v32}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 532
    .end local v7    # "appListSize":I
    .end local v11    # "count":I
    :cond_1e
    move-object/from16 v0, p0

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadFlashBarList(Lcom/sec/android/app/FlashBarService/MultiUserPreferences;)V

    .line 534
    new-instance v33, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupListFileName:Ljava/lang/String;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    const/16 v38, 0x1

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move-object/from16 v2, v37

    move/from16 v3, v38

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 535
    .local v33, "prefsForPopup":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const-string v36, "KEY_FILE_SAVE_POPUP"

    const/16 v37, 0x0

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v36

    if-eqz v36, :cond_1f

    sget-boolean v36, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v36, :cond_21

    .line 536
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    move-object/from16 v36, v0

    invoke-interface/range {v36 .. v36}, Ljava/util/List;->size()I

    move-result v7

    .line 537
    .restart local v7    # "appListSize":I
    const/4 v14, 0x0

    :goto_f
    if-ge v14, v7, :cond_20

    .line 538
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 539
    .restart local v19    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v36

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v36, v0

    move-object/from16 v0, v36

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v36, v0

    add-int/lit8 v37, v14, 0x1

    move-object/from16 v0, v33

    move-object/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 537
    add-int/lit8 v14, v14, 0x1

    goto :goto_f

    .line 541
    .end local v19    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_20
    invoke-virtual/range {v33 .. v33}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 543
    .end local v7    # "appListSize":I
    :cond_21
    move-object/from16 v0, p0

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadPopupList(Lcom/sec/android/app/FlashBarService/MultiUserPreferences;)V

    .line 544
    return-void
.end method

.method public getAppCnt()I
    .locals 2

    .prologue
    .line 255
    const-string v0, "MultiWindowTrayInfo"

    const-string v1, "getAppCnt"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 258
    const/4 v0, 0x0

    .line 260
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getAppListForSmartWindow()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    return-object v0
.end method

.method public getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1392
    const/4 v0, 0x0

    .line 1393
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1395
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v1

    return-object v1
.end method

.method public getAppTitle(IZ)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I
    .param p2, "bEditMode"    # Z

    .prologue
    .line 1374
    const/4 v1, -0x1

    if-le p1, v1, :cond_2

    .line 1375
    const/4 v0, 0x0

    .line 1376
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz p2, :cond_0

    .line 1377
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1382
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_0
    if-eqz v0, :cond_1

    .line 1383
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1388
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_1
    return-object v1

    .line 1379
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    goto :goto_0

    .line 1385
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    goto :goto_1

    .line 1388
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    goto :goto_1
.end method

.method public getAvailableTemplateText()Ljava/lang/CharSequence;
    .locals 7

    .prologue
    .line 2258
    const/4 v2, 0x0

    .line 2259
    .local v2, "num":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    const v6, 0x7f080014

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    .line 2260
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    .line 2262
    .local v4, "title":Ljava/lang/CharSequence;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_0

    .line 2263
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    .line 2281
    :goto_0
    return-object v5

    .line 2266
    :cond_0
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$3;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 2271
    .local v0, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<Ljava/lang/CharSequence;>;"
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-static {v5, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2273
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v3

    .line 2274
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_2

    .line 2275
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2276
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2277
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->prefixTemplate:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2274
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v5, v4

    .line 2281
    goto :goto_0
.end method

.method public getComponentInfo(I)Landroid/content/pm/ComponentInfo;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1369
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 1370
    .local v0, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    return-object v1
.end method

.method public getDefaultTemplateText(Ljava/util/List;)Ljava/lang/CharSequence;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .local p1, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2205
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    .line 2206
    .local v5, "title":Ljava/lang/String;
    const/4 v8, 0x4

    new-array v6, v8, [Ljava/lang/String;

    const-string v8, ""

    aput-object v8, v6, v10

    const-string v8, ""

    aput-object v8, v6, v11

    const-string v8, ""

    aput-object v8, v6, v12

    const-string v8, ""

    aput-object v8, v6, v13

    .line 2207
    .local v6, "titleArray":[Ljava/lang/String;
    const/4 v0, 0x0

    .line 2208
    .local v0, "app":Ljava/lang/String;
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 2209
    .local v4, "size":I
    const/4 v7, 0x0

    .line 2210
    .local v7, "zone":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_8

    .line 2211
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getClassName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 2212
    .local v3, "r":Landroid/content/pm/ResolveInfo;
    if-nez v3, :cond_0

    .line 2213
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPackageName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfoByPackage(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 2215
    :cond_0
    if-nez v3, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    if-eqz v8, :cond_2

    .line 2216
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 2217
    if-nez v3, :cond_1

    .line 2218
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->queryResolveInfo(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 2219
    .local v2, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_1

    .line 2220
    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "r":Landroid/content/pm/ResolveInfo;
    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 2223
    .end local v2    # "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v3    # "r":Landroid/content/pm/ResolveInfo;
    :cond_1
    if-nez v3, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    if-eqz v8, :cond_2

    .line 2224
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v8}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->queryResolveInfo(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 2225
    .restart local v2    # "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_2

    .line 2226
    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "r":Landroid/content/pm/ResolveInfo;
    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 2231
    .end local v2    # "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v3    # "r":Landroid/content/pm/ResolveInfo;
    :cond_2
    if-eqz v3, :cond_7

    .line 2232
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v8

    invoke-virtual {v3, v8}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2233
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v8, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v7

    .line 2234
    and-int/lit8 v8, v7, 0x3

    if-eqz v8, :cond_5

    .line 2235
    and-int/lit8 v8, v7, 0x1

    if-eqz v8, :cond_4

    .line 2236
    aput-object v0, v6, v10

    .line 2210
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 2238
    :cond_4
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v11

    goto :goto_1

    .line 2240
    :cond_5
    and-int/lit8 v8, v7, 0xc

    if-eqz v8, :cond_3

    .line 2241
    and-int/lit8 v8, v7, 0x4

    if-eqz v8, :cond_6

    .line 2242
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v12

    goto :goto_1

    .line 2244
    :cond_6
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v13

    goto :goto_1

    .line 2248
    :cond_7
    const/4 v8, 0x0

    .line 2254
    .end local v3    # "r":Landroid/content/pm/ResolveInfo;
    :goto_2
    return-object v8

    .line 2251
    :cond_8
    const/4 v1, 0x0

    :goto_3
    array-length v8, v6

    if-ge v1, v8, :cond_9

    .line 2252
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v6, v1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2251
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    move-object v8, v5

    .line 2254
    goto :goto_2
.end method

.method public getFlashBarIntent(IZI)Ljava/util/List;
    .locals 11
    .param p1, "index"    # I
    .param p2, "multiInstance"    # Z
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZI)",
            "Ljava/util/List",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1411
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1412
    .local v2, "intents":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    if-gez p1, :cond_1

    .line 1413
    const-string v8, "MultiWindowTrayInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "getFlashBarIntent: invalid index "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1480
    :cond_0
    :goto_0
    return-object v2

    .line 1417
    :cond_1
    const/4 v4, 0x0

    .line 1418
    .local v4, "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    const/4 v6, 0x0

    .line 1419
    .local v6, "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v3, 0x0

    .line 1420
    .local v3, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const/16 v8, 0x65

    if-ne p3, v8, :cond_4

    .line 1421
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1431
    .restart local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_1
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v4

    .line 1433
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 1434
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v6

    .line 1435
    iget-object v8, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    if-eqz v8, :cond_0

    .line 1436
    const/4 v1, 0x0

    .line 1437
    .local v1, "intent":Landroid/content/Intent;
    iget-object v8, v6, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    if-eqz v8, :cond_2

    iget-object v8, v6, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    const-string v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1438
    :cond_2
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1439
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1443
    :goto_2
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v10, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1444
    const-string v8, "com.google.android.youtube"

    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1445
    const/high16 v8, 0x10000000

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1449
    :goto_3
    if-eqz p2, :cond_3

    .line 1450
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeMultiInstanceIntent(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;)V

    .line 1452
    :cond_3
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1422
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_4
    const/16 v8, 0x66

    if-ne p3, v8, :cond_5

    .line 1423
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .restart local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    goto :goto_1

    .line 1424
    :cond_5
    const/16 v8, 0x67

    if-ne p3, v8, :cond_6

    .line 1425
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .restart local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    goto :goto_1

    .line 1426
    :cond_6
    const/16 v8, 0x68

    if-ne p3, v8, :cond_7

    .line 1427
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .restart local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    goto :goto_1

    .line 1429
    :cond_7
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .restart local v3    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    goto/16 :goto_1

    .line 1441
    .restart local v1    # "intent":Landroid/content/Intent;
    :cond_8
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_2

    .line 1447
    :cond_9
    const/high16 v8, 0x10200000

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_3

    .line 1455
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/util/Pair;

    .line 1456
    .local v5, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v6, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v6    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    check-cast v6, Landroid/content/pm/ResolveInfo;

    .line 1457
    .restart local v6    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v1, 0x0

    .line 1458
    .restart local v1    # "intent":Landroid/content/Intent;
    iget-object v8, v6, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    if-eqz v8, :cond_b

    iget-object v8, v6, Landroid/content/pm/ResolveInfo;->filter:Landroid/content/IntentFilter;

    const-string v9, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v9}, Landroid/content/IntentFilter;->hasCategory(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    .line 1459
    :cond_b
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    const-string v8, "android.intent.action.MAIN"

    invoke-direct {v1, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1460
    .restart local v1    # "intent":Landroid/content/Intent;
    const-string v8, "android.intent.category.LAUNCHER"

    invoke-virtual {v1, v8}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 1464
    :goto_5
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v10, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v9, v10}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1465
    const-string v8, "com.google.android.youtube"

    iget-object v9, v6, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1466
    const/high16 v8, 0x10000000

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1470
    :goto_6
    if-eqz p2, :cond_c

    .line 1471
    invoke-virtual {p0, v6, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeMultiInstanceIntent(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;)V

    .line 1473
    :cond_c
    new-instance v7, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 1474
    .local v7, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    iget-object v8, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1475
    invoke-virtual {v1, v7}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1476
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1462
    .end local v7    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_d
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "intent":Landroid/content/Intent;
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .restart local v1    # "intent":Landroid/content/Intent;
    goto :goto_5

    .line 1468
    :cond_e
    const/high16 v8, 0x10200000

    invoke-virtual {v1, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_6
.end method

.method public getFlashBarState()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 754
    const-string v1, "MultiWindowTrayInfo"

    const-string v2, "getFlashBarState"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "multi_window_flashbar_shown"

    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getIconByIndex(I)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 2193
    const/4 v0, 0x0

    .line 2194
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2195
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1
.end method

.method public getIconByRunningTaskInfo(Landroid/app/ActivityManager$RunningTaskInfo;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p1, "taskInfo"    # Landroid/app/ActivityManager$RunningTaskInfo;

    .prologue
    .line 2171
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getClassName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 2172
    .local v2, "r":Landroid/content/pm/ResolveInfo;
    if-nez v2, :cond_0

    .line 2173
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPackageName(Landroid/app/ActivityManager$RunningTaskInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfoByPackage(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 2175
    :cond_0
    if-nez v2, :cond_1

    iget-object v3, p1, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    if-eqz v3, :cond_1

    .line 2176
    iget-object v3, p1, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 2177
    if-nez v2, :cond_1

    .line 2178
    iget-object v3, p1, Landroid/app/ActivityManager$RunningTaskInfo;->sourceActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->queryResolveInfo(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 2179
    .local v1, "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    .line 2180
    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "r":Landroid/content/pm/ResolveInfo;
    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 2185
    .end local v1    # "launcherlist":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    .restart local v2    # "r":Landroid/content/pm/ResolveInfo;
    :cond_1
    if-eqz v2, :cond_3

    .line 2186
    invoke-direct {p0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->loadIconForResolve(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2187
    .local v0, "dr":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_2

    .line 2189
    .end local v0    # "dr":Landroid/graphics/drawable/Drawable;
    :goto_0
    return-object v0

    .line 2187
    .restart local v0    # "dr":Landroid/graphics/drawable/Drawable;
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 2189
    .end local v0    # "dr":Landroid/graphics/drawable/Drawable;
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLauncherLargeIconDensity()I
    .locals 5

    .prologue
    const/16 v3, 0x140

    .line 902
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 903
    .local v1, "res":Landroid/content/res/Resources;
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v0, v4, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 904
    .local v0, "density":I
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v2, v4, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 906
    .local v2, "sw":I
    const/16 v4, 0x258

    if-ge v2, v4, :cond_0

    .line 927
    .end local v0    # "density":I
    :goto_0
    return v0

    .line 911
    .restart local v0    # "density":I
    :cond_0
    sparse-switch v0, :sswitch_data_0

    .line 927
    int-to-float v3, v0

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v0, v3

    goto :goto_0

    .line 913
    :sswitch_0
    const/16 v0, 0xa0

    goto :goto_0

    .line 915
    :sswitch_1
    const/16 v0, 0xf0

    goto :goto_0

    :sswitch_2
    move v0, v3

    .line 917
    goto :goto_0

    :sswitch_3
    move v0, v3

    .line 919
    goto :goto_0

    .line 921
    :sswitch_4
    const/16 v0, 0x1e0

    goto :goto_0

    .line 923
    :sswitch_5
    const/16 v0, 0x280

    goto :goto_0

    .line 911
    nop

    :sswitch_data_0
    .sparse-switch
        0x78 -> :sswitch_0
        0xa0 -> :sswitch_1
        0xd5 -> :sswitch_2
        0xf0 -> :sswitch_3
        0x140 -> :sswitch_4
        0x1e0 -> :sswitch_5
    .end sparse-switch
.end method

.method public getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .locals 1
    .param p1, "index"    # I
    .param p2, "bEditmode"    # Z

    .prologue
    .line 321
    if-eqz p2, :cond_0

    .line 322
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 324
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    goto :goto_0
.end method

.method public getPopupAppCnt()I
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 265
    const/4 v0, 0x0

    .line 267
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getRunningAppByIndex(I)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRunningAppList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRunningAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 223
    :cond_0
    const/4 v0, 0x0

    .line 225
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRunningAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    goto :goto_0
.end method

.method public getRunningAppCnt()I
    .locals 2

    .prologue
    .line 237
    const-string v0, "MultiWindowTrayInfo"

    const-string v1, "getRunningAppCnt"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRunningAppList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 240
    const/4 v0, 0x0

    .line 242
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRunningAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getRunningAppTaskIdByIndex(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 229
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getRunningAppByIndex(I)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    .line 230
    .local v0, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz v0, :cond_0

    .line 231
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTaskId()I

    move-result v1

    .line 233
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getSmartWindowItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .locals 1
    .param p1, "index"    # I
    .param p2, "bEditmode"    # Z

    .prologue
    .line 329
    if-eqz p2, :cond_0

    .line 330
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 332
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    goto :goto_0
.end method

.method public getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;
    .locals 5
    .param p1, "componentName"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 1234
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1235
    .local v2, "pm":Landroid/content/pm/PackageManager;
    if-nez p1, :cond_0

    .line 1242
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return-object v3

    .line 1238
    .restart local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v2, p1, v4}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v0

    .line 1239
    .local v0, "Info":Landroid/content/pm/ActivityInfo;
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->taskAffinity:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1241
    .end local v0    # "Info":Landroid/content/pm/ActivityInfo;
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 1242
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method public getUnableAppCnt()I
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 274
    const/4 v0, 0x0

    .line 276
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getUnableSmartAppCnt()I
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 283
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getViewPagerAppListCnt(I)I
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 2419
    const/16 v0, 0x67

    if-ne p1, v0, :cond_0

    .line 2420
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2421
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2432
    :goto_0
    return v0

    .line 2423
    :cond_0
    const/16 v0, 0x66

    if-ne p1, v0, :cond_1

    .line 2424
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2425
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 2428
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2429
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    .line 2432
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .locals 1
    .param p1, "index"    # I
    .param p2, "type"    # I

    .prologue
    .line 2436
    const/16 v0, 0x67

    if-ne p2, v0, :cond_0

    .line 2437
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2438
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2449
    :goto_0
    return-object v0

    .line 2440
    :cond_0
    const/16 v0, 0x66

    if-ne p2, v0, :cond_1

    .line 2441
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2442
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    goto :goto_0

    .line 2445
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 2446
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    goto :goto_0

    .line 2449
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLaunchingBlockedItem(Landroid/content/Intent;Ljava/util/List;)Z
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/util/List",
            "<",
            "Landroid/app/ActivityManager$RunningTaskInfo;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1185
    .local p2, "resumedTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v1, 0x0

    .line 1186
    .local v1, "cnt":I
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v4

    .line 1187
    .local v4, "selectedAffinity":Ljava/lang/String;
    if-eqz v4, :cond_4

    .line 1188
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1189
    .local v5, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v9, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_1

    iget v9, v5, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    iget v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    if-eq v9, v10, :cond_0

    .line 1192
    :cond_1
    iget-object v9, v5, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v0

    .line 1193
    .local v0, "baseRunningAffinity":Ljava/lang/String;
    iget-object v9, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v8

    .line 1194
    .local v8, "topRunningAffinity":Ljava/lang/String;
    iget-object v6, v5, Landroid/app/ActivityManager$RunningTaskInfo;->taskAffinity:Ljava/lang/String;

    .line 1195
    .local v6, "taskRecordAffinity":Ljava/lang/String;
    iget-object v9, v5, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1196
    if-eqz v0, :cond_2

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1197
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1198
    :cond_2
    if-eqz v8, :cond_3

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1199
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1200
    :cond_3
    if-eqz v6, :cond_0

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1201
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1206
    .end local v0    # "baseRunningAffinity":Ljava/lang/String;
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v5    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "taskRecordAffinity":Ljava/lang/String;
    .end local v8    # "topRunningAffinity":Ljava/lang/String;
    :cond_4
    if-lez v1, :cond_c

    .line 1207
    const/4 v9, 0x1

    if-ne v1, v9, :cond_b

    .line 1208
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1209
    .local v7, "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v9, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    const/4 v10, -0x1

    if-eq v9, v10, :cond_6

    iget v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    iget v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    if-eq v9, v10, :cond_5

    .line 1212
    :cond_6
    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v9, :cond_7

    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_8

    :cond_7
    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    if-eqz v9, :cond_9

    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v9}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1214
    :cond_8
    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getTaskAffinity(Landroid/content/ComponentName;)Ljava/lang/String;

    move-result-object v3

    .line 1215
    .local v3, "runningAffinity":Ljava/lang/String;
    if-eqz v4, :cond_5

    if-eqz v3, :cond_5

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1216
    const/4 v9, 0x1

    .line 1229
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "runningAffinity":Ljava/lang/String;
    .end local v7    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_1
    return v9

    .line 1218
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v7    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_9
    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->taskAffinity:Ljava/lang/String;

    if-eqz v9, :cond_5

    .line 1219
    if-eqz v4, :cond_5

    iget-object v9, v7, Landroid/app/ActivityManager$RunningTaskInfo;->taskAffinity:Ljava/lang/String;

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1220
    const/4 v9, 0x1

    goto :goto_1

    .line 1224
    .end local v7    # "ti":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_a
    const/4 v9, 0x0

    goto :goto_1

    .line 1226
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_b
    const/4 v9, 0x1

    goto :goto_1

    .line 1229
    :cond_c
    const/4 v9, 0x0

    goto :goto_1
.end method

.method public isPenWindowOnly(I)Z
    .locals 5
    .param p1, "index"    # I

    .prologue
    .line 2285
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2286
    .local v1, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPenWindowOnlyList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2287
    .local v2, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2288
    const/4 v3, 0x1

    .line 2291
    .end local v2    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public isTemplate(IZ)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "bEditmode"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2065
    const/4 v3, -0x1

    if-le p1, v3, :cond_3

    .line 2066
    const/4 v0, 0x0

    .line 2067
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz p2, :cond_1

    .line 2068
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2069
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v1, :cond_2

    .line 2081
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    :goto_0
    return v1

    .line 2073
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2074
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-gt v3, v1, :cond_0

    :cond_2
    move v1, v2

    .line 2078
    goto :goto_0

    .line 2080
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_3
    const-string v1, "MultiWindowTrayInfo"

    const-string v3, "isTemplate : index == ESTIMATE_INVALID_VALUE"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    .line 2081
    goto :goto_0
.end method

.method public loadFlashBarList(Lcom/sec/android/app/FlashBarService/MultiUserPreferences;)V
    .locals 33
    .param p1, "prefs"    # Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    .prologue
    .line 547
    const-string v29, "MultiWindowTrayInfo"

    const-string v30, "loadFlashBarList"

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 549
    const/4 v6, 0x0

    .line 550
    .local v6, "bUpdateFile":Z
    const-string v29, "KEY_NUM_APP"

    const/16 v30, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    .line 551
    .local v14, "numAppInFile":I
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 553
    .local v17, "orderedFlashBarItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v3

    .line 554
    .local v3, "appListSize":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v3, :cond_2

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 556
    .local v11, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v29

    move-object/from16 v0, v29

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 557
    .local v4, "appName":Ljava/lang/String;
    const/16 v29, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v4, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v15

    .line 558
    .local v15, "order":I
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "loadFlashBarList : app = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", order = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, ", size = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    if-lez v15, :cond_1

    .line 562
    if-le v15, v14, :cond_0

    .line 563
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Abnormal xml(Too Large Order) order = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    const/4 v6, 0x1

    .line 567
    :cond_0
    new-instance v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    move-object/from16 v0, v18

    invoke-direct {v0, v11, v15}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;I)V

    .line 568
    .local v18, "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 554
    .end local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_0

    .line 572
    .end local v4    # "appName":Ljava/lang/String;
    .end local v11    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v15    # "order":I
    :cond_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v29

    move/from16 v0, v29

    if-eq v14, v0, :cond_3

    .line 573
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Abnormal xml(Empty Order) or the list of multiwindow apps is changed. size = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    const/4 v6, 0x1

    .line 578
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->clear()V

    .line 579
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v16

    .line 580
    .local v16, "orderItemSize":I
    const/4 v9, 0x0

    :goto_1
    if-ge v9, v3, :cond_7

    .line 581
    const/4 v8, 0x0

    .line 582
    .local v8, "exist":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 584
    .local v22, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const/4 v12, 0x0

    .local v12, "j":I
    :goto_2
    move/from16 v0, v16

    if-ge v12, v0, :cond_4

    .line 585
    move-object/from16 v0, v17

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    .line 586
    .restart local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mItem:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 587
    const/4 v8, 0x1

    .line 592
    .end local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_4
    if-nez v8, :cond_5

    .line 593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 580
    :cond_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 584
    .restart local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_6
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    .line 598
    .end local v8    # "exist":Z
    .end local v12    # "j":I
    .end local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    .end local v22    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_7
    const-string v29, "KEY_NUM_TEMPLATE"

    const/16 v30, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v25

    .line 599
    .local v25, "templateCnt":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    .line 600
    .local v19, "pm":Landroid/content/pm/PackageManager;
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Need to add templates. total size = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 601
    const/4 v9, 0x0

    :goto_3
    move/from16 v0, v25

    if-ge v9, v0, :cond_f

    .line 602
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 603
    .local v13, "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    const-string v29, "Pairedwindow_no"

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v26

    .line 604
    .local v26, "templateItem":Ljava/lang/String;
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "templateItem = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 605
    if-nez v26, :cond_8

    .line 601
    :goto_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 608
    :cond_8
    const-string v29, "#/#"

    move-object/from16 v0, v26

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v28

    .line 609
    .local v28, "value1":[Ljava/lang/String;
    const/16 v29, 0x0

    aget-object v29, v28, v29

    new-instance v30, Ljava/util/HashSet;

    invoke-direct/range {v30 .. v30}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    move-object/from16 v2, v30

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v24

    .line 610
    .local v24, "strCompoments":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 611
    .local v21, "sortByZone":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    invoke-interface/range {v24 .. v24}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v29

    if-eqz v29, :cond_9

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    .line 612
    .local v23, "strCompoment":Ljava/lang/String;
    const-string v29, "#/#"

    move-object/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v27

    .line 613
    .local v27, "value":[Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 615
    .end local v23    # "strCompoment":Ljava/lang/String;
    .end local v27    # "value":[Ljava/lang/String;
    :cond_9
    new-instance v7, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$2;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$2;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    .line 622
    .local v7, "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<[Ljava/lang/String;>;"
    move-object/from16 v0, v21

    invoke-static {v0, v7}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 624
    const/16 v29, 0x1

    aget-object v29, v28, v29

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 625
    .restart local v15    # "order":I
    const/4 v12, 0x0

    .restart local v12    # "j":I
    :goto_6
    invoke-virtual/range {v21 .. v21}, Ljava/util/ArrayList;->size()I

    move-result v29

    move/from16 v0, v29

    if-ge v12, v0, :cond_b

    .line 626
    const-string v30, "MultiWindowTrayInfo"

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Component = "

    move-object/from16 v0, v29

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, [Ljava/lang/String;

    const/16 v32, 0x0

    aget-object v29, v29, v32

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, [Ljava/lang/String;

    const/16 v32, 0x1

    aget-object v29, v29, v32

    move-object/from16 v0, v31

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v29

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, [Ljava/lang/String;

    const/16 v30, 0x0

    aget-object v29, v29, v30

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->findResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v20

    .line 628
    .local v20, "r":Landroid/content/pm/ResolveInfo;
    if-eqz v20, :cond_a

    .line 629
    new-instance v30, Landroid/util/Pair;

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, [Ljava/lang/String;

    const/16 v31, 0x1

    aget-object v29, v29, v31

    invoke-static/range {v29 .. v29}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v29

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object/from16 v0, v30

    invoke-interface {v13, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 625
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_6

    .line 631
    :cond_a
    invoke-interface {v13}, Ljava/util/List;->clear()V

    .line 635
    .end local v20    # "r":Landroid/content/pm/ResolveInfo;
    :cond_b
    invoke-interface {v13}, Ljava/util/List;->isEmpty()Z

    move-result v29

    if-nez v29, :cond_d

    .line 636
    const/4 v5, 0x0

    .line 637
    .local v5, "bDefaultTitle":Z
    move-object/from16 v0, v28

    array-length v0, v0

    move/from16 v29, v0

    const/16 v30, 0x2

    move/from16 v0, v29

    move/from16 v1, v30

    if-le v0, v1, :cond_c

    const-string v29, "true"

    const/16 v30, 0x2

    aget-object v30, v28, v30

    invoke-virtual/range {v29 .. v30}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 638
    const/4 v5, 0x1

    .line 640
    :cond_c
    new-instance v11, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    const/16 v29, 0x0

    aget-object v29, v28, v29

    move-object/from16 v0, p0

    move-object/from16 v1, v29

    invoke-direct {v11, v0, v13, v1, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Ljava/util/List;Ljava/lang/String;Z)V

    .line 641
    .restart local v11    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-lez v15, :cond_e

    .line 642
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " added to AppList"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 643
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 644
    new-instance v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    move-object/from16 v0, v18

    invoke-direct {v0, v11, v15}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;I)V

    .line 645
    .restart local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    invoke-interface/range {v17 .. v18}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 650
    .end local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 651
    invoke-interface {v13}, Ljava/util/List;->clear()V

    .line 653
    .end local v5    # "bDefaultTitle":Z
    .end local v11    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_d
    const/4 v6, 0x1

    goto/16 :goto_4

    .line 647
    .restart local v5    # "bDefaultTitle":Z
    .restart local v11    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_e
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    const-string v31, " added to DelList"

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    invoke-interface {v0, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 657
    .end local v5    # "bDefaultTitle":Z
    .end local v7    # "comparator":Ljava/util/Comparator;, "Ljava/util/Comparator<[Ljava/lang/String;>;"
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v12    # "j":I
    .end local v13    # "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v15    # "order":I
    .end local v21    # "sortByZone":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[Ljava/lang/String;>;"
    .end local v24    # "strCompoments":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v26    # "templateItem":Ljava/lang/String;
    .end local v28    # "value1":[Ljava/lang/String;
    :cond_f
    invoke-static/range {v17 .. v17}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 660
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->clear()V

    .line 661
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v16

    .line 662
    const/4 v9, 0x0

    :goto_8
    move/from16 v0, v16

    if-ge v9, v0, :cond_11

    .line 663
    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    .line 664
    .restart local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->getItem()Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v30

    invoke-interface/range {v29 .. v30}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 666
    move-object/from16 v0, v17

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    move-object/from16 v0, v29

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mDuplicatedOrder:Z

    move/from16 v29, v0

    if-eqz v29, :cond_10

    .line 667
    const-string v29, "MultiWindowTrayInfo"

    new-instance v30, Ljava/lang/StringBuilder;

    invoke-direct/range {v30 .. v30}, Ljava/lang/StringBuilder;-><init>()V

    const-string v31, "Abnormal xml(Duplicated Order) order = "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->getOrder()I

    move-result v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v29 .. v30}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 668
    const/4 v6, 0x1

    .line 662
    :cond_10
    add-int/lit8 v9, v9, 0x1

    goto :goto_8

    .line 673
    .end local v18    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v29, v0

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v29

    move/from16 v0, v29

    if-ne v14, v0, :cond_12

    if-eqz v6, :cond_13

    .line 674
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 676
    :cond_13
    return-void
.end method

.method public loadPopupList(Lcom/sec/android/app/FlashBarService/MultiUserPreferences;)V
    .locals 17
    .param p1, "prefs"    # Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    .prologue
    .line 679
    const-string v14, "MultiWindowTrayInfo"

    const-string v15, "loadPopupList"

    invoke-static {v14, v15}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    const/4 v3, 0x0

    .line 682
    .local v3, "bUpdateFile":Z
    const-string v14, "KEY_NUM_APP_POPUP"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    .line 683
    .local v8, "numAppInFile":I
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 685
    .local v12, "orderedPopupItems":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;>;"
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v1

    .line 686
    .local v1, "appListSize":I
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_2

    .line 687
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 688
    .local v6, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v14

    iget-object v14, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v14, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 689
    .local v2, "appName":Ljava/lang/String;
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 692
    .local v9, "order":I
    if-lez v9, :cond_1

    .line 693
    if-le v9, v8, :cond_0

    .line 694
    const-string v14, "MultiWindowTrayInfo"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "Abnormal xml(Too Large Order) order = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 695
    const/4 v3, 0x1

    .line 698
    :cond_0
    new-instance v11, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    invoke-direct {v11, v6, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;I)V

    .line 699
    .local v11, "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    invoke-interface {v12, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 686
    .end local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 703
    .end local v2    # "appName":Ljava/lang/String;
    .end local v6    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v9    # "order":I
    :cond_2
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v14

    if-eq v8, v14, :cond_3

    .line 704
    const/4 v3, 0x1

    .line 708
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 709
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v10

    .line 710
    .local v10, "orderItemSize":I
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v1, :cond_7

    .line 711
    const/4 v4, 0x0

    .line 712
    .local v4, "exist":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v14, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 714
    .local v13, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_2
    if-ge v7, v10, :cond_4

    .line 715
    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    .line 716
    .restart local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    iget-object v14, v11, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mItem:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v14, v13}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 717
    const/4 v4, 0x1

    .line 722
    .end local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_4
    if-nez v4, :cond_5

    .line 723
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v14, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 710
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 714
    .restart local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 728
    .end local v4    # "exist":Z
    .end local v7    # "j":I
    .end local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    .end local v13    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_7
    invoke-static {v12}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 731
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->clear()V

    .line 732
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v10

    .line 733
    const/4 v5, 0x0

    :goto_3
    if-ge v5, v10, :cond_9

    .line 734
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    .line 735
    .restart local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->getItem()Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v15

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 737
    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    iget-boolean v14, v14, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mDuplicatedOrder:Z

    if-eqz v14, :cond_8

    .line 738
    const/4 v3, 0x1

    .line 733
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 743
    .end local v11    # "orderedItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
    :cond_9
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v14}, Ljava/util/List;->size()I

    move-result v14

    if-ne v8, v14, :cond_a

    if-eqz v3, :cond_b

    .line 744
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 746
    :cond_b
    return-void
.end method

.method public makeAppListForScrollView(Ljava/util/List;ZI)V
    .locals 1
    .param p2, "editmode"    # Z
    .param p3, "appListPosition"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 1086
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/widget/LinearLayout;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 1087
    return-void
.end method

.method public makeAppListForScrollView(Ljava/util/List;ZIZ)V
    .locals 24
    .param p2, "editmode"    # Z
    .param p3, "appListPosition"    # I
    .param p4, "onlyBadgeUpdate"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;ZIZ)V"
        }
    .end annotation

    .prologue
    .line 1090
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/widget/LinearLayout;>;"
    const-string v22, "MultiWindowTrayInfo"

    const-string v23, "makeAppListForScrollView"

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1092
    if-nez p2, :cond_0

    .line 1093
    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setDisabledActivatedItems(Z)V

    .line 1095
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/util/SparseArray;->clear()V

    .line 1097
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v22, v0

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v3

    .line 1098
    .local v3, "appCount":I
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    .line 1100
    .local v4, "appSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const-string v23, "multiwindow_facade"

    invoke-virtual/range {v22 .. v23}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 1101
    .local v15, "multiWindowFacade":Lcom/samsung/android/multiwindow/MultiWindowFacade;
    const/16 v22, 0x64

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v21

    .line 1102
    .local v21, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/16 v22, 0x64

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v15, v0, v1}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v19

    .line 1104
    .local v19, "resumedTaskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v3, :cond_e

    if-ge v7, v4, :cond_e

    .line 1105
    const/4 v6, 0x0

    .line 1106
    .local v6, "badgeUpdated":Z
    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    .line 1107
    .local v11, "item":Landroid/widget/LinearLayout;
    const v22, 0x7f0f0059

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    .line 1108
    .local v9, "icon":Landroid/widget/ImageView;
    const v22, 0x7f0f005c

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 1109
    .local v16, "name":Landroid/widget/TextView;
    const v22, 0x7f0f005a

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 1110
    .local v17, "removeIcon":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1111
    .local v13, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const v22, 0x7f0f005b

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 1113
    .local v5, "badge":Landroid/widget/ImageView;
    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v18

    .line 1114
    .local v18, "resolveInfo":Landroid/content/pm/ResolveInfo;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportMultiInstance:Z

    move/from16 v22, v0

    const/16 v23, 0x64

    move-object/from16 v0, p0

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v7, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v12

    .line 1115
    .local v12, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v22

    if-eqz v22, :cond_2

    .line 1175
    .end local v5    # "badge":Landroid/widget/ImageView;
    .end local v6    # "badgeUpdated":Z
    .end local v9    # "icon":Landroid/widget/ImageView;
    .end local v11    # "item":Landroid/widget/LinearLayout;
    .end local v12    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v13    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v16    # "name":Landroid/widget/TextView;
    .end local v17    # "removeIcon":Landroid/widget/ImageView;
    .end local v18    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_1
    :goto_1
    return-void

    .line 1119
    .restart local v5    # "badge":Landroid/widget/ImageView;
    .restart local v6    # "badgeUpdated":Z
    .restart local v9    # "icon":Landroid/widget/ImageView;
    .restart local v11    # "item":Landroid/widget/LinearLayout;
    .restart local v12    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .restart local v13    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .restart local v16    # "name":Landroid/widget/TextView;
    .restart local v17    # "removeIcon":Landroid/widget/ImageView;
    .restart local v18    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_2
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_c

    .line 1120
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Intent;

    .line 1121
    .local v10, "intent":Landroid/content/Intent;
    const/16 v22, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1122
    const/16 v22, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1123
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportMultiInstance:Z

    move/from16 v22, v0

    if-eqz v22, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-object/from16 v22, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v23, v0

    invoke-virtual/range {v22 .. v23}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupporMultiInstance(Landroid/content/pm/ActivityInfo;)Z

    move-result v22

    if-nez v22, :cond_3

    invoke-virtual {v10}, Landroid/content/Intent;->getFlags()I

    move-result v22

    const/high16 v23, 0x8000000

    and-int v22, v22, v23

    if-eqz v22, :cond_b

    .line 1126
    :cond_3
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1127
    .local v20, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    move/from16 v22, v0

    const/16 v23, -0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_5

    move-object/from16 v0, v20

    iget v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    move/from16 v23, v0

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_4

    .line 1131
    :cond_5
    move-object/from16 v0, v18

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-nez v22, :cond_6

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v22, v0

    if-eqz v22, :cond_4

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v22

    invoke-virtual {v10}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v23

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 1133
    :cond_6
    const/4 v6, 0x1

    .line 1134
    const/16 v22, 0x67

    move/from16 v0, p3

    move/from16 v1, v22

    if-ne v0, v1, :cond_a

    .line 1135
    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/FrameLayout$LayoutParams;

    .line 1136
    .local v14, "lp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v22, 0x15

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1137
    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1138
    const v22, 0x7f0200e3

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1161
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v10    # "intent":Landroid/content/Intent;
    .end local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v20    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_7
    :goto_2
    if-eqz p4, :cond_8

    if-eqz v6, :cond_9

    .line 1162
    :cond_8
    if-eqz p2, :cond_d

    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v22

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-le v0, v1, :cond_d

    .line 1163
    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1164
    const/16 v22, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1169
    :goto_3
    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v22

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1104
    :cond_9
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 1140
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v10    # "intent":Landroid/content/Intent;
    .restart local v20    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_a
    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Landroid/widget/FrameLayout$LayoutParams;

    .line 1141
    .restart local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v22, 0x13

    move/from16 v0, v22

    iput v0, v14, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1142
    invoke-virtual {v5, v14}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1143
    const v22, 0x7f0200e4

    move/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_2

    .line 1149
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v14    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v20    # "t":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_b
    const/16 v22, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1150
    const/16 v22, 0xff

    move/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1151
    const/high16 v22, 0x3f800000    # 1.0f

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1153
    if-nez p2, :cond_7

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-virtual {v0, v10, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->isLaunchingBlockedItem(Landroid/content/Intent;Ljava/util/List;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 1154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v7, v11}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_2

    .line 1158
    .end local v10    # "intent":Landroid/content/Intent;
    :cond_c
    const/16 v22, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 1166
    :cond_d
    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1167
    const/16 v22, 0x8

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 1172
    .end local v5    # "badge":Landroid/widget/ImageView;
    .end local v6    # "badgeUpdated":Z
    .end local v9    # "icon":Landroid/widget/ImageView;
    .end local v11    # "item":Landroid/widget/LinearLayout;
    .end local v12    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    .end local v13    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v16    # "name":Landroid/widget/TextView;
    .end local v17    # "removeIcon":Landroid/widget/ImageView;
    .end local v18    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_e
    if-nez p2, :cond_1

    .line 1173
    const/16 v22, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setDisabledActivatedItems(Z)V

    goto/16 :goto_1
.end method

.method public makeAppListForSmartEditWindow(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1325
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/widget/LinearLayout;>;"
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    .line 1326
    .local v0, "appCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 1327
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1328
    .local v3, "item":Landroid/widget/LinearLayout;
    const v6, 0x7f0f0059

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1329
    .local v2, "icon":Landroid/widget/ImageView;
    const v6, 0x7f0f005c

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1330
    .local v5, "name":Landroid/widget/TextView;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1332
    .local v4, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz v2, :cond_0

    if-nez v5, :cond_1

    .line 1326
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1336
    :cond_1
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1338
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1339
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1341
    .end local v2    # "icon":Landroid/widget/ImageView;
    .end local v3    # "item":Landroid/widget/LinearLayout;
    .end local v4    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v5    # "name":Landroid/widget/TextView;
    :cond_2
    return-void
.end method

.method public makeFlashBarList()V
    .locals 2

    .prologue
    .line 246
    const-string v0, "MultiWindowTrayInfo"

    const-string v1, "makeFlashBarList"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->generateFlashBarList()V

    .line 251
    return-void
.end method

.method public makeInstancebadgeForGridView(Landroid/widget/ImageView;II)V
    .locals 12
    .param p1, "badge"    # Landroid/widget/ImageView;
    .param p2, "position"    # I
    .param p3, "appListPosition"    # I

    .prologue
    .line 1273
    const-string v10, "MultiWindowTrayInfo"

    const-string v11, "makeInstancebadgeForGridView"

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1275
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    const-string v11, "multiwindow_facade"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 1276
    .local v6, "multiWindowFacade":Lcom/samsung/android/multiwindow/MultiWindowFacade;
    const/4 v0, 0x0

    .line 1277
    .local v0, "badgeUpdated":Z
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v10, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1278
    .local v4, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v10, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v7

    .line 1279
    .local v7, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportMultiInstance:Z

    const/16 v11, 0x64

    invoke-virtual {p0, p2, v10, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v3

    .line 1280
    .local v3, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1314
    :cond_0
    :goto_0
    return-void

    .line 1283
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 1284
    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Intent;

    .line 1285
    .local v2, "intent":Landroid/content/Intent;
    const/4 v10, 0x0

    invoke-virtual {p1, v10}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1286
    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportMultiInstance:Z

    if-eqz v10, :cond_0

    .line 1287
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v11, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v10, v11}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupporMultiInstance(Landroid/content/pm/ActivityInfo;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v2}, Landroid/content/Intent;->getFlags()I

    move-result v10

    const/high16 v11, 0x8000000

    and-int/2addr v10, v11

    if-eqz v10, :cond_0

    .line 1289
    :cond_2
    const/16 v10, 0x64

    const/4 v11, 0x2

    invoke-virtual {v6, v10, v11}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v9

    .line 1290
    .local v9, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1291
    .local v8, "t":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->userId:I

    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v11

    if-ne v10, v11, :cond_3

    .line 1294
    iget-object v10, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v11, v8, Landroid/app/ActivityManager$RunningTaskInfo;->baseActivity:Landroid/content/ComponentName;

    invoke-virtual {v11}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    const/4 v11, 0x2

    if-lt v10, v11, :cond_5

    :cond_4
    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    if-eqz v10, :cond_3

    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v10}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1296
    :cond_5
    const/4 v0, 0x1

    .line 1297
    const/16 v10, 0x67

    if-ne p3, v10, :cond_6

    .line 1298
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 1299
    .local v5, "lp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v10, 0x15

    iput v10, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1300
    invoke-virtual {p1, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1301
    const v10, 0x7f0200e3

    invoke-virtual {p1, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0

    .line 1303
    .end local v5    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_6
    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/FrameLayout$LayoutParams;

    .line 1304
    .restart local v5    # "lp":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v10, 0x13

    iput v10, v5, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1305
    invoke-virtual {p1, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1306
    const v10, 0x7f020056

    invoke-virtual {p1, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public makeMultiInstanceIntent(Landroid/content/pm/ResolveInfo;Landroid/content/Intent;)V
    .locals 7
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x0

    const/high16 v6, 0x8000000

    const/4 v5, 0x3

    const/4 v4, 0x2

    .line 1484
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v2, :cond_3

    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1485
    .local v0, "applicationMetaData":Landroid/os/Bundle;
    :goto_0
    if-eqz v0, :cond_0

    const-string v2, "com.samsung.android.sdk.multiwindow.multiinstance.enable"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v2, v3}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupporMultiInstance(Landroid/content/pm/ActivityInfo;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1487
    :cond_1
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v2, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    if-eq v2, v4, :cond_4

    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v2, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    if-eq v2, v5, :cond_4

    .line 1488
    invoke-virtual {p2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1499
    :cond_2
    :goto_1
    return-void

    .end local v0    # "applicationMetaData":Landroid/os/Bundle;
    :cond_3
    move-object v0, v1

    .line 1484
    goto :goto_0

    .line 1490
    .restart local v0    # "applicationMetaData":Landroid/os/Bundle;
    :cond_4
    if-eqz v0, :cond_5

    const-string v2, "com.samsung.android.sdk.multiwindow.multiinstance.launchmode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1491
    .local v1, "multiInstanceLaunchMode":Ljava/lang/String;
    :cond_5
    if-eqz v1, :cond_2

    .line 1492
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v2, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    if-ne v2, v4, :cond_6

    const-string v2, "singleTask"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_6
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget v2, v2, Landroid/content/pm/ActivityInfo;->launchMode:I

    if-ne v2, v5, :cond_2

    const-string v2, "singleInstance"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1494
    :cond_7
    invoke-virtual {p2, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_1
.end method

.method public makePopupViewPagerAppList()V
    .locals 7

    .prologue
    .line 2399
    const/4 v3, 0x0

    .line 2400
    .local v3, "skip":Z
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 2401
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_3

    .line 2402
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2403
    .local v2, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const/4 v3, 0x0

    .line 2404
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 2405
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 2406
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Ljava/lang/CharSequence;

    move-result-object v4

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mTitle:Ljava/lang/CharSequence;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2407
    const/4 v3, 0x1

    .line 2412
    .end local v1    # "j":I
    :cond_0
    if-nez v3, :cond_1

    .line 2413
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupViewPagerAppList:Ljava/util/List;

    new-instance v6, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v4

    invoke-direct {v6, p0, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2401
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2405
    .restart local v1    # "j":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2416
    .end local v1    # "j":I
    .end local v2    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_3
    return-void
.end method

.method public makeViewPagerAppList()I
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2365
    const/4 v1, 0x0

    .line 2366
    .local v1, "frequentlyUsedAppCnt":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2367
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 2369
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 2370
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v6, :cond_0

    .line 2371
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 2372
    .local v0, "appName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    invoke-virtual {v3, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->getCount(Ljava/lang/String;)I

    move-result v3

    if-lt v3, v6, :cond_0

    .line 2373
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    new-instance v5, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    invoke-direct {v5, p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369
    .end local v0    # "appName":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2377
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mMultiWindowTrayInfoUsageStatus:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfoUsageStatus;->getSortingList(Ljava/util/List;)Ljava/util/List;

    .line 2379
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_2

    .line 2380
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0x8

    if-lt v3, v4, :cond_4

    .line 2381
    const/16 v1, 0x8

    .line 2387
    :cond_2
    :goto_1
    const/4 v2, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 2388
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->mLists:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v3, v6, :cond_3

    .line 2389
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v4, v3}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupportMultiWindow(Landroid/content/pm/ActivityInfo;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2390
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mViewPagerAppList:Ljava/util/List;

    new-instance v5, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    invoke-direct {v5, p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;-><init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Landroid/content/pm/ResolveInfo;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2387
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2383
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFrequentlyViewPagerAppList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_1

    .line 2395
    :cond_5
    return v1
.end method

.method public moveToAppListItem(II)V
    .locals 2
    .param p1, "dstindex"    # I
    .param p2, "editIndex"    # I

    .prologue
    .line 306
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 308
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-gez p1, :cond_0

    .line 309
    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicate(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 311
    return-void
.end method

.method public moveToEditListItem(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 287
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 289
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 291
    return-void
.end method

.method public moveToSmartEditItem(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 294
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 295
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    const v3, 0x7f08004d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 303
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 300
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 302
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    invoke-interface {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;->notifyAppListSizeChanged()V

    goto :goto_0
.end method

.method public moveToSmartWindowItem(II)V
    .locals 2
    .param p1, "dstIndex"    # I
    .param p2, "editIndex"    # I

    .prologue
    .line 314
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 315
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-gez p1, :cond_0

    .line 316
    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addwithcheckduplicateForPenWindow(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;)Z

    .line 317
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 318
    return-void
.end method

.method public pkgManagerList(Landroid/content/Intent;Z)Z
    .locals 10
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "forSmart"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1593
    const-string v7, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1595
    const-string v7, "MultiWindowTrayInfo"

    const-string v8, "android.intent.action.PACKAGE_ADDED"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1596
    const-string v7, "android.intent.extra.REPLACING"

    invoke-virtual {p1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1597
    .local v0, "bReplace":Z
    if-nez v0, :cond_2

    .line 1598
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 1599
    .local v3, "packageName":Ljava/lang/String;
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addInstalledPackage(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1650
    .end local v0    # "bReplace":Z
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return v5

    .line 1603
    :cond_1
    const-string v7, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1605
    const-string v7, "MultiWindowTrayInfo"

    const-string v8, "android.intent.action.PACKAGE_REMOVE"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1606
    const-string v7, "android.intent.extra.REPLACING"

    invoke-virtual {p1, v7, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1607
    .restart local v0    # "bReplace":Z
    if-nez v0, :cond_2

    .line 1608
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 1609
    .restart local v3    # "packageName":Ljava/lang/String;
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->removeInstalledPackage(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_0

    .end local v0    # "bReplace":Z
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_2
    :goto_1
    move v5, v6

    .line 1650
    goto :goto_0

    .line 1613
    :cond_3
    const-string v7, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1615
    const-string v7, "MultiWindowTrayInfo"

    const-string v8, "android.intent.action.PACKAGE_CHANGED"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1616
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 1617
    .restart local v3    # "packageName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 1618
    .local v4, "pm":Landroid/content/pm/PackageManager;
    const-string v7, "package"

    invoke-static {v7}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v7

    invoke-static {v7}, Landroid/content/pm/IPackageManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/content/pm/IPackageManager;

    move-result-object v1

    .line 1619
    .local v1, "mIPm":Landroid/content/pm/IPackageManager;
    const/4 v2, -0x1

    .line 1621
    .local v2, "packageChangedState":I
    if-eqz v4, :cond_4

    .line 1623
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I

    move-result v7

    invoke-interface {v1, v3, v7}, Landroid/content/pm/IPackageManager;->getApplicationEnabledSetting(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1627
    :cond_4
    :goto_2
    const/4 v7, 0x3

    if-eq v2, v7, :cond_5

    const/4 v7, 0x2

    if-ne v2, v7, :cond_6

    .line 1629
    :cond_5
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->removeInstalledPackage(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_0

    .line 1632
    :cond_6
    if-eqz v2, :cond_7

    if-ne v2, v5, :cond_8

    .line 1634
    :cond_7
    invoke-virtual {p0, v3, p2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->addInstalledPackage(Ljava/lang/String;Z)Z

    move-result v7

    if-eqz v7, :cond_2

    goto :goto_0

    .line 1638
    :cond_8
    const-string v5, "MultiWindowTrayInfo"

    const-string v7, "android.intent.action.PACKAGE_CHANGED : state is invalid"

    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 1640
    .end local v1    # "mIPm":Landroid/content/pm/IPackageManager;
    .end local v2    # "packageChangedState":I
    .end local v3    # "packageName":Ljava/lang/String;
    .end local v4    # "pm":Landroid/content/pm/PackageManager;
    :cond_9
    const-string v7, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1641
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v3

    .line 1643
    .restart local v3    # "packageName":Ljava/lang/String;
    const-string v7, "MultiWindowTrayInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "android.intent.action.PACKAGE_REPLACED packageName="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1644
    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->updateInstalledPackage(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    goto/16 :goto_0

    .line 1648
    .end local v3    # "packageName":Ljava/lang/String;
    :cond_a
    const-string v5, "MultiWindowTrayInfo"

    const-string v7, "RECEIVER"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 1624
    .restart local v1    # "mIPm":Landroid/content/pm/IPackageManager;
    .restart local v2    # "packageChangedState":I
    .restart local v3    # "packageName":Ljava/lang/String;
    .restart local v4    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v7

    goto :goto_2
.end method

.method public releaseAppListBitmap(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1065
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/widget/LinearLayout;>;"
    const-string v5, "MultiWindowTrayInfo"

    const-string v6, "releaseAppListBitmap"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    if-nez p1, :cond_1

    .line 1083
    :cond_0
    return-void

    .line 1068
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 1069
    .local v0, "appCount":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 1071
    .local v1, "appSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    if-ge v3, v1, :cond_0

    .line 1072
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    const v6, 0x7f0f0059

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 1073
    .local v4, "icon":Landroid/widget/ImageView;
    if-nez v4, :cond_3

    .line 1071
    :cond_2
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1075
    :cond_3
    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1076
    .local v2, "dr":Landroid/graphics/drawable/Drawable;
    instance-of v5, v2, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v5, :cond_2

    move-object v5, v2

    .line 1078
    check-cast v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1080
    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    .end local v2    # "dr":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_1
.end method

.method public removeInstalledPackage(Ljava/lang/String;Z)Z
    .locals 8
    .param p1, "pkgName"    # Ljava/lang/String;
    .param p2, "forSmart"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1778
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-nez v7, :cond_0

    .line 1840
    :goto_0
    return v6

    .line 1782
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1783
    .local v4, "rmovedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;>;"
    const/4 v2, 0x0

    .line 1784
    .local v2, "resolveInfo":Landroid/content/pm/ResolveInfo;
    const/4 v3, 0x0

    .line 1786
    .local v3, "resolveInfo2":Landroid/content/pm/ResolveInfo;
    if-eqz p2, :cond_4

    .line 1787
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1788
    .local v1, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 1789
    iget-object v7, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1790
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1793
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1794
    .restart local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 1795
    iget-object v7, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1796
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1797
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromPopupDelList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1801
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_4
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_5
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1802
    .restart local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 1803
    iget-object v7, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1804
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1805
    :cond_6
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v5, :cond_5

    .line 1806
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo2()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 1807
    iget-object v7, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1808
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1813
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_7
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_8
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1814
    .restart local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v2

    .line 1815
    iget-object v7, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 1816
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1817
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovedFromDelList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1818
    :cond_9
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, v5, :cond_8

    .line 1819
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo2()Landroid/content/pm/ResolveInfo;

    move-result-object v3

    .line 1820
    iget-object v7, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v7, v7, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v7, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1821
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1827
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_a
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_d

    .line 1828
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1829
    .restart local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1830
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1831
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1832
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPenWindowOnlyList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1833
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupDelList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1835
    .end local v1    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_b
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    invoke-interface {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;->notifyAppListSizeChanged()V

    .line 1836
    :cond_c
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 1837
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 1840
    :cond_d
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_e

    :goto_6
    move v6, v5

    goto/16 :goto_0

    :cond_e
    move v5, v6

    goto :goto_6
.end method

.method public removeTemplate(IZ)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "bEditmode"    # Z

    .prologue
    .line 2045
    const/4 v1, -0x1

    if-le p1, v1, :cond_2

    .line 2046
    const/4 v0, 0x0

    .line 2047
    .local v0, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz p2, :cond_1

    .line 2048
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2049
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2055
    :goto_0
    if-eqz v0, :cond_0

    .line 2056
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mTemplateList:Ljava/util/List;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2058
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 2062
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :goto_1
    return-void

    .line 2051
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2052
    .restart local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2060
    .end local v0    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_2
    const-string v1, "MultiWindowTrayInfo"

    const-string v2, "removeTemplate : index == ESTIMATE_INVALID_VALUE"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public reorderApplist(II)V
    .locals 2
    .param p1, "dst"    # I
    .param p2, "src"    # I

    .prologue
    const/4 v1, -0x1

    .line 1537
    if-le p1, v1, :cond_0

    if-le p2, v1, :cond_0

    .line 1538
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1540
    .local v0, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1541
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1544
    .end local v0    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->saveFlashBarList()V

    .line 1545
    return-void
.end method

.method public reorderSmartWindowList(II)V
    .locals 2
    .param p1, "dst"    # I
    .param p2, "src"    # I

    .prologue
    const/4 v1, -0x1

    .line 1548
    if-le p1, v1, :cond_0

    if-le p2, v1, :cond_0

    .line 1549
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1551
    .local v0, "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1552
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1553
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    invoke-interface {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;->notifyAppListSizeChanged()V

    .line 1555
    .end local v0    # "srcItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 1556
    return-void
.end method

.method public resetMultiWindowTray()V
    .locals 5

    .prologue
    .line 2295
    new-instance v0, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarListFileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 2296
    .local v0, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    new-instance v1, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupListFileName:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 2297
    .local v1, "prefsForPopup":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->clear()V

    .line 2298
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->clear()V

    .line 2299
    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2300
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 2301
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 2302
    return-void
.end method

.method public resetThemeManager()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2454
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mSupportLegacyTheme:Z

    if-eqz v1, :cond_1

    .line 2455
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "current_sec_theme_package"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2456
    .local v0, "themePackageName":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2457
    :cond_0
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    .line 2461
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeMgr:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v1}, Lcom/samsung/android/theme/SThemeManager;->resetTheme()V

    .line 2462
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mThemeChanged:Z

    .line 2464
    .end local v0    # "themePackageName":Ljava/lang/String;
    :cond_1
    return-void

    .line 2459
    .restart local v0    # "themePackageName":Ljava/lang/String;
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mIsUseDefaultTheme:Z

    goto :goto_0
.end method

.method public saveFlashBarList()V
    .locals 22

    .prologue
    .line 759
    const-string v19, "MultiWindowTrayInfo"

    const-string v20, "saveFlashBarList"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 761
    new-instance v13, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarListFileName:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v13, v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 762
    .local v13, "prefs":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    const/4 v15, 0x0

    .line 764
    .local v15, "templateIndex":I
    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->clear()V

    .line 766
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v4

    .line 767
    .local v4, "appListSize":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v4, :cond_4

    .line 768
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 769
    .local v10, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v12

    .line 771
    .local v12, "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 772
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v19, v0

    add-int/lit8 v20, v8, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 767
    :cond_0
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 773
    :cond_1
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_0

    .line 774
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    .line 777
    .local v5, "appName":Ljava/lang/CharSequence;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    add-int/lit8 v20, v8, 0x1

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 778
    .local v16, "value1":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->isDefaultTitle()Z

    move-result v19

    if-eqz v19, :cond_2

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "true"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 779
    :goto_2
    const-string v19, "Pairedwindow_no"

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 780
    add-int/lit8 v15, v15, 0x1

    .line 787
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 788
    .local v6, "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 790
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v11

    .line 791
    .local v11, "launchItemSize":I
    const/4 v9, 0x0

    .local v9, "j":I
    :goto_3
    if-ge v9, v11, :cond_3

    .line 792
    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/util/Pair;

    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 793
    .local v14, "resolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-interface {v12, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/util/Pair;

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 794
    .local v18, "windowMode":I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 795
    .local v17, "value2":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 791
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 778
    .end local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9    # "j":I
    .end local v11    # "launchItemSize":I
    .end local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v17    # "value2":Ljava/lang/String;
    .end local v18    # "windowMode":I
    :cond_2
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "false"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_2

    .line 797
    .restart local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v9    # "j":I
    .restart local v11    # "launchItemSize":I
    :cond_3
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0, v6}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putStringSet(Ljava/lang/String;Ljava/util/Set;)V

    goto/16 :goto_1

    .line 801
    .end local v5    # "appName":Ljava/lang/CharSequence;
    .end local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v9    # "j":I
    .end local v10    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v11    # "launchItemSize":I
    .end local v12    # "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    .end local v16    # "value1":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Ljava/util/List;->size()I

    move-result v3

    .line 802
    .local v3, "appDeleteListSize":I
    const/4 v9, 0x0

    .restart local v9    # "j":I
    :goto_4
    if-ge v9, v3, :cond_8

    .line 803
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 804
    .restart local v10    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v5

    .line 805
    .restart local v5    # "appName":Ljava/lang/CharSequence;
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v12

    .line 807
    .restart local v12    # "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_7

    .line 809
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "0"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 810
    .restart local v16    # "value1":Ljava/lang/String;
    invoke-virtual {v10}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->isDefaultTitle()Z

    move-result v19

    if-eqz v19, :cond_5

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "true"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    .line 811
    :goto_5
    const-string v19, "Pairedwindow_no"

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    add-int/lit8 v15, v15, 0x1

    .line 814
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 815
    .restart local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 817
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v11

    .line 818
    .restart local v11    # "launchItemSize":I
    const/4 v7, 0x0

    .local v7, "h":I
    :goto_6
    if-ge v7, v11, :cond_6

    .line 819
    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/util/Pair;

    move-object/from16 v0, v19

    iget-object v14, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 820
    .restart local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-interface {v12, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/util/Pair;

    move-object/from16 v0, v19

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v19, v0

    check-cast v19, Ljava/lang/Integer;

    invoke-virtual/range {v19 .. v19}, Ljava/lang/Integer;->intValue()I

    move-result v18

    .line 821
    .restart local v18    # "windowMode":I
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    .line 822
    .restart local v17    # "value2":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 818
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 810
    .end local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "h":I
    .end local v11    # "launchItemSize":I
    .end local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .end local v17    # "value2":Ljava/lang/String;
    .end local v18    # "windowMode":I
    :cond_5
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "#/#"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "false"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    goto/16 :goto_5

    .line 824
    .restart local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .restart local v7    # "h":I
    .restart local v11    # "launchItemSize":I
    :cond_6
    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v13, v0, v6}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putStringSet(Ljava/lang/String;Ljava/util/Set;)V

    .line 802
    .end local v6    # "components":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "h":I
    .end local v11    # "launchItemSize":I
    .end local v16    # "value1":Ljava/lang/String;
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_4

    .line 828
    .end local v5    # "appName":Ljava/lang/CharSequence;
    .end local v10    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v12    # "lists":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    :cond_8
    const-string v19, "KEY_FILE_SAVE"

    const/16 v20, 0x1

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putBoolean(Ljava/lang/String;Z)V

    .line 829
    const-string v19, "KEY_NUM_APP"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Ljava/util/List;->size()I

    move-result v20

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 830
    const-string v19, "KEY_NUM_TEMPLATE"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0, v15}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 831
    invoke-virtual {v13}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 832
    return-void
.end method

.method public savePopupList()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 835
    const-string v5, "MultiWindowTrayInfo"

    const-string v6, "savePopupList"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 837
    new-instance v4, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupListFileName:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5, v6, v7}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;-><init>(Ljava/lang/String;Landroid/content/Context;Z)V

    .line 838
    .local v4, "prefsForPopup":Lcom/sec/android/app/FlashBarService/MultiUserPreferences;
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->clear()V

    .line 840
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 841
    .local v0, "appListSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 842
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 843
    .local v3, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 844
    .local v1, "appName":Ljava/lang/CharSequence;
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v5

    iget-object v5, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    add-int/lit8 v6, v2, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 841
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 846
    .end local v1    # "appName":Ljava/lang/CharSequence;
    .end local v3    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 847
    const-string v5, "KEY_FILE_SAVE_POPUP"

    invoke-virtual {v4, v5, v7}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putBoolean(Ljava/lang/String;Z)V

    .line 849
    :cond_1
    const-string v5, "KEY_NUM_APP_POPUP"

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mPopupAppList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->putInt(Ljava/lang/String;I)V

    .line 850
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiUserPreferences;->commit()V

    .line 851
    return-void
.end method

.method public setAppListSizeChangeListener(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mListener:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;

    .line 136
    return-void
.end method

.method public setDisabledActivatedItems(Z)V
    .locals 8
    .param p1, "disable"    # Z

    .prologue
    .line 1247
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-gtz v5, :cond_1

    .line 1270
    :cond_0
    return-void

    .line 1249
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v0, v5, :cond_0

    .line 1250
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1251
    .local v2, "item":Landroid/widget/LinearLayout;
    const v5, 0x7f0f0059

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 1252
    .local v1, "icon":Landroid/widget/ImageView;
    const v5, 0x7f0f005c

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1253
    .local v4, "name":Landroid/widget/TextView;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v6

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1255
    .local v3, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    if-eqz p1, :cond_3

    .line 1256
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 1257
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1259
    :cond_2
    const/16 v5, 0x66

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1260
    const v5, 0x3ecccccd    # 0.4f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1261
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1262
    const-string v5, "MultiWindowTrayInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Disable ActivatedItems : AppIndex="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / name="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1249
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1264
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    .line 1265
    const/16 v5, 0xff

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1266
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1267
    const-string v5, "MultiWindowTrayInfo"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Enable ActivatedItems : AppIndex="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mActivatedLayoutList:Landroid/util/SparseArray;

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " / name="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setFlashBarState(Z)V
    .locals 4
    .param p1, "enableState"    # Z

    .prologue
    .line 749
    const-string v0, "MultiWindowTrayInfo"

    const-string v1, "setFlashBarState"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "multi_window_flashbar_shown"

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const/4 v3, -0x2

    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 751
    return-void

    .line 750
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRemoveTask(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 1317
    iput p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mRemovingTask:I

    .line 1318
    return-void
.end method

.method public updateAppListForScrollView(Ljava/util/List;Z)V
    .locals 10
    .param p2, "editMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1344
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/widget/LinearLayout;>;"
    const-string v8, "MultiWindowTrayInfo"

    const-string v9, "updateAppListForScrollView"

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    .line 1347
    .local v0, "appCount":I
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    .line 1349
    .local v1, "appSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    if-ge v3, v1, :cond_2

    .line 1350
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    .line 1351
    .local v5, "layout":Landroid/widget/LinearLayout;
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    const v9, 0x7f0f0059

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 1352
    .local v2, "button":Landroid/widget/ImageView;
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    const v9, 0x7f0f005c

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1353
    .local v6, "name":Landroid/widget/TextView;
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    const v9, 0x7f0f005a

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 1354
    .local v7, "removeIcon":Landroid/widget/ImageView;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1355
    .local v4, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1356
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_0

    .line 1357
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1358
    if-eqz p2, :cond_1

    .line 1359
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1363
    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1349
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1361
    :cond_1
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1366
    .end local v2    # "button":Landroid/widget/ImageView;
    .end local v4    # "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v5    # "layout":Landroid/widget/LinearLayout;
    .end local v6    # "name":Landroid/widget/TextView;
    .end local v7    # "removeIcon":Landroid/widget/ImageView;
    :cond_2
    return-void
.end method

.method public updateInstalledPackage(Ljava/lang/String;)Z
    .locals 13
    .param p1, "pkgName"    # Ljava/lang/String;

    .prologue
    .line 1845
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_1

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-nez v10, :cond_1

    .line 1846
    const/4 v9, 0x0

    .line 1893
    :cond_0
    return v9

    .line 1849
    :cond_1
    const/4 v9, 0x0

    .line 1850
    .local v9, "result":Z
    const/4 v8, 0x0

    .line 1851
    .local v8, "resolveInfo":Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->queryResolveInfo(Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    .line 1853
    .local v4, "infos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarAppList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1854
    .local v5, "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v6

    .line 1855
    .local v6, "itemList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/util/Pair;

    .line 1856
    .local v7, "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v8    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 1857
    .restart local v8    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v10, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1858
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 1859
    .local v3, "info":Landroid/content/pm/ResolveInfo;
    iget-object v10, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v11, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1860
    const-string v10, "MultiWindowTrayInfo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updateInstalledPackage() : item="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1861
    const/4 v9, 0x1

    goto :goto_0

    .line 1866
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    .end local v7    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_5
    if-eqz v9, :cond_2

    .line 1867
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 1868
    .restart local v3    # "info":Landroid/content/pm/ResolveInfo;
    invoke-virtual {v5, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->updateResolveInfo(Landroid/content/pm/ResolveInfo;)V

    .line 1869
    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->refreshItemIcon()V

    goto :goto_1

    .line 1873
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    .end local v5    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .end local v6    # "itemList":Ljava/util/List;, "Ljava/util/List<Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;>;"
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mFlashBarDelList:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 1874
    .restart local v5    # "item":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    const-string v10, "MultiWindowTrayInfo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updateInstalledPackage() : mFlashBarDelList item title="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1875
    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/util/Pair;

    .line 1876
    .restart local v7    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    iget-object v8, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v8    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    check-cast v8, Landroid/content/pm/ResolveInfo;

    .line 1877
    .restart local v8    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v10, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1878
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .restart local v2    # "i$":Ljava/util/Iterator;
    :cond_9
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 1879
    .restart local v3    # "info":Landroid/content/pm/ResolveInfo;
    iget-object v10, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v10, v10, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    iget-object v11, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1880
    const-string v10, "MultiWindowTrayInfo"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "updateInstalledPackage() : item="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1881
    const/4 v9, 0x1

    goto :goto_2

    .line 1886
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "info":Landroid/content/pm/ResolveInfo;
    .end local v7    # "pair":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/content/pm/ResolveInfo;Ljava/lang/Integer;>;"
    :cond_a
    if-eqz v9, :cond_7

    .line 1887
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    .line 1888
    .restart local v3    # "info":Landroid/content/pm/ResolveInfo;
    invoke-virtual {v5, v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->updateResolveInfo(Landroid/content/pm/ResolveInfo;)V

    .line 1889
    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->refreshItemIcon()V

    goto :goto_3
.end method

.method public updateSupportedAppList()V
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    invoke-virtual {v0}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->loadCscAppList()V

    .line 182
    return-void
.end method
