.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;)V
    .locals 0

    .prologue
    .line 4669
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x0

    .line 4673
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V
    invoke-static {v0, v1, v2, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    .line 4674
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mNeedUpdatePocketTrayButtonText:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13802(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Z)Z

    .line 4675
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 4671
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 4677
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80$1;->this$1:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$80;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListPosition:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->updatePocketTrayLayout(IZZ)V
    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$13700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;IZZ)V

    .line 4678
    return-void
.end method
