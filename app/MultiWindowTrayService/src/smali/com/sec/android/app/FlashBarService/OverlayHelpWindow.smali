.class public Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;
.super Landroid/widget/RelativeLayout;
.source "OverlayHelpWindow.java"


# instance fields
.field private mBinder:Landroid/os/IBinder;

.field private mIsShowing:Z

.field private mLayoutId:I

.field private mMainView:Landroid/view/View;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/IBinder;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "binder"    # Landroid/os/IBinder;
    .param p3, "layoutId"    # I

    .prologue
    const/4 v4, -0x1

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 20
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mIsShowing:Z

    .line 24
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mBinder:Landroid/os/IBinder;

    .line 25
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 26
    iput p3, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mLayoutId:I

    .line 27
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 28
    .local v0, "layoutInflater":Landroid/view/LayoutInflater;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mLayoutId:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mMainView:Landroid/view/View;

    .line 29
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 31
    .local v1, "rlp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mMainView:Landroid/view/View;

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    return-void
.end method

.method private generateLayoutParam()Landroid/view/WindowManager$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 54
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 56
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    const-string v1, "MultiWindow OverayHelpWindow"

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 57
    const v1, 0x800033

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 58
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 59
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 60
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 61
    const/16 v1, 0x831

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 62
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x100

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 63
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mBinder:Landroid/os/IBinder;

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 65
    return-object v0
.end method


# virtual methods
.method public dismissHelpWindow()V
    .locals 1

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mIsShowing:Z

    if-nez v0, :cond_0

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mIsShowing:Z

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 70
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 80
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 72
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->dismissHelpWindow()V

    goto :goto_0

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 85
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mIsShowing:Z

    return v0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->generateLayoutParam()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 36
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1, p0, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 38
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/OverlayHelpWindow;->mIsShowing:Z

    .line 39
    return-void
.end method
