.class public Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
.super Landroid/app/Activity;
.source "TryMultiWindowTemplate.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field static mPocketOpenBtnPushed:Z


# instance fields
.field public GUI_SCALE_PREVIEW:Z

.field drs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mFadeInAnimation:Landroid/view/animation/Animation;

.field private mFinishRunnable:Ljava/lang/Runnable;

.field private mHandle:Landroid/os/Handler;

.field private mInvalidToast:Landroid/widget/Toast;

.field private mOrientation:I

.field private mPocketBody:Landroid/widget/RelativeLayout;

.field private mPocketBtnLayout:Landroid/widget/RelativeLayout;

.field private mPocketLayout:Landroid/widget/RelativeLayout;

.field private mPocketOpenBtn:Landroid/widget/ImageButton;

.field mPocketTemplateCreateBtnListener:Landroid/view/View$OnClickListener;

.field mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

.field private mResouce:Landroid/content/res/Resources;

.field mTrayHandleListener:Landroid/view/View$OnClickListener;

.field private mTryMWViewImageContainer:Landroid/widget/LinearLayout;

.field private mTryMWViewImageContainer2:Landroid/widget/LinearLayout;

.field private mTryMultiWindowAppIcon1:Landroid/widget/ImageView;

.field private mTryMultiWindowAppIcon2:Landroid/widget/ImageView;

.field private mTryMultiWindowAppPaired:Landroid/widget/ImageView;

.field private mTryMultiWindowTemplateHelper:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowTemplateHelperWidthCenter:I

.field private mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateCue3:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateEditBtn:Landroid/widget/ImageButton;

.field private mTryMultiWindowtemplateFocusFrame:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateFocusFrame2:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateHandle:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateHandleClosed:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateHelpBtn:Landroid/widget/ImageButton;

.field private mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplatePopup1:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowtemplatePopup2Parent:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowtemplatePopup3:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowtemplatePopup3Parent:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowtemplatePopup4:Landroid/widget/RelativeLayout;

.field private mTryMultiWindowtemplateSplitCircle:Landroid/widget/ImageView;

.field private mTryMultiWindowtemplateTemplateBtn:Landroid/widget/ImageButton;

.field private mTryMultiWindowtemplateTray:Landroid/widget/ImageView;

.field private mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

.field private mTryMultiwindowtemplateFramesApp:Landroid/widget/LinearLayout;

.field private mTryMultiwindowtemplateFramesFocusLayout:Landroid/widget/LinearLayout;

.field private mWindowManager:Landroid/view/WindowManager;

.field private moveBtnHeight:I

.field private openButtonHeight:I

.field private openPocketTrayHeght:I

.field private pocketTopMargin:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtnPushed:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mInvalidToast:Landroid/widget/Toast;

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->GUI_SCALE_PREVIEW:Z

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->drs:Ljava/util/List;

    .line 101
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mHandle:Landroid/os/Handler;

    .line 367
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$5;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTrayHandleListener:Landroid/view/View$OnClickListener;

    .line 396
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$6;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketTemplateCreateBtnListener:Landroid/view/View$OnClickListener;

    .line 432
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$8;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

    .line 618
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$9;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFinishRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandleClosed:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTray:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandle:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup3Parent:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer2:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTemplateBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue3:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2Parent:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketBody:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->animationTemplateIconMake()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;IZZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->updatePocketTrayLayout(IZZ)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppPaired:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup4:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFinishRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mHandle:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method private animationTemplateIconMake()V
    .locals 2

    .prologue
    .line 348
    const v1, 0x7f04003b

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 350
    .local v0, "mPairedbubbleAnimation":Landroid/view/animation/Animation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$4;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 364
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppPaired:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 365
    return-void
.end method

.method private generateMultiIcon(Ljava/util/List;)Landroid/graphics/drawable/Drawable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 336
    .local p1, "drs":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/drawable/Drawable;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    .line 337
    .local v3, "size":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .local v2, "info":Ljava/util/List;, "Ljava/util/List<Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_0

    .line 339
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 342
    :cond_0
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-static {v4, v2}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->createFolderIconWithPlate(Landroid/graphics/Bitmap;Ljava/util/List;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 343
    .local v0, "buttonBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v4
.end method

.method private getSizeOfStatusBar()I
    .locals 3

    .prologue
    .line 584
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 585
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 586
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 587
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method private updatePocketTrayLayout(IZZ)V
    .locals 7
    .param p1, "position"    # I
    .param p2, "isStarting"    # Z
    .param p3, "forceClose"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 461
    new-instance v1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 464
    .local v1, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 465
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 466
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 468
    :cond_0
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 469
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openPocketTrayHeght:I

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    .line 470
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtn:Landroid/widget/ImageButton;

    const v3, 0x7f02003b

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 471
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 472
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 473
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 474
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtn:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 475
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketBtnLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    return-void
.end method


# virtual methods
.method public init()V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 174
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 175
    .local v2, "fullscreen":Landroid/graphics/Point;
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 176
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mResouce:Landroid/content/res/Resources;

    .line 177
    const-string v3, "window"

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    .line 179
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mResouce:Landroid/content/res/Resources;

    const/high16 v4, 0x7f090000

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->GUI_SCALE_PREVIEW:Z

    .line 181
    const v3, 0x7f0f0145

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtn:Landroid/widget/ImageButton;

    .line 182
    const v3, 0x7f0f0138

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

    .line 183
    const v3, 0x7f0f013b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTray:Landroid/widget/ImageView;

    .line 184
    const v3, 0x7f0f0137

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

    .line 185
    const v3, 0x7f0f013e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateSplitCircle:Landroid/widget/ImageView;

    .line 186
    const v3, 0x7f0f013c

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandle:Landroid/widget/ImageView;

    .line 187
    const v3, 0x7f0f013d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandleClosed:Landroid/widget/ImageView;

    .line 188
    const v3, 0x7f0f0100

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame:Landroid/widget/ImageView;

    .line 189
    const v3, 0x7f0f0101

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame2:Landroid/widget/ImageView;

    .line 191
    const v3, 0x7f0f004b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTemplateBtn:Landroid/widget/ImageButton;

    .line 192
    const v3, 0x7f0f004d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateEditBtn:Landroid/widget/ImageButton;

    .line 193
    const v3, 0x7f0f004f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHelpBtn:Landroid/widget/ImageButton;

    .line 194
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateEditBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 195
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHelpBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 197
    const v3, 0x7f0f0139

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon1:Landroid/widget/ImageView;

    .line 198
    const v3, 0x7f0f013a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon2:Landroid/widget/ImageView;

    .line 199
    const v3, 0x7f0f0141

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppPaired:Landroid/widget/ImageView;

    .line 201
    const v3, 0x7f0f0135

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelper:Landroid/widget/RelativeLayout;

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelper:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 203
    const v3, 0x7f0f0143

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketBtnLayout:Landroid/widget/RelativeLayout;

    .line 204
    const v3, 0x7f0f004a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketBody:Landroid/widget/RelativeLayout;

    .line 205
    const v3, 0x7f0f0142

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    .line 206
    const v3, 0x7f0f0146

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup1:Landroid/widget/RelativeLayout;

    .line 207
    const v3, 0x7f0f014a

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;

    .line 208
    const v3, 0x7f0f014e

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup3:Landroid/widget/RelativeLayout;

    .line 209
    const v3, 0x7f0f0149

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2Parent:Landroid/widget/RelativeLayout;

    .line 210
    const v3, 0x7f0f014d

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup3Parent:Landroid/widget/RelativeLayout;

    .line 211
    const v3, 0x7f0f0151

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup4:Landroid/widget/RelativeLayout;

    .line 213
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mResouce:Landroid/content/res/Resources;

    const v4, 0x7f0a0019

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->moveBtnHeight:I

    .line 214
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mResouce:Landroid/content/res/Resources;

    const v4, 0x7f0a006b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I

    .line 215
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mResouce:Landroid/content/res/Resources;

    const v4, 0x7f0a0068

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openPocketTrayHeght:I

    .line 217
    const v3, 0x7f0f0136

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateFramesApp:Landroid/widget/LinearLayout;

    .line 218
    const v3, 0x7f0f00ff

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateFramesFocusLayout:Landroid/widget/LinearLayout;

    .line 219
    const v3, 0x7f0f013f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    .line 220
    const v3, 0x7f0f0140

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer2:Landroid/widget/LinearLayout;

    .line 221
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 222
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer2:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 224
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

    const v4, 0x7f020084

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 226
    const/4 v3, 0x0

    sput-boolean v3, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtnPushed:Z

    .line 228
    const v3, 0x7f0f014b

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;

    .line 229
    const v3, 0x7f0f014f

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue3:Landroid/widget/ImageView;

    .line 231
    const v3, 0x7f040024

    invoke-static {p0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 232
    const v3, 0x7f040052

    invoke-static {p0, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;

    .line 234
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020086

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 236
    .local v0, "drawable1":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020087

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 239
    .local v1, "drawable2":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->drs:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->drs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mResouce:Landroid/content/res/Resources;

    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/FolderIconHelper;->initFolderResources(Landroid/content/res/Resources;)V

    .line 244
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->setListener()V

    .line 246
    return-void
.end method

.method public invalidTouch()V
    .locals 2

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mInvalidToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 480
    const v0, 0x7f08003a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mInvalidToast:Landroid/widget/Toast;

    .line 482
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mInvalidToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 483
    return-void
.end method

.method public makeScreenForLandscape()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 486
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 487
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 488
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 490
    :cond_0
    iget v7, v0, Landroid/graphics/Point;->x:I

    div-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    .line 491
    iget v7, v0, Landroid/graphics/Point;->y:I

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 492
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateFramesApp:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 493
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateFramesFocusLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 495
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 497
    .local v5, "mTryMWTemplateHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 499
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v7, v5, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 500
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v7, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 502
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 504
    .local v2, "mTryMWTemplateApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 505
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v7, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 506
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 508
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 510
    .local v4, "mTryMWTemplateFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getSizeOfStatusBar()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 511
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v7, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 512
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 514
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 516
    .local v3, "mTryMWTemplateFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getSizeOfStatusBar()I

    move-result v8

    sub-int/2addr v7, v8

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 517
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v7, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 518
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v7, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 521
    sget-boolean v7, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtnPushed:Z

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    .line 522
    invoke-direct {p0, v9, v9, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->updatePocketTrayLayout(IZZ)V

    .line 535
    :goto_0
    return-void

    .line 524
    :cond_1
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 525
    .local v1, "displaySizeTemp":Landroid/graphics/Point;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 526
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 528
    :cond_2
    iget v7, v1, Landroid/graphics/Point;->y:I

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 529
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    iget v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    .line 530
    new-instance v6, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 532
    .local v6, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    invoke-virtual {v6, v9, v7, v9, v9}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 533
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v8, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public makeScreenForPortrait()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 538
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelper:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    .line 539
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v6, v7}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 540
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateFramesApp:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 541
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateFramesFocusLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 543
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout$LayoutParams;

    .line 545
    .local v4, "mTryMWTemplateHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 546
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v6, v4, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 547
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHome:Landroid/widget/ImageView;

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 549
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 551
    .local v1, "mTryMWTemplateApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 552
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v6, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 553
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

    invoke-virtual {v6, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 555
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 557
    .local v3, "mTryMWTemplateFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 558
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v6, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 559
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 561
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 563
    .local v2, "mTryMWTemplateFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowTemplateHelperWidthCenter:I

    iput v6, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 564
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v6, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 565
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 567
    sget-boolean v6, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtnPushed:Z

    if-ne v6, v9, :cond_0

    .line 568
    invoke-direct {p0, v8, v8, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->updatePocketTrayLayout(IZZ)V

    .line 581
    :goto_0
    return-void

    .line 570
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 571
    .local v0, "displaySizeTemp":Landroid/graphics/Point;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 572
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 574
    :cond_1
    iget v6, v0, Landroid/graphics/Point;->y:I

    iput v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 575
    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I

    sub-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    .line 576
    new-instance v5, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 578
    .local v5, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    invoke-virtual {v5, v8, v6, v8, v8}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 579
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 593
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 595
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mOrientation:I

    .line 596
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mOrientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 597
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->makeScreenForPortrait()V

    .line 600
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 601
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->makeScreenForLandscape()V

    .line 603
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 109
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getWindow()Landroid/view/Window;

    move-result-object v3

    .line 111
    .local v3, "w":Landroid/view/Window;
    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 112
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 113
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v4, v4, 0x400

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 114
    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 116
    const v4, 0x7f03002d

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->setContentView(I)V

    .line 117
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->init()V

    .line 119
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    const-wide/16 v6, 0x12c

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 120
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    new-instance v5, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$1;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    invoke-virtual {v4, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 132
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->GUI_SCALE_PREVIEW:Z

    if-eqz v4, :cond_1

    .line 133
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 134
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 135
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 137
    :cond_0
    iget v4, v0, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    .line 138
    iget v4, v0, Landroid/graphics/Point;->y:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 139
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->openButtonHeight:I

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    .line 140
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 142
    .local v2, "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->pocketTopMargin:I

    invoke-virtual {v2, v8, v4, v8, v8}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 143
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 144
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 146
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTray:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 147
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandle:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 148
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateHandleClosed:Landroid/widget/ImageView;

    invoke-virtual {v4, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMWViewImageContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 150
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup1:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 151
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 152
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateCue2:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mFadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 153
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 154
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup2:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 158
    .end local v0    # "displaySize":Landroid/graphics/Point;
    .end local v2    # "pocketTrayLayoutParam":Landroid/view/ViewGroup$MarginLayoutParams;
    :goto_0
    return-void

    .line 156
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplatePopup1:Landroid/widget/RelativeLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 163
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 164
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->makeScreenForPortrait()V

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 168
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->makeScreenForLandscape()V

    .line 170
    :cond_1
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 608
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 615
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 610
    :pswitch_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->invalidTouch()V

    goto :goto_0

    .line 608
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public setListener()V
    .locals 2

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketOpenBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 454
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketBtnLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketTemplateOpenListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiwindowtemplateApp:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowtemplateTemplateBtn:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketTemplateCreateBtnListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 457
    return-void
.end method

.method public startPocketOpenAnimation()V
    .locals 2

    .prologue
    .line 412
    const/4 v0, 0x0

    .line 413
    .local v0, "mPocketOpenAnimation":Landroid/view/animation/Animation;
    const v1, 0x7f04004a

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 414
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mPocketLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 416
    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$7;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$7;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 430
    return-void
.end method

.method public trytemplateAnimationTemplate()V
    .locals 28

    .prologue
    .line 250
    const/4 v6, 0x0

    .line 251
    .local v6, "distance1X":F
    const/4 v15, 0x0

    .line 252
    .local v15, "distance2X":F
    const/4 v10, 0x0

    .line 253
    .local v10, "distance1Y":F
    const/16 v19, 0x0

    .line 255
    .local v19, "distance2Y":F
    const/16 v26, 0x0

    .line 256
    .local v26, "mDisplayAppIconX":I
    const/16 v27, 0x0

    .line 257
    .local v27, "mDisplayAppIconY":I
    const/16 v24, 0x0

    .line 258
    .local v24, "mDisplayApp2IconX":I
    const/16 v25, 0x0

    .line 260
    .local v25, "mDisplayApp2IconY":I
    new-instance v23, Landroid/graphics/Point;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Point;-><init>()V

    .line 261
    .local v23, "displaySize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 262
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    move-object/from16 v0, v23

    invoke-virtual {v3, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 264
    :cond_0
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    .line 265
    move-object/from16 v0, v23

    iget v3, v0, Landroid/graphics/Point;->y:I

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 268
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    div-int/lit8 v26, v3, 0x2

    .line 269
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    div-int/lit8 v24, v3, 0x2

    .line 270
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    div-int/lit8 v27, v3, 0x4

    .line 271
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v25, v3, 0x3

    .line 273
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 274
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    div-int/lit8 v26, v3, 0x4

    .line 275
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x4

    mul-int/lit8 v24, v3, 0x3

    .line 276
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    div-int/lit8 v27, v3, 0x2

    .line 277
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mDisplayHeight:I

    div-int/lit8 v25, v3, 0x2

    .line 280
    :cond_2
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x0

    move/from16 v0, v26

    int-to-float v4, v0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move/from16 v0, v27

    int-to-float v8, v0

    const/4 v9, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 283
    .local v2, "translateAnimation1":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 284
    new-instance v3, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 286
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    const/4 v12, 0x0

    move/from16 v0, v24

    int-to-float v13, v0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v11 .. v19}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 289
    .local v11, "translateAnimation2":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x3e8

    invoke-virtual {v11, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 290
    new-instance v3, Landroid/view/animation/AnticipateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AnticipateInterpolator;-><init>()V

    invoke-virtual {v11, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 292
    new-instance v20, Landroid/view/animation/AlphaAnimation;

    const/high16 v3, 0x3f800000    # 1.0f

    const v4, 0x3e4ccccd    # 0.2f

    move-object/from16 v0, v20

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 293
    .local v20, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 294
    const-wide/16 v4, 0x1f4

    move-object/from16 v0, v20

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 296
    new-instance v21, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v21

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 297
    .local v21, "animationSet1":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 298
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 300
    new-instance v22, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 301
    .local v22, "animationSet2":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 302
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 304
    new-instance v3, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$2;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 317
    new-instance v3, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$3;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate$3;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;)V

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 331
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon1:Landroid/widget/ImageView;

    move-object/from16 v0, v21

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 332
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTemplate;->mTryMultiWindowAppIcon2:Landroid/widget/ImageView;

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 333
    return-void
.end method
