.class Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "SwitchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SwitchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BorderDrawable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SwitchView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V
    .locals 0

    .prologue
    .line 830
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "c"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v2, 0x0

    .line 833
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 834
    .local v0, "bounds":Landroid/graphics/Rect;
    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 835
    iput v2, v0, Landroid/graphics/Rect;->left:I

    .line 836
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 837
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 839
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 840
    .local v1, "drawingRect":Landroid/graphics/Rect;
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 841
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 842
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBorderLineColor:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1600(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 844
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 845
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 846
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBorderLineColor:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1600(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 848
    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v4

    add-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 849
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 850
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBorderLineColor:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1600(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 852
    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v4

    sub-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 853
    sget-object v2, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 854
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;->this$0:Lcom/sec/android/app/FlashBarService/SwitchView;

    # getter for: Lcom/sec/android/app/FlashBarService/SwitchView;->mBorderLineColor:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->access$1600(Lcom/sec/android/app/FlashBarService/SwitchView;)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 855
    return-void
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 867
    const/4 v0, 0x0

    return v0
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "alpha"    # I

    .prologue
    .line 859
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 863
    return-void
.end method
