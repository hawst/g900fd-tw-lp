.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 1572
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 15
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1576
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v11

    const/4 v12, -0x1

    if-le v11, v12, :cond_0

    .line 1577
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    iget-object v13, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-boolean v13, v13, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSupportMultiInstance:Z

    const/16 v14, 0x64

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v2

    .line 1578
    .local v2, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1681
    .end local v2    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_0
    :goto_0
    return-void

    .line 1581
    .restart local v2    # "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    :cond_1
    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v11, 0x0

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v12, v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4402(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1582
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntents:Ljava/util/List;
    invoke-static {v11, v2}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4502(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Ljava/util/List;)Ljava/util/List;

    .line 1585
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    .line 1586
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    invoke-static {v11}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyle(Landroid/content/Context;)Z

    move-result v11

    if-nez v11, :cond_11

    .line 1587
    new-instance v9, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v11, 0x1

    invoke-direct {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 1588
    .local v9, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v12, 0x64

    const/4 v13, 0x2

    invoke-virtual {v11, v12, v13}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v7

    .line 1589
    .local v7, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v6, 0x0

    .line 1590
    .local v6, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v7, :cond_4

    .line 1591
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1592
    .local v8, "visibleTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v11, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v11

    if-nez v11, :cond_2

    iget-object v11, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v12, 0x400

    invoke-virtual {v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v11

    if-nez v11, :cond_2

    .line 1594
    iget-object v11, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v11

    if-nez v11, :cond_3

    iget-object v11, v8, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v11

    const/4 v12, 0x1

    if-eq v11, v12, :cond_2

    .line 1598
    :cond_3
    move-object v6, v8

    .line 1603
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v8    # "visibleTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_4
    if-nez v6, :cond_5

    .line 1604
    if-eqz v7, :cond_a

    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_a

    .line 1605
    const/4 v11, 0x0

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1615
    .restart local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_5
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v11

    if-gtz v11, :cond_6

    .line 1616
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Intent;->getFlags()I

    move-result v12

    const v13, -0x8000001

    and-int/2addr v12, v13

    invoke-virtual {v11, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1619
    :cond_6
    const/4 v4, 0x0

    .line 1620
    .local v4, "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const/4 v1, 0x0

    .line 1622
    .local v1, "isNextZoneSamePackage":Z
    iget-object v11, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v10

    .line 1623
    .local v10, "zone":I
    if-eqz v10, :cond_7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    const/4 v12, 0x2

    if-lt v11, v12, :cond_7

    .line 1624
    const/4 v11, 0x1

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1627
    .restart local v4    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_7
    iget-object v11, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v12, 0x2

    invoke-virtual {v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 1628
    if-eqz v4, :cond_8

    .line 1629
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppIconIndex:I
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4300(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v5

    .line 1630
    .local v5, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v11, v5, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v11, v11, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v12, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v12}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1631
    const/4 v1, 0x1

    .line 1635
    .end local v5    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_8
    iget-object v11, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v11

    const/4 v12, 0x3

    if-ne v11, v12, :cond_c

    .line 1636
    if-eqz v1, :cond_b

    .line 1637
    const/4 v11, 0x3

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1650
    :goto_1
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getFlags()I

    move-result v11

    const/high16 v12, 0x8000000

    and-int/2addr v11, v12

    if-eqz v11, :cond_9

    .line 1651
    const/high16 v11, 0x80000

    const/4 v12, 0x1

    invoke-virtual {v9, v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 1653
    :cond_9
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1662
    :goto_2
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v12

    const/4 v13, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static {v11, v12, v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 1607
    .end local v1    # "isNextZoneSamePackage":Z
    .end local v4    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v10    # "zone":I
    :cond_a
    const/4 v11, 0x1

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 1608
    const/16 v11, 0xf

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1609
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1610
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v12

    const/4 v13, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static {v11, v12, v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 1639
    .restart local v1    # "isNextZoneSamePackage":Z
    .restart local v4    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v10    # "zone":I
    :cond_b
    const/16 v11, 0xc

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 1641
    :cond_c
    iget-object v11, v6, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v11

    const/16 v12, 0xc

    if-ne v11, v12, :cond_e

    .line 1642
    if-eqz v1, :cond_d

    .line 1643
    const/16 v11, 0xc

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 1645
    :cond_d
    const/4 v11, 0x3

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 1648
    :cond_e
    const/16 v11, 0xc

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 1655
    :cond_f
    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 1656
    const/4 v11, 0x0

    invoke-virtual {v9, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 1657
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getFlags()I

    move-result v11

    const/high16 v12, 0x8000000

    and-int/2addr v11, v12

    if-eqz v11, :cond_10

    .line 1658
    const/high16 v11, 0x80000

    const/4 v12, 0x1

    invoke-virtual {v9, v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 1660
    :cond_10
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11, v9}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    goto :goto_2

    .line 1664
    .end local v1    # "isNextZoneSamePackage":Z
    .end local v4    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v6    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v7    # "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v9    # "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    .end local v10    # "zone":I
    :cond_11
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1665
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    const/4 v12, 0x0

    const/4 v13, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static {v11, v12, v13}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$5700(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;Landroid/content/Intent;Z)V

    .line 1667
    new-instance v3, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v11, 0x2

    invoke-direct {v3, v11}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 1668
    .local v3, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v11}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v11

    if-gtz v11, :cond_12

    .line 1669
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {v12}, Landroid/content/Intent;->getFlags()I

    move-result v12

    const v13, -0x8000001

    and-int/2addr v12, v13

    invoke-virtual {v11, v12}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1671
    :cond_12
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Intent;->getFlags()I

    move-result v11

    const/high16 v12, 0x8000000

    and-int/2addr v11, v12

    if-eqz v11, :cond_13

    .line 1672
    const/high16 v11, 0x80000

    const/4 v12, 0x1

    invoke-virtual {v3, v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 1674
    :cond_13
    const/16 v11, 0x800

    const/4 v12, 0x1

    invoke-virtual {v3, v11, v12}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 1675
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v11}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v11

    invoke-virtual {v11, v3}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 1676
    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v11, v11, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$24;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v12}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$4400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/content/Intent;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/app/FlashBarService/AppListController;->startActivitySafe(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
