.class final Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;
.super Landroid/widget/BaseAdapter;
.source "RecentsPanelView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "TaskDescriptionAdapter"
.end annotation


# instance fields
.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 126
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 127
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 142
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentItemLayoutId:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$100(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)I

    move-result v3

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 143
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;-><init>()V

    .line 144
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    const v2, 0x7f0f00e2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    .line 148
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$200(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getDefaultThumbnail()Landroid/graphics/Bitmap;

    move-result-object v3

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V
    invoke-static {v2, v1, v3, v4, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$300(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V

    .line 149
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->iconView:Landroid/widget/ImageView;

    .line 150
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->iconView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$200(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getDefaultIcon()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 151
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->labelView:Landroid/widget/TextView;

    .line 152
    const v2, 0x7f0f00e1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->deleteView:Landroid/widget/ImageView;

    .line 154
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 155
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mItemWidth:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)I

    move-result v2

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mItemHeight:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$500(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)I

    move-result v3

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    .line 157
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 138
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 161
    if-nez p2, :cond_0

    .line 162
    invoke-virtual {p0, p3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->createView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 164
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    .line 167
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    move v1, p1

    .line 169
    .local v1, "index":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTaskDescriptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 171
    .local v2, "td":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 173
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->isLoaded()Z

    move-result v3

    iput-boolean v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->loadedThumbnailAndIcon:Z

    .line 174
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->isLoaded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 175
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getThumbnail()Landroid/graphics/Bitmap;

    move-result-object v4

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V
    invoke-static {v3, v0, v4, v6, v5}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$300(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V

    .line 176
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateIcon(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/drawable/Drawable;ZZ)V
    invoke-static {v3, v0, v4, v6, v5}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$600(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/drawable/Drawable;ZZ)V

    .line 179
    :cond_1
    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 180
    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->taskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 181
    return-object p2
.end method

.method public recycleView(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 185
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;

    .line 187
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$TaskDescriptionAdapter;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->mRecentTasksLoader:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$200(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getDefaultThumbnail()Landroid/graphics/Bitmap;

    move-result-object v2

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->updateThumbnail(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V
    invoke-static {v1, v0, v2, v3, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->access$300(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;Landroid/graphics/Bitmap;ZZ)V

    .line 188
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->thumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 189
    iput-object v4, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->taskDescription:Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .line 190
    iput-boolean v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView$ViewHolder;->loadedThumbnailAndIcon:Z

    .line 191
    return-void
.end method
