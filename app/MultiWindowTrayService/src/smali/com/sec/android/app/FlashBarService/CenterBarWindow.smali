.class public Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;,
        Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;,
        Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;
    }
.end annotation


# static fields
.field private static final DOUBLE_TAP_MIN_TIME:I


# instance fields
.field private final CENTER_BAR_DIRECTION_BL:S

.field private final CENTER_BAR_DIRECTION_BOTTOM:S

.field private final CENTER_BAR_DIRECTION_BR:S

.field private final CENTER_BAR_DIRECTION_LEFT:S

.field private final CENTER_BAR_DIRECTION_RIGHT:S

.field private final CENTER_BAR_DIRECTION_TL:S

.field private final CENTER_BAR_DIRECTION_TOP:S

.field private final CENTER_BAR_DIRECTION_TR:S

.field private final DEBUG:Z

.field private final ESTIMATE_INVALID_VALUE:S

.field private final MAX_TASKS:I

.field private final WINDOW_PORTRAIT_MODE:S

.field private centerBarButtonSoundId:I

.field private mActionCancel:Z

.field private mActivityManager:Landroid/app/ActivityManager;

.field private mAnimationPlaying:Z

.field private mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

.field mAppSwitchingAnimIcon:Landroid/widget/ImageView;

.field private mAvailableAnims:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mAvailableButtons:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field mButtonsPopupWindow:Landroid/widget/PopupWindow;

.field private mCenterBar:Landroid/widget/ImageView;

.field private mCenterBarButtonCloseAnimating:Z

.field private mCenterBarButtonOpenAnimating:Z

.field private mCenterBarButtons:Landroid/view/View;

.field private mCenterBarDirection:I

.field private mCenterBarDockingSize:I

.field mCenterBarEventReceiver:Landroid/content/BroadcastReceiver;

.field private mCenterBarFlingNonQuadRatio:I

.field private mCenterBarFlingProportion:I

.field private mCenterBarFlingSizeH:I

.field private mCenterBarFlingSizeW:I

.field private mCenterBarFrame:Landroid/widget/FrameLayout;

.field private mCenterBarGestureDetector:Landroid/view/GestureDetector;

.field mCenterBarHoverListener:Landroid/view/View$OnHoverListener;

.field private mCenterBarPoint:Landroid/graphics/Point;

.field private mCenterBarSize:I

.field private mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

.field private mCenterbarCueTintColor:I

.field private mCenterbarTintColor:I

.field mCloseAnimIcon:Landroid/widget/ImageView;

.field mCloseButton:Landroid/view/View;

.field mCollapseButton:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mCurrentArrangeState:I

.field private mDisplayHeight:I

.field private mDisplayId:I

.field private mDisplayOrientation:I

.field private mDisplayWidth:I

.field private mDoubleTabbed:Z

.field private mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

.field mDragAnimIcon:Landroid/widget/ImageView;

.field mDragButton:Landroid/view/View;

.field mExternalAnimIcon:Landroid/widget/ImageView;

.field mExternalButton:Landroid/view/View;

.field private mFocusZoneInfo:I

.field mFullAnimIcon:Landroid/widget/ImageView;

.field mFullButton:Landroid/view/View;

.field private mFullSizeDocking:Z

.field private mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

.field private mHelpHubVersion:I

.field private mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIsCenterBarFixed:Z

.field private mIsCenterBarMoved:Z

.field private mIsCenterBarPressed:Z

.field private mIsCrossRect:Z

.field private mIsDragAndDropCheckBoxChecked:Z

.field private mIsDragAndDropStart:Z

.field private mIsDragMode:Z

.field private mIsFirst:Z

.field private mIsInLongPress:Z

.field private mIsNewHelpPopup:Z

.field private mIsShowButton:Z

.field private mIvt:[B

.field private mKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

.field private mLastBackgroundLandscape:I

.field private mLastBackgroundPortait:I

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field mRecentsButton:Landroid/view/View;

.field private mRefreshAfterAnimation:Z

.field private mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mSoundPool:Landroid/media/SoundPool;

.field mSplitMaxWeight:F

.field mSplitMinWeight:F

.field private mStartCenterBarPoint:Landroid/graphics/Point;

.field mSupportQuadView:Z

.field mSwitchAnimIcon:Landroid/widget/ImageView;

.field mSwitchButton:Landroid/view/View;

.field mTimerHandler:Landroid/os/Handler;

.field private mTouchEventX:I

.field private mTouchEventY:I

.field mTouchListener:Landroid/view/View$OnTouchListener;

.field private mTouchStartX:I

.field private mTouchStartY:I

.field private mTrayBarDisabledFunctions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTrayBarEnabledFunctions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVibrator:Landroid/os/SystemVibrator;

.field private mViewForLayout:Landroid/view/View;

.field private mWM:Landroid/view/IWindowManager;

.field private mWindowCenterBar:Landroid/view/Window;

.field mWindowGuideLine:Landroid/view/Window;

.field private mWindowManager:Landroid/view/WindowManager;

.field mbExitButton:Z

.field mbSideSyncButton:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 117
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapMinTime()I

    move-result v0

    sput v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->DOUBLE_TAP_MIN_TIME:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 249
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 119
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->DEBUG:Z

    .line 121
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->WINDOW_PORTRAIT_MODE:S

    .line 123
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_TOP:S

    .line 124
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_BOTTOM:S

    .line 125
    const/4 v13, 0x2

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_RIGHT:S

    .line 126
    const/4 v13, 0x3

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_LEFT:S

    .line 127
    const/4 v13, 0x4

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_TL:S

    .line 128
    const/4 v13, 0x5

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_TR:S

    .line 129
    const/4 v13, 0x6

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_BL:S

    .line 130
    const/4 v13, 0x7

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->CENTER_BAR_DIRECTION_BR:S

    .line 131
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput-short v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->ESTIMATE_INVALID_VALUE:S

    .line 133
    const/16 v13, 0x64

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->MAX_TASKS:I

    .line 136
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 137
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 149
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;

    .line 168
    const/16 v13, 0x14

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingProportion:I

    .line 173
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z

    .line 174
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullSizeDocking:Z

    .line 175
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z

    .line 177
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    .line 178
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    .line 188
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropCheckBoxChecked:Z

    .line 195
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayId:I

    .line 196
    const/16 v13, 0x12

    new-array v13, v13, [B

    fill-array-data v13, :array_0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIvt:[B

    .line 219
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mbExitButton:Z

    .line 220
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mbSideSyncButton:Z

    .line 221
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSupportQuadView:Z

    .line 223
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarFixed:Z

    .line 224
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    .line 225
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    .line 228
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    .line 233
    const v13, 0x7f02006a

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    .line 236
    const v13, 0x7f02006a

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 239
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mHelpHubVersion:I

    .line 243
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerBarButtonSoundId:I

    .line 365
    new-instance v13, Lcom/sec/android/app/FlashBarService/CenterBarWindow$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 375
    new-instance v13, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarEventReceiver:Landroid/content/BroadcastReceiver;

    .line 1011
    new-instance v13, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 1274
    new-instance v13, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchListener:Landroid/view/View$OnTouchListener;

    .line 2082
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonCloseAnimating:Z

    .line 2083
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonOpenAnimating:Z

    .line 2978
    new-instance v13, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    .line 252
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    .line 253
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 271
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v13

    const-string v14, "CscFeature_Framework_ConfigMultiWindowTrayBarFunction"

    invoke-virtual {v13, v14}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 273
    .local v11, "traybarFunction":Ljava/lang/String;
    if-eqz v11, :cond_2

    .line 274
    new-instance v12, Ljava/util/ArrayList;

    const-string v13, "\\,"

    invoke-virtual {v11, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 275
    .local v12, "traybarFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 276
    .local v6, "function":Ljava/lang/String;
    const-string v13, "+"

    invoke-virtual {v6, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 277
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 278
    :cond_1
    const-string v13, "-"

    invoke-virtual {v6, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 279
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 286
    .end local v6    # "function":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    .end local v12    # "traybarFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const/high16 v14, 0x7f080000

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 287
    .local v3, "centerbarButtonType":Ljava/lang/String;
    if-eqz v3, :cond_5

    .line 288
    new-instance v4, Ljava/util/ArrayList;

    const-string v13, "\\,"

    invoke-virtual {v3, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v13

    invoke-direct {v4, v13}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 289
    .local v4, "centerbarButtonTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .restart local v7    # "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 290
    .restart local v6    # "function":Ljava/lang/String;
    const-string v13, "+"

    invoke-virtual {v6, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 291
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 292
    :cond_4
    const-string v13, "-"

    invoke-virtual {v6, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 293
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-virtual {v14, v15}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 300
    .end local v4    # "centerbarButtonTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v6    # "function":Ljava/lang/String;
    .end local v7    # "i$":Ljava/util/Iterator;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const-string v14, "window"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/WindowManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 301
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const-string v14, "activity"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/app/ActivityManager;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActivityManager:Landroid/app/ActivityManager;

    .line 302
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const-string v14, "vibrator"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/SystemVibrator;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;

    .line 303
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const-string v14, "multiwindow_facade"

    invoke-virtual {v13, v14}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 305
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mKnoxCustomManager:Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    .line 307
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    .line 308
    const/4 v13, -0x1

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    .line 309
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    .line 310
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    .line 311
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 312
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 313
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I

    .line 314
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I

    .line 315
    const/4 v13, 0x1

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsFirst:Z

    .line 316
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    .line 317
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragMode:Z

    .line 318
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropStart:Z

    .line 319
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarMoved:Z

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCurrentArrangeState:I

    .line 321
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0001

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    .line 322
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0a0013

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v13

    float-to-int v13, v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDockingSize:I

    .line 323
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f07000e

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingNonQuadRatio:I

    .line 324
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f090001

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsNewHelpPopup:Z

    .line 325
    const-string v13, "window"

    invoke-static {v13}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v13

    invoke-static {v13}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWM:Landroid/view/IWindowManager;

    .line 328
    :try_start_0
    invoke-static {}, Landroid/app/ActivityThread;->getPackageManager()Landroid/content/pm/IPackageManager;

    move-result-object v10

    .line 329
    .local v10, "pm":Landroid/content/pm/IPackageManager;
    const-string v13, "com.sec.feature.multiwindow.quadview"

    invoke-interface {v10, v13}, Landroid/content/pm/IPackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSupportQuadView:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 334
    .end local v10    # "pm":Landroid/content/pm/IPackageManager;
    :goto_2
    new-instance v9, Landroid/content/IntentFilter;

    invoke-direct {v9}, Landroid/content/IntentFilter;-><init>()V

    .line 335
    .local v9, "intentFilter":Landroid/content/IntentFilter;
    const-string v13, "ResponseAxT9Info"

    invoke-virtual {v9, v13}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 336
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v13, v14, v9}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 338
    new-instance v2, Landroid/content/IntentFilter;

    const-string v13, "com.sec.android.action.ARRANGE_CONTROLL_BAR"

    invoke-direct {v2, v13}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 339
    .local v2, "centerBarArrangeFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v13, v14, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 343
    :try_start_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    const-string v14, "com.samsung.helphub"

    const/4 v15, 0x0

    invoke-virtual {v13, v14, v15}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v8

    .line 344
    .local v8, "info":Landroid/content/pm/PackageInfo;
    iget v13, v8, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v13, v13, 0xa

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mHelpHubVersion:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 349
    .end local v8    # "info":Landroid/content/pm/PackageInfo;
    :goto_3
    new-instance v13, Landroid/media/SoundPool;

    const/4 v14, 0x1

    const/4 v15, 0x1

    const/16 v16, 0x0

    invoke-direct/range {v13 .. v16}, Landroid/media/SoundPool;-><init>(III)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSoundPool:Landroid/media/SoundPool;

    .line 350
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSoundPool:Landroid/media/SoundPool;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const/high16 v15, 0x7f050000

    const/16 v16, 0x1

    invoke-virtual/range {v13 .. v16}, Landroid/media/SoundPool;->load(Landroid/content/Context;II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerBarButtonSoundId:I

    .line 352
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v13}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getSplitMinWeight()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSplitMinWeight:F

    .line 353
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v13}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getSplitMaxWeight()F

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSplitMaxWeight:F

    .line 355
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V

    .line 358
    sget-boolean v13, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_TRAYBAR_UI:Z

    if-nez v13, :cond_6

    .line 359
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f060006

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarTintColor:I

    .line 360
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f060007

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getColor(I)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarCueTintColor:I

    .line 363
    :cond_6
    return-void

    .line 345
    :catch_0
    move-exception v5

    .line 346
    .local v5, "e":Ljava/lang/Exception;
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mHelpHubVersion:I

    goto :goto_3

    .line 330
    .end local v2    # "centerBarArrangeFilter":Landroid/content/IntentFilter;
    .end local v5    # "e":Ljava/lang/Exception;
    .end local v9    # "intentFilter":Landroid/content/IntentFilter;
    :catch_1
    move-exception v13

    goto/16 :goto_2

    .line 196
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
    .end array-data
.end method

.method static synthetic access$002(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    return p1
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showButtonPopupWindow(Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startHideButtonTimer()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarFixed:Z

    return v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z

    return v0
.end method

.method static synthetic access$1602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayId:I

    return v0
.end method

.method static synthetic access$1702(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayId:I

    return p1
.end method

.method static synthetic access$1802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarPressed:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarMoved:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarMoved:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setCenterBarPoint()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I

    return v0
.end method

.method static synthetic access$2002(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartX:I

    return p1
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchStartY:I

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    return-object v0
.end method

.method static synthetic access$2302(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCurrentArrangeState:I

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 105
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawGuideCenterBar(II)V

    return-void
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mStartCenterBarPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z

    return v0
.end method

.method static synthetic access$2602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z

    return p1
.end method

.method static synthetic access$2702(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCrossRect:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDoubleTabbed:Z

    return v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDoubleTabbed:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Landroid/graphics/Point;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getCenterBarEdgeArea(Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerBarFullSizeDocking(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->findCandidateFocus()I

    move-result v0

    return v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    return v0
.end method

.method static synthetic access$3202(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    return p1
.end method

.method static synthetic access$3300()I
    .locals 1

    .prologue
    .line 105
    sget v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->DOUBLE_TAP_MIN_TIME:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$3500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    return v0
.end method

.method static synthetic access$3600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    return v0
.end method

.method static synthetic access$3802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # I

    .prologue
    .line 105
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    return p1
.end method

.method static synthetic access$3900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showButtonPopupWindow()V

    return-void
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showDragAndDropHelpDialog()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startDragAndDrop()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->forceHideInputMethod()V

    return-void
.end method

.method static synthetic access$4500(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/app/ActivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActivityManager:Landroid/app/ActivityManager;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animateCenterBarClose(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$4702(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonOpenAnimating:Z

    return p1
.end method

.method static synthetic access$4802(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonCloseAnimating:Z

    return p1
.end method

.method static synthetic access$4900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAnimationPlaying:Z

    return p1
.end method

.method static synthetic access$5102(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropCheckBoxChecked:Z

    return p1
.end method

.method static synthetic access$5200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropStart:Z

    return v0
.end method

.method static synthetic access$5202(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropStart:Z

    return p1
.end method

.method static synthetic access$5300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRefreshAfterAnimation:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRefreshAfterAnimation:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarCueTintColor:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarTintColor:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    return-object v0
.end method

.method private animateCenterBarClose(Landroid/view/View;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2159
    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonCloseAnimating:Z

    if-eqz v8, :cond_1

    .line 2160
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v8

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v8

    if-nez v8, :cond_1

    .line 2209
    :cond_0
    return-void

    .line 2165
    :cond_1
    iput-boolean v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonCloseAnimating:Z

    .line 2166
    iput-boolean v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonOpenAnimating:Z

    .line 2168
    const v8, 0x7f0f0074

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 2169
    .local v4, "leftBackground":Landroid/view/View;
    const v8, 0x7f0f0076

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 2170
    .local v7, "rightBackground":Landroid/view/View;
    const v8, 0x7f0f0075

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2172
    .local v1, "centerBackground":Landroid/view/View;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v11, :cond_4

    .line 2173
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f040027

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2177
    :goto_0
    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getButtonAnimation(ZZ)Landroid/view/animation/TranslateAnimation;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2178
    invoke-direct {p0, v10, v10}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getButtonAnimation(ZZ)Landroid/view/animation/TranslateAnimation;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2180
    const/4 v3, 0x0

    .line 2181
    .local v3, "left":I
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v6, v8, -0x1

    .line 2182
    .local v6, "right":I
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    rem-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    add-int/lit8 v2, v8, -0x1

    .line 2183
    .local v2, "closer":I
    const/4 v5, 0x0

    .line 2185
    .local v5, "offset":I
    :goto_1
    if-gt v3, v6, :cond_0

    .line 2186
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f04002c

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2187
    .local v0, "a":Landroid/view/animation/Animation;
    mul-int/lit16 v8, v5, 0xc8

    int-to-long v8, v8

    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 2188
    if-eq v3, v6, :cond_2

    .line 2189
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2191
    :cond_2
    if-ne v3, v2, :cond_3

    .line 2192
    new-instance v8, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;

    invoke-direct {v8, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$22;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v0, v8}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2206
    :cond_3
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2207
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v6, v6, -0x1

    add-int/lit8 v5, v5, 0x1

    .line 2208
    goto :goto_1

    .line 2175
    .end local v0    # "a":Landroid/view/animation/Animation;
    .end local v2    # "closer":I
    .end local v3    # "left":I
    .end local v5    # "offset":I
    .end local v6    # "right":I
    :cond_4
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v9, 0x7f040028

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private animateCenterBarOpen(Landroid/view/View;Z)V
    .locals 13
    .param p1, "view"    # Landroid/view/View;
    .param p2, "autoCloseAnim"    # Z

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 2086
    iget-boolean v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonOpenAnimating:Z

    if-eqz v9, :cond_1

    .line 2087
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v9

    if-nez v9, :cond_1

    .line 2156
    :cond_0
    return-void

    .line 2093
    :cond_1
    iput-boolean v12, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonCloseAnimating:Z

    .line 2094
    iput-boolean v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtonOpenAnimating:Z

    .line 2095
    const v9, 0x7f0f0074

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 2096
    .local v5, "leftBackground":Landroid/view/View;
    const v9, 0x7f0f0076

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 2097
    .local v8, "rightBackground":Landroid/view/View;
    const v9, 0x7f0f0075

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 2100
    .local v2, "centerBackground":Landroid/view/View;
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget v9, v9, Landroid/content/res/Configuration;->orientation:I

    if-ne v9, v11, :cond_4

    .line 2101
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f04002a

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 2105
    .local v1, "centerAnim":Landroid/view/animation/Animation;
    :goto_0
    new-instance v9, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;

    invoke-direct {v9, p0, p2, p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$20;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;ZLandroid/view/View;)V

    invoke-virtual {v1, v9}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2126
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2127
    invoke-direct {p0, v11, v11}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getButtonAnimation(ZZ)Landroid/view/animation/TranslateAnimation;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2128
    invoke-direct {p0, v11, v12}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getButtonAnimation(ZZ)Landroid/view/animation/TranslateAnimation;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2130
    const/4 v4, 0x0

    .line 2131
    .local v4, "left":I
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v7, v9, -0x1

    .line 2132
    .local v7, "right":I
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v10

    rem-int/lit8 v10, v10, 0x2

    add-int/2addr v9, v10

    add-int/lit8 v3, v9, -0x1

    .line 2133
    .local v3, "closer":I
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    rem-int/lit8 v9, v9, 0x2

    if-nez v9, :cond_5

    add-int/lit8 v6, v3, 0x1

    .line 2135
    .local v6, "offset":I
    :goto_1
    if-gt v4, v7, :cond_0

    .line 2136
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f04002b

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 2137
    .local v0, "a":Landroid/view/animation/Animation;
    mul-int/lit16 v9, v6, 0xc8

    int-to-long v10, v9

    invoke-virtual {v0, v10, v11}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 2138
    if-eq v4, v7, :cond_2

    .line 2139
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2141
    :cond_2
    if-ne v4, v3, :cond_3

    .line 2142
    new-instance v9, Lcom/sec/android/app/FlashBarService/CenterBarWindow$21;

    invoke-direct {v9, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$21;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v0, v9}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2153
    :cond_3
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    invoke-virtual {v9, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2154
    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v7, v7, -0x1

    add-int/lit8 v6, v6, -0x1

    .line 2155
    goto :goto_1

    .line 2103
    .end local v0    # "a":Landroid/view/animation/Animation;
    .end local v1    # "centerAnim":Landroid/view/animation/Animation;
    .end local v3    # "closer":I
    .end local v4    # "left":I
    .end local v6    # "offset":I
    .end local v7    # "right":I
    :cond_4
    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v10, 0x7f040029

    invoke-static {v9, v10}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .restart local v1    # "centerAnim":Landroid/view/animation/Animation;
    goto/16 :goto_0

    .restart local v3    # "closer":I
    .restart local v4    # "left":I
    .restart local v7    # "right":I
    :cond_5
    move v6, v3

    .line 2133
    goto :goto_1
.end method

.method private animationCenterBar3Buttons()V
    .locals 25

    .prologue
    .line 2549
    const/16 v16, 0x0

    .line 2550
    .local v16, "translateAnimation":Landroid/view/animation/Animation;
    const/16 v17, 0x0

    .line 2551
    .local v17, "translateAnimation1":Landroid/view/animation/Animation;
    const/16 v18, 0x0

    .line 2552
    .local v18, "translateAnimation2":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a000a

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v12, v0

    .line 2553
    .local v12, "buttonBgHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a000b

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v20

    move/from16 v0, v20

    float-to-int v11, v0

    .line 2555
    .local v11, "buttonArrangeMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0x2

    sub-int v13, v20, v21

    .line 2556
    .local v13, "fromX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0x2

    sub-int v14, v20, v21

    .line 2558
    .local v14, "fromY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    .local v15, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/view/View;

    .line 2559
    .local v19, "v":Landroid/view/View;
    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2562
    .end local v19    # "v":Landroid/view/View;
    :cond_0
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const v20, 0x3ecccccd    # 0.4f

    const/high16 v21, 0x3f800000    # 1.0f

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2564
    .local v6, "alphaAnimation":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_5

    .line 2565
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_3

    .line 2568
    :cond_1
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    .end local v16    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    mul-int/lit8 v22, v12, 0x3

    div-int/lit8 v22, v22, 0x2

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    sub-int v23, v23, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    add-int v23, v23, v11

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v16

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2570
    .restart local v16    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    div-int/lit8 v22, v12, 0x2

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    sub-int v23, v23, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2572
    .restart local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    div-int/lit8 v22, v12, 0x2

    add-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    sub-int v23, v23, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    add-int v23, v23, v11

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2606
    .restart local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    :cond_2
    :goto_1
    if-nez v16, :cond_9

    .line 2608
    const-string v20, "CenterBarWindow"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "animationCenterBar4Buttons Fail to load translateAnimation mFocusZoneInfo="

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2689
    :goto_2
    return-void

    .line 2574
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0xc

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 2577
    :cond_4
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    .end local v16    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    mul-int/lit8 v22, v12, 0x3

    div-int/lit8 v22, v22, 0x2

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    sub-int v23, v23, v11

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v16

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2579
    .restart local v16    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    div-int/lit8 v22, v12, 0x2

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2581
    .restart local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    div-int/lit8 v22, v12, 0x2

    add-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    sub-int v23, v23, v11

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2585
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_7

    .line 2588
    :cond_6
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    .end local v16    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    sub-int v21, v21, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    sub-int v21, v21, v22

    add-int v21, v21, v11

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    mul-int/lit8 v24, v12, 0x3

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v16

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2590
    .restart local v16    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    sub-int v21, v21, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    sub-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    div-int/lit8 v24, v12, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2592
    .restart local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    sub-int v21, v21, v12

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    sub-int v21, v21, v22

    add-int v21, v21, v11

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    div-int/lit8 v24, v12, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2594
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0xc

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v20, v0

    const/16 v21, 0x8

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_2

    .line 2597
    :cond_8
    new-instance v16, Landroid/view/animation/TranslateAnimation;

    .end local v16    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    add-int v21, v21, v22

    sub-int v21, v21, v11

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    mul-int/lit8 v24, v12, 0x3

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v16

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2599
    .restart local v16    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    add-int v21, v21, v22

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    div-int/lit8 v24, v12, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2601
    .restart local v17    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v13

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v22, v0

    div-int/lit8 v22, v22, 0x2

    add-int v21, v21, v22

    sub-int v21, v21, v11

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v23, v0

    div-int/lit8 v24, v12, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v18    # "translateAnimation2":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2612
    :cond_9
    new-instance v20, Lcom/sec/android/app/FlashBarService/CenterBarWindow$23;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$23;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2638
    new-instance v20, Lcom/sec/android/app/FlashBarService/CenterBarWindow$24;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$24;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2664
    const-wide/16 v20, 0x12c

    move-object/from16 v0, v16

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2665
    const-wide/16 v20, 0x12c

    move-object/from16 v0, v17

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2666
    const-wide/16 v20, 0x12c

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2667
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x10a0006

    invoke-static/range {v20 .. v21}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v20

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2668
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x10a0006

    invoke-static/range {v20 .. v21}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v20

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    const v21, 0x10a0006

    invoke-static/range {v20 .. v21}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2670
    const-wide/16 v20, 0x12c

    move-wide/from16 v0, v20

    invoke-virtual {v6, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2672
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2673
    .local v10, "buttonAnimationSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/animation/AnimationSet;>;"
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v7, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2674
    .local v7, "animationSet":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v16

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2675
    invoke-virtual {v7, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2676
    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2678
    new-instance v8, Landroid/view/animation/AnimationSet;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v8, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2679
    .local v8, "animationSet1":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2680
    invoke-virtual {v8, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2681
    invoke-virtual {v10, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2683
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2684
    .local v9, "animationSet2":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2685
    invoke-virtual {v9, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2686
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2688
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startButtonAnimation(Ljava/util/ArrayList;)V

    goto/16 :goto_2
.end method

.method private animationCenterBar4Buttons()V
    .locals 27

    .prologue
    .line 2692
    const/16 v17, 0x0

    .line 2693
    .local v17, "translateAnimation":Landroid/view/animation/Animation;
    const/16 v18, 0x0

    .line 2694
    .local v18, "translateAnimation1":Landroid/view/animation/Animation;
    const/16 v19, 0x0

    .line 2695
    .local v19, "translateAnimation2":Landroid/view/animation/Animation;
    const/16 v20, 0x0

    .line 2697
    .local v20, "translateAnimation3":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a000a

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v22

    move/from16 v0, v22

    float-to-int v13, v0

    .line 2698
    .local v13, "buttonBgHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v22

    const v23, 0x7f0a000b

    invoke-virtual/range {v22 .. v23}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v22

    move/from16 v0, v22

    float-to-int v12, v0

    .line 2700
    .local v12, "buttonArrangeMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v23, v0

    div-int/lit8 v23, v23, 0x2

    sub-int v14, v22, v23

    .line 2701
    .local v14, "fromX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v23, v0

    div-int/lit8 v23, v23, 0x2

    sub-int v15, v22, v23

    .line 2703
    .local v15, "fromY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    .local v16, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    .line 2704
    .local v21, "v":Landroid/view/View;
    const/16 v22, 0x0

    invoke-virtual/range {v21 .. v22}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2707
    .end local v21    # "v":Landroid/view/View;
    :cond_0
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const v22, 0x3ecccccd    # 0.4f

    const/high16 v23, 0x3f800000    # 1.0f

    move/from16 v0, v22

    move/from16 v1, v23

    invoke-direct {v6, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2709
    .local v6, "alphaAnimation":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_5

    .line 2710
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_3

    .line 2713
    :cond_1
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    mul-int/lit8 v24, v13, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    add-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2715
    .restart local v17    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    sub-int v23, v23, v13

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2717
    .restart local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v19, Landroid/view/animation/TranslateAnimation;

    .end local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2719
    .restart local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v20, Landroid/view/animation/TranslateAnimation;

    .end local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    add-int v23, v23, v13

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    sub-int v25, v25, v26

    add-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2759
    .restart local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    :cond_2
    :goto_1
    if-nez v17, :cond_9

    .line 2761
    const-string v22, "CenterBarWindow"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "animationCenterBar4Buttons Fail to load translateAnimation mFocusZoneInfo="

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2823
    :goto_2
    return-void

    .line 2721
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0xc

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x4

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x8

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    .line 2724
    :cond_4
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    mul-int/lit8 v24, v13, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    add-int v25, v25, v26

    sub-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2726
    .restart local v17    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    sub-int v23, v23, v13

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2728
    .restart local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v19, Landroid/view/animation/TranslateAnimation;

    .end local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    add-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2730
    .restart local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v20, Landroid/view/animation/TranslateAnimation;

    .end local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    add-int v23, v23, v13

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v26, v0

    div-int/lit8 v26, v26, 0x2

    add-int v25, v25, v26

    sub-int v25, v25, v12

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2734
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x3

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x1

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_7

    .line 2737
    :cond_6
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    sub-int v23, v23, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    add-int v23, v23, v12

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    mul-int/lit8 v26, v13, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2739
    .restart local v17    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    sub-int v23, v23, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v13

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2741
    .restart local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v19, Landroid/view/animation/TranslateAnimation;

    .end local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    sub-int v23, v23, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2743
    .restart local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v20, Landroid/view/animation/TranslateAnimation;

    .end local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    sub-int v23, v23, v13

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    sub-int v23, v23, v24

    add-int v23, v23, v12

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2745
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0xc

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x4

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v22, v0

    const/16 v23, 0x8

    move/from16 v0, v22

    move/from16 v1, v23

    if-ne v0, v1, :cond_2

    .line 2748
    :cond_8
    new-instance v17, Landroid/view/animation/TranslateAnimation;

    .end local v17    # "translateAnimation":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    sub-int v23, v23, v12

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    mul-int/lit8 v26, v13, 0x2

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v17

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2750
    .restart local v17    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v18, Landroid/view/animation/TranslateAnimation;

    .end local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    sub-int v25, v25, v13

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v18

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2752
    .restart local v18    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v19, Landroid/view/animation/TranslateAnimation;

    .end local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2754
    .restart local v19    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v20, Landroid/view/animation/TranslateAnimation;

    .end local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    int-to-float v0, v14

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v23, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v24, v0

    div-int/lit8 v24, v24, 0x2

    add-int v23, v23, v24

    sub-int v23, v23, v12

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    int-to-float v0, v15

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v25, v0

    add-int v25, v25, v13

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v20

    move/from16 v1, v22

    move/from16 v2, v23

    move/from16 v3, v24

    move/from16 v4, v25

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v20    # "translateAnimation3":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2765
    :cond_9
    new-instance v22, Lcom/sec/android/app/FlashBarService/CenterBarWindow$25;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$25;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2791
    const-wide/16 v22, 0x12c

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2792
    const-wide/16 v22, 0x12c

    move-object/from16 v0, v18

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2793
    const-wide/16 v22, 0x12c

    move-object/from16 v0, v19

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2794
    const-wide/16 v22, 0x12c

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2795
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x10a0006

    invoke-static/range {v22 .. v23}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v22

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x10a0006

    invoke-static/range {v22 .. v23}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x10a0006

    invoke-static/range {v22 .. v23}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v22

    move-object/from16 v0, v19

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2798
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v22, v0

    const v23, 0x10a0006

    invoke-static/range {v22 .. v23}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v22

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2799
    const-wide/16 v22, 0x12c

    move-wide/from16 v0, v22

    invoke-virtual {v6, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2801
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 2802
    .local v11, "buttonAnimationSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/animation/AnimationSet;>;"
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-direct {v7, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2803
    .local v7, "animationSet":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2804
    invoke-virtual {v7, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2805
    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2807
    new-instance v8, Landroid/view/animation/AnimationSet;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-direct {v8, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2808
    .local v8, "animationSet1":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2809
    invoke-virtual {v8, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2810
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2812
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2813
    .local v9, "animationSet2":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2814
    invoke-virtual {v9, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2815
    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2817
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-direct {v10, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2818
    .local v10, "animationSet3":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2819
    invoke-virtual {v10, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2820
    invoke-virtual {v11, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2822
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startButtonAnimation(Ljava/util/ArrayList;)V

    goto/16 :goto_2
.end method

.method private animationCenterBar5Buttons()V
    .locals 32

    .prologue
    .line 2826
    const/16 v21, 0x0

    .line 2827
    .local v21, "translateAnimation":Landroid/view/animation/Animation;
    const/16 v22, 0x0

    .line 2828
    .local v22, "translateAnimation1":Landroid/view/animation/Animation;
    const/16 v23, 0x0

    .line 2829
    .local v23, "translateAnimation2":Landroid/view/animation/Animation;
    const/16 v24, 0x0

    .line 2830
    .local v24, "translateAnimation3":Landroid/view/animation/Animation;
    const/16 v25, 0x0

    .line 2832
    .local v25, "translateAnimation4":Landroid/view/animation/Animation;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0a000a

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v16, v0

    .line 2833
    .local v16, "buttonBgHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0a000b

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v13, v0

    .line 2834
    .local v13, "buttonArrangeMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0a000c

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v14, v0

    .line 2835
    .local v14, "buttonArrangeMargin5b2pL":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0a0009

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v17, v0

    .line 2836
    .local v17, "buttonsMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    const v28, 0x7f0a000e

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v27

    move/from16 v0, v27

    float-to-int v15, v0

    .line 2838
    .local v15, "buttonArrangeOverlapMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    sub-int v18, v27, v28

    .line 2839
    .local v18, "fromX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    sub-int v19, v27, v28

    .line 2841
    .local v19, "fromY":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .local v20, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v27

    if-eqz v27, :cond_0

    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    .line 2842
    .local v26, "v":Landroid/view/View;
    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2845
    .end local v26    # "v":Landroid/view/View;
    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v27, v0

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_5

    .line 2846
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x3

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_3

    .line 2849
    :cond_1
    new-instance v21, Landroid/view/animation/TranslateAnimation;

    .end local v21    # "translateAnimation":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    mul-int/lit8 v29, v16, 0x5

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    sub-int v28, v28, v15

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    sub-int v30, v30, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    add-int v30, v30, v13

    sub-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2851
    .restart local v21    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v22, Landroid/view/animation/TranslateAnimation;

    .end local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    mul-int/lit8 v29, v16, 0x3

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    sub-int v30, v30, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    add-int v30, v30, v14

    sub-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v22

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2853
    .restart local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v23, Landroid/view/animation/TranslateAnimation;

    .end local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    div-int/lit8 v29, v16, 0x2

    sub-int v28, v28, v29

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    sub-int v30, v30, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    sub-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2855
    .restart local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v24, Landroid/view/animation/TranslateAnimation;

    .end local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    div-int/lit8 v29, v16, 0x2

    add-int v28, v28, v29

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    sub-int v30, v30, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    add-int v30, v30, v14

    sub-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2857
    .restart local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    new-instance v25, Landroid/view/animation/TranslateAnimation;

    .end local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    mul-int/lit8 v29, v16, 0x3

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    add-int v28, v28, v15

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    sub-int v30, v30, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    add-int v30, v30, v13

    sub-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v25

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2903
    .restart local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    :cond_2
    :goto_1
    if-nez v21, :cond_9

    .line 2905
    const-string v27, "CenterBarWindow"

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "animationCenterBar5Buttons Fail to load translateAnimation mFocusZoneInfo="

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v29, v0

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    invoke-static/range {v27 .. v28}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2976
    :goto_2
    return-void

    .line 2859
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0xc

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x8

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2

    .line 2862
    :cond_4
    new-instance v21, Landroid/view/animation/TranslateAnimation;

    .end local v21    # "translateAnimation":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    mul-int/lit8 v29, v16, 0x5

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    sub-int v28, v28, v15

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    sub-int v30, v30, v13

    add-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2864
    .restart local v21    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v22, Landroid/view/animation/TranslateAnimation;

    .end local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    mul-int/lit8 v29, v16, 0x3

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    sub-int v30, v30, v14

    add-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v22

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2866
    .restart local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v23, Landroid/view/animation/TranslateAnimation;

    .end local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    div-int/lit8 v29, v16, 0x2

    sub-int v28, v28, v29

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    add-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2868
    .restart local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v24, Landroid/view/animation/TranslateAnimation;

    .end local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    div-int/lit8 v29, v16, 0x2

    add-int v28, v28, v29

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    sub-int v30, v30, v14

    add-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2870
    .restart local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    new-instance v25, Landroid/view/animation/TranslateAnimation;

    .end local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    mul-int/lit8 v29, v16, 0x3

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    add-int v28, v28, v15

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v31, v0

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    sub-int v30, v30, v13

    add-int v30, v30, v17

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v25

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2874
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x3

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x1

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x2

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_7

    .line 2877
    :cond_6
    new-instance v21, Landroid/view/animation/TranslateAnimation;

    .end local v21    # "translateAnimation":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    sub-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    add-int v28, v28, v13

    sub-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    mul-int/lit8 v31, v16, 0x5

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    sub-int v30, v30, v15

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2879
    .restart local v21    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v22, Landroid/view/animation/TranslateAnimation;

    .end local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    sub-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    add-int v28, v28, v14

    sub-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    mul-int/lit8 v31, v16, 0x3

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v22

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2881
    .restart local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v23, Landroid/view/animation/TranslateAnimation;

    .end local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    sub-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    sub-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    div-int/lit8 v31, v16, 0x2

    sub-int v30, v30, v31

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2883
    .restart local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v24, Landroid/view/animation/TranslateAnimation;

    .end local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    sub-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    add-int v28, v28, v14

    sub-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    div-int/lit8 v31, v16, 0x2

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2885
    .restart local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    new-instance v25, Landroid/view/animation/TranslateAnimation;

    .end local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    sub-int v28, v28, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    sub-int v28, v28, v29

    add-int v28, v28, v13

    sub-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    mul-int/lit8 v31, v16, 0x3

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    add-int v30, v30, v15

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v25

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2887
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0xc

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x4

    move/from16 v0, v27

    move/from16 v1, v28

    if-eq v0, v1, :cond_8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v27, v0

    const/16 v28, 0x8

    move/from16 v0, v27

    move/from16 v1, v28

    if-ne v0, v1, :cond_2

    .line 2890
    :cond_8
    new-instance v21, Landroid/view/animation/TranslateAnimation;

    .end local v21    # "translateAnimation":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    sub-int v28, v28, v13

    add-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    mul-int/lit8 v31, v16, 0x5

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    sub-int v30, v30, v15

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v21

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2892
    .restart local v21    # "translateAnimation":Landroid/view/animation/Animation;
    new-instance v22, Landroid/view/animation/TranslateAnimation;

    .end local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    sub-int v28, v28, v14

    add-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    mul-int/lit8 v31, v16, 0x3

    div-int/lit8 v31, v31, 0x2

    sub-int v30, v30, v31

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v22

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2894
    .restart local v22    # "translateAnimation1":Landroid/view/animation/Animation;
    new-instance v23, Landroid/view/animation/TranslateAnimation;

    .end local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    add-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    div-int/lit8 v31, v16, 0x2

    sub-int v30, v30, v31

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v23

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2896
    .restart local v23    # "translateAnimation2":Landroid/view/animation/Animation;
    new-instance v24, Landroid/view/animation/TranslateAnimation;

    .end local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    sub-int v28, v28, v14

    add-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    div-int/lit8 v31, v16, 0x2

    add-int v30, v30, v31

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v24

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 2898
    .restart local v24    # "translateAnimation3":Landroid/view/animation/Animation;
    new-instance v25, Landroid/view/animation/TranslateAnimation;

    .end local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    move/from16 v29, v0

    div-int/lit8 v29, v29, 0x2

    add-int v28, v28, v29

    sub-int v28, v28, v13

    add-int v28, v28, v17

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v29, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v30, v0

    mul-int/lit8 v31, v16, 0x3

    div-int/lit8 v31, v31, 0x2

    add-int v30, v30, v31

    add-int v30, v30, v15

    move/from16 v0, v30

    int-to-float v0, v0

    move/from16 v30, v0

    move-object/from16 v0, v25

    move/from16 v1, v27

    move/from16 v2, v28

    move/from16 v3, v29

    move/from16 v4, v30

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .restart local v25    # "translateAnimation4":Landroid/view/animation/Animation;
    goto/16 :goto_1

    .line 2909
    :cond_9
    new-instance v6, Landroid/view/animation/AlphaAnimation;

    const v27, 0x3ecccccd    # 0.4f

    const/high16 v28, 0x3f800000    # 1.0f

    move/from16 v0, v27

    move/from16 v1, v28

    invoke-direct {v6, v0, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 2910
    .local v6, "alphaAnimation":Landroid/view/animation/Animation;
    new-instance v27, Lcom/sec/android/app/FlashBarService/CenterBarWindow$26;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$26;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2936
    const-wide/16 v28, 0x12c

    move-object/from16 v0, v21

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2937
    const-wide/16 v28, 0x12c

    move-object/from16 v0, v22

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2938
    const-wide/16 v28, 0x12c

    move-object/from16 v0, v23

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2939
    const-wide/16 v28, 0x12c

    move-object/from16 v0, v24

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2940
    const-wide/16 v28, 0x12c

    move-object/from16 v0, v25

    move-wide/from16 v1, v28

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2941
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const v28, 0x10a0006

    invoke-static/range {v27 .. v28}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v27

    move-object/from16 v0, v21

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const v28, 0x10a0006

    invoke-static/range {v27 .. v28}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v27

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const v28, 0x10a0006

    invoke-static/range {v27 .. v28}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v27

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const v28, 0x10a0006

    invoke-static/range {v27 .. v28}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v27

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2945
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v27, v0

    const v28, 0x10a0006

    invoke-static/range {v27 .. v28}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v27

    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2946
    const-wide/16 v28, 0x12c

    move-wide/from16 v0, v28

    invoke-virtual {v6, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2948
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2950
    .local v12, "buttonAnimationSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/animation/AnimationSet;>;"
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-direct {v7, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2951
    .local v7, "animationSet":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2952
    invoke-virtual {v7, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2953
    invoke-virtual {v12, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2955
    new-instance v8, Landroid/view/animation/AnimationSet;

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-direct {v8, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2956
    .local v8, "animationSet1":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2957
    invoke-virtual {v8, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2958
    invoke-virtual {v12, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2960
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2961
    .local v9, "animationSet2":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v23

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2962
    invoke-virtual {v9, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2963
    invoke-virtual {v12, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2965
    new-instance v10, Landroid/view/animation/AnimationSet;

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-direct {v10, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2966
    .local v10, "animationSet3":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2967
    invoke-virtual {v10, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2968
    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2970
    new-instance v11, Landroid/view/animation/AnimationSet;

    const/16 v27, 0x0

    move/from16 v0, v27

    invoke-direct {v11, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 2971
    .local v11, "animationSet4":Landroid/view/animation/AnimationSet;
    move-object/from16 v0, v25

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2972
    invoke-virtual {v11, v6}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 2973
    invoke-virtual {v12, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2975
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startButtonAnimation(Ljava/util/ArrayList;)V

    goto/16 :goto_2
.end method

.method private centerBarFullSizeDocking(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1160
    const/4 v2, 0x0

    .line 1161
    .local v2, "docking":Z
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    float-to-int v4, v6

    .line 1162
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    float-to-int v5, v6

    .line 1163
    .local v5, "y":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v0

    .line 1165
    .local v0, "arrange":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getCenterBarEdgeArea(Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    move-result-object v1

    .line 1166
    .local v1, "currentEdge":Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;
    new-instance v6, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    float-to-int v7, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    float-to-int v8, v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getCenterBarEdgeArea(Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    move-result-object v3

    .line 1167
    .local v3, "newEdge":Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;
    if-ne v1, v3, :cond_0

    sget-object v6, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->NONE:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    if-ne v1, v6, :cond_1

    .line 1168
    :cond_0
    const/4 v6, 0x0

    .line 1211
    :goto_0
    return v6

    .line 1171
    :cond_1
    const/4 v6, 0x1

    if-gt v0, v6, :cond_2

    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 1172
    :cond_2
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    div-int/lit8 v6, v6, 0x2

    if-gt v4, v6, :cond_3

    .line 1173
    const/4 v2, 0x1

    .line 1174
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    int-to-double v6, v6

    const-wide v8, 0x3fa999999999999aL    # 0.05

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 1176
    :cond_3
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    if-lt v4, v6, :cond_4

    .line 1177
    const/4 v2, 0x1

    .line 1178
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    int-to-double v6, v6

    const-wide v8, 0x3fee666666666666L    # 0.95

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 1182
    :cond_4
    const/4 v6, 0x1

    if-gt v0, v6, :cond_5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_7

    .line 1183
    :cond_5
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    div-int/lit8 v6, v6, 0x2

    if-gt v5, v6, :cond_6

    .line 1184
    const/4 v2, 0x1

    .line 1185
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    int-to-double v6, v6

    const-wide v8, 0x3fa999999999999aL    # 0.05

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 1187
    :cond_6
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    iget v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v6, v7

    if-lt v5, v6, :cond_7

    .line 1188
    const/4 v2, 0x1

    .line 1189
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    int-to-double v6, v6

    const-wide v8, 0x3fee666666666666L    # 0.95

    mul-double/2addr v6, v8

    double-to-int v6, v6

    iput v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 1193
    :cond_7
    if-eqz v2, :cond_a

    .line 1194
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setCenterBarPoint()V

    .line 1195
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerControlBarDocking(Z)Z

    .line 1196
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 1197
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    invoke-virtual {v6, v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setCenterBarPoint(ILandroid/graphics/Point;)V

    .line 1199
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v6, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 1200
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportStyleTransition(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1201
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->isEnableMakePenWindow()Z

    move-result v6

    if-nez v6, :cond_9

    .line 1202
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 1204
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIvt:[B

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v8}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 1205
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    new-instance v7, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v8

    float-to-int v8, v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    float-to-int v9, v9

    invoke-direct {v7, v8, v9}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->findCandidateFocus()I

    move-result v8

    xor-int/lit8 v8, v8, -0x1

    and-int/lit8 v8, v8, 0xf

    const/4 v9, 0x1

    invoke-virtual {v6, v7, v8, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->changeTaskToCascade(Landroid/graphics/Point;IZ)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1206
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->findCandidateFocus()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->changeTaskToFull(I)V

    .line 1210
    :cond_a
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullSizeDocking:Z

    move v6, v2

    .line 1211
    goto/16 :goto_0
.end method

.method private checkFunctionState(Ljava/util/List;Ljava/lang/String;)Z
    .locals 1
    .param p2, "disableFunction"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 3035
    .local p1, "disableFunctionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3036
    const/4 v0, 0x0

    .line 3038
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private clearButtonAnimation()V
    .locals 4

    .prologue
    .line 3053
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 3054
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->clearAnimation()V

    goto :goto_0

    .line 3056
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 3057
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    .line 3058
    .local v0, "currentAni":Landroid/view/animation/Animation;
    if-eqz v0, :cond_1

    .line 3059
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 3060
    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 3063
    .end local v0    # "currentAni":Landroid/view/animation/Animation;
    :cond_1
    return-void
.end method

.method private drawGuideCenterBar(II)V
    .locals 8
    .param p1, "positionX"    # I
    .param p2, "positionY"    # I

    .prologue
    const v7, 0x7f02006b

    const/4 v6, 0x1

    .line 818
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->cancelHideButtonTimer()V

    .line 820
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 821
    .local v0, "guideRect":Landroid/graphics/Rect;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v1, v4

    .line 823
    .local v1, "guideSize":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    if-nez v4, :cond_0

    .line 824
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 825
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f030013

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 826
    .local v2, "guideView":Landroid/view/View;
    new-instance v4, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5, v2, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/content/Context;Landroid/view/View;I)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    .line 829
    .end local v2    # "guideView":Landroid/view/View;
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCurrentArrangeState:I

    if-le v4, v6, :cond_1

    .line 830
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    const v5, 0x7f02006c

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->setDrawable(I)V

    .line 839
    :goto_0
    div-int/lit8 v4, v1, 0x2

    sub-int v4, p1, v4

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 840
    div-int/lit8 v4, v1, 0x2

    sub-int v4, p2, v4

    iput v4, v0, Landroid/graphics/Rect;->top:I

    .line 841
    iget v4, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v1

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 842
    iget v4, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v1

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 845
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    invoke-virtual {v4, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->show(Landroid/graphics/Rect;)V

    .line 846
    return-void

    .line 832
    :cond_1
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v4, v6, :cond_2

    .line 833
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->setDrawable(I)V

    goto :goto_0

    .line 835
    :cond_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    const v5, 0x7f02006d

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->setDrawable(I)V

    goto :goto_0
.end method

.method private findCandidateFocus()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/16 v4, 0x8

    .line 1216
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v2}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v0

    .line 1218
    .local v0, "arrange":I
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    .line 1219
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    if-gt v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x2

    if-gt v2, v3, :cond_2

    .line 1220
    if-eq v0, v4, :cond_0

    if-ne v0, v6, :cond_1

    .line 1221
    :cond_0
    const/16 v1, 0x8

    .line 1271
    .local v1, "zone":I
    :goto_0
    return v1

    .line 1223
    .end local v1    # "zone":I
    :cond_1
    const/16 v1, 0xc

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1225
    .end local v1    # "zone":I
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_5

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x2

    if-gt v2, v3, :cond_5

    .line 1226
    if-eq v0, v4, :cond_3

    if-ne v0, v6, :cond_4

    .line 1227
    :cond_3
    const/4 v1, 0x4

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1229
    .end local v1    # "zone":I
    :cond_4
    const/16 v1, 0xc

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1231
    .end local v1    # "zone":I
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_8

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_8

    .line 1232
    if-eq v0, v4, :cond_6

    if-ne v0, v5, :cond_7

    .line 1233
    :cond_6
    const/4 v1, 0x1

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1235
    .end local v1    # "zone":I
    :cond_7
    const/4 v1, 0x3

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1238
    .end local v1    # "zone":I
    :cond_8
    if-eq v0, v4, :cond_9

    if-ne v0, v5, :cond_a

    .line 1239
    :cond_9
    const/4 v1, 0x2

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1241
    .end local v1    # "zone":I
    :cond_a
    const/4 v1, 0x3

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1245
    .end local v1    # "zone":I
    :cond_b
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    if-gt v2, v3, :cond_e

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x2

    if-gt v2, v3, :cond_e

    .line 1246
    if-eq v0, v4, :cond_c

    if-ne v0, v6, :cond_d

    .line 1247
    :cond_c
    const/16 v1, 0x8

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1249
    .end local v1    # "zone":I
    :cond_d
    const/16 v1, 0xc

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1251
    .end local v1    # "zone":I
    :cond_e
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_11

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x2

    if-gt v2, v3, :cond_11

    .line 1252
    if-eq v0, v4, :cond_f

    if-ne v0, v5, :cond_10

    .line 1253
    :cond_f
    const/4 v1, 0x2

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1255
    .end local v1    # "zone":I
    :cond_10
    const/4 v1, 0x3

    .restart local v1    # "zone":I
    goto :goto_0

    .line 1257
    .end local v1    # "zone":I
    :cond_11
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_14

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    div-int/lit8 v3, v3, 0x2

    if-le v2, v3, :cond_14

    .line 1258
    if-eq v0, v4, :cond_12

    if-ne v0, v5, :cond_13

    .line 1259
    :cond_12
    const/4 v1, 0x1

    .restart local v1    # "zone":I
    goto/16 :goto_0

    .line 1261
    .end local v1    # "zone":I
    :cond_13
    const/4 v1, 0x3

    .restart local v1    # "zone":I
    goto/16 :goto_0

    .line 1264
    .end local v1    # "zone":I
    :cond_14
    if-eq v0, v4, :cond_15

    if-ne v0, v6, :cond_16

    .line 1265
    :cond_15
    const/4 v1, 0x4

    .restart local v1    # "zone":I
    goto/16 :goto_0

    .line 1267
    .end local v1    # "zone":I
    :cond_16
    const/16 v1, 0xc

    .restart local v1    # "zone":I
    goto/16 :goto_0
.end method

.method private forceHideInputMethod()V
    .locals 1

    .prologue
    .line 1675
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 1676
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 1677
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 1679
    :cond_0
    return-void
.end method

.method private getButtonAnimation(ZZ)Landroid/view/animation/TranslateAnimation;
    .locals 12
    .param p1, "open"    # Z
    .param p2, "left"    # Z

    .prologue
    .line 2040
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v2, 0x7f0f0075

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 2041
    .local v10, "centerImageView":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    .line 2043
    .local v9, "centerImageParams":Landroid/view/ViewGroup$LayoutParams;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 2044
    iget v1, v9, Landroid/view/ViewGroup$LayoutParams;->width:I

    div-int/lit8 v11, v1, 0x2

    .line 2045
    .local v11, "showposition":I
    if-eqz p1, :cond_1

    .line 2046
    if-eqz p2, :cond_0

    .line 2047
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    int-to-float v2, v11

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2076
    .local v0, "t":Landroid/view/animation/TranslateAnimation;
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2077
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const v2, 0x10a0005

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/content/Context;I)V

    .line 2078
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f070002

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2079
    return-object v0

    .line 2049
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_0
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    neg-int v2, v11

    int-to-float v2, v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    goto :goto_0

    .line 2052
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_1
    if-eqz p2, :cond_2

    .line 2053
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    int-to-float v4, v11

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2057
    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :goto_1
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setStartOffset(J)V

    goto :goto_0

    .line 2055
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_2
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    neg-int v4, v11

    int-to-float v4, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    goto :goto_1

    .line 2060
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    .end local v11    # "showposition":I
    :cond_3
    iget v1, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    div-int/lit8 v11, v1, 0x2

    .line 2061
    .restart local v11    # "showposition":I
    if-eqz p1, :cond_5

    .line 2062
    if-eqz p2, :cond_4

    .line 2063
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v6, v11

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    goto :goto_0

    .line 2065
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_4
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    neg-int v6, v11

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    goto :goto_0

    .line 2068
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_5
    if-eqz p2, :cond_6

    .line 2069
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    int-to-float v8, v11

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2073
    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :goto_2
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/TranslateAnimation;->setStartOffset(J)V

    goto/16 :goto_0

    .line 2071
    .end local v0    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_6
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    neg-int v8, v11

    int-to-float v8, v8

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .restart local v0    # "t":Landroid/view/animation/TranslateAnimation;
    goto :goto_2
.end method

.method private getCenterBarEdgeArea(Landroid/graphics/Point;)Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;
    .locals 2
    .param p1, "p"    # Landroid/graphics/Point;

    .prologue
    .line 1052
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v0, :cond_0

    .line 1053
    sget-object v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->NONE:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    .line 1067
    :goto_0
    return-object v0

    .line 1055
    :cond_0
    iget v0, p1, Landroid/graphics/Point;->x:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    if-gt v0, v1, :cond_1

    .line 1056
    sget-object v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->FORMER:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    goto :goto_0

    .line 1058
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    sub-int/2addr v0, v1

    iget v1, p1, Landroid/graphics/Point;->x:I

    if-gt v0, v1, :cond_2

    .line 1059
    sget-object v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->LATTER:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    goto :goto_0

    .line 1061
    :cond_2
    iget v0, p1, Landroid/graphics/Point;->y:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    if-gt v0, v1, :cond_3

    .line 1062
    sget-object v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->FORMER:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    goto :goto_0

    .line 1064
    :cond_3
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    sub-int/2addr v0, v1

    iget v1, p1, Landroid/graphics/Point;->y:I

    if-gt v0, v1, :cond_4

    .line 1065
    sget-object v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->LATTER:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    goto :goto_0

    .line 1067
    :cond_4
    sget-object v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;->NONE:Lcom/sec/android/app/FlashBarService/CenterBarWindow$EdgeArea;

    goto :goto_0
.end method

.method private initButtonPopup()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1567
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 1568
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    .line 1570
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v10, :cond_a

    .line 1571
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1572
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1573
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v10, :cond_9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportFreeStyleLaunch(Landroid/content/Context;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1574
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1578
    :goto_0
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1579
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1619
    :cond_0
    :goto_1
    const/4 v5, 0x0

    .line 1621
    .local v5, "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_0
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActivityManager:Landroid/app/ActivityManager;

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v9

    .line 1622
    .local v9, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_1

    .line 1623
    const/4 v10, 0x0

    invoke-interface {v9, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 1624
    .local v8, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 1625
    .local v7, "pm":Landroid/content/pm/PackageManager;
    if-eqz v7, :cond_1

    .line 1626
    iget-object v10, v8, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    const/16 v11, 0x80

    invoke-virtual {v7, v10, v11}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 1627
    .local v1, "activityInfo":Landroid/content/pm/ActivityInfo;
    if-eqz v1, :cond_1

    .line 1628
    iget-object v10, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v10, :cond_c

    iget-object v10, v1, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v10, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 1629
    .local v4, "applicationMetaData":Landroid/os/Bundle;
    :goto_2
    iget-object v2, v1, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    .line 1630
    .local v2, "activityMetaData":Landroid/os/Bundle;
    if-eqz v4, :cond_d

    .line 1631
    const-string v10, "com.samsung.android.sdk.multiwindow.disablefunction"

    invoke-virtual {v4, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1632
    .local v3, "applicationFunction":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 1633
    new-instance v6, Ljava/util/ArrayList;

    const-string v10, "\\|"

    invoke-virtual {v3, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v5    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v6, "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v5, v6

    .line 1647
    .end local v1    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v2    # "activityMetaData":Landroid/os/Bundle;
    .end local v3    # "applicationFunction":Ljava/lang/String;
    .end local v4    # "applicationMetaData":Landroid/os/Bundle;
    .end local v6    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    .end local v8    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v9    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .restart local v5    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    :goto_3
    const-string v10, "exit"

    invoke-direct {p0, v5, v10}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkFunctionState(Ljava/util/List;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 1648
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1651
    :cond_2
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v10, :cond_8

    .line 1653
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1654
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalAnimIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1656
    :cond_3
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1657
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppSwitchingAnimIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1659
    :cond_4
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1660
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchAnimIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1662
    :cond_5
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1663
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAnimIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1665
    :cond_6
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1666
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullAnimIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1668
    :cond_7
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 1669
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseAnimIcon:Landroid/widget/ImageView;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1672
    :cond_8
    return-void

    .line 1576
    .end local v5    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_9
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 1581
    :cond_a
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v10, v11}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSideSyncConnected(I)Z

    move-result v10

    if-eqz v10, :cond_b

    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mbSideSyncButton:Z

    if-eqz v10, :cond_b

    .line 1582
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1584
    :cond_b
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1585
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1586
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1587
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1588
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1590
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    const-string v11, "AppSwitching"

    invoke-direct {p0, v10, v11}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkFunctionState(Ljava/util/List;Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1591
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1592
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 1628
    .restart local v1    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .restart local v5    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v7    # "pm":Landroid/content/pm/PackageManager;
    .restart local v8    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v9    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 1635
    .restart local v2    # "activityMetaData":Landroid/os/Bundle;
    .restart local v4    # "applicationMetaData":Landroid/os/Bundle;
    :cond_d
    if-eqz v2, :cond_1

    .line 1636
    :try_start_1
    const-string v10, "com.samsung.android.sdk.multiwindow.disablefunction"

    invoke-virtual {v2, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1637
    .local v0, "activityFunction":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 1638
    new-instance v6, Ljava/util/ArrayList;

    const-string v10, "\\|"

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v10

    invoke-direct {v6, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .end local v5    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v6    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object v5, v6

    .end local v6    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v5    # "disableFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto/16 :goto_3

    .line 1644
    .end local v0    # "activityFunction":Ljava/lang/String;
    .end local v1    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v2    # "activityMetaData":Landroid/os/Bundle;
    .end local v4    # "applicationMetaData":Landroid/os/Bundle;
    .end local v7    # "pm":Landroid/content/pm/PackageManager;
    .end local v8    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v9    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    :catch_0
    move-exception v10

    goto/16 :goto_3
.end method

.method private makeButtonPopupLayout()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1682
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x7f070000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1870
    :cond_0
    :goto_0
    return-void

    .line 1685
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1686
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f03000e

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    .line 1687
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f0077

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    .line 1689
    new-instance v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$10;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 1708
    .local v1, "touchListener":Landroid/view/View$OnTouchListener;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f007a

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    .line 1709
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    if-eqz v2, :cond_2

    .line 1710
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1711
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1712
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$11;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1728
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f007d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    .line 1729
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    if-eqz v2, :cond_4

    .line 1730
    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mbSideSyncButton:Z

    if-nez v2, :cond_3

    .line 1731
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1733
    :cond_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1734
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1735
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$12;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$12;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1742
    :cond_4
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f0078

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    .line 1743
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    if-eqz v2, :cond_5

    .line 1744
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1745
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1746
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$13;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775
    :cond_5
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f0079

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    .line 1776
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    if-eqz v2, :cond_6

    .line 1777
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1778
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1779
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1796
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f007e

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    .line 1797
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    if-eqz v2, :cond_7

    .line 1798
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1799
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1800
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$15;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1820
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f007b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    .line 1821
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    if-eqz v2, :cond_8

    .line 1822
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1823
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1824
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$16;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1834
    :cond_8
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v3, 0x7f0f007c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    .line 1835
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1836
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHoverPopupType(I)V

    .line 1837
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1838
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    new-instance v3, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$17;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0
.end method

.method private makeDragAndDropHelpPopupLayout()V
    .locals 9

    .prologue
    .line 3067
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 3068
    .local v3, "layoutInflater":Landroid/view/LayoutInflater;
    const v6, 0x7f030018

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 3069
    .local v2, "dragAndDropHelpPopupView":Landroid/view/View;
    if-nez v2, :cond_0

    .line 3164
    .end local v2    # "dragAndDropHelpPopupView":Landroid/view/View;
    .end local v3    # "layoutInflater":Landroid/view/LayoutInflater;
    :goto_0
    return-void

    .line 3072
    .restart local v2    # "dragAndDropHelpPopupView":Landroid/view/View;
    .restart local v3    # "layoutInflater":Landroid/view/LayoutInflater;
    :cond_0
    const v6, 0x7f0f00ad

    invoke-virtual {v2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 3074
    .local v0, "checkBox":Landroid/widget/CheckBox;
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 3075
    new-instance v6, Lcom/sec/android/app/FlashBarService/CenterBarWindow$28;

    invoke-direct {v6, p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$28;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 3087
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f08000c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080011

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;

    invoke-direct {v8, p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$29;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 3101
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f080004

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;

    invoke-direct {v7, p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$30;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 3131
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 3132
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    .line 3133
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 3135
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/sec/android/app/FlashBarService/CenterBarWindow$31;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$31;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 3146
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/sec/android/app/FlashBarService/CenterBarWindow$32;

    invoke-direct {v7, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$32;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 3156
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 3157
    .local v5, "w":Landroid/view/Window;
    const/16 v6, 0x300

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 3158
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v4

    .line 3159
    .local v4, "p":Landroid/view/WindowManager$LayoutParams;
    const/16 v6, 0x3e8

    iput v6, v4, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 3160
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v6

    iput-object v6, v4, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 3161
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v6}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 3162
    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    .end local v1    # "dialogBuilder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "dragAndDropHelpPopupView":Landroid/view/View;
    .end local v3    # "layoutInflater":Landroid/view/LayoutInflater;
    .end local v4    # "p":Landroid/view/WindowManager$LayoutParams;
    .end local v5    # "w":Landroid/view/Window;
    :catch_0
    move-exception v6

    goto/16 :goto_0
.end method

.method private prepareCenterBarAnimation()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2512
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getGuidelineWindow()Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    .line 2513
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    if-nez v3, :cond_0

    .line 2545
    :goto_0
    return v2

    .line 2517
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0056

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2518
    .local v1, "positionGuidelineShadow":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 2519
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    rsub-int/lit8 v3, v1, 0x0

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 2520
    rsub-int/lit8 v3, v1, 0x0

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2521
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    mul-int/lit8 v4, v1, 0x2

    add-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2522
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    mul-int/lit8 v4, v1, 0x2

    add-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2523
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v3, :cond_1

    .line 2524
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getApplistIndicatorSize()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2525
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->getApplistIndicatorSize()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2527
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2528
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2529
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2531
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v2, :cond_2

    .line 2532
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    const v3, 0x7f0f00a1

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalAnimIcon:Landroid/widget/ImageView;

    .line 2533
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    const v3, 0x7f0f00a2

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppSwitchingAnimIcon:Landroid/widget/ImageView;

    .line 2534
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    const v3, 0x7f0f00a3

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchAnimIcon:Landroid/widget/ImageView;

    .line 2535
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    const v3, 0x7f0f00a4

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAnimIcon:Landroid/widget/ImageView;

    .line 2536
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    const v3, 0x7f0f00a5

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullAnimIcon:Landroid/widget/ImageView;

    .line 2537
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    const v3, 0x7f0f00a6

    invoke-virtual {v2, v3}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseAnimIcon:Landroid/widget/ImageView;

    .line 2538
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalAnimIcon:Landroid/widget/ImageView;

    const v3, 0x7f02000b

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2539
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppSwitchingAnimIcon:Landroid/widget/ImageView;

    const v3, 0x7f02000c

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2540
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchAnimIcon:Landroid/widget/ImageView;

    const v3, 0x7f02000d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2541
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAnimIcon:Landroid/widget/ImageView;

    const v3, 0x7f020007

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2542
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullAnimIcon:Landroid/widget/ImageView;

    const v3, 0x7f020009

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2543
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseAnimIcon:Landroid/widget/ImageView;

    const v3, 0x7f020004

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2545
    :cond_2
    const/4 v2, 0x1

    goto/16 :goto_0
.end method

.method private setButtonsVisibility(Z)V
    .locals 3
    .param p1, "bAnim"    # Z

    .prologue
    .line 2030
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 2031
    .local v1, "v":Landroid/view/View;
    if-eqz p1, :cond_0

    .line 2032
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2034
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2037
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private setCenterBarPoint()V
    .locals 2

    .prologue
    .line 920
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkSealedFixedWindow(Z)V

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 926
    return-void
.end method

.method private setHoverMarginOfButtons()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2491
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.sec.feature.cocktailbar"

    invoke-virtual {v4, v5}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2492
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 2493
    .local v0, "degrees":I
    const/4 v2, 0x0

    .line 2494
    .local v2, "yOffset":I
    if-ne v0, v1, :cond_0

    .line 2495
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0176

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2498
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2499
    .local v1, "isHoverEnabled":Z
    :goto_0
    if-eqz v1, :cond_1

    .line 2500
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mExternalButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2501
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSwitchButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2502
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2503
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRecentsButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2504
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2505
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCloseButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2506
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCollapseButton:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v4

    neg-int v5, v2

    invoke-virtual {v4, v3, v5}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2509
    .end local v0    # "degrees":I
    .end local v1    # "isHoverEnabled":Z
    .end local v2    # "yOffset":I
    :cond_1
    return-void

    .restart local v0    # "degrees":I
    .restart local v2    # "yOffset":I
    :cond_2
    move v1, v3

    .line 2498
    goto :goto_0
.end method

.method private setLayoutMarginButtons()V
    .locals 29

    .prologue
    .line 2212
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2213
    .local v3, "btnLayoutMargin":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/ViewGroup$MarginLayoutParams;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v22

    .local v22, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v26

    if-eqz v26, :cond_0

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/View;

    .line 2214
    .local v25, "v":Landroid/view/View;
    new-instance v26, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual/range {v25 .. v25}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2217
    .end local v25    # "v":Landroid/view/View;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a000b

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v4, v0

    .line 2218
    .local v4, "buttonArrangeMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a000c

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v5, v0

    .line 2219
    .local v5, "buttonArrangeMargin5b2pL":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a000d

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v6, v0

    .line 2220
    .local v6, "buttonArrangeMargin5b2pR":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a000e

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v7, v0

    .line 2221
    .local v7, "buttonArrangeOverlapMargin":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a000a

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v8, v0

    .line 2222
    .local v8, "buttonBgHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0032

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v9, v0

    .line 2223
    .local v9, "hoverMargin3b1p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0033

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v10, v0

    .line 2224
    .local v10, "hoverMargin3b2p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0034

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v11, v0

    .line 2225
    .local v11, "hoverMargin3b3p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0035

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v12, v0

    .line 2226
    .local v12, "hoverMargin4b1p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0036

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v13, v0

    .line 2227
    .local v13, "hoverMargin4b2p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0037

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v14, v0

    .line 2228
    .local v14, "hoverMargin4b3p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0038

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v15, v0

    .line 2229
    .local v15, "hoverMargin4b4p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a0039

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v16, v0

    .line 2230
    .local v16, "hoverMargin5b1p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a003a

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v17, v0

    .line 2231
    .local v17, "hoverMargin5b2p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a003b

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v18, v0

    .line 2232
    .local v18, "hoverMargin5b3p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a003c

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v19, v0

    .line 2233
    .local v19, "hoverMargin5b4p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    const v27, 0x7f0a003d

    invoke-virtual/range {v26 .. v27}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v26

    move/from16 v0, v26

    float-to-int v0, v0

    move/from16 v20, v0

    .line 2234
    .local v20, "hoverMargin5b5p":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    invoke-virtual/range {v26 .. v26}, Ljava/util/ArrayList;->size()I

    move-result v24

    .line 2235
    .local v24, "size":I
    const/16 v23, 0x0

    .line 2237
    .local v23, "isHoverEnabled":Z
    if-lez v24, :cond_1

    .line 2238
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    if-eqz v26, :cond_5

    const/16 v23, 0x1

    .line 2241
    :cond_1
    :goto_1
    const/16 v26, 0x5

    move/from16 v0, v24

    move/from16 v1, v26

    if-ne v0, v1, :cond_d

    .line 2242
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_8

    .line 2243
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_6

    .line 2246
    :cond_2
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2247
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2248
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2249
    const/16 v26, 0x4

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2250
    if-eqz v23, :cond_3

    .line 2251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v16

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v17

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2253
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v18

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2254
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x4

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v20

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2271
    :cond_3
    :goto_2
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2272
    const/16 v26, 0x4

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2485
    :cond_4
    :goto_3
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_4
    move/from16 v0, v21

    move/from16 v1, v24

    if-ge v0, v1, :cond_25

    .line 2486
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    new-instance v28, Landroid/widget/LinearLayout$LayoutParams;

    move/from16 v0, v21

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2485
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 2238
    .end local v21    # "i":I
    :cond_5
    const/16 v23, 0x0

    goto/16 :goto_1

    .line 2257
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_3

    .line 2260
    :cond_7
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2261
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2262
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2263
    if-eqz v23, :cond_3

    .line 2264
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v16

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v17

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v18

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2267
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v19

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2268
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x4

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v20

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_2

    .line 2274
    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_b

    .line 2277
    :cond_9
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2278
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2279
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2280
    const/16 v26, 0x4

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2281
    if-eqz v23, :cond_a

    .line 2282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v16, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v17, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v18, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2285
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v19, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2286
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x4

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v20, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2302
    :cond_a
    :goto_5
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2303
    const/16 v26, 0x4

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto/16 :goto_3

    .line 2288
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_a

    .line 2291
    :cond_c
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2292
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2293
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2294
    if-eqz v23, :cond_a

    .line 2295
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v16, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2296
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v17, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2297
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v18, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v19, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x4

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v20, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_5

    .line 2305
    :cond_d
    const/16 v26, 0x4

    move/from16 v0, v24

    move/from16 v1, v26

    if-ne v0, v1, :cond_15

    .line 2306
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_11

    .line 2307
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_e

    .line 2308
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2309
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2310
    if-eqz v23, :cond_4

    .line 2311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2312
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2313
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2314
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2316
    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_f

    .line 2317
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2318
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2319
    if-eqz v23, :cond_4

    .line 2320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2321
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2322
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2323
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2325
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_10

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 2329
    :cond_10
    if-eqz v23, :cond_4

    .line 2330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2331
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2332
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2333
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2337
    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_12

    .line 2338
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2339
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2340
    if-eqz v23, :cond_4

    .line 2341
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v12, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2342
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v13, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2343
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v14, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2344
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v15, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2346
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_13

    .line 2347
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2348
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2349
    if-eqz v23, :cond_4

    .line 2350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v12, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2351
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v13, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2352
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v14, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2353
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v15, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2355
    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_14

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 2359
    :cond_14
    if-eqz v23, :cond_4

    .line 2360
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2361
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2362
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2363
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2367
    :cond_15
    const/16 v26, 0x3

    move/from16 v0, v24

    move/from16 v1, v26

    if-ne v0, v1, :cond_1d

    .line 2368
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_19

    .line 2369
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_16

    .line 2370
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2371
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2372
    if-eqz v23, :cond_4

    .line 2373
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2374
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2375
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2377
    :cond_16
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_17

    .line 2378
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2379
    if-eqz v23, :cond_4

    .line 2380
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2381
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2382
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2384
    :cond_17
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 2388
    :cond_18
    if-eqz v23, :cond_4

    .line 2389
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2390
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2391
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2395
    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1a

    .line 2396
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2397
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2398
    if-eqz v23, :cond_4

    .line 2399
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v9, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v10, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2401
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v11, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2403
    :cond_1a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1b

    .line 2404
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2405
    if-eqz v23, :cond_4

    .line 2406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v9, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2407
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v10, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v11, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2410
    :cond_1b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_1c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 2414
    :cond_1c
    if-eqz v23, :cond_4

    .line 2415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v9, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v10, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2417
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v11, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2422
    :cond_1d
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_21

    .line 2423
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1e

    .line 2424
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2425
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2426
    if-eqz v23, :cond_4

    .line 2427
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2428
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2429
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2430
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v4, v27

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2432
    :cond_1e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_1f

    .line 2433
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2434
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2435
    if-eqz v23, :cond_4

    .line 2436
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2438
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    const/16 v27, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2439
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v4

    move/from16 v27, v0

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2441
    :cond_1f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 2445
    :cond_20
    if-eqz v23, :cond_4

    .line 2446
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2447
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2448
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2453
    :cond_21
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x3

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_22

    .line 2454
    const/16 v26, 0x0

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2455
    const/16 v26, 0x3

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2456
    if-eqz v23, :cond_4

    .line 2457
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v12, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2458
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v13, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2459
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v14, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2460
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    add-int v27, v27, v4

    sub-int v28, v15, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2462
    :cond_22
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0xc

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_23

    .line 2463
    const/16 v26, 0x1

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2464
    const/16 v26, 0x2

    move/from16 v0, v26

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/ViewGroup$MarginLayoutParams;

    move-object/from16 v0, v26

    iput v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 2465
    if-eqz v23, :cond_4

    .line 2466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v12, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2467
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v13, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2468
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v28, v14, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2469
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    div-int/lit8 v27, v8, 0x2

    sub-int v27, v27, v4

    sub-int v28, v15, v8

    invoke-virtual/range {v26 .. v28}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2471
    :cond_23
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x1

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x4

    move/from16 v0, v26

    move/from16 v1, v27

    if-eq v0, v1, :cond_24

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    move/from16 v26, v0

    const/16 v27, 0x8

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    .line 2475
    :cond_24
    if-eqz v23, :cond_4

    .line 2476
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x1

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v13, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x2

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v14, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 2479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    const/16 v27, 0x3

    invoke-virtual/range {v26 .. v27}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Landroid/view/View;

    invoke-virtual/range {v26 .. v26}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v26

    neg-int v0, v8

    move/from16 v27, v0

    div-int/lit8 v27, v27, 0x2

    move-object/from16 v0, v26

    move/from16 v1, v27

    invoke-virtual {v0, v15, v1}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    goto/16 :goto_3

    .line 2488
    .restart local v21    # "i":I
    :cond_25
    return-void
.end method

.method private showButtonPopupWindow()V
    .locals 1

    .prologue
    .line 1873
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showButtonPopupWindow(Z)V

    .line 1874
    return-void
.end method

.method private showButtonPopupWindow(Z)V
    .locals 26
    .param p1, "autoCloseAnim"    # Z

    .prologue
    .line 1881
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2027
    :cond_0
    :goto_0
    return-void

    .line 1886
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1889
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDoubleTabbed:Z

    if-nez v3, :cond_0

    .line 1892
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsInLongPress:Z

    if-nez v3, :cond_0

    .line 1895
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getFocusedZone()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    .line 1896
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    if-eqz v3, :cond_0

    .line 1899
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarFixed:Z

    if-nez v3, :cond_0

    .line 1904
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->prepareCenterBarAnimation()Z

    move-result v3

    if-nez v3, :cond_3

    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v3, :cond_0

    .line 1908
    :cond_3
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v3, :cond_4

    .line 1909
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSoundPool:Landroid/media/SoundPool;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->centerBarButtonSoundId:I

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual/range {v3 .. v9}, Landroid/media/SoundPool;->play(IFFIIF)I

    .line 1912
    :cond_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->initButtonPopup()V

    .line 1915
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v4, 0x7f0f0074

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v19

    .line 1916
    .local v19, "leftImageView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v4, 0x7f0f0076

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    .line 1917
    .local v23, "rightImageView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const v4, 0x7f0f0075

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    .line 1918
    .local v16, "centerImageView":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 1919
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    .line 1920
    .local v15, "centerImageParams":Landroid/view/ViewGroup$LayoutParams;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0177

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v20

    .line 1921
    .local v20, "padding":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_6

    .line 1922
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v3

    mul-int/lit8 v4, v20, 0x2

    add-int/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v15, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1927
    :goto_1
    new-instance v3, Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    const/4 v5, -0x2

    const/4 v6, -0x2

    invoke-direct {v3, v4, v5, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    .line 1929
    const/16 v21, 0x0

    .line 1930
    .local v21, "posX":I
    const/16 v22, 0x0

    .line 1931
    .local v22, "posY":I
    const/16 v17, 0x0

    .line 1932
    .local v17, "gravity":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0009

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v14, v3

    .line 1933
    .local v14, "buttonsMargin":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a000a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v12, v3

    .line 1934
    .local v12, "buttonBgHeight":I
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a000b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v11, v3

    .line 1936
    .local v11, "buttonArrangeMargin":I
    new-instance v13, Lcom/sec/android/app/FlashBarService/CenterBarWindow$18;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$18;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 1944
    .local v13, "buttonHoverListener":Landroid/view/View$OnHoverListener;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_2
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    .line 1945
    .local v10, "b":Landroid/view/View;
    invoke-virtual {v10}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 1946
    invoke-virtual {v10, v13}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1947
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v3, :cond_5

    .line 1948
    invoke-virtual {v10}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    goto :goto_2

    .line 1924
    .end local v10    # "b":Landroid/view/View;
    .end local v11    # "buttonArrangeMargin":I
    .end local v12    # "buttonBgHeight":I
    .end local v13    # "buttonHoverListener":Landroid/view/View$OnHoverListener;
    .end local v14    # "buttonsMargin":I
    .end local v17    # "gravity":I
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v21    # "posX":I
    .end local v22    # "posY":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v3

    mul-int/lit8 v4, v20, 0x2

    add-int/2addr v3, v4

    invoke-virtual/range {v19 .. v19}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iput v3, v15, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_1

    .line 1953
    .restart local v11    # "buttonArrangeMargin":I
    .restart local v12    # "buttonBgHeight":I
    .restart local v13    # "buttonHoverListener":Landroid/view/View$OnHoverListener;
    .restart local v14    # "buttonsMargin":I
    .restart local v17    # "gravity":I
    .restart local v18    # "i$":Ljava/util/Iterator;
    .restart local v21    # "posX":I
    .restart local v22    # "posY":I
    :cond_7
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v3, :cond_17

    .line 1954
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_d

    .line 1955
    const/16 v17, 0x30

    .line 1956
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_b

    .line 1959
    :cond_8
    add-int v3, v12, v14

    neg-int v0, v3

    move/from16 v22, v0

    .line 1979
    :cond_9
    :goto_3
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setLayoutMarginButtons()V

    .line 1980
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    if-nez v3, :cond_11

    const/4 v3, 0x1

    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setButtonsVisibility(Z)V

    .line 1981
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    if-nez v3, :cond_16

    .line 1982
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v24

    .line 1983
    .local v24, "size":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_a

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_13

    .line 1987
    :cond_a
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableButtons:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    :goto_5
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Landroid/view/View;

    .line 1988
    .local v25, "v":Landroid/view/View;
    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 1960
    .end local v24    # "size":I
    .end local v25    # "v":Landroid/view/View;
    :cond_b
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/16 v4, 0xc

    if-eq v3, v4, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_c

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_9

    .line 1963
    :cond_c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    add-int/2addr v3, v14

    sub-int v22, v3, v11

    goto :goto_3

    .line 1966
    :cond_d
    const/16 v17, 0x3

    .line 1967
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_e

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_e

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_f

    .line 1970
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1971
    add-int v3, v12, v14

    neg-int v0, v3

    move/from16 v21, v0

    goto/16 :goto_3

    .line 1972
    :cond_f
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/16 v4, 0xc

    if-eq v3, v4, :cond_10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_10

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    const/16 v4, 0x8

    if-ne v3, v4, :cond_9

    .line 1975
    :cond_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarButtonsContainer:Landroid/widget/LinearLayout;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1976
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    add-int/2addr v3, v14

    sub-int v21, v3, v11

    goto/16 :goto_3

    .line 1980
    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_4

    .line 1990
    .restart local v24    # "size":I
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2021
    .end local v24    # "size":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    move/from16 v0, v17

    move/from16 v1, v21

    move/from16 v2, v22

    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 2022
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    .line 2024
    sget-boolean v3, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v3, :cond_0

    .line 2025
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v3, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animateCenterBarOpen(Landroid/view/View;Z)V

    goto/16 :goto_0

    .line 1992
    .restart local v24    # "size":I
    :cond_13
    const/4 v3, 0x5

    move/from16 v0, v24

    if-ne v0, v3, :cond_14

    .line 1993
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animationCenterBar5Buttons()V

    goto :goto_6

    .line 1994
    :cond_14
    const/4 v3, 0x3

    move/from16 v0, v24

    if-ne v0, v3, :cond_15

    .line 1995
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animationCenterBar3Buttons()V

    goto :goto_6

    .line 1997
    :cond_15
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animationCenterBar4Buttons()V

    goto :goto_6

    .line 2001
    .end local v24    # "size":I
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 2004
    :cond_17
    const/16 v17, 0x11

    .line 2005
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2006
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setHoverMarginOfButtons()V

    .line 2008
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 2009
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>()V

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2010
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    new-instance v4, Lcom/sec/android/app/FlashBarService/CenterBarWindow$19;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$19;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v3, v4}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_6
.end method

.method private showDragAndDropHelpDialog()V
    .locals 1

    .prologue
    .line 3168
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3174
    :goto_0
    return-void

    .line 3172
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeDragAndDropHelpPopupLayout()V

    .line 3173
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow()V

    goto :goto_0
.end method

.method private startButtonAnimation(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/animation/AnimationSet;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 3042
    .local p1, "animatinSet":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/animation/AnimationSet;>;"
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 3043
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 3044
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3045
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 3046
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/animation/Animation;

    invoke-virtual {v2, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 3045
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3050
    .end local v0    # "N":I
    .end local v1    # "index":I
    :cond_0
    return-void
.end method

.method private startDragAndDrop()V
    .locals 3

    .prologue
    .line 3195
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 3196
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.sec.android.app.FlashBarService"

    const-string v2, "com.sec.android.app.FlashBarService.SmartClipDragDrop"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 3197
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startServiceAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)Landroid/content/ComponentName;

    .line 3198
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 3199
    return-void
.end method

.method private startHideButtonTimer()V
    .locals 4

    .prologue
    const/16 v2, 0xc9

    .line 1539
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1540
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1541
    :cond_0
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-nez v0, :cond_1

    .line 1542
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-static {v1, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1544
    :cond_1
    return-void
.end method


# virtual methods
.method public cancelHideButtonTimer()V
    .locals 2

    .prologue
    const/16 v1, 0xc9

    .line 1547
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1548
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1549
    :cond_0
    return-void
.end method

.method public centerControlBarDocking(Z)Z
    .locals 8
    .param p1, "checkAll"    # Z

    .prologue
    const/4 v7, 0x1

    .line 931
    const/4 v3, 0x0

    .line 932
    .local v3, "result":Z
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDockingSize:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v5, v5, 0x2

    add-int v1, v4, v5

    .line 933
    .local v1, "dockingRange":I
    const/4 v2, 0x0

    .line 934
    .local v2, "dockingWidth":I
    const/4 v0, 0x0

    .line 936
    .local v0, "dockingHeight":I
    sget-boolean v4, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_RECENT_UI:Z

    if-eqz v4, :cond_7

    .line 937
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v4, v7, :cond_6

    .line 938
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    .line 939
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    mul-int/lit8 v4, v4, 0x5

    div-int/lit8 v0, v4, 0x64

    .line 949
    :goto_0
    if-nez p1, :cond_0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 950
    :cond_0
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v5

    add-int/2addr v5, v0

    if-gt v4, v5, :cond_8

    .line 951
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v5

    add-int/2addr v5, v0

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 952
    const/4 v3, 0x1

    .line 963
    :cond_1
    :goto_1
    if-nez p1, :cond_2

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v4, v7, :cond_5

    .line 964
    :cond_2
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    if-gt v4, v2, :cond_3

    .line 965
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iput v2, v4, Landroid/graphics/Point;->x:I

    .line 966
    const/4 v3, 0x1

    .line 968
    :cond_3
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    sub-int/2addr v5, v2

    if-lt v4, v5, :cond_4

    .line 969
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    sub-int/2addr v5, v2

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 970
    const/4 v3, 0x1

    .line 972
    :cond_4
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v5, v5, 0x2

    sub-int/2addr v5, v1

    if-lt v4, v5, :cond_5

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v1

    if-gt v4, v5, :cond_5

    .line 974
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v5, v5, 0x2

    iput v5, v4, Landroid/graphics/Point;->x:I

    .line 975
    const/4 v3, 0x1

    .line 979
    :cond_5
    return v3

    .line 941
    :cond_6
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    mul-int/lit8 v4, v4, 0x5

    div-int/lit8 v2, v4, 0x64

    .line 942
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    goto :goto_0

    .line 945
    :cond_7
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    .line 946
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    goto :goto_0

    .line 953
    :cond_8
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    sub-int/2addr v5, v0

    if-lt v4, v5, :cond_9

    .line 954
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    sub-int/2addr v5, v0

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 955
    const/4 v3, 0x1

    goto :goto_1

    .line 956
    :cond_9
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v6

    add-int/2addr v5, v6

    sub-int/2addr v5, v1

    if-lt v4, v5, :cond_1

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v5, v1

    if-gt v4, v5, :cond_1

    .line 958
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 959
    const/4 v3, 0x1

    goto/16 :goto_1
.end method

.method public checkFixedBoundsForDimLayer()Z
    .locals 9

    .prologue
    const v8, 0x3f4ccccd    # 0.8f

    const v7, 0x3e4ccccd    # 0.2f

    const/4 v5, 0x1

    .line 983
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    int-to-float v6, v6

    mul-float/2addr v6, v7

    float-to-int v3, v6

    .line 984
    .local v3, "MIN_FIXED_WIDTH":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    int-to-float v6, v6

    mul-float/2addr v6, v8

    float-to-int v1, v6

    .line 986
    .local v1, "MAX_FIXED_WIDTH":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    int-to-float v6, v6

    mul-float/2addr v6, v7

    float-to-int v2, v6

    .line 987
    .local v2, "MIN_FIXED_HEIGHT":I
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    int-to-float v6, v6

    mul-float/2addr v6, v8

    float-to-int v0, v6

    .line 989
    .local v0, "MAX_FIXED_HEIGHT":I
    const-string v4, ""

    .line 990
    .local v4, "detailLog":Ljava/lang/String;
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v6, v5, :cond_1

    .line 993
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    if-ge v2, v6, :cond_0

    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    if-gt v0, v6, :cond_1

    .line 1008
    :cond_0
    :goto_0
    return v5

    .line 998
    :cond_1
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_2

    .line 1001
    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    if-ge v3, v6, :cond_0

    iget v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    if-le v1, v6, :cond_0

    .line 1008
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public checkSealedFixedWindow(Z)V
    .locals 5
    .param p1, "forceUpdate"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3208
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz p1, :cond_3

    .line 3209
    :cond_0
    const/16 v4, 0x1b9

    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getKnoxSealedMultiWindowFixedState(I)I

    move-result v4

    if-ne v4, v2, :cond_4

    move v0, v2

    .line 3210
    .local v0, "sealedMultiWindowFixed":Z
    :goto_0
    const/16 v4, 0x1ba

    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getKnoxSealedMultiWindowFixedState(I)I

    move-result v1

    .line 3211
    .local v1, "splitPercentage":I
    if-nez v0, :cond_1

    .line 3212
    const/16 v1, 0x32

    .line 3214
    :cond_1
    if-nez v0, :cond_2

    if-eqz p1, :cond_3

    .line 3215
    :cond_2
    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v4, v2, :cond_5

    .line 3216
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    mul-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 3217
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    iput v4, v2, Landroid/graphics/Point;->y:I

    .line 3218
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    mul-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 3219
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iput v4, v2, Landroid/graphics/Point;->x:I

    .line 3226
    :goto_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setCenterBarPoint(ILandroid/graphics/Point;)V

    .line 3227
    const-string v2, "CenterBarWindow"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Sealed Fixed mode - centre bar position ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " , "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3230
    .end local v0    # "sealedMultiWindowFixed":Z
    .end local v1    # "splitPercentage":I
    :cond_3
    return-void

    :cond_4
    move v0, v3

    .line 3209
    goto :goto_0

    .line 3221
    .restart local v0    # "sealedMultiWindowFixed":Z
    .restart local v1    # "splitPercentage":I
    :cond_5
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    mul-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 3222
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iput v4, v2, Landroid/graphics/Point;->x:I

    .line 3223
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    mul-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 3224
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    iput v4, v2, Landroid/graphics/Point;->y:I

    goto :goto_1
.end method

.method public drawCenterBar(II)V
    .locals 5
    .param p1, "positionX"    # I
    .param p2, "positionY"    # I

    .prologue
    .line 719
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    if-nez v3, :cond_0

    .line 754
    :goto_0
    return-void

    .line 732
    :cond_0
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarPressed:Z

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setImageToImageView(Z)V

    .line 734
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 735
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 736
    .local v0, "centerBarLayout":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 738
    .local v2, "transparentMargin":I
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    if-gtz v3, :cond_1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int p1, v3, v2

    .line 739
    :cond_1
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p1

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    if-lt v3, v4, :cond_2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    add-int p1, v3, v2

    .line 740
    :cond_2
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p2, v3

    if-gtz v3, :cond_3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int p2, v3, v2

    .line 741
    :cond_3
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, p2

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    if-lt v3, v4, :cond_4

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    add-int p2, v3, v2

    .line 743
    :cond_4
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 744
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, p2, v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 746
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 747
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 748
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 750
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 751
    :catch_0
    move-exception v1

    .line 752
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method public drawCenterBarButton(II)V
    .locals 0
    .param p1, "positionX"    # I
    .param p2, "positionY"    # I

    .prologue
    .line 859
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 860
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showButtonPopupWindow()V

    .line 861
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startHideButtonTimer()V

    .line 862
    return-void
.end method

.method public drawDragAndDrop()V
    .locals 2

    .prologue
    .line 670
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragMode:Z

    .line 672
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 686
    :goto_0
    return-void

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    .line 678
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 679
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v0, :cond_1

    .line 680
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const v1, 0x7f0200c1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 684
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 685
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    .line 682
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const v1, 0x7f0200bf

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method

.method public getCenterBarPoint()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 915
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    return-object v0
.end method

.method public getFocusedZoneInfo()I
    .locals 1

    .prologue
    .line 898
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    return v0
.end method

.method public getIndicatorSize()I
    .locals 1

    .prologue
    .line 3008
    const/4 v0, 0x0

    return v0
.end method

.method public hideButtonsPopupWindow()V
    .locals 1

    .prologue
    .line 865
    sget-boolean v0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 866
    return-void
.end method

.method public hideButtonsPopupWindow(Z)V
    .locals 3
    .param p1, "needAnim"    # Z

    .prologue
    .line 869
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v2, :cond_0

    .line 870
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->clearButtonAnimation()V

    .line 872
    sget-boolean v2, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_SCALE_PREVIEW:Z

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    .line 873
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarButtons:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->animateCenterBarClose(Landroid/view/View;)V

    .line 883
    :cond_0
    :goto_0
    return-void

    .line 875
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAvailableAnims:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 876
    .local v1, "v":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 878
    .end local v1    # "v":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->dismiss()V

    .line 879
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mButtonsPopupWindow:Landroid/widget/PopupWindow;

    .line 880
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    goto :goto_0
.end method

.method public hideDragAndDropHelpDialog()V
    .locals 1

    .prologue
    .line 3177
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3178
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 3181
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    .line 3182
    return-void
.end method

.method public hideGuideCenterBar()V
    .locals 1

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    if-eqz v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->dismiss()V

    .line 852
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mGuideView:Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;

    .line 853
    return-void
.end method

.method public initCenterBarButtonAnim()V
    .locals 4

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    new-instance v1, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1153
    return-void
.end method

.method public initCenterBarIfNeed()V
    .locals 12

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1087
    const/4 v4, 0x0

    .line 1088
    .local v4, "level":I
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v5

    .line 1089
    .local v5, "mode":I
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 1091
    .local v2, "displaySize":Landroid/graphics/Point;
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    if-nez v5, :cond_2

    .line 1092
    const/4 v4, 0x0

    .line 1100
    :goto_0
    invoke-static {}, Landroid/hardware/display/DisplayManagerGlobal;->getInstance()Landroid/hardware/display/DisplayManagerGlobal;

    move-result-object v10

    invoke-virtual {v10, v9}, Landroid/hardware/display/DisplayManagerGlobal;->getRealDisplay(I)Landroid/view/Display;

    move-result-object v1

    .line 1101
    .local v1, "display":Landroid/view/Display;
    if-eqz v1, :cond_0

    invoke-virtual {v1, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1102
    :cond_0
    iget v10, v2, Landroid/graphics/Point;->x:I

    if-lez v10, :cond_1

    iget v10, v2, Landroid/graphics/Point;->y:I

    if-gtz v10, :cond_3

    .line 1135
    .end local v1    # "display":Landroid/view/Display;
    :cond_1
    :goto_1
    return-void

    .line 1093
    :cond_2
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    if-ne v5, v8, :cond_1

    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSupportQuadView:Z

    if-eqz v10, :cond_1

    .line 1094
    const/4 v4, 0x1

    goto :goto_0

    .line 1107
    .restart local v1    # "display":Landroid/view/Display;
    :cond_3
    const/4 v0, 0x0

    .line 1108
    .local v0, "changed":Z
    iget v10, v2, Landroid/graphics/Point;->x:I

    iget v11, v2, Landroid/graphics/Point;->y:I

    if-le v10, v11, :cond_9

    move v3, v8

    .line 1109
    .local v3, "isLand":Z
    :goto_2
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->x:I

    int-to-float v10, v10

    iget v11, v2, Landroid/graphics/Point;->x:I

    int-to-float v11, v11

    div-float v6, v10, v11

    .line 1110
    .local v6, "xAxis":F
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    int-to-float v10, v10

    iget v11, v2, Landroid/graphics/Point;->y:I

    int-to-float v11, v11

    div-float v7, v10, v11

    .line 1112
    .local v7, "yAxis":F
    if-eqz v3, :cond_4

    if-eqz v4, :cond_5

    :cond_4
    if-nez v3, :cond_a

    if-ne v4, v8, :cond_a

    .line 1114
    :cond_5
    iget v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSplitMinWeight:F

    cmpg-float v8, v6, v8

    if-lez v8, :cond_6

    iget v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSplitMaxWeight:F

    cmpl-float v8, v6, v8

    if-ltz v8, :cond_7

    .line 1115
    :cond_6
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v10, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v10, v10, 0x2

    iput v10, v8, Landroid/graphics/Point;->x:I

    .line 1116
    const/4 v0, 0x1

    .line 1127
    :cond_7
    :goto_3
    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkSealedFixedWindow(Z)V

    .line 1129
    if-eqz v0, :cond_8

    .line 1130
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    invoke-virtual {v8, v9, v10}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->setCenterBarPoint(ILandroid/graphics/Point;)V

    .line 1131
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v10, v10, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v8, v10}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 1134
    :cond_8
    iput-boolean v9, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFullSizeDocking:Z

    goto :goto_1

    .end local v3    # "isLand":Z
    .end local v6    # "xAxis":F
    .end local v7    # "yAxis":F
    :cond_9
    move v3, v9

    .line 1108
    goto :goto_2

    .line 1120
    .restart local v3    # "isLand":Z
    .restart local v6    # "xAxis":F
    .restart local v7    # "yAxis":F
    :cond_a
    iget v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSplitMinWeight:F

    cmpg-float v8, v7, v8

    if-lez v8, :cond_b

    iget v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSplitMaxWeight:F

    cmpl-float v8, v7, v8

    if-ltz v8, :cond_7

    .line 1121
    :cond_b
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v10, v2, Landroid/graphics/Point;->y:I

    div-int/lit8 v10, v10, 0x2

    iput v10, v8, Landroid/graphics/Point;->y:I

    .line 1122
    const/4 v0, 0x1

    goto :goto_3
.end method

.method public initVariables()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 429
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    if-eqz v2, :cond_1

    .line 430
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 431
    .local v1, "displaySize":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 432
    iget v2, v1, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    .line 433
    iget v2, v1, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    .line 435
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/android/multiwindow/MultiWindowFeatures;->isSupportQuadView(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 436
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingNonQuadRatio:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingProportion:I

    .line 438
    :cond_0
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingProportion:I

    mul-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    .line 439
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    mul-int/lit8 v2, v2, 0x14

    div-int/lit8 v2, v2, 0x64

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    .line 441
    .end local v1    # "displaySize":Landroid/graphics/Point;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    .line 442
    const/4 v0, 0x0

    .line 443
    .local v0, "centerBarPoint":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v2, v5}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v0

    .line 444
    if-nez v0, :cond_2

    .line 467
    :goto_0
    return-void

    .line 447
    :cond_2
    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    .line 448
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    .line 449
    iget v2, v0, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 450
    if-eqz v0, :cond_3

    .line 451
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 464
    :goto_1
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkSealedFixedWindow(Z)V

    goto :goto_0

    .line 453
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->getIndicatorSize()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a002c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    float-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    goto :goto_1

    .line 456
    :cond_4
    iget v2, v0, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 457
    if-eqz v0, :cond_5

    .line 458
    iget v2, v0, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    goto :goto_1

    .line 460
    :cond_5
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    div-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    goto :goto_1
.end method

.method public makeCenterBarImageViews()V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mViewForLayout:Landroid/view/View;

    const v1, 0x7f0f0066

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    .line 521
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 522
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 523
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 525
    :cond_0
    return-void
.end method

.method public makeCenterBarLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 483
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 484
    .local v0, "centerBarLayout":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 485
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 486
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 487
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 488
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 489
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 490
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MultiWindowTrayService/CenterBar "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 491
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/view/WindowManager$LayoutParams;->hasSystemUiListeners:Z

    .line 492
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 493
    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 495
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 496
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 497
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    const v2, 0x106000d

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 498
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mViewForLayout:Landroid/view/View;

    const v2, 0x7f0f0065

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFrame:Landroid/widget/FrameLayout;

    .line 500
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFrame:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow$3;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 506
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFrame:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 512
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeCenterBarImageViews()V

    .line 513
    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setImageToImageView(Z)V

    .line 514
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 9
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 1407
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-eq v3, v4, :cond_3

    .line 1408
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v4, 0xcc

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1409
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v4, 0xcc

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1411
    :cond_0
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 1412
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1413
    iget v3, v0, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    .line 1414
    iget v3, v0, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    .line 1415
    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    .line 1416
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v3, v8, :cond_4

    .line 1417
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingProportion:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x64

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    .line 1418
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    mul-int/lit8 v3, v3, 0x14

    div-int/lit8 v3, v3, 0x64

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    .line 1426
    :cond_1
    :goto_0
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 1427
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    if-nez v3, :cond_5

    .line 1428
    iput v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    .line 1438
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    if-nez v3, :cond_8

    .line 1484
    .end local v0    # "displaySize":Landroid/graphics/Point;
    :cond_3
    :goto_2
    return-void

    .line 1420
    .restart local v0    # "displaySize":Landroid/graphics/Point;
    :cond_4
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    if-ne v3, v5, :cond_1

    .line 1421
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayWidth:I

    mul-int/lit8 v3, v3, 0x14

    div-int/lit8 v3, v3, 0x64

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeW:I

    .line 1422
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayHeight:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingProportion:I

    mul-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x64

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarFlingSizeH:I

    goto :goto_0

    .line 1429
    :cond_5
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    if-ne v3, v8, :cond_6

    .line 1430
    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    goto :goto_1

    .line 1431
    :cond_6
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    if-ne v3, v5, :cond_7

    .line 1432
    iput v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    goto :goto_1

    .line 1433
    :cond_7
    iget v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    if-ne v3, v7, :cond_2

    .line 1434
    iput v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarDirection:I

    goto :goto_1

    .line 1442
    :cond_8
    const/4 v2, 0x0

    .line 1443
    .local v2, "point":Landroid/graphics/Point;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3, v6}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v2

    .line 1445
    if-eqz v2, :cond_9

    .line 1446
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Point;->set(II)V

    .line 1447
    iget v3, v2, Landroid/graphics/Point;->x:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    .line 1448
    iget v3, v2, Landroid/graphics/Point;->y:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    .line 1449
    const-string v3, "CenterBarWindow"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onConfigurationChanged point = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    :cond_9
    invoke-virtual {p0, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkSealedFixedWindow(Z)V

    .line 1454
    invoke-virtual {p0, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setImageToImageView(Z)V

    .line 1455
    invoke-virtual {p0, v6}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 1457
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    if-nez v3, :cond_d

    .line 1459
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWM:Landroid/view/IWindowManager;

    invoke-interface {v3}, Landroid/view/IWindowManager;->isNavigationBarVisible()Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1460
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v4, 0xcf

    invoke-virtual {v3, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1461
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v4, 0xcf

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 1463
    :cond_a
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v5, 0xcf

    invoke-static {v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v4

    const-wide/16 v6, 0x12c

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1473
    :goto_3
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragMode:Z

    if-eqz v3, :cond_b

    .line 1474
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawDragAndDrop()V

    .line 1476
    :cond_b
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDragAndDropHelpPopupDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 1477
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->rotateDragAndDropHelpDialog()V

    .line 1481
    :goto_4
    iput-boolean v8, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mActionCancel:Z

    .line 1482
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V

    goto/16 :goto_2

    .line 1465
    :cond_c
    :try_start_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 1467
    :catch_0
    move-exception v1

    .line 1468
    .local v1, "e":Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_3

    .line 1471
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_d
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBarButton(II)V

    goto :goto_3

    .line 1479
    :cond_e
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideDragAndDropHelpDialog()V

    goto :goto_4
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3012
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarEventReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 3014
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 3017
    :goto_0
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarEventReceiver:Landroid/content/BroadcastReceiver;

    .line 3019
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 3021
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 3024
    :goto_1
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 3027
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->cancelHideButtonTimer()V

    .line 3029
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSoundPool:Landroid/media/SoundPool;

    if-eqz v0, :cond_2

    .line 3030
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mSoundPool:Landroid/media/SoundPool;

    invoke-virtual {v0}, Landroid/media/SoundPool;->release()V

    .line 3032
    :cond_2
    return-void

    .line 3022
    :catch_0
    move-exception v0

    goto :goto_1

    .line 3015
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public refreshWindow(I)V
    .locals 2
    .param p1, "arrageMode"    # I

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 421
    :cond_0
    return-void
.end method

.method public removeDragAndDrop()V
    .locals 2

    .prologue
    .line 691
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragMode:Z

    .line 693
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 702
    :goto_0
    return-void

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    .line 698
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 699
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 700
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarSize:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 701
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const v1, 0x7f02006a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public rotateDragAndDropHelpDialog()V
    .locals 1

    .prologue
    .line 3185
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragAndDropStart:Z

    .line 3187
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideDragAndDropHelpDialog()V

    .line 3188
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragMode:Z

    if-eqz v0, :cond_0

    .line 3189
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawDragAndDrop()V

    .line 3191
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showDragAndDropHelpDialog()V

    .line 3192
    return-void
.end method

.method public setAppListWindow(Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;)V
    .locals 0
    .param p1, "appListWindow"    # Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .prologue
    .line 414
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    .line 415
    return-void
.end method

.method public setCenterBarViewsVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 911
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 912
    return-void
.end method

.method public setFocusedZoneInfo(I)V
    .locals 3
    .param p1, "focusZoneInfo"    # I

    .prologue
    const/4 v1, 0x0

    .line 886
    if-eqz p1, :cond_0

    .line 887
    iget v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    if-eq v2, p1, :cond_1

    const/4 v0, 0x1

    .line 888
    .local v0, "focusZoneChanged":Z
    :goto_0
    iput p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mFocusZoneInfo:I

    .line 890
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsShowButton:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 892
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    .line 895
    .end local v0    # "focusZoneChanged":Z
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 887
    goto :goto_0
.end method

.method public setImageToImageView(Z)V
    .locals 10
    .param p1, "isPress"    # Z

    .prologue
    .line 529
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    if-nez v5, :cond_1

    .line 665
    :cond_0
    :goto_0
    return-void

    .line 532
    :cond_1
    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsDragMode:Z

    if-nez v5, :cond_0

    .line 536
    const/4 v0, 0x0

    .line 537
    .local v0, "animate":Z
    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    if-eqz v5, :cond_2

    .line 538
    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    const v6, 0x7f02006a

    if-ne v5, v6, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 539
    const/4 v0, 0x1

    .line 544
    :cond_2
    :goto_1
    if-nez v0, :cond_4

    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAnimationPlaying:Z

    if-eqz v5, :cond_4

    .line 545
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mRefreshAfterAnimation:Z

    goto :goto_0

    .line 540
    :cond_3
    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    const v6, 0x7f02006a

    if-eq v5, v6, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-nez v5, :cond_2

    .line 541
    const/4 v0, 0x1

    goto :goto_1

    .line 549
    :cond_4
    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsCenterBarFixed:Z

    if-eqz v5, :cond_5

    .line 550
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 551
    :cond_5
    invoke-static {}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getSealedState()Z

    move-result v5

    if-eqz v5, :cond_6

    const/16 v5, 0x1b9

    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/KnoxSettingProperty;->getKnoxSealedMultiWindowFixedState(I)I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    .line 554
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 556
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 557
    const/4 v5, 0x2

    new-array v3, v5, [Landroid/graphics/drawable/Drawable;

    .line 558
    .local v3, "layersLand":[Landroid/graphics/drawable/Drawable;
    const/4 v5, 0x2

    new-array v4, v5, [Landroid/graphics/drawable/Drawable;

    .line 559
    .local v4, "layersPort":[Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_7

    .line 561
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 562
    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 566
    :cond_7
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-eqz v5, :cond_9

    .line 567
    if-eqz p1, :cond_8

    .line 568
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020104

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 569
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020104

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 570
    const v5, 0x7f020104

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 571
    const v5, 0x7f020104

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    .line 585
    :goto_2
    if-eqz v0, :cond_b

    .line 587
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 588
    .local v1, "dLand":Landroid/graphics/drawable/TransitionDrawable;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 589
    const/16 v5, 0xc8

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 590
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v2, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 591
    .local v2, "dPort":Landroid/graphics/drawable/TransitionDrawable;
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 592
    const/16 v5, 0xc8

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 593
    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_a

    .line 594
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 598
    :goto_3
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAnimationPlaying:Z

    .line 599
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    new-instance v6, Lcom/sec/android/app/FlashBarService/CenterBarWindow$5;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    const-wide/16 v8, 0xc8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 618
    .end local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    .end local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    :goto_4
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isSwitchWindowActive()Z

    move-result v5

    if-eqz v5, :cond_e

    .line 619
    if-eqz p1, :cond_d

    .line 620
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020104

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 621
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020104

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 622
    const v5, 0x7f020104

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 623
    const v5, 0x7f020104

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    .line 637
    :goto_5
    if-eqz v0, :cond_10

    .line 639
    new-instance v1, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v1, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 640
    .restart local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    const/4 v5, 0x1

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 641
    const/16 v5, 0xc8

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 642
    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {v2, v4}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 643
    .restart local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    .line 644
    const/16 v5, 0xc8

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 645
    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_f

    .line 646
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 650
    :goto_6
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mAnimationPlaying:Z

    .line 651
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTimerHandler:Landroid/os/Handler;

    new-instance v6, Lcom/sec/android/app/FlashBarService/CenterBarWindow$6;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    const-wide/16 v8, 0xc8

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 573
    .end local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    .end local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    :cond_8
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020103

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 574
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020103

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 575
    const v5, 0x7f020103

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 576
    const v5, 0x7f020103

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    goto/16 :goto_2

    .line 579
    :cond_9
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02006a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 580
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02006a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 581
    const v5, 0x7f02006a

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 582
    const v5, 0x7f02006a

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    goto/16 :goto_2

    .line 596
    .restart local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    .restart local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 611
    .end local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    .end local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    :cond_b
    iget v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mDisplayOrientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_c

    .line 612
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const/4 v6, 0x1

    aget-object v6, v4, v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 614
    :cond_c
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const/4 v6, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 625
    :cond_d
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020103

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 626
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020103

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 627
    const v5, 0x7f020103

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 628
    const v5, 0x7f020103

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    goto/16 :goto_5

    .line 631
    :cond_e
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02006a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v3, v5

    .line 632
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02006a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    aput-object v6, v4, v5

    .line 633
    const v5, 0x7f02006a

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundPortait:I

    .line 634
    const v5, 0x7f02006a

    iput v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mLastBackgroundLandscape:I

    goto/16 :goto_5

    .line 648
    .restart local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    .restart local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    :cond_f
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_6

    .line 662
    .end local v1    # "dLand":Landroid/graphics/drawable/TransitionDrawable;
    .end local v2    # "dPort":Landroid/graphics/drawable/TransitionDrawable;
    :cond_10
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBar:Landroid/widget/ImageView;

    const v6, 0x7f02006a

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0
.end method

.method public setView(Landroid/view/View;)V
    .locals 0
    .param p1, "viewForLayout"    # Landroid/view/View;

    .prologue
    .line 425
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mViewForLayout:Landroid/view/View;

    .line 426
    return-void
.end method

.method public setWindow(Landroid/view/Window;I)V
    .locals 4
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "arrageMode"    # I

    .prologue
    .line 394
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mWindowCenterBar:Landroid/view/Window;

    .line 396
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->initVariables()V

    .line 402
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsFirst:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 403
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow$CenterBarGestureListener;-><init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Lcom/sec/android/app/FlashBarService/CenterBarWindow$1;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarGestureDetector:Landroid/view/GestureDetector;

    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mIsFirst:Z

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeCenterBarLayout()V

    .line 408
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->setCenterBarPoint()V

    .line 410
    iget v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventX:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mTouchEventY:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 411
    return-void
.end method

.method public updateCenterBarPoint(Landroid/graphics/Point;)V
    .locals 2
    .param p1, "centerPoint"    # Landroid/graphics/Point;

    .prologue
    .line 902
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, p1, Landroid/graphics/Point;->x:I

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 903
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, p1, Landroid/graphics/Point;->y:I

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 905
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->checkSealedFixedWindow(Z)V

    .line 906
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    .line 908
    return-void
.end method
