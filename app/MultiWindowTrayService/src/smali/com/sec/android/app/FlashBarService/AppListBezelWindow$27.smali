.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;
.super Landroid/view/OrientationEventListener;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeTraybarHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 1549
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 4
    .param p1, "angle"    # I

    .prologue
    .line 1552
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1553
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1554
    .local v0, "degrees":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->lastDegrees:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 1555
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1556
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27$1;-><init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1563
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->lastDegrees:I
    invoke-static {v2, v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4802(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 1565
    :cond_1
    return-void
.end method
