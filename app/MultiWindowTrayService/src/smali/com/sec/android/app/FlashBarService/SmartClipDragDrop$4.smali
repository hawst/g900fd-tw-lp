.class Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;
.super Ljava/lang/Object;
.source "SmartClipDragDrop.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->initDragDrop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v4, 0x1

    .line 281
    const-string v1, "SmartClipDragDrop"

    const-string v2, "onDrag"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 283
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 284
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 320
    const-string v1, "onDrag"

    const-string v2, "Unknown action type received by OnDragListener."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 323
    :goto_0
    return v4

    .line 287
    :pswitch_0
    const-string v1, "SmartClipDragDrop"

    const-string v2, "onDrag : ACTION_DRAG_STARTED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->notifyStartDragMode()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$100(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    .line 289
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIsDragging:Z
    invoke-static {v1, v4}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$202(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Z)Z

    goto :goto_0

    .line 294
    :pswitch_1
    const-string v1, "SmartClipDragDrop"

    const-string v2, "onDrag : ACTION_DRAG_ENTERED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 299
    :pswitch_2
    const-string v1, "SmartClipDragDrop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDrag : ACTION_DRAG_LOCATION : x = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", y = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 304
    :pswitch_3
    const-string v1, "SmartClipDragDrop"

    const-string v2, "onDrag : ACTION_DRAG_EXITED"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 309
    :pswitch_4
    const-string v1, "SmartClipDragDrop"

    const-string v2, "onDrag : ACTION_DROP"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 314
    :pswitch_5
    const-string v1, "SmartClipDragDrop"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onDrag : ACTION_DRAG_ENDED result = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/DragEvent;->getResult()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->mIsDragging:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$202(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Z)Z

    .line 316
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$4;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopService()V

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
