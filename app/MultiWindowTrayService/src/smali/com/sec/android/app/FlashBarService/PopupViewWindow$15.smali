.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$15;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 0

    .prologue
    .line 974
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 977
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0f00e9

    if-eq v1, v2, :cond_1

    .line 992
    :cond_0
    :goto_0
    return v3

    .line 981
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 982
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0x9

    if-eq v1, v2, :cond_2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_3

    .line 984
    :cond_2
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 989
    :catch_0
    move-exception v0

    .line 990
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "PopupViewWindow"

    const-string v2, "Failed to change Pen Point to HOVERING_SPENICON_DEFAULT"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 985
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_3
    :try_start_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    .line 986
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-static {v1, v2}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
