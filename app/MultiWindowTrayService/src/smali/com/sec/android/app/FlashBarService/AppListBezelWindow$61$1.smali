.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;
.super Landroid/view/View$DragShadowBuilder;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->onLongClick(Landroid/view/View;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;Landroid/view/View;)V
    .locals 0
    .param p2, "x0"    # Landroid/view/View;

    .prologue
    .line 4068
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    invoke-direct {p0, p2}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v3, 0x7f0f0059

    .line 4076
    const/4 v0, 0x0

    .line 4077
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelUI:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 4078
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 4082
    :goto_0
    if-eqz v0, :cond_0

    .line 4083
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 4085
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurSrcIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5602(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4086
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4087
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mbEditmode:Z
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 4088
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mcurDstIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$6000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->isLaunchingBlockedItem(I)Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4089
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v2, v3

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 4090
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mCancelDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 4091
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLunchBlock:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 4094
    :cond_1
    return-void

    .line 4080
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->getCurrentAppList()Ljava/util/List;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppIconIndex:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "shadowSize"    # Landroid/graphics/Point;
    .param p2, "shadowTouchPoint"    # Landroid/graphics/Point;

    .prologue
    .line 4070
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowWidth:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4071
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowHeight:I
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11302(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;I)I

    .line 4072
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 4073
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowWidth:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    div-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61$1;->this$1:Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$61;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mShadowHeight:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$11300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 4074
    return-void
.end method
