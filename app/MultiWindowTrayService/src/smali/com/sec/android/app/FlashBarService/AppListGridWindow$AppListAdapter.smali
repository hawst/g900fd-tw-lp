.class public Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;
.super Landroid/widget/BaseAdapter;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AppListAdapter"
.end annotation


# instance fields
.field context:Landroid/content/Context;

.field private mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1553
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1549
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->context:Landroid/content/Context;

    .line 1554
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->context:Landroid/content/Context;

    .line 1555
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 1556
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 1571
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f030005

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1573
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;-><init>()V

    .line 1575
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    .line 1576
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->labelView:Landroid/widget/TextView;

    .line 1577
    const v2, 0x7f0f005a

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    .line 1578
    const v2, 0x7f0f005b

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    move-object v2, v0

    .line 1579
    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->contentView:Landroid/widget/LinearLayout;

    .line 1581
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1582
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1583
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListItemDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 1585
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1586
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1559
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppCnt()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1565
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 1627
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 1590
    if-nez p2, :cond_0

    .line 1591
    invoke-virtual {p0, p3, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->createView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 1592
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1593
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Recycled child has parent"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1595
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 1596
    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "Recycled child has parent"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1599
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;

    .line 1601
    .local v0, "holder":Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v5, p1, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getListItemByIndex(IZ)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v3

    .line 1602
    .local v3, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    .line 1603
    .local v2, "label":Ljava/lang/CharSequence;
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1604
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1605
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1607
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getLists()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-le v5, v1, :cond_2

    .line 1609
    .local v1, "isTemplateItem":Z
    :goto_0
    if-eqz v1, :cond_3

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1610
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1615
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v6, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListAdapter;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    invoke-virtual {v5, v6, p1, v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeInstancebadgeForGridView(Landroid/widget/ImageView;II)V

    .line 1616
    if-eqz v1, :cond_4

    .line 1617
    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    invoke-virtual {v4, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1622
    :goto_2
    return-object p2

    .end local v1    # "isTemplateItem":Z
    :cond_2
    move v1, v4

    .line 1607
    goto :goto_0

    .line 1612
    .restart local v1    # "isTemplateItem":Z
    :cond_3
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->removeIconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1619
    :cond_4
    iget-object v5, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$AppListItemViewHolder;->badgeIconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2
.end method
