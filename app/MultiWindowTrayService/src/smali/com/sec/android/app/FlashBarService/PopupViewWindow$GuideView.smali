.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
.super Landroid/widget/FrameLayout;
.source "PopupViewWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GuideView"
.end annotation


# instance fields
.field private mBorderView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mFakeHeaderView:Landroid/widget/ImageView;

.field private mParent:Landroid/view/View;

.field private mShowing:Z

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1, "parentView"    # Landroid/view/View;

    .prologue
    .line 1007
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1008
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mParent:Landroid/view/View;

    .line 1009
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mContext:Landroid/content/Context;

    .line 1010
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mWindowManager:Landroid/view/WindowManager;

    .line 1012
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 1015
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mShowing:Z

    if-eqz v0, :cond_0

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1017
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mShowing:Z

    .line 1019
    :cond_0
    return-void
.end method

.method public show(IIII)V
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v5, -0x1

    .line 1022
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mShowing:Z

    if-nez v4, :cond_1

    .line 1023
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 1025
    .local v2, "lp":Landroid/view/WindowManager$LayoutParams;
    const v4, 0x800033

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1026
    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1027
    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1028
    const/4 v4, -0x2

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 1029
    const/16 v4, 0x3ea

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 1030
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mParent:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v4

    iput-object v4, v2, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 1031
    const/16 v4, 0x318

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1036
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    if-nez v4, :cond_0

    .line 1037
    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    .line 1038
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mFakeHeaderView:Landroid/widget/ImageView;

    .line 1040
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    const v5, 0x7f02010f

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1041
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mFakeHeaderView:Landroid/widget/ImageView;

    const v5, 0x7f02006e

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1042
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mFakeHeaderView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1044
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, p3, p4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1045
    .local v3, "vlp":Landroid/widget/FrameLayout$LayoutParams;
    iput p1, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1046
    iput p2, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1047
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {p0, v4, v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1049
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mFakeHeaderView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1050
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-direct {v1, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1051
    .local v1, "flp":Landroid/widget/FrameLayout$LayoutParams;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    sub-int v4, p3, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, p1

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1052
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int v4, p2, v4

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1054
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mFakeHeaderView:Landroid/widget/ImageView;

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1056
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    .end local v1    # "flp":Landroid/widget/FrameLayout$LayoutParams;
    .end local v3    # "vlp":Landroid/widget/FrameLayout$LayoutParams;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4, p0, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1057
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mShowing:Z

    .line 1073
    .end local v2    # "lp":Landroid/view/WindowManager$LayoutParams;
    :goto_0
    return-void

    .line 1059
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout$LayoutParams;

    .line 1061
    .restart local v3    # "vlp":Landroid/widget/FrameLayout$LayoutParams;
    iput p1, v3, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1062
    add-int/lit8 v4, p2, 0x3c

    iput v4, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1063
    iput p3, v3, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1064
    iput p4, v3, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1066
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mFakeHeaderView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    .line 1067
    .restart local v1    # "flp":Landroid/widget/FrameLayout$LayoutParams;
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    sub-int v4, p3, v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, p1

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1068
    iget v4, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    div-int/lit8 v4, v4, 0x2

    sub-int v4, p2, v4

    add-int/lit8 v4, v4, 0x3c

    iput v4, v1, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1070
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->requestLayout()V

    .line 1071
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->mBorderView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->invalidate()V

    goto :goto_0
.end method
