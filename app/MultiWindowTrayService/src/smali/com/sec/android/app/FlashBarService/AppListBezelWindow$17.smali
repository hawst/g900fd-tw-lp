.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

.field final synthetic val$sendIntent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1353
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->val$sendIntent:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/16 v1, 0x8

    .line 1366
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1367
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarAddImageLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1368
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1369
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHistoryBarNewThumbNailLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/RelativeLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1370
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->animationHistoryBarThumbNailNew()V
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 1371
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1361
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1357
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v1, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMakeInstanceAniRunning:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$3902(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 1358
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$17;->val$sendIntent:Landroid/content/Intent;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mNewInstanceIntent:Landroid/content/Intent;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4002(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1359
    return-void
.end method
