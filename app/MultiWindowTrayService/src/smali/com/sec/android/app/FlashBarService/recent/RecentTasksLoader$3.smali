.class Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;
.super Landroid/os/AsyncTask;
.source "RecentTasksLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->loadTasksInBackground(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
        ">;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

.field final synthetic val$tasksWaitingForThumbnails:Ljava/util/concurrent/LinkedBlockingQueue;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Ljava/util/concurrent/LinkedBlockingQueue;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->val$tasksWaitingForThumbnails:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 436
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 26
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 457
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$600(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Landroid/content/Context;

    move-result-object v2

    if-nez v2, :cond_0

    .line 458
    const/4 v2, 0x0

    .line 551
    :goto_0
    return-object v2

    .line 463
    :cond_0
    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v19

    .line 464
    .local v19, "origPri":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$600(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/ActivityManager;

    .line 465
    .local v8, "am":Landroid/app/ActivityManager;
    const/16 v2, 0xa

    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    .line 466
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$600(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v20

    .line 468
    .local v20, "pm":Landroid/content/pm/PackageManager;
    const/16 v2, 0x15

    const/4 v3, 0x2

    sget-object v4, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v4}, Landroid/os/UserHandle;->getIdentifier()I

    move-result v4

    invoke-virtual {v8, v2, v3, v4}, Landroid/app/ActivityManager;->getRecentTasksForUser(III)Ljava/util/List;

    move-result-object v22

    .line 471
    .local v22, "recentTasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RecentTaskInfo;>;"
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v18

    .line 472
    .local v18, "numTasks":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getRunningTasks()Ljava/util/ArrayList;

    move-result-object v24

    .line 473
    .local v24, "runningTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "android.intent.category.HOME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v11

    .line 476
    .local v11, "homeInfo":Landroid/content/pm/ActivityInfo;
    const/4 v10, 0x1

    .line 477
    .local v10, "firstScreenful":Z
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 480
    .local v25, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    const/4 v9, 0x0

    .line 481
    .local v9, "first":I
    const/4 v12, 0x0

    .local v12, "i":I
    const/4 v14, 0x0

    .local v14, "index":I
    :goto_1
    move/from16 v0, v18

    if-ge v12, v0, :cond_1

    const/16 v2, 0x15

    if-ge v14, v2, :cond_1

    .line 482
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 534
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 535
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/ArrayList;

    const/4 v3, 0x0

    aput-object v25, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->publishProgress([Ljava/lang/Object;)V

    .line 536
    if-eqz v10, :cond_2

    .line 538
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/ArrayList;

    const/4 v3, 0x0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    aput-object v4, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->publishProgress([Ljava/lang/Object;)V

    .line 544
    :cond_2
    :goto_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->val$tasksWaitingForThumbnails:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    invoke-direct {v3}, Lcom/sec/android/app/FlashBarService/recent/TaskDescription;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 550
    invoke-static/range {v19 .. v19}, Landroid/os/Process;->setThreadPriority(I)V

    .line 551
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 485
    :cond_3
    move-object/from16 v0, v22

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/app/ActivityManager$RecentTaskInfo;

    .line 487
    .local v21, "recentInfo":Landroid/app/ActivityManager$RecentTaskInfo;
    new-instance v15, Landroid/content/Intent;

    move-object/from16 v0, v21

    iget-object v2, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    invoke-direct {v15, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 488
    .local v15, "intent":Landroid/content/Intent;
    move-object/from16 v0, v21

    iget-object v2, v0, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    if-eqz v2, :cond_4

    .line 489
    move-object/from16 v0, v21

    iget-object v2, v0, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    invoke-virtual {v15, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 493
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    # invokes: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->isCurrentHomeActivity(Landroid/content/ComponentName;Landroid/content/pm/ActivityInfo;)Z
    invoke-static {v2, v3, v11}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$700(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;Landroid/content/ComponentName;Landroid/content/pm/ActivityInfo;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 481
    :cond_5
    :goto_3
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    .line 498
    :cond_6
    invoke-virtual {v15}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$600(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 502
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-object/from16 v0, v21

    iget v3, v0, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    move-object/from16 v0, v21

    iget v4, v0, Landroid/app/ActivityManager$RecentTaskInfo;->persistentId:I

    move-object/from16 v0, v21

    iget-object v5, v0, Landroid/app/ActivityManager$RecentTaskInfo;->baseIntent:Landroid/content/Intent;

    move-object/from16 v0, v21

    iget-object v6, v0, Landroid/app/ActivityManager$RecentTaskInfo;->origActivity:Landroid/content/ComponentName;

    move-object/from16 v0, v21

    iget-object v7, v0, Landroid/app/ActivityManager$RecentTaskInfo;->description:Ljava/lang/CharSequence;

    invoke-virtual/range {v2 .. v7}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->createTaskDescription(IILandroid/content/Intent;Landroid/content/ComponentName;Ljava/lang/CharSequence;)Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    move-result-object v17

    .line 507
    .local v17, "item":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    const/16 v16, 0x0

    .line 508
    .local v16, "isSameZone":Z
    invoke-virtual/range {v24 .. v24}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 509
    .local v23, "rt":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v23

    iget v2, v0, Landroid/app/ActivityManager$RunningTaskInfo;->id:I

    move-object/from16 v0, v21

    iget v3, v0, Landroid/app/ActivityManager$RecentTaskInfo;->id:I

    if-ne v2, v3, :cond_7

    .line 510
    const/16 v16, 0x1

    .line 515
    .end local v23    # "rt":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_8
    if-eqz v17, :cond_5

    if-eqz v16, :cond_5

    .line 518
    :goto_4
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->val$tasksWaitingForThumbnails:Ljava/util/concurrent/LinkedBlockingQueue;

    move-object/from16 v0, v17

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 523
    move-object/from16 v0, v25

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 524
    if-eqz v10, :cond_9

    invoke-virtual/range {v25 .. v25}, Ljava/util/ArrayList;->size()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mNumTasksInFirstScreenful:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$800(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)I

    move-result v3

    if-ne v2, v3, :cond_9

    .line 525
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/ArrayList;

    const/4 v3, 0x0

    aput-object v25, v2, v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->publishProgress([Ljava/lang/Object;)V

    .line 526
    new-instance v25, Ljava/util/ArrayList;

    .end local v25    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 527
    .restart local v25    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    const/4 v10, 0x0

    .line 530
    :cond_9
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    .line 546
    .end local v13    # "i$":Ljava/util/Iterator;
    .end local v15    # "intent":Landroid/content/Intent;
    .end local v16    # "isSameZone":Z
    .end local v17    # "item":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    .end local v21    # "recentInfo":Landroid/app/ActivityManager$RecentTaskInfo;
    :catch_0
    move-exception v2

    goto/16 :goto_2

    .line 520
    .restart local v13    # "i$":Ljava/util/Iterator;
    .restart local v15    # "intent":Landroid/content/Intent;
    .restart local v16    # "isSameZone":Z
    .restart local v17    # "item":Lcom/sec/android/app/FlashBarService/recent/TaskDescription;
    .restart local v21    # "recentInfo":Landroid/app/ActivityManager$RecentTaskInfo;
    :catch_1
    move-exception v2

    goto :goto_4
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 436
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 439
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 440
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->refreshViews()V

    .line 441
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onTasksLoaded()V

    .line 443
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 436
    check-cast p1, [Ljava/util/ArrayList;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->onProgressUpdate([Ljava/util/ArrayList;)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/TaskDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "values":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 447
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 450
    .local v0, "newTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 451
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$400(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->mFirstScreenful:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->access$500(Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onTasksLoaded(Ljava/util/ArrayList;Z)V

    .line 454
    .end local v0    # "newTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/TaskDescription;>;"
    :cond_0
    return-void
.end method
