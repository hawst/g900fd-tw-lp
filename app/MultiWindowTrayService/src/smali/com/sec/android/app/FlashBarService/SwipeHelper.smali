.class public Lcom/sec/android/app/FlashBarService/SwipeHelper;
.super Ljava/lang/Object;
.source "SwipeHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;
    }
.end annotation


# static fields
.field public static ALPHA_FADE_START:F

.field private static sLinearInterpolator:Landroid/view/animation/LinearInterpolator;


# instance fields
.field private DEFAULT_ESCAPE_ANIMATION_DURATION:I

.field private MAX_DISMISS_VELOCITY:I

.field private MAX_ESCAPE_ANIMATION_DURATION:I

.field private SWIPE_ESCAPE_VELOCITY:F

.field private mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

.field private mCanCurrViewBeDimissed:Z

.field private mCurrAnimView:Landroid/view/View;

.field private mCurrView:Landroid/view/View;

.field private mDensityScale:F

.field private mDragging:Z

.field private mHandler:Landroid/os/Handler;

.field private mInitialTouchPos:F

.field private mLongPressListener:Landroid/view/View$OnLongClickListener;

.field private mLongPressSent:Z

.field private mLongPressTimeout:J

.field private mMinAlpha:F

.field private mPagingTouchSlop:F

.field private mSwipeDirection:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field private mWatchLongPress:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    .line 54
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->ALPHA_FADE_START:F

    return-void
.end method

.method public constructor <init>(ILcom/sec/android/app/FlashBarService/SwipeHelper$Callback;FF)V
    .locals 2
    .param p1, "swipeDirection"    # I
    .param p2, "callback"    # Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;
    .param p3, "densityScale"    # F
    .param p4, "pagingTouchSlop"    # F

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/high16 v0, 0x42c80000    # 100.0f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:F

    .line 49
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    .line 50
    const/16 v0, 0x190

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    .line 51
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mMinAlpha:F

    .line 80
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    .line 81
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mHandler:Landroid/os/Handler;

    .line 82
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    .line 83
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    .line 84
    iput p3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDensityScale:F

    .line 85
    iput p4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mPagingTouchSlop:F

    .line 87
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    float-to-long v0, v0

    iput-wide v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressTimeout:J

    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/SwipeHelper;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwipeHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/SwipeHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwipeHelper;

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressSent:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/FlashBarService/SwipeHelper;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwipeHelper;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressSent:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/SwipeHelper;)Landroid/view/View$OnLongClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwipeHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressListener:Landroid/view/View$OnLongClickListener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/SwipeHelper;)Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwipeHelper;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/SwipeHelper;Landroid/view/View;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwipeHelper;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v0

    return v0
.end method

.method private createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "newPos"    # F

    .prologue
    .line 116
    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v1, :cond_0

    const-string v1, "translationX"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p2, v2, v3

    invoke-static {p1, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 118
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    return-object v0

    .line 116
    .end local v0    # "anim":Landroid/animation/ObjectAnimator;
    :cond_0
    const-string v1, "translationY"

    goto :goto_0
.end method

.method private getAlphaForOffset(Landroid/view/View;)F
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    .line 144
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v3

    .line 145
    .local v3, "viewSize":F
    const/high16 v4, 0x3f000000    # 0.5f

    mul-float v0, v4, v3

    .line 146
    .local v0, "fadeSize":F
    const/high16 v2, 0x3f800000    # 1.0f

    .line 147
    .local v2, "result":F
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v1

    .line 148
    .local v1, "pos":F
    sget v4, Lcom/sec/android/app/FlashBarService/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    cmpl-float v4, v1, v4

    if-ltz v4, :cond_1

    .line 149
    sget v4, Lcom/sec/android/app/FlashBarService/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    sub-float v4, v1, v4

    div-float/2addr v4, v0

    sub-float v2, v5, v4

    .line 153
    :cond_0
    :goto_0
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mMinAlpha:F

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v4

    return v4

    .line 150
    :cond_1
    sget v4, Lcom/sec/android/app/FlashBarService/SwipeHelper;->ALPHA_FADE_START:F

    sub-float v4, v5, v4

    mul-float/2addr v4, v3

    cmpg-float v4, v1, v4

    if-gez v4, :cond_0

    .line 151
    sget v4, Lcom/sec/android/app/FlashBarService/SwipeHelper;->ALPHA_FADE_START:F

    mul-float/2addr v4, v3

    add-float/2addr v4, v1

    div-float/2addr v4, v0

    add-float v2, v5, v4

    goto :goto_0
.end method

.method private getPerpendicularVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1, "vt"    # Landroid/view/VelocityTracker;

    .prologue
    .line 122
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    goto :goto_0
.end method

.method private getPos(Landroid/view/MotionEvent;)F
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 103
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    goto :goto_0
.end method

.method private getSize(Landroid/view/View;)F
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_0
.end method

.method private getTranslation(Landroid/view/View;)F
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 107
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    goto :goto_0
.end method

.method private getVelocity(Landroid/view/VelocityTracker;)F
    .locals 1
    .param p1, "vt"    # Landroid/view/VelocityTracker;

    .prologue
    .line 111
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    goto :goto_0
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;)V
    .locals 5
    .param p0, "view"    # Landroid/view/View;

    .prologue
    .line 158
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-static {p0, v0}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 161
    return-void
.end method

.method public static invalidateGlobalRegion(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 6
    .param p0, "view"    # Landroid/view/View;
    .param p1, "childBounds"    # Landroid/graphics/RectF;

    .prologue
    .line 169
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    .end local p0    # "view":Landroid/view/View;
    check-cast p0, Landroid/view/View;

    .line 171
    .restart local p0    # "view":Landroid/view/View;
    invoke-virtual {p0}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 172
    iget v0, p1, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iget v1, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    .line 183
    :cond_0
    return-void
.end method

.method private setTranslation(Landroid/view/View;F)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "translate"    # F

    .prologue
    .line 127
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    if-nez v0, :cond_0

    .line 128
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method


# virtual methods
.method public dismissChild(Landroid/view/View;F)V
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "velocity"    # F

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 259
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    invoke-interface {v7, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 260
    .local v1, "animView":Landroid/view/View;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    invoke-interface {v7, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v3

    .line 263
    .local v3, "canAnimViewBeDismissed":Z
    const v7, 0x7f0f00df

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    .line 264
    .local v2, "big":Landroid/widget/RelativeLayout;
    const v7, 0x7f0f00e2

    invoke-virtual {p1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 265
    .local v6, "thumbnail":Landroid/view/View;
    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 266
    invoke-virtual {v6, v8}, Landroid/view/View;->setClickable(Z)V

    .line 268
    cmpg-float v7, p2, v9

    if-ltz v7, :cond_1

    cmpl-float v7, p2, v9

    if-nez v7, :cond_0

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v7

    cmpg-float v7, v7, v9

    if-ltz v7, :cond_1

    :cond_0
    cmpl-float v7, p2, v9

    if-nez v7, :cond_2

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v7

    cmpl-float v7, v7, v9

    if-nez v7, :cond_2

    iget v7, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mSwipeDirection:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 272
    :cond_1
    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v7

    neg-float v5, v7

    .line 276
    .local v5, "newPos":F
    :goto_0
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->MAX_ESCAPE_ANIMATION_DURATION:I

    .line 277
    .local v4, "duration":I
    cmpl-float v7, p2, v9

    if-eqz v7, :cond_3

    .line 278
    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v7

    sub-float v7, v5, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    const/high16 v8, 0x447a0000    # 1000.0f

    mul-float/2addr v7, v8

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    div-float/2addr v7, v8

    float-to-int v7, v7

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 285
    :goto_1
    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v8}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 286
    invoke-direct {p0, v1, v5}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 287
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    sget-object v7, Lcom/sec/android/app/FlashBarService/SwipeHelper;->sLinearInterpolator:Landroid/view/animation/LinearInterpolator;

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 288
    int-to-long v8, v4

    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 289
    new-instance v7, Lcom/sec/android/app/FlashBarService/SwipeHelper$2;

    invoke-direct {v7, p0, p1, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$2;-><init>(Lcom/sec/android/app/FlashBarService/SwipeHelper;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 295
    new-instance v7, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;

    invoke-direct {v7, p0, v3, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$3;-><init>(Lcom/sec/android/app/FlashBarService/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v7}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 303
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 304
    return-void

    .line 274
    .end local v0    # "anim":Landroid/animation/ObjectAnimator;
    .end local v4    # "duration":I
    .end local v5    # "newPos":F
    :cond_2
    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v5

    .restart local v5    # "newPos":F
    goto :goto_0

    .line 282
    .restart local v4    # "duration":I
    :cond_3
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->DEFAULT_ESCAPE_ANIMATION_DURATION:I

    goto :goto_1
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 193
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 195
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 251
    :cond_0
    :goto_0
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDragging:Z

    return v3

    .line 197
    :pswitch_0
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDragging:Z

    .line 198
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressSent:Z

    .line 199
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    invoke-interface {v3, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    .line 200
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    .line 201
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 202
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    .line 203
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCanCurrViewBeDimissed:Z

    .line 204
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 205
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mInitialTouchPos:F

    .line 207
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressListener:Landroid/view/View$OnLongClickListener;

    if-eqz v3, :cond_0

    .line 208
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mWatchLongPress:Ljava/lang/Runnable;

    if-nez v3, :cond_1

    .line 209
    new-instance v3, Lcom/sec/android/app/FlashBarService/SwipeHelper$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/SwipeHelper$1;-><init>(Lcom/sec/android/app/FlashBarService/SwipeHelper;)V

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mWatchLongPress:Ljava/lang/Runnable;

    .line 220
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mWatchLongPress:Ljava/lang/Runnable;

    iget-wide v6, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressTimeout:J

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 227
    :pswitch_1
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressSent:Z

    if-nez v3, :cond_0

    .line 228
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 229
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v2

    .line 230
    .local v2, "pos":F
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mInitialTouchPos:F

    sub-float v1, v2, v3

    .line 231
    .local v1, "delta":F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mPagingTouchSlop:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 232
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v3, v4}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->onBeginDrag(Landroid/view/View;)V

    .line 233
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDragging:Z

    .line 234
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    invoke-direct {p0, v4}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v4

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mInitialTouchPos:F

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->removeLongPressCallback()V

    goto/16 :goto_0

    .line 244
    .end local v1    # "delta":F
    .end local v2    # "pos":F
    :pswitch_2
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDragging:Z

    .line 245
    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    .line 246
    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    .line 247
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressSent:Z

    .line 248
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->removeLongPressCallback()V

    goto/16 :goto_0

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 324
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mLongPressSent:Z

    if-eqz v13, :cond_0

    .line 325
    const/4 v13, 0x1

    .line 390
    :goto_0
    return v13

    .line 328
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDragging:Z

    if-nez v13, :cond_1

    .line 331
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->removeLongPressCallback()V

    .line 332
    const/4 v13, 0x0

    goto :goto_0

    .line 335
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 336
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    .line 337
    .local v2, "action":I
    packed-switch v2, :pswitch_data_0

    .line 390
    :cond_2
    :goto_1
    const/4 v13, 0x1

    goto :goto_0

    .line 340
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v13, :cond_2

    .line 341
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getPos(Landroid/view/MotionEvent;)F

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mInitialTouchPos:F

    sub-float v5, v13, v14

    .line 344
    .local v5, "delta":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v13, v14}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v13

    if-nez v13, :cond_3

    .line 345
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v11

    .line 346
    .local v11, "size":F
    const v13, 0x3e19999a    # 0.15f

    mul-float v8, v13, v11

    .line 347
    .local v8, "maxScrollDistance":F
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v13

    cmpl-float v13, v13, v11

    if-ltz v13, :cond_6

    .line 348
    const/4 v13, 0x0

    cmpl-float v13, v5, v13

    if-lez v13, :cond_5

    move v5, v8

    .line 353
    .end local v8    # "maxScrollDistance":F
    .end local v11    # "size":F
    :cond_3
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v5}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->setTranslation(Landroid/view/View;F)V

    .line 354
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCanCurrViewBeDimissed:Z

    if-eqz v13, :cond_4

    .line 355
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getAlphaForOffset(Landroid/view/View;)F

    move-result v14

    invoke-virtual {v13, v14}, Landroid/view/View;->setAlpha(F)V

    .line 357
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-static {v13}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->invalidateGlobalRegion(Landroid/view/View;)V

    goto :goto_1

    .line 348
    .restart local v8    # "maxScrollDistance":F
    .restart local v11    # "size":F
    :cond_5
    neg-float v5, v8

    goto :goto_2

    .line 350
    :cond_6
    div-float v13, v5, v11

    float-to-double v14, v13

    const-wide v16, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    double-to-float v13, v14

    mul-float v5, v8, v13

    goto :goto_2

    .line 362
    .end local v5    # "delta":F
    .end local v8    # "maxScrollDistance":F
    .end local v11    # "size":F
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v13, :cond_2

    .line 363
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->MAX_DISMISS_VELOCITY:I

    int-to-float v13, v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDensityScale:F

    mul-float v9, v13, v14

    .line 364
    .local v9, "maxVelocity":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v14, 0x3e8

    invoke-virtual {v13, v14, v9}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 365
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->SWIPE_ESCAPE_VELOCITY:F

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDensityScale:F

    mul-float v7, v13, v14

    .line 366
    .local v7, "escapeVelocity":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getVelocity(Landroid/view/VelocityTracker;)F

    move-result v12

    .line 367
    .local v12, "velocity":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mVelocityTracker:Landroid/view/VelocityTracker;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getPerpendicularVelocity(Landroid/view/VelocityTracker;)F

    move-result v10

    .line 370
    .local v10, "perpendicularVelocity":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Math;->abs(F)F

    move-result v13

    float-to-double v14, v13

    const-wide v16, 0x3fd999999999999aL    # 0.4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getSize(Landroid/view/View;)F

    move-result v13

    float-to-double v0, v13

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    cmpl-double v13, v14, v16

    if-lez v13, :cond_8

    const/4 v3, 0x1

    .line 372
    .local v3, "childSwipedFarEnough":Z
    :goto_3
    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v13

    cmpl-float v13, v13, v7

    if-lez v13, :cond_b

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v13

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v14

    cmpl-float v13, v13, v14

    if-lez v13, :cond_b

    const/4 v13, 0x0

    cmpl-float v13, v12, v13

    if-lez v13, :cond_9

    const/4 v13, 0x1

    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrAnimView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->getTranslation(Landroid/view/View;)F

    move-result v14

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_a

    const/4 v14, 0x1

    :goto_5
    if-ne v13, v14, :cond_b

    const/4 v4, 0x1

    .line 376
    .local v4, "childSwipedFastEnough":Z
    :goto_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v13, v14}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v13

    if-eqz v13, :cond_c

    if-nez v4, :cond_7

    if-eqz v3, :cond_c

    :cond_7
    const/4 v6, 0x1

    .line 379
    .local v6, "dismissChild":Z
    :goto_7
    if-eqz v6, :cond_e

    .line 381
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    if-eqz v4, :cond_d

    .end local v12    # "velocity":F
    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->dismissChild(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 370
    .end local v3    # "childSwipedFarEnough":Z
    .end local v4    # "childSwipedFastEnough":Z
    .end local v6    # "dismissChild":Z
    .restart local v12    # "velocity":F
    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    .line 372
    .restart local v3    # "childSwipedFarEnough":Z
    :cond_9
    const/4 v13, 0x0

    goto :goto_4

    :cond_a
    const/4 v14, 0x0

    goto :goto_5

    :cond_b
    const/4 v4, 0x0

    goto :goto_6

    .line 376
    .restart local v4    # "childSwipedFastEnough":Z
    :cond_c
    const/4 v6, 0x0

    goto :goto_7

    .line 381
    .restart local v6    # "dismissChild":Z
    :cond_d
    const/4 v12, 0x0

    goto :goto_8

    .line 384
    :cond_e
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    invoke-interface {v13, v14}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->onDragCancelled(Landroid/view/View;)V

    .line 385
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCurrView:Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v12}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->snapChild(Landroid/view/View;F)V

    goto/16 :goto_1

    .line 337
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public removeLongPressCallback()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mWatchLongPress:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mWatchLongPress:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mWatchLongPress:Ljava/lang/Runnable;

    .line 190
    :cond_0
    return-void
.end method

.method public setDensityScale(F)V
    .locals 0
    .param p1, "densityScale"    # F

    .prologue
    .line 95
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mDensityScale:F

    .line 96
    return-void
.end method

.method public setMinAlpha(F)V
    .locals 0
    .param p1, "minAlpha"    # F

    .prologue
    .line 140
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mMinAlpha:F

    .line 141
    return-void
.end method

.method public setPagingTouchSlop(F)V
    .locals 0
    .param p1, "pagingTouchSlop"    # F

    .prologue
    .line 99
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mPagingTouchSlop:F

    .line 100
    return-void
.end method

.method public snapChild(Landroid/view/View;F)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "velocity"    # F

    .prologue
    .line 307
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    invoke-interface {v4, p1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->getChildContentView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 308
    .local v1, "animView":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwipeHelper;->mCallback:Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;

    invoke-interface {v4, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$Callback;->canChildBeDismissed(Landroid/view/View;)Z

    move-result v2

    .line 309
    .local v2, "canAnimViewBeDismissed":Z
    const/4 v4, 0x0

    invoke-direct {p0, v1, v4}, Lcom/sec/android/app/FlashBarService/SwipeHelper;->createTranslationAnimation(Landroid/view/View;F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 310
    .local v0, "anim":Landroid/animation/ObjectAnimator;
    const/16 v3, 0x96

    .line 311
    .local v3, "duration":I
    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 312
    new-instance v4, Lcom/sec/android/app/FlashBarService/SwipeHelper$4;

    invoke-direct {v4, p0, v2, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper$4;-><init>(Lcom/sec/android/app/FlashBarService/SwipeHelper;ZLandroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 320
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 321
    return-void
.end method
