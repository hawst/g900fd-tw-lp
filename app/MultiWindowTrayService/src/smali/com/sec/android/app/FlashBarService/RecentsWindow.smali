.class public Lcom/sec/android/app/FlashBarService/RecentsWindow;
.super Ljava/lang/Object;
.source "RecentsWindow.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDisplay:Landroid/view/Display;

.field private mDisplayOrientation:I

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field private mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

.field mRecentsWindowKeyListener:Landroid/view/View$OnKeyListener;

.field mRecentsWindowTouchListener:Landroid/view/View$OnTouchListener;

.field private mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mShowing:Z

.field private mWindow:Landroid/view/Window;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    .line 216
    new-instance v0, Lcom/sec/android/app/FlashBarService/RecentsWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/RecentsWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/RecentsWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsWindowKeyListener:Landroid/view/View$OnKeyListener;

    .line 227
    new-instance v0, Lcom/sec/android/app/FlashBarService/RecentsWindow$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/RecentsWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/RecentsWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsWindowTouchListener:Landroid/view/View$OnTouchListener;

    .line 46
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    .line 47
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 50
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    const-string v1, "multiwindow_facade"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 51
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mDisplay:Landroid/view/Display;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/RecentsWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mShowing:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/RecentsWindow;)Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/RecentsWindow;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    return-object v0
.end method

.method private onCreate()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 55
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mDisplayOrientation:I

    .line 57
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 59
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090006

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 60
    const v3, 0x7f030020

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 65
    .local v2, "recentsWindow":Landroid/view/View;
    :goto_0
    const v3, 0x7f0f00d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    .line 66
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    const/16 v5, 0x7d2

    invoke-virtual {v3, v4, v6, v6, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->addWindow(Landroid/view/View;III)Landroid/view/Window;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    .line 67
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    if-eqz v3, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->getRecentsLayoutParams()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 69
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 70
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 71
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v3, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setRecentsWindow(Lcom/sec/android/app/FlashBarService/RecentsWindow;)V

    .line 72
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->setRecentsPanel(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)V

    .line 73
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getFocusedZoneInfo()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->setFocusedZone(I)V

    .line 74
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    const v4, 0x3cf5c28f    # 0.03f

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setMinSwipeAlpha(F)V

    .line 75
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsWindowKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 76
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsWindowTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 78
    .end local v1    # "lp":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    return-void

    .line 62
    .end local v2    # "recentsWindow":Landroid/view/View;
    :cond_1
    const v3, 0x7f030023

    invoke-virtual {v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .restart local v2    # "recentsWindow":Landroid/view/View;
    goto :goto_0
.end method

.method private onDestroy()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->setRecentsPanel(Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;)V

    .line 82
    return-void
.end method


# virtual methods
.method public closeRecentsPanel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 201
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mShowing:Z

    .line 202
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->show(Z)V

    .line 204
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->onUiHidden()V

    .line 206
    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    .line 188
    return-void
.end method

.method protected getRecentsLayoutParams()Landroid/view/WindowManager$LayoutParams;
    .locals 18

    .prologue
    .line 94
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v10

    .line 95
    .local v10, "point":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v15}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->getFocusedZoneInfo()I

    move-result v3

    .line 96
    .local v3, "focusZone":I
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 97
    .local v2, "displaySize":Landroid/graphics/Point;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mDisplay:Landroid/view/Display;

    invoke-virtual {v15, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 99
    const/4 v4, 0x0

    .line 100
    .local v4, "gravity":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    invoke-virtual {v15}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v7

    .line 101
    .local v7, "lp":Landroid/view/WindowManager$LayoutParams;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 102
    .local v11, "rsrc":Landroid/content/res/Resources;
    const v15, 0x7f090004

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 103
    .local v1, "bUseFixedWindowLayout":Z
    const v15, 0x7f070006

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v8

    .line 104
    .local v8, "nFixedLayoutRate":I
    const v15, 0x10501c6

    invoke-virtual {v11, v15}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v15

    float-to-int v9, v15

    .line 105
    .local v9, "nFrameCenterMargin":I
    const/4 v12, 0x0

    .line 107
    .local v12, "top":I
    iget v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v15, v15, -0x9

    iput v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 109
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mDisplayOrientation:I

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_3

    .line 111
    and-int/lit8 v15, v3, 0x3

    if-eqz v15, :cond_1

    .line 112
    iget v13, v2, Landroid/graphics/Point;->x:I

    .line 113
    .local v13, "width":I
    iget v15, v10, Landroid/graphics/Point;->y:I

    sub-int v5, v15, v12

    .line 114
    .local v5, "height":I
    iget v15, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v15, v12

    div-int/lit8 v6, v15, 0x2

    .line 115
    .local v6, "heightHalf":I
    const/16 v4, 0x33

    .line 116
    iput v12, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 123
    :goto_0
    if-nez v1, :cond_2

    .line 124
    if-ge v5, v6, :cond_0

    .line 125
    move v5, v6

    .line 130
    :cond_0
    :goto_1
    add-int/2addr v5, v9

    .line 154
    .end local v6    # "heightHalf":I
    :goto_2
    iput v13, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 155
    iput v5, v7, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 156
    iput v4, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 157
    iget v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v16, 0x40020

    or-int v15, v15, v16

    iput v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 160
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    move-result v15

    if-eqz v15, :cond_7

    .line 161
    iget v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/high16 v16, 0x1000000

    or-int v15, v15, v16

    iput v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 167
    :goto_3
    const-string v15, "RecentsPanel"

    invoke-virtual {v7, v15}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 168
    const v15, 0x10302e3

    iput v15, v7, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 170
    return-object v7

    .line 118
    .end local v5    # "height":I
    .end local v13    # "width":I
    :cond_1
    iget v13, v2, Landroid/graphics/Point;->x:I

    .line 119
    .restart local v13    # "width":I
    iget v15, v2, Landroid/graphics/Point;->y:I

    iget v0, v10, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    sub-int v5, v15, v16

    .line 120
    .restart local v5    # "height":I
    iget v15, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v15, v12

    div-int/lit8 v6, v15, 0x2

    .line 121
    .restart local v6    # "heightHalf":I
    const/16 v4, 0x53

    goto :goto_0

    .line 128
    :cond_2
    iget v15, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v15, v12

    int-to-float v15, v15

    int-to-float v0, v8

    move/from16 v16, v0

    const/high16 v17, 0x42c80000    # 100.0f

    div-float v16, v16, v17

    mul-float v15, v15, v16

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v5

    goto :goto_1

    .line 133
    .end local v5    # "height":I
    .end local v6    # "heightHalf":I
    .end local v13    # "width":I
    :cond_3
    and-int/lit8 v15, v3, 0x3

    if-eqz v15, :cond_5

    .line 134
    iget v13, v10, Landroid/graphics/Point;->x:I

    .line 135
    .restart local v13    # "width":I
    iget v15, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v14, v15, 0x2

    .line 136
    .local v14, "widthHalf":I
    iget v15, v2, Landroid/graphics/Point;->y:I

    sub-int v5, v15, v12

    .line 137
    .restart local v5    # "height":I
    const/16 v4, 0x53

    .line 144
    :goto_4
    if-nez v1, :cond_6

    .line 145
    if-ge v13, v14, :cond_4

    .line 146
    move v13, v14

    .line 151
    :cond_4
    :goto_5
    add-int/2addr v13, v9

    goto :goto_2

    .line 139
    .end local v5    # "height":I
    .end local v13    # "width":I
    .end local v14    # "widthHalf":I
    :cond_5
    iget v15, v2, Landroid/graphics/Point;->x:I

    iget v0, v10, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    sub-int v13, v15, v16

    .line 140
    .restart local v13    # "width":I
    iget v15, v2, Landroid/graphics/Point;->x:I

    div-int/lit8 v14, v15, 0x2

    .line 141
    .restart local v14    # "widthHalf":I
    iget v15, v2, Landroid/graphics/Point;->y:I

    sub-int v5, v15, v12

    .line 142
    .restart local v5    # "height":I
    const/16 v4, 0x55

    goto :goto_4

    .line 149
    :cond_6
    iget v15, v2, Landroid/graphics/Point;->x:I

    int-to-float v15, v15

    int-to-float v0, v8

    move/from16 v16, v0

    const/high16 v17, 0x42c80000    # 100.0f

    div-float v16, v16, v17

    mul-float v15, v15, v16

    invoke-static {v15}, Ljava/lang/Math;->round(F)I

    move-result v13

    goto :goto_5

    .line 163
    .end local v14    # "widthHalf":I
    :cond_7
    iget v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v15, v15, 0x2

    iput v15, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 164
    const/high16 v15, 0x3f400000    # 0.75f

    iput v15, v7, Landroid/view/WindowManager$LayoutParams;->dimAmount:F

    goto :goto_3
.end method

.method public getWindow()Landroid/view/Window;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    if-nez v0, :cond_0

    .line 175
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->onCreate()V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    return-object v0
.end method

.method public isWindowShowing()Z
    .locals 1

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mShowing:Z

    return v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 87
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mDisplayOrientation:I

    if-eq v0, v1, :cond_0

    .line 88
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mDisplayOrientation:I

    .line 89
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->dismiss()V

    .line 91
    :cond_0
    return-void
.end method

.method public openRecentsPanel()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 191
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mShowing:Z

    .line 192
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mRecentsPanel:Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getLoadedTasks()Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->getInstance(Landroid/content/Context;)Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentTasksLoader;->isFirstScreenful()Z

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsPanelView;->show(ZLjava/util/ArrayList;ZZ)V

    .line 198
    :cond_0
    return-void
.end method

.method public removeWindow()V
    .locals 2

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/RecentsWindow;->onDestroy()V

    .line 182
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeWindow(Landroid/view/Window;)Z

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/RecentsWindow;->mWindow:Landroid/view/Window;

    .line 184
    return-void
.end method
