.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeTraybarHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

.field final synthetic val$contents:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 1264
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->val$contents:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 1268
    const/4 v5, 0x0

    .line 1269
    .local v5, "xOffset":I
    const/4 v6, 0x0

    .line 1270
    .local v6, "yOffset":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->val$contents:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getHeight()I

    move-result v4

    .line 1272
    .local v4, "textViewHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0130

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v3, v7

    .line 1273
    .local v3, "helpPopupWidth":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0131

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    add-int v2, v7, v4

    .line 1275
    .local v2, "helpPopupHeight":I
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/PopupWindow;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1276
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1277
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1278
    .local v0, "degrees":I
    packed-switch v0, :pswitch_data_0

    .line 1304
    :goto_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/PopupWindow;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v9

    invoke-virtual {v7, v5, v6, v8, v9}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 1306
    .end local v0    # "degrees":I
    .end local v1    # "display":Landroid/view/Display;
    :cond_0
    return-void

    .line 1280
    .restart local v0    # "degrees":I
    .restart local v1    # "display":Landroid/view/Display;
    :pswitch_0
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    div-int/lit8 v8, v3, 0x2

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0139

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    sub-float/2addr v7, v8

    float-to-int v5, v7

    .line 1282
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0134

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    int-to-float v9, v2

    add-float/2addr v8, v9

    sub-float v8, v7, v8

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    if-nez v7, :cond_1

    const/4 v7, 0x0

    :goto_1
    int-to-float v7, v7

    sub-float v7, v8, v7

    float-to-int v6, v7

    .line 1284
    goto :goto_0

    .line 1282
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 1286
    :pswitch_1
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    int-to-float v7, v7

    int-to-float v8, v3

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0a0133

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    add-float/2addr v8, v9

    sub-float/2addr v7, v8

    float-to-int v5, v7

    .line 1287
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    if-nez v7, :cond_2

    :cond_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0137

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    add-int/lit8 v6, v7, 0x0

    .line 1290
    goto/16 :goto_0

    .line 1292
    :pswitch_2
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    div-int/lit8 v8, v3, 0x2

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0139

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    sub-float/2addr v7, v8

    float-to-int v5, v7

    .line 1294
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    if-nez v7, :cond_3

    const/4 v6, 0x0

    .line 1295
    :goto_2
    goto/16 :goto_0

    .line 1294
    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    .line 1297
    :pswitch_3
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0a0132

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v5, v7

    .line 1298
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayHeight:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    sub-int/2addr v7, v2

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSystemUiVisibility:I

    if-nez v8, :cond_4

    :cond_4
    add-int/lit8 v7, v7, 0x0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0138

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    sub-int v6, v7, v8

    goto/16 :goto_0

    .line 1278
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
