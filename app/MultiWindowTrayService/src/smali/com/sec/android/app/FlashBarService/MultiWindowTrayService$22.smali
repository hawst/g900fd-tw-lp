.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;
.super Landroid/content/BroadcastReceiver;
.source "MultiWindowTrayService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->initAlarmEventListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 1398
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 1400
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1401
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.sec.android.clockpackage.alarm.ALARM_ALERT_FROM_ALARM"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1403
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1404
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->updateFlashBarState(ZZ)Z

    .line 1405
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mAppListWindow:Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AbstractAppListWindow;->hideAppEditList()V

    .line 1407
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    .line 1408
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SmartWindow;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1409
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/SmartWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->hideSmartEditWindow()V

    .line 1411
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeSmartWindow()V

    .line 1412
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1413
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mPopupViewWindow:Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$200(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->hidePopupViewEditWindow()V

    .line 1415
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$22;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewWindow()V

    .line 1417
    :cond_3
    return-void
.end method
