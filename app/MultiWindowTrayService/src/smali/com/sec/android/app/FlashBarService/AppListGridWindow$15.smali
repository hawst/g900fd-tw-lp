.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;
.super Landroid/view/OrientationEventListener;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeTraybarHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 1309
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 3
    .param p1, "angle"    # I

    .prologue
    .line 1312
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 1313
    .local v1, "display":Landroid/view/Display;
    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 1314
    .local v0, "degrees":I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->lastDegrees:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 1315
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/PopupWindow;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1316
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->rotateTraybarHelpPopup()V

    .line 1318
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$15;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->lastDegrees:I
    invoke-static {v2, v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 1320
    :cond_1
    return-void
.end method
