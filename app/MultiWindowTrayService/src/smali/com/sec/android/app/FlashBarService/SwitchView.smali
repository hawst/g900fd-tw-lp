.class public Lcom/sec/android/app/FlashBarService/SwitchView;
.super Landroid/widget/RelativeLayout;
.source "SwitchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/SwitchView$6;,
        Lcom/sec/android/app/FlashBarService/SwitchView$BorderDrawable;,
        Lcom/sec/android/app/FlashBarService/SwitchView$State;,
        Lcom/sec/android/app/FlashBarService/SwitchView$Zone;,
        Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;
    }
.end annotation


# static fields
.field private static final ZONES:[I


# instance fields
.field private DEBUG:Z

.field private TAG:Ljava/lang/String;

.field private mArrangeState:I

.field private final mBorderLineColor:I

.field private final mBroderThickness:I

.field private mCurrentZoomAnimation:Landroid/animation/Animator;

.field private mDiffPoint:Landroid/graphics/Point;

.field private mFocusedAppZone:I

.field private mFocusedZone:I

.field private mFrames:I

.field private mHLine:Landroid/widget/ImageView;

.field private mHideRequested:Z

.field private mLockedView:Landroid/widget/ImageView;

.field private mLockedZone:I

.field private final mMultiwindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field private mNeedsHideAnimation:Z

.field private mPoint:Landroid/graphics/Point;

.field private mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

.field private mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

.field private mTarget:I

.field private mTimeAnimator:Landroid/animation/TimeAnimator;

.field private mVLine:Landroid/widget/ImageView;

.field private mZones:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/app/FlashBarService/SwitchView$Zone;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/app/FlashBarService/SwitchView;->ZONES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x3
        0xc
        0x1
        0x2
        0x4
        0x8
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/android/app/FlashBarService/SwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/FlashBarService/SwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, -0x1

    .line 141
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    .line 39
    iput v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mArrangeState:I

    .line 46
    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;

    .line 99
    new-instance v5, Landroid/animation/TimeAnimator;

    invoke-direct {v5}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTimeAnimator:Landroid/animation/TimeAnimator;

    .line 100
    iput v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I

    .line 114
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    .line 115
    const-string v5, "SwitchView"

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    .line 118
    iput v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    .line 119
    iput v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    .line 120
    sget-object v5, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 121
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mDiffPoint:Landroid/graphics/Point;

    .line 143
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mContext:Landroid/content/Context;

    const-string v6, "multiwindow_facade"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mMultiwindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 146
    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHLine:Landroid/widget/ImageView;

    .line 147
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHLine:Landroid/widget/ImageView;

    const v6, 0x7f020067

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 148
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHLine:Landroid/widget/ImageView;

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 149
    new-instance v5, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mVLine:Landroid/widget/ImageView;

    .line 150
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mVLine:Landroid/widget/ImageView;

    const v6, 0x7f020068

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 151
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mVLine:Landroid/widget/ImageView;

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 153
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHLine:Landroid/widget/ImageView;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->addView(Landroid/view/View;)V

    .line 154
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mVLine:Landroid/widget/ImageView;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->addView(Landroid/view/View;)V

    .line 156
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v5, 0x6

    if-ge v1, v5, :cond_0

    .line 157
    new-instance v3, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mContext:Landroid/content/Context;

    invoke-direct {v3, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 158
    .local v3, "v":Landroid/widget/ImageView;
    new-instance v0, Landroid/view/View;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 159
    .local v0, "border":Landroid/view/View;
    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 161
    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->addView(Landroid/view/View;)V

    .line 162
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->addView(Landroid/view/View;)V

    .line 164
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    sget-object v6, Lcom/sec/android/app/FlashBarService/SwitchView;->ZONES:[I

    aget v6, v6, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    invoke-direct {v7, p0, v3, v0, v8}, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/widget/ImageView;Landroid/view/View;Landroid/graphics/Rect;)V

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 167
    .end local v0    # "border":Landroid/view/View;
    .end local v3    # "v":Landroid/widget/ImageView;
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 168
    .local v4, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v5, v4, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    const/4 v6, 0x1

    invoke-virtual {p0, v5, v6}, Lcom/sec/android/app/FlashBarService/SwitchView;->bringChildToBack(Landroid/view/View;Z)V

    goto :goto_1

    .line 173
    .end local v4    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_1
    const/high16 v5, -0x1000000

    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->setBackgroundColor(I)V

    .line 175
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTimeAnimator:Landroid/animation/TimeAnimator;

    new-instance v6, Lcom/sec/android/app/FlashBarService/SwitchView$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/FlashBarService/SwitchView$1;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    invoke-virtual {v5, v6}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 195
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x10501b5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I

    .line 197
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x1060151

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBorderLineColor:I

    .line 200
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/SwitchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/FlashBarService/SwitchView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I

    return p1
.end method

.method static synthetic access$008(Lcom/sec/android/app/FlashBarService/SwitchView;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFrames:I

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/TimeAnimator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTimeAnimator:Landroid/animation/TimeAnimator;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/FlashBarService/SwitchView;Lcom/sec/android/app/FlashBarService/SwitchView$State;)Lcom/sec/android/app/FlashBarService/SwitchView$State;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/SwitchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mNeedsHideAnimation:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/SwitchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateTransparentHiding()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/SwitchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/android/app/FlashBarService/SwitchView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;
    .param p1, "x1"    # I

    .prologue
    .line 27
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    return p1
.end method

.method static synthetic access$1402(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/SwitchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/SwitchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBorderLineColor:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/SwitchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/FlashBarService/SwitchView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/SwitchView;)Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/SwitchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->triggerEndCallback()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/SwitchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->triggerRealEndCallback()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/SwitchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/SwitchView;)Landroid/animation/Animator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/animation/Animator;)Landroid/animation/Animator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;
    .param p1, "x1"    # Landroid/animation/Animator;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mCurrentZoomAnimation:Landroid/animation/Animator;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/SwitchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/SwitchView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SwitchView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    return-object v0
.end method

.method private activateZone(I)V
    .locals 4
    .param p1, "zone"    # I

    .prologue
    const/4 v3, 0x0

    .line 694
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 696
    .local v0, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 697
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    .line 698
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 699
    return-void
.end method

.method private animateTransparentHiding()V
    .locals 5

    .prologue
    .line 617
    const-string v1, "alpha"

    const/4 v2, 0x1

    new-array v2, v2, [F

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x96

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 618
    .local v0, "a":Landroid/animation/ObjectAnimator;
    new-instance v1, Lcom/sec/android/app/FlashBarService/SwitchView$5;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/SwitchView$5;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 637
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 638
    return-void
.end method

.method private triggerEndCallback()V
    .locals 1

    .prologue
    .line 641
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    if-eqz v0, :cond_0

    .line 642
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;->onLastAnimationFinished()V

    .line 644
    :cond_0
    return-void
.end method

.method private triggerRealEndCallback()V
    .locals 1

    .prologue
    .line 647
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    if-eqz v0, :cond_0

    .line 648
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    invoke-interface {v0}, Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;->onLastAnimationFinishedReal()V

    .line 650
    :cond_0
    return-void
.end method


# virtual methods
.method public adjustBounds(Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "bounds"    # Landroid/graphics/Rect;

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 805
    iget v0, p1, Landroid/graphics/Rect;->left:I

    if-eqz v0, :cond_0

    .line 806
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 808
    :cond_0
    iget v0, p1, Landroid/graphics/Rect;->top:I

    if-eqz v0, :cond_1

    .line 809
    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    .line 811
    :cond_1
    iget v0, p1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->getWidth()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 812
    iget v0, p1, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 814
    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->getHeight()I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 815
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mBroderThickness:I

    int-to-float v1, v1

    div-float/2addr v1, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    .line 828
    :cond_3
    return-void
.end method

.method animateAlpha(FLandroid/view/View;)V
    .locals 6
    .param p1, "to"    # F
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 384
    const-wide/16 v2, 0x96

    .line 386
    .local v2, "duration":J
    const-string v1, "alpha"

    const/4 v4, 0x1

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput p1, v4, v5

    invoke-static {p2, v1, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v4, 0x96

    invoke-virtual {v1, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 389
    .local v0, "a":Landroid/animation/ObjectAnimator;
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 390
    return-void
.end method

.method animateBack()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1f4

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 563
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    if-nez v3, :cond_1

    .line 564
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    const-string v4, "animateBack(): mLockedView=null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :cond_0
    :goto_0
    return-void

    .line 568
    :cond_1
    sget-object v3, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_BACK:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 570
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    const-string v4, "translationX"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 573
    .local v1, "ax":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    const-string v4, "translationY"

    new-array v5, v7, [F

    aput v8, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 576
    .local v2, "ay":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 577
    .local v0, "a":Landroid/animation/AnimatorSet;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v6

    aput-object v2, v3, v7

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 579
    new-instance v3, Lcom/sec/android/app/FlashBarService/SwitchView$4;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/SwitchView$4;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 608
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 610
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    if-eq v3, v4, :cond_0

    .line 611
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    invoke-virtual {p0, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderOut(I)V

    .line 612
    const/4 v3, -0x1

    iput v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    goto :goto_0
.end method

.method animateBorder(FI)V
    .locals 4
    .param p1, "to"    # F
    .param p2, "zone"    # I

    .prologue
    .line 376
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "animateBorder("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 380
    .local v0, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {p0, p1, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateAlpha(FLandroid/view/View;)V

    .line 381
    return-void
.end method

.method animateBorderIn(I)V
    .locals 1
    .param p1, "zone"    # I

    .prologue
    .line 372
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorder(FI)V

    .line 373
    return-void
.end method

.method animateBorderOut(I)V
    .locals 1
    .param p1, "zone"    # I

    .prologue
    .line 368
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorder(FI)V

    .line 369
    return-void
.end method

.method animateSwitch(II)V
    .locals 25
    .param p1, "zone1"    # I
    .param p2, "zone2"    # I

    .prologue
    .line 491
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    move/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    move-object/from16 v20, v0

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "animateSwitch("

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ", "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string v22, ")"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_0
    sget-object v20, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_SWITCH:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 496
    .local v15, "z1":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    move-object/from16 v20, v0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 498
    .local v16, "z2":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->width()I

    move-result v20

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->width()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_1

    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/Rect;->height()I

    move-result v20

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Rect;->height()I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_2

    :cond_1
    const/16 v20, 0x1

    :goto_0
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/FlashBarService/SwitchView;->mNeedsHideAnimation:Z

    .line 501
    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "x"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v10

    .line 503
    .local v10, "ax1":Landroid/animation/ObjectAnimator;
    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "y"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v12

    .line 506
    .local v12, "ay1":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "x"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v11

    .line 508
    .local v11, "ax2":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "y"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v13

    .line 511
    .local v13, "ay2":Landroid/animation/ObjectAnimator;
    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "scaleX"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const v24, 0x3f63d70a    # 0.89f

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 513
    .local v4, "asx1":Landroid/animation/ObjectAnimator;
    iget-object v0, v15, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "scaleY"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const v24, 0x3f63d70a    # 0.89f

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x1f4

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v7

    .line 516
    .local v7, "asy1":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "scaleX"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const v24, 0x3f4ccccd    # 0.8f

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0xa7

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 518
    .local v5, "asx2":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "scaleY"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const v24, 0x3f4ccccd    # 0.8f

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0xa7

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v8

    .line 520
    .local v8, "asy2":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "scaleX"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const v24, 0x3f63d70a    # 0.89f

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x14d

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 522
    .local v6, "asx22":Landroid/animation/ObjectAnimator;
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    const-string v21, "scaleY"

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [F

    move-object/from16 v22, v0

    const/16 v23, 0x0

    const v24, 0x3f63d70a    # 0.89f

    aput v24, v22, v23

    invoke-static/range {v20 .. v22}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v20

    const-wide/16 v22, 0x14d

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v9

    .line 525
    .local v9, "asy22":Landroid/animation/ObjectAnimator;
    new-instance v18, Landroid/animation/AnimatorSet;

    invoke-direct/range {v18 .. v18}, Landroid/animation/AnimatorSet;-><init>()V

    .line 526
    .local v18, "zset1":Landroid/animation/AnimatorSet;
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v5, v20, v21

    const/16 v21, 0x1

    aput-object v8, v20, v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 528
    new-instance v19, Landroid/animation/AnimatorSet;

    invoke-direct/range {v19 .. v19}, Landroid/animation/AnimatorSet;-><init>()V

    .line 529
    .local v19, "zset2":Landroid/animation/AnimatorSet;
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v6, v20, v21

    const/16 v21, 0x1

    aput-object v9, v20, v21

    invoke-virtual/range {v19 .. v20}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 531
    new-instance v17, Landroid/animation/AnimatorSet;

    invoke-direct/range {v17 .. v17}, Landroid/animation/AnimatorSet;-><init>()V

    .line 532
    .local v17, "zset":Landroid/animation/AnimatorSet;
    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v18, v20, v21

    const/16 v21, 0x1

    aput-object v19, v20, v21

    move-object/from16 v0, v17

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 534
    new-instance v14, Landroid/animation/AnimatorSet;

    invoke-direct {v14}, Landroid/animation/AnimatorSet;-><init>()V

    .line 535
    .local v14, "set":Landroid/animation/AnimatorSet;
    const/16 v20, 0x7

    move/from16 v0, v20

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    aput-object v10, v20, v21

    const/16 v21, 0x1

    aput-object v12, v20, v21

    const/16 v21, 0x2

    aput-object v11, v20, v21

    const/16 v21, 0x3

    aput-object v13, v20, v21

    const/16 v21, 0x4

    aput-object v17, v20, v21

    const/16 v21, 0x5

    aput-object v4, v20, v21

    const/16 v21, 0x6

    aput-object v7, v20, v21

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 536
    new-instance v20, Lcom/sec/android/app/FlashBarService/SwitchView$3;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView$3;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView;)V

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 555
    invoke-virtual {v14}, Landroid/animation/AnimatorSet;->start()V

    .line 556
    return-void

    .line 498
    .end local v4    # "asx1":Landroid/animation/ObjectAnimator;
    .end local v5    # "asx2":Landroid/animation/ObjectAnimator;
    .end local v6    # "asx22":Landroid/animation/ObjectAnimator;
    .end local v7    # "asy1":Landroid/animation/ObjectAnimator;
    .end local v8    # "asy2":Landroid/animation/ObjectAnimator;
    .end local v9    # "asy22":Landroid/animation/ObjectAnimator;
    .end local v10    # "ax1":Landroid/animation/ObjectAnimator;
    .end local v11    # "ax2":Landroid/animation/ObjectAnimator;
    .end local v12    # "ay1":Landroid/animation/ObjectAnimator;
    .end local v13    # "ay2":Landroid/animation/ObjectAnimator;
    .end local v14    # "set":Landroid/animation/AnimatorSet;
    .end local v17    # "zset":Landroid/animation/AnimatorSet;
    .end local v18    # "zset1":Landroid/animation/AnimatorSet;
    .end local v19    # "zset2":Landroid/animation/AnimatorSet;
    :cond_2
    const/16 v20, 0x0

    goto/16 :goto_0
.end method

.method animateTouchLock()V
    .locals 1

    .prologue
    .line 346
    const v0, 0x3f68f5c3    # 0.91f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateTouchLockInternal(F)V

    .line 347
    return-void
.end method

.method animateTouchLockInternal(F)V
    .locals 10
    .param p1, "to"    # F

    .prologue
    const-wide/16 v8, 0xfa

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 354
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "animateTouchLockInternal("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 356
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    const-string v4, "scaleX"

    new-array v5, v7, [F

    aput p1, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 358
    .local v1, "ax":Landroid/animation/ObjectAnimator;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    const-string v4, "scaleY"

    new-array v5, v7, [F

    aput p1, v5, v6

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 361
    .local v2, "ay":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 362
    .local v0, "as":Landroid/animation/AnimatorSet;
    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v1, v3, v6

    aput-object v2, v3, v7

    invoke-virtual {v0, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 364
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 365
    return-void
.end method

.method animateTouchUnlock()V
    .locals 1

    .prologue
    .line 350
    const v0, 0x3f63d70a    # 0.89f

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateTouchLockInternal(F)V

    .line 351
    return-void
.end method

.method animateZoom(FJ)V
    .locals 12
    .param p1, "to"    # F
    .param p2, "duration"    # J

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 405
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "animateZoom("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ")"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    :cond_0
    sget-object v7, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_ZOOM:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 408
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 409
    .local v4, "set":Landroid/animation/AnimatorSet;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 410
    .local v6, "zone":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 412
    .local v5, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-boolean v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    if-eqz v7, :cond_1

    .line 414
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    const-string v8, "scaleX"

    new-array v9, v11, [F

    aput p1, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 416
    .local v1, "ax":Landroid/animation/ObjectAnimator;
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    const-string v8, "scaleY"

    new-array v9, v11, [F

    aput p1, v9, v10

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v7, p2, p3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 419
    .local v2, "ay":Landroid/animation/ObjectAnimator;
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 420
    .local v0, "as":Landroid/animation/AnimatorSet;
    const/4 v7, 0x2

    new-array v7, v7, [Landroid/animation/Animator;

    aput-object v1, v7, v10

    aput-object v2, v7, v11

    invoke-virtual {v0, v7}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 422
    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto :goto_0

    .line 425
    .end local v0    # "as":Landroid/animation/AnimatorSet;
    .end local v1    # "ax":Landroid/animation/ObjectAnimator;
    .end local v2    # "ay":Landroid/animation/ObjectAnimator;
    .end local v5    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    .end local v6    # "zone":Ljava/lang/Integer;
    :cond_2
    new-instance v7, Lcom/sec/android/app/FlashBarService/SwitchView$2;

    invoke-direct {v7, p0, v4}, Lcom/sec/android/app/FlashBarService/SwitchView$2;-><init>(Lcom/sec/android/app/FlashBarService/SwitchView;Landroid/animation/AnimatorSet;)V

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 484
    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    .line 485
    return-void
.end method

.method animateZoomIn()V
    .locals 4

    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    const-string v1, "animateZoomIn()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    const-wide/16 v2, 0xfa

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateZoom(FJ)V

    .line 402
    return-void
.end method

.method animateZoomOut()V
    .locals 4

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    const-string v1, "animateZoomOut()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    :cond_0
    const v0, 0x3f63d70a    # 0.89f

    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v0, v2, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateZoom(FJ)V

    .line 396
    return-void
.end method

.method bringChildToBack(Landroid/view/View;Z)V
    .locals 8
    .param p1, "child"    # Landroid/view/View;
    .param p2, "force"    # Z

    .prologue
    .line 214
    const/4 v2, 0x1

    .line 215
    .local v2, "index":I
    if-nez p2, :cond_1

    .line 216
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 217
    .local v4, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v5, v4, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 218
    .local v0, "borderIndex":I
    if-le v0, v2, :cond_0

    .line 219
    move v2, v0

    goto :goto_0

    .line 223
    .end local v0    # "borderIndex":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v4    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 225
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 226
    .local v3, "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/SwitchView;->removeView(Landroid/view/View;)V

    .line 227
    iget-boolean v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "bringChildToBack(): index="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    :cond_2
    invoke-virtual {p0, p1, v2, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 229
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 782
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 784
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->hide()V

    .line 787
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v8, -0x1

    .line 244
    sget-object v4, Lcom/sec/android/app/FlashBarService/SwitchView$6;->$SwitchMap$com$sec$android$app$FlashBarService$SwitchView$State:[I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 339
    :cond_0
    :goto_0
    return v9

    .line 246
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    .line 247
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 248
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    const-string v5, "State error!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 251
    .local v3, "zone":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 253
    .local v2, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-boolean v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 256
    iget-object v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    .line 257
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    .line 258
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mDiffPoint:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getX()F

    move-result v6

    sub-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getY()F

    move-result v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Point;->set(II)V

    .line 260
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->bringToFront()V

    .line 261
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->requestLayout()V

    .line 265
    .end local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    .end local v3    # "zone":Ljava/lang/Integer;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    if-eqz v4, :cond_0

    .line 266
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateTouchLock()V

    .line 267
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    if-eq v4, v8, :cond_4

    .line 268
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderOut(I)V

    .line 270
    :cond_4
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    .line 271
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderIn(I)V

    .line 272
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    .line 273
    sget-object v4, Lcom/sec/android/app/FlashBarService/SwitchView$State;->MOVING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    goto/16 :goto_0

    .line 279
    .end local v0    # "i$":Ljava/util/Iterator;
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_b

    .line 280
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mDiffPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setX(F)V

    .line 281
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mDiffPoint:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setY(F)V

    .line 283
    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    .line 284
    .local v1, "oldTarget":I
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    .line 285
    const/4 v2, 0x0

    .line 286
    .restart local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 287
    .restart local v3    # "zone":Ljava/lang/Integer;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    check-cast v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 289
    .restart local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-boolean v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    if-eqz v4, :cond_5

    iget-object v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 291
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    .line 295
    .end local v3    # "zone":Ljava/lang/Integer;
    :cond_6
    if-nez v2, :cond_7

    .line 296
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    const-string v5, "No target zone found for event! Zones configuration is not correct!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 299
    :cond_7
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    if-eq v4, v8, :cond_8

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    if-eq v4, v5, :cond_8

    .line 300
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderOut(I)V

    .line 301
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    .line 303
    :cond_8
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    if-eq v4, v1, :cond_0

    .line 304
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    if-eq v4, v8, :cond_9

    .line 305
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderIn(I)V

    .line 306
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    .line 308
    :cond_9
    if-eq v1, v8, :cond_a

    .line 309
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBorderOut(I)V

    .line 311
    :cond_a
    if-eq v1, v8, :cond_0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    if-eq v4, v8, :cond_0

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    if-eq v4, v5, :cond_0

    .line 312
    iget-object v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->bringChildToBack(Landroid/view/View;Z)V

    .line 313
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->requestLayout()V

    goto/16 :goto_0

    .line 316
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "oldTarget":I
    .end local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_b
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-ne v4, v9, :cond_e

    .line 317
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    if-eq v4, v8, :cond_d

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    if-eq v4, v5, :cond_d

    .line 318
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    if-eqz v4, :cond_c

    .line 319
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    iget v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    invoke-interface {v4, v5, v6}, Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;->onSwitch(II)V

    .line 322
    :cond_c
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateSwitch(II)V

    .line 323
    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    .line 324
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    .line 325
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    goto/16 :goto_0

    .line 327
    :cond_d
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBack()V

    goto/16 :goto_0

    .line 329
    :cond_e
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 330
    sget-object v4, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 331
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateBack()V

    .line 332
    iput-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedView:Landroid/widget/ImageView;

    .line 333
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mLockedZone:I

    .line 334
    iput v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTarget:I

    goto/16 :goto_0

    .line 244
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHideRequested:Z

    .line 55
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING_ZOOM:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    if-ne v0, v1, :cond_1

    .line 57
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->animateZoomIn()V

    .line 59
    :cond_1
    return-void
.end method

.method public invalidateRectangles()V
    .locals 4

    .prologue
    .line 702
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 703
    .local v1, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 704
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 706
    .end local v1    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/SwitchView;->setAlpha(F)V

    .line 707
    return-void
.end method

.method public layoutZones()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 731
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 732
    .local v1, "dm":Landroid/util/DisplayMetrics;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    .line 733
    .local v6, "zone":Ljava/lang/Integer;
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v7, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 735
    .local v5, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-boolean v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    if-eqz v7, :cond_0

    .line 739
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mMultiwindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getZoneBounds(I)Landroid/graphics/Rect;

    move-result-object v7

    iput-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    .line 741
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    if-nez v7, :cond_1

    .line 742
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "getZoneBounds("

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ") is null"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 746
    :cond_1
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    iget v8, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    if-le v7, v8, :cond_2

    .line 747
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v8, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v8, v7, Landroid/graphics/Rect;->right:I

    .line 749
    :cond_2
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget v8, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v7, v8, :cond_3

    .line 750
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v8, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v8, v7, Landroid/graphics/Rect;->bottom:I

    .line 752
    :cond_3
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-gez v7, :cond_4

    .line 753
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iput v12, v7, Landroid/graphics/Rect;->top:I

    .line 755
    :cond_4
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    if-gez v7, :cond_5

    .line 756
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iput v12, v7, Landroid/graphics/Rect;->left:I

    .line 758
    :cond_5
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    iget-object v8, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget-object v9, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    iget-object v10, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    iget-object v11, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/widget/ImageView;->layout(IIII)V

    .line 759
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 760
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 761
    new-instance v0, Landroid/graphics/Rect;

    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    invoke-direct {v0, v7}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 762
    .local v0, "borderRect":Landroid/graphics/Rect;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/SwitchView;->adjustBounds(Landroid/graphics/Rect;)V

    .line 763
    iget-object v7, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    iget v8, v0, Landroid/graphics/Rect;->left:I

    iget v9, v0, Landroid/graphics/Rect;->top:I

    iget v10, v0, Landroid/graphics/Rect;->right:I

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 764
    iget-boolean v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "layoutZones(): rect="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v5, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 767
    .end local v0    # "borderRect":Landroid/graphics/Rect;
    .end local v5    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    .end local v6    # "zone":Ljava/lang/Integer;
    :cond_6
    const/4 v2, 0x0

    .line 768
    .local v2, "horizontalLeft":I
    const/4 v7, 0x4

    iget v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mArrangeState:I

    if-ne v7, v8, :cond_7

    .line 769
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    iget v2, v7, Landroid/graphics/Point;->x:I

    .line 771
    :cond_7
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->getMeasuredWidth()I

    move-result v3

    .line 772
    .local v3, "horizontalRight":I
    const/4 v7, 0x2

    iget v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mArrangeState:I

    if-ne v7, v8, :cond_8

    .line 773
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    iget v3, v7, Landroid/graphics/Point;->x:I

    .line 776
    :cond_8
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mHLine:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    add-int/lit8 v8, v8, -0x1

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v7, v2, v8, v3, v9}, Landroid/widget/ImageView;->layout(IIII)V

    .line 777
    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mVLine:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    add-int/lit8 v8, v8, -0x1

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    iget v9, v9, Landroid/graphics/Point;->x:I

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->getMeasuredHeight()I

    move-result v10

    invoke-virtual {v7, v8, v12, v9, v10}, Landroid/widget/ImageView;->layout(IIII)V

    .line 778
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 792
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->NONE:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    if-ne v0, v1, :cond_0

    .line 793
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->invalidateRectangles()V

    .line 794
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->triggerEndCallback()V

    .line 795
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->triggerRealEndCallback()V

    .line 797
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->updateActiveZones()V

    .line 798
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 343
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 721
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->getMeasuredWidth()I

    move-result v0

    .line 723
    .local v0, "width":I
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 725
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->getMeasuredWidth()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 726
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->layoutZones()V

    .line 728
    :cond_0
    return-void
.end method

.method public prepareAnimation()V
    .locals 3

    .prologue
    .line 232
    sget-object v1, Lcom/sec/android/app/FlashBarService/SwitchView$State;->ANIMATING:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mState:Lcom/sec/android/app/FlashBarService/SwitchView$State;

    .line 233
    iget v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 234
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 235
    .local v0, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/SwitchView;->bringChildToFront(Landroid/view/View;)V

    .line 236
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->requestLayout()V

    .line 238
    .end local v0    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mTimeAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->start()V

    .line 239
    return-void
.end method

.method public setAnimationFinishedCallback(Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;)V
    .locals 0
    .param p1, "animationFinishedCallback"    # Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mSwitchWindowControllerCallbacks:Lcom/sec/android/app/FlashBarService/SwitchView$SwitchWindowControllerCallbacks;

    .line 44
    return-void
.end method

.method public setFocusedZone(I)V
    .locals 0
    .param p1, "focusedZone"    # I

    .prologue
    .line 801
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    iput p1, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedAppZone:I

    .line 802
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 710
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 711
    .local v2, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-boolean v3, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    if-eqz v3, :cond_0

    .line 713
    iget-object v3, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget-object v6, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->rect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-static {p1, v3, v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 714
    .local v0, "cropped":Landroid/graphics/Bitmap;
    iget-object v3, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 716
    .end local v0    # "cropped":Landroid/graphics/Bitmap;
    .end local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 717
    return-void
.end method

.method public updateActiveZones()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/16 v5, 0x8

    .line 653
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    const-string v4, "setActiveZones()"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 655
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mMultiwindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getArrangeState()I

    move-result v0

    .line 657
    .local v0, "arrangeState":I
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mArrangeState:I

    if-eq v3, v0, :cond_3

    .line 658
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 659
    .local v2, "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v3, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 660
    iput-boolean v9, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->isActive:Z

    .line 661
    iget-object v3, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 663
    .end local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_1
    if-ne v6, v0, :cond_5

    .line 664
    const/4 v3, 0x3

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 665
    const/16 v3, 0xc

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 682
    :cond_2
    :goto_1
    iput v0, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mArrangeState:I

    .line 684
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    iget v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    .line 685
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mZones:Ljava/util/HashMap;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mFocusedZone:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;

    .line 686
    .restart local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    iget-object v3, v2, Lcom/sec/android/app/FlashBarService/SwitchView$Zone;->border:Landroid/view/View;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 689
    .end local v2    # "z":Lcom/sec/android/app/FlashBarService/SwitchView$Zone;
    :cond_4
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mMultiwindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    invoke-virtual {v3, v9}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getCenterBarPoint(I)Landroid/graphics/Point;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->mPoint:Landroid/graphics/Point;

    .line 690
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SwitchView;->layoutZones()V

    .line 691
    return-void

    .line 666
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_5
    if-ne v5, v0, :cond_6

    .line 667
    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 668
    invoke-direct {p0, v7}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 669
    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 670
    invoke-direct {p0, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    goto :goto_1

    .line 671
    :cond_6
    if-ne v8, v0, :cond_7

    .line 672
    const/4 v3, 0x3

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 673
    invoke-direct {p0, v8}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 674
    invoke-direct {p0, v5}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    goto :goto_1

    .line 675
    :cond_7
    if-ne v7, v0, :cond_8

    .line 676
    const/16 v3, 0xc

    invoke-direct {p0, v3}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 677
    invoke-direct {p0, v6}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    .line 678
    invoke-direct {p0, v7}, Lcom/sec/android/app/FlashBarService/SwitchView;->activateZone(I)V

    goto :goto_1

    .line 680
    :cond_8
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->DEBUG:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SwitchView;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown arranged state ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")! All views are hidden."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
