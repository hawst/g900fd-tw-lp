.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 852
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 19
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 857
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v15}, Lcom/sec/android/app/FlashBarService/AppListController;->isEnableMakePenWindow()Z

    move-result v15

    if-nez v15, :cond_1

    .line 954
    :cond_0
    :goto_0
    return-void

    .line 860
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v15

    const/16 v16, -0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_0

    .line 861
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mSupportMultiInstance:Z

    move/from16 v17, v0

    const/16 v18, 0x64

    invoke-virtual/range {v15 .. v18}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getFlashBarIntent(IZI)Ljava/util/List;

    move-result-object v5

    .line 862
    .local v5, "lIntent":Ljava/util/List;, "Ljava/util/List<Landroid/content/Intent;>;"
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_0

    .line 865
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    const/4 v15, 0x0

    invoke-interface {v5, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/content/Intent;

    move-object/from16 v0, v16

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v0, v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;)Landroid/content/Intent;

    .line 866
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntents:Ljava/util/List;
    invoke-static {v15, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Ljava/util/List;)Ljava/util/List;

    .line 869
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 870
    .local v2, "displayFrame":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v16

    const/16 v17, 0x0

    invoke-virtual/range {v15 .. v17}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    move-result-object v1

    .line 871
    .local v1, "aInfo":Landroid/content/pm/ActivityInfo;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v15, v1}, Lcom/sec/android/app/FlashBarService/AppListController;->isSupportScaleApp(Landroid/content/pm/ActivityInfo;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 872
    new-instance v6, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v15, 0x2

    invoke-direct {v6, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 873
    .local v6, "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    const/16 v15, 0x800

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setOption(IZ)V

    .line 874
    invoke-virtual {v6, v2}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setBounds(Landroid/graphics/Rect;)V

    .line 875
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v15

    invoke-virtual {v15, v6}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 876
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v16

    const/16 v17, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 878
    .end local v6    # "mwStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v15, v0, :cond_0

    .line 879
    new-instance v13, Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/4 v15, 0x1

    invoke-direct {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;-><init>(I)V

    .line 880
    .local v13, "windowStyle":Lcom/samsung/android/multiwindow/MultiWindowStyle;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    const/16 v16, 0x64

    const/16 v17, 0x1

    invoke-virtual/range {v15 .. v17}, Lcom/samsung/android/multiwindow/MultiWindowFacade;->getRunningTasks(II)Ljava/util/List;

    move-result-object v11

    .line 881
    .local v11, "taskInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v10, 0x0

    .line 882
    .local v10, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    if-eqz v11, :cond_5

    .line 883
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 884
    .local v12, "visibleTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget-object v15, v12, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v16, 0x8

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v15

    if-nez v15, :cond_3

    iget-object v15, v12, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v16, 0x400

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v15

    if-nez v15, :cond_3

    .line 886
    iget-object v15, v12, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v15

    if-nez v15, :cond_4

    iget-object v15, v12, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getType()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-eq v15, v0, :cond_3

    .line 890
    :cond_4
    move-object v10, v12

    .line 895
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v12    # "visibleTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_5
    if-nez v10, :cond_6

    .line 896
    if-eqz v11, :cond_a

    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_a

    .line 897
    const/4 v15, 0x0

    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    .end local v10    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    check-cast v10, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 907
    .restart local v10    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/16 v16, 0x0

    const/16 v17, 0x1

    const/16 v18, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getRunningTaskCnt(ZZZ)I
    invoke-static/range {v15 .. v18}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;ZZZ)I

    move-result v9

    .line 908
    .local v9, "runningCnt":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v15}, Lcom/sec/android/app/FlashBarService/AppListController;->getAvailableMultiInstanceCnt()I

    move-result v15

    if-lt v9, v15, :cond_7

    .line 909
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/Intent;->getFlags()I

    move-result v16

    const v17, -0x8000001

    and-int v16, v16, v17

    invoke-virtual/range {v15 .. v16}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 912
    :cond_7
    const/4 v7, 0x0

    .line 913
    .local v7, "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    const/4 v4, 0x0

    .line 915
    .local v4, "isNextZoneSamePackage":Z
    iget-object v15, v10, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v14

    .line 916
    .local v14, "zone":I
    if-eqz v14, :cond_8

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x2

    move/from16 v0, v16

    if-lt v15, v0, :cond_8

    .line 917
    const/4 v15, 0x1

    invoke-interface {v11, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    .end local v7    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    check-cast v7, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 920
    .restart local v7    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_8
    iget-object v15, v10, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    const/16 v16, 0x2

    invoke-virtual/range {v15 .. v16}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->isEnabled(I)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 921
    if-eqz v7, :cond_9

    .line 922
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v15, v15, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconIndex:I
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v16

    invoke-virtual/range {v15 .. v16}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAppResolveInfo(I)Landroid/content/pm/ResolveInfo;

    move-result-object v8

    .line 923
    .local v8, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v15, v8, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v15, v15, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v7, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 924
    const/4 v4, 0x1

    .line 928
    .end local v8    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_9
    iget-object v15, v10, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v15

    const/16 v16, 0x3

    move/from16 v0, v16

    if-ne v15, v0, :cond_c

    .line 929
    if-eqz v4, :cond_b

    .line 930
    const/4 v15, 0x3

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 943
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v15

    invoke-virtual {v15, v13}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 949
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v16

    const/16 v17, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 899
    .end local v4    # "isNextZoneSamePackage":Z
    .end local v7    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v9    # "runningCnt":I
    .end local v14    # "zone":I
    :cond_a
    const/4 v15, 0x1

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 900
    const/16 v15, 0xf

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 901
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v15

    invoke-virtual {v15, v13}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    .line 902
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    move-object/from16 v16, v0

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v16

    const/16 v17, 0x1

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationHistoryBarThumbNailAddDisappear(Landroid/content/Intent;Z)V
    invoke-static/range {v15 .. v17}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Landroid/content/Intent;Z)V

    goto/16 :goto_0

    .line 932
    .restart local v4    # "isNextZoneSamePackage":Z
    .restart local v7    # "nextTaskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    .restart local v9    # "runningCnt":I
    .restart local v14    # "zone":I
    :cond_b
    const/16 v15, 0xc

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 934
    :cond_c
    iget-object v15, v10, Landroid/app/ActivityManager$RunningTaskInfo;->multiWindowStyle:Lcom/samsung/android/multiwindow/MultiWindowStyle;

    invoke-virtual {v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->getZone()I

    move-result v15

    const/16 v16, 0xc

    move/from16 v0, v16

    if-ne v15, v0, :cond_e

    .line 935
    if-eqz v4, :cond_d

    .line 936
    const/16 v15, 0xc

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 938
    :cond_d
    const/4 v15, 0x3

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 941
    :cond_e
    const/16 v15, 0xc

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    goto :goto_1

    .line 945
    :cond_f
    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setType(I)V

    .line 946
    const/4 v15, 0x0

    invoke-virtual {v13, v15}, Lcom/samsung/android/multiwindow/MultiWindowStyle;->setZone(I)V

    .line 947
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIntent:Landroid/content/Intent;
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/Intent;

    move-result-object v15

    invoke-virtual {v15, v13}, Landroid/content/Intent;->setMultiWindowStyle(Lcom/samsung/android/multiwindow/MultiWindowStyle;)V

    goto :goto_2
.end method
