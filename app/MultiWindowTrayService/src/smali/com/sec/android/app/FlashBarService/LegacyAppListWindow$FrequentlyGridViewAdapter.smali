.class public Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FrequentlyGridViewAdapter"
.end annotation


# instance fields
.field mInflater:Landroid/view/LayoutInflater;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;I)V
    .locals 2
    .param p2, "position"    # I

    .prologue
    .line 5832
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 5833
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    .line 5834
    return-void
.end method


# virtual methods
.method public createView(Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 5856
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f03000b

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 5858
    .local v0, "convertView":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;-><init>()V

    .line 5859
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;
    const v2, 0x7f0f0059

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    .line 5860
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 5861
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 5862
    const v2, 0x7f0f005c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    .line 5863
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 5864
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 5866
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyPageIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5867
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 5868
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyPageIconClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 5869
    iget-object v2, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyIconTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 5871
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 5872
    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 5838
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFrequentlyUsedAppCnt:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15200(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 5844
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/16 v1, 0x68

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 5851
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/16 v8, 0x50

    const/4 v7, 0x0

    .line 5877
    if-nez p2, :cond_0

    .line 5878
    invoke-virtual {p0, p3, p1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->createView(Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    .line 5879
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 5880
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Recycled child has parent"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 5882
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 5883
    new-instance v5, Ljava/lang/RuntimeException;

    const-string v6, "Recycled child has parent"

    invoke-direct {v5, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 5886
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;

    .line 5887
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;
    const/4 v3, 0x0

    .line 5888
    .local v3, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/16 v6, 0x68

    invoke-virtual {v5, p1, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v3

    .line 5890
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    .line 5891
    .local v2, "label":Ljava/lang/CharSequence;
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 5892
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 5893
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 5895
    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getResolveInfo()Landroid/content/pm/ResolveInfo;

    move-result-object v4

    .line 5896
    .local v4, "resolveInfo":Landroid/content/pm/ResolveInfo;
    if-eqz v4, :cond_3

    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v5, v5, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mSelectedAppPackage:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 5897
    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    if-eqz v5, :cond_4

    iget-object v5, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v5, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 5898
    .local v0, "applicationMetaData":Landroid/os/Bundle;
    :goto_0
    if-eqz v0, :cond_2

    const-string v5, "com.samsung.android.sdk.multiwindow.multiinstance.enable"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$15400(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-result-object v5

    iget-object v6, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    invoke-virtual {v5, v6}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->isSupporMultiInstance(Landroid/content/pm/ActivityInfo;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 5900
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 5901
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;->labelView:Landroid/widget/TextView;

    invoke-static {v8, v7, v7, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 5902
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$FrequentlyGridViewAdapter;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-virtual {v5, v1}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->removeListener(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$AppListItemGridViewHolder;)V

    .line 5906
    .end local v0    # "applicationMetaData":Landroid/os/Bundle;
    :cond_3
    return-object p2

    .line 5897
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
