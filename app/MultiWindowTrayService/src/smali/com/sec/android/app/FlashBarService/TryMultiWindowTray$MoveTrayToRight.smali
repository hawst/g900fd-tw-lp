.class Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;
.super Ljava/lang/Object;
.source "TryMultiWindowTray.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MoveTrayToRight"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

.field tryMWTrayLeft:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Landroid/view/View;)V
    .locals 0
    .param p2, "vw"    # Landroid/view/View;

    .prologue
    .line 684
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 685
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    .line 686
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v12, -0x1

    const/4 v11, 0x1

    const/4 v10, -0x2

    const/16 v9, 0x8

    const/4 v8, 0x0

    .line 690
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    float-to-int v0, v5

    .line 691
    .local v0, "X":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    float-to-int v1, v5

    .line 693
    .local v1, "Y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    and-int/lit16 v5, v5, 0xff

    packed-switch v5, :pswitch_data_0

    .line 816
    :cond_0
    :goto_0
    :pswitch_0
    return v11

    .line 696
    :pswitch_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 697
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 698
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 703
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 704
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 705
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup1:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/view/animation/Animation;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 710
    :goto_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopUpArrow1:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 712
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 713
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 714
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 715
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 717
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mIvt:[B
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)[B

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 718
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWFeatureLevel:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 719
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayPopup2:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mBubbleAnimation:Landroid/view/animation/Animation;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/view/animation/Animation;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 721
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerRight:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 722
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->trayToRight:Z
    invoke-static {v5, v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3402(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    goto/16 :goto_0

    .line 708
    :cond_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayGuideCue1:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2800(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 724
    :cond_3
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayLeft:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 725
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 726
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 727
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayDragLeft:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 728
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setX(F)V

    .line 729
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setY(F)V

    .line 730
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mIvt:[B
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)[B

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/os/SystemVibrator;

    move-result-object v7

    invoke-virtual {v7}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 731
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 732
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    new-instance v6, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    const/4 v8, 0x0

    invoke-direct {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRightLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$1;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 733
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 734
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayRight:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 743
    :pswitch_3
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3600(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 744
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mRotated:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3602(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    goto/16 :goto_0

    .line 747
    :cond_4
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00b1

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    sub-int v5, v0, v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayWindowHelper:Landroid/widget/RelativeLayout;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    if-le v5, v6, :cond_6

    .line 748
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z
    invoke-static {v5, v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    .line 749
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInLeft:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3802(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    .line 750
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 751
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ImageView;->bringToFront()V

    .line 752
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 754
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayBarShadow:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    const v6, 0x7f0200fb

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 756
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v12, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 759
    .local v2, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00b2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 760
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 762
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    const v6, 0x7f020101

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 763
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 766
    .local v3, "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00b4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 767
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00cb

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 768
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00cc

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 769
    const/16 v5, 0xc

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 770
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 771
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 774
    .local v4, "lp3":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00b4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 775
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketDivider:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 803
    :goto_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v5

    if-ne v5, v11, :cond_5

    .line 804
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x4

    sub-int v6, v0, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setX(F)V

    .line 805
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v6, v1, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setY(F)V

    .line 808
    :cond_5
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mOrientation:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 809
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x7

    sub-int v6, v0, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setX(F)V

    .line 810
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->tryMWTrayLeft:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v6, v1, v6

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setY(F)V

    goto/16 :goto_0

    .line 777
    .end local v2    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "lp3":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInRight:Z
    invoke-static {v5, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3002(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    .line 778
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # setter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTrayInLeft:Z
    invoke-static {v5, v11}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3802(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;Z)Z

    .line 779
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleRightShadow:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3500(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 780
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0200eb

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 781
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayHandleLeft:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 782
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayBarShadow:Landroid/widget/ImageView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/ImageView;

    move-result-object v5

    const v6, 0x7f0200e6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 784
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v12, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 787
    .restart local v2    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00b3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 788
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mTryMWTrayImageContainerLeft:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 790
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    const v6, 0x7f0200ec

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 791
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 794
    .restart local v3    # "lp2":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00cb

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 795
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mResource:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00cc

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v5, v5

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 796
    const/16 v5, 0xc

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 797
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketButton:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 798
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 801
    .restart local v4    # "lp3":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray$MoveTrayToRight;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->mMWTryPocketDivider:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;->access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowTray;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 693
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
