.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;
.super Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeFlashBarLayout(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 1780
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrawerClosed(Landroid/view/View;)V
    .locals 6
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 1796
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v3, v5}, Lcom/sec/android/app/FlashBarService/AppListController;->setMWTrayOpenState(Z)V

    .line 1797
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 1798
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 1799
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 1801
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1802
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v3

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1803
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1804
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v3, v3, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1805
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->dismissHistoryWindow(Z)V
    invoke-static {v3, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)V

    .line 1806
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;->onDrawerClosed(Landroid/view/View;)V

    .line 1809
    new-instance v1, Landroid/graphics/Region;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    invoke-direct {v1, v5, v5, v3, v4}, Landroid/graphics/Region;-><init>(IIII)V

    .line 1810
    .local v1, "region":Landroid/graphics/Region;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v2

    .line 1811
    .local v2, "v":Landroid/view/ViewRootImpl;
    if-eqz v2, :cond_0

    .line 1812
    invoke-virtual {v2, v1}, Landroid/view/ViewRootImpl;->setTransparentRegion(Landroid/graphics/Region;)V

    .line 1814
    :cond_0
    return-void
.end method

.method public onDrawerOpened(Landroid/view/View;)V
    .locals 3
    .param p1, "drawerView"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 1819
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v0

    .line 1820
    .local v0, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getSealedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1830
    :goto_0
    return-void

    .line 1824
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListController;->setMWTrayOpenState(Z)V

    .line 1825
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1202(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 1826
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 1827
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 1828
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->showUsingHelpPopup()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 1829
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;->onDrawerOpened(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onDrawerSlide(Landroid/view/View;F)V
    .locals 3
    .param p1, "drawerView"    # Landroid/view/View;
    .param p2, "slideOffset"    # F

    .prologue
    .line 1784
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v0

    .line 1785
    .local v0, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getSealedState()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1792
    :goto_0
    return-void

    .line 1789
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastDrawerSlideOffset:F
    invoke-static {v1, p2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;F)F

    .line 1790
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelSliding:Z
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1102(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Z)Z

    .line 1791
    invoke-super {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;->onDrawerSlide(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public onDrawerStateChanged(I)V
    .locals 13
    .param p1, "newState"    # I

    .prologue
    const v8, 0x7f0a0019

    const v12, 0x7f0a0023

    const/16 v11, 0x68

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1835
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v2

    .line 1836
    .local v2, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getSealedState()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1887
    :goto_0
    return-void

    .line 1840
    :cond_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v3

    .line 1841
    .local v3, "l":Landroid/view/WindowManager$LayoutParams;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0018

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v5, v6

    .line 1842
    .local v5, "moveBtnWidth":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v4, v6

    .line 1843
    .local v4, "moveBtnHeight":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v0, v6

    .line 1844
    .local v0, "applistHeight":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v6, v6

    add-int/2addr v6, v0

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnOverlapMargin:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mMoveBtnShadowMargin:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v8

    add-int/2addr v7, v8

    sub-int v1, v6, v7

    .line 1847
    .local v1, "flashBarHeight":I
    packed-switch p1, :pswitch_data_0

    .line 1882
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1883
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v7

    invoke-interface {v6, v7, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1885
    invoke-super {p0, p1}, Landroid/support/v4/widget/DrawerLayoutMW$SimpleDrawerListener;->onDrawerStateChanged(I)V

    goto :goto_0

    .line 1849
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mLastDrawerSlideOffset:F
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5100(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)F

    move-result v6

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-nez v6, :cond_3

    .line 1850
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1851
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v11, :cond_2

    .line 1852
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    sub-int/2addr v6, v7

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1854
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updatePocketTrayLayout(IZZ)V
    invoke-static {v6, v7, v9, v10}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZ)V

    .line 1856
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsUsingHelpPopupOpened:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$5500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1857
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->startCollapseTimer()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    goto :goto_1

    .line 1861
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mStatusBarUpdate:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1000(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1862
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v7, v10, v10, v9}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;IZZZ)V

    .line 1863
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v7

    invoke-virtual {v6, v7, v9}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->displayAppListEditWindow(IZ)V

    .line 1866
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mIsBezelOpened:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1867
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->cancelCollapseTimer()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$2700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V

    .line 1869
    :cond_5
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1870
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v11, :cond_1

    .line 1871
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_1

    .line 1875
    :pswitch_2
    iput v0, v3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1876
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    if-ne v6, v11, :cond_1

    .line 1877
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    float-to-int v7, v7

    sub-int/2addr v6, v7

    iput v6, v3, Landroid/view/WindowManager$LayoutParams;->x:I

    goto/16 :goto_1

    .line 1847
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
