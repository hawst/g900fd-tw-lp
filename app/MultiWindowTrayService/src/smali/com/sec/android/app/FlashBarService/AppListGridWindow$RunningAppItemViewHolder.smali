.class final Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppItemViewHolder;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "RunningAppItemViewHolder"
.end annotation


# instance fields
.field iconView:Landroid/widget/ImageView;

.field labelView:Landroid/widget/TextView;

.field tumbnailView:Landroid/widget/ImageView;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "appClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 239
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppItemViewHolder;->iconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 240
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$RunningAppItemViewHolder;->tumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    return-void
.end method
