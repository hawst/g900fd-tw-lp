.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 0

    .prologue
    .line 460
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 464
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setEditMode(Z)V

    .line 466
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->updatePopupViewWindowPosition(Z)V

    .line 470
    :goto_0
    return-void

    .line 468
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-virtual {v0, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->setEditMode(Z)V

    goto :goto_0
.end method
