.class Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;
.super Ljava/lang/Object;
.source "BaseContainer.java"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/recent/BaseContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private initialPos:F

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollDistance(FF)F

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->initialPos:F

    .line 203
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mFingerOnDisplay:Z

    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 10
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const v6, 0x7fffffff

    const/high16 v5, -0x80000000

    .line 181
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollVelocity(FF)F

    move-result v9

    .line 187
    .local v9, "scrollVelocity":F
    const v0, 0x453b8000    # 3000.0f

    cmpl-float v0, v9, v0

    if-lez v0, :cond_1

    .line 188
    const v9, 0x453b8000    # 3000.0f

    .line 193
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    const/4 v3, 0x0

    float-to-int v4, v9

    move v7, v5

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 197
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mover:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 198
    const/4 v0, 0x1

    return v0

    .line 189
    :cond_1
    const v0, -0x3ac48000    # -3000.0f

    cmpg-float v0, v9, v0

    if-gez v0, :cond_0

    .line 190
    const v9, -0x3ac48000    # -3000.0f

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 177
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v1, 0x1

    .line 161
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {v2, p3, p4}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollDistance(FF)F

    move-result v0

    .line 163
    .local v0, "scrollDistance":F
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-boolean v2, v2, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrolling:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollDistance(FF)F

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->initialPos:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mPagingTouchSlop:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->access$100(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 167
    const/4 v1, 0x0

    .line 173
    :goto_0
    return v1

    .line 169
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iput-boolean v1, v2, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mScrolling:Z

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    neg-float v3, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(F)Z

    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 157
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 149
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getChildAtPosition(Landroid/view/MotionEvent;)Landroid/view/View;

    move-result-object v0

    .line 150
    .local v0, "v":Landroid/view/View;
    if-nez v0, :cond_0

    .line 151
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$1;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->access$000(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;->dismiss()V

    .line 153
    :cond_0
    const/4 v1, 0x0

    return v1
.end method
