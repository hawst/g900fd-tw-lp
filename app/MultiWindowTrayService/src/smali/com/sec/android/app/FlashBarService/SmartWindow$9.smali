.class Lcom/sec/android/app/FlashBarService/SmartWindow$9;
.super Ljava/lang/Object;
.source "SmartWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mScrollDownBound:I

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V
    .locals 1

    .prologue
    .line 854
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 855
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->mScrollDownBound:I

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v7, -0x1

    .line 858
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->setDragWindow(Z)V
    invoke-static {v4, v10}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2900(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)V

    .line 859
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 861
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 937
    :cond_0
    :goto_0
    return v10

    .line 863
    :pswitch_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/widget/ScrollView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a0051

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x3

    sub-int/2addr v4, v5

    iput v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->mScrollDownBound:I

    goto :goto_0

    .line 868
    :pswitch_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragEnterChildCount:I
    invoke-static {v4, v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3102(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 869
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z
    invoke-static {v4, v10}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2102(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z

    goto :goto_0

    .line 873
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v4

    float-to-int v3, v4

    .line 875
    .local v3, "touchY":I
    iget v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->mScrollDownBound:I

    if-le v3, v4, :cond_1

    .line 876
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowScrollView:Landroid/widget/ScrollView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3000(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/widget/ScrollView;

    move-result-object v4

    const/16 v5, 0x14

    invoke-virtual {v4, v8, v5}, Landroid/widget/ScrollView;->scrollBy(II)V

    .line 879
    :cond_1
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2200(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v4

    if-ne v4, v7, :cond_0

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 880
    const/4 v2, -0x1

    .line 882
    .local v2, "position":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 883
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragEnterChildCount:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3100(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 888
    :goto_1
    if-eq v2, v7, :cond_0

    .line 889
    const/4 v1, 0x0

    .line 890
    .local v1, "item_v":Landroid/view/View;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 891
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v5

    const/4 v6, 0x2

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z
    invoke-static {v4, v2, v5, v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z

    .line 892
    if-nez v2, :cond_4

    .line 893
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 897
    :goto_2
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v4, v2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2202(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 899
    :cond_2
    if-eqz v1, :cond_0

    .line 900
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 901
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 885
    .end local v1    # "item_v":Landroid/view/View;
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 895
    .restart local v1    # "item_v":Landroid/view/View;
    :cond_4
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartWindowGridView:Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$3200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    goto :goto_2

    .line 907
    .end local v1    # "item_v":Landroid/view/View;
    .end local v2    # "position":I
    .end local v3    # "touchY":I
    :pswitch_3
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->setDragWindow(Z)V
    invoke-static {v4, v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2900(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)V

    goto/16 :goto_0

    .line 910
    :pswitch_4
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 911
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v4, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 912
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v4, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 913
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2200(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v4

    if-eq v4, v7, :cond_0

    .line 914
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2200(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v6

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z
    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z

    .line 915
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v4, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2202(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    goto/16 :goto_0

    .line 920
    :pswitch_5
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z
    invoke-static {v4, v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2102(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z

    .line 921
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 922
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mIvt:[B
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1800(Lcom/sec/android/app/FlashBarService/SmartWindow;)[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 923
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2200(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v6

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->addAndChangelistItem(II)Z
    invoke-static {v4, v5, v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2800(Lcom/sec/android/app/FlashBarService/SmartWindow;II)Z

    .line 924
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v4, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2202(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 925
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v4, v7}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setItemIndexFromAppList(I)V

    .line 926
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowRelayout()V

    .line 927
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->updateSmartEditChanged()V

    .line 928
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 930
    :cond_5
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v9}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 931
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$9;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v4, v4, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    invoke-virtual {v4, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 861
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
