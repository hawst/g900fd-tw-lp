.class Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;
.super Landroid/widget/BaseAdapter;
.source "TryMultiWindowBezel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GridAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mTryMWBezelAppListIcons:[I

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 466
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 442
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->mTryMWBezelAppListIcons:[I

    .line 467
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->mContext:Landroid/content/Context;

    .line 468
    return-void

    .line 442
    nop

    :array_0
    .array-data 4
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
        0x7f02008b
        0x7f02008c
        0x7f02008d
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
        0x7f02008b
        0x7f02008c
        0x7f02008d
        0x7f020086
        0x7f020087
        0x7f020088
        0x7f020089
        0x7f02008a
    .end array-data
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->mTryMWBezelAppListIcons:[I

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->mTryMWBezelAppListIcons:[I

    rem-int/lit8 v1, p1, 0x5

    aget v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 482
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 488
    if-nez p2, :cond_2

    .line 489
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 490
    .local v0, "imageView":Landroid/widget/ImageView;
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelAppIconSize:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    # getter for: Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelAppIconSize:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 491
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 492
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 497
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->mTryMWBezelAppListIcons:[I

    rem-int/lit8 v2, p1, 0x5

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 499
    if-nez p1, :cond_0

    .line 500
    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {v1, v2, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 501
    const-string v1, "100"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 504
    :cond_0
    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    .line 505
    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;->this$0:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    invoke-direct {v1, v2, v4}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 506
    const-string v1, "101"

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 508
    :cond_1
    return-object v0

    .end local v0    # "imageView":Landroid/widget/ImageView;
    :cond_2
    move-object v0, p2

    .line 494
    check-cast v0, Landroid/widget/ImageView;

    .restart local v0    # "imageView":Landroid/widget/ImageView;
    goto :goto_0
.end method
