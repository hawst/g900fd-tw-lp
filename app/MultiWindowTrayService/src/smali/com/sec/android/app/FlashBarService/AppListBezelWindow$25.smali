.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeTraybarHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 1519
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 1522
    const/4 v1, 0x4

    if-ne p2, v1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1525
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideTraybarHelpPopup()V

    .line 1526
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->openBezelUI()V

    .line 1527
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1528
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$25;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mHelpPopupWindowTraybar:Landroid/widget/PopupWindow;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$4900(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/PopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1532
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
