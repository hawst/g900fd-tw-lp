.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;
.super Ljava/lang/Object;
.source "CenterBarWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;->makeButtonPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 1779
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1781
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isRecentsWindowShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1782
    invoke-static {}, Landroid/view/inputmethod/InputMethodManager;->peekInstance()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    .line 1783
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 1784
    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->forceHideSoftInput()Z

    .line 1786
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "do_not_show_help_popup_drag_and_drop"

    const/4 v3, 0x0

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    move-result v1

    if-nez v1, :cond_2

    .line 1787
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showDragAndDropHelpDialog()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4100(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 1793
    .end local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_1
    :goto_0
    return-void

    .line 1789
    .restart local v0    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawDragAndDrop()V

    .line 1790
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startDragAndDrop()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    goto :goto_0
.end method
