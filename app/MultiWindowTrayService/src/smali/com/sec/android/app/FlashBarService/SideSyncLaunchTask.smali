.class Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;
.super Ljava/lang/Object;
.source "MultiWindowTrayService.java"


# static fields
.field static final SAFE_DEBUG:Z

.field public static mNumberOfRunningThread:I

.field private static final sStaticLock:Ljava/lang/Object;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mLaunchedDisplayId:I

.field private mStartActivity:Z

.field private mStartIntent:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1690
    invoke-static {}, Landroid/os/Debug;->isProductShip()I

    move-result v2

    if-ne v2, v0, :cond_0

    move v0, v1

    :cond_0
    sput-boolean v0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    .line 1697
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->sStaticLock:Ljava/lang/Object;

    .line 1699
    sput v1, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "displayId"    # I

    .prologue
    .line 1742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1743
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    .line 1744
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mStartIntent:Landroid/content/Intent;

    .line 1745
    iput p2, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mLaunchedDisplayId:I

    .line 1746
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mStartActivity:Z

    .line 1747
    return-void
.end method

.method private getSideSyncState()I
    .locals 3

    .prologue
    .line 1760
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "sidesync_source_connect"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private isKmsConnected()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1750
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "KMS_SERVICE_CONNECTED"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1751
    .local v0, "kmsState":I
    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private isPssConnected()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1755
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "PSS_SERVICE_CONNECTED"

    invoke-static {v3, v4, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 1756
    .local v0, "pssState":I
    if-ne v0, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private requestPssStart()V
    .locals 4

    .prologue
    .line 1764
    new-instance v0, Landroid/content/Intent;

    const-string v1, "sidesync.app.action.SET_PSSMODE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1765
    .local v0, "setPssModeIntent":Landroid/content/Intent;
    const-string v1, "sidesync.app.extra.PSSMODE"

    const-string v2, "On"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1766
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1767
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "SideSyncLaunchTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SideSyncLaunchTask requestPssStart. intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1768
    :cond_0
    return-void
.end method

.method private requestSidesyncResume()V
    .locals 4

    .prologue
    .line 1771
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.sidesync.source.READY_MULTIDISPLAY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1772
    .local v0, "multiDisplayIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1773
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "SideSyncLaunchTask"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "requestSidesyncResume intent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1774
    :cond_0
    return-void
.end method


# virtual methods
.method public startingLaunch()Z
    .locals 13

    .prologue
    const/4 v8, 0x1

    .line 1777
    sget v9, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    add-int/lit8 v9, v9, 0x1

    sput v9, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    .line 1778
    sget-object v9, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->sStaticLock:Ljava/lang/Object;

    monitor-enter v9

    .line 1779
    const/4 v2, 0x0

    .line 1780
    .local v2, "count":I
    :try_start_0
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->getSideSyncState()I

    move-result v7

    .line 1781
    .local v7, "sideSyncState":I
    const/4 v1, 0x0

    .line 1782
    .local v1, "connectionCheckDuration":I
    const/4 v0, 0x0

    .line 1783
    .local v0, "connectionCheckCount":I
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->isKmsConnected()Z

    move-result v5

    .line 1784
    .local v5, "kmsState":Z
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->isPssConnected()Z

    move-result v6

    .line 1788
    .local v6, "pssState":Z
    const/4 v10, 0x2

    if-ne v7, v10, :cond_5

    .line 1790
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->requestSidesyncResume()V

    .line 1791
    const/16 v1, 0x96

    .line 1792
    const/16 v0, 0x28

    .line 1801
    :cond_0
    :goto_0
    const/4 v2, 0x0

    .line 1802
    const/4 v4, 0x0

    .local v4, "externalDisplayId":I
    move v3, v2

    .line 1803
    .end local v2    # "count":I
    .local v3, "count":I
    :goto_1
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "count":I
    .restart local v2    # "count":I
    if-ge v3, v0, :cond_1

    .line 1804
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->getSideSyncState()I

    move-result v7

    .line 1805
    if-ne v7, v8, :cond_6

    .line 1806
    const/4 v4, 0x0

    .line 1807
    if-lez v4, :cond_6

    .line 1808
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v10, :cond_1

    const-string v10, "SideSyncLaunchTask"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "run External Display is connected displayId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1819
    :cond_1
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v10, :cond_2

    .line 1820
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->getSideSyncState()I

    move-result v7

    .line 1821
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->isKmsConnected()Z

    move-result v5

    .line 1822
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->isPssConnected()Z

    move-result v6

    .line 1823
    const-string v10, "SideSyncLaunchTask"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "run After timeout or respond sideSyncState="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " kmsState="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " pssState="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " externalDisplayId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1826
    :cond_2
    if-ne v7, v8, :cond_3

    if-gtz v4, :cond_7

    .line 1828
    :cond_3
    sget-boolean v8, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v8, :cond_4

    const-string v8, "SideSyncLaunchTask"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "fail to connect sidesync sideSyncState="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " DisplayId="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mLaunchedDisplayId:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " mStartIntent="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mStartIntent:Landroid/content/Intent;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1830
    :cond_4
    sget v8, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    add-int/lit8 v8, v8, -0x1

    sput v8, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    .line 1831
    const/4 v8, 0x0

    monitor-exit v9

    .line 1843
    :goto_2
    return v8

    .line 1793
    .end local v4    # "externalDisplayId":I
    :cond_5
    if-eqz v5, :cond_0

    if-nez v6, :cond_0

    .line 1795
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->requestPssStart()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1796
    const/16 v1, 0x12c

    .line 1797
    const/16 v0, 0x28

    goto/16 :goto_0

    .line 1814
    .restart local v4    # "externalDisplayId":I
    :cond_6
    int-to-long v10, v1

    :try_start_1
    invoke-static {v10, v11}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v3, v2

    .line 1816
    .end local v2    # "count":I
    .restart local v3    # "count":I
    goto/16 :goto_1

    .line 1815
    .end local v3    # "count":I
    .restart local v2    # "count":I
    :catch_0
    move-exception v10

    move v3, v2

    .line 1816
    .end local v2    # "count":I
    .restart local v3    # "count":I
    goto/16 :goto_1

    .line 1832
    .end local v3    # "count":I
    .restart local v2    # "count":I
    :cond_7
    :try_start_2
    iget-boolean v10, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mStartActivity:Z

    if-eqz v10, :cond_a

    .line 1834
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v10, :cond_8

    const-string v10, "SideSyncLaunchTask"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "start activity sidesync mStartIntent="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mStartIntent:Landroid/content/Intent;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1835
    :cond_8
    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mContext:Landroid/content/Context;

    iget-object v11, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mStartIntent:Landroid/content/Intent;

    sget-object v12, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v10, v11, v12}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1841
    :cond_9
    :goto_3
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1842
    sget v9, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    add-int/lit8 v9, v9, -0x1

    sput v9, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mNumberOfRunningThread:I

    goto :goto_2

    .line 1838
    :cond_a
    :try_start_3
    sget-boolean v10, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->SAFE_DEBUG:Z

    if-eqz v10, :cond_9

    const-string v10, "SideSyncLaunchTask"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "relaunch top application to mLaunchedDisplayId="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/sec/android/app/FlashBarService/SideSyncLaunchTask;->mLaunchedDisplayId:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 1841
    .end local v0    # "connectionCheckCount":I
    .end local v1    # "connectionCheckDuration":I
    .end local v4    # "externalDisplayId":I
    .end local v5    # "kmsState":Z
    .end local v6    # "pssState":Z
    .end local v7    # "sideSyncState":I
    :catchall_0
    move-exception v8

    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v8
.end method
