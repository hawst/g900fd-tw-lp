.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;
.super Landroid/os/Handler;
.source "LegacyAppListWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 2303
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x5

    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 2306
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_1

    .line 2307
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mScrollDirection:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$7900(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2318
    :goto_0
    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8100()I

    move-result v0

    if-le v0, v3, :cond_0

    .line 2319
    # -= operator for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8120(I)I

    .line 2321
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mDragHandler:Landroid/os/Handler;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8100()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2323
    :cond_1
    return-void

    .line 2309
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, -0x6

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_0

    .line 2312
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$32;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->access$8000(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v2, v1}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_0

    .line 2307
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
