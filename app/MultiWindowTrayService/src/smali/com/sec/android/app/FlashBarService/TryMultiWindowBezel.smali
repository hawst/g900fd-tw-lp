.class public Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
.super Landroid/app/Activity;
.source "TryMultiWindowBezel.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$AppIconLongClick;,
        Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;
    }
.end annotation


# static fields
.field private static mTryMWViewSplitControlY:F


# instance fields
.field private DEBUG:Z

.field private FRAME_LOCATION_DOWN:I

.field private FRAME_LOCATION_UP:I

.field private TAG:Ljava/lang/String;

.field private app1DragArea:Z

.field private app2DragArea:Z

.field private isApp1Drawn:Z

.field private isApp1DrawnSuccess:Z

.field private isApp2Drawn:Z

.field private isFirstTipShown:Z

.field private isSecondTipShown:Z

.field private mAdjustControlMarginLastState:I

.field private mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

.field private mAppTag1:Ljava/lang/String;

.field private mBroderThickness:I

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mCenterBarGuideSize:I

.field private mCenterBarSize:I

.field private mCloseBtnAnimation:Landroid/view/animation/Animation;

.field mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mDisplayHeight:I

.field private mDisplayWidth:I

.field private mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

.field private mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mFinishRunnable:Ljava/lang/Runnable;

.field private mFrameLocation:I

.field private mFullScreenBtnAnimation:Landroid/view/animation/Animation;

.field private mHandle:Landroid/os/Handler;

.field private mHelpBubble5MarginX:I

.field private mHelpBubble5MarginY:I

.field private mHelpCueMargin:I

.field private mInvalidToast:Landroid/widget/Toast;

.field mIsMoved:Z

.field private mOrientation:I

.field private mResource:Landroid/content/res/Resources;

.field private mSplitControlAdjustMargin:I

.field private mSplitControlImageOffset:I

.field private mSplitControlLandAdjustMargin:I

.field private mSplitControlLeftTopMargin:I

.field private mSplitControlPopupMargin:I

.field private mSplitControlPortAdjustMargin:I

.field private mSplitControlRightBottomMargin:I

.field private mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

.field private mTrayBarDisabledFunctions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTrayBarEnabledFunctions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

.field private mTryMWBezelAppIconSize:I

.field private mTryMWBezelGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;

.field private mTryMWBezelGridView:Landroid/widget/GridView;

.field private mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;

.field private mTryMWBezelHelpPopupHolder2:Landroid/widget/RelativeLayout;

.field private mTryMWBezelHelpPopupHolder3:Landroid/widget/RelativeLayout;

.field private mTryMWBezelHelpPopupHolder4:Landroid/widget/RelativeLayout;

.field private mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

.field private mTryMWCloseBtn:Landroid/widget/ImageView;

.field private mTryMWDragAndDropBtn:Landroid/widget/ImageView;

.field private mTryMWFullScreenBtn:Landroid/widget/ImageView;

.field private mTryMWGuideCueAnimTip2:Landroid/widget/ImageView;

.field private mTryMWGuideCueAnimTip3:Landroid/widget/ImageView;

.field private mTryMWGuideCueAnimTip4:Landroid/widget/ImageView;

.field private mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

.field private mTryMWSplitControlSubButtonMargin:I

.field private mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

.field private mTryMWTrayWidth:I

.field private mTryMWViewApp2:Landroid/widget/ImageView;

.field private mTryMWViewAppViewer:Landroid/widget/LinearLayout;

.field private mTryMWViewContainer:Landroid/widget/RelativeLayout;

.field private mTryMWViewContainerWidthCenter:I

.field private mTryMWViewEdit:Landroid/widget/ImageView;

.field private mTryMWViewFocusFrame:Landroid/widget/ImageView;

.field private mTryMWViewFocusFrame2:Landroid/widget/ImageView;

.field private mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

.field private mTryMWViewHelpPopup1:Landroid/widget/RelativeLayout;

.field private mTryMWViewHelpPopup2:Landroid/widget/RelativeLayout;

.field private mTryMWViewHelpPopup3:Landroid/widget/RelativeLayout;

.field private mTryMWViewHelpPopup4:Landroid/widget/RelativeLayout;

.field private mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

.field private mTryMWViewHelpPopup6:Landroid/widget/RelativeLayout;

.field private mTryMWViewHome:Landroid/widget/ImageView;

.field private mTryMWViewSplitControl:Landroid/widget/ImageView;

.field private mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

.field protected mWindowManager:Landroid/view/WindowManager;

.field private openDrawerRunnable:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlY:F

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 49
    const-string v0, "TryMultiWindowBezel"

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->TAG:Ljava/lang/String;

    .line 50
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->DEBUG:Z

    .line 51
    const-string v0, "100"

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppTag1:Ljava/lang/String;

    .line 52
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    .line 63
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isFirstTipShown:Z

    .line 64
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z

    .line 66
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1DrawnSuccess:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp2Drawn:Z

    .line 68
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app1DragArea:Z

    .line 69
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app2DragArea:Z

    .line 71
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mIsMoved:Z

    .line 78
    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    .line 81
    iput v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I

    .line 82
    iput v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I

    .line 101
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    .line 102
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup1:Landroid/widget/RelativeLayout;

    .line 105
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup2:Landroid/widget/RelativeLayout;

    .line 106
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup3:Landroid/widget/RelativeLayout;

    .line 107
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup4:Landroid/widget/RelativeLayout;

    .line 108
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    .line 109
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup6:Landroid/widget/RelativeLayout;

    .line 111
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;

    .line 112
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder2:Landroid/widget/RelativeLayout;

    .line 113
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder3:Landroid/widget/RelativeLayout;

    .line 114
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder4:Landroid/widget/RelativeLayout;

    .line 115
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    .line 117
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 119
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;

    .line 120
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    .line 121
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    .line 122
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    .line 123
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    .line 124
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    .line 125
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    .line 126
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    .line 127
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip2:Landroid/widget/ImageView;

    .line 128
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip3:Landroid/widget/ImageView;

    .line 129
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip4:Landroid/widget/ImageView;

    .line 130
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    .line 132
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

    .line 133
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    .line 134
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    .line 135
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    .line 136
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    .line 138
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;

    .line 139
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridView:Landroid/widget/GridView;

    .line 143
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mInvalidToast:Landroid/widget/Toast;

    .line 146
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 148
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHandle:Landroid/os/Handler;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    .line 151
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    .line 1117
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$3;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFinishRunnable:Ljava/lang/Runnable;

    .line 1124
    new-instance v0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$4;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->openDrawerRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isSecondTipShown:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isFirstTipShown:Z

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isFirstTipShown:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app1DragArea:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app1DragArea:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1DrawnSuccess:Z

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1DrawnSuccess:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayWidth:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWTrayWidth:I

    return v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app2DragArea:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->app2DragArea:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    return v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    return p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I

    return v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I

    return v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->openDrawerRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHandle:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp2Drawn:Z

    return p1
.end method

.method static synthetic access$2900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    return v0
.end method

.method static synthetic access$3400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3502(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    return p1
.end method

.method static synthetic access$3600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBroderThickness:I

    return v0
.end method

.method static synthetic access$3700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup4:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder4:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z

    return v0
.end method

.method static synthetic access$4000(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Z

    .prologue
    .line 47
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->isApp1Drawn:Z

    return p1
.end method

.method static synthetic access$4100(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Ljava/util/List;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;
    .param p1, "x1"    # Ljava/util/List;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->checkFunctionState(Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4200(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->startAnimationFor4SubButton()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->startAnimationFor5SubButton()V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup6:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFinishRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup3:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder2:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelAppIconSize:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppTag1:Ljava/lang/String;

    return-object v0
.end method

.method private checkFunctionState(Ljava/util/List;Ljava/lang/String;)Z
    .locals 1
    .param p2, "disableFunction"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 912
    .local p1, "disableFunctionList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 913
    const/4 v0, 0x0

    .line 916
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getSizeOfStatusBar()I
    .locals 3

    .prologue
    .line 1111
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1112
    .local v0, "rect":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 1113
    .local v1, "window":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1114
    iget v2, v0, Landroid/graphics/Rect;->top:I

    return v2
.end method

.method private makeScreenForLandscape()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 409
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 410
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 412
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 414
    .local v3, "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 415
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 416
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 418
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 420
    .local v0, "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 421
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 422
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 424
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 426
    .local v2, "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getSizeOfStatusBar()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 427
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 428
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 430
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 432
    .local v1, "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayHeight:I

    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getSizeOfStatusBar()I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 433
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 434
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 436
    return-void
.end method

.method private makeScreenForPortrait()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 378
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    invoke-virtual {v4, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 379
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 380
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 382
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 384
    .local v3, "mTryMWViewHomeParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 385
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, v3, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 386
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 388
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 390
    .local v0, "mTryMWViewApp2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 391
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 392
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 394
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 396
    .local v2, "mTryMWViewFocusFrameParams":Landroid/widget/LinearLayout$LayoutParams;
    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainerWidthCenter:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 397
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 398
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 400
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 402
    .local v1, "mTryMWViewFocusFrame2Params":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 403
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCurrentDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v4, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 404
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 406
    return-void
.end method

.method private removeListener()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 920
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 921
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 922
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 923
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 925
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$2;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 949
    return-void
.end method

.method private setDrawerLayoutListener()V
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setScrimColor(I)V

    .line 314
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    new-instance v1, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;

    invoke-direct {v1, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$1;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 348
    return-void
.end method

.method private setSubSplitBtn(FF)V
    .locals 7
    .param p1, "X"    # F
    .param p2, "Y"    # F

    .prologue
    const/4 v6, 0x2

    const/high16 v5, 0x40000000    # 2.0f

    .line 893
    const/4 v2, 0x5

    new-array v1, v2, [Landroid/widget/ImageView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    aput-object v2, v1, v6

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    aput-object v3, v1, v2

    .line 898
    .local v1, "tmp":[Landroid/widget/ImageView;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 899
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v2, v6, :cond_0

    .line 900
    aget-object v2, v1, v0

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setX(F)V

    .line 902
    aget-object v2, v1, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSplitControlSubButtonMargin:I

    int-to-float v3, v3

    sub-float v3, p2, v3

    aget-object v4, v1, v0

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 898
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 905
    :cond_0
    aget-object v2, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    sub-float v3, p1, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 906
    aget-object v2, v1, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSplitControlSubButtonMargin:I

    int-to-float v3, v3

    sub-float v3, p2, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    goto :goto_1

    .line 909
    :cond_1
    return-void
.end method

.method private startAnimationFor4SubButton()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 952
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 954
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 955
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 956
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 957
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 958
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 960
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v0, v2, :cond_1

    .line 961
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I

    if-ne v0, v1, :cond_0

    .line 964
    const v0, 0x7f040018

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 966
    const v0, 0x7f040014

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 968
    const v0, 0x7f040010

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 970
    const v0, 0x7f04000c

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 974
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I

    if-ne v0, v1, :cond_1

    .line 977
    const v0, 0x7f04001b

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 979
    const v0, 0x7f040017

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 981
    const v0, 0x7f040013

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 983
    const v0, 0x7f04000f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 986
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 987
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I

    if-ne v0, v1, :cond_2

    .line 990
    const v0, 0x7f04001a

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 992
    const v0, 0x7f040016

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 994
    const v0, 0x7f040012

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 996
    const v0, 0x7f04000e

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 1000
    :cond_2
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I

    if-ne v0, v1, :cond_3

    .line 1003
    const v0, 0x7f040019

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 1005
    const v0, 0x7f040015

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 1007
    const v0, 0x7f040011

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 1009
    const v0, 0x7f04000d

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 1015
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1016
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1017
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1018
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1021
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1022
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1023
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1024
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1025
    return-void
.end method

.method private startAnimationFor5SubButton()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1028
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1030
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1031
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1032
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1033
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1034
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1036
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v0, v2, :cond_1

    .line 1037
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I

    if-ne v0, v1, :cond_0

    .line 1038
    const/high16 v0, 0x7f040000

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

    .line 1040
    const v0, 0x7f040020

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 1042
    const v0, 0x7f04001c

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 1044
    const v0, 0x7f040008

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 1046
    const v0, 0x7f040004

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 1050
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I

    if-ne v0, v1, :cond_1

    .line 1051
    const v0, 0x7f040003

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

    .line 1053
    const v0, 0x7f040023

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 1055
    const v0, 0x7f04001f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 1057
    const v0, 0x7f04000b

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 1059
    const v0, 0x7f040007

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 1062
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1063
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_DOWN:I

    if-ne v0, v1, :cond_2

    .line 1064
    const v0, 0x7f040002

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

    .line 1066
    const v0, 0x7f040022

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 1068
    const v0, 0x7f04001e

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 1070
    const v0, 0x7f04000a

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 1072
    const v0, 0x7f040006

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 1076
    :cond_2
    iget v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFrameLocation:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->FRAME_LOCATION_UP:I

    if-ne v0, v1, :cond_3

    .line 1077
    const v0, 0x7f040001

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

    .line 1079
    const v0, 0x7f040021

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    .line 1081
    const v0, 0x7f04001d

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    .line 1083
    const v0, 0x7f040009

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    .line 1085
    const v0, 0x7f040005

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    .line 1090
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1091
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1092
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1093
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1094
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1096
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAppSwitchingBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1097
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSwitchWindowBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1098
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mFullScreenBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1099
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDragAndDropBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1100
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCloseBtnAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1101
    return-void
.end method


# virtual methods
.method public init()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 169
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    .line 187
    const/4 v6, 0x0

    .line 188
    .local v6, "traybarFunction":Ljava/lang/String;
    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->DEBUG:Z

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CenterBarWindow() CscFeatureTagFramework.TAG_CSCFEATURE_FRAMEWORK_CONFIGMULTIWINDOWTRAYBARFUNCTION="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_0
    if-eqz v6, :cond_5

    .line 190
    new-instance v7, Ljava/util/ArrayList;

    const-string v8, "\\,"

    invoke-virtual {v6, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 191
    .local v7, "traybarFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 192
    .local v3, "function":Ljava/lang/String;
    const-string v8, "+"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 193
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 194
    :cond_2
    const-string v8, "-"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 195
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 198
    .end local v3    # "function":Ljava/lang/String;
    :cond_3
    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->DEBUG:Z

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CenterBarWindow() mTrayBarEnabledFunctions="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_4
    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->DEBUG:Z

    if-eqz v8, :cond_5

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CenterBarWindow() mTrayBarDisabledFunctions="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "traybarFunctions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const/high16 v9, 0x7f080000

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 203
    .local v0, "centerbarButtonType":Ljava/lang/String;
    if-eqz v0, :cond_a

    .line 204
    new-instance v1, Ljava/util/ArrayList;

    const-string v8, "\\,"

    invoke-virtual {v0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 205
    .local v1, "centerbarButtonTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 206
    .restart local v3    # "function":Ljava/lang/String;
    const-string v8, "+"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 207
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 208
    :cond_7
    const-string v8, "-"

    invoke-virtual {v3, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 209
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 212
    .end local v3    # "function":Ljava/lang/String;
    :cond_8
    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->DEBUG:Z

    if-eqz v8, :cond_9

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CenterBarWindow() mTrayBarEnabledFunctions="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarEnabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_9
    iget-boolean v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->DEBUG:Z

    if-eqz v8, :cond_a

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "CenterBarWindow() mTrayBarDisabledFunctions="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTrayBarDisabledFunctions:Ljava/util/ArrayList;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    .end local v1    # "centerbarButtonTypes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_a
    new-instance v8, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;

    invoke-direct {v8, p0, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;Landroid/content/Context;)V

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;

    .line 217
    const v8, 0x7f0f010c

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/GridView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridView:Landroid/widget/GridView;

    .line 218
    const v8, 0x7f0f010a

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/support/v4/widget/DrawerLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 219
    const v8, 0x7f0f0102

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewEdit:Landroid/widget/ImageView;

    .line 220
    const v8, 0x7f0f00fb

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    .line 221
    const v8, 0x7f0f00fe

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewApp2:Landroid/widget/ImageView;

    .line 222
    const v8, 0x7f0f0100

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    .line 223
    const v8, 0x7f0f0101

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame2:Landroid/widget/ImageView;

    .line 224
    const v8, 0x7f0f00fd

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    .line 226
    const v8, 0x7f0f010d

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder1:Landroid/widget/RelativeLayout;

    .line 227
    const v8, 0x7f0f010f

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder2:Landroid/widget/RelativeLayout;

    .line 228
    const v8, 0x7f0f0112

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder3:Landroid/widget/RelativeLayout;

    .line 229
    const v8, 0x7f0f0115

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder4:Landroid/widget/RelativeLayout;

    .line 230
    const v8, 0x7f0f0118

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    .line 232
    const v8, 0x7f0f010e

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup1:Landroid/widget/RelativeLayout;

    .line 233
    const v8, 0x7f0f0110

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup2:Landroid/widget/RelativeLayout;

    .line 234
    const v8, 0x7f0f0113

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup3:Landroid/widget/RelativeLayout;

    .line 235
    const v8, 0x7f0f0116

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup4:Landroid/widget/RelativeLayout;

    .line 236
    const v8, 0x7f0f0119

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    .line 237
    const v8, 0x7f0f011b

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup6:Landroid/widget/RelativeLayout;

    .line 239
    const v8, 0x7f0f00fc

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewAppViewer:Landroid/widget/LinearLayout;

    .line 240
    const v8, 0x7f0f00ff

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrameLinearLayout:Landroid/widget/LinearLayout;

    .line 241
    const v8, 0x7f0f0103

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    .line 242
    const v8, 0x7f0f0104

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    .line 244
    const v8, 0x7f0f0111

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip2:Landroid/widget/ImageView;

    .line 245
    const v8, 0x7f0f0114

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip3:Landroid/widget/ImageView;

    .line 246
    const v8, 0x7f0f0117

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip4:Landroid/widget/ImageView;

    .line 247
    const v8, 0x7f0f011a

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    .line 249
    const v8, 0x7f0f0105

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWAppSwitchingBtn:Landroid/widget/ImageView;

    .line 250
    const v8, 0x7f0f0106

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSwitchWindowBtn:Landroid/widget/ImageView;

    .line 251
    const v8, 0x7f0f0107

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWFullScreenBtn:Landroid/widget/ImageView;

    .line 252
    const v8, 0x7f0f0108

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWDragAndDropBtn:Landroid/widget/ImageView;

    .line 253
    const v8, 0x7f0f0109

    invoke-virtual {p0, v8}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWCloseBtn:Landroid/widget/ImageView;

    .line 255
    const v8, 0x7f040052

    invoke-static {p0, v8}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v5

    .line 256
    .local v5, "mFadeInAnimation":Landroid/view/animation/Animation;
    const v8, 0x7f040024

    invoke-static {p0, v8}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 258
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip2:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 259
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip3:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 260
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip4:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 261
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    invoke-virtual {v8, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 263
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0149

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWTrayWidth:I

    .line 264
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0001

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    .line 265
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0002

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarGuideSize:I

    .line 266
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0158

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelAppIconSize:I

    .line 267
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0003

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    .line 268
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0159

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAdjustControlMarginLastState:I

    .line 270
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0151

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginX:I

    .line 272
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0150

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginY:I

    .line 274
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0152

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    .line 278
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a0157

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWSplitControlSubButtonMargin:I

    .line 281
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v8, p0}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 283
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    new-instance v9, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;

    invoke-direct {v9, p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$TryMWViewDragListener;-><init>(Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;)V

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 284
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v8, p0}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 286
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 287
    .local v2, "displaySize":Landroid/graphics/Point;
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    if-eqz v8, :cond_b

    .line 288
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 290
    :cond_b
    iget v8, v2, Landroid/graphics/Point;->x:I

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayWidth:I

    .line 291
    iget v8, v2, Landroid/graphics/Point;->y:I

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayHeight:I

    .line 292
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    .line 294
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x10501b5

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBroderThickness:I

    .line 295
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00ac

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    .line 297
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00ad

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    .line 299
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a014e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLandAdjustMargin:I

    .line 301
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a014f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPortAdjustMargin:I

    .line 303
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0a00ae

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v8

    float-to-int v8, v8

    iput v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    .line 305
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup1:Landroid/widget/RelativeLayout;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 307
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/support/v4/widget/DrawerLayout;->setFocusableInTouchMode(Z)V

    .line 308
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setDrawerLayoutListener()V

    .line 309
    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridView:Landroid/widget/GridView;

    iget-object v9, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelGridAdapter:Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel$GridAdapter;

    invoke-virtual {v8, v9}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 310
    return-void
.end method

.method public invalidTouch()V
    .locals 2

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mInvalidToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 1105
    const v0, 0x7f08003a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mInvalidToast:Landroid/widget/Toast;

    .line 1107
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mInvalidToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1108
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 688
    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setResult(I)V

    .line 689
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->finish()V

    .line 690
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 357
    invoke-super {p0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 358
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder5:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_2

    .line 363
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 364
    .local v0, "displaySize":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 365
    iget v1, v0, Landroid/graphics/Point;->x:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayWidth:I

    .line 366
    iget v1, v0, Landroid/graphics/Point;->y:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mDisplayHeight:I

    .line 368
    .end local v0    # "displaySize":Landroid/graphics/Point;
    :cond_2
    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    iput v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    .line 369
    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 370
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->makeScreenForPortrait()V

    .line 372
    :cond_3
    iget v1, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 373
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->makeScreenForLandscape()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 158
    .local v1, "w":Landroid/view/Window;
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 159
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 160
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 162
    const v2, 0x7f03002b

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setContentView(I)V

    .line 164
    const-string v2, "window"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mWindowManager:Landroid/view/WindowManager;

    .line 165
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->init()V

    .line 166
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 353
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v5, 0x7

    const/4 v9, 0x6

    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 695
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v0, v2

    .line 696
    .local v0, "X":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v1, v2

    .line 698
    .local v1, "Y":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    .line 888
    :cond_0
    :goto_0
    :pswitch_0
    return v7

    .line 700
    :pswitch_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 701
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->invalidTouch()V

    goto :goto_0

    .line 703
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWBezelHelpPopupHolder4:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    .line 707
    :pswitch_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mIsMoved:Z

    if-eqz v2, :cond_0

    .line 708
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mIsMoved:Z

    .line 710
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarGuideSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 711
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 712
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarGuideSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 713
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 714
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 716
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v2, v7, :cond_3

    .line 717
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setRequestedOrientation(I)V

    .line 718
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    const v3, 0x7f02006a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 721
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    sub-int/2addr v2, v3

    if-le v0, v2, :cond_5

    .line 722
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 723
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    .line 729
    :cond_2
    :goto_1
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    if-ge v1, v2, :cond_6

    .line 730
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAdjustControlMarginLastState:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 731
    int-to-float v2, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    int-to-float v3, v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setSubSplitBtn(FF)V

    .line 732
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPortAdjustMargin:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v5, v6

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 735
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPortAdjustMargin:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v5, v6

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 739
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginY:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 741
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginX:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 743
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAdjustControlMarginLastState:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 745
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 747
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->removeListener()V

    .line 783
    :cond_3
    :goto_2
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v2, v8, :cond_0

    .line 784
    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setRequestedOrientation(I)V

    .line 785
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    const v3, 0x7f02006a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 788
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_8

    .line 789
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 790
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    .line 796
    :cond_4
    :goto_3
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    if-ge v0, v2, :cond_9

    .line 797
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 799
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    int-to-float v2, v2

    int-to-float v3, v1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setSubSplitBtn(FF)V

    .line 801
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLandAdjustMargin:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 804
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLandAdjustMargin:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 808
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getY()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginY:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 810
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 812
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getY()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 814
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLeftTopMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 816
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->removeListener()V

    goto/16 :goto_0

    .line 724
    :cond_5
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    if-ge v0, v2, :cond_2

    .line 725
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 726
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    goto/16 :goto_1

    .line 749
    :cond_6
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v2, v3

    if-le v1, v2, :cond_7

    .line 751
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAdjustControlMarginLastState:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 753
    int-to-float v2, v0

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setSubSplitBtn(FF)V

    .line 754
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPortAdjustMargin:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v5, v6

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 759
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPortAdjustMargin:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v5, v6

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 765
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginY:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 767
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginX:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 769
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mAdjustControlMarginLastState:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 771
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getX()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 773
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->removeListener()V

    goto/16 :goto_2

    .line 776
    :cond_7
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-direct {v3, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 778
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-direct {v3, v4, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    .line 791
    :cond_8
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    if-ge v1, v2, :cond_4

    .line 792
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 793
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlPopupMargin:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    add-int v1, v2, v3

    goto/16 :goto_3

    .line 818
    :cond_9
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v2, v3

    if-le v0, v2, :cond_a

    .line 820
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 823
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    int-to-float v3, v1

    invoke-direct {p0, v2, v3}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setSubSplitBtn(FF)V

    .line 825
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLandAdjustMargin:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 829
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlLandAdjustMargin:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlAdjustMargin:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 834
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getY()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginY:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 836
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHelpPopup5:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpBubble5MarginX:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 838
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getY()F

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlImageOffset:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 840
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWGuideCueAnimTip5:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mSplitControlRightBottomMargin:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mHelpCueMargin:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 842
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->removeListener()V

    goto/16 :goto_0

    .line 845
    :cond_a
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v0, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 847
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewFocusFrame:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewHome:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v3, v0, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 859
    :pswitch_3
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 860
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v2, v7, :cond_b

    .line 861
    invoke-virtual {p0, v5}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setRequestedOrientation(I)V

    .line 862
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const v3, 0x7f02006b

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 865
    :cond_b
    iget v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mOrientation:I

    if-ne v2, v8, :cond_c

    .line 866
    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->setRequestedOrientation(I)V

    .line 867
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    const v3, 0x7f02006d

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 870
    :cond_c
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 871
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarGuideSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 872
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setY(F)V

    .line 873
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlPressed:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarGuideSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 874
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mCenterBarSize:I

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setX(F)V

    .line 876
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControl:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getY()F

    move-result v2

    sput v2, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mTryMWViewSplitControlY:F

    .line 878
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mIsMoved:Z

    goto/16 :goto_0

    .line 882
    :pswitch_4
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/TryMultiWindowBezel;->mIsMoved:Z

    goto/16 :goto_0

    .line 698
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
