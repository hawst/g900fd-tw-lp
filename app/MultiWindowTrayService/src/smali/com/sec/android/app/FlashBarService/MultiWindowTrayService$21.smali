.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;
.super Ljava/lang/Object;
.source "MultiWindowTrayService.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeGuidelineWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 1081
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1900(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1085
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$2100(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$2000(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1086
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowGuideline:Landroid/view/Window;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$2002(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Landroid/view/Window;)Landroid/view/Window;

    .line 1087
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$21;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowGuideline:Z
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1902(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z

    .line 1088
    const-string v0, "MultiWindowTrayService"

    const-string v1, "Success : mWindowGuideline, removeViewImmediate after attached"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    :cond_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1092
    return-void
.end method
