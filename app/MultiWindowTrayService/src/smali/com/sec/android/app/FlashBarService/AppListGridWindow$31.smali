.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createConfirmDialog(IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3043
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 3046
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDelIndex:I
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbFromEdit:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->removeTemplate(IZ)V

    .line 3047
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbFromEdit:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3048
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListEditWindow:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->updateEditListChanged()V

    .line 3052
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->dismissConfirmDialog()V

    .line 3053
    return-void

    .line 3050
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$31;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    goto :goto_0
.end method
