.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3420
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v10, 0x68

    const/16 v9, 0x67

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 3422
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mResouces:Landroid/content/res/Resources;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a0019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v2, v6

    .line 3423
    .local v2, "moveBtnHeight":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 3509
    :cond_0
    :goto_0
    return v4

    .line 3426
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->cancelCollapseTimer()V
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3427
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPressMoveBtn:Z
    invoke-static {v6, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3428
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setImageToMoveButton(IZ)V
    invoke-static {v5, v6, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZ)V

    .line 3429
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->isRecentsWindowShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 3430
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removeRecentsWindow()V

    goto :goto_0

    .line 3434
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsAvailableMove:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3435
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v7

    float-to-int v7, v7

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8702(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3436
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v7

    float-to-int v7, v7

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$302(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3437
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v1

    .line 3438
    .local v1, "left":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    sub-int v3, v6, v7

    .line 3440
    .local v3, "right":I
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    packed-switch v6, :pswitch_data_1

    .line 3451
    :cond_1
    :goto_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 3452
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-eq v6, v9, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-ne v6, v10, :cond_3

    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setAppListHandlePosition(I)V
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V

    .line 3454
    :goto_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v7, v5, v8, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZZ)V

    :goto_3
    move v4, v5

    .line 3472
    goto/16 :goto_0

    .line 3442
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-lt v6, v2, :cond_1

    .line 3443
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v6, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    goto :goto_1

    .line 3446
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    sub-int/2addr v7, v2

    if-gt v6, v7, :cond_1

    .line 3447
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v6, v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    goto :goto_1

    .line 3453
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setAppListHandlePosition(I)V
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V

    goto :goto_2

    .line 3456
    :cond_4
    if-ge v1, v3, :cond_6

    .line 3457
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-ne v6, v10, :cond_5

    .line 3458
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I
    invoke-static {v6, v9}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3459
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v9, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3460
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v9, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3462
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v9, v4, v7, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZZ)V

    goto :goto_3

    .line 3464
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-ne v6, v9, :cond_7

    .line 3465
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mBeforeAppListPosition:I
    invoke-static {v6, v10}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8902(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)I

    .line 3466
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListHorizontal:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v10, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3467
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListVertical:Ljava/util/List;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$9100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mbEditmode:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    invoke-virtual {v6, v7, v8, v10, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForScrollView(Ljava/util/List;ZIZ)V

    .line 3469
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZZ)V
    invoke-static {v6, v10, v4, v7, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZZ)V

    goto/16 :goto_3

    .line 3475
    .end local v1    # "left":I
    .end local v3    # "right":I
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsPressMoveBtn:Z
    invoke-static {v6, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8402(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3476
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setImageToMoveButton(IZ)V
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZ)V

    .line 3477
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMoveBtn:Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$6200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/ImageView;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->playSoundEffect(I)V

    .line 3478
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsAvailableMove:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v6

    if-ne v6, v5, :cond_a

    .line 3479
    const/4 v0, 0x0

    .line 3480
    .local v0, "hide":Z
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsAvailableMove:Z
    invoke-static {v6, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8602(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 3481
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    packed-switch v6, :pswitch_data_2

    .line 3491
    :cond_8
    :goto_4
    if-eqz v0, :cond_9

    .line 3492
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V
    invoke-static {v6, v7, v5, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V

    .line 3496
    :goto_5
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIvt:[B
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$4300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)[B

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mVibrator:Landroid/os/SystemVibrator;

    invoke-virtual {v7}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 3497
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->startCollapseTimer()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    .line 3505
    .end local v0    # "hide":Z
    :goto_6
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    invoke-virtual {v5, v6, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->setGlowEffect(IZ)V

    goto/16 :goto_0

    .line 3483
    .restart local v0    # "hide":Z
    :pswitch_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    if-gt v6, v2, :cond_8

    .line 3484
    const/4 v0, 0x1

    goto :goto_4

    .line 3487
    :pswitch_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionX:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$8700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mDisplayWidth:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$3500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    sub-int/2addr v7, v2

    if-lt v6, v7, :cond_8

    .line 3488
    const/4 v0, 0x1

    goto :goto_4

    .line 3494
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V
    invoke-static {v6, v7, v5, v8}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V

    goto :goto_5

    .line 3499
    .end local v0    # "hide":Z
    :cond_a
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 3500
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->closeFlashBar()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$2500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_6

    .line 3502
    :cond_b
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$42;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->openFlashBar()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$7500(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_6

    .line 3423
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_4
    .end packed-switch

    .line 3440
    :pswitch_data_1
    .packed-switch 0x67
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 3481
    :pswitch_data_2
    .packed-switch 0x67
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
