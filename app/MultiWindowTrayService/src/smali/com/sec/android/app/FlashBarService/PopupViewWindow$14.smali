.class Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mLastX:I

.field private mLastY:I

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 877
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 879
    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastX:I

    .line 880
    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastY:I

    return-void
.end method

.method private move(II)V
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 924
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 925
    .local v0, "lp":Landroid/view/WindowManager$LayoutParams;
    iput p1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 926
    iput p2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 927
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 928
    return-void
.end method

.method private reviseWindowPosition(Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "windowRect"    # Landroid/graphics/Rect;
    .param p2, "maxSize"    # Landroid/graphics/Point;

    .prologue
    .line 954
    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    div-int/lit8 v0, v2, 0x3

    .line 955
    .local v0, "boundaryX":I
    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iget v3, p1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    div-int/lit8 v1, v2, 0x3

    .line 957
    .local v1, "boundaryY":I
    iget v2, p1, Landroid/graphics/Rect;->left:I

    neg-int v3, v0

    if-ge v2, v3, :cond_2

    .line 958
    neg-int v2, v0

    iget v3, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 964
    :cond_0
    :goto_0
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget v3, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v1

    if-le v2, v3, :cond_3

    .line 965
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v1

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 970
    :cond_1
    :goto_1
    return-void

    .line 960
    :cond_2
    iget v2, p1, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Point;->x:I

    add-int/2addr v3, v0

    if-le v2, v3, :cond_0

    .line 961
    iget v2, p2, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    sub-int/2addr v2, v3

    iget v3, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    goto :goto_0

    .line 967
    :cond_3
    iget v2, p1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 968
    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    goto :goto_1
.end method

.method private setCurrenWindowRect(Landroid/graphics/Rect;)V
    .locals 3
    .param p1, "smartWindowRect"    # Landroid/graphics/Rect;

    .prologue
    .line 944
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 945
    .local v0, "windowAttribute":Landroid/view/WindowManager$LayoutParams;
    iget v1, p1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 946
    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 948
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 949
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/WindowManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 950
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    .line 884
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$1100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 920
    :goto_0
    return v1

    .line 888
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 890
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastX:I

    .line 891
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastY:I

    .line 920
    :cond_1
    :goto_1
    const/4 v1, 0x1

    goto :goto_0

    .line 895
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    move-result-object v1

    if-nez v1, :cond_2

    .line 896
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    new-instance v2, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;-><init>(Landroid/view/View;)V

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2102(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    .line 898
    :cond_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    float-to-int v2, v2

    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastX:I

    sub-int/2addr v2, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v3

    float-to-int v3, v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastY:I

    sub-int/2addr v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    .line 899
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v4

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->show(IIII)V

    .line 900
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastX:I

    .line 901
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->mLastY:I

    goto/16 :goto_1

    .line 906
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->move(II)V

    .line 907
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 908
    .local v0, "maxSize":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 909
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->reviseWindowPosition(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    .line 910
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->setCurrenWindowRect(Landroid/graphics/Rect;)V

    .line 911
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 912
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;->dismiss()V

    .line 913
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    const/4 v2, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->access$2102(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    goto/16 :goto_1

    .line 888
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
