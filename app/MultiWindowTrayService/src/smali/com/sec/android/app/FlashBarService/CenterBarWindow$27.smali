.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;
.super Landroid/os/Handler;
.source "CenterBarWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V
    .locals 0

    .prologue
    .line 2978
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2980
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 3003
    :goto_0
    :pswitch_0
    return-void

    .line 2982
    :pswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->hideButtonsPopupWindow(Z)V

    goto :goto_0

    .line 2986
    :pswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$2200(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeRecentsWindow()V

    goto :goto_0

    .line 2990
    :pswitch_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->showButtonPopupWindow()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$4000(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    .line 2991
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->startHideButtonTimer()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$1300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)V

    goto :goto_0

    .line 2995
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/MotionEvent;

    .line 2996
    .local v0, "event":Landroid/view/MotionEvent;
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    goto :goto_0

    .line 3000
    .end local v0    # "event":Landroid/view/MotionEvent;
    :pswitch_5
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$27;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterBarPoint:Landroid/graphics/Point;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$300(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)Landroid/graphics/Point;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->drawCenterBar(II)V

    goto :goto_0

    .line 2980
    nop

    :pswitch_data_0
    .packed-switch 0xc9
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
