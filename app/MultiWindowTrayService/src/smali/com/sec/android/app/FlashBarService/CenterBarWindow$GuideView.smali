.class Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;
.super Landroid/view/View;
.source "CenterBarWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/CenterBarWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GuideView"
.end annotation


# instance fields
.field private mContainer:Landroid/widget/PopupWindow;

.field private mShowing:Z

.field private mView:Landroid/widget/ImageView;

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/CenterBarWindow;Landroid/content/Context;Landroid/view/View;I)V
    .locals 5
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "view"    # Landroid/view/View;
    .param p4, "resourceId"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 762
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->this$0:Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    .line 763
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 764
    new-instance v1, Landroid/widget/PopupWindow;

    invoke-direct {v1, p2}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    .line 765
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setLayoutInScreenEnabled(Z)V

    .line 766
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setClippingEnabled(Z)V

    .line 767
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setLayoutInsetDecor(Z)V

    .line 768
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, p3}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    .line 769
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    const/16 v2, 0x7d2

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWindowLayoutType(I)V

    .line 770
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setShowForAllUsers(Z)V

    .line 771
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 772
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mShowing:Z

    .line 773
    const v1, 0x7f0f008a

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    .line 774
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    invoke-virtual {v1, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 777
    sget-boolean v1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->GUI_TRAYBAR_UI:Z

    if-nez v1, :cond_0

    .line 778
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarCueTintColor:I
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$700(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v2

    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundTintList(Landroid/content/res/ColorStateList;)V

    .line 779
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 780
    const v1, 0x7f0f008b

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 781
    .local v0, "pointView":Landroid/widget/ImageView;
    # getter for: Lcom/sec/android/app/FlashBarService/CenterBarWindow;->mCenterbarTintColor:I
    invoke-static {p1}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->access$800(Lcom/sec/android/app/FlashBarService/CenterBarWindow;)I

    move-result v1

    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 782
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageTintMode(Landroid/graphics/PorterDuff$Mode;)V

    .line 785
    .end local v0    # "pointView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    .prologue
    .line 802
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 803
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 804
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 805
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mShowing:Z

    .line 806
    return-void
.end method

.method public setDrawable(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 809
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 810
    return-void
.end method

.method public show(Landroid/graphics/Rect;)V
    .locals 6
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 788
    iget-boolean v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mShowing:Z

    if-nez v1, :cond_0

    .line 789
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    .line 790
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setHeight(I)V

    .line 791
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    const/4 v1, 0x0

    check-cast v1, Landroid/os/Binder;

    const/4 v3, 0x0

    iget v4, p1, Landroid/graphics/Rect;->left:I

    iget v5, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/os/IBinder;III)V

    .line 792
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 793
    .local v0, "alphaAnimation":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 794
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 795
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mShowing:Z

    .line 799
    .end local v0    # "alphaAnimation":Landroid/view/animation/Animation;
    :goto_0
    return-void

    .line 797
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/CenterBarWindow$GuideView;->mContainer:Landroid/widget/PopupWindow;

    iget v2, p1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    goto :goto_0
.end method
