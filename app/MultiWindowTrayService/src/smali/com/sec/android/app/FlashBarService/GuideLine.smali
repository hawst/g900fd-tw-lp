.class public Lcom/sec/android/app/FlashBarService/GuideLine;
.super Ljava/lang/Object;
.source "GuideLine.java"


# instance fields
.field private final DOCKING_AREA_INVERSE_RATIO:F

.field private final HIDE_TIMER:J

.field private final MSG_HIDE:I

.field mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

.field private mContext:Landroid/content/Context;

.field mDisplayHeight:I

.field mDisplayOrientation:I

.field mDisplayWidth:I

.field mDocking:Z

.field mFeatureMultiwindowRecentUI:Z

.field mFloatingMinimumSizeRatio:F

.field mFullGuideRect:Z

.field mGuideLineDragListener:Landroid/view/View$OnDragListener;

.field mGuideView:Landroid/view/View;

.field mHandler:Landroid/os/Handler;

.field mIntent:Landroid/content/Intent;

.field private mIvt:[B

.field mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field mRecentPenWindowBody:Landroid/widget/ImageView;

.field mRecentPenWindowHeader:Landroid/widget/ImageView;

.field mStatusBarHeight:I

.field mSupportMultiWindow:Z

.field mTaskId:I

.field mVibrator:Landroid/os/SystemVibrator;

.field mWindowGuideLine:Landroid/view/Window;

.field mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const v0, 0x3d8f5c29    # 0.07f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->DOCKING_AREA_INVERSE_RATIO:F

    .line 42
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->MSG_HIDE:I

    .line 43
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->HIDE_TIMER:J

    .line 45
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    .line 46
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFeatureMultiwindowRecentUI:Z

    .line 47
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFullGuideRect:Z

    .line 48
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDocking:Z

    .line 49
    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mSupportMultiWindow:Z

    .line 54
    iput v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mStatusBarHeight:I

    .line 55
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mTaskId:I

    .line 56
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    .line 57
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideView:Landroid/view/View;

    .line 58
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    .line 59
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    .line 60
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    .line 61
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowManager:Landroid/view/WindowManager;

    .line 66
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIntent:Landroid/content/Intent;

    .line 68
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIvt:[B

    .line 138
    new-instance v0, Lcom/sec/android/app/FlashBarService/GuideLine$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/GuideLine$1;-><init>(Lcom/sec/android/app/FlashBarService/GuideLine;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideLineDragListener:Landroid/view/View$OnDragListener;

    .line 321
    new-instance v0, Lcom/sec/android/app/FlashBarService/GuideLine$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/GuideLine$2;-><init>(Lcom/sec/android/app/FlashBarService/GuideLine;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mHandler:Landroid/os/Handler;

    .line 76
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    .line 77
    return-void

    .line 68
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/GuideLine;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/GuideLine;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIvt:[B

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/GuideLine;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/GuideLine;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public hide()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    if-nez v1, :cond_0

    .line 136
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 128
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 129
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 130
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 131
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 132
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 133
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 135
    const-string v1, "GuideLine"

    const-string v2, "hide"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/AppListController;)V
    .locals 2
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "appListController"    # Lcom/sec/android/app/FlashBarService/AppListController;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    .line 81
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    .line 82
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowManager:Landroid/view/WindowManager;

    .line 83
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    const-string v1, "multiwindow_facade"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 84
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mVibrator:Landroid/os/SystemVibrator;

    .line 85
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1050010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mStatusBarHeight:I

    .line 86
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e00b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFloatingMinimumSizeRatio:F

    .line 87
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.multiwindow.recentui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mFeatureMultiwindowRecentUI:Z

    .line 89
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    const v1, 0x7f0f008d

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideView:Landroid/view/View;

    .line 90
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideView:Landroid/view/View;

    const v1, 0x7f0f00dd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    .line 91
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowHeader:Landroid/widget/ImageView;

    const v1, 0x108063c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 92
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideView:Landroid/view/View;

    const v1, 0x7f0f00dc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    .line 93
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mRecentPenWindowBody:Landroid/widget/ImageView;

    const v1, 0x1080674

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 94
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideView:Landroid/view/View;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mGuideLineDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 95
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 96
    return-void
.end method

.method public show(Landroid/content/Intent;IZ)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "taskId"    # I
    .param p3, "bSupportMultiWindow"    # Z

    .prologue
    const/4 v5, 0x0

    .line 99
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    if-nez v3, :cond_0

    .line 121
    :goto_0
    return-void

    .line 102
    :cond_0
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mIntent:Landroid/content/Intent;

    .line 103
    iput p2, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mTaskId:I

    .line 104
    iput-boolean p3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mSupportMultiWindow:Z

    .line 105
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 106
    .local v1, "displayMetrix":Landroid/util/DisplayMetrics;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 107
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 108
    iget v3, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    .line 109
    iget v3, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    .line 110
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    iput v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayOrientation:I

    .line 111
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 112
    .local v2, "l":Landroid/view/WindowManager$LayoutParams;
    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 113
    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 114
    iget v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayWidth:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 115
    iget v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mDisplayHeight:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 116
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 117
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mWindowGuideLine:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 119
    const-string v3, "GuideLine"

    const-string v4, "show"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/GuideLine;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x1

    const-wide/16 v6, 0x7d0

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
