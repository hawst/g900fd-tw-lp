.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;
.super Landroid/os/Handler;
.source "AppListBezelWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 2348
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/16 v3, 0xc

    const/4 v4, 0x3

    const/16 v2, -0xc

    const/4 v1, 0x0

    .line 2351
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_0

    .line 2352
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mScrollDirection:I
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7400(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2369
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDragHandler:Landroid/os/Handler;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->SCROLL_MOVE_DELAY:I
    invoke-static {}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7700()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2371
    :cond_0
    return-void

    .line 2354
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    invoke-virtual {v0, v3, v1}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    goto :goto_0

    .line 2357
    :pswitch_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarHorizontal:Landroid/widget/HorizontalScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/HorizontalScrollView;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Landroid/widget/HorizontalScrollView;->scrollBy(II)V

    goto :goto_0

    .line 2360
    :pswitch_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_0

    .line 2363
    :pswitch_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$33;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mFlashBarVertical:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$7600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v1, v3}, Landroid/widget/ScrollView;->scrollBy(II)V

    goto :goto_0

    .line 2352
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
