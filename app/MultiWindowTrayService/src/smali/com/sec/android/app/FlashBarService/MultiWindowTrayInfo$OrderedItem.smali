.class public Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;
.super Ljava/lang/Object;
.source "MultiWindowTrayInfo.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OrderedItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;",
        ">;"
    }
.end annotation


# instance fields
.field mDuplicatedOrder:Z

.field mItem:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

.field mOrder:I


# direct methods
.method public constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;I)V
    .locals 0
    .param p1, "launchItem"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .param p2, "order"    # I

    .prologue
    .line 2309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mItem:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    .line 2311
    iput p2, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mOrder:I

    .line 2312
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;)I
    .locals 2
    .param p1, "info"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    .prologue
    .line 2323
    iget v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mOrder:I

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->getOrder()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mDuplicatedOrder:Z

    .line 2326
    :cond_0
    iget v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mOrder:I

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->getOrder()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2304
    check-cast p1, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->compareTo(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;)I

    move-result v0

    return v0
.end method

.method public getItem()Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    .locals 1

    .prologue
    .line 2315
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mItem:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    return-object v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 2319
    iget v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$OrderedItem;->mOrder:I

    return v0
.end method
