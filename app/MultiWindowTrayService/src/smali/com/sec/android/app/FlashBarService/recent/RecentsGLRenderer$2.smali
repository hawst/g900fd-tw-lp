.class Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;
.super Ljava/lang/Object;
.source "RecentsGLRenderer.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->onTaskDismissed(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

.field final synthetic val$t:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
    .locals 0

    .prologue
    .line 506
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->val$t:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 524
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 514
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$500(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 515
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$500(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->val$t:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 516
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 517
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->val$t:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->cleanUp()V

    .line 518
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->this$0:Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    # getter for: Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->access$600(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;->val$t:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    invoke-interface {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;->handleSwipe(Ljava/lang/Object;)V

    .line 519
    return-void

    .line 516
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 529
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animator"    # Landroid/animation/Animator;

    .prologue
    .line 510
    return-void
.end method
