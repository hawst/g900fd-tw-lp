.class public Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;
.super Lcom/sec/android/app/FlashBarService/recent/BaseContainer;
.source "RecentsVerticalScrollView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    .line 36
    invoke-direct {p0, p1, p2, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->density:F

    .line 39
    .local v0, "densityScale":F
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v2

    int-to-float v1, v2

    .line 41
    .local v1, "pagingTouchSlop":F
    new-instance v2, Lcom/sec/android/app/FlashBarService/SwipeHelper;

    invoke-direct {v2, v3, p0, v0, v1}, Lcom/sec/android/app/FlashBarService/SwipeHelper;-><init>(ILcom/sec/android/app/FlashBarService/SwipeHelper$Callback;FF)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mSwipeHelper:Lcom/sec/android/app/FlashBarService/SwipeHelper;

    .line 44
    sget v2, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mBottomStackVisiblePart:F

    const/high16 v3, 0x42700000    # 60.0f

    add-float/2addr v2, v3

    sput v2, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->BOTTOM_STACK_BORDER:F

    .line 45
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 251
    const/high16 v0, 0x43b40000    # 360.0f

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotX(F)V

    .line 252
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setPivotY(F)V

    .line 253
    invoke-super {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->addView(Landroid/view/View;)V

    .line 254
    return-void
.end method

.method protected createBottomStackInAnimation()Landroid/view/animation/Animation;
    .locals 3

    .prologue
    .line 223
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->bottomStackIndex:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 224
    .local v0, "v":Landroid/view/View;
    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->createBottomStackInAnimationInternal(Landroid/view/View;)Landroid/view/animation/Animation;

    move-result-object v1

    return-object v1
.end method

.method protected createBottomStackInAnimationInternal(Landroid/view/View;)Landroid/view/animation/Animation;
    .locals 12
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v2, 0x3f8ccccd    # 1.1f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 229
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v7, v3, 0x2

    .line 230
    .local v7, "pivotX":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    .line 231
    .local v8, "pivotY":I
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    int-to-float v5, v7

    int-to-float v6, v8

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 233
    .local v0, "s":Landroid/view/animation/ScaleAnimation;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v0, v2}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 234
    new-instance v10, Landroid/view/animation/TranslateAnimation;

    sget v2, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->BOTTOM_STACK_BORDER:F

    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mBottomStackVisiblePart:F

    sub-float/2addr v2, v3

    invoke-direct {v10, v11, v11, v11, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 235
    .local v10, "t":Landroid/view/animation/TranslateAnimation;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v10, v2}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 237
    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v2, 0x0

    invoke-direct {v9, v2}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 238
    .local v9, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v9, v10}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 239
    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 240
    sget v2, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->STACK_ANIMATION_DURATION:I

    int-to-long v2, v2

    invoke-virtual {v9, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 241
    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 242
    iput v11, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->rotated_angle:F

    .line 243
    int-to-float v2, v7

    invoke-virtual {p1, v2}, Landroid/view/View;->setPivotX(F)V

    .line 244
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    .line 245
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    .line 246
    return-object v9
.end method

.method protected createBottomStackOutAnimation()Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const v1, 0x3f8ccccd    # 1.1f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 162
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->bottomStackIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 163
    .local v9, "v":Landroid/view/View;
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v5, v3, v4

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 164
    .local v0, "s":Landroid/view/animation/ScaleAnimation;
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 166
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->bottomStackIndex:I

    if-nez v1, :cond_0

    .line 167
    new-instance v8, Landroid/view/animation/TranslateAnimation;

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->BOTTOM_STACK_BORDER:F

    sget v2, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mBottomStackVisiblePart:F

    sub-float/2addr v1, v2

    invoke-direct {v8, v11, v11, v1, v11}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 186
    .local v8, "t":Landroid/view/animation/TranslateAnimation;
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v8, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 188
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/4 v1, 0x0

    invoke-direct {v7, v1}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 189
    .local v7, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v7, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 190
    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 191
    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->STACK_ANIMATION_DURATION:I

    int-to-long v2, v1

    invoke-virtual {v7, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 192
    const/4 v1, 0x1

    invoke-virtual {v7, v1}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 193
    return-object v7

    .line 169
    .end local v7    # "set":Landroid/view/animation/AnimationSet;
    .end local v8    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->bottomStackIndex:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->getScrollPosition(Landroid/view/View;)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mBottomStackReleaseDistance:F

    sub-float/2addr v1, v2

    float-to-int v10, v1

    .line 171
    .local v10, "ydiff":I
    new-instance v8, Landroid/view/animation/TranslateAnimation;

    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->BOTTOM_STACK_BORDER:F

    sget v2, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mBottomStackVisiblePart:F

    sub-float/2addr v1, v2

    int-to-float v2, v10

    add-float/2addr v1, v2

    invoke-direct {v8, v11, v11, v1, v11}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 172
    .restart local v8    # "t":Landroid/view/animation/TranslateAnimation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView$2;

    invoke-direct {v1, p0, v9, v10}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView$2;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;Landroid/view/View;I)V

    invoke-virtual {v8, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method protected createTopStackInAnimation()Landroid/view/animation/Animation;
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 198
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 199
    .local v9, "v":Landroid/view/View;
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->bottomStackIndex:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->Z_DIFF:F

    mul-float/2addr v3, v4

    sub-float v1, v10, v3

    .line 200
    .local v1, "scaleFrom":F
    const v3, 0x3f733333    # 0.95f

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    sget v5, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->MAX_STACK_APPS:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    sget v5, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->TOP_STACK_Z_DIFF:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 201
    .local v2, "scaleTo":F
    const-string v3, "MIK"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "In: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v5, v3, v11

    move v3, v1

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 203
    .local v0, "s":Landroid/view/animation/ScaleAnimation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mAccelerateInterpolator:Landroid/view/animation/AccelerateInterpolator;

    invoke-virtual {v0, v3}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 204
    new-instance v8, Landroid/view/animation/TranslateAnimation;

    const/high16 v3, -0x3ee00000    # -10.0f

    invoke-direct {v8, v6, v6, v6, v3}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 205
    .local v8, "t":Landroid/view/animation/TranslateAnimation;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {v8, v3}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 207
    new-instance v7, Landroid/view/animation/AnimationSet;

    const/4 v3, 0x0

    invoke-direct {v7, v3}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 208
    .local v7, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v7, v8}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 209
    invoke-virtual {v7, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 210
    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->STACK_ANIMATION_DURATION:I

    int-to-long v4, v3

    invoke-virtual {v7, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 211
    const/4 v3, 0x1

    invoke-virtual {v7, v3}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 212
    iput v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->rotated_angle:F

    .line 214
    invoke-virtual {p0, v9}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->stopAnimators(Landroid/view/View;)V

    .line 215
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v11

    invoke-virtual {v9, v3}, Landroid/view/View;->setPivotX(F)V

    .line 216
    invoke-virtual {v9, v10}, Landroid/view/View;->setScaleX(F)V

    .line 217
    invoke-virtual {v9, v10}, Landroid/view/View;->setScaleY(F)V

    .line 218
    return-object v7
.end method

.method protected createTopStackOutAnimation()Landroid/view/animation/Animation;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 122
    const v1, 0x3f733333    # 0.95f

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    sget v4, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->MAX_STACK_APPS:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    sget v4, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->TOP_STACK_Z_DIFF:F

    mul-float/2addr v3, v4

    add-float v7, v1, v3

    .line 123
    .local v7, "scaleFrom":F
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/View;

    .line 124
    .local v12, "v":Landroid/view/View;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->bottomStackIndex:I

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    sget v3, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->Z_DIFF:F

    mul-float/2addr v1, v3

    sub-float v8, v2, v1

    .line 127
    .local v8, "scaleTo":F
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    div-float v1, v7, v8

    div-float v3, v7, v8

    invoke-virtual {v12}, Landroid/view/View;->getWidth()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float v5, v4, v5

    move v4, v2

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    .line 129
    .local v0, "s":Landroid/view/animation/ScaleAnimation;
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 130
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    const/high16 v1, -0x3ee00000    # -10.0f

    invoke-direct {v11, v6, v6, v1, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 152
    .local v11, "t":Landroid/view/animation/TranslateAnimation;
    :goto_0
    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v13}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 153
    .local v10, "set":Landroid/view/animation/AnimationSet;
    invoke-virtual {v10, v11}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 154
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 155
    invoke-virtual {v10, v13}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 156
    sget v1, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->STACK_ANIMATION_DURATION:I

    int-to-long v2, v1

    invoke-virtual {v10, v2, v3}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    .line 157
    return-object v10

    .line 132
    .end local v10    # "set":Landroid/view/animation/AnimationSet;
    .end local v11    # "t":Landroid/view/animation/TranslateAnimation;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->views:Ljava/util/ArrayList;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->topStackIndex:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->getScrollPosition(Landroid/view/View;)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->mTopStackReleaseDistance:F

    sub-float/2addr v1, v2

    float-to-int v9, v1

    .line 133
    .local v9, "scroll_diff":I
    new-instance v11, Landroid/view/animation/TranslateAnimation;

    rsub-int/lit8 v1, v9, -0xa

    int-to-float v1, v1

    invoke-direct {v11, v6, v6, v1, v6}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 134
    .restart local v11    # "t":Landroid/view/animation/TranslateAnimation;
    new-instance v1, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView$1;

    invoke-direct {v1, p0, v12, v9, v8}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView$1;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;Landroid/view/View;IF)V

    invoke-virtual {v11, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    goto :goto_0
.end method

.method protected getScrollDistance(FF)F
    .locals 0
    .param p1, "distanceX"    # F
    .param p2, "distanceY"    # F

    .prologue
    .line 49
    return p2
.end method

.method protected getScrollPosition(Landroid/view/View;)F
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 81
    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v0

    return v0
.end method

.method protected getScrollSize()I
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsVerticalScrollView;->getMeasuredHeight()I

    move-result v0

    return v0
.end method

.method protected getScrollVelocity(FF)F
    .locals 0
    .param p1, "velocityX"    # F
    .param p2, "velocityY"    # F

    .prologue
    .line 64
    return p2
.end method

.method protected optHide(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 259
    return-void
.end method

.method protected optShow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 264
    return-void
.end method

.method protected setRotation(Landroid/view/View;F)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "angle"    # F

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v7

    .line 70
    .local v7, "curr":Landroid/view/animation/Animation;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 73
    :cond_0
    new-instance v0, Lcom/sec/android/app/FlashBarService/recent/Rotate3dAnimation;

    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    div-float v3, v1, v4

    invoke-virtual {p1}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    div-float v4, v1, v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v1, p2

    move v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/FlashBarService/recent/Rotate3dAnimation;-><init>(FFFFFZ)V

    .line 74
    .local v0, "a":Landroid/view/animation/Animation;
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 75
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 76
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method protected setScrollPosition(Landroid/view/View;F)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "pos"    # F

    .prologue
    .line 86
    invoke-virtual {p1, p2}, Landroid/view/View;->setY(F)V

    .line 87
    return-void
.end method
