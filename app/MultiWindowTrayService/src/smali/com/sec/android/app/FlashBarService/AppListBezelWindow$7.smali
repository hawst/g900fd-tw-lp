.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)V
    .locals 0

    .prologue
    .line 625
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 6
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I
    .param p5, "arg4"    # I
    .param p6, "arg5"    # I
    .param p7, "arg6"    # I
    .param p8, "arg7"    # I
    .param p9, "arg8"    # I

    .prologue
    const/4 v5, 0x0

    .line 630
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 631
    .local v0, "l":Landroid/view/WindowManager$LayoutParams;
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    if-ne v3, v4, :cond_0

    .line 632
    new-instance v1, Landroid/graphics/Region;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mBezelRecognitionArea:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1600(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v3

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mDisplayHeight:I
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1700(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)I

    move-result v4

    invoke-direct {v1, v5, v5, v3, v4}, Landroid/graphics/Region;-><init>(IIII)V

    .line 633
    .local v1, "region":Landroid/graphics/Region;
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$7;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mWindowAppList:Landroid/view/Window;
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->access$1500(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;)Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getViewRootImpl()Landroid/view/ViewRootImpl;

    move-result-object v2

    .line 634
    .local v2, "v":Landroid/view/ViewRootImpl;
    if-eqz v2, :cond_0

    .line 635
    invoke-virtual {v2, v1}, Landroid/view/ViewRootImpl;->setTransparentRegion(Landroid/graphics/Region;)V

    .line 639
    .end local v1    # "region":Landroid/graphics/Region;
    .end local v2    # "v":Landroid/view/ViewRootImpl;
    :cond_0
    return-void
.end method
