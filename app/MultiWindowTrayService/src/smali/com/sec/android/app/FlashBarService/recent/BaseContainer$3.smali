.class Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;
.super Ljava/lang/Object;
.source "BaseContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/recent/BaseContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field lastY:I

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;)V
    .locals 1

    .prologue
    .line 623
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 624
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 627
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    if-nez v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 629
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    .line 644
    :goto_0
    return-void

    .line 631
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    if-ne v1, v3, :cond_1

    .line 632
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v1

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    .line 634
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    .line 635
    .local v0, "currY":I
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    sub-int v2, v0, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(F)Z

    move-result v1

    if-nez v1, :cond_2

    .line 636
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->scroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 637
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    goto :goto_0

    .line 640
    :cond_2
    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->lastY:I

    .line 641
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->invalidate()V

    .line 642
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$3;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mover:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1

    invoke-virtual {v1, v2, v4, v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
