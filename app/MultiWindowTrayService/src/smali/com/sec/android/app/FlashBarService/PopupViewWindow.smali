.class public Lcom/sec/android/app/FlashBarService/PopupViewWindow;
.super Ljava/lang/Object;
.source "PopupViewWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;,
        Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;,
        Lcom/sec/android/app/FlashBarService/PopupViewWindow$AppListItemGridViewHolder;,
        Lcom/sec/android/app/FlashBarService/PopupViewWindow$GridViewAdapter;
    }
.end annotation


# instance fields
.field private final PEN_WINDOW_RATIO_MARGIN:F

.field private final POPUP_VIEW_PAGER_MOVE_LEFT:I

.field private final POPUP_VIEW_PAGER_MOVE_RIGHT:I

.field private mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

.field private mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

.field mClosePopupViewWindowListener:Landroid/view/View$OnClickListener;

.field private mContext:Landroid/content/Context;

.field private mDisplayHeight:I

.field private mDisplayRect:Landroid/graphics/Rect;

.field private mDisplayWidth:I

.field mEditSmartWindowListener:Landroid/view/View$OnClickListener;

.field private mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

.field private mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

.field private mHasEditWindow:Z

.field private mLastConfig:Landroid/content/res/Configuration;

.field private mLastPositionX:I

.field private mLastPositionY:I

.field private mMinHeight:I

.field private mMinWidth:I

.field private mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

.field private mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field private mPopUpViewWindow:Landroid/view/Window;

.field private mPopupViewEditButton:Landroid/widget/ImageView;

.field protected mPopupViewEditWindow:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

.field private mPopupViewPageCount:I

.field private mPopupViewPager:Landroid/support/v4/view/ViewPager;

.field private mPopupViewPagerAdapter:Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;

.field mPopupViewPagerHoverListener:Landroid/view/View$OnHoverListener;

.field private mPopupViewPagerLandHeight:I

.field private mPopupViewPagerPortHeight:I

.field private mPopupViewPagerSelectedPage:I

.field private mPopupViewPagerWrapper:Landroid/widget/RelativeLayout;

.field private mPopupViewTitle:Landroid/widget/ImageView;

.field private mPopupViewTitleButtons:Landroid/widget/RelativeLayout;

.field private mPopupViewTitleMargin:I

.field private mPopupViewWindowCloseButton:Landroid/widget/ImageView;

.field private mPopupViewWindowKeyListener:Landroid/view/View$OnKeyListener;

.field private mPopupViewWrapperHeight:I

.field private mPopupViewWrapperWidth:I

.field private mPopupWindowItemNum:I

.field private mResouces:Landroid/content/res/Resources;

.field private mShadowPaddingRect:Landroid/graphics/Rect;

.field private mStatusBarHeight:I

.field mTimerHandler:Landroid/os/Handler;

.field mTitleHoverListener:Landroid/view/View$OnHoverListener;

.field private mTitleTouchListener:Landroid/view/View$OnTouchListener;

.field mViewPagerIconClickListener:Landroid/view/View$OnClickListener;

.field mViewPagerIconTouchListener:Landroid/view/View$OnTouchListener;

.field private mViewPagerMark:Landroid/widget/LinearLayout;

.field private mViewPagerMarkerMarginLeft:I

.field private mViewPagerMarkerSize:I

.field private mViewPagerMoveLeft:Z

.field private mViewPagerMoveRight:Z

.field private mViewPagerScrollHoverMargin:I

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;Lcom/sec/android/app/FlashBarService/AppListController;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .param p3, "tayinfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .param p4, "applistController"    # Lcom/sec/android/app/FlashBarService/AppListController;

    .prologue
    const/4 v1, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/16 v0, 0xcb

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->POPUP_VIEW_PAGER_MOVE_LEFT:I

    .line 76
    const/16 v0, 0xcc

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->POPUP_VIEW_PAGER_MOVE_RIGHT:I

    .line 78
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->PEN_WINDOW_RATIO_MARGIN:F

    .line 88
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    .line 100
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditWindow:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z

    .line 103
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 104
    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 129
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWindowKeyListener:Landroid/view/View$OnKeyListener;

    .line 357
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerHoverListener:Landroid/view/View$OnHoverListener;

    .line 460
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mEditSmartWindowListener:Landroid/view/View$OnClickListener;

    .line 473
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mClosePopupViewWindowListener:Landroid/view/View$OnClickListener;

    .line 629
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$10;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$10;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTimerHandler:Landroid/os/Handler;

    .line 679
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$12;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$12;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerIconTouchListener:Landroid/view/View$OnTouchListener;

    .line 708
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$13;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerIconClickListener:Landroid/view/View$OnClickListener;

    .line 877
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$14;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTitleTouchListener:Landroid/view/View$OnTouchListener;

    .line 974
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewWindow$15;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$15;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTitleHoverListener:Landroid/view/View$OnHoverListener;

    .line 155
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    .line 156
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 157
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 158
    iput-object p4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    .line 160
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 161
    new-instance v0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-direct {v0, v1, p0, v2}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;-><init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/PopupViewWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditWindow:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    .line 163
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x1050010

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    .line 165
    invoke-static {}, Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;->getInstance()Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mApplicationInfos:Lcom/samsung/android/multiwindow/MultiWindowApplicationInfos;

    .line 166
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    const-string v1, "multiwindow_facade"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/multiwindow/MultiWindowFacade;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    .line 167
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a016c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    .line 168
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a016d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerMarginLeft:I

    .line 169
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f07000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I

    .line 170
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a016f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    .line 171
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a0170

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    .line 172
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a0171

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I

    .line 173
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a0173

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerPortHeight:I

    .line 174
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a0175

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerLandHeight:I

    .line 175
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    const v1, 0x7f0a016e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerScrollHoverMargin:I

    .line 177
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/Window;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerScrollHoverMargin:I

    return v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z

    return v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->applyShadePressEffect(Landroid/view/MotionEvent;Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/samsung/android/multiwindow/MultiWindowFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowFacade:Lcom/samsung/android/multiwindow/MultiWindowFacade;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I

    return v0
.end method

.method static synthetic access$1600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/content/res/Configuration;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    return-object v0
.end method

.method static synthetic access$1902(Lcom/sec/android/app/FlashBarService/PopupViewWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPageCount:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I

    return v0
.end method

.method static synthetic access$2000(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/View$OnKeyListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWindowKeyListener:Landroid/view/View$OnKeyListener;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;)Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .param p1, "x1"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mGuideWindowLayout:Lcom/sec/android/app/FlashBarService/PopupViewWindow$GuideView;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/graphics/Rect;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/view/WindowManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Lcom/sec/android/app/FlashBarService/AppListController;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    return v0
.end method

.method static synthetic access$402(Lcom/sec/android/app/FlashBarService/PopupViewWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    return p1
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleButtons:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/PopupViewWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z

    return p1
.end method

.method private applyShadePressEffect(Landroid/view/MotionEvent;Landroid/view/View;)V
    .locals 6
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 692
    const/4 v1, 0x0

    .line 693
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x0

    .line 694
    .local v2, "launchItem":Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 695
    .local v0, "count":I
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupWindowItemNum:I

    mul-int/2addr v4, v5

    add-int/2addr v4, v0

    const/16 v5, 0x67

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListItem(II)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;

    move-result-object v2

    .line 696
    if-eqz v2, :cond_0

    .line 697
    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$LaunchItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 699
    :cond_0
    if-eqz v1, :cond_2

    .line 700
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    .line 701
    :cond_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    .line 706
    :cond_2
    :goto_0
    invoke-virtual {p2}, Landroid/view/View;->invalidate()V

    .line 707
    return-void

    .line 703
    :cond_3
    new-instance v3, Landroid/graphics/PorterDuffColorFilter;

    const/high16 v4, 0x44000000    # 512.0f

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v3, v4, v5}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v1, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method private initViewPageMark()V
    .locals 10

    .prologue
    const/4 v9, -0x2

    const/high16 v8, 0x40000000    # 2.0f

    const v7, 0x3f99999a    # 1.2f

    .line 645
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 646
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPageCount:I

    if-ge v0, v4, :cond_1

    .line 647
    new-instance v1, Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 648
    .local v1, "iv":Landroid/widget/ImageView;
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 649
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 651
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-nez v0, :cond_0

    .line 652
    invoke-virtual {v1}, Landroid/widget/ImageView;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    .line 653
    .local v3, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v3, v7, v7}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 654
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    int-to-float v5, v5

    mul-float/2addr v5, v7

    sub-float/2addr v4, v5

    div-float/2addr v4, v8

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    int-to-float v5, v5

    iget v6, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    int-to-float v6, v6

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    div-float/2addr v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 656
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 662
    .end local v3    # "matrix":Landroid/graphics/Matrix;
    :goto_1
    const v4, 0x7f020130

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 663
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 664
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerSize:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 665
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 667
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 668
    new-instance v4, Lcom/sec/android/app/FlashBarService/PopupViewWindow$11;

    invoke-direct {v4, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$11;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658
    :cond_0
    const/16 v4, 0x66

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 659
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMarkerMarginLeft:I

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_1

    .line 676
    .end local v1    # "iv":Landroid/widget/ImageView;
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    .line 677
    return-void
.end method


# virtual methods
.method public closePopupViewWindow()V
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setAppListSizeChangeListener(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;)V

    .line 483
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->hidePopupViewEditWindow()V

    .line 484
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewWindow()V

    .line 485
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    .line 486
    return-void
.end method

.method public hidePopupViewEditWindow()V
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z

    .line 623
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->savePopupList()V

    .line 624
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditWindow:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->closeSmartEditWindow()V

    .line 625
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->removePopupViewEditWindow()V

    .line 626
    return-void
.end method

.method public makePopupViewPagerLand(Z)V
    .locals 4
    .param p1, "isRotated"    # Z

    .prologue
    .line 593
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerWrapper:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 594
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 595
    iget v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 596
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerWrapper:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 598
    if-nez p1, :cond_0

    .line 599
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    .line 601
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerAdapter:Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 602
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 604
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 605
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerLandHeight:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 606
    const/16 v2, 0x3c

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 607
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 608
    return-void
.end method

.method public makePopupViewPagerPort(Z)V
    .locals 4
    .param p1, "isRotated"    # Z

    .prologue
    .line 575
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerWrapper:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 576
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 577
    iget v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 578
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerWrapper:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 580
    if-nez p1, :cond_0

    .line 581
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    .line 583
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerAdapter:Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 584
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 586
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 587
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerPortHeight:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 588
    const/16 v2, 0x70

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 589
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, v0}, Landroid/support/v4/view/ViewPager;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 590
    return-void
.end method

.method public moveViewPagerLeft()V
    .locals 2

    .prologue
    .line 397
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveLeft:Z

    .line 398
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    if-nez v0, :cond_0

    .line 403
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public moveViewPagerRight()V
    .locals 2

    .prologue
    .line 406
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMoveRight:Z

    .line 407
    iget v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    iget v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 412
    :goto_0
    return-void

    .line 410
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerSelectedPage:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 489
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0, p1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 490
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 492
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->updatePopupViewWindowPosition(Z)V

    .line 493
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleButtons:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleButtons:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 500
    :cond_0
    return-void
.end method

.method public pkgManagerList(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->pkgManagerList(Landroid/content/Intent;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    :cond_0
    return-void
.end method

.method public setEditMode(Z)V
    .locals 3
    .param p1, "enableMode"    # Z

    .prologue
    .line 447
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z

    .line 448
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditWindow:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->setEditWindowVisibility(Z)V

    .line 449
    if-eqz p1, :cond_0

    .line 451
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditButton:Landroid/widget/ImageView;

    const v1, 0x7f020119

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 452
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080002

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 458
    :goto_0
    return-void

    .line 455
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditButton:Landroid/widget/ImageView;

    const v1, 0x7f020118

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 456
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    const v2, 0x7f080001

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setHoverIcon(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 416
    const/4 v1, -0x1

    :try_start_0
    invoke-static {p1, v1}, Landroid/view/PointerIcon;->setHoveringSpenIcon(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    :goto_0
    return-void

    .line 417
    :catch_0
    move-exception v0

    .line 418
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "PopupViewWindow"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to change PointerIcon to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setPopupViewWindow(Landroid/view/Window;Landroid/graphics/Rect;Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 12
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "displayRect"    # Landroid/graphics/Rect;
    .param p3, "service"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/high16 v6, 0x3f000000    # 0.5f

    .line 181
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mFBService:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    .line 183
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    .line 184
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00d3

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleButtons:Landroid/widget/RelativeLayout;

    .line 185
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00d4

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditButton:Landroid/widget/ImageView;

    .line 186
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00d6

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWindowCloseButton:Landroid/widget/ImageView;

    .line 188
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewEditButton:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mEditSmartWindowListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWindowCloseButton:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mClosePopupViewWindowListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00d2

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitle:Landroid/widget/ImageView;

    .line 192
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTitleTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 193
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitle:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mTitleHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 194
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00cf

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerWrapper:Landroid/widget/RelativeLayout;

    .line 195
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00d0

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v4/view/ViewPager;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    .line 198
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    const v5, 0x7f0f00d1

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    .line 199
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mViewPagerMark:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 202
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 203
    .local v2, "point":Landroid/graphics/Point;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 204
    iget v4, v2, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    .line 205
    iget v4, v2, Landroid/graphics/Point;->y:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    .line 206
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    int-to-float v4, v4

    mul-float/2addr v4, v6

    add-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinWidth:I

    .line 207
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    int-to-float v4, v4

    mul-float/2addr v4, v6

    add-float/2addr v4, v6

    float-to-int v4, v4

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinHeight:I

    .line 208
    new-instance v4, Landroid/content/res/Configuration;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mResouces:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    .line 210
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 211
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    if-eqz p2, :cond_8

    .line 212
    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    if-ge v4, v5, :cond_0

    .line 213
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    iput v4, p2, Landroid/graphics/Rect;->top:I

    .line 216
    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinWidth:I

    if-ge v4, v5, :cond_1

    .line 217
    iget v4, p2, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinWidth:I

    add-int/2addr v4, v5

    iput v4, p2, Landroid/graphics/Rect;->right:I

    .line 219
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinHeight:I

    if-ge v4, v5, :cond_2

    .line 220
    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinHeight:I

    add-int/2addr v4, v5

    iput v4, p2, Landroid/graphics/Rect;->bottom:I

    .line 223
    :cond_2
    iget v4, p2, Landroid/graphics/Rect;->right:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    if-le v4, v5, :cond_3

    .line 224
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    iget v5, p2, Landroid/graphics/Rect;->right:I

    sub-int v0, v4, v5

    .line 225
    .local v0, "gap":I
    invoke-virtual {p2, v0, v10}, Landroid/graphics/Rect;->offset(II)V

    .line 227
    .end local v0    # "gap":I
    :cond_3
    iget v4, p2, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    if-le v4, v5, :cond_4

    .line 228
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    iget v5, p2, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v4, v5

    .line 229
    .restart local v0    # "gap":I
    invoke-virtual {p2, v10, v0}, Landroid/graphics/Rect;->offset(II)V

    .line 232
    .end local v0    # "gap":I
    :cond_4
    iget v4, p2, Landroid/graphics/Rect;->left:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 233
    iget v4, p2, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 235
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v11, :cond_7

    .line 236
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 237
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I

    add-int/2addr v4, v5

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 250
    :goto_0
    const/4 v3, 0x0

    .line 251
    .local v3, "popupButtonMargin":I
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitle:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 252
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitle:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    div-int/lit8 v3, v4, 0x2

    .line 255
    :cond_5
    new-instance v4, Landroid/graphics/Rect;

    iget v5, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v7, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v7, v8

    iget v8, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v9, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v8, v9

    sub-int/2addr v8, v3

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;

    .line 257
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastPositionX:I

    .line 258
    iget v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastPositionY:I

    .line 259
    const/16 v4, 0x33

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 261
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v4, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 262
    const-string v4, "PopupViewWindow"

    invoke-virtual {v1, v4}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v5}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v5

    invoke-interface {v4, v5, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 266
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/16 v5, 0x67

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListCnt(I)I

    move-result v4

    if-nez v4, :cond_6

    .line 267
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 270
    :cond_6
    new-instance v4, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    invoke-direct {v4, p0, v5}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPagerAdapter:Lcom/sec/android/app/FlashBarService/PopupViewWindow$ViewPagerAdapter;

    .line 272
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v11, :cond_9

    .line 273
    invoke-virtual {p0, v10}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->makePopupViewPagerPort(Z)V

    .line 278
    :goto_1
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->initViewPageMark()V

    .line 280
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v5, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    invoke-virtual {v4, v5}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 298
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewPager:Landroid/support/v4/view/ViewPager;

    new-instance v5, Lcom/sec/android/app/FlashBarService/PopupViewWindow$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    invoke-virtual {v4, v5}, Landroid/support/v4/view/ViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 309
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/PopupViewWindow$4;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 319
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/FlashBarService/PopupViewWindow$5;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 348
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    new-instance v5, Lcom/sec/android/app/FlashBarService/PopupViewWindow$6;

    invoke-direct {v5, p0}, Lcom/sec/android/app/FlashBarService/PopupViewWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewWindow;)V

    invoke-virtual {v4, v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->setAppListSizeChangeListener(Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo$AppListSizeChangeListener;)V

    .line 354
    return-void

    .line 239
    .end local v3    # "popupButtonMargin":I
    :cond_7
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 240
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I

    add-int/2addr v4, v5

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/16 :goto_0

    .line 244
    :cond_8
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinWidth:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 245
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinHeight:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 246
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinWidth:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v5

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 247
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMinHeight:I

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mShadowPaddingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v5

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/16 :goto_0

    .line 275
    .restart local v3    # "popupButtonMargin":I
    :cond_9
    invoke-virtual {p0, v10}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->makePopupViewPagerLand(Z)V

    goto :goto_1
.end method

.method public updatePopupViewWindowPosition(Z)V
    .locals 8
    .param p1, "configurationChanged"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 503
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 504
    .local v1, "fullscreen":Landroid/graphics/Point;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 505
    iget v4, v1, Landroid/graphics/Point;->x:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    .line 506
    iget v4, v1, Landroid/graphics/Point;->y:I

    iput v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    .line 508
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 509
    .local v2, "l":Landroid/view/WindowManager$LayoutParams;
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mHasEditWindow:Z

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0162

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 511
    .local v0, "editWindowHeight":I
    :goto_0
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v7, :cond_5

    .line 512
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 513
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I

    add-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 519
    :goto_1
    if-eqz p1, :cond_8

    .line 520
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    sub-int/2addr v4, v5

    if-le v3, v4, :cond_0

    .line 521
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 524
    :cond_0
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_6

    .line 525
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 529
    :goto_2
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    sub-int/2addr v3, v0

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_7

    .line 530
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v4, v0

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 551
    :cond_1
    :goto_3
    iget v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    if-ge v3, v4, :cond_2

    .line 552
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 554
    :cond_2
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 555
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastConfig:Landroid/content/res/Configuration;

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v7, :cond_c

    .line 556
    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->makePopupViewPagerPort(Z)V

    .line 561
    :goto_4
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopUpViewWindow:Landroid/view/Window;

    invoke-virtual {v4}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 563
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    const/16 v4, 0x67

    invoke-virtual {v3, v4}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getViewPagerAppListCnt(I)I

    move-result v3

    if-nez v3, :cond_3

    .line 564
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v3}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeFlashBarList()V

    .line 568
    :cond_3
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 569
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 570
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 571
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayRect:Landroid/graphics/Rect;

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v4, v5

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 572
    return-void

    .end local v0    # "editWindowHeight":I
    :cond_4
    move v0, v3

    .line 509
    goto/16 :goto_0

    .line 515
    .restart local v0    # "editWindowHeight":I
    :cond_5
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperHeight:I

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 516
    iget v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewWrapperWidth:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mPopupViewTitleMargin:I

    add-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    goto/16 :goto_1

    .line 527
    :cond_6
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastPositionX:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    goto :goto_2

    .line 532
    :cond_7
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mLastPositionY:I

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto :goto_3

    .line 536
    :cond_8
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    iget v6, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_9

    .line 537
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mStatusBarHeight:I

    sub-int/2addr v4, v5

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 539
    :cond_9
    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    if-gez v4, :cond_a

    .line 540
    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 542
    :cond_a
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_b

    .line 543
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayWidth:I

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 545
    :cond_b
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    sub-int/2addr v3, v0

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iget v5, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v4, v5

    if-ge v3, v4, :cond_1

    .line 546
    iget v3, p0, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->mDisplayHeight:I

    iget v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    add-int/2addr v4, v0

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    goto/16 :goto_3

    .line 558
    :cond_c
    invoke-virtual {p0, v7}, Lcom/sec/android/app/FlashBarService/PopupViewWindow;->makePopupViewPagerLand(Z)V

    goto/16 :goto_4
.end method

.method public updateSmartWindowRelayout()V
    .locals 0

    .prologue
    .line 611
    return-void
.end method
