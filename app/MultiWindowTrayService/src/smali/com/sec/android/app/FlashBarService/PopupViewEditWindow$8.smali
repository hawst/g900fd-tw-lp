.class Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;
.super Ljava/lang/Object;
.source "PopupViewEditWindow.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)V
    .locals 0

    .prologue
    .line 445
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 448
    new-array v3, v8, [Ljava/lang/String;

    const-string v4, "text/uri-list"

    aput-object v4, v3, v7

    .line 449
    .local v3, "strs":[Ljava/lang/String;
    new-instance v2, Landroid/content/ClipData$Item;

    const-string v4, "cropUri"

    invoke-direct {v2, v4}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    .line 450
    .local v2, "item":Landroid/content/ClipData$Item;
    new-instance v0, Landroid/content/ClipData;

    const-string v4, "cropUri"

    invoke-direct {v0, v4, v3, v2}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    .line 452
    .local v0, "dragData":Landroid/content/ClipData;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    invoke-virtual {v5, p1}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->getItemIndex(Landroid/view/View;)I

    move-result v5

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditDragIndex:I
    invoke-static {v4, v5}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$602(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;I)I

    .line 453
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditDragIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$600(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->getImageViewByIndex(I)Landroid/widget/ImageView;

    move-result-object v5

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mEditingDragView:Landroid/widget/ImageView;
    invoke-static {v4, v5}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$502(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    .line 454
    if-eqz v0, :cond_0

    .line 456
    new-instance v1, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8$1;-><init>(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;Landroid/view/View;)V

    .line 468
    .local v1, "dragShadow":Landroid/view/View$DragShadowBuilder;
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mbStartDragFromEdit:Z
    invoke-static {v4, v8}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$702(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;Z)Z

    .line 469
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$1000(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/os/SystemVibrator;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mIvt:[B
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$900(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)[B

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;->access$1000(Lcom/sec/android/app/FlashBarService/PopupViewEditWindow;)Landroid/os/SystemVibrator;

    move-result-object v6

    invoke-virtual {v6}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 471
    const/4 v4, 0x0

    invoke-virtual {p1, v0, v1, v4, v7}, Landroid/view/View;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 473
    .end local v1    # "dragShadow":Landroid/view/View$DragShadowBuilder;
    :cond_0
    return v7
.end method
