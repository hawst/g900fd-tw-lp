.class Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;
.super Ljava/lang/Object;
.source "MultiWindowTrayService.java"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->makeCenterBarWindow(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)V
    .locals 0

    .prologue
    .line 1049
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 1052
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1500(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1053
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowManager:Landroid/view/WindowManager;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1700(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/WindowManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1600(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 1054
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mWindowCenterBar:Landroid/view/Window;
    invoke-static {v0, v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1602(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Landroid/view/Window;)Landroid/view/Window;

    .line 1055
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mRemoveViewWindowCenterBar:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1502(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z

    .line 1056
    const-string v0, "MultiWindowTrayService"

    const-string v1, "Success : mWindowCenterBar, removeViewImmediate after attached "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowCenterbarButton:Z
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1800(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1059
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # setter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mShowCenterbarButton:Z
    invoke-static {v0, v2}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$1802(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;Z)Z

    .line 1060
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1061
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService$20;->this$0:Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;

    # getter for: Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->mCenterBarWindow:Lcom/sec/android/app/FlashBarService/CenterBarWindow;
    invoke-static {v0}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;->access$400(Lcom/sec/android/app/FlashBarService/MultiWindowTrayService;)Lcom/sec/android/app/FlashBarService/CenterBarWindow;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/CenterBarWindow;->initCenterBarButtonAnim()V

    .line 1064
    :cond_1
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1066
    return-void
.end method
