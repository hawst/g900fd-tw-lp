.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;
.super Ljava/lang/Object;
.source "AppListGridWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 3552
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 3554
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v1, p1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getRunningAppItemIndex(Landroid/view/View;)I

    move-result v0

    .line 3555
    .local v0, "runningAppIndex":I
    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    .line 3556
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListController:Lcom/sec/android/app/FlashBarService/AppListController;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/FlashBarService/AppListController;->startRunningApp(I)V

    .line 3558
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$45;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v2, 0xcf

    const-wide/16 v4, 0x320

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3559
    return-void
.end method
