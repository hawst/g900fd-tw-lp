.class Lcom/sec/android/app/FlashBarService/SmartWindow$8;
.super Ljava/lang/Object;
.source "SmartWindow.java"

# interfaces
.implements Landroid/view/View$OnDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/SmartWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartWindow;)V
    .locals 0

    .prologue
    .line 738
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrag(Landroid/view/View;Landroid/view/DragEvent;)Z
    .locals 10
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/DragEvent;

    .prologue
    .line 741
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v0

    .line 742
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 850
    .end local p1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    const/4 v6, 0x1

    return v6

    .line 744
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_0
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->findCurIndex(Landroid/view/View;)I
    invoke-static {v7, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1600(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;)I

    move-result v7

    if-ne v6, v7, :cond_0

    .line 746
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v6, 0x7f0f0059

    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object v4, v6

    check-cast v4, Landroid/widget/ImageView;

    .line 747
    .local v4, "orgView":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1702(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 748
    const v6, 0x7f02007f

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 749
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mIvt:[B
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1800(Lcom/sec/android/app/FlashBarService/SmartWindow;)[B

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 751
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setItemIndexFromAppList(I)V

    goto :goto_0

    .line 755
    .end local v4    # "orgView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_1
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2102(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z

    .line 756
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2200(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 757
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPopupAppCnt()I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v8

    const/4 v9, 0x6

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z

    .line 758
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddEmptyItemPosition:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2202(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    goto/16 :goto_0

    .line 762
    :pswitch_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 763
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->decideScrollMove(Landroid/view/View;Landroid/view/DragEvent;)Z
    invoke-static {v6, p1, p2}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2400(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;Landroid/view/DragEvent;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 764
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->findCurIndex(Landroid/view/View;)I
    invoke-static {v7, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1600(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;)I

    move-result v7

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 765
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 766
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 767
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, v7, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v7

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 768
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPopupAppCnt()I

    move-result v7

    if-ge v6, v7, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    if-eq v6, v7, :cond_2

    .line 769
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v7, v8, v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z

    .line 778
    :cond_1
    :goto_1
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v6, 0x7f0f0059

    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object v1, v6

    check-cast v1, Landroid/widget/ImageView;

    .line 779
    .local v1, "blankView":Landroid/widget/ImageView;
    const v6, 0x7f02007f

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    .line 771
    .end local v1    # "blankView":Landroid/widget/ImageView;
    .restart local p1    # "view":Landroid/view/View;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getPopupAppCnt()I

    move-result v7

    if-ne v6, v7, :cond_1

    .line 772
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v7, v8, v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z

    goto :goto_1

    .line 775
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changelistItem(II)Z
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2600(Lcom/sec/android/app/FlashBarService/SmartWindow;II)Z

    .line 776
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setItemIndexFromAppList(I)V

    goto :goto_1

    .line 785
    :pswitch_3
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->checkAppListLayout()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 786
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowRelayout()V

    .line 787
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setStartDragFromEdit(Z)V

    .line 788
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1702(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 789
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 790
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 792
    :cond_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_5

    .line 793
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mFBInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$400(Lcom/sec/android/app/FlashBarService/SmartWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->changeSmartWindowItem(IIZ)V

    .line 794
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 795
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowRelayout()V

    .line 798
    :cond_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->findCurIndex(Landroid/view/View;)I
    invoke-static {v6, p1}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1600(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/view/View;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    if-ne v6, v7, :cond_7

    .line 799
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v6, 0x7f0f0059

    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object v3, v6

    check-cast v3, Landroid/widget/ImageView;

    .line 800
    .local v3, "org":Landroid/widget/ImageView;
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 801
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 802
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1702(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 804
    :cond_6
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 805
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 807
    .end local v3    # "org":Landroid/widget/ImageView;
    :cond_7
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 808
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 811
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_4
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 812
    :cond_8
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->doOneTimeAfterDragStarts:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2702(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z

    .line 813
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v6, 0x7f0f0059

    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object v1, v6

    check-cast v1, Landroid/widget/ImageView;

    .line 814
    .restart local v1    # "blankView":Landroid/widget/ImageView;
    const v6, 0x7f02007f

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 816
    .end local v1    # "blankView":Landroid/widget/ImageView;
    :cond_9
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 817
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->changlistItemFromEditList(III)Z
    invoke-static {v6, v7, v8, v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2300(Lcom/sec/android/app/FlashBarService/SmartWindow;III)Z

    .line 818
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 819
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    goto/16 :goto_0

    .line 823
    .restart local p1    # "view":Landroid/view/View;
    :pswitch_5
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAppListDragMode:Z
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2102(Lcom/sec/android/app/FlashBarService/SmartWindow;Z)Z

    .line 824
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_a

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 825
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2000(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v8, v8, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v8}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getItemIndexFromEditList()I

    move-result v8

    # invokes: Lcom/sec/android/app/FlashBarService/SmartWindow;->addAndChangelistItem(II)Z
    invoke-static {v6, v7, v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2800(Lcom/sec/android/app/FlashBarService/SmartWindow;II)Z

    .line 826
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mIvt:[B
    invoke-static {v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1800(Lcom/sec/android/app/FlashBarService/SmartWindow;)[B

    move-result-object v7

    iget-object v8, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mVibrator:Landroid/os/SystemVibrator;
    invoke-static {v8}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1900(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/os/SystemVibrator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/os/SystemVibrator;->getMaxMagnitude()I

    move-result v8

    invoke-virtual {v6, v7, v8}, Landroid/os/SystemVibrator;->vibrateImmVibe([BI)V

    .line 827
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    const/4 v7, -0x1

    invoke-virtual {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->setItemIndexFromAppList(I)V

    .line 828
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mAddToListItem:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 829
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->updateSmartEditChanged()V

    .line 831
    :cond_a
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1500(Lcom/sec/android/app/FlashBarService/SmartWindow;)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_b

    .line 832
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02007f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 833
    .local v2, "curIcon":Landroid/graphics/drawable/Drawable;
    check-cast p1, Landroid/widget/LinearLayout;

    .end local p1    # "view":Landroid/view/View;
    const v6, 0x7f0f0059

    invoke-virtual {p1, v6}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    move-object v5, v6

    check-cast v5, Landroid/widget/ImageView;

    .line 834
    .local v5, "tmpiv":Landroid/widget/ImageView;
    invoke-virtual {v5}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    if-ne v6, v2, :cond_b

    .line 835
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1700(Lcom/sec/android/app/FlashBarService/SmartWindow;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 836
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, 0x0

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->orgIcon:Landroid/graphics/drawable/Drawable;
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1702(Lcom/sec/android/app/FlashBarService/SmartWindow;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 839
    .end local v2    # "curIcon":Landroid/graphics/drawable/Drawable;
    .end local v5    # "tmpiv":Landroid/widget/ImageView;
    :cond_b
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 840
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mDragHandler:Landroid/os/Handler;

    const/4 v7, 0x3

    invoke-virtual {v6, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 842
    :cond_c
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mHasEditWindow:Z
    invoke-static {v6}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$200(Lcom/sec/android/app/FlashBarService/SmartWindow;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    iget-object v6, v6, Lcom/sec/android/app/FlashBarService/SmartWindow;->mSmartEditWindow:Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    invoke-virtual {v6}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->getStartDragFromEdit()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 843
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurDstIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$2002(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    .line 844
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartWindow$8;->this$0:Lcom/sec/android/app/FlashBarService/SmartWindow;

    const/4 v7, -0x1

    # setter for: Lcom/sec/android/app/FlashBarService/SmartWindow;->mcurSrcIndex:I
    invoke-static {v6, v7}, Lcom/sec/android/app/FlashBarService/SmartWindow;->access$1502(Lcom/sec/android/app/FlashBarService/SmartWindow;I)I

    goto/16 :goto_0

    .line 742
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
