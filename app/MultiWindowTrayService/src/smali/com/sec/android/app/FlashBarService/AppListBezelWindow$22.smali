.class Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;
.super Ljava/lang/Object;
.source "AppListBezelWindow.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->makeUsingHelpPopupLayout()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListBezelWindow;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 1453
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    .line 1456
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1457
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "do_not_show_help_popup_using"

    const/4 v3, 0x1

    const/4 v4, -0x2

    invoke-static {v1, v2, v3, v4}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    .line 1459
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->hideUsingHelpPopup()V

    .line 1460
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    invoke-virtual {v1, v5, v5}, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->updateFlashBarState(ZZ)Z

    .line 1461
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.helphub.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1462
    .local v0, "helpIntent":Landroid/content/Intent;
    const/high16 v1, 0x30200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1463
    const-string v1, "helphub:section"

    const-string v2, "multi_window"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1464
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListBezelWindow$22;->this$0:Lcom/sec/android/app/FlashBarService/AppListBezelWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListBezelWindow;->mContext:Landroid/content/Context;

    sget-object v2, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    .line 1465
    return-void
.end method
