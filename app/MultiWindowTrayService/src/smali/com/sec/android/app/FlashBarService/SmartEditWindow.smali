.class public Lcom/sec/android/app/FlashBarService/SmartEditWindow;
.super Ljava/lang/Object;
.source "SmartEditWindow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;
    }
.end annotation


# instance fields
.field private final APPLIST_DRAG_ZONE:I

.field private final APPLIST_LONG_PRESS:I

.field private final APPLIST_TIMER_LONG_PRESS:I

.field private final SCROLL_MOVE:I

.field private final SCROLL_MOVE_DELAY:I

.field private final SCROLL_MOVE_LEFT:I

.field private final SCROLL_MOVE_RIGHT:I

.field mAppClickListener:Landroid/view/View$OnClickListener;

.field mAppEditListDragListener:Landroid/view/View$OnDragListener;

.field private mAppListDragIndex:I

.field mAppListEditKeyListener:Landroid/view/View$OnKeyListener;

.field mAppLongClickListener:Landroid/view/View$OnLongClickListener;

.field mAppTouchListener:Landroid/view/View$OnTouchListener;

.field mArrowHoverListener:Landroid/view/View$OnHoverListener;

.field mArrowTouchListener:Landroid/view/View$OnTouchListener;

.field private mContext:Landroid/content/Context;

.field private mCurrentDownEvent:Landroid/view/MotionEvent;

.field private mCurrentDownView:Landroid/view/View;

.field private mDisplayWidth:I

.field private mEditDragIndex:I

.field private mEditItemLayout:Landroid/widget/LinearLayout;

.field private mEditList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private mEditScrollView:Landroid/widget/HorizontalScrollView;

.field private mEditingDragView:Landroid/widget/ImageView;

.field private mIvt:[B

.field private mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

.field private mPressArrowbutton:Z

.field mScrollHandler:Landroid/os/Handler;

.field private mSmartEditLeftButton:Landroid/widget/ImageButton;

.field private mSmartEditRightButton:Landroid/widget/ImageButton;

.field private mSmartEditWindow:Landroid/view/Window;

.field private mSmartTextView:Landroid/widget/TextView;

.field private mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

.field mTimerHandler:Landroid/os/Handler;

.field private mVibrator:Landroid/os/SystemVibrator;

.field private mWindowManager:Landroid/view/WindowManager;

.field private mbStartDragFromEdit:Z

.field private res:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/SmartWindow;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "smartWindow"    # Lcom/sec/android/app/FlashBarService/SmartWindow;
    .param p3, "appInfo"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/16 v0, 0xca

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->APPLIST_LONG_PRESS:I

    .line 54
    const/16 v0, 0xc8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->APPLIST_TIMER_LONG_PRESS:I

    .line 55
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->APPLIST_DRAG_ZONE:I

    .line 56
    iput v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->SCROLL_MOVE:I

    .line 57
    iput v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->SCROLL_MOVE_LEFT:I

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->SCROLL_MOVE_RIGHT:I

    .line 59
    const/16 v0, 0xf

    iput v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->SCROLL_MOVE_DELAY:I

    .line 71
    const/16 v0, 0x26

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mIvt:[B

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    .line 87
    iput v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppListDragIndex:I

    .line 91
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    .line 92
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mPressArrowbutton:Z

    .line 115
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$1;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mScrollHandler:Landroid/os/Handler;

    .line 302
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$2;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    .line 330
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$3;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$3;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppEditListDragListener:Landroid/view/View$OnDragListener;

    .line 369
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$4;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mArrowTouchListener:Landroid/view/View$OnTouchListener;

    .line 395
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$5;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$5;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mArrowHoverListener:Landroid/view/View$OnHoverListener;

    .line 409
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$6;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$6;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppClickListener:Landroid/view/View$OnClickListener;

    .line 416
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$7;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$7;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppTouchListener:Landroid/view/View$OnTouchListener;

    .line 445
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$8;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$8;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppLongClickListener:Landroid/view/View$OnLongClickListener;

    .line 477
    new-instance v0, Lcom/sec/android/app/FlashBarService/SmartEditWindow$9;

    invoke-direct {v0, p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$9;-><init>(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppListEditKeyListener:Landroid/view/View$OnKeyListener;

    .line 125
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mContext:Landroid/content/Context;

    .line 126
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mWindowManager:Landroid/view/WindowManager;

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->res:Landroid/content/res/Resources;

    .line 129
    iput v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditDragIndex:I

    .line 130
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mContext:Landroid/content/Context;

    const-string v1, "vibrator"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/SystemVibrator;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mVibrator:Landroid/os/SystemVibrator;

    .line 132
    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 133
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    .line 134
    return-void

    .line 71
    :array_0
    .array-data 1
        0x1t
        0x0t
        0x1t
        0x0t
        0xat
        0x0t
        0x14t
        0x0t
        0x0t
        0x0t
        0x20t
        0x1t
        0x0t
        0x0t
        0x7ft
        0x0t
        0x0t
        0x42t
        0x0t
        0x0t
        0x4dt
        0x0t
        0x61t
        0x0t
        0x67t
        0x0t
        0x53t
        0x0t
        0x77t
        0x0t
        0x65t
        0x0t
        0x65t
        0x0t
        0x70t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Landroid/widget/HorizontalScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Landroid/os/SystemVibrator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mVibrator:Landroid/os/SystemVibrator;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Landroid/view/MotionEvent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Lcom/sec/android/app/FlashBarService/SmartWindow;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mCurrentDownView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mCurrentDownView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mPressArrowbutton:Z

    return v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mPressArrowbutton:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->checkCanScroll()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditingDragView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # Landroid/widget/ImageView;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditingDragView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditDragIndex:I

    return v0
.end method

.method static synthetic access$602(Lcom/sec/android/app/FlashBarService/SmartEditWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditDragIndex:I

    return p1
.end method

.method static synthetic access$700(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/app/FlashBarService/SmartEditWindow;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppListDragIndex:I

    return v0
.end method

.method static synthetic access$802(Lcom/sec/android/app/FlashBarService/SmartEditWindow;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;
    .param p1, "x1"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppListDragIndex:I

    return p1
.end method

.method static synthetic access$900(Lcom/sec/android/app/FlashBarService/SmartEditWindow;)[B
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/SmartEditWindow;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mIvt:[B

    return-object v0
.end method

.method private checkCanScroll()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 284
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 285
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->canScrollHorizontally(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 293
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 300
    :cond_1
    return-void

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 288
    :goto_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 295
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 296
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method private makeSmartEditWindowLayout()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 163
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 164
    .local v0, "fullscreen":Landroid/graphics/Point;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mWindowManager:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 165
    iget v2, v0, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mDisplayWidth:I

    .line 167
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 168
    .local v1, "l":Landroid/view/WindowManager$LayoutParams;
    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 169
    const/16 v2, 0x50

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 170
    iget v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mDisplayWidth:I

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0162

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 171
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v2, v2, 0x20

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 172
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 173
    const-string v2, "SmartEditWindow"

    invoke-virtual {v1, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    .line 175
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 176
    return-void
.end method


# virtual methods
.method public closeSmartEditWindow()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    .line 214
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    .line 215
    return-void
.end method

.method public getImageViewByIndex(I)Landroid/widget/ImageView;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 275
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 276
    .local v0, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v0, :cond_0

    .line 277
    const v1, 0x7f0f0059

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 279
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getItemIndex(Landroid/view/View;)I
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 261
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .line 262
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 263
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 264
    .local v2, "tmpView":Landroid/widget/LinearLayout;
    if-eqz v2, :cond_2

    .line 265
    const v3, 0x7f0f0059

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-ne p1, v3, :cond_1

    .line 271
    .end local v1    # "i":I
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_0
    :goto_1
    return v1

    .line 267
    .restart local v1    # "i":I
    .restart local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_1
    const v3, 0x7f0f005c

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eq p1, v3, :cond_0

    .line 262
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 271
    .end local v2    # "tmpView":Landroid/widget/LinearLayout;
    :cond_3
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public getItemIndexFromEditList()I
    .locals 1

    .prologue
    .line 238
    iget v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditDragIndex:I

    return v0
.end method

.method public getStartDragFromEdit()Z
    .locals 1

    .prologue
    .line 242
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    return v0
.end method

.method public makeSmartEditItemList()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 180
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getUnableSmartAppCnt()I

    move-result v0

    .line 181
    .local v0, "cnt":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 183
    .local v3, "inflater":Landroid/view/LayoutInflater;
    if-nez v0, :cond_0

    .line 184
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartTextView:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 190
    const v5, 0x7f030005

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 191
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 193
    .local v4, "item":Landroid/view/View;
    new-instance v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;

    invoke-direct {v1}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;-><init>()V

    .line 195
    .local v1, "holder":Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;
    const v5, 0x7f0f0059

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    iput-object v5, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;->iconView:Landroid/widget/ImageView;

    .line 196
    const v5, 0x7f0f005c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    .line 197
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    const v6, -0x9090a

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 198
    iget-object v5, v1, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;->labelView:Landroid/widget/TextView;

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 200
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 201
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v5}, Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    invoke-virtual {v4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 205
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    check-cast v4, Landroid/widget/LinearLayout;

    .end local v4    # "item":Landroid/view/View;
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 186
    .end local v1    # "holder":Lcom/sec/android/app/FlashBarService/SmartEditWindow$ViewHolder;
    .end local v2    # "i":I
    :cond_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartTextView:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 208
    .restart local v2    # "i":I
    :cond_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    invoke-virtual {v5, v6}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->makeAppListForSmartEditWindow(Ljava/util/List;)V

    .line 209
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->checkCanScroll()V

    .line 210
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->makeSmartEditWindowLayout()V

    .line 328
    return-void
.end method

.method public setEditWindowVisibility(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    const/4 v2, 0x1

    .line 218
    if-eqz p1, :cond_2

    .line 219
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 223
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    .line 224
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 225
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->updateSmartEditChanged()V

    .line 231
    :cond_1
    :goto_0
    return-void

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setItemIndexFromAppList(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppListDragIndex:I

    .line 235
    return-void
.end method

.method public setStartDragFromEdit(Z)V
    .locals 0
    .param p1, "bEnd"    # Z

    .prologue
    .line 246
    iput-boolean p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    .line 247
    return-void
.end method

.method public setWindow(Landroid/view/Window;Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;)V
    .locals 2
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "info"    # Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .prologue
    .line 138
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    .line 139
    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    .line 141
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    const v1, 0x7f0f00f4

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    .line 142
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    const v1, 0x7f0f00f5

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    .line 143
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    const v1, 0x7f0f00f2

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditLeftButton:Landroid/widget/ImageButton;

    .line 144
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    const v1, 0x7f0f00f3

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditRightButton:Landroid/widget/ImageButton;

    .line 145
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    const v1, 0x7f0f00f6

    invoke-virtual {v0, v1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartTextView:Landroid/widget/TextView;

    .line 147
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppEditListDragListener:Landroid/view/View$OnDragListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 148
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mArrowTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 149
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditScrollView:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mArrowHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 150
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditLeftButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mArrowTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 151
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditRightButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mArrowTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 152
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mAppListEditKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 154
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->makeSmartEditWindowLayout()V

    .line 155
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->makeSmartEditItemList()V

    .line 157
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartEditWindow:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    .line 160
    return-void
.end method

.method public updateSmartEditChanged()V
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 251
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mEditItemLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 254
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->makeSmartEditItemList()V

    .line 255
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mbStartDragFromEdit:Z

    if-nez v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/SmartEditWindow;->mSmartWindow:Lcom/sec/android/app/FlashBarService/SmartWindow;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/SmartWindow;->updateSmartWindowRelayout()V

    .line 258
    :cond_1
    return-void
.end method
