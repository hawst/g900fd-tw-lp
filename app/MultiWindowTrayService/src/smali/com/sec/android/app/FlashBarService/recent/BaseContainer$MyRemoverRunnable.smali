.class Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;
.super Ljava/lang/Object;
.source "BaseContainer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/recent/BaseContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyRemoverRunnable"
.end annotation


# instance fields
.field animate:Z

.field index:I

.field next:Landroid/view/View;

.field scrollStep:F

.field scrollTo:F

.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/recent/BaseContainer;IFI)V
    .locals 3
    .param p2, "index"    # I
    .param p3, "pos"    # F
    .param p4, "s"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 228
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 225
    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    .line 226
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->animate:Z

    .line 229
    int-to-float v0, p4

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    .line 230
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    .line 231
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 232
    sget v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAnimationScrollStepForSizeOne:I

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    .line 236
    :goto_0
    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->animate:Z

    .line 238
    :cond_0
    iput p2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->index:I

    .line 239
    iput p3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollTo:F

    .line 240
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 241
    iget-object v0, p1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->next:Landroid/view/View;

    .line 245
    :goto_1
    return-void

    .line 234
    :cond_1
    sget v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->mAnimationScrollStep:I

    neg-int v0, v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    goto :goto_0

    .line 243
    :cond_2
    invoke-virtual {p1, p2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getNextChild(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->next:Landroid/view/View;

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 248
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->views:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 249
    const-string v0, "MIK"

    const-string v1, "can\'t start MyRemoveRunnable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 254
    :cond_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 255
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->next:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollTo:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 256
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->index:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(FI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->animate:Z

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {v0, p0, v4, v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollTo:F

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->next:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->index:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(FI)Z

    goto :goto_0

    .line 269
    :cond_3
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->next:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v0

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollTo:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 270
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->index:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(FI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    iget-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->animate:Z

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    invoke-virtual {v0, p0, v4, v5}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 279
    :cond_4
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->this$0:Lcom/sec/android/app/FlashBarService/recent/BaseContainer;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->next:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->getScrollPosition(Landroid/view/View;)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollStep:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->scrollTo:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/BaseContainer$MyRemoverRunnable;->index:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/BaseContainer;->doScroll(FI)Z

    goto :goto_0
.end method
