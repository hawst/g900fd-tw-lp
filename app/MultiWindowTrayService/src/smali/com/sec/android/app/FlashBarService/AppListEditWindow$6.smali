.class Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;
.super Ljava/lang/Object;
.source "AppListEditWindow.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListEditWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)V
    .locals 0

    .prologue
    .line 619
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v8, 0xca

    .line 621
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 644
    :cond_0
    :goto_0
    const/4 v5, 0x0

    return v5

    .line 624
    :pswitch_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 625
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v5, p1}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$202(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/view/View;)Landroid/view/View;

    .line 626
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v6

    # setter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$1202(Lcom/sec/android/app/FlashBarService/AppListEditWindow;Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 627
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mTimerHandler:Landroid/os/Handler;

    const-wide/16 v6, 0xc8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 630
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    .line 631
    .local v3, "x":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 632
    .local v4, "y":F
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    sub-float v5, v3, v5

    float-to-int v0, v5

    .line 633
    .local v0, "deltaX":I
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mCurrentDownEvent:Landroid/view/MotionEvent;
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListEditWindow;)Landroid/view/MotionEvent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    sub-float v5, v4, v5

    float-to-int v1, v5

    .line 634
    .local v1, "deltaY":I
    mul-int v5, v0, v0

    mul-int v6, v1, v1

    add-int v2, v5, v6

    .line 635
    .local v2, "distance":I
    const/16 v5, 0x3e8

    if-le v2, v5, :cond_0

    .line 636
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 641
    .end local v0    # "deltaX":I
    .end local v1    # "deltaY":I
    .end local v2    # "distance":I
    .end local v3    # "x":F
    .end local v4    # "y":F
    :pswitch_2
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/AppListEditWindow$6;->this$0:Lcom/sec/android/app/FlashBarService/AppListEditWindow;

    iget-object v5, v5, Lcom/sec/android/app/FlashBarService/AppListEditWindow;->mTimerHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 621
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
