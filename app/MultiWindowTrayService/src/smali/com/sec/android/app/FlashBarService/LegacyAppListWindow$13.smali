.class Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$13;
.super Ljava/lang/Object;
.source "LegacyAppListWindow.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$PageTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;->createViewPagerTray()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;)V
    .locals 0

    .prologue
    .line 980
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/LegacyAppListWindow$13;->this$0:Lcom/sec/android/app/FlashBarService/LegacyAppListWindow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public transformPage(Landroid/view/View;F)V
    .locals 3
    .param p1, "page"    # Landroid/view/View;
    .param p2, "position"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 983
    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_0

    cmpg-float v1, p2, v2

    if-gtz v1, :cond_0

    .line 984
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 985
    .local v0, "normalizedposition":F
    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 987
    .end local v0    # "normalizedposition":F
    :cond_0
    return-void
.end method
