.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;
.super Landroid/os/Handler;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 569
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x0

    .line 571
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 610
    :goto_0
    return-void

    .line 573
    :sswitch_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v1, v2, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateFlashBarState(ZZ)Z

    goto :goto_0

    .line 577
    :sswitch_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppIconLongClickListener:Landroid/view/View$OnLongClickListener;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mCurrentDownView:Landroid/view/View;
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$800(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/view/View;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/view/View$OnLongClickListener;->onLongClick(Landroid/view/View;)Z

    goto :goto_0

    .line 581
    :sswitch_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationRecentIconAppearByHomeKey()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$900(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 585
    :sswitch_3
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeRecentApp()V

    .line 586
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->animationRecentIcon()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1000(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 590
    :sswitch_4
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mThumbnailType:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1100(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->makeHistoryBarDialog(I)V
    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V

    goto :goto_0

    .line 594
    :sswitch_5
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v1, v1, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mMultiWindowTrayInfo:Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;

    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/MultiWindowTrayInfo;->getAvailableTemplateText()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mEditTextView:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v2, v3, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->createTemplateDialog(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 598
    :sswitch_6
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mExpandAppList:Z
    invoke-static {v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$600(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)Z

    move-result v4

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListPosition(IZZ)V
    invoke-static {v1, v2, v3, v4}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$700(Lcom/sec/android/app/FlashBarService/AppListGridWindow;IZZ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 599
    :catch_0
    move-exception v0

    .line 600
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "AppListGridWindow"

    const-string v2, "Exception in mInputMethodChangedReceiver."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 604
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_7
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateAppListRelayout(Z)V

    .line 605
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$3;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->updateRunningAppList()V
    invoke-static {v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$1400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V

    goto :goto_0

    .line 571
    nop

    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0xca -> :sswitch_1
        0xcb -> :sswitch_2
        0xcc -> :sswitch_3
        0xcd -> :sswitch_5
        0xce -> :sswitch_6
        0xcf -> :sswitch_7
        0x12c -> :sswitch_4
    .end sparse-switch
.end method
