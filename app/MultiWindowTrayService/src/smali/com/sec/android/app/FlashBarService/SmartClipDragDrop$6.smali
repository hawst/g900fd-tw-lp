.class Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;
.super Ljava/lang/Object;
.source "SmartClipDragDrop.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getSmartClipData(Landroid/view/View;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

.field final synthetic val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

.field final synthetic val$view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 742
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iput-object p2, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    iput-object p3, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 746
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    if-eqz v5, :cond_7

    .line 748
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    invoke-virtual {v5, v8}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->dump(Z)Z

    .line 749
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getTextMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$700(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;

    move-result-object v2

    .line 750
    .local v2, "textData":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getUrlMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$800(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;

    move-result-object v4

    .line 751
    .local v4, "urlData":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->getTextSelectionMetaData(Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;
    invoke-static {v5, v6}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$900(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;)Ljava/lang/String;

    move-result-object v3

    .line 752
    .local v3, "textSelectionData":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    invoke-virtual {v5}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getContentRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 753
    .local v0, "croppedRect":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    invoke-virtual {v5}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getWindowLayer()I

    move-result v1

    .line 755
    .local v1, "layerToCapture":I
    const-string v5, "SmartClipDragDrop"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onTouch : The target layer of dragging is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    if-eqz v3, :cond_0

    .line 758
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : text selection is extracted"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 759
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    const/4 v7, 0x0

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    invoke-static {v5, v6, v3, v7, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1000(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V

    .line 788
    .end local v0    # "croppedRect":Landroid/graphics/Rect;
    .end local v1    # "layerToCapture":I
    .end local v2    # "textData":Ljava/lang/String;
    .end local v3    # "textSelectionData":Ljava/lang/String;
    .end local v4    # "urlData":Ljava/lang/String;
    :goto_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopSmartClipDragDropThread()V
    invoke-static {v5}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1200(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;)V

    .line 790
    return-void

    .line 760
    .restart local v0    # "croppedRect":Landroid/graphics/Rect;
    .restart local v1    # "layerToCapture":I
    .restart local v2    # "textData":Ljava/lang/String;
    .restart local v3    # "textSelectionData":Ljava/lang/String;
    .restart local v4    # "urlData":Ljava/lang/String;
    :cond_0
    if-eqz v2, :cond_1

    .line 761
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : text is extracted"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    invoke-static {v5, v6, v2, v0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1000(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V

    goto :goto_0

    .line 763
    :cond_1
    if-eqz v4, :cond_2

    const-string v5, "youtube"

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    invoke-virtual {v6}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getContentType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v5, v8, :cond_2

    .line 764
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : youtube url will be pasted"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    invoke-static {v5, v6, v4, v0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1000(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V

    goto :goto_0

    .line 766
    :cond_2
    const-string v5, "com.android.chrome"

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$repository:Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;

    invoke-virtual {v6}, Lcom/samsung/android/smartclip/SmartClipDataRepositoryImpl;->getAppPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-ne v5, v8, :cond_4

    .line 767
    if-eqz v4, :cond_3

    .line 768
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : google chrome url will be pasted"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    invoke-static {v5, v6, v4, v0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1000(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V

    goto :goto_0

    .line 771
    :cond_3
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : google chrome url is empty. stop the service"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopService()V

    goto :goto_0

    .line 774
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-lez v5, :cond_5

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    if-lez v5, :cond_5

    .line 775
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : no text meta data -> image will be pasted"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startImageDrag(Landroid/view/View;Landroid/graphics/Rect;I)V
    invoke-static {v5, v6, v0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1100(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Landroid/graphics/Rect;I)V

    goto :goto_0

    .line 777
    :cond_5
    if-eqz v4, :cond_6

    .line 778
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : url will be pasted"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->val$view:Landroid/view/View;

    # invokes: Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->startTextDrag(Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V
    invoke-static {v5, v6, v4, v0, v1}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->access$1000(Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;Landroid/view/View;Ljava/lang/String;Landroid/graphics/Rect;I)V

    goto/16 :goto_0

    .line 781
    :cond_6
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : there is no data to drag&drop"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 782
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopService()V

    goto/16 :goto_0

    .line 785
    .end local v0    # "croppedRect":Landroid/graphics/Rect;
    .end local v1    # "layerToCapture":I
    .end local v2    # "textData":Ljava/lang/String;
    .end local v3    # "textSelectionData":Ljava/lang/String;
    .end local v4    # "urlData":Ljava/lang/String;
    :cond_7
    const-string v5, "SmartClipDragDrop"

    const-string v6, "onTouch : repository is null!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 786
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop$6;->this$0:Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;

    invoke-virtual {v5}, Lcom/sec/android/app/FlashBarService/SmartClipDragDrop;->stopService()V

    goto/16 :goto_0
.end method
