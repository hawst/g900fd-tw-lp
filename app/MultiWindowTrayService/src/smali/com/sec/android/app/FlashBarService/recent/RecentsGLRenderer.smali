.class public Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;
.super Ljava/lang/Object;
.source "RecentsGLRenderer.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private eyeY:F

.field private mBgMatrix:[F

.field private mBgTextureHandle:I

.field private mBgTextureVerticesData:Ljava/nio/FloatBuffer;

.field private mBgVerticesData:Ljava/nio/FloatBuffer;

.field private mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

.field private mChildRemoved:Z

.field private mContext:Landroid/content/Context;

.field private mDiff:F

.field mFlingAnimator:Landroid/animation/TimeAnimator;

.field private final mGLActionsQueue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mHeight:I

.field private mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

.field private mMaxPos:F

.field private mMinPos:F

.field private mProjectionMatrix:[F

.field private mScrollMultiplier:F

.field private mScrollPosition:F

.field private mScroller:Landroid/widget/Scroller;

.field private mSmoothingSpeed:F

.field private mTapped:Z

.field private mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

.field private final mTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;",
            ">;"
        }
    .end annotation
.end field

.field private mViewMatrix:[F

.field private mWidth:I

.field private final zoom:F


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x10

    const/4 v4, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    .line 37
    new-instance v2, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    invoke-direct {v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    .line 38
    new-array v2, v5, [F

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mViewMatrix:[F

    .line 39
    new-array v2, v5, [F

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mProjectionMatrix:[F

    .line 40
    iput v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 43
    iput v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMinPos:F

    .line 44
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMaxPos:F

    .line 47
    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->zoom:F

    .line 49
    iput-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    .line 50
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 51
    const v2, 0x443b8000    # 750.0f

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    .line 53
    const/high16 v2, 0x41900000    # 18.0f

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    .line 58
    new-instance v2, Landroid/animation/TimeAnimator;

    invoke-direct {v2}, Landroid/animation/TimeAnimator;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mFlingAnimator:Landroid/animation/TimeAnimator;

    .line 63
    new-array v2, v5, [F

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgMatrix:[F

    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    .line 68
    new-instance v2, Landroid/widget/Scroller;

    invoke-direct {v2, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    .line 69
    new-instance v2, Landroid/view/GestureDetector;

    invoke-direct {v2, p1, p0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGestureDetector:Landroid/view/GestureDetector;

    .line 71
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mContext:Landroid/content/Context;

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 74
    .local v0, "r":Landroid/content/res/Resources;
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 76
    .local v1, "t":Landroid/util/TypedValue;
    const v2, 0x7f0a01a5

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 77
    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v2

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->eyeY:F

    .line 79
    const v2, 0x7f0a01a6

    invoke-virtual {v0, v2, v1, v4}, Landroid/content/res/Resources;->getValue(ILandroid/util/TypedValue;Z)V

    .line 80
    invoke-virtual {v1}, Landroid/util/TypedValue;->getFloat()F

    move-result v2

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    .line 82
    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    .line 83
    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    .line 85
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mFlingAnimator:Landroid/animation/TimeAnimator;

    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;

    invoke-direct {v3, p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$1;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)V

    invoke-virtual {v2, v3}, Landroid/animation/TimeAnimator;->setTimeListener(Landroid/animation/TimeAnimator$TimeListener;)V

    .line 104
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Landroid/widget/Scroller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;
    .param p1, "x1"    # F

    .prologue
    .line 31
    iput p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->adjustPosition()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasks()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;)Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    return-object v0
.end method

.method private adjustPosition()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 339
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMinPos:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 340
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMinPos:F

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 346
    :goto_0
    return v0

    .line 342
    :cond_0
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMaxPos:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    .line 343
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMaxPos:F

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    goto :goto_0

    .line 346
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private computeCoords(Landroid/util/Pair;F)Landroid/graphics/PointF;
    .locals 9
    .param p2, "dist"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<[F[F>;F)",
            "Landroid/graphics/PointF;"
        }
    .end annotation

    .prologue
    .local p1, "vector":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 290
    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, [F

    aget v3, v3, v8

    sub-float v4, p2, v3

    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, [F

    aget v5, v3, v8

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, [F

    aget v3, v3, v8

    sub-float v3, v5, v3

    div-float v2, v4, v3

    .line 291
    .local v2, "z":F
    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, [F

    aget v4, v3, v6

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, [F

    aget v3, v3, v6

    sub-float v3, v4, v3

    mul-float v4, v2, v3

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, [F

    aget v3, v3, v6

    add-float v0, v4, v3

    .line 292
    .local v0, "x":F
    iget-object v3, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, [F

    aget v4, v3, v7

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, [F

    aget v3, v3, v7

    sub-float v3, v4, v3

    mul-float v4, v2, v3

    iget-object v3, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, [F

    aget v3, v3, v7

    add-float v1, v4, v3

    .line 294
    .local v1, "y":F
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v3
.end method

.method private computeVector(FF)Landroid/util/Pair;
    .locals 22
    .param p1, "x"    # F
    .param p2, "y"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair",
            "<[F[F>;"
        }
    .end annotation

    .prologue
    .line 274
    const/4 v1, 0x4

    new-array v10, v1, [F

    .line 275
    .local v10, "xyz":[F
    const/4 v1, 0x4

    new-array v8, v1, [I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v8, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput v2, v8, v1

    const/4 v1, 0x2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mWidth:I

    aput v2, v8, v1

    const/4 v1, 0x3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHeight:I

    aput v2, v8, v1

    .line 276
    .local v8, "viewport":[I
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHeight:I

    int-to-float v1, v1

    sub-float v2, v1, p2

    const/high16 v3, -0x40000000    # -2.0f

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mViewMatrix:[F

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mProjectionMatrix:[F

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v11, 0x0

    move/from16 v1, p1

    invoke-static/range {v1 .. v11}, Landroid/opengl/GLU;->gluUnProject(FFF[FI[FI[II[FI)I

    .line 277
    const/4 v1, 0x0

    const/4 v2, 0x0

    aget v2, v10, v2

    const/4 v3, 0x3

    aget v3, v10, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    aput v2, v10, v1

    .line 278
    const/4 v1, 0x1

    const/4 v2, 0x1

    aget v2, v10, v2

    const/4 v3, 0x3

    aget v3, v10, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    aput v2, v10, v1

    .line 279
    const/4 v1, 0x2

    const/4 v2, 0x2

    aget v2, v10, v2

    const/4 v3, 0x3

    aget v3, v10, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    aput v2, v10, v1

    .line 280
    const/4 v1, 0x4

    new-array v0, v1, [F

    move-object/from16 v20, v0

    .line 281
    .local v20, "xyz2":[F
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHeight:I

    int-to-float v1, v1

    sub-float v12, v1, p2

    const/high16 v13, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mViewMatrix:[F

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mProjectionMatrix:[F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v19, 0x0

    const/16 v21, 0x0

    move/from16 v11, p1

    move-object/from16 v18, v8

    invoke-static/range {v11 .. v21}, Landroid/opengl/GLU;->gluUnProject(FFF[FI[FI[II[FI)I

    .line 282
    const/4 v1, 0x0

    const/4 v2, 0x0

    aget v2, v20, v2

    const/4 v3, 0x3

    aget v3, v20, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    aput v2, v20, v1

    .line 283
    const/4 v1, 0x1

    const/4 v2, 0x1

    aget v2, v20, v2

    const/4 v3, 0x3

    aget v3, v20, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    aput v2, v20, v1

    .line 284
    const/4 v1, 0x2

    const/4 v2, 0x2

    aget v2, v20, v2

    const/4 v3, 0x3

    aget v3, v20, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    mul-float/2addr v2, v3

    aput v2, v20, v1

    .line 286
    new-instance v1, Landroid/util/Pair;

    move-object/from16 v0, v20

    invoke-direct {v1, v10, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1
.end method

.method private drawBackground()V
    .locals 7

    .prologue
    const/16 v2, 0x1406

    const/4 v6, 0x4

    const/4 v3, 0x0

    .line 234
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    invoke-virtual {v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getProgram()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 235
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const-string v1, "u_MVPMatrix"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgMatrix:[F

    invoke-static {v0, v1, v3, v4, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 236
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const-string v1, "a_Position"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x3

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgVerticesData:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 237
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const-string v1, "a_TexCoordinate"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x2

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgTextureVerticesData:Ljava/nio/FloatBuffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 238
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const-string v1, "a_Position"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 239
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const-string v1, "a_TexCoordinate"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 240
    const/16 v0, 0xde1

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgTextureHandle:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 241
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const-string v1, "a_Color"

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getLocation(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    new-array v1, v6, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1, v3}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    .line 242
    const/4 v0, 0x5

    invoke-static {v0, v3, v6}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 243
    return-void

    .line 241
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private layoutTasks()V
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasks(Z)V

    .line 420
    return-void
.end method

.method private layoutTasks(Z)V
    .locals 2
    .param p1, "force"    # Z

    .prologue
    .line 423
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v1

    .line 424
    :try_start_0
    invoke-direct {p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasksLocked(Z)V

    .line 425
    monitor-exit v1

    .line 426
    return-void

    .line 425
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private layoutTasksLocked()V
    .locals 1

    .prologue
    .line 429
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasksLocked(Z)V

    .line 430
    return-void
.end method

.method private layoutTasksLocked(Z)V
    .locals 6
    .param p1, "force"    # Z

    .prologue
    .line 433
    iget-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mChildRemoved:Z

    if-eqz v3, :cond_2

    .line 435
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_0

    .line 455
    :goto_0
    return-void

    .line 438
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    iget v3, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMinPos:F

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    .line 439
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 441
    :cond_1
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mChildRemoved:Z

    .line 443
    :cond_2
    const-string v3, "MIK"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "--------------------- layoutTasksLocked begin: mScrollPosition = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 445
    .local v1, "nextChildPosition":F
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_4

    .line 446
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 447
    .local v2, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    iget-boolean v3, v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIsDisappearing:Z

    if-eqz v3, :cond_3

    .line 445
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 450
    :cond_3
    invoke-virtual {v2, v1, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->layout(FZ)V

    .line 451
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    sub-float/2addr v1, v3

    goto :goto_2

    .line 453
    .end local v2    # "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    :cond_4
    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    sub-float/2addr v3, v1

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mMaxPos:F

    .line 454
    const-string v3, "MIK"

    const-string v4, "--------------------- layoutTasksLocked end"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addTask(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
    .locals 2
    .param p1, "task"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .prologue
    .line 402
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v1

    .line 403
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasksLocked()V

    .line 405
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 407
    return-void

    .line 405
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public clearTaskList()V
    .locals 4

    .prologue
    .line 410
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v3

    .line 411
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 412
    .local v1, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    invoke-virtual {p0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->requestTaskCleanUp(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    goto :goto_0

    .line 415
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 414
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 415
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 416
    return-void
.end method

.method public getSmoothingSpeed()F
    .locals 1

    .prologue
    .line 549
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    return v0
.end method

.method public getTaskForDescription(Lcom/sec/android/app/FlashBarService/recent/TaskDescription;)Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    .locals 5
    .param p1, "td"    # Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    .prologue
    .line 554
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v4

    .line 555
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    .line 556
    .local v2, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;>;"
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 557
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 558
    .local v1, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    invoke-virtual {v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getTaskDescription()Lcom/sec/android/app/FlashBarService/recent/TaskDescription;

    move-result-object v3

    if-ne v3, p1, :cond_0

    .line 562
    .end local v1    # "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    :goto_0
    return-object v1

    .line 556
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;>;"
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 562
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;>;"
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getZoom()F
    .locals 1

    .prologue
    .line 545
    const/high16 v0, 0x41200000    # 10.0f

    return v0
.end method

.method public isSmoothing()Z
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 247
    const-string v4, "MIK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDown, mScroller.isFinished() = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v6}, Landroid/widget/Scroller;->isFinished()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->isFinished()Z

    move-result v4

    if-nez v4, :cond_1

    .line 249
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4, v7}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 270
    :cond_0
    :goto_0
    return v7

    .line 252
    :cond_1
    iput-boolean v7, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    .line 257
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->computeVector(FF)Landroid/util/Pair;

    move-result-object v3

    .line 259
    .local v3, "vector":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_0

    .line 260
    iget-object v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 262
    .local v2, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    iget v4, v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    invoke-direct {p0, v3, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->computeCoords(Landroid/util/Pair;F)Landroid/graphics/PointF;

    move-result-object v1

    .line 264
    .local v1, "p":Landroid/graphics/PointF;
    iget v4, v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    const v6, 0x38d1b717    # 1.0E-4f

    sub-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_2

    iget v4, v1, Landroid/graphics/PointF;->x:F

    iget v5, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v8, v4, v5, v8}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->onMotionEvent(IFFZ)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 265
    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    goto :goto_0

    .line 259
    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 9
    .param p1, "gl10"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/high16 v8, 0x40400000    # 3.0f

    const/4 v6, 0x0

    .line 198
    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v6, v6, v6, v5}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 200
    const/16 v5, 0x4100

    invoke-static {v5}, Landroid/opengl/GLES20;->glClear(I)V

    .line 202
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->drawBackground()V

    .line 204
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    monitor-enter v6

    .line 205
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Runnable;

    .line 206
    .local v1, "r":Ljava/lang/Runnable;
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 209
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "r":Ljava/lang/Runnable;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 208
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 209
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 212
    const/4 v2, 0x0

    .line 213
    .local v2, "smoothing":Z
    iget-object v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v6

    .line 214
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 216
    .local v4, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 217
    .local v3, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    iget-object v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mViewMatrix:[F

    iget-object v7, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mProjectionMatrix:[F

    invoke-virtual {v3, v5, v7}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->draw([F[F)V

    .line 218
    iget v5, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    iget v7, v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    sub-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v7, 0x38d1b717    # 1.0E-4f

    cmpl-float v5, v5, v7

    if-lez v5, :cond_1

    .line 219
    const/4 v2, 0x1

    goto :goto_1

    .line 222
    .end local v3    # "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    :cond_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 223
    if-nez v2, :cond_4

    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    cmpl-float v5, v5, v8

    if-eqz v5, :cond_4

    .line 224
    iput v8, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    .line 225
    const-string v5, "MIK"

    const-string v6, "Smoothing ended!"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_3
    :goto_2
    return-void

    .line 222
    .end local v4    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;>;"
    :catchall_1
    move-exception v5

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    .line 226
    .restart local v4    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;>;"
    :cond_4
    iget v5, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    cmpl-float v5, v5, v8

    if-eqz v5, :cond_3

    .line 227
    const-string v5, "MIK"

    const-string v6, "Smoothing!"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 13
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;
    .param p2, "motionEvent2"    # Landroid/view/MotionEvent;
    .param p3, "v"    # F
    .param p4, "v2"    # F

    .prologue
    .line 361
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    const/4 v3, 0x0

    move/from16 v0, p4

    float-to-int v4, v0

    const/4 v5, 0x0

    const v6, -0x186a0

    const v7, 0x186a0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v1 .. v9}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 363
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getFinalX()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    div-float v12, v1, v2

    .line 364
    .local v12, "fx":F
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    rem-float/2addr v2, v3

    sub-float v11, v1, v2

    .line 365
    .local v11, "fixedScrollPosition":F
    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFling to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    rem-float v10, v12, v1

    .line 367
    .local v10, "diff":F
    sub-float v1, v11, v12

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 368
    const/4 v1, 0x0

    cmpl-float v1, p4, v1

    if-lez v1, :cond_2

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_2

    .line 369
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    add-float v12, v11, v1

    .line 375
    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    mul-float/2addr v2, v12

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 376
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    const/16 v2, 0x14d

    invoke-virtual {v1, v2}, Landroid/widget/Scroller;->extendDuration(I)V

    .line 377
    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFling 1 fixed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", v2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :cond_0
    :goto_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    .line 391
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    if-eqz v1, :cond_1

    .line 392
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->onMotionEvent(I)V

    .line 393
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 396
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mFlingAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v1}, Landroid/animation/TimeAnimator;->start()V

    .line 397
    const-string v1, "MIK"

    const-string v2, "mFlingAnimator started"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    const/4 v1, 0x1

    return v1

    .line 370
    :cond_2
    const/4 v1, 0x0

    cmpg-float v1, p4, v1

    if-gez v1, :cond_3

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    .line 371
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    sub-float v12, v11, v1

    goto :goto_0

    .line 373
    :cond_3
    move v12, v11

    goto :goto_0

    .line 378
    :cond_4
    const/4 v1, 0x0

    cmpl-float v1, v10, v1

    if-eqz v1, :cond_0

    .line 379
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_5

    const/4 v1, 0x0

    cmpl-float v1, v10, v1

    if-lez v1, :cond_5

    .line 380
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    sub-float/2addr v1, v10

    add-float/2addr v12, v1

    .line 386
    :goto_2
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    mul-float/2addr v2, v12

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 387
    const-string v1, "MIK"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFling 2 fixed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 381
    :cond_5
    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    const/4 v1, 0x0

    cmpg-float v1, v10, v1

    if-gez v1, :cond_6

    .line 382
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    add-float/2addr v1, v10

    sub-float/2addr v12, v1

    goto :goto_2

    .line 384
    :cond_6
    sub-float/2addr v12, v10

    goto :goto_2
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 351
    const-string v0, "MIK"

    const-string v1, "onLongPress"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    .line 353
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->onMotionEvent(I)V

    .line 355
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 357
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;
    .param p2, "motionEvent2"    # Landroid/view/MotionEvent;
    .param p3, "v"    # F
    .param p4, "v2"    # F

    .prologue
    .line 322
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 324
    .local v0, "oldPos":F
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    div-float v2, p4, v2

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 325
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->adjustPosition()Z

    .line 326
    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    cmpl-float v1, v1, v0

    if-eqz v1, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasks()V

    .line 329
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    if-eqz v1, :cond_1

    .line 330
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->onMotionEvent(I)V

    .line 331
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 334
    :cond_1
    const/high16 v1, 0x40400000    # 3.0f

    iput v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    .line 335
    const/4 v1, 0x1

    return v1
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 300
    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 304
    const-string v4, "MIK"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onSingleTapUp mTapped = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-boolean v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    iget-boolean v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    if-nez v4, :cond_0

    .line 316
    :goto_0
    return v2

    .line 308
    :cond_0
    iput-boolean v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTapped:Z

    .line 309
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    if-eqz v2, :cond_1

    .line 310
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v2, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->computeVector(FF)Landroid/util/Pair;

    move-result-object v1

    .line 311
    .local v1, "vector":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    iget v2, v2, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    invoke-direct {p0, v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->computeCoords(Landroid/util/Pair;F)Landroid/graphics/PointF;

    move-result-object v0

    .line 313
    .local v0, "p":Landroid/graphics/PointF;
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v2, v3, v4, v5, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->onMotionEvent(IFFZ)Z

    .line 314
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTappedTask:Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .end local v0    # "p":Landroid/graphics/PointF;
    .end local v1    # "vector":Landroid/util/Pair;, "Landroid/util/Pair<[F[F>;"
    :cond_1
    move v2, v3

    .line 316
    goto :goto_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 24
    .param p1, "gl10"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 145
    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v2, v3, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 149
    move/from16 v0, p2

    int-to-float v2, v0

    move/from16 v0, p3

    int-to-float v3, v0

    div-float v21, v2, v3

    .line 150
    .local v21, "ratio":F
    const/high16 v19, -0x40800000    # -1.0f

    .line 151
    .local v19, "left":F
    const/high16 v22, 0x3f800000    # 1.0f

    .line 152
    .local v22, "right":F
    const/high16 v2, -0x40800000    # -1.0f

    div-float v6, v2, v21

    .line 153
    .local v6, "bottom":F
    neg-float v7, v6

    .line 154
    .local v7, "top":F
    const/high16 v20, 0x3f800000    # 1.0f

    .line 155
    .local v20, "near":F
    const/high16 v17, 0x41f00000    # 30.0f

    .line 157
    .local v17, "far":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mProjectionMatrix:[F

    const/4 v3, 0x0

    const/high16 v4, -0x40800000    # -1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    const/high16 v9, 0x41f00000    # 30.0f

    invoke-static/range {v2 .. v9}, Landroid/opengl/Matrix;->frustumM([FIFFFFFF)V

    .line 159
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgMatrix:[F

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mProjectionMatrix:[F

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgMatrix:[F

    const/4 v13, 0x0

    invoke-static/range {v8 .. v13}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 161
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mWidth:I

    .line 162
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHeight:I

    .line 164
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v2, 0x40000000    # 2.0f

    mul-float v11, v7, v2

    move/from16 v12, p2

    move/from16 v13, p3

    invoke-static/range {v8 .. v13}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->initGL(Landroid/content/Context;Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;FFII)V

    .line 165
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasks(Z)V

    .line 166
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v3

    .line 167
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a01aa

    invoke-static {v2, v4}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->getFloatFromResource(Landroid/content/res/Resources;I)F

    move-result v14

    .line 169
    .local v14, "back":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v18

    .local v18, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface/range {v18 .. v18}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .line 170
    .local v23, "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    move-object/from16 v0, v23

    iget v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->position:F

    cmpg-float v2, v2, v14

    if-gez v2, :cond_0

    .line 171
    const/4 v2, 0x0

    move-object/from16 v0, v23

    iput v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->drawPosition:F

    goto :goto_0

    .line 176
    .end local v14    # "back":F
    .end local v18    # "i$":Ljava/util/Iterator;
    .end local v23    # "t":Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v14    # "back":F
    .restart local v18    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 178
    const-string v2, "MIK"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "left = -1.0, right = 1.0, bottom = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", top = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", near = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", far = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", width = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    const/16 v2, 0xc

    new-array v0, v2, [F

    move-object/from16 v16, v0

    const/4 v2, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, v16, v2

    const/4 v2, 0x1

    aput v7, v16, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput v3, v16, v2

    const/4 v2, 0x3

    const/high16 v3, -0x40800000    # -1.0f

    aput v3, v16, v2

    const/4 v2, 0x4

    aput v6, v16, v2

    const/4 v2, 0x5

    const/4 v3, 0x0

    aput v3, v16, v2

    const/4 v2, 0x6

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v16, v2

    const/4 v2, 0x7

    aput v7, v16, v2

    const/16 v2, 0x8

    const/4 v3, 0x0

    aput v3, v16, v2

    const/16 v2, 0x9

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v16, v2

    const/16 v2, 0xa

    aput v6, v16, v2

    const/16 v2, 0xb

    const/4 v3, 0x0

    aput v3, v16, v2

    .line 186
    .local v16, "bgVertices":[F
    const/16 v2, 0x8

    new-array v15, v2, [F

    fill-array-data v15, :array_0

    .line 192
    .local v15, "bgTextureVertices":[F
    invoke-static/range {v16 .. v16}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgVerticesData:Ljava/nio/FloatBuffer;

    .line 193
    invoke-static {v15}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadToBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgTextureVerticesData:Ljava/nio/FloatBuffer;

    .line 194
    return-void

    .line 186
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 20
    .param p1, "gl10"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "eglConfig"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 108
    const/16 v1, 0xbe2

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 109
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 111
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mHelper:Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->init(Landroid/content/Context;)V

    .line 114
    const/4 v12, 0x0

    .line 115
    .local v12, "eyeX":F
    const/high16 v13, 0x3f800000    # 1.0f

    .line 118
    .local v13, "eyeZ":F
    const/4 v14, 0x0

    .line 119
    .local v14, "lookX":F
    const/4 v15, 0x0

    .line 120
    .local v15, "lookY":F
    const/high16 v16, -0x3f600000    # -5.0f

    .line 123
    .local v16, "lookZ":F
    const/16 v17, 0x0

    .line 124
    .local v17, "upX":F
    const/high16 v18, 0x3f800000    # 1.0f

    .line 125
    .local v18, "upY":F
    const/16 v19, 0x0

    .line 128
    .local v19, "upZ":F
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mViewMatrix:[F

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->eyeY:F

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, -0x3f600000    # -5.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 129
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mViewMatrix:[F

    const/4 v2, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v1, v2, v3, v4, v5}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 132
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgMatrix:[F

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/high16 v8, -0x3f600000    # -5.0f

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    invoke-static/range {v1 .. v11}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    .line 133
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgMatrix:[F

    const/4 v2, 0x0

    const/high16 v3, 0x41200000    # 10.0f

    const/high16 v4, 0x41200000    # 10.0f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v1, v2, v3, v4, v5}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 135
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mContext:Landroid/content/Context;

    const v2, 0x7f0200ae

    invoke-static {v1, v2}, Lcom/sec/android/app/FlashBarService/recent/RecentsHelper;->loadTexture(Landroid/content/Context;I)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mBgTextureHandle:I

    .line 137
    const/high16 v1, 0x41900000    # 18.0f

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    .line 139
    const-string v1, "MIK"

    const-string v2, "onSurfaceCreated"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    return-void
.end method

.method public onTaskDismissed(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
    .locals 8
    .param p1, "t"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 487
    iput-boolean v6, p1, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->mIsDisappearing:Z

    .line 488
    iput-boolean v6, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mChildRemoved:Z

    .line 490
    const/high16 v3, 0x41900000    # 18.0f

    iput v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mSmoothingSpeed:F

    .line 492
    invoke-direct {p0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->layoutTasks()V

    .line 494
    const-string v3, "alpha"

    new-array v4, v6, [F

    const/4 v5, 0x0

    aput v5, v4, v7

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 495
    .local v0, "a":Landroid/animation/ObjectAnimator;
    const-wide/16 v4, 0xa7

    invoke-virtual {v0, v4, v5}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 496
    new-instance v3, Landroid/view/animation/interpolator/SineEaseInOut;

    invoke-direct {v3}, Landroid/view/animation/interpolator/SineEaseInOut;-><init>()V

    invoke-virtual {v0, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 497
    const-string v3, "scale"

    new-array v4, v6, [F

    const v5, 0x3f7851ec    # 0.97f

    aput v5, v4, v7

    invoke-static {p1, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 498
    .local v1, "s":Landroid/animation/ObjectAnimator;
    new-instance v3, Landroid/view/animation/interpolator/CubicEaseOut;

    invoke-direct {v3}, Landroid/view/animation/interpolator/CubicEaseOut;-><init>()V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 500
    const-string v3, "MIK"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "alpha="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getAlpha()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", scale="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getScale()F

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getAlpha()F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->setAlpha(F)V

    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getScale()F

    move-result v3

    invoke-virtual {p1, v3}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->setScale(F)V

    .line 504
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 505
    .local v2, "set":Landroid/animation/AnimatorSet;
    invoke-virtual {v2, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 506
    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$2;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 531
    const-wide/16 v4, 0x29b

    invoke-virtual {v2, v4, v5}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 532
    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->start()V

    .line 533
    return-void
.end method

.method public onTaskLaunched(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
    .locals 2
    .param p1, "t"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .prologue
    .line 536
    const-string v0, "MIK"

    const-string v1, "RecentsGLTask launched!"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 537
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    invoke-interface {v0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;->handleOnClick(Ljava/lang/Object;)V

    .line 538
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const-wide v10, 0x3fe4cccccccccccdL    # 0.65

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 458
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 483
    :cond_0
    :goto_0
    return v2

    .line 462
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 465
    :pswitch_1
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    rem-float v6, v0, v1

    .line 466
    .local v6, "diff":F
    iget v7, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    .line 467
    .local v7, "fx":F
    cmpl-float v0, v6, v8

    if-eqz v0, :cond_0

    .line 470
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    float-to-double v4, v3

    mul-double/2addr v4, v10

    cmpl-double v0, v0, v4

    if-lez v0, :cond_2

    cmpl-float v0, v6, v8

    if-lez v0, :cond_2

    .line 471
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    sub-float/2addr v0, v6

    add-float/2addr v7, v0

    .line 477
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    mul-float/2addr v1, v3

    float-to-int v1, v1

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollPosition:F

    sub-float v3, v7, v3

    iget v4, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mScrollMultiplier:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    const/16 v5, 0x14d

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 480
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mFlingAnimator:Landroid/animation/TimeAnimator;

    invoke-virtual {v0}, Landroid/animation/TimeAnimator;->start()V

    goto :goto_0

    .line 472
    :cond_2
    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    iget v3, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    float-to-double v4, v3

    mul-double/2addr v4, v10

    cmpl-double v0, v0, v4

    if-lez v0, :cond_3

    cmpg-float v0, v6, v8

    if-gez v0, :cond_3

    .line 473
    iget v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mDiff:F

    add-float/2addr v0, v6

    sub-float/2addr v7, v0

    goto :goto_1

    .line 475
    :cond_3
    sub-float/2addr v7, v6

    goto :goto_1

    .line 462
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public requestTaskCleanUp(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
    .locals 4
    .param p1, "t"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .prologue
    .line 586
    const-string v1, "MIK"

    const-string v2, "requestTaskCleanUp"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    invoke-virtual {p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;->getLocalTextures()[I

    move-result-object v0

    .line 588
    .local v0, "textures":[I
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    monitor-enter v2

    .line 589
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$4;

    invoke-direct {v3, p0, v0}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$4;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;[I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    monitor-exit v2

    .line 596
    return-void

    .line 595
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public requestTaskInit(Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V
    .locals 4
    .param p1, "t"    # Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;

    .prologue
    .line 570
    const-string v0, "MIK"

    const-string v1, "requestTaskInit"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    iget-object v1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    monitor-enter v1

    .line 572
    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 573
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 574
    :try_start_1
    iget-object v0, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mGLActionsQueue:Ljava/util/ArrayList;

    new-instance v3, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$3;

    invoke-direct {v3, p0, p1}, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer$3;-><init>(Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;Lcom/sec/android/app/FlashBarService/recent/RecentsGLTask;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 582
    :cond_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 583
    return-void

    .line 580
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    .line 582
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public setCallback(Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    .prologue
    .line 566
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/recent/RecentsGLRenderer;->mCallback:Lcom/sec/android/app/FlashBarService/recent/RecentsCallback;

    .line 567
    return-void
.end method
