.class Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;
.super Landroid/content/BroadcastReceiver;
.source "AppListGridWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/FlashBarService/AppListGridWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;


# direct methods
.method constructor <init>(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 455
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 457
    .local v0, "action":Ljava/lang/String;
    const-string v2, "ResponseAxT9Info"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 458
    const-string v2, "AxT9IME.isVisibleWindow"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 460
    .local v1, "isInputMethodShown":Z
    const-string v2, "AxT9IME.isMovableKeypad"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 473
    .end local v1    # "isInputMethodShown":Z
    :cond_0
    :goto_0
    return-void

    .line 463
    .restart local v1    # "isInputMethodShown":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    invoke-virtual {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->getFlashBarEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 466
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mIsInputMethodShown:Z
    invoke-static {v2, v1}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$002(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 467
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    const/4 v3, 0x1

    # setter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mInputMethodChanged:Z
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$102(Lcom/sec/android/app/FlashBarService/AppListGridWindow;Z)Z

    .line 468
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    const/16 v3, 0x67

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mAppListPosition:I
    invoke-static {v2}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$200(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v2

    const/16 v3, 0x68

    if-ne v2, v3, :cond_0

    .line 469
    :cond_2
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v3, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    # getter for: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mPositionY:I
    invoke-static {v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$300(Lcom/sec/android/app/FlashBarService/AppListGridWindow;)I

    move-result v3

    # invokes: Lcom/sec/android/app/FlashBarService/AppListGridWindow;->setAppListHandlePosition(I)V
    invoke-static {v2, v3}, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->access$400(Lcom/sec/android/app/FlashBarService/AppListGridWindow;I)V

    .line 470
    iget-object v2, p0, Lcom/sec/android/app/FlashBarService/AppListGridWindow$1;->this$0:Lcom/sec/android/app/FlashBarService/AppListGridWindow;

    iget-object v2, v2, Lcom/sec/android/app/FlashBarService/AppListGridWindow;->mTimerHandler:Landroid/os/Handler;

    const/16 v3, 0xce

    const-wide/16 v4, 0xc8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
