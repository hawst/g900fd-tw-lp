.class public Lcom/visionobjects/resourcemanager/InitResourceManagerService;
.super Ljava/lang/Object;
.source "InitResourceManagerService.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Initialize(Landroid/content/res/Resources;)V
    .locals 5
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 10
    sget v4, Lcom/visionobjects/resourcemanager/R$string;->vo_rm_authority:I

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 11
    .local v0, "authority":Ljava/lang/String;
    const-string v4, "com.visionobjects.resourcemanager."

    invoke-static {v4}, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->definePrefix(Ljava/lang/String;)V

    .line 13
    if-eqz v0, :cond_0

    const-string v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 15
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/ResourceManagerConfig;->setAuthority(Ljava/lang/String;)V

    .line 18
    :cond_0
    sget v4, Lcom/visionobjects/resourcemanager/R$string;->vo_rm_local_path:I

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 19
    .local v1, "localPath":Ljava/lang/String;
    if-eqz v1, :cond_1

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 20
    invoke-static {v1}, Lcom/visionobjects/resourcemanager/ResourceManagerConfig;->setLocalPath(Ljava/lang/String;)V

    .line 22
    :cond_1
    sget v4, Lcom/visionobjects/resourcemanager/R$string;->vo_rm_preload_path:I

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 23
    .local v2, "preloadPath":Ljava/lang/String;
    if-eqz v2, :cond_2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 24
    invoke-static {v2}, Lcom/visionobjects/resourcemanager/ResourceManagerConfig;->setPreloadPath(Ljava/lang/String;)V

    .line 26
    :cond_2
    sget v4, Lcom/visionobjects/resourcemanager/R$string;->vo_rm_server:I

    invoke-virtual {p0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 27
    .local v3, "serverUrl":Ljava/lang/String;
    if-eqz v3, :cond_3

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 28
    invoke-static {v3}, Lcom/visionobjects/resourcemanager/ResourceManagerConfig;->setServer(Ljava/lang/String;)V

    .line 30
    :cond_3
    const/4 v4, 0x1

    invoke-static {v4}, Lcom/visionobjects/resourcemanager/ResourceManagerConfig;->enablePreloadPathUsage(Z)V

    .line 31
    const/4 v4, 0x0

    invoke-static {v4}, Lcom/visionobjects/resourcemanager/ResourceManagerConfig;->enableLastAvailableVersion(Z)V

    .line 32
    return-void
.end method
