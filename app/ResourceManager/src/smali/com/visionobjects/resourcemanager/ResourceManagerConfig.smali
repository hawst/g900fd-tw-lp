.class public final Lcom/visionobjects/resourcemanager/ResourceManagerConfig;
.super Ljava/lang/Object;
.source "ResourceManagerConfig.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final enableLastAvailableVersion(Z)V
    .locals 0
    .param p0, "enable"    # Z

    .prologue
    .line 52
    sput-boolean p0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUseLastAvailableVersion:Z

    .line 53
    return-void
.end method

.method public static final enableLibFolderUsage(Z)V
    .locals 0
    .param p0, "enable"    # Z

    .prologue
    .line 61
    sput-boolean p0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUseLibFolder:Z

    .line 62
    return-void
.end method

.method public static final enablePreloadPathUsage(Z)V
    .locals 0
    .param p0, "enable"    # Z

    .prologue
    .line 43
    sput-boolean p0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUsePreloadPath:Z

    .line 44
    return-void
.end method

.method public static final setAuthority(Ljava/lang/String;)V
    .locals 0
    .param p0, "authority"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/ResourceManagerContract;->setAuthority(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method public static final setLocalPath(Ljava/lang/String;)V
    .locals 0
    .param p0, "localPath"    # Ljava/lang/String;

    .prologue
    .line 23
    sput-object p0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sLocalPath:Ljava/lang/String;

    .line 24
    return-void
.end method

.method public static final setPreloadPath(Ljava/lang/String;)V
    .locals 1
    .param p0, "preloadPath"    # Ljava/lang/String;

    .prologue
    .line 33
    const/4 v0, 0x1

    sput-boolean v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUsePreloadPath:Z

    .line 34
    sput-object p0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sPreloadPath:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static final setServer(Ljava/lang/String;)V
    .locals 0
    .param p0, "ServerURL"    # Ljava/lang/String;

    .prologue
    .line 87
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/model/ServerConfig;->setServerUrl(Ljava/lang/String;)V

    .line 88
    return-void
.end method
