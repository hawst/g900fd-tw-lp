.class public Lcom/visionobjects/resourcemanager/LangCacheEntry;
.super Ljava/lang/Object;
.source "LangCacheEntry.java"


# instance fields
.field private mConfig:Lcom/visionobjects/resourcemanager/model/LangConfig;

.field private mLang:Ljava/lang/String;

.field public path:Ljava/lang/String;

.field public preloaded:Z

.field public version:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1
    .param p1, "version"    # Ljava/lang/String;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "preloaded"    # Z
    .param p4, "lang"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->version:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->path:Ljava/lang/String;

    .line 18
    iput-boolean p3, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->preloaded:Z

    .line 20
    iput-object p4, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mLang:Ljava/lang/String;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mConfig:Lcom/visionobjects/resourcemanager/model/LangConfig;

    .line 22
    return-void
.end method


# virtual methods
.method public getLangConfig()Lcom/visionobjects/resourcemanager/model/LangConfig;
    .locals 4

    .prologue
    .line 25
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mConfig:Lcom/visionobjects/resourcemanager/model/LangConfig;

    if-nez v1, :cond_0

    .line 29
    :try_start_0
    new-instance v1, Lcom/visionobjects/resourcemanager/model/LangConfig;

    iget-object v2, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mLang:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/visionobjects/resourcemanager/model/LangConfig;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mConfig:Lcom/visionobjects/resourcemanager/model/LangConfig;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :cond_0
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mConfig:Lcom/visionobjects/resourcemanager/model/LangConfig;

    return-object v1

    .line 31
    :catch_0
    move-exception v0

    .line 33
    .local v0, "e":Ljava/io/IOException;
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Lang file for language "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/visionobjects/resourcemanager/LangCacheEntry;->mLang:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is corrupted!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
