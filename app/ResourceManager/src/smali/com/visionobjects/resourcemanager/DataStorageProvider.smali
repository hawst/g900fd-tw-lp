.class public Lcom/visionobjects/resourcemanager/DataStorageProvider;
.super Landroid/content/ContentProvider;
.source "DataStorageProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;
    }
.end annotation


# static fields
.field private static CacheEngineNeedToBeRebuild:Z = false

.field private static CacheLangNeedToBeRebuild:Z = false

.field private static final DBG:Z = false

.field private static final LANGUAGES:I = 0x0

.field private static final LANGUAGES_DOWNLOADING:I = 0x6

.field private static final LANGUAGES_UPDATE:I = 0x5

.field private static final LANGUAGE_MODE:I = 0x3

.field private static final LANGUAGE_MODE_RESOURCES:I = 0x4

.field private static final RECOGNITION_ENGINE:I = 0x2

.field private static final RECOGNITION_ENGINES:I = 0x1

.field private static final REFRESH:I = 0x8

.field private static final TAG:Ljava/lang/String;

.field private static final VERSION_INFO:I = 0x7

.field private static sUriMatcher:Landroid/content/UriMatcher;


# instance fields
.field private final mEngineCache:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;",
            ">;"
        }
    .end annotation
.end field

.field private final mLangCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/visionobjects/resourcemanager/LangCacheEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->TAG:Ljava/lang/String;

    .line 96
    const/4 v0, 0x0

    sput-object v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->sUriMatcher:Landroid/content/UriMatcher;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 77
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    .line 102
    return-void
.end method

.method private buildEngineCache()V
    .locals 9

    .prologue
    .line 253
    new-instance v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    .line 254
    .local v0, "fileSystemHelper":Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getLibs()Ljava/util/HashMap;

    move-result-object v2

    .line 255
    .local v2, "libs":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 257
    .local v4, "versions":Ljava/util/Set;, "Ljava/util/Set<Lcom/visionobjects/resourcemanager/util/Version;>;"
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 259
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/visionobjects/resourcemanager/util/Version;

    .line 261
    .local v3, "version":Lcom/visionobjects/resourcemanager/util/Version;
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    new-instance v7, Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;

    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/util/Version;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct {v7, p0, v8, v5}, Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;-><init>(Lcom/visionobjects/resourcemanager/DataStorageProvider;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 263
    .end local v3    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_0
    return-void
.end method

.method private buildEnginesCursor()Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 267
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v6

    const-string v5, "version"

    aput-object v5, v4, v7

    const-string v5, "path"

    aput-object v5, v4, v8

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 269
    .local v0, "cursor":Landroid/database/MatrixCursor;
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    monitor-enter v5

    .line 271
    :try_start_0
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    sget-boolean v4, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheEngineNeedToBeRebuild:Z

    if-eqz v4, :cond_1

    .line 273
    :cond_0
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildEngineCache()V

    .line 274
    const/4 v4, 0x0

    sput-boolean v4, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheEngineNeedToBeRebuild:Z

    .line 277
    :cond_1
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 278
    .local v3, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 280
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mEngineCache:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;

    .line 281
    .local v1, "entry":Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;
    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v6

    const/4 v6, 0x1

    iget-object v7, v1, Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;->version:Ljava/lang/String;

    aput-object v7, v4, v6

    const/4 v6, 0x2

    iget-object v7, v1, Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;->path:Ljava/lang/String;

    aput-object v7, v4, v6

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 278
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 283
    .end local v1    # "entry":Lcom/visionobjects/resourcemanager/DataStorageProvider$EngineCacheEntry;
    :cond_2
    monitor-exit v5

    .line 285
    return-object v0

    .line 283
    .end local v2    # "i":I
    .end local v3    # "size":I
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method private buildLangCache()V
    .locals 10

    .prologue
    .line 295
    new-instance v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v0, v7}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    .line 296
    .local v0, "fileSystemHelper":Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getLangs()Ljava/util/Set;

    move-result-object v3

    .line 298
    .local v3, "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 300
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 302
    .local v2, "lang":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersion(Ljava/lang/String;)Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v6

    .line 304
    .local v6, "version":Lcom/visionobjects/resourcemanager/util/Version;
    invoke-virtual {v0, v2}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 305
    .local v4, "path":Ljava/lang/String;
    if-nez v4, :cond_0

    .line 307
    sget-object v7, Lcom/visionobjects/resourcemanager/DataStorageProvider;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Language "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is not installed!"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 311
    :cond_0
    const/4 v5, 0x0

    .line 312
    .local v5, "preloaded":Z
    if-eqz v4, :cond_1

    .line 314
    invoke-static {v4}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->isPreloadedPath(Ljava/lang/String;)Z

    move-result v5

    .line 317
    :cond_1
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    new-instance v8, Lcom/visionobjects/resourcemanager/LangCacheEntry;

    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/util/Version;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9, v4, v5, v2}, Lcom/visionobjects/resourcemanager/LangCacheEntry;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-virtual {v7, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 319
    .end local v2    # "lang":Ljava/lang/String;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "preloaded":Z
    .end local v6    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_2
    return-void
.end method

.method private buildLangModeResourcesCursor(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 19
    .param p1, "lang"    # Ljava/lang/String;
    .param p2, "mode"    # Ljava/lang/String;

    .prologue
    .line 397
    new-instance v1, Landroid/database/MatrixCursor;

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string v17, "_id"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    const-string v17, "resource"

    aput-object v17, v15, v16

    invoke-direct {v1, v15}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 399
    .local v1, "cursor":Landroid/database/MatrixCursor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    move-object/from16 v16, v0

    monitor-enter v16

    .line 401
    :try_start_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v15}, Ljava/util/HashMap;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_0

    sget-boolean v15, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheLangNeedToBeRebuild:Z

    if-eqz v15, :cond_1

    .line 403
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLangCache()V

    .line 404
    const/4 v15, 0x0

    sput-boolean v15, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheLangNeedToBeRebuild:Z

    .line 407
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 409
    const-string v15, "langfile"

    move-object/from16 v0, p2

    invoke-virtual {v0, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 412
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/visionobjects/resourcemanager/LangCacheEntry;

    iget-object v13, v15, Lcom/visionobjects/resourcemanager/LangCacheEntry;->path:Ljava/lang/String;

    .line 413
    .local v13, "path":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v17, ".lang"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 414
    .local v14, "voimConfigFile":Ljava/lang/String;
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v15, v17

    const/16 v17, 0x1

    aput-object v14, v15, v17

    invoke-virtual {v1, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 452
    .end local v13    # "path":Ljava/lang/String;
    .end local v14    # "voimConfigFile":Ljava/lang/String;
    :cond_2
    monitor-exit v16

    .line 454
    return-object v1

    .line 420
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/visionobjects/resourcemanager/LangCacheEntry;

    invoke-virtual {v15}, Lcom/visionobjects/resourcemanager/LangCacheEntry;->getLangConfig()Lcom/visionobjects/resourcemanager/model/LangConfig;

    move-result-object v10

    .line 421
    .local v10, "langConfig":Lcom/visionobjects/resourcemanager/model/LangConfig;
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/model/LangConfig;->getLangModes()Ljava/util/List;

    move-result-object v12

    .line 422
    .local v12, "langModes":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;>;"
    const/4 v6, 0x0

    .line 423
    .local v6, "files":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    .line 425
    .local v11, "langMode":Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;
    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->getRefModeName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p2

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 427
    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->resourceList()Ljava/util/ArrayList;

    move-result-object v6

    .line 431
    .end local v11    # "langMode":Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;
    :cond_5
    if-eqz v6, :cond_2

    .line 433
    const/4 v7, 0x0

    .line 434
    .local v7, "i":I
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/model/LangConfig;->getResourceDir()Ljava/util/List;

    move-result-object v3

    .line 435
    .local v3, "dirs":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .end local v8    # "i$":Ljava/util/Iterator;
    :cond_6
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;

    .line 437
    .local v5, "file":Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_7
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;

    .line 439
    .local v2, "dir":Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-interface {v2}, Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/visionobjects/resourcemanager/LangCacheEntry;

    iget-object v13, v15, Lcom/visionobjects/resourcemanager/LangCacheEntry;->path:Ljava/lang/String;

    .line 440
    .restart local v13    # "path":Ljava/lang/String;
    new-instance v4, Ljava/io/File;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v17, Ljava/io/File;->separator:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-interface {v5}, Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;->name()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v4, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 441
    .local v4, "f":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v15

    if-eqz v15, :cond_7

    .line 443
    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v17, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v15, v17

    const/16 v17, 0x1

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    invoke-virtual {v1, v15}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 444
    add-int/lit8 v7, v7, 0x1

    .line 445
    goto :goto_0

    .line 452
    .end local v2    # "dir":Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;
    .end local v3    # "dirs":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;>;"
    .end local v4    # "f":Ljava/io/File;
    .end local v5    # "file":Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;
    .end local v6    # "files":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;>;"
    .end local v7    # "i":I
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "langConfig":Lcom/visionobjects/resourcemanager/model/LangConfig;
    .end local v12    # "langModes":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;>;"
    .end local v13    # "path":Ljava/lang/String;
    :catchall_0
    move-exception v15

    monitor-exit v16
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v15
.end method

.method private buildLangModesCursor(Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 367
    new-instance v0, Landroid/database/MatrixCursor;

    new-array v6, v6, [Ljava/lang/String;

    const-string v8, "_id"

    aput-object v8, v6, v7

    const-string v8, "mode"

    aput-object v8, v6, v9

    const-string v8, "speed_quality_compromise"

    aput-object v8, v6, v10

    invoke-direct {v0, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 369
    .local v0, "cursor":Landroid/database/MatrixCursor;
    iget-object v8, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    monitor-enter v8

    .line 371
    :try_start_0
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v6}, Ljava/util/HashMap;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    sget-boolean v6, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheLangNeedToBeRebuild:Z

    if-eqz v6, :cond_1

    .line 373
    :cond_0
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLangCache()V

    .line 376
    :cond_1
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 378
    const/4 v1, 0x0

    .line 379
    .local v1, "i":I
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v6, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/visionobjects/resourcemanager/LangCacheEntry;

    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/LangCacheEntry;->getLangConfig()Lcom/visionobjects/resourcemanager/model/LangConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/model/LangConfig;->getLangModes()Ljava/util/List;

    move-result-object v4

    .line 380
    .local v4, "langModes":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    .line 382
    .local v3, "langMode":Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;
    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedQualityCompromise()Ljava/lang/Integer;

    move-result-object v5

    .line 383
    .local v5, "speedQualityCompromise":Ljava/lang/Integer;
    const/4 v6, 0x3

    new-array v9, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v6, 0x1

    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->getRefModeName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v6

    const/4 v10, 0x2

    if-nez v5, :cond_2

    move v6, v7

    :goto_1
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v10

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 385
    add-int/lit8 v1, v1, 0x1

    .line 386
    goto :goto_0

    .line 383
    :cond_2
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_1

    .line 388
    .end local v3    # "langMode":Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;
    .end local v5    # "speedQualityCompromise":Ljava/lang/Integer;
    :cond_3
    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    const-string v9, "langfile"

    aput-object v9, v6, v7

    const/4 v7, 0x2

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v6, v7

    invoke-virtual {v0, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 390
    .end local v1    # "i":I
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "langModes":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;>;"
    :cond_4
    monitor-exit v8

    .line 392
    return-object v0

    .line 390
    :catchall_0
    move-exception v6

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method private buildLangsCursor()Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 326
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "_id"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string v11, "lang"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string v11, "version"

    aput-object v11, v9, v10

    const/4 v10, 0x3

    const-string v11, "lastAvailableVersion"

    aput-object v11, v9, v10

    const/4 v10, 0x4

    const-string v11, "preloaded"

    aput-object v11, v9, v10

    invoke-direct {v0, v9}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 328
    .local v0, "cursor":Landroid/database/MatrixCursor;
    const-string v8, ""

    .line 329
    .local v8, "versionString":Ljava/lang/String;
    sget-boolean v9, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUseLastAvailableVersion:Z

    if-eqz v9, :cond_0

    .line 331
    new-instance v6, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v6, v9}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;-><init>(Landroid/content/Context;)V

    .line 332
    .local v6, "remoteSystemHelper":Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;
    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v7

    .line 333
    .local v7, "remoteVersion":Lcom/visionobjects/resourcemanager/util/Version;
    if-eqz v7, :cond_0

    .line 335
    invoke-virtual {v7}, Lcom/visionobjects/resourcemanager/util/Version;->toString()Ljava/lang/String;

    move-result-object v8

    .line 339
    .end local v6    # "remoteSystemHelper":Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;
    .end local v7    # "remoteVersion":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_0
    iget-object v10, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    monitor-enter v10

    .line 341
    :try_start_0
    iget-object v9, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    sget-boolean v9, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheLangNeedToBeRebuild:Z

    if-eqz v9, :cond_2

    .line 343
    :cond_1
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLangCache()V

    .line 346
    :cond_2
    const/4 v2, 0x0

    .line 347
    .local v2, "i":I
    iget-object v9, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v9}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    .line 348
    .local v5, "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 350
    .local v4, "lang":Ljava/lang/String;
    invoke-static {v4}, Lcom/visionobjects/resourcemanager/util/Utils;->isSpecificLanguage(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_3

    .line 352
    iget-object v9, p0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->mLangCache:Ljava/util/HashMap;

    invoke-virtual {v9, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/visionobjects/resourcemanager/LangCacheEntry;

    .line 353
    .local v1, "entry":Lcom/visionobjects/resourcemanager/LangCacheEntry;
    const-string v9, ""

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 354
    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v9, v11

    const/4 v11, 0x1

    aput-object v4, v9, v11

    const/4 v11, 0x2

    iget-object v12, v1, Lcom/visionobjects/resourcemanager/LangCacheEntry;->version:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v11, 0x3

    iget-object v12, v1, Lcom/visionobjects/resourcemanager/LangCacheEntry;->version:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v11, 0x4

    iget-boolean v12, v1, Lcom/visionobjects/resourcemanager/LangCacheEntry;->preloaded:Z

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v9, v11

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 357
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 356
    :cond_4
    const/4 v9, 0x5

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v9, v11

    const/4 v11, 0x1

    aput-object v4, v9, v11

    const/4 v11, 0x2

    iget-object v12, v1, Lcom/visionobjects/resourcemanager/LangCacheEntry;->version:Ljava/lang/String;

    aput-object v12, v9, v11

    const/4 v11, 0x3

    aput-object v8, v9, v11

    const/4 v11, 0x4

    iget-boolean v12, v1, Lcom/visionobjects/resourcemanager/LangCacheEntry;->preloaded:Z

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v9, v11

    invoke-virtual {v0, v9}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_1

    .line 360
    .end local v1    # "entry":Lcom/visionobjects/resourcemanager/LangCacheEntry;
    .end local v2    # "i":I
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "lang":Ljava/lang/String;
    .end local v5    # "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catchall_0
    move-exception v9

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v9

    .restart local v2    # "i":I
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_5
    :try_start_1
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    return-object v0
.end method

.method private buildLanguagesDownloadingCursor()Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 462
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    const-string v12, "_id"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string v12, "lang"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string v12, "version"

    aput-object v12, v10, v11

    invoke-direct {v0, v10}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 464
    .local v0, "cursor":Landroid/database/MatrixCursor;
    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getDownloadingLanguages(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v1

    .line 466
    .local v1, "downloading":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;>;"
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->getWaitForDownloadingLanguage()Ljava/util/List;

    move-result-object v2

    .line 469
    .local v2, "enqueuedLanguage":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x0

    .line 470
    .local v3, "i":I
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v7

    .line 471
    .local v7, "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v8, 0x0

    .line 472
    .local v8, "topVersion":Lcom/visionobjects/resourcemanager/util/Version;
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 474
    .local v6, "lang":Ljava/lang/String;
    invoke-static {v6}, Lcom/visionobjects/resourcemanager/util/Utils;->isSpecificLanguage(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 476
    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/visionobjects/resourcemanager/util/Version;

    .line 477
    .local v9, "versionInstalled":Lcom/visionobjects/resourcemanager/util/Version;
    if-eqz v8, :cond_1

    invoke-virtual {v8, v9}, Lcom/visionobjects/resourcemanager/util/Version;->compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I

    move-result v10

    if-gez v10, :cond_2

    .line 478
    :cond_1
    move-object v8, v9

    .line 479
    :cond_2
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .local v4, "i":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v6, v10, v11

    const/4 v11, 0x2

    invoke-virtual {v9}, Lcom/visionobjects/resourcemanager/util/Version;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_0

    .line 483
    .end local v6    # "lang":Ljava/lang/String;
    .end local v9    # "versionInstalled":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_3
    if-nez v8, :cond_4

    .line 484
    new-instance v8, Lcom/visionobjects/resourcemanager/util/Version;

    .end local v8    # "topVersion":Lcom/visionobjects/resourcemanager/util/Version;
    invoke-direct {v8}, Lcom/visionobjects/resourcemanager/util/Version;-><init>()V

    .line 486
    .restart local v8    # "topVersion":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 488
    .restart local v6    # "lang":Ljava/lang/String;
    invoke-static {v6}, Lcom/visionobjects/resourcemanager/util/Utils;->isSpecificLanguage(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 490
    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    add-int/lit8 v4, v3, 0x1

    .end local v3    # "i":I
    .restart local v4    # "i":I
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v6, v10, v11

    const/4 v11, 0x2

    invoke-virtual {v8}, Lcom/visionobjects/resourcemanager/util/Version;->toString()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v3, v4

    .end local v4    # "i":I
    .restart local v3    # "i":I
    goto :goto_1

    .line 495
    .end local v6    # "lang":Ljava/lang/String;
    :cond_6
    if-nez v3, :cond_7

    .line 497
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->cleanDownloadFiles()V

    .line 500
    :cond_7
    return-object v0
.end method

.method private buildLanguagesUpdateCursor()Landroid/database/Cursor;
    .locals 15

    .prologue
    .line 508
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "_id"

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-string v14, "lang"

    aput-object v14, v12, v13

    const/4 v13, 0x2

    const-string v14, "version"

    aput-object v14, v12, v13

    invoke-direct {v0, v12}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 510
    .local v0, "cursor":Landroid/database/MatrixCursor;
    new-instance v10, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    const/4 v13, 0x1

    invoke-direct {v10, v12, v13}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;-><init>(Landroid/content/Context;Z)V

    .line 511
    .local v10, "remoteSystemHelper":Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;
    new-instance v1, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v1, v12}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    .line 513
    .local v1, "fileSystemHelper":Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->hasResourcesParserfailed()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 543
    :cond_0
    return-object v0

    .line 517
    :cond_1
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getLangs()Ljava/util/Set;

    move-result-object v9

    .line 518
    .local v9, "remoteLangs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v11

    .line 519
    .local v11, "remoteVersion":Lcom/visionobjects/resourcemanager/util/Version;
    const/4 v2, 0x0

    .line 521
    .local v2, "i":I
    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 523
    .local v5, "lang":Ljava/lang/String;
    invoke-virtual {v1, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 524
    .local v6, "localMd5":Ljava/lang/String;
    const/4 v8, 0x0

    .line 526
    .local v8, "needToUpdate":Z
    if-nez v6, :cond_4

    .line 529
    const/4 v8, 0x1

    .line 538
    :cond_3
    :goto_1
    if-eqz v8, :cond_2

    .line 540
    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    add-int/lit8 v3, v2, 0x1

    .end local v2    # "i":I
    .local v3, "i":I
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x1

    aput-object v5, v12, v13

    const/4 v13, 0x2

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/util/Version;->toString()Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v0, v12}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move v2, v3

    .end local v3    # "i":I
    .restart local v2    # "i":I
    goto :goto_0

    .line 533
    :cond_4
    invoke-virtual {v1, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersion(Ljava/lang/String;)Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v7

    .line 534
    .local v7, "localVersion":Lcom/visionobjects/resourcemanager/util/Version;
    invoke-virtual {v11, v7}, Lcom/visionobjects/resourcemanager/util/Version;->compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I

    move-result v12

    if-lez v12, :cond_3

    .line 535
    const/4 v8, 0x1

    goto :goto_1
.end method

.method private buildVersionInfoCursor()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 551
    new-instance v1, Landroid/database/MatrixCursor;

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "_id"

    aput-object v5, v4, v6

    const-string v5, "company_name"

    aput-object v5, v4, v7

    const-string v5, "version_name"

    aput-object v5, v4, v8

    const-string v5, "version_code"

    aput-object v5, v4, v9

    invoke-direct {v1, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 554
    .local v1, "cursor":Landroid/database/MatrixCursor;
    const-string v0, "MyScript"

    .line 555
    .local v0, "companyName":Ljava/lang/String;
    const-string v3, "1.3.0"

    .line 556
    .local v3, "versionName":Ljava/lang/String;
    const/16 v2, 0x105

    .line 558
    .local v2, "versionCode":I
    new-array v4, v10, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    const-string v5, "MyScript"

    aput-object v5, v4, v7

    const-string v5, "1.3.0"

    aput-object v5, v4, v8

    const/16 v5, 0x105

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v9

    invoke-virtual {v1, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 560
    return-object v1
.end method

.method public static clearCache()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 106
    sput-boolean v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheEngineNeedToBeRebuild:Z

    .line 107
    sput-boolean v0, Lcom/visionobjects/resourcemanager/DataStorageProvider;->CacheLangNeedToBeRebuild:Z

    .line 108
    return-void
.end method

.method public static initUriMatcher()V
    .locals 4

    .prologue
    .line 82
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    .line 83
    .local v1, "matcher":Landroid/content/UriMatcher;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerContract;->getAuthority()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, "auth":Ljava/lang/String;
    const-string v2, "engine"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 85
    const-string v2, "engine/#"

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 86
    const-string v2, "langs/*"

    const/4 v3, 0x3

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 87
    const-string v2, "langs/*/*"

    const/4 v3, 0x4

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 88
    const-string v2, "langs"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    const-string v2, "updates"

    const/4 v3, 0x5

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    const-string v2, "downloading"

    const/4 v3, 0x6

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    const-string v2, "version"

    const/4 v3, 0x7

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    const-string v2, "refresh"

    const/16 v3, 0x8

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    sput-object v1, Lcom/visionobjects/resourcemanager/DataStorageProvider;->sUriMatcher:Landroid/content/UriMatcher;

    .line 94
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 160
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Read Only URI"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 235
    sget-object v1, Lcom/visionobjects/resourcemanager/DataStorageProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 237
    .local v0, "res":I
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 239
    const-string v1, "vnd.android.cursor.dir/com.visionobjects.resoucesprovider.langs"

    .line 243
    :goto_0
    return-object v1

    .line 241
    :cond_0
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 243
    const-string v1, "vnd.android.cursor.dir/com.visionobjects.resoucesprovider.enginelocation"

    goto :goto_0

    .line 245
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 170
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Read only URI"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate()Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 119
    :try_start_0
    const-string v4, "com.visionobjects.resourcemanager.InitResourceManagerService"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 120
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v4, 0x1

    new-array v3, v4, [Ljava/lang/Class;

    .line 121
    .local v3, "params":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v4, 0x0

    const-class v5, Landroid/content/res/Resources;

    aput-object v5, v3, v4

    .line 122
    const-string v4, "Initialize"

    invoke-virtual {v0, v4, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 123
    .local v2, "methodInit":Ljava/lang/reflect/Method;
    const/4 v4, 0x0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 146
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v2    # "methodInit":Ljava/lang/reflect/Method;
    .end local v3    # "params":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->initUriMatcher()V

    .line 147
    return v8

    .line 125
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Ljava/lang/ClassNotFoundException;
    const-string v4, "ResourceManagerService"

    const-string v5, ""

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 129
    .end local v1    # "e":Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v1

    .line 131
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "ResourceManagerService"

    const-string v5, "NoSuchMethodException "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 133
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_2
    move-exception v1

    .line 135
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "ResourceManagerService"

    const-string v5, "IllegalArgumentException "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 137
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 139
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "ResourceManagerService"

    const-string v5, "IllegalAccessException "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 141
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_4
    move-exception v1

    .line 143
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "ResourceManagerService"

    const-string v5, "InvocationTargetException "

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 191
    sget-object v4, Lcom/visionobjects/resourcemanager/DataStorageProvider;->sUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v4, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 192
    .local v2, "res":I
    if-nez v2, :cond_0

    .line 194
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLangsCursor()Landroid/database/Cursor;

    move-result-object v4

    .line 227
    :goto_0
    return-object v4

    .line 196
    :cond_0
    const/4 v4, 0x3

    if-ne v2, v4, :cond_1

    .line 198
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 199
    .local v0, "lang":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLangModesCursor(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 201
    .end local v0    # "lang":Ljava/lang/String;
    :cond_1
    const/4 v4, 0x4

    if-ne v2, v4, :cond_2

    .line 203
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v3

    .line 204
    .local v3, "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 205
    .restart local v0    # "lang":Ljava/lang/String;
    const/4 v4, 0x2

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 206
    .local v1, "mode":Ljava/lang/String;
    invoke-direct {p0, v0, v1}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLangModeResourcesCursor(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 208
    .end local v0    # "lang":Ljava/lang/String;
    .end local v1    # "mode":Ljava/lang/String;
    .end local v3    # "strings":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    if-ne v2, v6, :cond_3

    .line 210
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildEnginesCursor()Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 212
    :cond_3
    const/4 v4, 0x5

    if-ne v2, v4, :cond_4

    .line 214
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLanguagesUpdateCursor()Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 216
    :cond_4
    const/4 v4, 0x6

    if-ne v2, v4, :cond_5

    .line 218
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildLanguagesDownloadingCursor()Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 220
    :cond_5
    const/4 v4, 0x7

    if-ne v2, v4, :cond_6

    .line 222
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->buildVersionInfoCursor()Landroid/database/Cursor;

    move-result-object v4

    goto :goto_0

    .line 224
    :cond_6
    const/16 v4, 0x8

    if-ne v2, v4, :cond_7

    .line 226
    new-instance v4, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v6}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;-><init>(Landroid/content/Context;Z)V

    .line 227
    const/4 v4, 0x0

    goto :goto_0

    .line 229
    :cond_7
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unknown URI "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 180
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
