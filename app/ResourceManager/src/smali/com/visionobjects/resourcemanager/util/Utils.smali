.class public Lcom/visionobjects/resourcemanager/util/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final BUFFER_SIZE:I = 0x400

.field private static final DBG:Z = false

.field public static LocalPartitionFull:Z = false

.field private static final TAG:Ljava/lang/String; = "Utils"

.field public static TempoSuffix:Ljava/lang/String;

.field private static buffer:[B

.field private static cpt:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput v0, Lcom/visionobjects/resourcemanager/util/Utils;->cpt:I

    .line 35
    const-string v0, "rkljEFDS"

    sput-object v0, Lcom/visionobjects/resourcemanager/util/Utils;->TempoSuffix:Ljava/lang/String;

    .line 37
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    sput-object v0, Lcom/visionobjects/resourcemanager/util/Utils;->buffer:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static computePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .local v0, "oSbBuilder":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_0

    .line 60
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static copyAssetsFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 7
    .param p0, "filename"    # Ljava/lang/String;
    .param p1, "dest"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 246
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, "newFileName":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 250
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    const/16 v6, 0x400

    new-array v1, v6, [B

    .line 252
    .local v1, "buffer":[B
    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2

    .line 253
    .local v2, "in":Ljava/io/InputStream;
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 256
    .local v4, "out":Ljava/io/OutputStream;
    :goto_0
    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .local v5, "read":I
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 257
    const/4 v6, 0x0

    invoke-virtual {v4, v1, v6, v5}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 259
    :cond_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 260
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 261
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 262
    return-void
.end method

.method public static declared-synchronized copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 14
    .param p0, "src"    # Ljava/lang/String;
    .param p1, "dst"    # Ljava/lang/String;

    .prologue
    .line 70
    const-class v11, Lcom/visionobjects/resourcemanager/util/Utils;

    monitor-enter v11

    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 71
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    .line 72
    .local v3, "fileExist":Z
    if-nez v3, :cond_0

    .line 74
    new-instance v2, Ljava/io/File;

    .end local v2    # "f":Ljava/io/File;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Lcom/visionobjects/resourcemanager/util/Utils;->TempoSuffix:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 75
    .restart local v2    # "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v3

    .line 77
    :cond_0
    if-nez v3, :cond_2

    .line 78
    const/4 v10, 0x0

    .line 131
    :cond_1
    :goto_0
    monitor-exit v11

    return v10

    .line 80
    :cond_2
    :try_start_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    .local v0, "dest":Ljava/io/File;
    const/4 v5, 0x0

    .line 82
    .local v5, "in":Ljava/io/FileInputStream;
    const/4 v7, 0x0

    .line 83
    .local v7, "out":Ljava/io/FileOutputStream;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    sget-object v10, Lcom/visionobjects/resourcemanager/util/Utils;->buffer:[B

    array-length v10, v10

    if-ge v4, v10, :cond_3

    .line 84
    sget-object v10, Lcom/visionobjects/resourcemanager/util/Utils;->buffer:[B

    const/4 v12, 0x0

    aput-byte v12, v10, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 83
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 90
    :cond_3
    :try_start_2
    new-instance v6, Ljava/io/FileInputStream;

    invoke-direct {v6, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 92
    .end local v5    # "in":Ljava/io/FileInputStream;
    .local v6, "in":Ljava/io/FileInputStream;
    :try_start_3
    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    .line 93
    new-instance v8, Ljava/io/FileOutputStream;

    invoke-direct {v8, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 94
    .end local v7    # "out":Ljava/io/FileOutputStream;
    .local v8, "out":Ljava/io/FileOutputStream;
    :try_start_4
    sget-object v10, Lcom/visionobjects/resourcemanager/util/Utils;->buffer:[B

    invoke-virtual {v6, v10}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    .line 95
    .local v9, "size":I
    :goto_2
    if-lez v9, :cond_4

    .line 96
    sget-object v10, Lcom/visionobjects/resourcemanager/util/Utils;->buffer:[B

    const/4 v12, 0x0

    invoke-virtual {v8, v10, v12, v9}, Ljava/io/FileOutputStream;->write([BII)V

    .line 97
    sget-object v10, Lcom/visionobjects/resourcemanager/util/Utils;->buffer:[B

    invoke-virtual {v6, v10}, Ljava/io/FileInputStream;->read([B)I

    move-result v9

    goto :goto_2

    .line 99
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, -0x3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v12

    invoke-virtual {p1, v10, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    const-string v12, ".so"

    invoke-virtual {v10, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 100
    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-virtual {v0, v10, v12}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 101
    :cond_5
    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-virtual {v0, v10, v12}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    .line 113
    if-eqz v6, :cond_6

    .line 115
    :try_start_5
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    .line 117
    :cond_6
    if-eqz v8, :cond_7

    .line 119
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 131
    :cond_7
    const/4 v10, 0x1

    goto :goto_0

    .line 122
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Ljava/io/IOException;
    :try_start_6
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error during copy - file src:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in dst:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error2: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 126
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 103
    .end local v1    # "e":Ljava/io/IOException;
    .end local v6    # "in":Ljava/io/FileInputStream;
    .end local v8    # "out":Ljava/io/FileOutputStream;
    .end local v9    # "size":I
    .restart local v5    # "in":Ljava/io/FileInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catch_1
    move-exception v1

    .line 105
    .restart local v1    # "e":Ljava/io/IOException;
    :goto_3
    :try_start_7
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error during copy - file src:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in dst:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error1: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v10, "TAG"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "File exists "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " existed at the beginning of the copy ? "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 108
    const/4 v10, 0x0

    .line 113
    if-eqz v5, :cond_8

    .line 115
    :try_start_8
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 117
    :cond_8
    if-eqz v7, :cond_1

    .line 119
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_0

    .line 122
    :catch_2
    move-exception v1

    .line 124
    :try_start_9
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error during copy - file src:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in dst:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error2: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 126
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 111
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v10

    .line 113
    :goto_4
    if-eqz v5, :cond_9

    .line 115
    :try_start_a
    invoke-virtual {v5}, Ljava/io/FileInputStream;->close()V

    .line 117
    :cond_9
    if-eqz v7, :cond_a

    .line 119
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 126
    :cond_a
    :try_start_b
    throw v10
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 70
    .end local v0    # "dest":Ljava/io/File;
    .end local v2    # "f":Ljava/io/File;
    .end local v3    # "fileExist":Z
    .end local v4    # "i":I
    .end local v5    # "in":Ljava/io/FileInputStream;
    .end local v7    # "out":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v10

    monitor-exit v11

    throw v10

    .line 122
    .restart local v0    # "dest":Ljava/io/File;
    .restart local v2    # "f":Ljava/io/File;
    .restart local v3    # "fileExist":Z
    .restart local v4    # "i":I
    .restart local v5    # "in":Ljava/io/FileInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catch_3
    move-exception v1

    .line 124
    .restart local v1    # "e":Ljava/io/IOException;
    :try_start_c
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error during copy - file src:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " in dst:"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const-string v10, "Utils"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "..... error2: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v10, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    .line 126
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 111
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    :catchall_2
    move-exception v10

    move-object v5, v6

    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .end local v5    # "in":Ljava/io/FileInputStream;
    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    :catchall_3
    move-exception v10

    move-object v7, v8

    .end local v8    # "out":Ljava/io/FileOutputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    goto :goto_4

    .line 103
    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    :catch_4
    move-exception v1

    move-object v5, v6

    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    goto/16 :goto_3

    .end local v5    # "in":Ljava/io/FileInputStream;
    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    .restart local v8    # "out":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v1

    move-object v7, v8

    .end local v8    # "out":Ljava/io/FileOutputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    move-object v5, v6

    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    goto/16 :goto_3
.end method

.method public static copyFileOrDirFromAssets(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 8
    .param p0, "path"    # Ljava/lang/String;
    .param p1, "dest"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 272
    invoke-virtual {p2}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 273
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "assets":[Ljava/lang/String;
    array-length v5, v1

    if-nez v5, :cond_1

    .line 275
    invoke-static {p0, p1, p2}, Lcom/visionobjects/resourcemanager/util/Utils;->copyAssetsFile(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 291
    :cond_0
    return-void

    .line 277
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 278
    .local v3, "fullPath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 279
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    .line 282
    invoke-virtual {v2, v7}, Ljava/io/File;->setReadable(Z)Z

    .line 283
    invoke-virtual {v2, v7}, Ljava/io/File;->setWritable(Z)Z

    .line 284
    invoke-virtual {v2, v7}, Ljava/io/File;->setExecutable(Z)Z

    .line 285
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 287
    :cond_2
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v5, v1

    if-ge v4, v5, :cond_0

    .line 288
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v1, v4

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1, p2}, Lcom/visionobjects/resourcemanager/util/Utils;->copyFileOrDirFromAssets(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 287
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method public static deleteDir(Ljava/io/File;)Z
    .locals 6
    .param p0, "dir"    # Ljava/io/File;

    .prologue
    .line 203
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 206
    new-instance v0, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "checked"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 207
    .local v0, "checkedFile":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 208
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 210
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    .line 211
    .local v1, "children":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v4, v1

    if-ge v2, v4, :cond_2

    .line 213
    new-instance v4, Ljava/io/File;

    aget-object v5, v1, v2

    invoke-direct {v4, p0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {v4}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/io/File;)Z

    move-result v3

    .line 214
    .local v3, "success":Z
    if-nez v3, :cond_1

    .line 215
    const/4 v4, 0x0

    .line 218
    .end local v0    # "checkedFile":Ljava/io/File;
    .end local v1    # "children":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "success":Z
    :goto_1
    return v4

    .line 211
    .restart local v0    # "checkedFile":Ljava/io/File;
    .restart local v1    # "children":[Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "success":Z
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 218
    .end local v0    # "checkedFile":Ljava/io/File;
    .end local v1    # "children":[Ljava/lang/String;
    .end local v2    # "i":I
    .end local v3    # "success":Z
    :cond_2
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v4

    goto :goto_1
.end method

.method public static deleteDir(Ljava/lang/String;)Z
    .locals 1
    .param p0, "dir"    # Ljava/lang/String;

    .prologue
    .line 192
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method public static getRandomFileName()Ljava/lang/String;
    .locals 4

    .prologue
    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tempoVoFiles"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x14

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/visionobjects/resourcemanager/util/Utils;->cpt:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/visionobjects/resourcemanager/util/Utils;->cpt:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static initDefaultLangPack(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "defaultVersion"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 231
    .local v1, "path":Ljava/lang/String;
    :try_start_0
    invoke-static {p1, v1, p0}, Lcom/visionobjects/resourcemanager/util/Utils;->copyFileOrDirFromAssets(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :goto_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    .line 233
    .local v0, "e":Ljava/io/IOException;
    const-string v2, "Utils"

    const-string v3, "> impossible to copy the default langPack"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    const-string v2, "Utils"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "> error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static isSpecificLanguage(Ljava/lang/String;)Z
    .locals 2
    .param p0, "lang"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    .line 41
    const-string v1, "mul"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 48
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    const-string v1, "lib"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 45
    const-string v1, "equation"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static processLocalMove(Lcom/visionobjects/resourcemanager/model/FileToMove;)Z
    .locals 5
    .param p0, "fileToMove"    # Lcom/visionobjects/resourcemanager/model/FileToMove;

    .prologue
    .line 150
    const/4 v2, 0x1

    .line 151
    .local v2, "success":Z
    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getSrcFilename()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getDestFilename()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/visionobjects/resourcemanager/util/Utils;->copyFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    .line 155
    if-eqz v2, :cond_1

    .line 157
    const/4 v3, 0x0

    :try_start_0
    sput-boolean v3, Lcom/visionobjects/resourcemanager/util/Utils;->LocalPartitionFull:Z

    .line 158
    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getDestFilename()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/visionobjects/resourcemanager/util/Md5Builder;->getMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getMd5()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-nez v3, :cond_0

    .line 160
    const/4 v2, 0x0

    .line 176
    :cond_0
    :goto_0
    return v2

    .line 165
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/io/IOException;
    const/4 v2, 0x0

    .line 170
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    const-string v4, "No space left"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    sput-boolean v3, Lcom/visionobjects/resourcemanager/util/Utils;->LocalPartitionFull:Z

    goto :goto_0

    .line 172
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 174
    .local v1, "e2":Ljava/security/NoSuchAlgorithmException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static processLocalMove(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/FileToMove;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 136
    .local p0, "filesToMove":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/FileToMove;>;"
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 137
    const/4 v2, 0x1

    .line 145
    :goto_0
    return v2

    .line 138
    :cond_0
    const/4 v2, 0x1

    .line 139
    .local v2, "result":Z
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/resourcemanager/model/FileToMove;

    .line 141
    .local v0, "fileToMove":Lcom/visionobjects/resourcemanager/model/FileToMove;
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/util/Utils;->processLocalMove(Lcom/visionobjects/resourcemanager/model/FileToMove;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 142
    const/4 v2, 0x0

    goto :goto_1

    .line 144
    .end local v0    # "fileToMove":Lcom/visionobjects/resourcemanager/model/FileToMove;
    :cond_2
    const/4 v3, 0x0

    sput-boolean v3, Lcom/visionobjects/resourcemanager/util/Utils;->LocalPartitionFull:Z

    goto :goto_0
.end method
