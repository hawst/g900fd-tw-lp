.class public Lcom/visionobjects/resourcemanager/util/Version;
.super Ljava/lang/Object;
.source "Version.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/visionobjects/resourcemanager/util/Version;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mFields:[I

.field private mString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcom/visionobjects/resourcemanager/util/Version;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/util/Version;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, ""

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    .line 68
    const/4 v0, 0x0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    .line 69
    return-void
.end method

.method public constructor <init>(Lcom/visionobjects/resourcemanager/util/Version;)V
    .locals 1
    .param p1, "other"    # Lcom/visionobjects/resourcemanager/util/Version;

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iget-object v0, p1, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    .line 74
    iget-object v0, p1, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 11
    .param p1, "versionStr"    # Ljava/lang/String;

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 20
    :cond_0
    const-string v7, ""

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    .line 21
    new-array v7, v10, [I

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    .line 63
    :cond_1
    return-void

    .line 24
    :cond_2
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    .line 26
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    .line 27
    .local v4, "len":I
    const/4 v5, 0x0

    .line 28
    .local v5, "offset":I
    const/4 v2, -0x2

    .line 29
    .local v2, "index":I
    new-array v6, v4, [I

    .line 31
    .local v6, "tab":[I
    const/4 v1, 0x0

    .line 32
    .local v1, "i":I
    :goto_0
    if-eq v2, v9, :cond_5

    .line 34
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    const/16 v8, 0x2e

    invoke-virtual {v7, v8, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 37
    if-ne v2, v9, :cond_3

    .line 38
    :try_start_0
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v7, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v6, v1

    .line 56
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 57
    add-int/lit8 v5, v2, 0x1

    goto :goto_0

    .line 40
    :cond_3
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v7, v5, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v6, v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 42
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/NumberFormatException;
    sget-object v7, Lcom/visionobjects/resourcemanager/util/Version;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Number version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is corrupted"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    array-length v7, v6

    if-ge v1, v7, :cond_4

    .line 46
    aput v10, v6, v1

    .line 47
    :cond_4
    const-string v7, ""

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    .line 59
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_5
    :goto_2
    new-array v7, v1, [I

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    .line 61
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    if-ge v3, v1, :cond_1

    .line 62
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    aget v8, v6, v3

    aput v8, v7, v3

    .line 61
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 50
    .end local v3    # "j":I
    :catch_1
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    sget-object v7, Lcom/visionobjects/resourcemanager/util/Version;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Number version "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " is corrupted"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const-string v7, ""

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method public compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I
    .locals 8
    .param p1, "other"    # Lcom/visionobjects/resourcemanager/util/Version;

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, -0x1

    .line 90
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    iget-object v7, p1, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v3

    .line 93
    :cond_1
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    array-length v1, v6

    .line 94
    .local v1, "sizeVersion1":I
    iget-object v6, p1, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    array-length v2, v6

    .line 96
    .local v2, "sizeVersion2":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 98
    if-ne v1, v0, :cond_2

    move v3, v4

    .line 99
    goto :goto_0

    .line 100
    :cond_2
    if-ne v2, v0, :cond_3

    move v3, v5

    .line 101
    goto :goto_0

    .line 102
    :cond_3
    iget-object v6, p1, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    aget v6, v6, v0

    iget-object v7, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    aget v7, v7, v0

    if-le v6, v7, :cond_4

    move v3, v4

    .line 103
    goto :goto_0

    .line 104
    :cond_4
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    aget v6, v6, v0

    iget-object v7, p1, Lcom/visionobjects/resourcemanager/util/Version;->mFields:[I

    aget v7, v7, v0

    if-le v6, v7, :cond_5

    move v3, v5

    .line 105
    goto :goto_0

    .line 96
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 9
    check-cast p1, Lcom/visionobjects/resourcemanager/util/Version;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/visionobjects/resourcemanager/util/Version;->compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 124
    instance-of v0, p1, Lcom/visionobjects/resourcemanager/util/Version;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/visionobjects/resourcemanager/util/Version;

    .end local p1    # "obj":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/visionobjects/resourcemanager/util/Version;->compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/util/Version;->mString:Ljava/lang/String;

    return-object v0
.end method
