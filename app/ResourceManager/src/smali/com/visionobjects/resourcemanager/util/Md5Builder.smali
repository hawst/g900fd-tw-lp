.class public Lcom/visionobjects/resourcemanager/util/Md5Builder;
.super Ljava/lang/Object;
.source "Md5Builder.java"


# static fields
.field private static final MD5:Ljava/lang/String; = "MD5"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getMd5(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 10
    .param p0, "in"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 27
    const-string v8, "MD5"

    invoke-static {v8}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    .line 29
    .local v0, "digest":Ljava/security/MessageDigest;
    const/4 v6, 0x0

    .line 30
    .local v6, "size":I
    const/16 v8, 0x2000

    new-array v7, v8, [B

    .line 31
    .local v7, "tempo":[B
    invoke-virtual {p0, v7}, Ljava/io/InputStream;->read([B)I

    move-result v6

    .line 32
    :goto_0
    if-ltz v6, :cond_0

    .line 34
    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8, v6}, Ljava/security/MessageDigest;->update([BII)V

    .line 35
    invoke-virtual {p0, v7}, Ljava/io/InputStream;->read([B)I

    move-result v6

    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v4

    .line 40
    .local v4, "messageDigest":[B
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 41
    .local v2, "hexString":Ljava/lang/StringBuffer;
    array-length v1, v4

    .line 42
    .local v1, "digestSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_2

    .line 44
    aget-byte v8, v4, v3

    and-int/lit16 v8, v8, 0xff

    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    .line 46
    .local v5, "s":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 47
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "0"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 48
    :cond_1
    invoke-virtual {v2, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 42
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 50
    .end local v5    # "s":Ljava/lang/String;
    :cond_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 51
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    return-object v8
.end method

.method public static getMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "filename"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/security/NoSuchAlgorithmException;
        }
    .end annotation

    .prologue
    .line 18
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 19
    .local v0, "f":Ljava/io/File;
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 20
    .local v1, "in":Ljava/io/FileInputStream;
    invoke-static {v1}, Lcom/visionobjects/resourcemanager/util/Md5Builder;->getMd5(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
