.class public Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;
.super Ljava/lang/Object;
.source "LanguageNameHelper.java"


# instance fields
.field private final mDisplayedLanguageKeys:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mMultipleCountriesLanguages:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 14
    .local p1, "displayedLanguageKeys":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->mDisplayedLanguageKeys:Ljava/util/Collection;

    .line 16
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->getMultipleCountriesLanguages()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->mMultipleCountriesLanguages:Ljava/util/Set;

    .line 17
    return-void
.end method

.method private capitalizedDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;
    .locals 4
    .param p1, "locale"    # Ljava/util/Locale;

    .prologue
    const/4 v3, 0x1

    .line 87
    invoke-virtual {p1, p1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 88
    .local v0, "displayLanguage":Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getMultipleCountriesLanguages()Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 66
    .local v4, "result":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->mDisplayedLanguageKeys:Ljava/util/Collection;

    if-eqz v5, :cond_1

    .line 68
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 69
    .local v0, "checkedLanguages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->mDisplayedLanguageKeys:Ljava/util/Collection;

    invoke-interface {v5}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 71
    .local v2, "languageKey":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->getLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v3

    .line 72
    .local v3, "locale":Ljava/util/Locale;
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 74
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 78
    :cond_0
    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 82
    .end local v0    # "checkedLanguages":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "languageKey":Ljava/lang/String;
    .end local v3    # "locale":Ljava/util/Locale;
    :cond_1
    return-object v4
.end method


# virtual methods
.method public getDisplayLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "languageKey"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->getLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    .line 23
    .local v0, "locale":Ljava/util/Locale;
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->mMultipleCountriesLanguages:Ljava/util/Set;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->capitalizedDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v0}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 26
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;->capitalizedDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 7
    .param p1, "languageKey"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 31
    const-string v4, "_"

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 32
    .local v0, "keys":[Ljava/lang/String;
    const-string v2, ""

    .line 33
    .local v2, "localeLanguage":Ljava/lang/String;
    const-string v1, ""

    .line 34
    .local v1, "localeCountry":Ljava/lang/String;
    const-string v3, ""

    .line 36
    .local v3, "localeVariant":Ljava/lang/String;
    array-length v4, v0

    if-lez v4, :cond_0

    .line 37
    const/4 v4, 0x0

    aget-object v2, v0, v4

    .line 38
    :cond_0
    array-length v4, v0

    if-le v4, v5, :cond_1

    .line 39
    aget-object v1, v0, v5

    .line 40
    :cond_1
    array-length v4, v0

    if-le v4, v6, :cond_2

    .line 41
    aget-object v3, v0, v6

    .line 43
    :cond_2
    const-string v4, "sr"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 45
    const-string v4, "Cyrl"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 47
    move-object v3, v1

    .line 48
    const-string v1, "SR"

    .line 55
    :cond_3
    :goto_0
    move-object v3, v1

    .line 59
    :cond_4
    new-instance v4, Ljava/util/Locale;

    invoke-direct {v4, v2, v1, v3}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v4

    .line 50
    :cond_5
    const-string v4, "Lat"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 52
    move-object v3, v1

    .line 53
    const-string v1, "RS"

    goto :goto_0
.end method
