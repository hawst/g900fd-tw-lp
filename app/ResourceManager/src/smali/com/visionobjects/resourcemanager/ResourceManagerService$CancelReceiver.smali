.class public Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ResourceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/ResourceManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CancelReceiver"
.end annotation


# static fields
.field private static final DBG:Z = true


# instance fields
.field private final TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;


# direct methods
.method public constructor <init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V
    .locals 1

    .prologue
    .line 286
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 292
    const-class v0, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;->TAG:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intentToCancel"    # Landroid/content/Intent;

    .prologue
    .line 300
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 302
    .local v1, "langName":Ljava/lang/String;
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "> onReceive : lang="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 304
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->getWaitForDownloadingLanguage()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 306
    invoke-static {v1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->removeFromWaitingDownloadList(Ljava/lang/String;)V

    .line 307
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 308
    .local v0, "intent":Landroid/content/Intent;
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 309
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v3, -0x4

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 311
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SENDING FAIL ACTION_UPDATE_LANG_RESULT : lang="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 314
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 316
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
