.class public Lcom/visionobjects/resourcemanager/model/ResourcesData;
.super Ljava/lang/Object;
.source "ResourcesData.java"


# instance fields
.field private mDate:Ljava/util/Date;

.field private mDelay:I

.field private final mLanguagesMd5Map:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVersion:Lcom/visionobjects/resourcemanager/util/Version;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mLanguagesMd5Map:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public addLanguage(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "languageKey"    # Ljava/lang/String;
    .param p2, "md5"    # Ljava/lang/String;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mLanguagesMd5Map:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    return-void
.end method

.method public getDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mDate:Ljava/util/Date;

    return-object v0
.end method

.method public getDelay()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mDelay:I

    return v0
.end method

.method public getLanguageKeys()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mLanguagesMd5Map:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getLanguageMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "languageKey"    # Ljava/lang/String;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mLanguagesMd5Map:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Lcom/visionobjects/resourcemanager/util/Version;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    return-object v0
.end method

.method public setDate(Ljava/util/Date;)V
    .locals 0
    .param p1, "date"    # Ljava/util/Date;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mDate:Ljava/util/Date;

    .line 34
    return-void
.end method

.method public setDelay(I)V
    .locals 0
    .param p1, "delay"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mDelay:I

    .line 44
    return-void
.end method

.method public setVersion(Lcom/visionobjects/resourcemanager/util/Version;)V
    .locals 0
    .param p1, "version"    # Lcom/visionobjects/resourcemanager/util/Version;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesData;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    .line 24
    return-void
.end method
