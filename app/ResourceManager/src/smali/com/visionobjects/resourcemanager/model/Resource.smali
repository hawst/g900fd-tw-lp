.class public Lcom/visionobjects/resourcemanager/model/Resource;
.super Ljava/lang/Object;
.source "Resource.java"


# instance fields
.field private folder:Ljava/lang/String;

.field private md5:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private version:Lcom/visionobjects/resourcemanager/util/Version;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "md5"    # Ljava/lang/String;
    .param p3, "folder"    # Ljava/lang/String;
    .param p4, "version"    # Lcom/visionobjects/resourcemanager/util/Version;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/Resource;->name:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/model/Resource;->md5:Ljava/lang/String;

    .line 20
    iput-object p3, p0, Lcom/visionobjects/resourcemanager/model/Resource;->folder:Ljava/lang/String;

    .line 21
    iput-object p4, p0, Lcom/visionobjects/resourcemanager/model/Resource;->version:Lcom/visionobjects/resourcemanager/util/Version;

    .line 22
    return-void
.end method


# virtual methods
.method public getFolder()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/Resource;->folder:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/Resource;->md5:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/Resource;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Lcom/visionobjects/resourcemanager/util/Version;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/Resource;->version:Lcom/visionobjects/resourcemanager/util/Version;

    return-object v0
.end method
