.class public Lcom/visionobjects/resourcemanager/model/ServerConfig;
.super Ljava/lang/Object;
.source "ServerConfig.java"


# static fields
.field public static final FILES_TXT:Ljava/lang/String; = "files.txt"

.field public static final LATEST_TXT:Ljava/lang/String; = "latest.txt"

.field public static final RESOURCES_TXT:Ljava/lang/String; = "resources.txt"

.field public static SERVER:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isServerSet()Z
    .locals 2

    .prologue
    .line 20
    sget-object v0, Lcom/visionobjects/resourcemanager/model/ServerConfig;->SERVER:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/visionobjects/resourcemanager/model/ServerConfig;->SERVER:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setServerUrl(Ljava/lang/String;)V
    .locals 2
    .param p0, "Server"    # Ljava/lang/String;

    .prologue
    .line 12
    const-string v0, "/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 13
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/model/ServerConfig;->SERVER:Ljava/lang/String;

    .line 16
    :goto_0
    return-void

    .line 15
    :cond_0
    sput-object p0, Lcom/visionobjects/resourcemanager/model/ServerConfig;->SERVER:Ljava/lang/String;

    goto :goto_0
.end method
