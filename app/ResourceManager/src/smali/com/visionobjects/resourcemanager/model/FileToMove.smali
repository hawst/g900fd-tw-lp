.class public Lcom/visionobjects/resourcemanager/model/FileToMove;
.super Ljava/lang/Object;
.source "FileToMove.java"


# instance fields
.field private mDestFilename:Ljava/lang/String;

.field private mMd5:Ljava/lang/String;

.field private mSize:I

.field private mSrcFilename:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;)V
    .locals 1
    .param p1, "srcFilename"    # Ljava/lang/String;
    .param p2, "destFilename"    # Ljava/lang/String;
    .param p3, "fileInfo"    # Lcom/visionobjects/resourcemanager/model/ResFileInfo;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSrcFilename:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mDestFilename:Ljava/lang/String;

    .line 31
    invoke-virtual {p3}, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->getMd5()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mMd5:Ljava/lang/String;

    .line 32
    invoke-virtual {p3}, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->getSize()I

    move-result v0

    iput v0, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSize:I

    .line 33
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "srcFilename"    # Ljava/lang/String;
    .param p2, "destFilename"    # Ljava/lang/String;
    .param p3, "md5"    # Ljava/lang/String;
    .param p4, "size"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSrcFilename:Ljava/lang/String;

    .line 50
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mDestFilename:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mMd5:Ljava/lang/String;

    .line 52
    iput p4, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSize:I

    .line 53
    return-void
.end method


# virtual methods
.method public getDestFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mDestFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getMd5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mMd5:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 104
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSize:I

    return v0
.end method

.method public getSrcFilename()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSrcFilename:Ljava/lang/String;

    return-object v0
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 63
    iput p1, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSize:I

    .line 64
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "file[name:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mSrcFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "][dest:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mDestFilename:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "][md5:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/model/FileToMove;->mMd5:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
