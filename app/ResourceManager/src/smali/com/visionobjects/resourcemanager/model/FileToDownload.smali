.class public Lcom/visionobjects/resourcemanager/model/FileToDownload;
.super Lcom/visionobjects/resourcemanager/model/FileToMove;
.source "FileToDownload.java"


# instance fields
.field private downloadedSize:I

.field private uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;)V
    .locals 1
    .param p1, "srcFilename"    # Ljava/lang/String;
    .param p2, "destFilename"    # Ljava/lang/String;
    .param p3, "md5"    # Ljava/lang/String;
    .param p4, "size"    # I
    .param p5, "uri"    # Landroid/net/Uri;

    .prologue
    .line 15
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/visionobjects/resourcemanager/model/FileToMove;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lcom/visionobjects/resourcemanager/model/FileToDownload;->downloadedSize:I

    .line 16
    iput-object p5, p0, Lcom/visionobjects/resourcemanager/model/FileToDownload;->uri:Landroid/net/Uri;

    .line 17
    return-void
.end method


# virtual methods
.method public getDownloadedSize()I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/FileToDownload;->downloadedSize:I

    return v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FileToDownload;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method public setDownloadedSize(I)V
    .locals 0
    .param p1, "downloadedSize"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/visionobjects/resourcemanager/model/FileToDownload;->downloadedSize:I

    .line 32
    return-void
.end method
