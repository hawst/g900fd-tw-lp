.class public Lcom/visionobjects/resourcemanager/model/VODBConfiguration;
.super Ljava/lang/Object;
.source "VODBConfiguration.java"


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;

.field private static _instance:Lcom/visionobjects/resourcemanager/model/VODBConfiguration;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method public static getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->_instance:Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    invoke-direct {v0}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;-><init>()V

    sput-object v0, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->_instance:Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    .line 37
    :cond_0
    sget-object v0, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->_instance:Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    return-object v0
.end method


# virtual methods
.method public getFilesTxt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    const-string v0, "files.txt"

    .line 89
    .local v0, "files":Ljava/lang/String;
    return-object v0
.end method

.method public getLatestFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    const-string v0, "latest.txt"

    .line 76
    .local v0, "latest":Ljava/lang/String;
    return-object v0
.end method

.method public getResourcesTxtName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const-string v0, "resources.txt"

    .line 102
    .local v0, "resources":Ljava/lang/String;
    return-object v0
.end method

.method public getRootUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getServerUrl()Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "root":Ljava/lang/String;
    return-object v0
.end method

.method public getServerUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/visionobjects/resourcemanager/model/ServerConfig;->SERVER:Ljava/lang/String;

    .line 50
    .local v0, "server":Ljava/lang/String;
    return-object v0
.end method
