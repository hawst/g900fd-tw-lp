.class public Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;
.super Ljava/lang/Object;
.source "LangConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/model/LangConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LangMode"
.end annotation


# instance fields
.field public attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

.field public bDisableMultipleLine:Z

.field public bDoubleSizePunct:Z

.field public bInkScroll:Z

.field public bRTLOrientation:Z

.field public bShowMidLine:Z

.field public bSmartSpace:Z

.field public bUseFakeDot:Z

.field public bUserLexicon:Z

.field public baselineRatio:F

.field public boxNbLand:I

.field public boxNbPort:I

.field public candidateMode:I

.field public fs_bDisableMultipleLine:Z

.field public inputTypeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public isDefaultMode:Z

.field public midlineRatio:F

.field public refModeName:Ljava/lang/String;

.field public resourceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;",
            ">;"
        }
    .end annotation
.end field

.field public spaceCandList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public speedMemoryCompromise:Ljava/lang/Integer;

.field public speedQualityCompromise:Ljava/lang/Integer;

.field final synthetic this$0:Lcom/visionobjects/resourcemanager/model/LangConfig;


# direct methods
.method public constructor <init>(Lcom/visionobjects/resourcemanager/model/LangConfig;Ljava/lang/String;)V
    .locals 3
    .param p2, "modeName"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 119
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->this$0:Lcom/visionobjects/resourcemanager/model/LangConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->refModeName:Ljava/lang/String;

    .line 95
    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    .line 96
    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedQualityCompromise:Ljava/lang/Integer;

    .line 97
    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedMemoryCompromise:Ljava/lang/Integer;

    .line 98
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bDisableMultipleLine:Z

    .line 99
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->isDefaultMode:Z

    .line 100
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bInkScroll:Z

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->resourceList:Ljava/util/ArrayList;

    .line 102
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->spaceCandList:Ljava/util/ArrayList;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->inputTypeList:Ljava/util/ArrayList;

    .line 104
    iput v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->baselineRatio:F

    .line 105
    iput v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->midlineRatio:F

    .line 106
    iput v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->boxNbPort:I

    .line 107
    iput v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->boxNbLand:I

    .line 108
    iput v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->candidateMode:I

    .line 109
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bSmartSpace:Z

    .line 110
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bShowMidLine:Z

    .line 111
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bUserLexicon:Z

    .line 112
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bDoubleSizePunct:Z

    .line 113
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bUseFakeDot:Z

    .line 114
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bRTLOrientation:Z

    .line 116
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->fs_bDisableMultipleLine:Z

    .line 120
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->refModeName:Ljava/lang/String;

    .line 121
    new-instance v0, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    invoke-direct {v0, p1}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;-><init>(Lcom/visionobjects/resourcemanager/model/LangConfig;)V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    .line 122
    return-void
.end method


# virtual methods
.method public FillLangMode()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 126
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "default_mode:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 127
    .local v0, "str":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v3, "true"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->isDefaultMode:Z

    .line 131
    :cond_0
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "ink_scroll:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 132
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 135
    :cond_1
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bInkScroll:Z

    .line 136
    :cond_2
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "baseline_ratio:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 137
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 139
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->baselineRatio:F

    .line 140
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "midline_ratio:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 141
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 142
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->midlineRatio:F

    .line 143
    :cond_3
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "box_nb_portrait:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 144
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_4

    .line 145
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->boxNbPort:I

    .line 146
    :cond_4
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "box_nb_landscape:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 147
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_5

    .line 148
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->boxNbLand:I

    .line 150
    :cond_5
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "Result-Item-Type:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 151
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 153
    const-string v3, "C"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "c"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 154
    :cond_6
    iput v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->candidateMode:I

    .line 155
    :cond_7
    const-string v3, "W"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "w"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 156
    :cond_8
    const/4 v3, 0x2

    iput v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->candidateMode:I

    .line 158
    :cond_9
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "smart_space:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 159
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_b

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_a

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 162
    :cond_a
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bSmartSpace:Z

    .line 163
    :cond_b
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "mid_line:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 164
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_d

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 167
    :cond_c
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bShowMidLine:Z

    .line 168
    :cond_d
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "Use-User-Dictionary:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 169
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_f

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_e

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 172
    :cond_e
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bUserLexicon:Z

    .line 173
    :cond_f
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "SetSpeedQualityCompromise"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 174
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_10

    .line 175
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedQualityCompromise:Ljava/lang/Integer;

    .line 176
    :cond_10
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "SetSpeedMemoryCompromise"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 177
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_11

    .line 178
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedMemoryCompromise:Ljava/lang/Integer;

    .line 179
    :cond_11
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "fake_dot:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 180
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_13

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_12

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_13

    .line 183
    :cond_12
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bUseFakeDot:Z

    .line 184
    :cond_13
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "disable_multiple_lines:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 185
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_15

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_14

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 188
    :cond_14
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bDisableMultipleLine:Z

    .line 189
    :cond_15
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "double_size_punct:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 190
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_17

    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 193
    :cond_16
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bDoubleSizePunct:Z

    .line 196
    :cond_17
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->fs_bDisableMultipleLine:Z

    .line 197
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v4, "fullscreen_disable_multiple_lines:"

    invoke-virtual {v3, v4}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 198
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_1a

    .line 199
    const-string v3, "1"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string v3, "T"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string v3, "t"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_19

    :cond_18
    move v1, v2

    :cond_19
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->fs_bDisableMultipleLine:Z

    .line 201
    :cond_1a
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    const-string v3, "orientation:"

    invoke-virtual {v1, v3}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "str":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 202
    .restart local v0    # "str":Ljava/lang/String;
    if-eqz v0, :cond_1b

    const-string v1, "RTL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 203
    iput-boolean v2, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bRTLOrientation:Z

    .line 204
    :cond_1b
    return-void
.end method

.method public bDisableMultipleLine()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bDisableMultipleLine:Z

    return v0
.end method

.method public bDoubleSizePunct()Z
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bDoubleSizePunct:Z

    return v0
.end method

.method public bInkScroll()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bInkScroll:Z

    return v0
.end method

.method public bShowMidLine()Z
    .locals 1

    .prologue
    .line 288
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bShowMidLine:Z

    return v0
.end method

.method public bSmartSpace()Z
    .locals 1

    .prologue
    .line 283
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bSmartSpace:Z

    return v0
.end method

.method public bUseFakeDot()Z
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bUseFakeDot:Z

    return v0
.end method

.method public bUserLexicon()Z
    .locals 1

    .prologue
    .line 293
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bUserLexicon:Z

    return v0
.end method

.method public baselineRatio()F
    .locals 1

    .prologue
    .line 258
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->baselineRatio:F

    return v0
.end method

.method public boxNbLand()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->boxNbLand:I

    return v0
.end method

.method public boxNbPort()I
    .locals 1

    .prologue
    .line 268
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->boxNbPort:I

    return v0
.end method

.method public candidateMode()I
    .locals 1

    .prologue
    .line 278
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->candidateMode:I

    return v0
.end method

.method public fs_bDisableMultipleLine()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->fs_bDisableMultipleLine:Z

    return v0
.end method

.method public getRefModeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->refModeName:Ljava/lang/String;

    return-object v0
.end method

.method public inputTypeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->inputTypeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isDefaultMode()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->isDefaultMode:Z

    return v0
.end method

.method public isRTL()Z
    .locals 1

    .prologue
    .line 313
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->bRTLOrientation:Z

    return v0
.end method

.method public midlineRatio()F
    .locals 1

    .prologue
    .line 263
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->midlineRatio:F

    return v0
.end method

.method public resourceDirectoryList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 238
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->this$0:Lcom/visionobjects/resourcemanager/model/LangConfig;

    # getter for: Lcom/visionobjects/resourcemanager/model/LangConfig;->resDirList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/model/LangConfig;->access$000(Lcom/visionobjects/resourcemanager/model/LangConfig;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public resourceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->resourceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public spaceCandList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->spaceCandList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public speedMemoryCompromise()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedMemoryCompromise:Ljava/lang/Integer;

    return-object v0
.end method

.method public speedQualityCompromise()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->speedQualityCompromise:Ljava/lang/Integer;

    return-object v0
.end method
