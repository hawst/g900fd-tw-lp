.class public Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
.super Ljava/lang/Object;
.source "FilesTxtParser.java"


# static fields
.field private static final DATA_PART_COUNT:I = 0x3

.field private static final DBG:Z = false

.field public static final ERROR_FILE_IS_EMPTY:I = -0x1

.field public static final ERROR_FILE_NOT_FOUND:I = -0x2

.field public static final ERROR_INVALID_FILE_FORMAT:I = -0x5

.field public static final ERROR_INVALID_SIZE:I = -0x4

.field public static final ERROR_IO_EXCEPTION:I = -0x3

.field private static final PARSING_RADIX:I = 0xa

.field public static final SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FilesTxtParser"


# instance fields
.field private mError:I

.field private mFilesMd5:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mFilesSize:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesMd5:Ljava/util/HashMap;

    .line 59
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesSize:Ljava/util/HashMap;

    .line 102
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->parse(Ljava/io/InputStream;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesMd5:Ljava/util/HashMap;

    .line 59
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iput-object v3, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesSize:Ljava/util/HashMap;

    .line 69
    const/4 v1, 0x0

    .line 72
    .local v1, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    .end local v1    # "in":Ljava/io/FileInputStream;
    .local v2, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-direct {p0, v2}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->parse(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 84
    if-eqz v2, :cond_0

    .line 85
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_0
    move-object v1, v2

    .line 92
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    :cond_1
    :goto_0
    return-void

    .line 87
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move-object v1, v2

    .line 91
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_0

    .line 75
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v3, -0x2

    :try_start_3
    iput v3, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I

    .line 78
    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 84
    if-eqz v1, :cond_1

    .line 85
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 87
    :catch_2
    move-exception v0

    .line 89
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 82
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    .line 84
    :goto_2
    if-eqz v1, :cond_2

    .line 85
    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 90
    :cond_2
    :goto_3
    throw v3

    .line 87
    :catch_3
    move-exception v0

    .line 89
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 82
    .end local v0    # "e":Ljava/io/IOException;
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v3

    move-object v1, v2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .line 75
    .end local v1    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_4
    move-exception v0

    move-object v1, v2

    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v1    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private parse(Ljava/io/InputStream;)V
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 116
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 120
    .local v4, "r":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 121
    .local v1, "line":Ljava/lang/String;
    const/4 v2, 0x0

    .line 123
    .local v2, "lineCount":I
    :goto_0
    :try_start_0
    iget v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I

    if-nez v5, :cond_2

    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 125
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 126
    .local v3, "parts":[Ljava/lang/String;
    array-length v5, v3

    const/4 v6, 0x3

    if-ne v5, v6, :cond_0

    .line 128
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesMd5:Ljava/util/HashMap;

    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesSize:Ljava/util/HashMap;

    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x2

    aget-object v7, v3, v7

    const/16 v8, 0xa

    invoke-static {v7, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 136
    goto :goto_0

    .line 133
    :cond_0
    const/4 v5, -0x5

    iput v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 144
    .end local v3    # "parts":[Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 146
    .local v0, "e":Ljava/io/IOException;
    const/4 v5, -0x3

    :try_start_1
    iput v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I

    .line 147
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return-void

    .line 138
    :cond_2
    if-nez v2, :cond_1

    .line 140
    const/4 v5, -0x1

    :try_start_2
    iput v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 149
    :catch_1
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v5, -0x4

    :try_start_3
    iput v5, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I

    .line 152
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 156
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :catchall_0
    move-exception v5

    throw v5
.end method


# virtual methods
.method public getError()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I

    return v0
.end method

.method public getMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesMd5:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 189
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesMd5:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getSize(Ljava/lang/String;)I
    .locals 1
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    .line 213
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mFilesSize:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public hasFailed()Z
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->mError:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
