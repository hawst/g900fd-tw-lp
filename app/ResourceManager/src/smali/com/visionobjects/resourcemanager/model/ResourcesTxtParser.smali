.class public Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
.super Ljava/lang/Object;
.source "ResourcesTxtParser.java"


# static fields
.field public static final ERROR_FILE_IS_EMPTY:I = -0x1

.field public static final ERROR_FILE_NOT_FOUND:I = -0x2

.field public static final ERROR_INVALID_DELAY:I = -0x4

.field public static final ERROR_IO_EXCEPTION:I = -0x3

.field public static final ERROR_OTHER_UNKNOWN:I = -0x6

.field public static final ERROR_OUT_OF_BOUND_ARRAY:I = -0x5

.field public static final SUCCESS:I


# instance fields
.field private mDate:Ljava/util/Date;

.field private mDelay:I

.field private mError:I

.field private mLangsMd5:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mVersion:Lcom/visionobjects/resourcemanager/util/Version;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mLangsMd5:Ljava/util/HashMap;

    .line 85
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    iput-object v1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDate:Ljava/util/Date;

    .line 88
    :try_start_0
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->parse(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/Exception;
    const/4 v1, -0x6

    iput v1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 7
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    const/4 v5, -0x2

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    iput-object v4, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mLangsMd5:Ljava/util/HashMap;

    .line 41
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    iput-object v4, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDate:Ljava/util/Date;

    .line 42
    if-nez p1, :cond_1

    .line 44
    iput v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 49
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 51
    new-instance v4, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    iput-object v4, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDate:Ljava/util/Date;

    .line 53
    const/4 v2, 0x0

    .line 56
    .local v2, "in":Ljava/io/FileInputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    .end local v2    # "in":Ljava/io/FileInputStream;
    .local v3, "in":Ljava/io/FileInputStream;
    :try_start_1
    invoke-direct {p0, v3}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->parse(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 72
    if-eqz v3, :cond_0

    .line 73
    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 59
    .end local v0    # "e":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    :catch_1
    move-exception v0

    .line 61
    .local v0, "e":Ljava/io/FileNotFoundException;
    :goto_1
    const/4 v4, -0x2

    :try_start_3
    iput v4, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    if-eqz v2, :cond_0

    .line 73
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 75
    :catch_2
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 63
    .end local v0    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v0

    .line 66
    .local v0, "e":Ljava/lang/Exception;
    :goto_2
    const/4 v4, -0x6

    :try_start_5
    iput v4, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 72
    if-eqz v2, :cond_0

    .line 73
    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 75
    :catch_4
    move-exception v0

    .line 77
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 70
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 72
    :goto_3
    if-eqz v2, :cond_2

    .line 73
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 78
    :cond_2
    :goto_4
    throw v4

    .line 75
    :catch_5
    move-exception v0

    .line 77
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 70
    .end local v0    # "e":Ljava/io/IOException;
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v4

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 63
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :catch_6
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .line 59
    .end local v2    # "in":Ljava/io/FileInputStream;
    .restart local v3    # "in":Ljava/io/FileInputStream;
    :catch_7
    move-exception v0

    move-object v2, v3

    .end local v3    # "in":Ljava/io/FileInputStream;
    .restart local v2    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method private parse(Ljava/io/InputStream;)V
    .locals 9
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v8, 0x1

    .line 99
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    invoke-direct {v5, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 103
    .local v4, "r":Ljava/io/BufferedReader;
    const/4 v1, 0x0

    .line 104
    .local v1, "line":Ljava/lang/String;
    const/4 v2, 0x0

    .line 106
    .local v2, "lineCount":I
    :goto_0
    :try_start_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 108
    if-nez v2, :cond_0

    .line 111
    new-instance v5, Lcom/visionobjects/resourcemanager/util/Version;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/visionobjects/resourcemanager/util/Version;-><init>(Ljava/lang/String;)V

    iput-object v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    .line 124
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 113
    :cond_0
    if-ne v2, v8, :cond_2

    .line 116
    const/16 v5, 0xa

    invoke-static {v1, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    iput v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDelay:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 133
    :catch_0
    move-exception v0

    .line 135
    .local v0, "e":Ljava/io/IOException;
    const/4 v5, -0x3

    iput v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    .line 136
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    .line 148
    .end local v0    # "e":Ljava/io/IOException;
    :cond_1
    :goto_2
    return-void

    .line 121
    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\t"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 122
    .local v3, "parts":[Ljava/lang/String;
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mLangsMd5:Ljava/util/HashMap;

    const/4 v6, 0x0

    aget-object v6, v3, v6

    const/4 v7, 0x1

    aget-object v7, v3, v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 138
    .end local v3    # "parts":[Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 140
    .local v0, "e":Ljava/lang/NumberFormatException;
    const/4 v5, -0x4

    iput v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    .line 141
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_2

    .line 127
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    :cond_3
    if-nez v2, :cond_1

    .line 129
    const/4 v5, -0x1

    :try_start_2
    iput v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 143
    :catch_2
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const/4 v5, -0x5

    iput v5, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    .line 146
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public failed()Z
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDelay()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDelay:I

    return v0
.end method

.method public getError()I
    .locals 1

    .prologue
    .line 170
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    return v0
.end method

.method public getLangs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mLangsMd5:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 190
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mLangsMd5:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Lcom/visionobjects/resourcemanager/util/Version;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    return-object v0
.end method

.method public isOutdated()Z
    .locals 3

    .prologue
    .line 152
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 153
    .local v0, "calendar":Ljava/util/Calendar;
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDate:Ljava/util/Date;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 154
    const/4 v1, 0x5

    iget v2, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mDelay:I

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 155
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v1

    return v1
.end method

.method public setFailed(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 195
    const/4 v0, -0x3

    iput v0, p0, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->mError:I

    .line 196
    return-void
.end method
