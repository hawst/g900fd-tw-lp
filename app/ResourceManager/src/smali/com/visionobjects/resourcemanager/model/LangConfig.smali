.class public Lcom/visionobjects/resourcemanager/model/LangConfig;
.super Ljava/lang/Object;
.source "LangConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;,
        Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;,
        Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;,
        Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;
    }
.end annotation


# static fields
.field public static final CANDIDATE_CHAR:I = 0x1

.field public static final CANDIDATE_WORD:I = 0x2

.field public static final LANG_EXTENSION:Ljava/lang/String; = ".lang"

.field protected static final LANG_FORMAT_ORIENTATION_RTL:Ljava/lang/String; = "RTL"

.field protected static final LANG_FORMAT_VERSION_1_0:Ljava/lang/String; = "1.0"

.field private static final LANG_TAG_DIR:I = 0x3

.field private static final LANG_TAG_MODE:I = 0x2

.field private static final LANG_TAG_NONE:I = 0x0

.field private static final LANG_TAG_ROOT:I = 0x1

.field private static final TAG:Ljava/lang/String;

.field protected static final _langAttrDoubleSizePunct:Ljava/lang/String; = "double_size_punct:"

.field protected static final _langAttrModeBaseLine:Ljava/lang/String; = "baseline_ratio:"

.field protected static final _langAttrModeBoxNbLand:Ljava/lang/String; = "box_nb_landscape:"

.field protected static final _langAttrModeBoxNbPort:Ljava/lang/String; = "box_nb_portrait:"

.field protected static final _langAttrModeCandidate:Ljava/lang/String; = "Result-Item-Type:"

.field protected static final _langAttrModeDefaultMode:Ljava/lang/String; = "default_mode:"

.field protected static final _langAttrModeDisableMLines:Ljava/lang/String; = "disable_multiple_lines:"

.field protected static final _langAttrModeFakeDot:Ljava/lang/String; = "fake_dot:"

.field protected static final _langAttrModeFullscreenDisableMLines:Ljava/lang/String; = "fullscreen_disable_multiple_lines:"

.field protected static final _langAttrModeInkScroll:Ljava/lang/String; = "ink_scroll:"

.field protected static final _langAttrModeMidLine:Ljava/lang/String; = "midline_ratio:"

.field protected static final _langAttrModeOrientation:Ljava/lang/String; = "orientation:"

.field protected static final _langAttrModeShowMidLine:Ljava/lang/String; = "mid_line:"

.field protected static final _langAttrModeSmartSpace:Ljava/lang/String; = "smart_space:"

.field protected static final _langAttrModeSpeedMem:Ljava/lang/String; = "SetSpeedMemoryCompromise"

.field protected static final _langAttrModeSpeedQual:Ljava/lang/String; = "SetSpeedQualityCompromise"

.field protected static final _langAttrModeUserLexicon:Ljava/lang/String; = "Use-User-Dictionary:"

.field protected static final _langAttrResCandType:Ljava/lang/String; = "single_candidate"

.field protected static final _langTagInputType:Ljava/lang/String; = "Input_Type:"

.field protected static final _langTagMode:Ljava/lang/String; = "Name:"

.field protected static final _langTagName:Ljava/lang/String; = "Language-Name:"

.field protected static final _langTagResource:Ljava/lang/String; = "AddResource"

.field protected static final _langTagResourceDir:Ljava/lang/String; = "AddResDir"

.field protected static final _langTagRoot_1_0:Ljava/lang/String; = "Language-Version:"

.field protected static final _langTagRoot_1_1:Ljava/lang/String; = "Format-Version:"

.field protected static final _langTagSpaceCandidate:Ljava/lang/String; = "space_candidate:"


# instance fields
.field private _currentTag:I

.field private langName:Ljava/lang/String;

.field private final modeList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;",
            ">;"
        }
    .end annotation
.end field

.field private parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

.field private final resDirList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/visionobjects/resourcemanager/model/LangConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "fileDir"    # Ljava/lang/String;
    .param p2, "local"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 334
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317
    iput-object v5, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->langName:Ljava/lang/String;

    .line 318
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    iput-object v4, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->modeList:Ljava/util/LinkedList;

    .line 319
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->resDirList:Ljava/util/ArrayList;

    .line 320
    iput-object v5, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    .line 321
    const/4 v4, 0x0

    iput v4, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->_currentTag:I

    .line 335
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".lang"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 336
    .local v3, "voimConfigFile":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    .local v1, "langFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 339
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/FileReader;

    invoke-direct {v4, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 342
    .local v0, "filelist_reader":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    .local v2, "str":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 344
    invoke-direct {p0, v2}, Lcom/visionobjects/resourcemanager/model/LangConfig;->parseLangLine(Ljava/lang/String;)V

    goto :goto_0

    .line 346
    :cond_0
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    if-eqz v4, :cond_1

    .line 347
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->modeList:Ljava/util/LinkedList;

    iget-object v5, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    invoke-virtual {v4, v5}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_1
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 354
    .end local v0    # "filelist_reader":Ljava/io/BufferedReader;
    .end local v2    # "str":Ljava/lang/String;
    :goto_1
    return-void

    .line 352
    :cond_2
    sget-object v4, Lcom/visionobjects/resourcemanager/model/LangConfig;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Impossible to find .lang for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/visionobjects/resourcemanager/model/LangConfig;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/model/LangConfig;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->resDirList:Ljava/util/ArrayList;

    return-object v0
.end method

.method private parseLangLine(Ljava/lang/String;)V
    .locals 19
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 361
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    const-string v18, ";"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-nez v17, :cond_0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    const-string v18, "#"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->_currentTag:I

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    goto :goto_0

    .line 368
    :pswitch_0
    const-string v17, "Language-Name:"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 370
    const-string v17, ":"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 371
    .local v12, "strings":[Ljava/lang/String;
    const/16 v17, 0x1

    aget-object v14, v12, v17

    .line 372
    .local v14, "value":Ljava/lang/String;
    const-string v17, " "

    const-string v18, ""

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v14

    .line 373
    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->langName:Ljava/lang/String;

    .line 374
    const/16 v17, 0x3

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/visionobjects/resourcemanager/model/LangConfig;->_currentTag:I

    goto :goto_0

    .line 378
    .end local v12    # "strings":[Ljava/lang/String;
    .end local v14    # "value":Ljava/lang/String;
    :pswitch_1
    const-string v17, "AddResDir"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 380
    const-string v17, "AddResDir"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 381
    .local v7, "inputModePos":I
    if-lez v7, :cond_0

    .line 383
    new-instance v10, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;-><init>(Lcom/visionobjects/resourcemanager/model/LangConfig;)V

    .line 384
    .local v10, "res":Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;
    const-string v17, "AddResDir"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    .line 386
    .local v13, "tempo":Ljava/lang/String;
    const-string v17, "."

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 387
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->langName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput-object v0, v10, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;->name:Ljava/lang/String;

    .line 393
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->resDirList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 391
    :cond_2
    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v10, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;->name:Ljava/lang/String;

    goto :goto_1

    .line 396
    .end local v7    # "inputModePos":I
    .end local v10    # "res":Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;
    .end local v13    # "tempo":Ljava/lang/String;
    :cond_3
    const-string v17, ""

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 397
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/visionobjects/resourcemanager/model/LangConfig;->_currentTag:I

    goto/16 :goto_0

    .line 400
    :pswitch_2
    const-string v17, "Name:"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_0

    .line 402
    const-string v17, ": "

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 403
    .restart local v7    # "inputModePos":I
    if-lez v7, :cond_0

    .line 405
    add-int/lit8 v17, v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 406
    .local v6, "inputModeName":Ljava/lang/String;
    new-instance v17, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;-><init>(Lcom/visionobjects/resourcemanager/model/LangConfig;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    .line 407
    const/16 v17, 0x2

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/visionobjects/resourcemanager/model/LangConfig;->_currentTag:I

    goto/16 :goto_0

    .line 412
    .end local v6    # "inputModeName":Ljava/lang/String;
    .end local v7    # "inputModePos":I
    :pswitch_3
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v17

    if-nez v17, :cond_4

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->FillLangMode()V

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->clear()V

    .line 416
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->modeList:Ljava/util/LinkedList;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 417
    const/16 v17, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    .line 418
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/visionobjects/resourcemanager/model/LangConfig;->_currentTag:I

    goto/16 :goto_0

    .line 421
    :cond_4
    const-string v17, "SetSpeedMemoryCompromise"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    const-string v17, "Use-User-Dictionary:"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 429
    const-string v17, "AddResource"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 432
    new-instance v10, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;-><init>(Lcom/visionobjects/resourcemanager/model/LangConfig;)V

    .line 433
    .restart local v10    # "res":Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;
    const-string v17, "AddResource"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    iput-object v0, v10, Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;->name:Ljava/lang/String;

    .line 434
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 435
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->resourceList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 438
    .end local v10    # "res":Lcom/visionobjects/resourcemanager/model/LangConfig$LangResource;
    :cond_5
    const-string v17, "single_candidate"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 445
    const-string v17, "space_candidate:"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 447
    const-string v17, "space_candidate:"

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int/lit8 v15, v17, 0x1

    .line 448
    .local v15, "valuePos":I
    add-int/lit8 v17, v15, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v15, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 449
    .local v3, "delimiter":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 450
    .restart local v12    # "strings":[Ljava/lang/String;
    array-length v11, v12

    .line 451
    .local v11, "size":I
    const/4 v5, 0x1

    .local v5, "idx":I
    :goto_2
    if-ge v5, v11, :cond_0

    .line 453
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    if-eqz v17, :cond_6

    .line 454
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->spaceCandList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    aget-object v18, v12, v5

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 458
    .end local v3    # "delimiter":Ljava/lang/String;
    .end local v5    # "idx":I
    .end local v11    # "size":I
    .end local v12    # "strings":[Ljava/lang/String;
    .end local v15    # "valuePos":I
    :cond_7
    const-string v17, "Input_Type:"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 460
    const-string v17, " "

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 461
    .restart local v12    # "strings":[Ljava/lang/String;
    array-length v11, v12

    .line 462
    .restart local v11    # "size":I
    const/4 v5, 0x1

    .restart local v5    # "idx":I
    :goto_3
    if-ge v5, v11, :cond_0

    .line 464
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    if-eqz v17, :cond_8

    .line 465
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->inputTypeList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    aget-object v18, v12, v5

    invoke-virtual/range {v17 .. v18}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 462
    :cond_8
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 469
    .end local v5    # "idx":I
    .end local v11    # "size":I
    .end local v12    # "strings":[Ljava/lang/String;
    :cond_9
    const-string v17, "Configuration-Script:"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    .line 475
    const-string v17, " "

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 476
    .restart local v12    # "strings":[Ljava/lang/String;
    const/4 v8, 0x0

    .line 477
    .local v8, "key":Ljava/lang/String;
    const/4 v14, 0x0

    .line 478
    .restart local v14    # "value":Ljava/lang/String;
    move-object v2, v12

    .local v2, "arr$":[Ljava/lang/String;
    array-length v9, v2

    .local v9, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_4
    if-ge v4, v9, :cond_c

    aget-object v16, v2, v4

    .line 479
    .local v16, "word":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v17

    if-eqz v17, :cond_a

    .line 481
    if-nez v8, :cond_b

    .line 483
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    .line 478
    :cond_a
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 485
    :cond_b
    if-nez v14, :cond_a

    .line 487
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v14

    .line 491
    .end local v16    # "word":Ljava/lang/String;
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 492
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig;->parsingLangMode:Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;->attr:Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8, v14}, Lcom/visionobjects/resourcemanager/model/LangConfig$Attributes;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 363
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getLangModes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$LangMode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->modeList:Ljava/util/LinkedList;

    return-object v0
.end method

.method public getResourceDir()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/LangConfig$IResource;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/LangConfig;->resDirList:Ljava/util/ArrayList;

    return-object v0
.end method
