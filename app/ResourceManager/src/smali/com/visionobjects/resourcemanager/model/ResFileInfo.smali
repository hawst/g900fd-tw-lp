.class public Lcom/visionobjects/resourcemanager/model/ResFileInfo;
.super Ljava/lang/Object;
.source "ResFileInfo.java"


# instance fields
.field private md5:Ljava/lang/String;

.field private name:Ljava/lang/String;

.field private size:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "md5"    # Ljava/lang/String;
    .param p3, "size"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->name:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->md5:Ljava/lang/String;

    .line 16
    iput p3, p0, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->size:I

    .line 17
    return-void
.end method


# virtual methods
.method public getMd5()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->md5:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->size:I

    return v0
.end method
