.class public final Lcom/visionobjects/resourcemanager/model/LocalConfig;
.super Ljava/lang/Object;
.source "LocalConfig.java"


# static fields
.field public static final CHECKED:Ljava/lang/String; = "checked"

.field private static final DBG:Z = false

.field public static final EQUATION:Ljava/lang/String; = "equation"

.field public static final FILES_TXT:Ljava/lang/String; = "files.txt"

.field public static final LANG_FILE:Ljava/lang/String; = "langfile"

.field public static final LIB:Ljava/lang/String; = "lib"

.field public static final MUL:Ljava/lang/String; = "mul"

.field public static final RESOURCES_TXT:Ljava/lang/String; = "resources.txt"

.field private static final TAG:Ljava/lang/String;

.field public static final TEST_CONFIG_FILE:Ljava/lang/String; = "configuration.txt"

.field public static final TEST_DIR:Ljava/lang/String;

.field public static final VO_PREFIX:Ljava/lang/String; = "tempoVoFiles"

.field public static sConnectionExtendedTimeout:I

.field public static sConnectionTimeout:I

.field public static sLocalPath:Ljava/lang/String;

.field public static sPreloadPath:Ljava/lang/String;

.field public static sUseLastAvailableVersion:Z

.field public static sUseLibFolder:Z

.field public static sUsePreloadPath:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/VODB_debug/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->TEST_DIR:Ljava/lang/String;

    .line 43
    const-class v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->TAG:Ljava/lang/String;

    .line 50
    sput-boolean v2, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUsePreloadPath:Z

    .line 52
    sput-boolean v2, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUseLibFolder:Z

    .line 54
    const/4 v0, 0x1

    sput-boolean v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUseLastAvailableVersion:Z

    .line 57
    const/16 v0, 0x5dc

    sput v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sConnectionTimeout:I

    .line 59
    const/16 v0, 0x1d4c

    sput v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sConnectionExtendedTimeout:I

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    return-void
.end method

.method public static getLocalPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    sget-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sLocalPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sLocalPath:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x6

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/VODB/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sLocalPath:Ljava/lang/String;

    .line 86
    :cond_1
    sget-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sLocalPath:Ljava/lang/String;

    return-object v0
.end method

.method public static final getPreloadPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sPreloadPath:Ljava/lang/String;

    return-object v0
.end method
