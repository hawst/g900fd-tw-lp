.class Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ResourceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/ResourceManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateReceiver"
.end annotation


# static fields
.field private static final DBG:Z = true


# instance fields
.field private final TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;


# direct methods
.method private constructor <init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V
    .locals 1

    .prologue
    .line 219
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 225
    const-class v0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;Lcom/visionobjects/resourcemanager/ResourceManagerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;
    .param p2, "x1"    # Lcom/visionobjects/resourcemanager/ResourceManagerService$1;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x3

    .line 235
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "> onReceive ( numberLanguagesProccessing : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$300()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$400()Ljava/util/ArrayList;

    move-result-object v6

    monitor-enter v6

    .line 239
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 241
    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$300()I

    move-result v5

    if-gt v5, v8, :cond_0

    iget-object v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->mPrepareFinished:Z
    invoke-static {v5}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$000(Lcom/visionobjects/resourcemanager/ResourceManagerService;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 243
    :cond_0
    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$400()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    :cond_1
    :goto_0
    monitor-exit v6

    .line 280
    return-void

    .line 247
    :cond_2
    # operator++ for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$308()I

    .line 248
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    # invokes: Lcom/visionobjects/resourcemanager/ResourceManagerService;->makeUpdate(Landroid/content/Intent;Landroid/content/Context;)V
    invoke-static {v5, p2, p1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$500(Lcom/visionobjects/resourcemanager/ResourceManagerService;Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 251
    :cond_3
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_RESULT:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 253
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 254
    .local v2, "lg":Ljava/lang/String;
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$600(Lcom/visionobjects/resourcemanager/ResourceManagerService;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;

    .line 255
    .local v4, "toUnregister":Lcom/visionobjects/resourcemanager/core/LanguageUpdater;
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->unregisterBroadCastReceiver()V

    .line 256
    :cond_4
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;
    invoke-static {v5}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$600(Lcom/visionobjects/resourcemanager/ResourceManagerService;)Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    invoke-static {v2}, Lcom/visionobjects/resourcemanager/util/Utils;->isSpecificLanguage(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 259
    # operator-- for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$310()I

    .line 261
    :cond_5
    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$400()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$300()I

    move-result v5

    if-ge v5, v8, :cond_1

    .line 263
    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$400()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Intent;

    .line 264
    .local v3, "tempoItent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 265
    .local v0, "lang":Ljava/lang/String;
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->isCancelled(Ljava/lang/String;)Z

    move-result v1

    .line 266
    .local v1, "languageCancelled":Z
    :goto_1
    if-eqz v1, :cond_6

    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$400()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_6

    .line 268
    # getter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$400()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "tempoItent":Landroid/content/Intent;
    check-cast v3, Landroid/content/Intent;

    .line 269
    .restart local v3    # "tempoItent":Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v5

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->isCancelled(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 272
    :cond_6
    if-nez v1, :cond_1

    .line 274
    # operator++ for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$308()I

    .line 275
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    # invokes: Lcom/visionobjects/resourcemanager/ResourceManagerService;->makeUpdate(Landroid/content/Intent;Landroid/content/Context;)V
    invoke-static {v5, v3, p1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$500(Lcom/visionobjects/resourcemanager/ResourceManagerService;Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method
