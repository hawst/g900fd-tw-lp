.class public final Lcom/visionobjects/resourcemanager/ResourceManagerContract;
.super Ljava/lang/Object;
.source "ResourceManagerContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$VersionInfo;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Resource;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Mode;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Langs;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Downloading;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Updates;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Engine;,
        Lcom/visionobjects/resourcemanager/ResourceManagerContract$Refresh;
    }
.end annotation


# static fields
.field private static mAuthority:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "com.visionobjects.resourcemanager"

    sput-object v0, Lcom/visionobjects/resourcemanager/ResourceManagerContract;->mAuthority:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    return-void
.end method

.method public static final getAuthority()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/visionobjects/resourcemanager/ResourceManagerContract;->mAuthority:Ljava/lang/String;

    return-object v0
.end method

.method protected static final setAuthority(Ljava/lang/String;)V
    .locals 0
    .param p0, "authority"    # Ljava/lang/String;

    .prologue
    .line 22
    sput-object p0, Lcom/visionobjects/resourcemanager/ResourceManagerContract;->mAuthority:Ljava/lang/String;

    .line 23
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->initUriMatcher()V

    .line 24
    return-void
.end method
