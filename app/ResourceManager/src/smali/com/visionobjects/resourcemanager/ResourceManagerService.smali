.class public Lcom/visionobjects/resourcemanager/ResourceManagerService;
.super Landroid/app/Service;
.source "ResourceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;,
        Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;,
        Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final MAX_CONCURRENT_DOWNLOADS:I = 0x3

.field private static final MSG_VO_WAITING_PREPARATION_TO_BE_FINISHED:I = 0x1

.field private static numberLanguagesProccessing:I

.field private static waitingForDownloadLanguages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private isDeleteReceiverRegistered:Z

.field private isOnCancelReceiverRegistered:Z

.field private isUpdateDoneReceiverRegistered:Z

.field private isUpdateReceiverRegistered:Z

.field private mDeleteReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;

.field mHandler:Landroid/os/Handler;

.field private mLanguageNameHelper:Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;

.field private mLanguageUpdaterCreated:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/visionobjects/resourcemanager/core/LanguageUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private mOnCancelReceiver:Landroid/content/BroadcastReceiver;

.field private mPrepareFinished:Z

.field private mUpdateDoneReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

.field private mUpdateReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 46
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mPrepareFinished:Z

    .line 48
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isOnCancelReceiverRegistered:Z

    .line 50
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateReceiverRegistered:Z

    .line 52
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateDoneReceiverRegistered:Z

    .line 54
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isDeleteReceiverRegistered:Z

    .line 58
    new-instance v0, Lcom/visionobjects/resourcemanager/ResourceManagerService$1;

    invoke-direct {v0, p0}, Lcom/visionobjects/resourcemanager/ResourceManagerService$1;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mHandler:Landroid/os/Handler;

    .line 363
    return-void
.end method

.method static synthetic access$000(Lcom/visionobjects/resourcemanager/ResourceManagerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mPrepareFinished:Z

    return v0
.end method

.method static synthetic access$002(Lcom/visionobjects/resourcemanager/ResourceManagerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mPrepareFinished:Z

    return p1
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    return v0
.end method

.method static synthetic access$308()I
    .locals 2

    .prologue
    .line 32
    sget v0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    return v0
.end method

.method static synthetic access$310()I
    .locals 2

    .prologue
    .line 32
    sget v0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    add-int/lit8 v1, v0, -0x1

    sput v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    return v0
.end method

.method static synthetic access$400()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/visionobjects/resourcemanager/ResourceManagerService;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;
    .param p1, "x1"    # Landroid/content/Intent;
    .param p2, "x2"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->makeUpdate(Landroid/content/Intent;Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$600(Lcom/visionobjects/resourcemanager/ResourceManagerService;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$700(Lcom/visionobjects/resourcemanager/ResourceManagerService;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->runPrepare(Landroid/content/Context;)V

    return-void
.end method

.method private cleanFailedLangs()V
    .locals 0

    .prologue
    .line 152
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->cleanFailedLangs(Landroid/content/Context;)V

    .line 153
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->cleanDownloadFiles()V

    .line 154
    return-void
.end method

.method public static getWaitForDownloadingLanguage()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 424
    .local v2, "langs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v4, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    monitor-enter v4

    .line 426
    :try_start_0
    sget-object v3, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 428
    .local v0, "enqueueLanguage":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 430
    .end local v0    # "enqueueLanguage":Landroid/content/Intent;
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_0
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 431
    return-object v2
.end method

.method private makeUpdate(Landroid/content/Intent;Landroid/content/Context;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 346
    new-instance v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;

    invoke-direct {v0, p2}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;-><init>(Landroid/content/Context;)V

    .line 348
    .local v0, "updater":Lcom/visionobjects/resourcemanager/core/LanguageUpdater;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 349
    .local v1, "languageKey":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_PENDING_INTENT:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/app/PendingIntent;

    .line 350
    .local v2, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_NOTIFICATION_TITLE:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 351
    .local v3, "notificationTitle":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_SUCCESS_TEXT:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 352
    .local v4, "successText":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_FAILED_TEXT:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 354
    .local v5, "failedText":Ljava/lang/String;
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;

    invoke-virtual {v6, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    invoke-virtual/range {v0 .. v5}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->update(Ljava/lang/String;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->clearCache()V

    .line 358
    return-void
.end method

.method public static removeFromWaitingDownloadList(Ljava/lang/String;)V
    .locals 5
    .param p0, "langName"    # Ljava/lang/String;

    .prologue
    .line 436
    sget-object v3, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    monitor-enter v3

    .line 438
    :try_start_0
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 440
    .local v0, "enqueueLanguage":Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    sget-object v4, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 442
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 446
    .end local v0    # "enqueueLanguage":Landroid/content/Intent;
    :cond_1
    monitor-exit v3

    .line 447
    return-void

    .line 446
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private runPrepare(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 463
    new-instance v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-direct {v1, p1, v4}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;-><init>(Landroid/content/Context;Z)V

    .line 464
    .local v1, "remoteSystemHelper":Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;
    new-instance v2, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;

    invoke-virtual {v1}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getLangs()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageNameHelper:Lcom/visionobjects/resourcemanager/helper/LanguageNameHelper;

    .line 466
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 467
    .local v0, "msg":Landroid/os/Message;
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 468
    return-void
.end method

.method private runPrepareThread(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 451
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/visionobjects/resourcemanager/ResourceManagerService$2;

    invoke-direct {v1, p0, p1}, Lcom/visionobjects/resourcemanager/ResourceManagerService$2;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 459
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 87
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 90
    :try_start_0
    const-string v7, "com.visionobjects.resourcemanager.InitResourceManagerService"

    invoke-static {v7}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 91
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v7, 0x1

    new-array v4, v7, [Ljava/lang/Class;

    .line 92
    .local v4, "params":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    const/4 v7, 0x0

    const-class v8, Landroid/content/res/Resources;

    aput-object v8, v4, v7

    .line 93
    const-string v7, "Initialize"

    invoke-virtual {v1, v7, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 94
    .local v3, "methodInit":Ljava/lang/reflect/Method;
    const/4 v7, 0x0

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v3, v7, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    .end local v3    # "methodInit":Ljava/lang/reflect/Method;
    .end local v4    # "params":[Ljava/lang/Class;, "[Ljava/lang/Class<*>;"
    :goto_0
    new-instance v6, Landroid/content/IntentFilter;

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG:Ljava/lang/String;

    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 122
    .local v6, "updateFilter":Landroid/content/IntentFilter;
    new-instance v5, Landroid/content/IntentFilter;

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_RESULT:Ljava/lang/String;

    invoke-direct {v5, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 123
    .local v5, "updateDoneFilter":Landroid/content/IntentFilter;
    new-instance v2, Landroid/content/IntentFilter;

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_DELETE_LANG:Ljava/lang/String;

    invoke-direct {v2, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 124
    .local v2, "deleteFilter":Landroid/content/IntentFilter;
    new-instance v0, Landroid/content/IntentFilter;

    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_CANCEL_LANG_UPDATE:Ljava/lang/String;

    invoke-direct {v0, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, "cancelFilter":Landroid/content/IntentFilter;
    new-instance v7, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;

    invoke-direct {v7, p0}, Lcom/visionobjects/resourcemanager/ResourceManagerService$CancelReceiver;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mOnCancelReceiver:Landroid/content/BroadcastReceiver;

    .line 127
    new-instance v7, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-direct {v7, p0, v12}, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;Lcom/visionobjects/resourcemanager/ResourceManagerService$1;)V

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mUpdateReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    .line 128
    new-instance v7, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-direct {v7, p0, v12}, Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;Lcom/visionobjects/resourcemanager/ResourceManagerService$1;)V

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mUpdateDoneReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    .line 129
    new-instance v7, Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;

    invoke-direct {v7, p0, v12}, Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;Lcom/visionobjects/resourcemanager/ResourceManagerService$1;)V

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mDeleteReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;

    .line 130
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mUpdateReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-virtual {p0, v7, v6}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 131
    iput-boolean v11, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateReceiverRegistered:Z

    .line 132
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mUpdateDoneReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-virtual {p0, v7, v5}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 133
    iput-boolean v11, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateDoneReceiverRegistered:Z

    .line 134
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mDeleteReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;

    invoke-virtual {p0, v7, v2}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 135
    iput-boolean v11, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isDeleteReceiverRegistered:Z

    .line 136
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mOnCancelReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7, v0}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 137
    iput-boolean v11, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isOnCancelReceiverRegistered:Z

    .line 139
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;

    .line 141
    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->runPrepareThread(Landroid/content/Context;)V

    .line 147
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->cleanFailedLangs()V

    .line 148
    return-void

    .line 116
    .end local v0    # "cancelFilter":Landroid/content/IntentFilter;
    .end local v2    # "deleteFilter":Landroid/content/IntentFilter;
    .end local v5    # "updateDoneFilter":Landroid/content/IntentFilter;
    .end local v6    # "updateFilter":Landroid/content/IntentFilter;
    :catch_0
    move-exception v7

    goto :goto_0

    .line 111
    :catch_1
    move-exception v7

    goto :goto_0

    .line 106
    :catch_2
    move-exception v7

    goto :goto_0

    .line 101
    :catch_3
    move-exception v7

    goto :goto_0

    .line 96
    :catch_4
    move-exception v7

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 162
    iget-boolean v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateReceiverRegistered:Z

    if-eqz v4, :cond_0

    .line 164
    :try_start_0
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mUpdateReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-virtual {p0, v4}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    .line 169
    :goto_0
    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateReceiverRegistered:Z

    .line 171
    :cond_0
    iget-boolean v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateDoneReceiverRegistered:Z

    if-eqz v4, :cond_1

    .line 173
    :try_start_1
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mUpdateDoneReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$UpdateReceiver;

    invoke-virtual {p0, v4}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 178
    :goto_1
    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isUpdateDoneReceiverRegistered:Z

    .line 180
    :cond_1
    iget-boolean v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isDeleteReceiverRegistered:Z

    if-eqz v4, :cond_2

    .line 182
    :try_start_2
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mDeleteReceiver:Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;

    invoke-virtual {p0, v4}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    .line 187
    :goto_2
    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isDeleteReceiverRegistered:Z

    .line 189
    :cond_2
    iget-boolean v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mPrepareFinished:Z

    if-nez v4, :cond_3

    .line 190
    iget-boolean v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isOnCancelReceiverRegistered:Z

    if-eqz v4, :cond_3

    .line 192
    :try_start_3
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mOnCancelReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    .line 197
    :goto_3
    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isOnCancelReceiverRegistered:Z

    .line 201
    :cond_3
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 203
    .local v2, "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 205
    .local v1, "lang":Ljava/lang/String;
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;

    .line 206
    .local v3, "toUnregister":Lcom/visionobjects/resourcemanager/core/LanguageUpdater;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->unregisterBroadCastReceiver()V

    goto :goto_4

    .line 208
    .end local v1    # "lang":Ljava/lang/String;
    .end local v3    # "toUnregister":Lcom/visionobjects/resourcemanager/core/LanguageUpdater;
    :cond_5
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mLanguageUpdaterCreated:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    .line 210
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 214
    return-void

    .line 193
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v2    # "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :catch_0
    move-exception v4

    goto :goto_3

    .line 183
    :catch_1
    move-exception v4

    goto :goto_2

    .line 174
    :catch_2
    move-exception v4

    goto :goto_1

    .line 165
    :catch_3
    move-exception v4

    goto :goto_0
.end method

.method public prepareFinished()V
    .locals 4

    .prologue
    .line 324
    sget-object v2, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    monitor-enter v2

    .line 326
    :try_start_0
    iget-boolean v1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isOnCancelReceiverRegistered:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    .line 328
    :try_start_1
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->mOnCancelReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 333
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService;->isOnCancelReceiverRegistered:Z

    .line 335
    :cond_0
    :goto_1
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_1

    sget v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    const/4 v3, 0x3

    if-ge v1, v3, :cond_1

    .line 337
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->waitingForDownloadLanguages:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 338
    .local v0, "tempoItent":Landroid/content/Intent;
    sget v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/visionobjects/resourcemanager/ResourceManagerService;->numberLanguagesProccessing:I

    .line 339
    invoke-virtual {p0}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->makeUpdate(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 341
    .end local v0    # "tempoItent":Landroid/content/Intent;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 342
    return-void

    .line 329
    :catch_0
    move-exception v1

    goto :goto_0
.end method
