.class Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "ResourceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/ResourceManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteReceiver"
.end annotation


# static fields
.field private static final DBG:Z


# instance fields
.field private final TAG:Ljava/lang/String;

.field final synthetic this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;


# direct methods
.method private constructor <init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V
    .locals 1

    .prologue
    .line 363
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 368
    const-class v0, Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;->TAG:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;Lcom/visionobjects/resourcemanager/ResourceManagerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/visionobjects/resourcemanager/ResourceManagerService;
    .param p2, "x1"    # Lcom/visionobjects/resourcemanager/ResourceManagerService$1;

    .prologue
    .line 363
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/ResourceManagerService$DeleteReceiver;-><init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 376
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_DELETE_LANG:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 418
    :goto_0
    return-void

    .line 383
    :cond_0
    new-instance v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-direct {v0, p1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    .line 385
    .local v0, "fileSystemHelper":Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    sget-object v8, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 386
    .local v2, "lang":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 388
    .local v3, "path":Ljava/lang/String;
    const/4 v5, -0x1

    .line 389
    .local v5, "result":I
    const/4 v4, 0x0

    .line 391
    .local v4, "preload":Z
    if-eqz v3, :cond_2

    .line 393
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 395
    .local v1, "folderToDelete":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-static {v1}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 396
    const/4 v5, 0x0

    .line 399
    :cond_1
    new-instance v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    .end local v0    # "fileSystemHelper":Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
    invoke-direct {v0, p1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    .line 401
    .restart local v0    # "fileSystemHelper":Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
    invoke-virtual {v0, v2}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 402
    if-eqz v3, :cond_2

    invoke-static {v3}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->isPreloadedPath(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 404
    const/4 v4, 0x1

    .line 411
    .end local v1    # "folderToDelete":Ljava/io/File;
    :cond_2
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->clearCache()V

    .line 412
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 413
    .local v6, "returnIntent":Landroid/content/Intent;
    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_DELETE_LANG_RESULT:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 414
    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_DELETE_RESULT:Ljava/lang/String;

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 415
    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 416
    sget-object v7, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_PRELOADED:Ljava/lang/String;

    invoke-virtual {v6, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 417
    invoke-virtual {p1, v6}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method
