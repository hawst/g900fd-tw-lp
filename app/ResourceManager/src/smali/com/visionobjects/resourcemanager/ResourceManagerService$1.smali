.class Lcom/visionobjects/resourcemanager/ResourceManagerService$1;
.super Landroid/os/Handler;
.source "ResourceManagerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/ResourceManagerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;


# direct methods
.method constructor <init>(Lcom/visionobjects/resourcemanager/ResourceManagerService;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$1;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 61
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 68
    :goto_0
    return-void

    .line 63
    :pswitch_0
    const-string v0, "ResourceManagerService"

    const-string v1, "> Got message : MSG_VO_WAITING_PREPARATION_TO_BE_FINISHED"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$1;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->prepareFinished()V

    .line 65
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/ResourceManagerService$1;->this$0:Lcom/visionobjects/resourcemanager/ResourceManagerService;

    const/4 v1, 0x1

    # setter for: Lcom/visionobjects/resourcemanager/ResourceManagerService;->mPrepareFinished:Z
    invoke-static {v0, v1}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->access$002(Lcom/visionobjects/resourcemanager/ResourceManagerService;Z)Z

    goto :goto_0

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
