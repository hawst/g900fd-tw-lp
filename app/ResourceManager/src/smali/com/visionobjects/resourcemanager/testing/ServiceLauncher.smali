.class public Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;
.super Landroid/app/Activity;
.source "ServiceLauncher.java"


# instance fields
.field private mServiceIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->mServiceIntent:Landroid/content/Intent;

    .line 23
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->mServiceIntent:Landroid/content/Intent;

    const-string v1, "com.visionobjects.resourcemanager"

    const-string v2, "com.visionobjects.resourcemanager.ResourceManagerService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 24
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->mServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 25
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 32
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 33
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->mServiceIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->mServiceIntent:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/visionobjects/resourcemanager/testing/ServiceLauncher;->stopService(Landroid/content/Intent;)Z

    .line 35
    :cond_0
    return-void
.end method
