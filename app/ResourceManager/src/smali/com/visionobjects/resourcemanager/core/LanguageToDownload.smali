.class public Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
.super Ljava/lang/Object;
.source "LanguageToDownload.java"

# interfaces
.implements Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final DownloadManagerFailed:J

.field private mAbortedPending:Z

.field private final mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

.field private final mEnqueuedFiles:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/visionobjects/resourcemanager/model/FileToDownload;",
            ">;"
        }
    .end annotation
.end field

.field private final mFilesToCopy:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/FileToMove;",
            ">;"
        }
    .end annotation
.end field

.field private final mFilesToDownload:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/FileToDownload;",
            ">;"
        }
    .end annotation
.end field

.field private mId:I

.field private final mLanguageKey:Ljava/lang/String;

.field private mLanguageName:Ljava/lang/String;

.field private mLastProgress:I

.field private final mMonitorOnCancel:Ljava/lang/Object;

.field private final mMonitorOnUpdate:Ljava/lang/Object;

.field private mMonitorOnUpdateNeedToWait:Z

.field private mNotificationTitle:Ljava/lang/String;

.field private mPendingIntent:Landroid/app/PendingIntent;

.field private mProcessedSize:I

.field private mSuccess:Z

.field private mUpdateFailedText:Ljava/lang/String;

.field private mUpdateSuccessText:Ljava/lang/String;

.field private final mVersion:Lcom/visionobjects/resourcemanager/util/Version;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;Lcom/visionobjects/resourcemanager/core/DownloadService;)V
    .locals 2
    .param p1, "languageKey"    # Ljava/lang/String;
    .param p2, "version"    # Lcom/visionobjects/resourcemanager/util/Version;
    .param p3, "languageDownloader"    # Lcom/visionobjects/resourcemanager/core/DownloadService;

    .prologue
    const/4 v0, 0x1

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    .line 47
    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdateNeedToWait:Z

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->DownloadManagerFailed:J

    .line 71
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdate:Ljava/lang/Object;

    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnCancel:Ljava/lang/Object;

    .line 73
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToCopy:Ljava/util/List;

    .line 74
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToDownload:Ljava/util/List;

    .line 75
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    .line 77
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLanguageKey:Ljava/lang/String;

    .line 78
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    .line 79
    iput-object p3, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    .line 80
    return-void
.end method

.method private notifyProgress()V
    .locals 9

    .prologue
    .line 330
    iget v6, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mProcessedSize:I

    .line 331
    .local v6, "totalSize":I
    iget v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mProcessedSize:I

    .line 332
    .local v4, "processedSize":I
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 334
    .local v1, "fileId":J
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/resourcemanager/model/FileToDownload;

    .line 335
    .local v0, "file":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSize()I

    move-result v7

    add-int/2addr v6, v7

    .line 338
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getDownloadedSize()I

    move-result v7

    add-int/2addr v4, v7

    goto :goto_0

    .line 342
    .end local v0    # "file":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .end local v1    # "fileId":J
    :cond_1
    mul-int/lit8 v7, v4, 0x64

    div-int v5, v7, v6

    .line 343
    .local v5, "progress":I
    iget v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLastProgress:I

    if-gt v5, v7, :cond_2

    .line 349
    :goto_1
    return-void

    .line 346
    :cond_2
    iput v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLastProgress:I

    .line 348
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    iget-object v8, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLanguageKey:Ljava/lang/String;

    invoke-virtual {v7, v8, v5, v4, v6}, Lcom/visionobjects/resourcemanager/core/DownloadService;->notifyProgress(Ljava/lang/String;III)V

    goto :goto_1
.end method


# virtual methods
.method public abort()V
    .locals 8

    .prologue
    .line 214
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnCancel:Ljava/lang/Object;

    monitor-enter v4

    .line 216
    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mAbortedPending:Z

    .line 217
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 218
    .local v2, "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 220
    .local v1, "id":Ljava/lang/Long;
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/visionobjects/resourcemanager/core/DownloadService;->remove(J)I

    goto :goto_0

    .line 225
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "id":Ljava/lang/Long;
    .end local v2    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 224
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "ids":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    :cond_0
    const-wide/16 v5, -0x1

    const/4 v3, 0x0

    :try_start_1
    const-string v7, ""

    invoke-virtual {p0, v5, v6, v3, v7}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->fileDownloadFinished(JZLjava/lang/String;)V

    .line 225
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 229
    return-void
.end method

.method public downloadsFile(Ljava/lang/Long;)Z
    .locals 1
    .param p1, "fileId"    # Ljava/lang/Long;

    .prologue
    .line 298
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public fileDownloadFinished(JZLjava/lang/String;)V
    .locals 15
    .param p1, "id"    # J
    .param p3, "success"    # Z
    .param p4, "filename"    # Ljava/lang/String;

    .prologue
    .line 236
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/visionobjects/resourcemanager/model/FileToDownload;

    .line 237
    .local v11, "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    const/4 v8, 0x0

    .line 239
    .local v8, "f":Ljava/io/File;
    if-eqz v11, :cond_6

    if-eqz p3, :cond_6

    .line 241
    new-instance v8, Ljava/io/File;

    .end local v8    # "f":Ljava/io/File;
    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSrcFilename()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v8, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 244
    .restart local v8    # "f":Ljava/io/File;
    new-instance v13, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSrcFilename()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/visionobjects/resourcemanager/util/Utils;->TempoSuffix:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v13, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 245
    .local v13, "newFile":Ljava/io/File;
    invoke-virtual {v8, v13}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    .line 247
    if-eqz p4, :cond_5

    const-string v4, ""

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 249
    const-string v4, "file://"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 251
    const-string v4, "file://"

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, p4

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p4

    .line 254
    :cond_0
    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSrcFilename()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 256
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 257
    .local v3, "newPathFile":Ljava/lang/String;
    new-instance v2, Lcom/visionobjects/resourcemanager/model/FileToDownload;

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getDestFilename()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getMd5()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSize()I

    move-result v6

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getUri()Landroid/net/Uri;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/visionobjects/resourcemanager/model/FileToDownload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;)V

    .line 261
    .end local v3    # "newPathFile":Ljava/lang/String;
    .end local v11    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .local v2, "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    :goto_0
    invoke-static {v2}, Lcom/visionobjects/resourcemanager/util/Utils;->processLocalMove(Lcom/visionobjects/resourcemanager/model/FileToMove;)Z

    move-result p3

    .line 263
    invoke-virtual {v13}, Ljava/io/File;->delete()Z

    .line 264
    iget v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mProcessedSize:I

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSize()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mProcessedSize:I

    .line 266
    .end local v13    # "newFile":Ljava/io/File;
    :goto_1
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/visionobjects/resourcemanager/core/DownloadService;->remove(J)I

    .line 267
    if-eqz p3, :cond_4

    .line 269
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    :cond_1
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    if-nez v4, :cond_3

    .line 285
    iget-boolean v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    if-eqz v4, :cond_2

    .line 288
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLanguageKey:Ljava/lang/String;

    const/16 v6, 0x64

    iget v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mProcessedSize:I

    iget v14, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mProcessedSize:I

    invoke-virtual {v4, v5, v6, v7, v14}, Lcom/visionobjects/resourcemanager/core/DownloadService;->notifyProgress(Ljava/lang/String;III)V

    .line 289
    :cond_2
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdate:Ljava/lang/Object;

    monitor-enter v5

    .line 291
    :try_start_0
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdate:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->notify()V

    .line 292
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 294
    :cond_3
    return-void

    .line 273
    :cond_4
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    .line 274
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v10

    .line 275
    .local v10, "fileIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    move-wide/from16 v0, p1

    invoke-virtual {v4, v0, v1}, Lcom/visionobjects/resourcemanager/core/DownloadService;->remove(J)I

    .line 276
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    .line 279
    .local v9, "fileId":Ljava/lang/Long;
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/visionobjects/resourcemanager/core/DownloadService;->remove(J)I

    .line 280
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v4, v9}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 292
    .end local v9    # "fileId":Ljava/lang/Long;
    .end local v10    # "fileIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Long;>;"
    .end local v12    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .end local v2    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .restart local v11    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .restart local v13    # "newFile":Ljava/io/File;
    :cond_5
    move-object v2, v11

    .end local v11    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .restart local v2    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    goto/16 :goto_0

    .end local v2    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .end local v13    # "newFile":Ljava/io/File;
    .restart local v11    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    :cond_6
    move-object v2, v11

    .end local v11    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .restart local v2    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    goto :goto_1
.end method

.method public fileDownloadProgress(JII)V
    .locals 3
    .param p1, "fileId"    # J
    .param p3, "downloadSize"    # I
    .param p4, "totalSize"    # I

    .prologue
    .line 314
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/resourcemanager/model/FileToDownload;

    .line 316
    .local v0, "file":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    if-nez v0, :cond_0

    .line 326
    :goto_0
    return-void

    .line 319
    :cond_0
    const/4 v1, -0x1

    if-eq p4, v1, :cond_2

    .line 320
    invoke-virtual {v0, p4}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->setSize(I)V

    .line 323
    :cond_1
    :goto_1
    invoke-virtual {v0, p3}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->setDownloadedSize(I)V

    .line 325
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->notifyProgress()V

    goto :goto_0

    .line 321
    :cond_2
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSize()I

    move-result v1

    if-le p3, v1, :cond_1

    .line 322
    invoke-virtual {v0, p3}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->setSize(I)V

    goto :goto_1
.end method

.method public getEnqueuedFileIds()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 354
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 379
    iget v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mId:I

    return v0
.end method

.method public getLanguageKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLanguageKey:Ljava/lang/String;

    return-object v0
.end method

.method public getNotificationTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mNotificationTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getPendingIntent()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mPendingIntent:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public getUpdateFailedText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mUpdateFailedText:Ljava/lang/String;

    return-object v0
.end method

.method public getUpdateSuccessText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mUpdateSuccessText:Ljava/lang/String;

    return-object v0
.end method

.method public getVersion()Lcom/visionobjects/resourcemanager/util/Version;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    return-object v0
.end method

.method public hasBeenAborted()Z
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mAbortedPending:Z

    return v0
.end method

.method public setFilesToCopy(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/FileToMove;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/FileToMove;>;"
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToCopy:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 110
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToCopy:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 111
    return-void
.end method

.method public setFilesToDownload(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/model/FileToMove;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p1, "files":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/model/FileToMove;>;"
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToDownload:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 116
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .local v7, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/visionobjects/resourcemanager/model/FileToMove;

    .line 118
    .local v6, "file":Lcom/visionobjects/resourcemanager/model/FileToMove;
    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getSrcFilename()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 119
    .local v5, "uri":Landroid/net/Uri;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/core/DownloadService;->getExternalStorage()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/visionobjects/resourcemanager/util/Utils;->getRandomFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "localFilename":Ljava/lang/String;
    new-instance v0, Lcom/visionobjects/resourcemanager/model/FileToDownload;

    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getDestFilename()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getMd5()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/model/FileToMove;->getSize()I

    move-result v4

    invoke-direct/range {v0 .. v5}, Lcom/visionobjects/resourcemanager/model/FileToDownload;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/net/Uri;)V

    .line 122
    .local v0, "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToDownload:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 124
    .end local v0    # "fileToDownload":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .end local v1    # "localFilename":Ljava/lang/String;
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v6    # "file":Lcom/visionobjects/resourcemanager/model/FileToMove;
    :cond_0
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 104
    iput p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mId:I

    .line 105
    return-void
.end method

.method public setNotificationTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mNotificationTitle:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setPendingIntent(Landroid/app/PendingIntent;)V
    .locals 0
    .param p1, "pendingIntent"    # Landroid/app/PendingIntent;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mPendingIntent:Landroid/app/PendingIntent;

    .line 85
    return-void
.end method

.method public setUpdateFailedText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 99
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mUpdateFailedText:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public setUpdateSuccessText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mUpdateSuccessText:Ljava/lang/String;

    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mLanguageKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mVersion:Lcom/visionobjects/resourcemanager/util/Version;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public update()Z
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 136
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnCancel:Ljava/lang/Object;

    monitor-enter v6

    .line 138
    :try_start_0
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mAbortedPending:Z

    if-ne v5, v7, :cond_0

    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    if-nez v5, :cond_0

    .line 140
    sget-object v5, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->TAG:Ljava/lang/String;

    const-string v7, "..... [update] abort is pending.. Don\'t progess update"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    monitor-exit v6

    .line 202
    :goto_0
    return v5

    .line 147
    :cond_0
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToCopy:Ljava/util/List;

    invoke-static {v5}, Lcom/visionobjects/resourcemanager/util/Utils;->processLocalMove(Ljava/util/List;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    .line 149
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToDownload:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 151
    :cond_1
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    monitor-exit v6

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 154
    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v5, p0}, Lcom/visionobjects/resourcemanager/core/DownloadService;->registerProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    :try_start_2
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mFilesToDownload:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/visionobjects/resourcemanager/model/FileToDownload;

    .line 160
    .local v1, "file":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v5, p0, v1}, Lcom/visionobjects/resourcemanager/core/DownloadService;->enqueue(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Lcom/visionobjects/resourcemanager/model/FileToDownload;)J

    move-result-wide v2

    .line 161
    .local v2, "fileId":J
    const-wide/16 v7, -0x1

    cmp-long v5, v2, v7

    if-nez v5, :cond_4

    .line 162
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    .line 163
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v5, p0}, Lcom/visionobjects/resourcemanager/core/DownloadService;->unregisterProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    .line 164
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z
    :try_end_2
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 166
    :cond_4
    :try_start_4
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->isCancelled(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 168
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mEnqueuedFiles:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 172
    .end local v1    # "file":Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .end local v2    # "fileId":J
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/SecurityException;
    :try_start_5
    sget-object v5, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SecurityException while queuing for download: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/SecurityException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    .line 177
    .end local v0    # "e":Ljava/lang/SecurityException;
    :cond_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 183
    :try_start_6
    iget-object v6, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdate:Ljava/lang/Object;

    monitor-enter v6
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1

    .line 187
    :try_start_7
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdateNeedToWait:Z

    if-eqz v5, :cond_6

    invoke-static {p0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->isCancelled(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 188
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdate:Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/lang/Object;->wait()V

    .line 189
    :cond_6
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mMonitorOnUpdateNeedToWait:Z

    .line 190
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 197
    :goto_2
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v5, p0}, Lcom/visionobjects/resourcemanager/core/DownloadService;->unregisterProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    .line 202
    iget-boolean v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    goto/16 :goto_0

    .line 190
    :catchall_1
    move-exception v5

    :try_start_8
    monitor-exit v6
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v5
    :try_end_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_1

    .line 192
    :catch_1
    move-exception v0

    .line 194
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v5, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "InterruptedException while waiting for download: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 195
    iput-boolean v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->mSuccess:Z

    goto :goto_2
.end method
