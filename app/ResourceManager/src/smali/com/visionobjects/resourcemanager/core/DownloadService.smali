.class public Lcom/visionobjects/resourcemanager/core/DownloadService;
.super Ljava/lang/Object;
.source "DownloadService.java"


# static fields
.field private static AndroidDownloadManager:Landroid/app/DownloadManager;

.field private static DBG:Z

.field private static ExternalStorage:Ljava/io/File;

.field private static ProgressMonitor:Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

.field private static final TAG:Ljava/lang/String;

.field private static cptRequest:I

.field public static mDirectoryPath:Ljava/lang/String;

.field private static macAdressEncoded:Ljava/lang/String;

.field private static userAgent:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mUnregisterCompleted:Z

.field public onComplete:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    const-class v0, Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    .line 33
    sput-boolean v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    .line 40
    sput v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->cptRequest:I

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_DOWNLOADS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/MyScriptDownload"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mDirectoryPath:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v7, 0x105

    const/4 v6, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-boolean v6, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mUnregisterCompleted:Z

    .line 206
    new-instance v3, Lcom/visionobjects/resourcemanager/core/DownloadService$1;

    invoke-direct {v3, p0}, Lcom/visionobjects/resourcemanager/core/DownloadService$1;-><init>(Lcom/visionobjects/resourcemanager/core/DownloadService;)V

    iput-object v3, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->onComplete:Landroid/content/BroadcastReceiver;

    .line 48
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    .line 50
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->macAdressEncoded:Ljava/lang/String;

    if-nez v3, :cond_0

    .line 54
    :try_start_0
    const-string v3, "wifi"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 55
    .local v2, "wifiMgr":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v1

    .line 56
    .local v1, "mac":Ljava/lang/String;
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v3}, Lcom/visionobjects/resourcemanager/util/Md5Builder;->getMd5(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->macAdressEncoded:Ljava/lang/String;

    .line 57
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/visionobjects/resourcemanager/core/DownloadService;->macAdressEncoded:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":RM-Version:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "1.3.0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v4, 0x105

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":sdk:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":request:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->userAgent:Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 78
    .end local v1    # "mac":Ljava/lang/String;
    .end local v2    # "wifiMgr":Landroid/net/wifi/WifiManager;
    :cond_0
    :goto_0
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->ExternalStorage:Ljava/io/File;

    if-nez v3, :cond_1

    .line 80
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->mDirectoryPath:Ljava/lang/String;

    invoke-static {v3}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    sput-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->ExternalStorage:Ljava/io/File;

    .line 82
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->ExternalStorage:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->mkdirs()Z

    .line 85
    :cond_1
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;

    if-nez v3, :cond_3

    .line 87
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    const-string v4, "download"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/DownloadManager;

    sput-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;

    .line 88
    sget-boolean v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    if-eqz v3, :cond_2

    .line 89
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v4, "> register downloadManager -> OK"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_2
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->onComplete:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 92
    new-instance v3, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

    sget-object v4, Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;

    invoke-direct {v3, v4}, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;-><init>(Landroid/app/DownloadManager;)V

    sput-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->ProgressMonitor:Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

    .line 93
    new-instance v3, Ljava/lang/Thread;

    sget-object v4, Lcom/visionobjects/resourcemanager/core/DownloadService;->ProgressMonitor:Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

    invoke-direct {v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 101
    :goto_1
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v0}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    goto :goto_0

    .line 65
    .end local v0    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_1
    move-exception v0

    .line 67
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 71
    .local v0, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GSMOnly"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x8

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":RM-Version:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "1.3.0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":sdk:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ":request:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sput-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->userAgent:Ljava/lang/String;

    goto/16 :goto_0

    .line 97
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_3
    sget-boolean v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    if-eqz v3, :cond_4

    .line 98
    sget-object v3, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v4, "> register downloadManager -> OK"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_4
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->onComplete:Landroid/content/BroadcastReceiver;

    new-instance v5, Landroid/content/IntentFilter;

    const-string v6, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto/16 :goto_1
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 30
    sget-boolean v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Landroid/app/DownloadManager;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;

    return-object v0
.end method


# virtual methods
.method public enqueue(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Lcom/visionobjects/resourcemanager/model/FileToDownload;)J
    .locals 13
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    .param p2, "file"    # Lcom/visionobjects/resourcemanager/model/FileToDownload;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const-wide/16 v7, -0x1

    .line 152
    sget-boolean v9, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    if-eqz v9, :cond_0

    .line 154
    sget-object v9, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v10, "> startFileDownload"

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    sget-object v9, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_0
    new-instance v4, Ljava/io/File;

    invoke-virtual {p2}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getSrcFilename()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 159
    .local v4, "tempFile":Ljava/io/File;
    invoke-static {v4}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v5

    .line 161
    .local v5, "tempUri":Landroid/net/Uri;
    new-instance v9, Landroid/app/DownloadManager$Request;

    invoke-virtual {p2}, Lcom/visionobjects/resourcemanager/model/FileToDownload;->getUri()Landroid/net/Uri;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/app/DownloadManager$Request;->setVisibleInDownloadsUi(Z)Landroid/app/DownloadManager$Request;

    move-result-object v9

    const-string v10, "application/octet-stream"

    invoke-virtual {v9, v10}, Landroid/app/DownloadManager$Request;->setMimeType(Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object v9

    const-string v10, "user-agent"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v12, Lcom/visionobjects/resourcemanager/core/DownloadService;->userAgent:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget v12, Lcom/visionobjects/resourcemanager/core/DownloadService;->cptRequest:I

    add-int/lit8 v12, v12, 0x1

    sput v12, Lcom/visionobjects/resourcemanager/core/DownloadService;->cptRequest:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/app/DownloadManager$Request;->addRequestHeader(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/app/DownloadManager$Request;->setDestinationUri(Landroid/net/Uri;)Landroid/app/DownloadManager$Request;

    move-result-object v3

    .line 165
    .local v3, "request":Landroid/app/DownloadManager$Request;
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getNotificationTitle()Ljava/lang/String;

    move-result-object v6

    .line 166
    .local v6, "title":Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 167
    invoke-virtual {v3, v6}, Landroid/app/DownloadManager$Request;->setTitle(Ljava/lang/CharSequence;)Landroid/app/DownloadManager$Request;

    .line 170
    :cond_1
    :goto_0
    const-wide/16 v1, -0x1

    .line 172
    .local v1, "fileId":J
    :try_start_0
    sget-object v9, Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;

    invoke-virtual {v9, v3}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-wide v1

    move-wide v7, v1

    .line 181
    :goto_1
    return-wide v7

    .line 168
    .end local v1    # "fileId":J
    :cond_2
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0xb

    if-lt v9, v10, :cond_1

    .line 169
    const/4 v9, 0x2

    invoke-virtual {v3, v9}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    goto :goto_0

    .line 173
    .restart local v1    # "fileId":J
    :catch_0
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v9, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v10, "Error on enqueue: Android DownloadManager not started?"

    invoke-static {v9, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 176
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 177
    .local v0, "e":Ljava/lang/Exception;
    sget-object v9, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v10, "Other error in enqueuing Android DownloadManager"

    invoke-static {v9, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public finalize()V
    .locals 3

    .prologue
    .line 106
    iget-boolean v1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mUnregisterCompleted:Z

    if-nez v1, :cond_0

    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->onComplete:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 114
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-boolean v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    if-eqz v1, :cond_0

    .line 115
    sget-object v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v2, "Cannot unregister onComplete receiver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getExternalStorage()Ljava/io/File;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->ExternalStorage:Ljava/io/File;

    return-object v0
.end method

.method public notifyProgress(Ljava/lang/String;III)V
    .locals 4
    .param p1, "langName"    # Ljava/lang/String;
    .param p2, "progress"    # I
    .param p3, "progressSize"    # I
    .param p4, "totalSize"    # I

    .prologue
    .line 194
    sget-boolean v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    if-eqz v1, :cond_0

    .line 195
    sget-object v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DownloadProgress of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/100 ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 198
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_PROGRESS:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 200
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_PROGRESS:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 201
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_PROGRESS_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 202
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_TOTAL_SIZE:Ljava/lang/String;

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 203
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 204
    return-void
.end method

.method public registerProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 1
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 141
    sget-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->ProgressMonitor:Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

    invoke-virtual {v0, p1}, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->registerProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    .line 142
    return-void
.end method

.method public remove(J)I
    .locals 3
    .param p1, "fileId"    # J

    .prologue
    const/4 v0, 0x0

    .line 186
    const-wide/16 v1, -0x1

    cmp-long v1, p1, v1

    if-eqz v1, :cond_0

    .line 187
    sget-object v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;

    const/4 v2, 0x1

    new-array v2, v2, [J

    aput-wide p1, v2, v0

    invoke-virtual {v1, v2}, Landroid/app/DownloadManager;->remove([J)I

    move-result v0

    .line 189
    :cond_0
    return v0
.end method

.method public unregisterBroadCastReceiver()V
    .locals 3

    .prologue
    .line 124
    :try_start_0
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->onComplete:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService;->mUnregisterCompleted:Z

    .line 132
    return-void

    .line 126
    :catch_0
    move-exception v0

    .line 128
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-boolean v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z

    if-eqz v1, :cond_0

    .line 129
    sget-object v1, Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;

    const-string v2, "Cannot unregister onComplete receiver in unregisterBroadCastReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public unregisterProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 1
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 146
    sget-object v0, Lcom/visionobjects/resourcemanager/core/DownloadService;->ProgressMonitor:Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

    invoke-virtual {v0, p1}, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->unregisterProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    .line 147
    return-void
.end method
