.class public interface abstract Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;
.super Ljava/lang/Object;
.source "ProgressMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/core/ProgressMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnProgressListener"
.end annotation


# virtual methods
.method public abstract fileDownloadFinished(JZLjava/lang/String;)V
.end method

.method public abstract fileDownloadProgress(JII)V
.end method

.method public abstract getEnqueuedFileIds()Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end method
