.class public Lcom/visionobjects/resourcemanager/core/ProgressMonitor;
.super Ljava/lang/Object;
.source "ProgressMonitor.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;
    }
.end annotation


# static fields
.field private static DBG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mDownloadManager:Landroid/app/DownloadManager;

.field private final mFinishedFiles:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mProgressListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->TAG:Ljava/lang/String;

    .line 22
    const/4 v0, 0x0

    sput-boolean v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->DBG:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/DownloadManager;)V
    .locals 1
    .param p1, "downloadManager"    # Landroid/app/DownloadManager;

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mProgressListeners:Ljava/util/List;

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mFinishedFiles:Ljava/util/Set;

    .line 42
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mDownloadManager:Landroid/app/DownloadManager;

    .line 43
    return-void
.end method


# virtual methods
.method public registerProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 1
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mProgressListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method

.method public run()V
    .locals 20

    .prologue
    .line 58
    new-instance v10, Landroid/app/DownloadManager$Query;

    invoke-direct {v10}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 63
    .local v10, "query":Landroid/app/DownloadManager$Query;
    :cond_0
    const-wide/16 v17, 0x3e8

    :try_start_0
    invoke-static/range {v17 .. v18}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mProgressListeners:Ljava/util/List;

    move-object/from16 v17, v0

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;

    .line 71
    .local v9, "progressListener":Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;
    invoke-interface {v9}, Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;->getEnqueuedFileIds()Ljava/util/Set;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 73
    .local v5, "fileId":J
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [J

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-wide v5, v17, v18

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mDownloadManager:Landroid/app/DownloadManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v2

    .line 75
    .local v2, "cursor":Landroid/database/Cursor;
    const-string v17, "status"

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 76
    .local v14, "statusIndex":I
    const-string v17, "reason"

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 77
    .local v12, "reasonIndex":I
    const-string v17, "bytes_so_far"

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 78
    .local v4, "downloadSizeIndex":I
    const-string v17, "total_size"

    move-object/from16 v0, v17

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 80
    .local v16, "totalSizeIndex":I
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v17

    if-eqz v17, :cond_3

    .line 82
    invoke-interface {v2, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 83
    .local v13, "status":I
    invoke-interface {v2, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 84
    .local v11, "reason":I
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 85
    .local v3, "downloadSize":I
    move/from16 v0, v16

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 87
    .local v15, "totalSize":I
    const/16 v17, 0x10

    move/from16 v0, v17

    if-ne v13, v0, :cond_4

    .line 89
    sget-boolean v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->DBG:Z

    if-eqz v17, :cond_2

    .line 90
    sget-object v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " Cannot download file reason "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mFinishedFiles:Ljava/util/Set;

    move-object/from16 v17, v0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 93
    const/16 v17, 0x0

    const-string v18, ""

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-interface {v9, v5, v6, v0, v1}, Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;->fileDownloadFinished(JZLjava/lang/String;)V

    .line 119
    .end local v3    # "downloadSize":I
    .end local v11    # "reason":I
    .end local v13    # "status":I
    .end local v15    # "totalSize":I
    :cond_3
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 97
    .restart local v3    # "downloadSize":I
    .restart local v11    # "reason":I
    .restart local v13    # "status":I
    .restart local v15    # "totalSize":I
    :cond_4
    sget-boolean v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->DBG:Z

    if-eqz v17, :cond_5

    .line 98
    sget-object v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " Download progress on "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mFinishedFiles:Ljava/util/Set;

    move-object/from16 v17, v0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_8

    .line 102
    if-ne v3, v15, :cond_7

    .line 104
    sget-boolean v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->DBG:Z

    if-eqz v17, :cond_6

    .line 105
    sget-object v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " Download finished on "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mFinishedFiles:Ljava/util/Set;

    move-object/from16 v17, v0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    invoke-interface/range {v17 .. v18}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 109
    :cond_7
    invoke-interface {v9, v5, v6, v3, v15}, Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;->fileDownloadProgress(JII)V

    goto/16 :goto_2

    .line 113
    :cond_8
    sget-boolean v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->DBG:Z

    if-eqz v17, :cond_3

    .line 114
    sget-object v17, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->TAG:Ljava/lang/String;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, " Download already finished on "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 65
    .end local v2    # "cursor":Landroid/database/Cursor;
    .end local v3    # "downloadSize":I
    .end local v4    # "downloadSizeIndex":I
    .end local v5    # "fileId":J
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "progressListener":Lcom/visionobjects/resourcemanager/core/ProgressMonitor$OnProgressListener;
    .end local v11    # "reason":I
    .end local v12    # "reasonIndex":I
    .end local v13    # "status":I
    .end local v14    # "statusIndex":I
    .end local v15    # "totalSize":I
    .end local v16    # "totalSizeIndex":I
    :catch_0
    move-exception v17

    goto/16 :goto_0
.end method

.method public unregisterProgressListener(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 1
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/ProgressMonitor;->mProgressListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 53
    return-void
.end method
