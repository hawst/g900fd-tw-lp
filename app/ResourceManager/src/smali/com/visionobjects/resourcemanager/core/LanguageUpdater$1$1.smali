.class Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1$1;
.super Ljava/lang/Object;
.source "LanguageUpdater.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$intent:Landroid/content/Intent;


# direct methods
.method constructor <init>(Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1;Landroid/content/Intent;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1$1;->this$0:Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1;

    iput-object p2, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1$1;->val$intent:Landroid/content/Intent;

    iput-object p3, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    .line 58
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1$1;->val$intent:Landroid/content/Intent;

    sget-object v6, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "langName":Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->getWaitForDownloadingLanguage()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 63
    invoke-static {v2}, Lcom/visionobjects/resourcemanager/ResourceManagerService;->removeFromWaitingDownloadList(Ljava/lang/String;)V

    .line 64
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v1, "intent":Landroid/content/Intent;
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_RESULT:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v6, -0x4

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 68
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1$1;->val$context:Landroid/content/Context;

    invoke-virtual {v5, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 115
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->clearCache()V

    .line 116
    :goto_1
    return-void

    .line 75
    :cond_0
    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$000()Ljava/util/List;

    move-result-object v6

    monitor-enter v6

    .line 79
    const/4 v4, 0x0

    .line 80
    .local v4, "languageToCancel":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :try_start_0
    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$000()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .line 82
    .local v3, "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getLanguageKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 84
    move-object v4, v3

    .line 90
    .end local v3    # "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :cond_2
    if-nez v4, :cond_3

    .line 94
    monitor-exit v6

    goto :goto_1

    .line 111
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 98
    .restart local v0    # "i$":Ljava/util/Iterator;
    :cond_3
    :try_start_1
    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$000()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    int-to-long v7, v5

    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$100()Ljava/util/concurrent/CountDownLatch;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v9

    const-wide/16 v11, 0x1

    add-long/2addr v9, v11

    cmp-long v5, v7, v9

    if-nez v5, :cond_4

    .line 100
    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$000()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .line 102
    .restart local v3    # "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->abort()V

    .line 103
    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$200()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 108
    .end local v3    # "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :cond_4
    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->abort()V

    .line 109
    # getter for: Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->access$200()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    :cond_5
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
