.class public Lcom/visionobjects/resourcemanager/core/LanguageUpdater;
.super Ljava/lang/Object;
.source "LanguageUpdater.java"


# static fields
.field private static final DBG:Z = false

.field private static final DELETE_OLD_LANGUAGE_LIMIT:I = 0x3

.field private static final ENQUEUED_LANGUAGES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/core/LanguageToDownload;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String;

.field private static languagesCanceled:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/core/LanguageToDownload;",
            ">;"
        }
    .end annotation
.end field

.field private static sCptRegisterReceiver:I

.field private static sDownloadCount:I

.field private static sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

.field public static sOnCancel:Landroid/content/BroadcastReceiver;

.field private static sVersionLock:Lcom/visionobjects/resourcemanager/util/Version;

.field private static sVersionLockCount:I


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

.field private mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

.field private final mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

.field private mUnregisterCompleted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    new-instance v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1;

    invoke-direct {v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$1;-><init>()V

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sOnCancel:Landroid/content/BroadcastReceiver;

    .line 124
    const-class v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->TAG:Ljava/lang/String;

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    .line 131
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    .line 133
    sput v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLockCount:I

    .line 134
    sput v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sDownloadCount:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mUnregisterCompleted:Z

    .line 157
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    .line 158
    new-instance v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    .line 159
    new-instance v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    .line 160
    new-instance v0, Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-direct {v0, p1}, Lcom/visionobjects/resourcemanager/core/DownloadService;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    .line 162
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    .line 163
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sOnCancel:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    sget-object v3, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_CANCEL_LANG_UPDATE:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 164
    sget-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    if-nez v0, :cond_0

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    .line 166
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method static synthetic access$200()Ljava/util/List;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$300(Lcom/visionobjects/resourcemanager/core/LanguageUpdater;Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 0
    .param p0, "x0"    # Lcom/visionobjects/resourcemanager/core/LanguageUpdater;
    .param p1, "x1"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->runUpdate(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    return-void
.end method

.method private createCheckedFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p1, "dir"    # Ljava/lang/String;
    .param p2, "lang"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 745
    new-instance v1, Ljava/io/File;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "checked"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 748
    .local v1, "f":Ljava/io/File;
    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 755
    invoke-static {p2}, Lcom/visionobjects/resourcemanager/util/Utils;->isSpecificLanguage(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 779
    :goto_0
    return v7

    .line 750
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/io/IOException;
    move v7, v8

    .line 752
    goto :goto_0

    .line 759
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    iget-object v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-virtual {v9, p2}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 760
    .local v3, "path":Ljava/lang/String;
    if-eqz v3, :cond_1

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-static {v9}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 762
    invoke-static {v3}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 763
    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v3, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 764
    .local v5, "versionPath":Ljava/lang/String;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 765
    .local v6, "versionPathDir":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v2

    .line 766
    .local v2, "fileList":[Ljava/lang/String;
    if-eqz v2, :cond_1

    array-length v8, v2

    const/4 v9, 0x3

    if-ne v8, v9, :cond_1

    .line 770
    new-instance v4, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v9

    invoke-virtual {v9}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getResourcesTxtName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 771
    .local v4, "res":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 772
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "mul"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 773
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lib"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 774
    invoke-static {v5}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 778
    .end local v2    # "fileList":[Ljava/lang/String;
    .end local v4    # "res":Ljava/io/File;
    .end local v5    # "versionPath":Ljava/lang/String;
    .end local v6    # "versionPathDir":Ljava/io/File;
    :cond_1
    new-instance v8, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-direct {v8, v9}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;-><init>(Landroid/content/Context;)V

    iput-object v8, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    goto/16 :goto_0
.end method

.method private createDownloadCompleteNotification(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Z)V
    .locals 7
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    .param p2, "success"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v6, 0xb

    .line 675
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getNotificationTitle()Ljava/lang/String;

    move-result-object v2

    .line 676
    .local v2, "contentTitle":Ljava/lang/String;
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getUpdateSuccessText()Ljava/lang/String;

    move-result-object v1

    .line 679
    .local v1, "contentText":Ljava/lang/String;
    :goto_0
    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v6, :cond_1

    .line 682
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getPendingIntent()Landroid/app/PendingIntent;

    move-result-object v3

    .line 684
    .local v3, "pendingIntent":Landroid/app/PendingIntent;
    new-instance v0, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 686
    .local v0, "builder":Landroid/app/Notification$Builder;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    .line 687
    const v4, 0x1080081

    invoke-virtual {v0, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 688
    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 689
    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 691
    if-eqz v3, :cond_0

    .line 692
    invoke-virtual {v0, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 694
    :cond_0
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_3

    .line 695
    invoke-direct {p0, p1, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->launchJellyBeanNotification(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Landroid/app/Notification$Builder;)V

    .line 699
    .end local v0    # "builder":Landroid/app/Notification$Builder;
    .end local v3    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_1
    :goto_1
    return-void

    .line 676
    .end local v1    # "contentText":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getUpdateFailedText()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 696
    .restart local v0    # "builder":Landroid/app/Notification$Builder;
    .restart local v1    # "contentText":Ljava/lang/String;
    .restart local v3    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_3
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v6, :cond_1

    .line 697
    invoke-direct {p0, p1, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->launchHoneyCombNotification(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Landroid/app/Notification$Builder;)V

    goto :goto_1
.end method

.method private finalizeLanguageUpdate(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Z)V
    .locals 11
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    .param p2, "success"    # Z
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 573
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getLanguageKey()Ljava/lang/String;

    move-result-object v0

    .line 574
    .local v0, "langName":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersionDir(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 577
    .local v4, "localeDir":Ljava/lang/String;
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/util/Utils;->isSpecificLanguage(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 583
    if-eqz p2, :cond_0

    .line 584
    invoke-direct {p0, v4, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->createCheckedFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    .line 585
    :cond_0
    sget-object v8, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v8}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 650
    :cond_1
    :goto_0
    if-eqz p2, :cond_8

    .line 651
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->notifySuccess(Ljava/lang/String;)V

    .line 657
    :goto_1
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->unlockVersion()V

    .line 658
    sget-object v8, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 660
    invoke-direct {p0, p1, p2}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->createDownloadCompleteNotification(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Z)V

    .line 661
    return-void

    .line 588
    :cond_2
    if-eqz p2, :cond_1

    .line 594
    :try_start_0
    sget-object v8, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v8}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 602
    :goto_2
    if-eqz p2, :cond_5

    .line 604
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersionDir(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "mul"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 606
    .local v5, "mulDir":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersionDir(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "lib"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 609
    .local v1, "libDir":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "checked"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 610
    .local v7, "mulDirectoryChecked":Ljava/io/File;
    new-instance v3, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "checked"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 612
    .local v3, "libDirectoryChecked":Ljava/io/File;
    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 613
    .local v6, "mulDirectory":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 615
    .local v2, "libDirectory":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 617
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 619
    const/4 p2, 0x1

    .line 627
    :cond_3
    :goto_3
    if-eqz p2, :cond_4

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 629
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 631
    const/4 p2, 0x1

    .line 639
    :cond_4
    :goto_4
    if-nez p2, :cond_5

    .line 641
    invoke-static {v5}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 642
    invoke-static {v1}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 643
    const/4 p2, 0x0

    .line 646
    .end local v1    # "libDir":Ljava/lang/String;
    .end local v2    # "libDirectory":Ljava/io/File;
    .end local v3    # "libDirectoryChecked":Ljava/io/File;
    .end local v5    # "mulDir":Ljava/lang/String;
    .end local v6    # "mulDirectory":Ljava/io/File;
    .end local v7    # "mulDirectoryChecked":Ljava/io/File;
    :cond_5
    if-eqz p2, :cond_1

    .line 647
    invoke-direct {p0, v4, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->createCheckedFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p2

    goto/16 :goto_0

    .line 623
    .restart local v1    # "libDir":Ljava/lang/String;
    .restart local v2    # "libDirectory":Ljava/io/File;
    .restart local v3    # "libDirectoryChecked":Ljava/io/File;
    .restart local v5    # "mulDir":Ljava/lang/String;
    .restart local v6    # "mulDirectory":Ljava/io/File;
    .restart local v7    # "mulDirectoryChecked":Ljava/io/File;
    :cond_6
    const/4 p2, 0x0

    goto :goto_3

    .line 635
    :cond_7
    const/4 p2, 0x0

    goto :goto_4

    .line 654
    .end local v1    # "libDir":Ljava/lang/String;
    .end local v2    # "libDirectory":Ljava/io/File;
    .end local v3    # "libDirectoryChecked":Ljava/io/File;
    .end local v5    # "mulDir":Ljava/lang/String;
    .end local v6    # "mulDirectory":Ljava/io/File;
    .end local v7    # "mulDirectoryChecked":Ljava/io/File;
    :cond_8
    invoke-static {v4}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/lang/String;)Z

    .line 655
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->notifyAborted(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 596
    :catch_0
    move-exception v8

    goto/16 :goto_2
.end method

.method public static getLanguageForFileId(J)Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    .locals 4
    .param p0, "fileId"    # J

    .prologue
    .line 213
    sget-object v3, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    monitor-enter v3

    .line 215
    :try_start_0
    sget-object v2, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .line 217
    .local v1, "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->downloadsFile(Ljava/lang/Long;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 218
    monitor-exit v3

    .line 221
    .end local v1    # "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :goto_0
    return-object v1

    .line 220
    :cond_1
    monitor-exit v3

    .line 221
    const/4 v1, 0x0

    goto :goto_0

    .line 220
    .end local v0    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public static isCancelled(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)Z
    .locals 2
    .param p0, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 845
    sget-object v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 846
    .local v0, "contains":Z
    return v0
.end method

.method public static isCancelled(Ljava/lang/String;)Z
    .locals 3
    .param p0, "language"    # Ljava/lang/String;

    .prologue
    .line 851
    sget-object v2, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .line 853
    .local v1, "lg":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    invoke-virtual {v1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getLanguageKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 854
    const/4 v2, 0x1

    .line 856
    .end local v1    # "lg":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isLanguageVersionLocked()Z
    .locals 3

    .prologue
    .line 370
    const/4 v0, 0x0

    .line 371
    .local v0, "result":Z
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->hasResourcesParserfailed()Z

    move-result v2

    if-nez v2, :cond_1

    .line 373
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v1

    .line 374
    .local v1, "version":Lcom/visionobjects/resourcemanager/util/Version;
    sget-object v2, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLock:Lcom/visionobjects/resourcemanager/util/Version;

    if-nez v2, :cond_0

    .line 375
    sput-object v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLock:Lcom/visionobjects/resourcemanager/util/Version;

    .line 376
    :cond_0
    sget-object v2, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLock:Lcom/visionobjects/resourcemanager/util/Version;

    invoke-virtual {v1, v2}, Lcom/visionobjects/resourcemanager/util/Version;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 378
    monitor-enter p0

    .line 380
    :try_start_0
    sget v2, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLockCount:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLockCount:I

    .line 381
    monitor-exit p0

    .line 382
    const/4 v0, 0x1

    .line 387
    .end local v1    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_1
    return v0

    .line 381
    .restart local v1    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method private launchHoneyCombNotification(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Landroid/app/Notification$Builder;)V
    .locals 3
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    .param p2, "builder"    # Landroid/app/Notification$Builder;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 729
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 730
    .local v0, "nm":Landroid/app/NotificationManager;
    if-eqz v0, :cond_0

    .line 731
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getId()I

    move-result v1

    invoke-virtual {p2}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 732
    :cond_0
    return-void
.end method

.method private launchJellyBeanNotification(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Landroid/app/Notification$Builder;)V
    .locals 3
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    .param p2, "builder"    # Landroid/app/Notification$Builder;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 712
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    const-string v2, "notification"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 713
    .local v0, "nm":Landroid/app/NotificationManager;
    if-eqz v0, :cond_0

    .line 714
    invoke-virtual {p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getId()I

    move-result v1

    invoke-virtual {p2}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 715
    :cond_0
    return-void
.end method

.method private libIsInPreloadPath()Z
    .locals 6

    .prologue
    .line 324
    const/4 v2, 0x0

    .line 325
    .local v2, "result":Z
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    const-string v5, "lib"

    invoke-virtual {v4, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "libPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 328
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v1

    .line 329
    .local v1, "preloadedPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 331
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v3

    .line 332
    .local v3, "systemPath":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 335
    .end local v1    # "preloadedPath":Ljava/lang/String;
    .end local v3    # "systemPath":Ljava/lang/String;
    :cond_0
    return v2
.end method

.method private libUpdateRequiredForLanguage(Ljava/lang/String;)Z
    .locals 8
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 263
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    const-string v5, "lib"

    invoke-virtual {v4, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersion(Ljava/lang/String;)Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v0

    .line 264
    .local v0, "localLibVersion":Lcom/visionobjects/resourcemanager/util/Version;
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v1

    .line 266
    .local v1, "remoteLibVersion":Lcom/visionobjects/resourcemanager/util/Version;
    sget-boolean v4, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUseLibFolder:Z

    if-nez v4, :cond_1

    .line 283
    :cond_0
    :goto_0
    return v2

    .line 270
    :cond_1
    sget-object v4, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    .line 274
    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Lcom/visionobjects/resourcemanager/util/Version;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move v2, v3

    .line 276
    goto :goto_0

    .line 278
    :cond_3
    invoke-virtual {v0, v1}, Lcom/visionobjects/resourcemanager/util/Version;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->libIsInPreloadPath()Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 281
    goto :goto_0
.end method

.method private mulIsInPreloadPath()Z
    .locals 6

    .prologue
    .line 347
    const/4 v2, 0x0

    .line 348
    .local v2, "result":Z
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    const-string v5, "mul"

    invoke-virtual {v4, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 349
    .local v0, "mulPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 351
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v1

    .line 352
    .local v1, "preloadedPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 354
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v3

    .line 355
    .local v3, "systemPath":Ljava/lang/String;
    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    .line 358
    .end local v1    # "preloadedPath":Ljava/lang/String;
    .end local v3    # "systemPath":Ljava/lang/String;
    :cond_0
    return v2
.end method

.method private mulUpdateRequiredForLanguage(Ljava/lang/String;)Z
    .locals 8
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 296
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    const-string v5, "mul"

    invoke-virtual {v4, v5}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersion(Ljava/lang/String;)Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v0

    .line 297
    .local v0, "localMulVersion":Lcom/visionobjects/resourcemanager/util/Version;
    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v1

    .line 299
    .local v1, "remoteMulVersion":Lcom/visionobjects/resourcemanager/util/Version;
    sget-object v4, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v4}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    .line 312
    :cond_0
    :goto_0
    return v2

    .line 303
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Lcom/visionobjects/resourcemanager/util/Version;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    :cond_2
    move v2, v3

    .line 305
    goto :goto_0

    .line 307
    :cond_3
    invoke-virtual {v0, v1}, Lcom/visionobjects/resourcemanager/util/Version;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mulIsInPreloadPath()Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 310
    goto :goto_0
.end method

.method private notifyAborted(Ljava/lang/String;)V
    .locals 7
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 812
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->clearCache()V

    .line 813
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 814
    .local v2, "intent":Landroid/content/Intent;
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_RESULT:Ljava/lang/String;

    invoke-virtual {v2, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 815
    const/4 v0, 0x0

    .line 816
    .local v0, "found":Z
    const/4 v4, 0x0

    .line 817
    .local v4, "lgFound":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    sget-object v5, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .line 819
    .local v3, "lg":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    invoke-virtual {v3}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getLanguageKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 821
    const/4 v0, 0x1

    .line 822
    move-object v4, v3

    .line 826
    .end local v3    # "lg":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :cond_1
    if-eqz v0, :cond_2

    .line 828
    sget-object v5, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->languagesCanceled:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 829
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v6, -0x4

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 837
    :goto_0
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v2, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 840
    iget-object v5, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 841
    return-void

    .line 831
    :cond_2
    const-string v5, "removed"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 832
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v6, -0x3

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 833
    :cond_3
    sget-boolean v5, Lcom/visionobjects/resourcemanager/util/Utils;->LocalPartitionFull:Z

    if-eqz v5, :cond_4

    .line 834
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v6, -0x2

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0

    .line 836
    :cond_4
    sget-object v5, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v6, -0x1

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method private notifySuccess(Ljava/lang/String;)V
    .locals 3
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 792
    invoke-static {}, Lcom/visionobjects/resourcemanager/DataStorageProvider;->clearCache()V

    .line 793
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 794
    .local v0, "intent":Landroid/content/Intent;
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->ACTION_UPDATE_LANG_RESULT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 795
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_UPDATE_RESULT:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 796
    sget-object v1, Lcom/visionobjects/resourcemanager/ResourceManagerIntent;->EXTRA_LANG_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 799
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 800
    return-void
.end method

.method private runUpdate(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 25
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 473
    invoke-virtual/range {p1 .. p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getLanguageKey()Ljava/lang/String;

    move-result-object v10

    .line 478
    .local v10, "langName":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->isLanguageVersionLocked()Z

    move-result v23

    if-nez v23, :cond_0

    .line 480
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->finalizeLanguageUpdate(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Z)V

    .line 559
    :goto_0
    return-void

    .line 484
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v21

    .line 485
    .local v21, "version":Lcom/visionobjects/resourcemanager/util/Version;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-static {v0, v1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersionDir(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;

    move-result-object v22

    .line 486
    .local v22, "versionDirName":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-direct {v11, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 490
    .local v11, "languageDir":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->mkdir()Z

    .line 491
    const/16 v23, 0x1

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 492
    const/16 v23, 0x1

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-virtual {v11, v0, v1}, Ljava/io/File;->setReadable(ZZ)Z

    .line 494
    const/16 v20, 0x0

    .line 496
    .local v20, "success":Z
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 497
    .local v7, "filesToCopyLocally":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/model/FileToMove;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 499
    .local v8, "filesToDownload":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/model/FileToMove;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getResFileInfos(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v14

    .line 500
    .local v14, "localeFileList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getResFileInfos(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v16

    .line 501
    .local v16, "remoteFileList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    if-nez v16, :cond_1

    .line 503
    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v23

    invoke-direct {v0, v1, v2}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->finalizeLanguageUpdate(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Z)V

    goto/16 :goto_0

    .line 507
    :cond_1
    if-nez v14, :cond_2

    .line 511
    new-instance v14, Ljava/util/HashMap;

    .end local v14    # "localeFileList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 514
    .restart local v14    # "localeFileList":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 515
    .local v3, "baseDir":Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v17

    .line 517
    .local v17, "remoteFileNames":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface/range {v17 .. v17}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 519
    .local v6, "fileName":Ljava/lang/String;
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 520
    .local v4, "dstFileName":Ljava/lang/String;
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/visionobjects/resourcemanager/model/ResFileInfo;

    .line 521
    .local v15, "remoteFileInfo":Lcom/visionobjects/resourcemanager/model/ResFileInfo;
    invoke-virtual {v14, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/visionobjects/resourcemanager/model/ResFileInfo;

    .line 523
    .local v12, "localFileInfo":Lcom/visionobjects/resourcemanager/model/ResFileInfo;
    if-eqz v12, :cond_3

    invoke-virtual {v12}, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->getMd5()Ljava/lang/String;

    move-result-object v23

    invoke-virtual {v15}, Lcom/visionobjects/resourcemanager/model/ResFileInfo;->getMd5()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 525
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 526
    .local v19, "srcFileName":Ljava/lang/String;
    new-instance v5, Lcom/visionobjects/resourcemanager/model/FileToMove;

    move-object/from16 v0, v19

    invoke-direct {v5, v0, v4, v12}, Lcom/visionobjects/resourcemanager/model/FileToMove;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;)V

    .line 527
    .local v5, "file":Lcom/visionobjects/resourcemanager/model/FileToMove;
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 532
    .end local v5    # "file":Lcom/visionobjects/resourcemanager/model/FileToMove;
    .end local v19    # "srcFileName":Ljava/lang/String;
    :cond_3
    const-string v23, "lib"

    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 533
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getServerUrl()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .line 539
    .restart local v19    # "srcFileName":Ljava/lang/String;
    :goto_2
    new-instance v5, Lcom/visionobjects/resourcemanager/model/FileToMove;

    move-object/from16 v0, v19

    invoke-direct {v5, v0, v4, v15}, Lcom/visionobjects/resourcemanager/model/FileToMove;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;)V

    .line 540
    .restart local v5    # "file":Lcom/visionobjects/resourcemanager/model/FileToMove;
    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 536
    .end local v5    # "file":Lcom/visionobjects/resourcemanager/model/FileToMove;
    .end local v19    # "srcFileName":Ljava/lang/String;
    :cond_4
    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getServerUrl()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    sget-object v24, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    .restart local v19    # "srcFileName":Ljava/lang/String;
    goto :goto_2

    .line 543
    .end local v4    # "dstFileName":Ljava/lang/String;
    .end local v6    # "fileName":Ljava/lang/String;
    .end local v12    # "localFileInfo":Lcom/visionobjects/resourcemanager/model/ResFileInfo;
    .end local v15    # "remoteFileInfo":Lcom/visionobjects/resourcemanager/model/ResFileInfo;
    .end local v19    # "srcFileName":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setFilesToCopy(Ljava/util/List;)V

    .line 544
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setFilesToDownload(Ljava/util/List;)V

    .line 546
    invoke-virtual/range {p1 .. p1}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->update()Z

    move-result v20

    .line 548
    if-eqz v20, :cond_6

    .line 552
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    invoke-virtual {v0, v10}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getFilesTxtFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 553
    .local v18, "remotePath":Ljava/lang/String;
    move-object/from16 v0, v22

    invoke-static {v0, v10}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getFilesTxtFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 555
    .local v13, "localPath":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-static {v0, v13}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->downloadToLocalFile(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v20

    .line 558
    .end local v13    # "localPath":Ljava/lang/String;
    .end local v18    # "remotePath":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->finalizeLanguageUpdate(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Z)V

    goto/16 :goto_0
.end method

.method private runUpdateThread(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V
    .locals 2
    .param p1, "language"    # Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    .prologue
    .line 461
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$2;

    invoke-direct {v1, p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater$2;-><init>(Lcom/visionobjects/resourcemanager/core/LanguageUpdater;Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 469
    return-void
.end method

.method private unlockVersion()V
    .locals 1

    .prologue
    .line 395
    monitor-enter p0

    .line 397
    :try_start_0
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLockCount:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLockCount:I

    .line 398
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 399
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLockCount:I

    if-nez v0, :cond_0

    .line 400
    const/4 v0, 0x0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sVersionLock:Lcom/visionobjects/resourcemanager/util/Version;

    .line 401
    :cond_0
    return-void

    .line 398
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private updateAvailableForLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 233
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->hasResourcesParserfailed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getLangs()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateRequiredForLanguage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 246
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mFileSystemHelper:Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-virtual {v2, p1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getVersion(Ljava/lang/String;)Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v0

    .line 247
    .local v0, "localeLangVersion":Lcom/visionobjects/resourcemanager/util/Version;
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v1

    .line 250
    .local v1, "remoteLangVersion":Lcom/visionobjects/resourcemanager/util/Version;
    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/visionobjects/resourcemanager/util/Version;->compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I

    move-result v2

    if-lez v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public finalize()V
    .locals 2

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mUnregisterCompleted:Z

    if-nez v0, :cond_0

    .line 176
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    .line 177
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    if-nez v0, :cond_0

    .line 181
    :try_start_0
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sOnCancel:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 183
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public unregisterBroadCastReceiver()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/core/DownloadService;->unregisterBroadCastReceiver()V

    .line 195
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    .line 196
    sget v0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sCptRegisterReceiver:I

    if-nez v0, :cond_0

    .line 200
    :try_start_0
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sOnCancel:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mUnregisterCompleted:Z

    .line 209
    return-void

    .line 202
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public update(Ljava/lang/String;Landroid/app/PendingIntent;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "languageKey"    # Ljava/lang/String;
    .param p2, "pendingIntent"    # Landroid/app/PendingIntent;
    .param p3, "notificationTitle"    # Ljava/lang/String;
    .param p4, "successText"    # Ljava/lang/String;
    .param p5, "failedText"    # Ljava/lang/String;

    .prologue
    .line 406
    const/4 v4, 0x0

    .line 407
    .local v4, "mulAndLibUpdateCount":I
    const/4 v2, 0x0

    .line 408
    .local v2, "libUpdateNeeded":Z
    const/4 v5, 0x0

    .line 410
    .local v5, "mulUpdateNeeded":Z
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->updateAvailableForLanguage(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 412
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->notifyAborted(Ljava/lang/String;)V

    .line 457
    :goto_0
    return-void

    .line 415
    :cond_0
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->updateRequiredForLanguage(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 417
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->notifySuccess(Ljava/lang/String;)V

    goto :goto_0

    .line 421
    :cond_1
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mRemoteSystemHelper:Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v7}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v6

    .line 423
    .local v6, "version":Lcom/visionobjects/resourcemanager/util/Version;
    new-instance v0, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    iget-object v7, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-direct {v0, p1, v6, v7}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;-><init>(Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;Lcom/visionobjects/resourcemanager/core/DownloadService;)V

    .line 424
    .local v0, "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    invoke-virtual {v0, p2}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setPendingIntent(Landroid/app/PendingIntent;)V

    .line 425
    invoke-virtual {v0, p3}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setNotificationTitle(Ljava/lang/String;)V

    .line 426
    invoke-virtual {v0, p4}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setUpdateSuccessText(Ljava/lang/String;)V

    .line 427
    invoke-virtual {v0, p5}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setUpdateFailedText(Ljava/lang/String;)V

    .line 428
    sget v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sDownloadCount:I

    invoke-virtual {v0, v7}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->setId(I)V

    .line 429
    sget-object v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 431
    sget v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sDownloadCount:I

    add-int/lit8 v7, v7, 0x1

    sput v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sDownloadCount:I

    .line 433
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->libUpdateRequiredForLanguage(Ljava/lang/String;)Z

    move-result v2

    .line 434
    invoke-direct {p0, p1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mulUpdateRequiredForLanguage(Ljava/lang/String;)Z

    move-result v5

    .line 436
    if-eqz v2, :cond_2

    .line 437
    add-int/lit8 v4, v4, 0x1

    .line 438
    :cond_2
    if-eqz v5, :cond_3

    .line 439
    add-int/lit8 v4, v4, 0x1

    .line 441
    :cond_3
    if-lez v4, :cond_4

    .line 442
    new-instance v7, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v7, v4}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->sMulAndLibDownloadLatch:Ljava/util/concurrent/CountDownLatch;

    .line 444
    :cond_4
    if-eqz v2, :cond_5

    .line 446
    new-instance v1, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    const-string v7, "lib"

    iget-object v8, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-direct {v1, v7, v6, v8}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;-><init>(Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;Lcom/visionobjects/resourcemanager/core/DownloadService;)V

    .line 447
    .local v1, "lib":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    sget-object v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    invoke-direct {p0, v1}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->runUpdateThread(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    .line 450
    .end local v1    # "lib":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :cond_5
    if-eqz v5, :cond_6

    .line 452
    new-instance v3, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    const-string v7, "mul"

    iget-object v8, p0, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->mDownloadService:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-direct {v3, v7, v6, v8}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;-><init>(Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;Lcom/visionobjects/resourcemanager/core/DownloadService;)V

    .line 453
    .local v3, "mul":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    sget-object v7, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->ENQUEUED_LANGUAGES:Ljava/util/List;

    invoke-interface {v7, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 454
    invoke-direct {p0, v3}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->runUpdateThread(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    .line 456
    .end local v3    # "mul":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    :cond_6
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->runUpdateThread(Lcom/visionobjects/resourcemanager/core/LanguageToDownload;)V

    goto :goto_0
.end method
