.class public Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;
.super Landroid/os/AsyncTask;
.source "DownloadFileAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final DBG:Z = false

.field private static final MAX_BYTE_ARRAY_SIZE:I = 0x2000

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mOutputStream:Ljava/io/OutputStream;

.field private final mRemotePath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "remotePath"    # Ljava/lang/String;
    .param p2, "outputStream"    # Ljava/io/OutputStream;

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->mRemotePath:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->mOutputStream:Ljava/io/OutputStream;

    .line 54
    return-void
.end method

.method private process(Ljava/net/HttpURLConnection;Ljava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;Z)V
    .locals 5
    .param p1, "conn"    # Ljava/net/HttpURLConnection;
    .param p2, "in"    # Ljava/io/BufferedInputStream;
    .param p3, "out"    # Ljava/io/BufferedOutputStream;
    .param p4, "extendedTimeout"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 59
    new-instance v2, Ljava/net/URL;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getRootUrl()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->mRemotePath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 60
    .local v2, "url":Ljava/net/URL;
    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object p1

    .end local p1    # "conn":Ljava/net/HttpURLConnection;
    check-cast p1, Ljava/net/HttpURLConnection;

    .line 61
    .restart local p1    # "conn":Ljava/net/HttpURLConnection;
    if-eqz p4, :cond_0

    sget v3, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sConnectionExtendedTimeout:I

    :goto_0
    invoke-virtual {p1, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 62
    if-eqz p4, :cond_1

    sget v3, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sConnectionExtendedTimeout:I

    :goto_1
    invoke-virtual {p1, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 63
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->connect()V

    .line 66
    new-instance p2, Ljava/io/BufferedInputStream;

    .end local p2    # "in":Ljava/io/BufferedInputStream;
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {p2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 67
    .restart local p2    # "in":Ljava/io/BufferedInputStream;
    new-instance p3, Ljava/io/BufferedOutputStream;

    .end local p3    # "out":Ljava/io/BufferedOutputStream;
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->mOutputStream:Ljava/io/OutputStream;

    invoke-direct {p3, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 69
    .restart local p3    # "out":Ljava/io/BufferedOutputStream;
    const/16 v3, 0x2000

    new-array v0, v3, [B

    .line 72
    .local v0, "buffer":[B
    :goto_2
    invoke-virtual {p2, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v1

    .local v1, "length":I
    const/4 v3, -0x1

    if-eq v1, v3, :cond_2

    .line 74
    const/4 v3, 0x0

    invoke-virtual {p3, v0, v3, v1}, Ljava/io/BufferedOutputStream;->write([BII)V

    goto :goto_2

    .line 61
    .end local v0    # "buffer":[B
    .end local v1    # "length":I
    :cond_0
    sget v3, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sConnectionTimeout:I

    goto :goto_0

    .line 62
    :cond_1
    sget v3, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sConnectionTimeout:I

    goto :goto_1

    .line 76
    .restart local v0    # "buffer":[B
    .restart local v1    # "length":I
    :cond_2
    invoke-virtual {p3}, Ljava/io/BufferedOutputStream;->flush()V

    .line 77
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 87
    const/4 v5, 0x1

    .line 89
    .local v5, "success":Z
    const/4 v0, 0x0

    .line 90
    .local v0, "conn":Ljava/net/HttpURLConnection;
    const/4 v3, 0x0

    .line 91
    .local v3, "in":Ljava/io/BufferedInputStream;
    const/4 v4, 0x0

    .line 95
    .local v4, "out":Ljava/io/BufferedOutputStream;
    const/4 v6, 0x0

    :try_start_0
    invoke-direct {p0, v0, v3, v4, v6}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->process(Ljava/net/HttpURLConnection;Ljava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;Z)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_d
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    if-eqz v0, :cond_0

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 135
    :cond_0
    if-eqz v3, :cond_1

    .line 136
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 137
    :cond_1
    if-eqz v4, :cond_2

    .line 138
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 150
    :cond_2
    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    return-object v6

    .line 140
    :catch_0
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed to close input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 144
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error on closing input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 97
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v1

    .line 99
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_2
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "no server set"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 100
    const/4 v5, 0x0

    .line 132
    if-eqz v0, :cond_3

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 135
    :cond_3
    if-eqz v3, :cond_4

    .line 136
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 137
    :cond_4
    if-eqz v4, :cond_2

    .line 138
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_0

    .line 140
    :catch_3
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed to close input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 144
    .local v1, "e":Ljava/lang/NullPointerException;
    :catch_4
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error on closing input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 102
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_5
    move-exception v1

    .line 107
    .local v1, "e":Ljava/net/SocketTimeoutException;
    const/4 v6, 0x1

    :try_start_4
    invoke-direct {p0, v0, v3, v4, v6}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->process(Ljava/net/HttpURLConnection;Ljava/io/BufferedInputStream;Ljava/io/BufferedOutputStream;Z)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 132
    :goto_1
    if-eqz v0, :cond_5

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 135
    :cond_5
    if-eqz v3, :cond_6

    .line 136
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 137
    :cond_6
    if-eqz v4, :cond_2

    .line 138
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_6
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_9

    goto :goto_0

    .line 140
    :catch_6
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed to close input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 109
    .local v1, "e":Ljava/net/SocketTimeoutException;
    :catch_7
    move-exception v2

    .line 111
    .local v2, "e1":Ljava/io/IOException;
    :try_start_6
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    .line 112
    const/4 v5, 0x0

    .line 118
    goto :goto_1

    .line 114
    .end local v2    # "e1":Ljava/io/IOException;
    :catch_8
    move-exception v2

    .line 116
    .local v2, "e1":Ljava/lang/Exception;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error case found in 2nd try"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 117
    const/4 v5, 0x0

    goto :goto_1

    .line 144
    .end local v2    # "e1":Ljava/lang/Exception;
    :catch_9
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error on closing input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 120
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_a
    move-exception v1

    .line 122
    .local v1, "e":Ljava/io/IOException;
    :try_start_7
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "No connection right now or file not found"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 123
    const/4 v5, 0x0

    .line 132
    if-eqz v0, :cond_7

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 135
    :cond_7
    if-eqz v3, :cond_8

    .line 136
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 137
    :cond_8
    if-eqz v4, :cond_2

    .line 138
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_b
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_c

    goto/16 :goto_0

    .line 140
    :catch_b
    move-exception v1

    .line 142
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed to close input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 144
    :catch_c
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error on closing input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 125
    .end local v1    # "e":Ljava/lang/Exception;
    :catch_d
    move-exception v1

    .line 127
    .restart local v1    # "e":Ljava/lang/Exception;
    :try_start_9
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error case found in 1st try"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 128
    const/4 v5, 0x0

    .line 132
    if-eqz v0, :cond_9

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 135
    :cond_9
    if-eqz v3, :cond_a

    .line 136
    :try_start_a
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 137
    :cond_a
    if-eqz v4, :cond_2

    .line 138
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_e
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_f

    goto/16 :goto_0

    .line 140
    :catch_e
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Failed to close input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 144
    .local v1, "e":Ljava/lang/Exception;
    :catch_f
    move-exception v1

    .line 146
    sget-object v6, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v7, "Other error on closing input/output steam"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 132
    .end local v1    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v6

    if-eqz v0, :cond_b

    .line 133
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 135
    :cond_b
    if-eqz v3, :cond_c

    .line 136
    :try_start_b
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 137
    :cond_c
    if-eqz v4, :cond_d

    .line 138
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_10
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_11

    .line 147
    :cond_d
    :goto_2
    throw v6

    .line 140
    :catch_10
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    sget-object v7, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v8, "Failed to close input/output steam"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 144
    .end local v1    # "e":Ljava/io/IOException;
    :catch_11
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    sget-object v7, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->TAG:Ljava/lang/String;

    const-string v8, "Other error on closing input/output steam"

    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 172
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 173
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
