.class Lcom/visionobjects/resourcemanager/core/DownloadService$1;
.super Landroid/content/BroadcastReceiver;
.source "DownloadService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/visionobjects/resourcemanager/core/DownloadService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/visionobjects/resourcemanager/core/DownloadService;


# direct methods
.method constructor <init>(Lcom/visionobjects/resourcemanager/core/DownloadService;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/DownloadService$1;->this$0:Lcom/visionobjects/resourcemanager/core/DownloadService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 16
    .param p1, "ctxt"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 211
    const-string v12, "extra_download_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    .line 212
    .local v5, "fileId":Ljava/lang/Long;
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 213
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "> onReceive -download complete- "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :cond_0
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/visionobjects/resourcemanager/core/LanguageUpdater;->getLanguageForFileId(J)Lcom/visionobjects/resourcemanager/core/LanguageToDownload;

    move-result-object v7

    .line 218
    .local v7, "language":Lcom/visionobjects/resourcemanager/core/LanguageToDownload;
    if-nez v7, :cond_2

    .line 220
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_1

    .line 221
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "> onReceive -download complete- "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " stop processing"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 226
    :cond_2
    const/4 v4, 0x0

    .line 227
    .local v4, "dispName":Ljava/lang/String;
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_3

    .line 228
    invoke-virtual {v7}, Lcom/visionobjects/resourcemanager/core/LanguageToDownload;->getLanguageKey()Ljava/lang/String;

    move-result-object v4

    .line 230
    :cond_3
    new-instance v8, Landroid/app/DownloadManager$Query;

    invoke-direct {v8}, Landroid/app/DownloadManager$Query;-><init>()V

    .line 231
    .local v8, "query":Landroid/app/DownloadManager$Query;
    const/4 v12, 0x1

    new-array v12, v12, [J

    const/4 v13, 0x0

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    aput-wide v14, v12, v13

    invoke-virtual {v8, v12}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    .line 232
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->AndroidDownloadManager:Landroid/app/DownloadManager;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$200()Landroid/app/DownloadManager;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v3

    .line 234
    .local v3, "cursor":Landroid/database/Cursor;
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 236
    const-string v12, "status"

    invoke-interface {v3, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 237
    .local v2, "columnIndex":I
    invoke-interface {v3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    .line 239
    .local v11, "status":I
    const/16 v12, 0x10

    if-ne v11, v12, :cond_6

    .line 241
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_4

    .line 243
    const-string v12, "reason"

    invoke-interface {v3, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 244
    .local v10, "reasonIndex":I
    invoke-interface {v3, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 245
    .local v9, "reason":I
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ">>> Download FAILED in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", reason "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    .end local v9    # "reason":I
    .end local v10    # "reasonIndex":I
    :cond_4
    new-instance v12, Ljava/lang/Thread;

    new-instance v13, Lcom/visionobjects/resourcemanager/core/DownloadService$1$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v7, v5}, Lcom/visionobjects/resourcemanager/core/DownloadService$1$1;-><init>(Lcom/visionobjects/resourcemanager/core/DownloadService$1;Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Ljava/lang/Long;)V

    invoke-direct {v12, v13}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v12}, Ljava/lang/Thread;->start()V

    .line 289
    .end local v2    # "columnIndex":I
    .end local v11    # "status":I
    :cond_5
    :goto_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 257
    .restart local v2    # "columnIndex":I
    .restart local v11    # "status":I
    :cond_6
    const/4 v12, 0x4

    if-ne v11, v12, :cond_7

    .line 259
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 260
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ">>> Download PAUSED in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 262
    :cond_7
    const/4 v12, 0x1

    if-ne v11, v12, :cond_8

    .line 264
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 265
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ">>> Download PENDING in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 267
    :cond_8
    const/4 v12, 0x2

    if-ne v11, v12, :cond_9

    .line 269
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 270
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ">>> Download RUNNING in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 275
    :cond_9
    const-string v12, "local_uri"

    invoke-interface {v3, v12}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 276
    .local v1, "columnFilename":I
    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 277
    .local v6, "filename":Ljava/lang/String;
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->DBG:Z
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$000()Z

    move-result v12

    if-eqz v12, :cond_a

    .line 278
    # getter for: Lcom/visionobjects/resourcemanager/core/DownloadService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/DownloadService;->access$100()Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, ">>> Download SUCCESSFUL in "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_a
    new-instance v12, Ljava/lang/Thread;

    new-instance v13, Lcom/visionobjects/resourcemanager/core/DownloadService$1$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v7, v5, v6}, Lcom/visionobjects/resourcemanager/core/DownloadService$1$2;-><init>(Lcom/visionobjects/resourcemanager/core/DownloadService$1;Lcom/visionobjects/resourcemanager/core/LanguageToDownload;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-direct {v12, v13}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v12}, Ljava/lang/Thread;->start()V

    goto/16 :goto_1
.end method
