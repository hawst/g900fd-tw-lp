.class public Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;
.super Ljava/lang/Object;
.source "RemoteSystemHelper.java"


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;

.field private static sFallBackResTxt:Z

.field private static sLock:Ljava/lang/Object;

.field private static sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;-><init>(Landroid/content/Context;Z)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "forceUpdate"    # Z

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getLatestResourcesTxtFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 69
    .local v1, "fileName":Ljava/lang/String;
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->mContext:Landroid/content/Context;

    .line 71
    sget-object v3, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v3

    .line 73
    :try_start_0
    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    if-eqz v2, :cond_0

    if-eqz p2, :cond_2

    .line 75
    :cond_0
    new-instance v2, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-direct {v2, v1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    .line 77
    if-nez p2, :cond_1

    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->isOutdated()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->isPreloadedPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 80
    :cond_1
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getLatestFileName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->downloadToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 81
    .local v0, "bytes":[B
    if-nez v0, :cond_4

    .line 84
    invoke-static {p1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getLatestResourcesTxtFileName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 85
    if-eqz v1, :cond_3

    .line 86
    new-instance v2, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-direct {v2, v1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;-><init>(Ljava/lang/String;)V

    sput-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    .line 89
    :goto_0
    const/4 v2, 0x1

    sput-boolean v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sFallBackResTxt:Z

    .line 101
    .end local v0    # "bytes":[B
    :cond_2
    :goto_1
    monitor-exit v3

    .line 102
    return-void

    .line 88
    .restart local v0    # "bytes":[B
    :cond_3
    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->setFailed(Z)V

    goto :goto_0

    .line 101
    .end local v0    # "bytes":[B
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 93
    .restart local v0    # "bytes":[B
    :cond_4
    :try_start_1
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->initResourcesTxtParser([B)V

    goto :goto_1

    .line 98
    .end local v0    # "bytes":[B
    :cond_5
    const/4 v2, 0x0

    sput-boolean v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sFallBackResTxt:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private checkPermissionForDebug(Ljava/lang/String;)V
    .locals 5
    .param p1, "permission"    # Ljava/lang/String;

    .prologue
    .line 404
    iget-object v2, p0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v2, p1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    .line 405
    .local v1, "res":I
    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 406
    .local v0, "checkPermission":Z
    :goto_0
    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "..... check permission "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 407
    return-void

    .line 405
    .end local v0    # "checkPermission":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static downloadFile(Ljava/lang/String;Ljava/io/OutputStream;)Z
    .locals 6
    .param p0, "remotePath"    # Ljava/lang/String;
    .param p1, "outputStream"    # Ljava/io/OutputStream;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 222
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getRootUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getRootUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getRootUrl()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 224
    :cond_0
    const/4 v2, 0x0

    .line 266
    :cond_1
    :goto_0
    return v2

    .line 227
    :cond_2
    const/4 v2, 0x0

    .line 228
    .local v2, "result":Z
    new-instance v0, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;

    invoke-direct {v0, p0, p1}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;-><init>(Ljava/lang/String;Ljava/io/OutputStream;)V

    .line 230
    .local v0, "downloadTask":Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v4, v5, :cond_3

    .line 231
    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    check-cast v3, [Ljava/lang/Void;

    invoke-virtual {v0, v4, v3}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 238
    :goto_1
    const-wide/16 v3, 0x251c

    :try_start_0
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v3, v4, v5}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result v2

    .line 263
    :goto_2
    if-nez v2, :cond_1

    .line 264
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->cancel(Z)Z

    goto :goto_0

    .line 233
    :cond_3
    check-cast v3, [Ljava/lang/Void;

    invoke-virtual {v0, v3}, Lcom/visionobjects/resourcemanager/core/DownloadFileAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    .line 242
    :catch_0
    move-exception v1

    .line 244
    .local v1, "e":Ljava/lang/InterruptedException;
    sget-object v3, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "!!!!! InterruptedException during GET: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    const/4 v2, 0x0

    .line 261
    goto :goto_2

    .line 247
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v1

    .line 249
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    sget-object v3, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "!!!!! ExecutionException during GET: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    const/4 v2, 0x0

    .line 261
    goto :goto_2

    .line 252
    .end local v1    # "e":Ljava/util/concurrent/ExecutionException;
    :catch_2
    move-exception v1

    .line 254
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    sget-object v3, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "!!!!! Exception timeout to avoid Android ANR: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    const/4 v2, 0x0

    .line 261
    goto :goto_2

    .line 257
    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_3
    move-exception v1

    .line 259
    .local v1, "e":Ljava/lang/Exception;
    sget-object v3, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "!!!!! Other exception on running download Task: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static downloadToByteArray(Ljava/lang/String;)[B
    .locals 2
    .param p0, "remotePath"    # Ljava/lang/String;

    .prologue
    .line 303
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 305
    .local v0, "outputStream":Ljava/io/ByteArrayOutputStream;
    invoke-static {p0, v0}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->downloadFile(Ljava/lang/String;Ljava/io/OutputStream;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static downloadToLocalFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p0, "remotePath"    # Ljava/lang/String;
    .param p1, "localPath"    # Ljava/lang/String;

    .prologue
    .line 271
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    .local v1, "localFile":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    .line 273
    .local v4, "parentFile":Ljava/io/File;
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 274
    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    .line 276
    :cond_0
    const/4 v2, 0x0

    .line 279
    .local v2, "localFileStream":Ljava/io/OutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v2    # "localFileStream":Ljava/io/OutputStream;
    .local v3, "localFileStream":Ljava/io/OutputStream;
    move-object v2, v3

    .line 285
    .end local v3    # "localFileStream":Ljava/io/OutputStream;
    .restart local v2    # "localFileStream":Ljava/io/OutputStream;
    :goto_0
    invoke-static {p0, v2}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->downloadFile(Ljava/lang/String;Ljava/io/OutputStream;)Z

    move-result v6

    .line 288
    .local v6, "result":Z
    :try_start_1
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    .line 290
    new-instance v5, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;

    invoke-direct {v5, p1}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;-><init>(Ljava/lang/String;)V

    .line 291
    .local v5, "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    invoke-virtual {v5}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->hasFailed()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v7

    if-nez v7, :cond_1

    const/4 v7, 0x1

    :goto_1
    and-int/2addr v6, v7

    .line 298
    .end local v5    # "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    :goto_2
    return v6

    .line 281
    .end local v6    # "result":Z
    :catch_0
    move-exception v0

    .line 283
    .local v0, "e":Ljava/io/FileNotFoundException;
    sget-object v7, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Error accessing local file \""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "\""

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 291
    .end local v0    # "e":Ljava/io/FileNotFoundException;
    .restart local v5    # "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    .restart local v6    # "result":Z
    :cond_1
    const/4 v7, 0x0

    goto :goto_1

    .line 293
    .end local v5    # "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    :catch_1
    move-exception v0

    .line 295
    .local v0, "e":Ljava/io/IOException;
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private initResourcesTxtParser([B)V
    .locals 13
    .param p1, "bytes"    # [B

    .prologue
    .line 106
    sget-object v11, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v11

    .line 108
    :try_start_0
    new-instance v10, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    new-instance v12, Ljava/io/ByteArrayInputStream;

    invoke-direct {v12, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v10, v12}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;-><init>(Ljava/io/InputStream;)V

    sput-object v10, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    .line 110
    sget-object v10, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v10

    if-nez v10, :cond_2

    .line 112
    iget-object v10, p0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->mContext:Landroid/content/Context;

    sget-object v12, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v12}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v12

    invoke-static {v10, v12}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getResourcesTxtFileName(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;

    move-result-object v9

    .line 115
    .local v9, "resourcesTxt":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 116
    .local v5, "outFile":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v6

    .line 117
    .local v6, "parentFile":Ljava/io/File;
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    .line 119
    invoke-virtual {v6}, Ljava/io/File;->mkdirs()Z

    .line 120
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7}, Ljava/lang/String;-><init>()V

    .line 121
    .local v7, "path":Ljava/lang/String;
    sget-object v10, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 122
    .local v8, "paths":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v10, v8

    add-int/lit8 v10, v10, -0x1

    if-ge v2, v10, :cond_1

    .line 124
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    aget-object v12, v8, v2

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 125
    array-length v10, v8

    add-int/lit8 v10, v10, -0x3

    if-lt v2, v10, :cond_0

    .line 127
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 128
    .local v0, "dir":Ljava/io/File;
    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-virtual {v0, v10, v12}, Ljava/io/File;->setExecutable(ZZ)Z

    .line 129
    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-virtual {v0, v10, v12}, Ljava/io/File;->setReadable(ZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    .end local v0    # "dir":Ljava/io/File;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 135
    .end local v2    # "i":I
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "paths":[Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    .line 139
    .local v3, "out":Ljava/io/BufferedOutputStream;
    :try_start_1
    new-instance v4, Ljava/io/BufferedOutputStream;

    new-instance v10, Ljava/io/FileOutputStream;

    invoke-direct {v10, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v4, v10}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .local v4, "out":Ljava/io/BufferedOutputStream;
    :try_start_2
    invoke-virtual {v4, p1}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 141
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v3, v4

    .line 151
    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    :goto_1
    const/4 v10, 0x0

    :try_start_3
    sput-boolean v10, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sFallBackResTxt:Z

    .line 153
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .end local v5    # "outFile":Ljava/io/File;
    .end local v6    # "parentFile":Ljava/io/File;
    .end local v9    # "resourcesTxt":Ljava/lang/String;
    :cond_2
    monitor-exit v11

    .line 154
    return-void

    .line 143
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    .restart local v5    # "outFile":Ljava/io/File;
    .restart local v6    # "parentFile":Ljava/io/File;
    .restart local v9    # "resourcesTxt":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/io/FileNotFoundException;
    :goto_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 153
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .end local v5    # "outFile":Ljava/io/File;
    .end local v6    # "parentFile":Ljava/io/File;
    .end local v9    # "resourcesTxt":Ljava/lang/String;
    :catchall_0
    move-exception v10

    monitor-exit v11
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v10

    .line 147
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    .restart local v5    # "outFile":Ljava/io/File;
    .restart local v6    # "parentFile":Ljava/io/File;
    .restart local v9    # "resourcesTxt":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 149
    .local v1, "e":Ljava/io/IOException;
    :goto_3
    :try_start_4
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 147
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    goto :goto_3

    .line 143
    .end local v3    # "out":Ljava/io/BufferedOutputStream;
    .restart local v4    # "out":Ljava/io/BufferedOutputStream;
    :catch_3
    move-exception v1

    move-object v3, v4

    .end local v4    # "out":Ljava/io/BufferedOutputStream;
    .restart local v3    # "out":Ljava/io/BufferedOutputStream;
    goto :goto_2
.end method


# virtual methods
.method public getFilesTxtFilePath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 411
    sget-object v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 413
    :try_start_0
    sget-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    const/4 v0, 0x0

    monitor-exit v1

    .line 419
    :goto_0
    return-object v0

    .line 415
    :cond_0
    const-string v0, "lib"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 416
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getFilesTxt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 420
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 419
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getFilesTxt()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public getLangs()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 177
    sget-object v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 179
    :try_start_0
    sget-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getLangs()Ljava/util/Set;

    move-result-object v0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 193
    sget-object v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    sget-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v0, p1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getResFileInfos(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 15
    .param p1, "lang"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/visionobjects/resourcemanager/model/ResFileInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    .line 319
    sget-object v13, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v13

    .line 321
    :try_start_0
    sget-object v11, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v11}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getLangs()Ljava/util/Set;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_1

    .line 324
    monitor-exit v13

    move-object v3, v12

    .line 393
    :cond_0
    :goto_0
    return-object v3

    .line 326
    :cond_1
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 328
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 330
    .local v3, "fileInfos":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    const/4 v1, 0x0

    .line 334
    .local v1, "conn":Ljava/net/HttpURLConnection;
    :try_start_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v13

    invoke-virtual {v13}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getRootUrl()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual/range {p0 .. p1}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->getFilesTxtFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 352
    .local v10, "urlPath":Ljava/lang/String;
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 353
    .local v9, "url":Ljava/net/URL;
    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v11

    move-object v0, v11

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v1, v0

    .line 354
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->connect()V

    .line 356
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    .line 357
    .local v8, "stream":Ljava/io/InputStream;
    new-instance v7, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;

    invoke-direct {v7, v8}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;-><init>(Ljava/io/InputStream;)V

    .line 359
    .local v7, "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    invoke-virtual {v7}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->hasFailed()Z

    move-result v11

    if-nez v11, :cond_3

    .line 361
    invoke-virtual {v7}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->getNames()Ljava/util/Set;

    move-result-object v6

    .line 362
    .local v6, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 364
    .local v5, "name":Ljava/lang/String;
    new-instance v11, Lcom/visionobjects/resourcemanager/model/ResFileInfo;

    invoke-virtual {v7, v5}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->getMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v7, v5}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->getSize(Ljava/lang/String;)I

    move-result v14

    invoke-direct {v11, v5, v13, v14}, Lcom/visionobjects/resourcemanager/model/ResFileInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v3, v5, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    .line 369
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "name":Ljava/lang/String;
    .end local v6    # "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v7    # "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    .end local v8    # "stream":Ljava/io/InputStream;
    .end local v9    # "url":Ljava/net/URL;
    .end local v10    # "urlPath":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 371
    .local v2, "e":Ljava/io/IOException;
    :try_start_2
    sget-object v11, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "> the target server failed to respond: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v11, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 387
    if-eqz v1, :cond_2

    .line 388
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    move-object v3, v12

    goto/16 :goto_0

    .line 326
    .end local v1    # "conn":Ljava/net/HttpURLConnection;
    .end local v2    # "e":Ljava/io/IOException;
    .end local v3    # "fileInfos":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    :catchall_0
    move-exception v11

    :try_start_3
    monitor-exit v13
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v11

    .line 387
    .restart local v1    # "conn":Ljava/net/HttpURLConnection;
    .restart local v3    # "fileInfos":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    .restart local v7    # "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    .restart local v8    # "stream":Ljava/io/InputStream;
    .restart local v9    # "url":Ljava/net/URL;
    .restart local v10    # "urlPath":Ljava/lang/String;
    :cond_3
    if-eqz v1, :cond_0

    .line 388
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    .line 375
    .end local v7    # "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    .end local v8    # "stream":Ljava/io/InputStream;
    .end local v9    # "url":Ljava/net/URL;
    .end local v10    # "urlPath":Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 377
    .local v2, "e":Ljava/lang/NullPointerException;
    :try_start_4
    sget-object v11, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    const-string v13, "server not yet set"

    invoke-static {v11, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 387
    if-eqz v1, :cond_4

    .line 388
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move-object v3, v12

    goto/16 :goto_0

    .line 380
    .end local v2    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v2

    .line 382
    .local v2, "e":Ljava/lang/SecurityException;
    :try_start_5
    sget-object v11, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->TAG:Ljava/lang/String;

    const-string v13, "security exception, user may have forbidden internet access"

    invoke-static {v11, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 387
    if-eqz v1, :cond_5

    .line 388
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    move-object v3, v12

    goto/16 :goto_0

    .line 387
    .end local v2    # "e":Ljava/lang/SecurityException;
    :catchall_1
    move-exception v11

    if-eqz v1, :cond_6

    .line 388
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    throw v11
.end method

.method public getVersion()Lcom/visionobjects/resourcemanager/util/Version;
    .locals 3

    .prologue
    .line 207
    sget-object v2, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v2

    .line 209
    :try_start_0
    sget-boolean v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sFallBackResTxt:Z

    if-eqz v1, :cond_0

    .line 211
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getInstance()Lcom/visionobjects/resourcemanager/model/VODBConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/visionobjects/resourcemanager/model/VODBConfiguration;->getLatestFileName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->downloadToByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 212
    .local v0, "bytes":[B
    if-eqz v0, :cond_0

    .line 213
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->initResourcesTxtParser([B)V

    .line 215
    .end local v0    # "bytes":[B
    :cond_0
    sget-object v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v1

    :goto_0
    monitor-exit v2

    return-object v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 216
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public hasResourcesParserfailed()Z
    .locals 2

    .prologue
    .line 163
    sget-object v1, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 165
    :try_start_0
    sget-object v0, Lcom/visionobjects/resourcemanager/core/RemoteSystemHelper;->sResTxtParser:Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->failed()Z

    move-result v0

    monitor-exit v1

    return v0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
