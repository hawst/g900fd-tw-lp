.class public Lcom/visionobjects/resourcemanager/core/FileSystemHelper;
.super Ljava/lang/Object;
.source "FileSystemHelper.java"


# static fields
.field private static final DBG:Z

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mLanguageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/visionobjects/resourcemanager/model/Resource;",
            ">;"
        }
    .end annotation
.end field

.field private final mLibMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/visionobjects/resourcemanager/util/Version;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mContext:Landroid/content/Context;

    .line 45
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLibMap:Ljava/util/HashMap;

    .line 46
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    .line 48
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getSearchPaths(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 49
    .local v0, "searchPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 51
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillLanguageMap(Ljava/util/List;)V

    .line 52
    invoke-direct {p0, v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillLibMap(Ljava/util/List;)V

    .line 55
    :cond_0
    sget-boolean v1, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUsePreloadPath:Z

    if-eqz v1, :cond_1

    .line 56
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillPreLoadedVersion()V

    .line 57
    :cond_1
    invoke-direct {p0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->keepOnlyMostRecentLibVersion()V

    .line 58
    return-void
.end method

.method public static cleanDownloadFiles()V
    .locals 9

    .prologue
    .line 443
    sget-object v7, Lcom/visionobjects/resourcemanager/core/DownloadService;->mDirectoryPath:Ljava/lang/String;

    invoke-static {v7}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 444
    .local v1, "baseDirectory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v4

    .line 445
    .local v4, "filenames":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 447
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_1

    aget-object v3, v0, v5

    .line 449
    .local v3, "filename":Ljava/lang/String;
    const-string v7, "tempoVoFiles"

    invoke-virtual {v3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 451
    new-instance v2, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 452
    .local v2, "f":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 453
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 447
    .end local v2    # "f":Ljava/io/File;
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 457
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v3    # "filename":Ljava/lang/String;
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    :cond_1
    return-void
.end method

.method public static cleanFailedLangs(Landroid/content/Context;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 411
    new-instance v11, Ljava/io/File;

    invoke-static {p0}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v10

    .line 412
    .local v10, "versionDirs":[Ljava/io/File;
    if-eqz v10, :cond_2

    .line 414
    move-object v0, v10

    .local v0, "arr$":[Ljava/io/File;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    move v4, v3

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v7    # "len$":I
    .local v4, "i$":I
    :goto_0
    if-ge v4, v7, :cond_2

    aget-object v9, v0, v4

    .line 416
    .local v9, "versionDir":Ljava/io/File;
    invoke-virtual {v9}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 418
    invoke-virtual {v9}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 419
    .local v6, "langDirs":[Ljava/io/File;
    if-eqz v6, :cond_1

    .line 421
    move-object v1, v6

    .local v1, "arr$":[Ljava/io/File;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v3, 0x0

    .end local v4    # "i$":I
    .restart local v3    # "i$":I
    :goto_1
    if-ge v3, v8, :cond_1

    aget-object v5, v1, v3

    .line 423
    .local v5, "langDir":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->isDirectory()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 425
    new-instance v2, Ljava/io/File;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "checked"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 426
    .local v2, "checkedFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 428
    invoke-static {v5}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/io/File;)Z

    .line 421
    .end local v2    # "checkedFile":Ljava/io/File;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 414
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v3    # "i$":I
    .end local v5    # "langDir":Ljava/io/File;
    .end local v6    # "langDirs":[Ljava/io/File;
    .end local v8    # "len$":I
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .restart local v3    # "i$":I
    move v4, v3

    .end local v3    # "i$":I
    .restart local v4    # "i$":I
    goto :goto_0

    .line 436
    .end local v4    # "i$":I
    .end local v9    # "versionDir":Ljava/io/File;
    :cond_2
    return-void
.end method

.method private fillLanguageMap(Ljava/lang/String;Ljava/io/File;Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;)V
    .locals 14
    .param p1, "fileName"    # Ljava/lang/String;
    .param p2, "dir"    # Ljava/io/File;
    .param p3, "version"    # Lcom/visionobjects/resourcemanager/util/Version;
    .param p4, "baseDir"    # Ljava/lang/String;

    .prologue
    .line 239
    new-instance v10, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-direct {v10, p1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;-><init>(Ljava/lang/String;)V

    .line 241
    .local v10, "parser":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getLangs()Ljava/util/Set;

    move-result-object v7

    .line 242
    .local v7, "langs":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    new-instance v12, Ljava/io/File;

    move-object/from16 v0, p4

    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v9

    .line 243
    .local v9, "listFile":[Ljava/lang/String;
    if-eqz v9, :cond_2

    .line 245
    move-object v1, v9

    .local v1, "arr$":[Ljava/lang/String;
    array-length v8, v1

    .local v8, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v8, :cond_2

    aget-object v5, v1, v4

    .line 247
    .local v5, "lang":Ljava/lang/String;
    invoke-interface {v7, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 249
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p4

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 255
    .local v6, "langPath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 256
    .local v3, "directory":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 258
    new-instance v2, Ljava/io/File;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    sget-object v13, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "checked"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v2, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 259
    .local v2, "checked":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 261
    invoke-virtual {v3}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v12

    array-length v12, v12

    const/4 v13, 0x2

    if-le v12, v13, :cond_1

    .line 263
    iget-object v12, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v12, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 265
    new-instance v11, Lcom/visionobjects/resourcemanager/model/Resource;

    invoke-virtual {v10, v5}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p3

    invoke-direct {v11, v5, v12, v6, v0}, Lcom/visionobjects/resourcemanager/model/Resource;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;)V

    .line 266
    .local v11, "resource":Lcom/visionobjects/resourcemanager/model/Resource;
    iget-object v12, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v12, v5, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    .end local v2    # "checked":Ljava/io/File;
    .end local v3    # "directory":Ljava/io/File;
    .end local v6    # "langPath":Ljava/lang/String;
    .end local v11    # "resource":Lcom/visionobjects/resourcemanager/model/Resource;
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 272
    .restart local v2    # "checked":Ljava/io/File;
    .restart local v3    # "directory":Ljava/io/File;
    .restart local v6    # "langPath":Ljava/lang/String;
    :cond_1
    invoke-static {v3}, Lcom/visionobjects/resourcemanager/util/Utils;->deleteDir(Ljava/io/File;)Z

    goto :goto_1

    .line 279
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "checked":Ljava/io/File;
    .end local v3    # "directory":Ljava/io/File;
    .end local v4    # "i$":I
    .end local v5    # "lang":Ljava/lang/String;
    .end local v6    # "langPath":Ljava/lang/String;
    .end local v8    # "len$":I
    :cond_2
    return-void
.end method

.method private fillLanguageMap(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 283
    .local p1, "searchPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 285
    .local v5, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 287
    .local v1, "dir":Ljava/io/File;
    invoke-static {v1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->filterAndSortVersionFolders(Ljava/io/File;)Ljava/util/List;

    move-result-object v7

    .line 289
    .local v7, "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/visionobjects/resourcemanager/util/Version;

    .line 291
    .local v6, "version":Lcom/visionobjects/resourcemanager/util/Version;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "resources.txt"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 292
    .local v2, "fileName":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 293
    .local v0, "baseDir":Ljava/lang/String;
    invoke-direct {p0, v2, v1, v6, v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillLanguageMap(Ljava/lang/String;Ljava/io/File;Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;)V

    goto :goto_0

    .line 296
    .end local v0    # "baseDir":Ljava/lang/String;
    .end local v1    # "dir":Ljava/io/File;
    .end local v2    # "fileName":Ljava/lang/String;
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "path":Ljava/lang/String;
    .end local v6    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    .end local v7    # "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :cond_1
    return-void
.end method

.method private fillLibMap(Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;)V
    .locals 5
    .param p1, "version"    # Lcom/visionobjects/resourcemanager/util/Version;
    .param p2, "baseDir"    # Ljava/lang/String;

    .prologue
    .line 198
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "lib"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 199
    .local v2, "libPath":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    .local v1, "directory":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 202
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getAbsoluteFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "checked"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 203
    .local v0, "checked":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 209
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v3

    array-length v3, v3

    const/4 v4, 0x2

    if-le v3, v4, :cond_1

    .line 211
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLibMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    .end local v0    # "checked":Ljava/io/File;
    :cond_0
    :goto_0
    return-void

    .line 215
    .restart local v0    # "checked":Ljava/io/File;
    :cond_1
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method private fillLibMap(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "searchPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 225
    .local v4, "path":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 227
    .local v1, "dir":Ljava/io/File;
    invoke-static {v1}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->filterAndSortVersionFolders(Ljava/io/File;)Ljava/util/List;

    move-result-object v6

    .line 229
    .local v6, "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/visionobjects/resourcemanager/util/Version;

    .line 231
    .local v5, "version":Lcom/visionobjects/resourcemanager/util/Version;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "baseDir":Ljava/lang/String;
    invoke-direct {p0, v5, v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillLibMap(Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;)V

    goto :goto_0

    .line 235
    .end local v0    # "baseDir":Ljava/lang/String;
    .end local v1    # "dir":Ljava/io/File;
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "path":Ljava/lang/String;
    .end local v5    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    .end local v6    # "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :cond_1
    return-void
.end method

.method private fillPreLoadedVersion()V
    .locals 9

    .prologue
    .line 82
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, "preloadedPath":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 85
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 86
    .local v2, "preloadedDir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 88
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "resources.txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 89
    .local v1, "fileName":Ljava/lang/String;
    new-instance v4, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-direct {v4, v1}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;-><init>(Ljava/lang/String;)V

    .line 90
    .local v4, "resourcesTxtParser":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 91
    .local v5, "searchPath":Ljava/io/File;
    invoke-virtual {v4}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v6

    .line 92
    .local v6, "version":Lcom/visionobjects/resourcemanager/util/Version;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "baseDir":Ljava/lang/String;
    invoke-direct {p0, v1, v5, v6, v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillLanguageMap(Ljava/lang/String;Ljava/io/File;Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;)V

    .line 94
    invoke-direct {p0, v6, v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->fillLibMap(Lcom/visionobjects/resourcemanager/util/Version;Ljava/lang/String;)V

    .line 97
    .end local v0    # "baseDir":Ljava/lang/String;
    .end local v1    # "fileName":Ljava/lang/String;
    .end local v2    # "preloadedDir":Ljava/io/File;
    .end local v4    # "resourcesTxtParser":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    .end local v5    # "searchPath":Ljava/io/File;
    .end local v6    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_0
    return-void
.end method

.method private static filterAndSortVersionFolders(Ljava/io/File;)Ljava/util/List;
    .locals 15
    .param p0, "dir"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/visionobjects/resourcemanager/util/Version;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    invoke-virtual {p0}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v8

    .line 134
    .local v8, "items":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 136
    .local v1, "directlyBurnDirectoy":Z
    move-object v0, v8

    .local v0, "arr$":[Ljava/lang/String;
    array-length v9, v0

    .local v9, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v9, :cond_1

    aget-object v3, v0, v5

    .line 138
    .local v3, "filename":Ljava/lang/String;
    const-string v13, "resources.txt"

    invoke-virtual {v3, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 141
    const/4 v1, 0x1

    .line 136
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 145
    .end local v3    # "filename":Ljava/lang/String;
    :cond_1
    if-eqz v1, :cond_5

    .line 147
    const/4 v6, 0x0

    .line 148
    .local v6, "in":Ljava/io/FileInputStream;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v12, "version":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "resources.txt"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    .end local v6    # "in":Ljava/io/FileInputStream;
    .local v7, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v10, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;

    invoke-direct {v10, v7}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;-><init>(Ljava/io/InputStream;)V

    .line 153
    .local v10, "res":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    invoke-virtual {v10}, Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 162
    if-eqz v7, :cond_2

    .line 163
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_2
    move-object v6, v7

    .line 192
    .end local v7    # "in":Ljava/io/FileInputStream;
    .end local v10    # "res":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    .end local v12    # "version":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :cond_3
    :goto_1
    return-object v12

    .line 165
    .restart local v7    # "in":Ljava/io/FileInputStream;
    .restart local v10    # "res":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    .restart local v12    # "version":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :catch_0
    move-exception v13

    move-object v6, v7

    .line 168
    .end local v7    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    goto :goto_1

    .line 155
    .end local v10    # "res":Lcom/visionobjects/resourcemanager/model/ResourcesTxtParser;
    :catch_1
    move-exception v13

    .line 162
    :goto_2
    if-eqz v6, :cond_3

    .line 163
    :try_start_3
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1

    .line 165
    :catch_2
    move-exception v13

    goto :goto_1

    .line 160
    :catchall_0
    move-exception v13

    .line 162
    :goto_3
    if-eqz v6, :cond_4

    .line 163
    :try_start_4
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 167
    :cond_4
    :goto_4
    throw v13

    .line 176
    .end local v6    # "in":Ljava/io/FileInputStream;
    .end local v12    # "version":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :cond_5
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 177
    .local v11, "sortedVersions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    if-eqz v8, :cond_8

    .line 180
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_5
    array-length v13, v8

    if-ge v4, v13, :cond_7

    .line 182
    new-instance v2, Ljava/io/File;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v8, v4

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v2, v13}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 183
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v13

    if-eqz v13, :cond_6

    .line 184
    new-instance v13, Lcom/visionobjects/resourcemanager/util/Version;

    aget-object v14, v8, v4

    invoke-direct {v13, v14}, Lcom/visionobjects/resourcemanager/util/Version;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 188
    .end local v2    # "file":Ljava/io/File;
    :cond_7
    invoke-static {v11}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 190
    invoke-static {v11}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .end local v4    # "i":I
    :cond_8
    move-object v12, v11

    .line 192
    goto :goto_1

    .line 165
    .end local v11    # "sortedVersions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    .restart local v6    # "in":Ljava/io/FileInputStream;
    .restart local v12    # "version":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :catch_3
    move-exception v14

    goto :goto_4

    .line 160
    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v7    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v13

    move-object v6, v7

    .end local v7    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 155
    .end local v6    # "in":Ljava/io/FileInputStream;
    .restart local v7    # "in":Ljava/io/FileInputStream;
    :catch_4
    move-exception v13

    move-object v6, v7

    .end local v7    # "in":Ljava/io/FileInputStream;
    .restart local v6    # "in":Ljava/io/FileInputStream;
    goto :goto_2
.end method

.method public static getDownloadingLanguages(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 16
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/visionobjects/resourcemanager/util/Version;",
            ">;"
        }
    .end annotation

    .prologue
    .line 370
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 372
    .local v3, "downloadingMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/util/Version;>;"
    new-instance v14, Ljava/io/File;

    invoke-static/range {p0 .. p0}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v13

    .line 373
    .local v13, "versionDirs":[Ljava/io/File;
    if-eqz v13, :cond_2

    .line 375
    move-object v0, v13

    .local v0, "arr$":[Ljava/io/File;
    array-length v9, v0

    .local v9, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v5, v4

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v9    # "len$":I
    .local v5, "i$":I
    :goto_0
    if-ge v5, v9, :cond_2

    aget-object v12, v0, v5

    .line 377
    .local v12, "versionDir":Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 379
    invoke-virtual {v12}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v8

    .line 380
    .local v8, "langDirs":[Ljava/io/File;
    new-instance v11, Lcom/visionobjects/resourcemanager/util/Version;

    invoke-virtual {v12}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v11, v14}, Lcom/visionobjects/resourcemanager/util/Version;-><init>(Ljava/lang/String;)V

    .line 381
    .local v11, "version":Lcom/visionobjects/resourcemanager/util/Version;
    if-eqz v8, :cond_1

    .line 383
    move-object v1, v8

    .local v1, "arr$":[Ljava/io/File;
    array-length v10, v1

    .local v10, "len$":I
    const/4 v4, 0x0

    .end local v5    # "i$":I
    .restart local v4    # "i$":I
    :goto_1
    if-ge v4, v10, :cond_1

    aget-object v7, v1, v4

    .line 385
    .local v7, "langDir":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 387
    new-instance v2, Ljava/io/File;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    sget-object v15, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "checked"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v2, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 388
    .local v2, "checkedFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_0

    .line 390
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 391
    .local v6, "lang":Ljava/lang/String;
    invoke-virtual {v3, v6, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 383
    .end local v2    # "checkedFile":Ljava/io/File;
    .end local v6    # "lang":Ljava/lang/String;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 375
    .end local v1    # "arr$":[Ljava/io/File;
    .end local v4    # "i$":I
    .end local v7    # "langDir":Ljava/io/File;
    .end local v8    # "langDirs":[Ljava/io/File;
    .end local v10    # "len$":I
    .end local v11    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_1
    add-int/lit8 v4, v5, 0x1

    .restart local v4    # "i$":I
    move v5, v4

    .end local v4    # "i$":I
    .restart local v5    # "i$":I
    goto :goto_0

    .line 399
    .end local v5    # "i$":I
    .end local v12    # "versionDir":Ljava/io/File;
    :cond_2
    invoke-virtual {v3}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v14

    invoke-interface {v14}, Ljava/util/Set;->size()I

    move-result v14

    if-nez v14, :cond_3

    .line 402
    invoke-static {}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->cleanDownloadFiles()V

    .line 404
    :cond_3
    return-object v3
.end method

.method public static getFilesTxtFileName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "versionDir"    # Ljava/lang/String;
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "files.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLatestResourcesTxtFileName(Landroid/content/Context;)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 465
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->getSearchPaths(Landroid/content/Context;)Ljava/util/List;

    move-result-object v8

    .line 467
    .local v8, "searchPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v5, 0x0

    .line 468
    .local v5, "latestVersion":Lcom/visionobjects/resourcemanager/util/Version;
    const/4 v4, 0x0

    .line 469
    .local v4, "latestResourcesTxt":Ljava/lang/String;
    const/4 v7, 0x0

    .line 473
    .local v7, "preLoadResourcesTxt":Ljava/lang/String;
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 475
    .local v6, "path":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 477
    .local v0, "dir":Ljava/io/File;
    invoke-static {v0}, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->filterAndSortVersionFolders(Ljava/io/File;)Ljava/util/List;

    move-result-object v10

    .line 479
    .local v10, "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    .line 481
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/visionobjects/resourcemanager/util/Version;

    .line 482
    .local v9, "version":Lcom/visionobjects/resourcemanager/util/Version;
    if-eqz v5, :cond_1

    invoke-virtual {v5, v9}, Lcom/visionobjects/resourcemanager/util/Version;->compareTo(Lcom/visionobjects/resourcemanager/util/Version;)I

    move-result v11

    if-gez v11, :cond_0

    .line 484
    :cond_1
    move-object v5, v9

    .line 485
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "resources.txt"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 486
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 487
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v11

    if-nez v11, :cond_0

    .line 490
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    sget-object v12, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "resources.txt"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 491
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 492
    .local v2, "fPre":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v11

    if-eqz v11, :cond_0

    .line 494
    move-object v4, v7

    goto :goto_0

    .line 502
    .end local v0    # "dir":Ljava/io/File;
    .end local v1    # "f":Ljava/io/File;
    .end local v2    # "fPre":Ljava/io/File;
    .end local v6    # "path":Ljava/lang/String;
    .end local v9    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    .end local v10    # "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    :cond_2
    return-object v4
.end method

.method public static getPreLoadedResourcesTxtFileName(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 510
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v1

    .line 511
    .local v1, "preloadedPath":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 513
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 514
    .local v0, "preloadedDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 515
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "resources.txt"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 517
    .end local v0    # "preloadedDir":Ljava/io/File;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getResourcesTxtFileName(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "version"    # Lcom/visionobjects/resourcemanager/util/Version;

    .prologue
    .line 529
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "resources.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSearchPaths(Landroid/content/Context;)Ljava/util/List;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 112
    .local v4, "searchPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p0}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/visionobjects/resourcemanager/util/Utils;->computePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 113
    .local v3, "path":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 114
    .local v2, "dir":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_0
    sget-boolean v5, Lcom/visionobjects/resourcemanager/model/LocalConfig;->sUsePreloadPath:Z

    if-eqz v5, :cond_1

    .line 121
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/visionobjects/resourcemanager/util/Utils;->computePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "PreLoadPath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 123
    .local v0, "PreLoadDir":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 125
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    .end local v0    # "PreLoadDir":Ljava/io/File;
    .end local v1    # "PreLoadPath":Ljava/lang/String;
    :cond_1
    return-object v4
.end method

.method public static getVersionDir(Landroid/content/Context;Lcom/visionobjects/resourcemanager/util/Version;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "version"    # Lcom/visionobjects/resourcemanager/util/Version;

    .prologue
    .line 523
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getLocalPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isPreloadedPath(Ljava/lang/String;)Z
    .locals 2
    .param p0, "pathName"    # Ljava/lang/String;

    .prologue
    .line 548
    invoke-static {}, Lcom/visionobjects/resourcemanager/model/LocalConfig;->getPreloadPath()Ljava/lang/String;

    move-result-object v0

    .line 549
    .local v0, "preloadedPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private keepOnlyMostRecentLibVersion()V
    .locals 5

    .prologue
    .line 65
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v2, "versions":Ljava/util/List;, "Ljava/util/List<Lcom/visionobjects/resourcemanager/util/Version;>;"
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLibMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 67
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    .line 69
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 70
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 71
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/visionobjects/resourcemanager/util/Version;

    .line 73
    .local v1, "version":Lcom/visionobjects/resourcemanager/util/Version;
    iget-object v3, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLibMap:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 76
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "version":Lcom/visionobjects/resourcemanager/util/Version;
    :cond_0
    return-void
.end method


# virtual methods
.method public getLangs()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 302
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getLibs()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Lcom/visionobjects/resourcemanager/util/Version;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 362
    iget-object v0, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLibMap:Ljava/util/HashMap;

    return-object v0
.end method

.method public getMd5(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 353
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/resourcemanager/model/Resource;

    .line 354
    .local v0, "r":Lcom/visionobjects/resourcemanager/model/Resource;
    if-nez v0, :cond_0

    .line 355
    const/4 v1, 0x0

    .line 357
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/Resource;->getMd5()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 307
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/resourcemanager/model/Resource;

    .line 308
    .local v0, "r":Lcom/visionobjects/resourcemanager/model/Resource;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/Resource;->getFolder()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getResFileInfos(Ljava/lang/String;)Ljava/util/HashMap;
    .locals 10
    .param p1, "language"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/visionobjects/resourcemanager/model/ResFileInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    iget-object v7, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/visionobjects/resourcemanager/model/Resource;

    .line 328
    .local v6, "resource":Lcom/visionobjects/resourcemanager/model/Resource;
    if-nez v6, :cond_1

    .line 331
    const/4 v1, 0x0

    .line 348
    :cond_0
    return-object v1

    .line 334
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 336
    .local v1, "fileInfos":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/visionobjects/resourcemanager/model/ResFileInfo;>;"
    invoke-virtual {v6}, Lcom/visionobjects/resourcemanager/model/Resource;->getFolder()Ljava/lang/String;

    move-result-object v0

    .line 337
    .local v0, "dir":Ljava/lang/String;
    new-instance v5, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "files.txt"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v7}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;-><init>(Ljava/lang/String;)V

    .line 339
    .local v5, "parser":Lcom/visionobjects/resourcemanager/model/FilesTxtParser;
    invoke-virtual {v5}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->hasFailed()Z

    move-result v7

    if-nez v7, :cond_0

    .line 341
    invoke-virtual {v5}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->getNames()Ljava/util/Set;

    move-result-object v4

    .line 342
    .local v4, "names":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 344
    .local v3, "name":Ljava/lang/String;
    new-instance v7, Lcom/visionobjects/resourcemanager/model/ResFileInfo;

    invoke-virtual {v5, v3}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->getMd5(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v3}, Lcom/visionobjects/resourcemanager/model/FilesTxtParser;->getSize(Ljava/lang/String;)I

    move-result v9

    invoke-direct {v7, v3, v8, v9}, Lcom/visionobjects/resourcemanager/model/ResFileInfo;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v3, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public getVersion(Ljava/lang/String;)Lcom/visionobjects/resourcemanager/util/Version;
    .locals 2
    .param p1, "lang"    # Ljava/lang/String;

    .prologue
    .line 313
    iget-object v1, p0, Lcom/visionobjects/resourcemanager/core/FileSystemHelper;->mLanguageMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/visionobjects/resourcemanager/model/Resource;

    .line 314
    .local v0, "r":Lcom/visionobjects/resourcemanager/model/Resource;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/visionobjects/resourcemanager/model/Resource;->getVersion()Lcom/visionobjects/resourcemanager/util/Version;

    move-result-object v1

    goto :goto_0
.end method
