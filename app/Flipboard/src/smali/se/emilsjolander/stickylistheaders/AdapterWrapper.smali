.class Lse/emilsjolander/stickylistheaders/AdapterWrapper;
.super Landroid/widget/BaseAdapter;
.source "AdapterWrapper.java"

# interfaces
.implements Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;


# instance fields
.field final a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

.field b:Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:I

.field private g:Landroid/database/DataSetObserver;


# direct methods
.method constructor <init>(Landroid/content/Context;Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->c:Ljava/util/List;

    .line 38
    new-instance v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper$1;

    invoke-direct {v0, p0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper$1;-><init>(Lse/emilsjolander/stickylistheaders/AdapterWrapper;)V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->g:Landroid/database/DataSetObserver;

    .line 54
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->d:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    .line 56
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->g:Landroid/database/DataSetObserver;

    invoke-interface {p2, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 57
    return-void
.end method

.method static synthetic a(Lse/emilsjolander/stickylistheaders/AdapterWrapper;)Ljava/util/List;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->c:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lse/emilsjolander/stickylistheaders/AdapterWrapper;)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    return-void
.end method

.method static synthetic c(Lse/emilsjolander/stickylistheaders/AdapterWrapper;)V
    .locals 0

    .prologue
    .line 26
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method static synthetic d(Lse/emilsjolander/stickylistheaders/AdapterWrapper;)Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b:Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1, p2, p3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method final a(Landroid/graphics/drawable/Drawable;I)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->e:Landroid/graphics/drawable/Drawable;

    .line 61
    iput p2, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->f:I

    .line 62
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->notifyDataSetChanged()V

    .line 63
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0, p1, p2, p3}, Landroid/widget/BaseAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public synthetic getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 26
    if-nez p2, :cond_2

    new-instance p2, Lse/emilsjolander/stickylistheaders/WrapperView;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->d:Landroid/content/Context;

    invoke-direct {p2, v1}, Lse/emilsjolander/stickylistheaders/WrapperView;-><init>(Landroid/content/Context;)V

    :goto_0
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    iget-object v4, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->a:Landroid/view/View;

    invoke-interface {v1, p1, v4, p3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    if-eqz p1, :cond_3

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v1, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->b(I)J

    move-result-wide v6

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    add-int/lit8 v5, p1, -0x1

    invoke-interface {v1, v5}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->b(I)J

    move-result-wide v8

    cmp-long v1, v6, v8

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    iget-object v1, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->c:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    move-object v1, v0

    :goto_2
    instance-of v0, v4, Landroid/widget/Checkable;

    if-eqz v0, :cond_8

    instance-of v0, p2, Lse/emilsjolander/stickylistheaders/CheckableWrapperView;

    if-nez v0, :cond_8

    new-instance p2, Lse/emilsjolander/stickylistheaders/CheckableWrapperView;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->d:Landroid/content/Context;

    invoke-direct {p2, v0}, Lse/emilsjolander/stickylistheaders/CheckableWrapperView;-><init>(Landroid/content/Context;)V

    :cond_1
    :goto_3
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->e:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->f:I

    if-nez v4, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "List view item must not be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast p2, Lse/emilsjolander/stickylistheaders/WrapperView;

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    iget-object v1, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    if-nez v1, :cond_6

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :cond_5
    :goto_4
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v1, p1, v0, p2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Header view must not be null."

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    goto :goto_4

    :cond_7
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    new-instance v1, Lse/emilsjolander/stickylistheaders/AdapterWrapper$2;

    invoke-direct {v1, p0, p1}, Lse/emilsjolander/stickylistheaders/AdapterWrapper$2;-><init>(Lse/emilsjolander/stickylistheaders/AdapterWrapper;I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v1, v0

    goto :goto_2

    :cond_8
    instance-of v0, v4, Landroid/widget/Checkable;

    if-nez v0, :cond_1

    instance-of v0, p2, Lse/emilsjolander/stickylistheaders/CheckableWrapperView;

    if-eqz v0, :cond_1

    new-instance p2, Lse/emilsjolander/stickylistheaders/WrapperView;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->d:Landroid/content/Context;

    invoke-direct {p2, v0}, Lse/emilsjolander/stickylistheaders/WrapperView;-><init>(Landroid/content/Context;)V

    goto :goto_3

    :cond_9
    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->a:Landroid/view/View;

    if-eq v0, v4, :cond_b

    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->a:Landroid/view/View;

    invoke-virtual {p2, v0}, Lse/emilsjolander/stickylistheaders/WrapperView;->removeView(Landroid/view/View;)V

    iput-object v4, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_a

    if-eq v0, p2, :cond_a

    instance-of v5, v0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_a

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_a
    invoke-virtual {p2, v4}, Lse/emilsjolander/stickylistheaders/WrapperView;->addView(Landroid/view/View;)V

    :cond_b
    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    if-eq v0, v1, :cond_d

    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    if-eqz v0, :cond_c

    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    invoke-virtual {p2, v0}, Lse/emilsjolander/stickylistheaders/WrapperView;->removeView(Landroid/view/View;)V

    :cond_c
    iput-object v1, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    if-eqz v1, :cond_d

    invoke-virtual {p2, v1}, Lse/emilsjolander/stickylistheaders/WrapperView;->addView(Landroid/view/View;)V

    :cond_d
    iget-object v0, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->b:Landroid/graphics/drawable/Drawable;

    if-eq v0, v2, :cond_e

    iput-object v2, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->b:Landroid/graphics/drawable/Drawable;

    iput v3, p2, Lse/emilsjolander/stickylistheaders/WrapperView;->c:I

    invoke-virtual {p2}, Lse/emilsjolander/stickylistheaders/WrapperView;->invalidate()V

    :cond_e
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 203
    return-void
.end method

.method public notifyDataSetInvalidated()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetInvalidated()V

    .line 208
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
