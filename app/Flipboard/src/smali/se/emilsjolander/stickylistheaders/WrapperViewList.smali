.class public Lse/emilsjolander/stickylistheaders/WrapperViewList;
.super Landroid/widget/ListView;
.source "WrapperViewList.java"


# instance fields
.field a:Lse/emilsjolander/stickylistheaders/WrapperViewList$LifeCycleListener;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field private d:Landroid/graphics/Rect;

.field private e:Ljava/lang/reflect/Field;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 29
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->d:Landroid/graphics/Rect;

    .line 26
    iput-boolean v1, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->f:Z

    .line 34
    :try_start_0
    const-class v0, Landroid/widget/AbsListView;

    const-string v1, "mSelectorRect"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 35
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 36
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->d:Landroid/graphics/Rect;

    .line 38
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 39
    const-class v0, Landroid/widget/AbsListView;

    const-string v1, "mSelectorPosition"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->e:Ljava/lang/reflect/Field;

    .line 40
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->e:Ljava/lang/reflect/Field;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 42
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    .line 44
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 46
    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->b:Ljava/util/List;

    if-nez v0, :cond_0

    .line 127
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->b:Ljava/util/List;

    .line 129
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method

.method private getSelectorPosition()I
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->e:Ljava/lang/reflect/Field;

    if-nez v0, :cond_1

    .line 76
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 77
    invoke-virtual {p0, v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-ne v1, v2, :cond_0

    .line 78
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :goto_1
    return v0

    .line 76
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    :try_start_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->e:Ljava/lang/reflect/Field;

    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_1

    .line 84
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 90
    :cond_2
    :goto_2
    const/4 v0, -0x1

    goto :goto_1

    .line 86
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2
.end method


# virtual methods
.method public addFooterView(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 116
    invoke-direct {p0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->a(Landroid/view/View;)V

    .line 117
    return-void
.end method

.method public addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 121
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 122
    invoke-direct {p0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->a(Landroid/view/View;)V

    .line 123
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getSelectorPosition()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lse/emilsjolander/stickylistheaders/WrapperView;

    if-eqz v1, :cond_0

    check-cast v0, Lse/emilsjolander/stickylistheaders/WrapperView;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperView;->getTop()I

    move-result v2

    iget v0, v0, Lse/emilsjolander/stickylistheaders/WrapperView;->e:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 96
    :cond_0
    iget v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->c:I

    if-eqz v0, :cond_1

    .line 97
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 98
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 99
    iget v1, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->c:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 100
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 101
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 102
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 106
    :goto_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->a:Lse/emilsjolander/stickylistheaders/WrapperViewList$LifeCycleListener;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList$LifeCycleListener;->a(Landroid/graphics/Canvas;)V

    .line 107
    return-void

    .line 104
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/ListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method final getFixedFirstVisibleItem()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFirstVisiblePosition()I

    move-result v2

    .line 154
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    .line 176
    :goto_0
    return v2

    :cond_0
    move v0, v1

    .line 160
    :goto_1
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 161
    invoke-virtual {p0, v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ltz v3, :cond_2

    .line 162
    add-int/2addr v0, v2

    .line 170
    :goto_2
    iget-boolean v2, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->f:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getPaddingTop()I

    move-result v2

    if-lez v2, :cond_1

    if-lez v0, :cond_1

    .line 171
    invoke-virtual {p0, v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-lez v1, :cond_1

    .line 172
    add-int/lit8 v0, v0, -0x1

    :cond_1
    move v2, v0

    .line 176
    goto :goto_0

    .line 160
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 1

    .prologue
    .line 53
    instance-of v0, p1, Lse/emilsjolander/stickylistheaders/WrapperView;

    if-eqz v0, :cond_0

    .line 54
    check-cast p1, Lse/emilsjolander/stickylistheaders/WrapperView;

    iget-object p1, p1, Lse/emilsjolander/stickylistheaders/WrapperView;->a:Landroid/view/View;

    .line 56
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method public removeFooterView(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 134
    invoke-super {p0, p1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 136
    const/4 v0, 0x1

    .line 138
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setClipToPadding(Z)V
    .locals 0

    .prologue
    .line 181
    iput-boolean p1, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->f:Z

    .line 182
    invoke-super {p0, p1}, Landroid/widget/ListView;->setClipToPadding(Z)V

    .line 183
    return-void
.end method

.method final setLifeCycleListener(Lse/emilsjolander/stickylistheaders/WrapperViewList$LifeCycleListener;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->a:Lse/emilsjolander/stickylistheaders/WrapperViewList$LifeCycleListener;

    .line 111
    return-void
.end method

.method final setTopClippingLength(I)V
    .locals 0

    .prologue
    .line 149
    iput p1, p0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->c:I

    .line 150
    return-void
.end method
