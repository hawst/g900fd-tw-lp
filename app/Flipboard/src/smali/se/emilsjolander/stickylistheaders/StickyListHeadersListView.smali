.class public Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;
.super Landroid/widget/FrameLayout;
.source "StickyListHeadersListView.java"


# instance fields
.field private a:Landroid/view/View;

.field private b:Ljava/lang/Long;

.field public c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Landroid/widget/AbsListView$OnScrollListener;

.field private g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

.field private q:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

.field private r:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

.field private s:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

.field private t:Landroid/graphics/drawable/Drawable;

.field private u:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v8, 0xb

    const/16 v7, 0x9

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 118
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 91
    iput-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->h:Z

    .line 92
    iput-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    .line 93
    iput-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->j:Z

    .line 94
    iput v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->k:I

    .line 95
    iput v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    .line 96
    iput v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    .line 97
    iput v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->n:I

    .line 98
    iput v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->o:I

    .line 121
    new-instance v2, Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-direct {v2, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    .line 124
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getDivider()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    .line 125
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getDividerHeight()I

    move-result v2

    iput v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    .line 126
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 127
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v2, v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setDividerHeight(I)V

    .line 129
    if-eqz p2, :cond_5

    .line 130
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, Lse/emilsjolander/stickylistheaders/R$styleable;->StickyListHeadersListView:[I

    invoke-virtual {v2, p2, v3, v1, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 134
    const/4 v2, 0x1

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 135
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    .line 136
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    .line 137
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v4

    iput v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->n:I

    .line 138
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->o:I

    .line 140
    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    iget v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    iget v5, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->n:I

    iget v6, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->o:I

    invoke-virtual {p0, v2, v4, v5, v6}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setPadding(IIII)V

    .line 144
    const/16 v2, 0x8

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    .line 145
    const/4 v2, 0x1

    invoke-super {p0, v2}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    .line 146
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    iget-boolean v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    invoke-virtual {v2, v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setClipToPadding(Z)V

    .line 149
    const/4 v2, 0x6

    const/16 v4, 0x200

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 150
    iget-object v5, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    and-int/lit16 v2, v4, 0x200

    if-eqz v2, :cond_6

    move v2, v0

    :goto_0
    invoke-virtual {v5, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setVerticalScrollBarEnabled(Z)V

    .line 151
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    and-int/lit16 v4, v4, 0x100

    if-eqz v4, :cond_7

    :goto_1
    invoke-virtual {v2, v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setHorizontalScrollBarEnabled(Z)V

    .line 154
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_0

    .line 155
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0x12

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOverScrollMode(I)V

    .line 159
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x7

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getVerticalFadingEdgeLength()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setFadingEdgeLength(I)V

    .line 161
    const/16 v0, 0x14

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 162
    const/16 v2, 0x1000

    if-ne v0, v2, :cond_8

    .line 163
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setVerticalFadingEdgeEnabled(Z)V

    .line 164
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setHorizontalFadingEdgeEnabled(Z)V

    .line 172
    :goto_2
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0xd

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getCacheColorHint()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setCacheColorHint(I)V

    .line 174
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_1

    .line 175
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0x10

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChoiceMode()I

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setChoiceMode(I)V

    .line 178
    :cond_1
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0xa

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setDrawSelectorOnTop(Z)V

    .line 179
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0x11

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->isFastScrollEnabled()Z

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setFastScrollEnabled(Z)V

    .line 181
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_2

    .line 182
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0x13

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->isFastScrollAlwaysVisible()Z

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setFastScrollAlwaysVisible(Z)V

    .line 187
    :cond_2
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setScrollBarStyle(I)V

    .line 189
    const/16 v0, 0x9

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0x9

    invoke-virtual {v3, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 193
    :cond_3
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0xb

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->isScrollingCacheEnabled()Z

    move-result v4

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setScrollingCacheEnabled(Z)V

    .line 196
    const/16 v0, 0xe

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 197
    const/16 v0, 0xe

    invoke-virtual {v3, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    .line 200
    :cond_4
    const/16 v0, 0xf

    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    .line 203
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/16 v2, 0xc

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setTranscriptMode(I)V

    .line 207
    const/16 v0, 0x15

    const/4 v2, 0x1

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->h:Z

    .line 208
    const/16 v0, 0x16

    const/4 v2, 0x1

    invoke-virtual {v3, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->j:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 217
    :cond_5
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    new-instance v2, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;

    invoke-direct {v2, p0, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$WrapperViewListLifeCycleListener;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;B)V

    iput-object v2, v0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->a:Lse/emilsjolander/stickylistheaders/WrapperViewList$LifeCycleListener;

    .line 218
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    new-instance v2, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;

    invoke-direct {v2, p0, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$WrapperListScrollListener;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;B)V

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 220
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->addView(Landroid/view/View;)V

    .line 221
    return-void

    :cond_6
    move v2, v1

    .line 150
    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 151
    goto/16 :goto_1

    .line 165
    :cond_8
    const/16 v2, 0x2000

    if-ne v0, v2, :cond_9

    .line 166
    :try_start_1
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setVerticalFadingEdgeEnabled(Z)V

    .line 167
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setHorizontalFadingEdgeEnabled(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_2

    .line 212
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 169
    :cond_9
    :try_start_2
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setVerticalFadingEdgeEnabled(Z)V

    .line 170
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setHorizontalFadingEdgeEnabled(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_2
.end method

.method static synthetic a(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->removeView(Landroid/view/View;)V

    .line 280
    iput-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    .line 281
    iput-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b:Ljava/lang/Long;

    .line 282
    iput-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->d:Ljava/lang/Integer;

    .line 283
    iput-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    .line 286
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v1, 0x0

    iput v1, v0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->c:I

    .line 287
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b()V

    .line 289
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 292
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-nez v0, :cond_1

    move v1, v3

    .line 293
    :goto_0
    if-eqz v1, :cond_0

    iget-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->h:Z

    if-nez v0, :cond_2

    .line 321
    :cond_0
    :goto_1
    return-void

    .line 292
    :cond_1
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->getCount()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 297
    :cond_2
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getHeaderViewsCount()I

    move-result v0

    .line 298
    sub-int v0, p1, v0

    .line 299
    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v4

    if-lez v4, :cond_3

    .line 300
    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4, v3}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 301
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 302
    add-int/lit8 v0, v0, 0x1

    .line 309
    :cond_3
    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v4

    if-eqz v4, :cond_6

    move v5, v2

    .line 310
    :goto_2
    if-eqz v5, :cond_7

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFirstVisiblePosition()I

    move-result v4

    if-nez v4, :cond_7

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v4, v3}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c()I

    move-result v6

    if-lt v4, v6, :cond_7

    move v4, v2

    .line 313
    :goto_3
    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_4

    if-gez v0, :cond_8

    :cond_4
    move v1, v2

    .line 315
    :goto_4
    if-eqz v5, :cond_5

    if-nez v1, :cond_5

    if-eqz v4, :cond_9

    .line 316
    :cond_5
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a()V

    goto :goto_1

    :cond_6
    move v5, v3

    .line 309
    goto :goto_2

    :cond_7
    move v4, v3

    .line 310
    goto :goto_3

    :cond_8
    move v1, v3

    .line 313
    goto :goto_4

    .line 320
    :cond_9
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-eq v1, v0, :cond_11

    :cond_a
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->d:Ljava/lang/Integer;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    invoke-virtual {v1, v0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b(I)J

    move-result-wide v0

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b:Ljava/lang/Long;

    if-eqz v4, :cond_b

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-eqz v4, :cond_11

    :cond_b
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b:Ljava/lang/Long;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v0, v1, v4, p0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    if-eq v1, v0, :cond_f

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "header may not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {p0, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->removeView(Landroid/view/View;)V

    :cond_d
    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->p:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    new-instance v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$1;

    invoke-direct {v1, p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$1;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_e
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    :cond_f
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(Landroid/view/View;)V

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c(Landroid/view/View;)V

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->r:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->r:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    :cond_10
    const/4 v0, 0x0

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    :cond_11
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c()I

    move-result v1

    add-int v6, v0, v1

    move v5, v3

    :goto_5
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v0

    if-ge v5, v0, :cond_13

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, v5}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lse/emilsjolander/stickylistheaders/WrapperView;

    if-eqz v0, :cond_15

    move-object v0, v1

    check-cast v0, Lse/emilsjolander/stickylistheaders/WrapperView;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperView;->a()Z

    move-result v0

    if-eqz v0, :cond_15

    move v0, v2

    :goto_6
    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    iget-object v7, v4, Lse/emilsjolander/stickylistheaders/WrapperViewList;->b:Ljava/util/List;

    if-nez v7, :cond_16

    move v4, v3

    :goto_7
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c()I

    move-result v8

    if-lt v7, v8, :cond_17

    if-nez v0, :cond_12

    if-eqz v4, :cond_17

    :cond_12
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, v6

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    :cond_13
    invoke-direct {p0, v3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setHeaderOffet(I)V

    iget-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->j:Z

    if-nez v0, :cond_14

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->c:I

    :cond_14
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b()V

    goto/16 :goto_1

    :cond_15
    move v0, v3

    goto :goto_6

    :cond_16
    iget-object v4, v4, Lse/emilsjolander/stickylistheaders/WrapperViewList;->b:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    goto :goto_7

    :cond_17
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_5
.end method

.method static synthetic a(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;I)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a(I)V

    return-void
.end method

.method static synthetic a(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;Landroid/graphics/Canvas;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 37
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v1, 0x0

    .line 399
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 400
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->k:I

    add-int/2addr v0, v2

    move v2, v0

    .line 404
    :goto_1
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v4

    move v3, v1

    .line 405
    :goto_2
    if-ge v3, v4, :cond_4

    .line 408
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, v3}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 409
    instance-of v5, v0, Lse/emilsjolander/stickylistheaders/WrapperView;

    if-eqz v5, :cond_0

    .line 410
    check-cast v0, Lse/emilsjolander/stickylistheaders/WrapperView;

    .line 415
    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperView;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 416
    iget-object v5, v0, Lse/emilsjolander/stickylistheaders/WrapperView;->d:Landroid/view/View;

    .line 421
    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperView;->getTop()I

    move-result v0

    if-ge v0, v2, :cond_3

    .line 422
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v6, :cond_0

    .line 423
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 405
    :cond_0
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_1
    move v0, v1

    .line 400
    goto :goto_0

    .line 402
    :cond_2
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c()I

    move-result v0

    move v2, v0

    goto :goto_1

    .line 426
    :cond_3
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 427
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 431
    :cond_4
    return-void
.end method

.method private static b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 230
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 231
    if-nez v0, :cond_1

    .line 232
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 233
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 239
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v1, v3, :cond_2

    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v1, v2, :cond_0

    .line 235
    :cond_2
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 236
    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 237
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private static b(I)Z
    .locals 3

    .prologue
    .line 644
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, p0, :cond_0

    .line 645
    const-string v0, "StickyListHeaders"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Api lvl must be at least "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to call this method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 646
    const/4 v0, 0x0

    .line 648
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;Landroid/graphics/Canvas;Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 37
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0
.end method

.method private c()I
    .locals 2

    .prologue
    .line 542
    iget v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->k:I

    iget-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b:Ljava/lang/Long;

    return-object v0
.end method

.method private c(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 242
    if-eqz p1, :cond_0

    .line 243
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    sub-int/2addr v0, v1

    iget v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->n:I

    sub-int/2addr v0, v1

    .line 244
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 246
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 248
    invoke-virtual {p0, p1, v0, v1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->measureChild(Landroid/view/View;II)V

    .line 251
    :cond_0
    return-void
.end method

.method static synthetic d(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->p:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    return-object v0
.end method

.method static synthetic e(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a()V

    return-void
.end method

.method static synthetic f(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Landroid/widget/AbsListView$OnScrollListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    return-object v0
.end method

.method static synthetic g(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Lse/emilsjolander/stickylistheaders/WrapperViewList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    return-object v0
.end method

.method static synthetic h(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    return v0
.end method

.method static synthetic i(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    return v0
.end method

.method private setHeaderOffet(I)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 437
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 438
    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    .line 439
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 440
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 446
    :goto_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->q:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

    if-eqz v0, :cond_1

    .line 447
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->q:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 450
    :cond_1
    return-void

    .line 442
    :cond_2
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 443
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 444
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->addFooterView(Landroid/view/View;)V

    .line 757
    return-void
.end method

.method public canScrollVertically(I)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 1069
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->canScrollVertically(I)Z

    move-result v0

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 270
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 271
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    .line 273
    :cond_1
    return-void
.end method

.method public getAdapter()Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v0, v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    goto :goto_0
.end method

.method public getAreHeadersSticky()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 567
    iget-boolean v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->h:Z

    return v0
.end method

.method public getCheckedItemCount()I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 910
    const/16 v0, 0xb

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 911
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getCheckedItemCount()I

    move-result v0

    .line 913
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCheckedItemIds()[J
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x8
    .end annotation

    .prologue
    .line 918
    const/16 v0, 0x8

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 919
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getCheckedItemIds()[J

    move-result-object v0

    .line 921
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCheckedItemPosition()I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 926
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getCheckedItemPosition()I

    move-result v0

    return v0
.end method

.method public getCheckedItemPositions()Landroid/util/SparseBooleanArray;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 931
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 935
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getCount()I

    move-result v0

    return v0
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 702
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerHeight()I
    .locals 1

    .prologue
    .line 706
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getEmptyView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFirstVisiblePosition()I

    move-result v0

    return v0
.end method

.method public getFooterViewsCount()I
    .locals 1

    .prologue
    .line 764
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFooterViewsCount()I

    move-result v0

    return v0
.end method

.method public getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getHeaderViewsCount()I

    move-result v0

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getLastVisiblePosition()I

    move-result v0

    return v0
.end method

.method public getListChildCount()I
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getOverScrollMode()I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 798
    const/16 v0, 0x9

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 799
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getOverScrollMode()I

    move-result v0

    .line 801
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPaddingBottom()I
    .locals 1

    .prologue
    .line 1006
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->o:I

    return v0
.end method

.method public getPaddingLeft()I
    .locals 1

    .prologue
    .line 991
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    return v0
.end method

.method public getPaddingRight()I
    .locals 1

    .prologue
    .line 1001
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->n:I

    return v0
.end method

.method public getPaddingTop()I
    .locals 1

    .prologue
    .line 996
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    return v0
.end method

.method public getScrollBarStyle()I
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getScrollBarStyle()I

    move-result v0

    return v0
.end method

.method public getStickyHeaderTopOffset()I
    .locals 1

    .prologue
    .line 581
    iget v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->k:I

    return v0
.end method

.method public getWrappedList()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 640
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    return-object v0
.end method

.method public isHorizontalScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 782
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->isHorizontalScrollBarEnabled()Z

    move-result v0

    return v0
.end method

.method public isVerticalScrollBarEnabled()Z
    .locals 1

    .prologue
    .line 777
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->isVerticalScrollBarEnabled()Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 255
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->layout(IIII)V

    .line 256
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 258
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 259
    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    iget-object v3, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    iget v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 262
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 225
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 226
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c(Landroid/view/View;)V

    .line 227
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1062
    sget-object v0, Landroid/view/View$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1063
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1064
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1053
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1054
    sget-object v1, Landroid/view/View$BaseSavedState;->EMPTY_STATE:Landroid/view/AbsSavedState;

    if-eq v0, v1, :cond_0

    .line 1055
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Handling non empty state of parent class is not implemented"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1057
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setAdapter(Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 654
    if-nez p1, :cond_0

    .line 655
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, v3}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 656
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a()V

    .line 681
    :goto_0
    return-void

    .line 659
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-eqz v0, :cond_1

    .line 660
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->s:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 663
    :cond_1
    instance-of v0, p1, Landroid/widget/SectionIndexer;

    if-eqz v0, :cond_2

    .line 664
    new-instance v0, Lse/emilsjolander/stickylistheaders/SectionIndexerAdapterWrapper;

    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lse/emilsjolander/stickylistheaders/SectionIndexerAdapterWrapper;-><init>(Landroid/content/Context;Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    .line 668
    :goto_1
    new-instance v0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    invoke-direct {v0, p0, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;B)V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->s:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    .line 669
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->s:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperDataSetObserver;

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 671
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->p:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    if-eqz v0, :cond_3

    .line 672
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    new-instance v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;

    invoke-direct {v1, p0, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;B)V

    iput-object v1, v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b:Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    .line 677
    :goto_2
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    invoke-virtual {v0, v1, v2}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 679
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 680
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a()V

    goto :goto_0

    .line 666
    :cond_2
    new-instance v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;-><init>(Landroid/content/Context;Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    goto :goto_1

    .line 674
    :cond_3
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iput-object v3, v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b:Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    goto :goto_2
.end method

.method public setAreHeadersSticky(Z)V
    .locals 1

    .prologue
    .line 548
    iput-boolean p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->h:Z

    .line 549
    if-nez p1, :cond_0

    .line 550
    invoke-direct {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a()V

    .line 555
    :goto_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->invalidate()V

    .line 556
    return-void

    .line 552
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result v0

    invoke-direct {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a(I)V

    goto :goto_0
.end method

.method public setChoiceMode(I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 900
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setChoiceMode(I)V

    .line 901
    return-void
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    if-eqz v0, :cond_0

    .line 963
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setClipToPadding(Z)V

    .line 965
    :cond_0
    iput-boolean p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    .line 966
    return-void
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 688
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    .line 689
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    invoke-virtual {v0, v1, v2}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 692
    :cond_0
    return-void
.end method

.method public setDividerHeight(I)V
    .locals 3

    .prologue
    .line 695
    iput p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    .line 696
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->t:Landroid/graphics/drawable/Drawable;

    iget v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->u:I

    invoke-virtual {v0, v1, v2}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 699
    :cond_0
    return-void
.end method

.method public setDrawingListUnderStickyHeader(Z)V
    .locals 2

    .prologue
    .line 586
    iput-boolean p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->j:Z

    .line 588
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v1, 0x0

    iput v1, v0, Lse/emilsjolander/stickylistheaders/WrapperViewList;->c:I

    .line 589
    return-void
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setEmptyView(Landroid/view/View;)V

    .line 769
    return-void
.end method

.method public setFastScrollAlwaysVisible(Z)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1015
    const/16 v0, 0xb

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1016
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setFastScrollAlwaysVisible(Z)V

    .line 1018
    :cond_0
    return-void
.end method

.method public setFastScrollEnabled(Z)V
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setFastScrollEnabled(Z)V

    .line 1011
    return-void
.end method

.method public setHorizontalScrollBarEnabled(Z)V
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setHorizontalScrollBarEnabled(Z)V

    .line 793
    return-void
.end method

.method public setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1046
    const/16 v0, 0xb

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    .line 1049
    :cond_0
    return-void
.end method

.method public setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V
    .locals 1

    .prologue
    .line 948
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 949
    return-void
.end method

.method public setOnHeaderClickListener(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;)V
    .locals 3

    .prologue
    .line 596
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->p:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    .line 597
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-eqz v0, :cond_0

    .line 598
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->p:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnHeaderClickListener;

    if-eqz v0, :cond_1

    .line 599
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    new-instance v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$AdapterWrapperHeaderClickHandler;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;B)V

    iput-object v1, v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b:Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    .line 601
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 602
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a:Landroid/view/View;

    new-instance v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$2;

    invoke-direct {v1, p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$2;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 615
    :cond_0
    :goto_0
    return-void

    .line 612
    :cond_1
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    const/4 v1, 0x0

    iput-object v1, v0, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b:Lse/emilsjolander/stickylistheaders/AdapterWrapper$OnHeaderClickListener;

    goto :goto_0
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 728
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 729
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1

    .prologue
    .line 732
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 733
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 710
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    .line 711
    return-void
.end method

.method public setOnStickyHeaderChangedListener(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->r:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderChangedListener;

    .line 623
    return-void
.end method

.method public setOnStickyHeaderOffsetChangedListener(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;)V
    .locals 0

    .prologue
    .line 618
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->q:Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$OnStickyHeaderOffsetChangedListener;

    .line 619
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 715
    if-eqz p1, :cond_0

    .line 716
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    new-instance v1, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$3;

    invoke-direct {v1, p0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView$3;-><init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 725
    :goto_0
    return-void

    .line 723
    :cond_0
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto :goto_0
.end method

.method public setOverScrollMode(I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 807
    const/16 v0, 0x9

    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 808
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    if-eqz v0, :cond_0

    .line 809
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setOverScrollMode(I)V

    .line 812
    :cond_0
    return-void
.end method

.method public setPadding(IIII)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 970
    iput p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->l:I

    .line 971
    iput p2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    .line 972
    iput p3, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->n:I

    .line 973
    iput p4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->o:I

    .line 975
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    if-eqz v0, :cond_0

    .line 976
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, p2, p3, p4}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setPadding(IIII)V

    .line 978
    :cond_0
    invoke-super {p0, v1, v1, v1, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 979
    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->requestLayout()V

    .line 980
    return-void
.end method

.method public setScrollBarStyle(I)V
    .locals 1

    .prologue
    .line 1033
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setScrollBarStyle(I)V

    .line 1034
    return-void
.end method

.method public setSelection(I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 869
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p1, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    invoke-virtual {v2, v0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b(I)J

    move-result-wide v2

    iget-object v4, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->b(I)J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->g:Lse/emilsjolander/stickylistheaders/AdapterWrapper;

    const/4 v2, 0x0

    iget-object v3, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1, v2, v3}, Lse/emilsjolander/stickylistheaders/AdapterWrapper;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "header may not be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->b(Landroid/view/View;)V

    invoke-direct {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c(Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_1
    add-int/lit8 v0, v0, 0x0

    iget-boolean v2, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->i:Z

    if-eqz v2, :cond_4

    :goto_2
    sub-int/2addr v0, v1

    iget-object v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v1, p1, v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setSelectionFromTop(II)V

    .line 870
    return-void

    :cond_3
    move v0, v1

    .line 869
    goto :goto_1

    :cond_4
    iget v1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->m:I

    goto :goto_2
.end method

.method public setSelector(I)V
    .locals 1

    .prologue
    .line 887
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setSelector(I)V

    .line 888
    return-void
.end method

.method public setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 884
    return-void
.end method

.method public setStickyHeaderTopOffset(I)V
    .locals 1

    .prologue
    .line 576
    iput p1, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->k:I

    .line 577
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->getFixedFirstVisibleItem()I

    move-result v0

    invoke-direct {p0, v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->a(I)V

    .line 578
    return-void
.end method

.method public setTranscriptMode(I)V
    .locals 1

    .prologue
    .line 1073
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setTranscriptMode(I)V

    .line 1074
    return-void
.end method

.method public setVerticalScrollBarEnabled(Z)V
    .locals 1

    .prologue
    .line 787
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0, p1}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->setVerticalScrollBarEnabled(Z)V

    .line 788
    return-void
.end method

.method public showContextMenu()Z
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->c:Lse/emilsjolander/stickylistheaders/WrapperViewList;

    invoke-virtual {v0}, Lse/emilsjolander/stickylistheaders/WrapperViewList;->showContextMenu()Z

    move-result v0

    return v0
.end method
