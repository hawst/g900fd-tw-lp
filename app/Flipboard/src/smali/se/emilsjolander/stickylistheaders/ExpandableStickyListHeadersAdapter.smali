.class Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;
.super Landroid/widget/BaseAdapter;
.source "ExpandableStickyListHeadersAdapter.java"

# interfaces
.implements Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;


# instance fields
.field a:Lse/emilsjolander/stickylistheaders/DualHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lse/emilsjolander/stickylistheaders/DualHashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field b:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;


# direct methods
.method constructor <init>(Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 18
    new-instance v0, Lse/emilsjolander/stickylistheaders/DualHashMap;

    invoke-direct {v0}, Lse/emilsjolander/stickylistheaders/DualHashMap;-><init>()V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->a:Lse/emilsjolander/stickylistheaders/DualHashMap;

    .line 19
    new-instance v0, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;

    invoke-direct {v0}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;-><init>()V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->b:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->c:Ljava/util/List;

    .line 23
    iput-object p1, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    .line 24
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1, p2, p3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->b(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getItemViewType(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 78
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1, p2, p3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 79
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->a:Lse/emilsjolander/stickylistheaders/DualHashMap;

    invoke-virtual {p0, p1}, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->getItemId(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/DualHashMap;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lse/emilsjolander/stickylistheaders/DualHashMap;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Lse/emilsjolander/stickylistheaders/DualHashMap;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v3, v0, Lse/emilsjolander/stickylistheaders/DualHashMap;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/DualHashMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, v0, Lse/emilsjolander/stickylistheaders/DualHashMap;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Lse/emilsjolander/stickylistheaders/DualHashMap;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v3, v0, Lse/emilsjolander/stickylistheaders/DualHashMap;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v0, Lse/emilsjolander/stickylistheaders/DualHashMap;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lse/emilsjolander/stickylistheaders/DualHashMap;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v2, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->b:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;

    invoke-virtual {p0, p1}, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->b(I)J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v0, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;

    invoke-interface {v0, v3}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iget-object v4, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    iget-object v4, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->b:Ljava/util/LinkedHashMap;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4, v0, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v0, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->c:Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;

    invoke-interface {v4, v1}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v4, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->b:Ljava/util/LinkedHashMap;

    iget-object v5, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;

    invoke-interface {v5, v0}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_3
    iget-object v0, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->c:Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;

    invoke-interface {v4, v1}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->b:Ljava/util/LinkedHashMap;

    iget-object v4, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;

    invoke-interface {v4, v3}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {v2, v0, v1}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a(Ljava/util/List;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->b:Ljava/util/LinkedHashMap;

    iget-object v2, v2, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap;->a:Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;

    invoke-interface {v2, v3}, Lse/emilsjolander/stickylistheaders/DistinctMultiHashMap$IDMapper;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_4
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->c:Ljava/util/List;

    invoke-virtual {p0, p1}, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->b(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 82
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 86
    :goto_0
    return-object v1

    .line 84
    :cond_5
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->getViewTypeCount()I

    move-result v0

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->isEnabled(I)Z

    move-result v0

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 49
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lse/emilsjolander/stickylistheaders/ExpandableStickyListHeadersAdapter;->d:Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;

    invoke-interface {v0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 54
    return-void
.end method
