.class Lbolts/Task$10$1;
.super Ljava/lang/Object;
.source "Task.java"

# interfaces
.implements Lbolts/Continuation;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbolts/Continuation",
        "<TTContinuationResult;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lbolts/Task$10;


# direct methods
.method constructor <init>(Lbolts/Task$10;)V
    .locals 0

    .prologue
    .line 484
    iput-object p1, p0, Lbolts/Task$10$1;->a:Lbolts/Task$10;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic then(Lbolts/Task;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 484
    invoke-virtual {p1}, Lbolts/Task;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbolts/Task$10$1;->a:Lbolts/Task$10;

    iget-object v0, v0, Lbolts/Task$10;->c:Lbolts/Task$TaskCompletionSource;

    invoke-virtual {v0}, Lbolts/Task$TaskCompletionSource;->a()V

    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lbolts/Task;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbolts/Task$10$1;->a:Lbolts/Task$10;

    iget-object v0, v0, Lbolts/Task$10;->c:Lbolts/Task$TaskCompletionSource;

    invoke-virtual {p1}, Lbolts/Task;->e()Ljava/lang/Exception;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbolts/Task$TaskCompletionSource;->a(Ljava/lang/Exception;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lbolts/Task$10$1;->a:Lbolts/Task$10;

    iget-object v0, v0, Lbolts/Task$10;->c:Lbolts/Task$TaskCompletionSource;

    invoke-virtual {p1}, Lbolts/Task;->d()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbolts/Task$TaskCompletionSource;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method
