.class public Lbolts/Task$TaskCompletionSource;
.super Ljava/lang/Object;
.source "Task.java"


# instance fields
.field public final synthetic a:Lbolts/Task;


# direct methods
.method private constructor <init>(Lbolts/Task;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528
    return-void
.end method

.method synthetic constructor <init>(Lbolts/Task;B)V
    .locals 0

    .prologue
    .line 526
    invoke-direct {p0, p1}, Lbolts/Task$TaskCompletionSource;-><init>(Lbolts/Task;)V

    return-void
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 541
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->a(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 542
    :try_start_0
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->b(Lbolts/Task;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    const/4 v0, 0x0

    monitor-exit v1

    .line 549
    :goto_0
    return v0

    .line 545
    :cond_0
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->c(Lbolts/Task;)Z

    .line 546
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->d(Lbolts/Task;)Z

    .line 547
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->a(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 548
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->e(Lbolts/Task;)V

    .line 549
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 550
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Ljava/lang/Exception;)Z
    .locals 2

    .prologue
    .line 573
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->a(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 574
    :try_start_0
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->b(Lbolts/Task;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    const/4 v0, 0x0

    monitor-exit v1

    .line 581
    :goto_0
    return v0

    .line 577
    :cond_0
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->c(Lbolts/Task;)Z

    .line 578
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0, p1}, Lbolts/Task;->a(Lbolts/Task;Ljava/lang/Exception;)Ljava/lang/Exception;

    .line 579
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->a(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 580
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->e(Lbolts/Task;)V

    .line 581
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 582
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResult;)Z"
        }
    .end annotation

    .prologue
    .line 557
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->a(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 558
    :try_start_0
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->b(Lbolts/Task;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 559
    const/4 v0, 0x0

    monitor-exit v1

    .line 565
    :goto_0
    return v0

    .line 561
    :cond_0
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->c(Lbolts/Task;)Z

    .line 562
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0, p1}, Lbolts/Task;->a(Lbolts/Task;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->a(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 564
    iget-object v0, p0, Lbolts/Task$TaskCompletionSource;->a:Lbolts/Task;

    invoke-static {v0}, Lbolts/Task;->e(Lbolts/Task;)V

    .line 565
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 566
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 589
    invoke-direct {p0}, Lbolts/Task$TaskCompletionSource;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 590
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot cancel a completed task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 592
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 607
    invoke-direct {p0, p1}, Lbolts/Task$TaskCompletionSource;->b(Ljava/lang/Exception;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 608
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set the error on a completed task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 610
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TTResult;)V"
        }
    .end annotation

    .prologue
    .line 598
    invoke-direct {p0, p1}, Lbolts/Task$TaskCompletionSource;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 599
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set the result of a completed task."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 601
    :cond_0
    return-void
.end method
