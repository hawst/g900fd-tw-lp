.class final Lbolts/Task$10;
.super Ljava/lang/Object;
.source "Task.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbolts/Continuation;

.field final synthetic b:Lbolts/Task;

.field final synthetic c:Lbolts/Task$TaskCompletionSource;


# direct methods
.method constructor <init>(Lbolts/Continuation;Lbolts/Task;Lbolts/Task$TaskCompletionSource;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lbolts/Task$10;->a:Lbolts/Continuation;

    iput-object p2, p0, Lbolts/Task$10;->b:Lbolts/Task;

    iput-object p3, p0, Lbolts/Task$10;->c:Lbolts/Task$TaskCompletionSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 480
    :try_start_0
    iget-object v0, p0, Lbolts/Task$10;->a:Lbolts/Continuation;

    iget-object v1, p0, Lbolts/Task$10;->b:Lbolts/Task;

    invoke-interface {v0, v1}, Lbolts/Continuation;->then(Lbolts/Task;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbolts/Task;

    .line 481
    if-nez v0, :cond_0

    .line 482
    iget-object v0, p0, Lbolts/Task$10;->c:Lbolts/Task$TaskCompletionSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbolts/Task$TaskCompletionSource;->a(Ljava/lang/Object;)V

    .line 501
    :goto_0
    return-void

    .line 484
    :cond_0
    new-instance v1, Lbolts/Task$10$1;

    invoke-direct {v1, p0}, Lbolts/Task$10$1;-><init>(Lbolts/Task$10;)V

    invoke-virtual {v0, v1}, Lbolts/Task;->a(Lbolts/Continuation;)Lbolts/Task;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 498
    :catch_0
    move-exception v0

    .line 499
    iget-object v1, p0, Lbolts/Task$10;->c:Lbolts/Task$TaskCompletionSource;

    invoke-virtual {v1, v0}, Lbolts/Task$TaskCompletionSource;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method
