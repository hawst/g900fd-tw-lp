.class Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Lorg/askerov/dynamicgrid/DynamicGridView$SwitchCellAnimator;


# instance fields
.field final synthetic a:Lorg/askerov/dynamicgrid/DynamicGridView;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Lorg/askerov/dynamicgrid/DynamicGridView;II)V
    .locals 0

    .prologue
    .line 592
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 593
    iput p2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->c:I

    .line 594
    iput p3, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->b:I

    .line 595
    return-void
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;)I
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->b:I

    return v0
.end method

.method static synthetic b(Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;)I
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->c:I

    return v0
.end method


# virtual methods
.method public final a(II)V
    .locals 4

    .prologue
    .line 599
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->m(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2, p1, p2}, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;Landroid/view/View;II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 600
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/view/View;)Landroid/view/View;

    .line 601
    return-void
.end method
