.class Lorg/askerov/dynamicgrid/DynamicGridView$7;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field final synthetic a:Lorg/askerov/dynamicgrid/DynamicGridView;

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method constructor <init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 808
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 810
    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    .line 811
    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->c:I

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, -0x1

    .line 818
    iput p2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->d:I

    .line 819
    iput p3, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->e:I

    .line 821
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->d:I

    :goto_0
    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    .line 823
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->c:I

    if-ne v0, v1, :cond_3

    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->e:I

    :goto_1
    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->c:I

    .line 826
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->d:I

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->n(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;J)V

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->s(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    .line 827
    :cond_0
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->d:I

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->e:I

    add-int/2addr v0, v1

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->c:I

    add-int/2addr v1, v2

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->n(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;J)V

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->s(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    .line 829
    :cond_1
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->d:I

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    .line 830
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->e:I

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->c:I

    .line 831
    return-void

    .line 821
    :cond_2
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->b:I

    goto :goto_0

    .line 823
    :cond_3
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->c:I

    goto :goto_1
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    .prologue
    .line 835
    iput p2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->f:I

    .line 836
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0, p2}, Lorg/askerov/dynamicgrid/DynamicGridView;->c(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 837
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->e:I

    if-lez v0, :cond_0

    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->f:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->n(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->o(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->p(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    .line 838
    :cond_0
    :goto_0
    return-void

    .line 837
    :cond_1
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->q(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$7;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->r(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    goto :goto_0
.end method
