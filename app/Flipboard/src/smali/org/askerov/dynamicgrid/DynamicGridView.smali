.class public Lorg/askerov/dynamicgrid/DynamicGridView;
.super Landroid/widget/GridView;
.source "DynamicGridView.java"


# instance fields
.field private A:Landroid/widget/AdapterView$OnItemClickListener;

.field private B:Landroid/widget/AdapterView$OnItemClickListener;

.field private C:Landroid/widget/AbsListView$OnScrollListener;

.field private a:Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

.field public b:Z

.field public c:Z

.field private d:Landroid/graphics/drawable/BitmapDrawable;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private n:J

.field private o:Z

.field private p:I

.field private q:Z

.field private r:I

.field private s:Z

.field private t:I

.field private u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

.field private v:Z

.field private w:Z

.field private x:Landroid/view/View;

.field private y:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private z:Landroid/widget/AdapterView$OnItemLongClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 124
    invoke-direct {p0, p1}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    .line 44
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    .line 45
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    .line 47
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    .line 48
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    .line 49
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    .line 50
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->m:Ljava/util/List;

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    .line 56
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    .line 57
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 60
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->r:I

    .line 61
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    .line 62
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->t:I

    .line 64
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    .line 72
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->c:Z

    .line 76
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$1;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$1;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->z:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 114
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$2;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$2;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->B:Landroid/widget/AdapterView$OnItemClickListener;

    .line 808
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$7;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$7;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->C:Landroid/widget/AbsListView$OnScrollListener;

    .line 125
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/content/Context;)V

    .line 126
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 129
    invoke-direct {p0, p1, p2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    .line 45
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    .line 47
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    .line 48
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    .line 49
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    .line 50
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->m:Ljava/util/List;

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    .line 56
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    .line 57
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 60
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->r:I

    .line 61
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    .line 62
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->t:I

    .line 64
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    .line 72
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->c:Z

    .line 76
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$1;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$1;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->z:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 114
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$2;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$2;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->B:Landroid/widget/AdapterView$OnItemClickListener;

    .line 808
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$7;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$7;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->C:Landroid/widget/AbsListView$OnScrollListener;

    .line 130
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/content/Context;)V

    .line 131
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 134
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    .line 45
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    .line 47
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    .line 48
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    .line 49
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    .line 50
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->m:Ljava/util/List;

    .line 54
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    .line 56
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    .line 57
    iput v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 60
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->r:I

    .line 61
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    .line 62
    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->t:I

    .line 64
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    .line 72
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->c:Z

    .line 76
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$1;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$1;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->z:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 114
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$2;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$2;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->B:Landroid/widget/AdapterView$OnItemClickListener;

    .line 808
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$7;

    invoke-direct {v0, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$7;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->C:Landroid/widget/AbsListView$OnScrollListener;

    .line 135
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/content/Context;)V

    .line 136
    return-void
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    return p1
.end method

.method private a(I)J
    .locals 2

    .prologue
    .line 720
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;J)J
    .locals 1

    .prologue
    .line 32
    iput-wide p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    return-wide p1
.end method

.method private static a(Landroid/view/View;FF)Landroid/animation/AnimatorSet;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 770
    const-string v0, "translationX"

    new-array v1, v3, [F

    aput p1, v1, v4

    aput v6, v1, v5

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 771
    const-string v1, "translationY"

    new-array v2, v3, [F

    aput p2, v2, v4

    aput v6, v2, v5

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 772
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 773
    new-array v3, v3, [Landroid/animation/Animator;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 774
    return-object v2
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    return-object p1
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;)Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->a:Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    return-object v0
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;II)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 32
    if-le p2, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    if-eqz v0, :cond_2

    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    :goto_1
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    if-ge v0, v2, :cond_4

    invoke-direct {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(I)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v4

    rem-int/2addr v3, v4

    if-nez v3, :cond_1

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    neg-int v3, v3

    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/view/View;FF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-static {v2, v3, v5}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/view/View;FF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_3
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-le v0, v2, :cond_4

    invoke-direct {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(I)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v3

    add-int/2addr v3, v0

    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v4

    rem-int/2addr v3, v4

    if-nez v3, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    mul-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-static {v2, v3, v4}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/view/View;FF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    neg-int v3, v3

    int-to-float v3, v3

    invoke-static {v2, v3, v5}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/view/View;FF)Landroid/animation/AnimatorSet;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_4
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lorg/askerov/dynamicgrid/DynamicGridView$6;

    invoke-direct {v1, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$6;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Landroid/view/View;)V

    return-void
.end method

.method static synthetic a()Z
    .locals 1

    .prologue
    .line 32
    invoke-static {}, Lorg/askerov/dynamicgrid/DynamicGridView;->d()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lorg/askerov/dynamicgrid/DynamicGridView;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->c:Z

    return p1
.end method

.method static synthetic b(Lorg/askerov/dynamicgrid/DynamicGridView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    return v0
.end method

.method static synthetic b(Lorg/askerov/dynamicgrid/DynamicGridView;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    return p1
.end method

.method static synthetic b(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    return-object p1
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->computeVerticalScrollOffset()I

    move-result v3

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->computeVerticalScrollExtent()I

    move-result v5

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->computeVerticalScrollRange()I

    move-result v6

    iget v7, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    if-gtz v7, :cond_0

    if-lez v3, :cond_0

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->r:I

    neg-int v2, v2

    invoke-virtual {p0, v2, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->smoothScrollBy(II)V

    :goto_0
    iput-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->q:Z

    .line 370
    return-void

    .line 369
    :cond_0
    add-int/2addr v2, v7

    if-lt v2, v4, :cond_1

    add-int v2, v3, v5

    if-ge v2, v6, :cond_1

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->r:I

    invoke-virtual {p0, v2, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->smoothScrollBy(II)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private b(J)V
    .locals 7

    .prologue
    .line 223
    invoke-virtual {p0, p1, p2}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    .line 224
    :goto_0
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v1

    :goto_1
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getLastVisiblePosition()I

    move-result v2

    if-gt v1, v2, :cond_2

    .line 225
    if-eq v0, v1, :cond_0

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->a:Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    invoke-virtual {v2, v1}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 226
    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->m:Ljava/util/List;

    invoke-direct {p0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 223
    :cond_1
    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    goto :goto_0

    .line 229
    :cond_2
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 467
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    .line 468
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 469
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    .line 470
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->invalidate()V

    .line 471
    return-void
.end method

.method static synthetic b(Lorg/askerov/dynamicgrid/DynamicGridView;J)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(J)V

    return-void
.end method

.method static synthetic b(Lorg/askerov/dynamicgrid/DynamicGridView;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->v:Z

    return p1
.end method

.method static synthetic c(Lorg/askerov/dynamicgrid/DynamicGridView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    return v0
.end method

.method static synthetic c(Lorg/askerov/dynamicgrid/DynamicGridView;I)I
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->t:I

    return p1
.end method

.method private c(Landroid/view/View;)Landroid/graphics/Point;
    .locals 3

    .prologue
    .line 712
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 713
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getColumnCount()I

    move-result v1

    .line 714
    rem-int v2, v0, v1

    .line 715
    div-int/2addr v0, v1

    .line 716
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v1
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 400
    iget-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    invoke-virtual {p0, v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 401
    iget-boolean v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    if-eqz v1, :cond_2

    .line 402
    :cond_0
    iput-boolean v6, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    .line 403
    iput-boolean v6, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    .line 404
    iput-boolean v6, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->q:Z

    .line 405
    const/4 v1, -0x1

    iput v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 410
    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->t:I

    if-eqz v1, :cond_1

    .line 411
    iput-boolean v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    .line 421
    :goto_0
    return-void

    .line 415
    :cond_1
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 417
    new-instance v1, Lorg/askerov/dynamicgrid/DynamicGridView$3;

    invoke-direct {v1, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$3;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    const-string v3, "bounds"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    aput-object v5, v4, v6

    invoke-static {v2, v3, v1, v4}, Landroid/animation/ObjectAnimator;->ofObject(Ljava/lang/Object;Ljava/lang/String;Landroid/animation/TypeEvaluator;[Ljava/lang/Object;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v2, Lorg/askerov/dynamicgrid/DynamicGridView$4;

    invoke-direct {v2, p0}, Lorg/askerov/dynamicgrid/DynamicGridView$4;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v2, Lorg/askerov/dynamicgrid/DynamicGridView$5;

    invoke-direct {v2, p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView$5;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 419
    :cond_2
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->e()V

    goto :goto_0
.end method

.method static synthetic c(Lorg/askerov/dynamicgrid/DynamicGridView;Z)Z
    .locals 0

    .prologue
    .line 32
    iput-boolean p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->w:Z

    return p1
.end method

.method private static d()Z
    .locals 2

    .prologue
    .line 484
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lorg/askerov/dynamicgrid/DynamicGridView;)Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    return v0
.end method

.method static synthetic e(Lorg/askerov/dynamicgrid/DynamicGridView;)J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    return-wide v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 488
    iget-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    invoke-virtual {p0, v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v0

    .line 489
    iget-boolean v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    if-eqz v1, :cond_0

    .line 490
    invoke-direct {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Landroid/view/View;)V

    .line 492
    :cond_0
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    .line 493
    iput-boolean v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->q:Z

    .line 494
    const/4 v0, -0x1

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 496
    return-void
.end method

.method static synthetic f(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->y:Landroid/widget/AdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method private f()V
    .locals 14

    .prologue
    .line 499
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    sub-int v6, v0, v1

    .line 500
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    sub-int v7, v0, v1

    .line 501
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    add-int/2addr v0, v1

    add-int v8, v0, v6

    .line 502
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    add-int/2addr v0, v1

    add-int v9, v0, v7

    .line 503
    iget-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    invoke-virtual {p0, v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    .line 504
    const/4 v4, 0x0

    .line 505
    const/4 v2, 0x0

    .line 506
    const/4 v1, 0x0

    .line 507
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    invoke-direct {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->c(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v10

    .line 508
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 509
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {p0, v12, v13}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v5

    .line 510
    if-eqz v5, :cond_15

    .line 511
    invoke-direct {p0, v5}, Lorg/askerov/dynamicgrid/DynamicGridView;->c(Landroid/view/View;)Landroid/graphics/Point;

    move-result-object v3

    .line 512
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-ge v0, v12, :cond_8

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-le v0, v12, :cond_8

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 513
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    if-ge v8, v0, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    if-gt v9, v0, :cond_7

    .line 514
    :cond_0
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-ge v0, v12, :cond_9

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-ge v0, v12, :cond_9

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    .line 515
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    if-ge v8, v0, :cond_1

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    if-lt v9, v0, :cond_7

    .line 516
    :cond_1
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-le v0, v12, :cond_a

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-le v0, v12, :cond_a

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_2

    .line 517
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v0

    if-le v8, v0, :cond_2

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    if-gt v9, v0, :cond_7

    .line 518
    :cond_2
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-le v0, v12, :cond_b

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-ge v0, v12, :cond_b

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_3

    .line 519
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v0

    if-le v8, v0, :cond_3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    if-lt v9, v0, :cond_7

    .line 520
    :cond_3
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-ge v0, v12, :cond_c

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-ne v0, v12, :cond_c

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    if-lt v8, v0, :cond_7

    .line 521
    :cond_4
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-le v0, v12, :cond_d

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-ne v0, v12, :cond_d

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v0

    if-gt v8, v0, :cond_7

    .line 522
    :cond_5
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-ne v0, v12, :cond_e

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v12, v10, Landroid/graphics/Point;->x:I

    if-le v0, v12, :cond_e

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_6

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v0

    if-gt v9, v0, :cond_7

    .line 523
    :cond_6
    iget v0, v3, Landroid/graphics/Point;->y:I

    iget v12, v10, Landroid/graphics/Point;->y:I

    if-ne v0, v12, :cond_f

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v3, v10, Landroid/graphics/Point;->x:I

    if-ge v0, v3, :cond_f

    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_15

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v0

    if-ge v9, v0, :cond_15

    .line 524
    :cond_7
    invoke-static {v5}, Lorg/askerov/dynamicgrid/DynamicGridUtils;->a(Landroid/view/View;)F

    move-result v0

    iget-object v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    invoke-static {v3}, Lorg/askerov/dynamicgrid/DynamicGridUtils;->a(Landroid/view/View;)F

    move-result v3

    sub-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 525
    invoke-static {v5}, Lorg/askerov/dynamicgrid/DynamicGridUtils;->b(Landroid/view/View;)F

    move-result v0

    iget-object v12, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    invoke-static {v12}, Lorg/askerov/dynamicgrid/DynamicGridUtils;->b(Landroid/view/View;)F

    move-result v12

    sub-float/2addr v0, v12

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 526
    cmpl-float v12, v3, v2

    if-ltz v12, :cond_15

    cmpl-float v12, v0, v1

    if-ltz v12, :cond_15

    move v1, v3

    move-object v2, v5

    :goto_9
    move-object v4, v2

    move v2, v1

    move v1, v0

    .line 533
    goto/16 :goto_0

    .line 512
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 514
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 516
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 518
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 520
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_5

    .line 521
    :cond_d
    const/4 v0, 0x0

    goto :goto_6

    .line 522
    :cond_e
    const/4 v0, 0x0

    goto :goto_7

    .line 523
    :cond_f
    const/4 v0, 0x0

    goto :goto_8

    .line 534
    :cond_10
    if-eqz v4, :cond_11

    .line 535
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v1

    .line 536
    invoke-virtual {p0, v4}, Lorg/askerov/dynamicgrid/DynamicGridView;->getPositionForView(Landroid/view/View;)I

    move-result v2

    .line 538
    const/4 v0, -0x1

    if-ne v2, v0, :cond_12

    .line 539
    iget-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    invoke-direct {p0, v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(J)V

    .line 562
    :cond_11
    :goto_a
    return-void

    .line 542
    :cond_12
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapterInterface()Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->a(II)V

    .line 544
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    .line 545
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    .line 550
    invoke-static {}, Lorg/askerov/dynamicgrid/DynamicGridView;->d()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-static {}, Lorg/askerov/dynamicgrid/DynamicGridView;->g()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 551
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    invoke-direct {v0, p0, v7, v6}, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;II)V

    .line 558
    :goto_b
    iget-wide v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    invoke-direct {p0, v4, v5}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(J)V

    .line 560
    invoke-interface {v0, v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView$SwitchCellAnimator;->a(II)V

    goto :goto_a

    .line 552
    :cond_13
    invoke-static {}, Lorg/askerov/dynamicgrid/DynamicGridView;->g()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 553
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$PreHoneycombCellAnimator;

    invoke-direct {v0, p0, v7, v6}, Lorg/askerov/dynamicgrid/DynamicGridView$PreHoneycombCellAnimator;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;II)V

    goto :goto_b

    .line 555
    :cond_14
    new-instance v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    invoke-direct {v0, p0, v7, v6}, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;-><init>(Lorg/askerov/dynamicgrid/DynamicGridView;II)V

    goto :goto_b

    :cond_15
    move v0, v1

    move v1, v2

    move-object v2, v4

    goto :goto_9
.end method

.method private static g()Z
    .locals 2

    .prologue
    const/16 v1, 0x14

    .line 899
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ne v0, v1, :cond_1

    const-string v0, "L"

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lorg/askerov/dynamicgrid/DynamicGridView;)Z
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    return v0
.end method

.method private getAdapterInterface()Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;
    .locals 1

    .prologue
    .line 184
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    return-object v0
.end method

.method private getColumnCount()I
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapterInterface()Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->a()I

    move-result v0

    return v0
.end method

.method static synthetic h(Lorg/askerov/dynamicgrid/DynamicGridView;)Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    return-object v0
.end method

.method static synthetic i(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->A:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method

.method static synthetic j(Lorg/askerov/dynamicgrid/DynamicGridView;)V
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->v:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->w:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic k(Lorg/askerov/dynamicgrid/DynamicGridView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    return v0
.end method

.method static synthetic l(Lorg/askerov/dynamicgrid/DynamicGridView;)I
    .locals 1

    .prologue
    .line 32
    iget v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    return v0
.end method

.method static synthetic m(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->x:Landroid/view/View;

    return-object v0
.end method

.method static synthetic n(Lorg/askerov/dynamicgrid/DynamicGridView;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    return v0
.end method

.method static synthetic o(Lorg/askerov/dynamicgrid/DynamicGridView;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->q:Z

    return v0
.end method

.method static synthetic p(Lorg/askerov/dynamicgrid/DynamicGridView;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->b()V

    return-void
.end method

.method static synthetic q(Lorg/askerov/dynamicgrid/DynamicGridView;)Z
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->s:Z

    return v0
.end method

.method static synthetic r(Lorg/askerov/dynamicgrid/DynamicGridView;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->c()V

    return-void
.end method

.method static synthetic s(Lorg/askerov/dynamicgrid/DynamicGridView;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->f()V

    return-void
.end method


# virtual methods
.method final a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 7

    .prologue
    .line 194
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 195
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    .line 197
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 199
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v5}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 201
    new-instance v5, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-direct {v5, v6, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 203
    new-instance v4, Landroid/graphics/Rect;

    add-int/2addr v0, v3

    add-int/2addr v1, v2

    invoke-direct {v4, v3, v2, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->f:Landroid/graphics/Rect;

    .line 204
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->f:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    .line 206
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    invoke-virtual {v5, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 208
    return-object v5
.end method

.method public final a(J)Landroid/view/View;
    .locals 7

    .prologue
    .line 244
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v3

    .line 245
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    .line 246
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 247
    invoke-virtual {p0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 248
    add-int v4, v3, v1

    .line 249
    invoke-virtual {v0, v4}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->getItemId(I)J

    move-result-wide v4

    .line 250
    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    move-object v0, v2

    .line 254
    :goto_1
    return-object v0

    .line 246
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 254
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->C:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 171
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 172
    const/high16 v1, 0x41000000    # 8.0f

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->r:I

    .line 173
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 779
    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 780
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 781
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 783
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    .line 268
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 270
    :cond_0
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    invoke-interface {v0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;->a(Landroid/view/MotionEvent;)V

    .line 273
    :cond_1
    iget-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    if-eqz v0, :cond_2

    .line 275
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->requestDisallowInterceptTouchEvent(Z)V

    .line 277
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/GridView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public getLayoutDirection()I
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 282
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    .line 365
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/GridView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_1
    :goto_1
    return v0

    .line 284
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    .line 285
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    .line 286
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    .line 288
    iget-boolean v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 289
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->layoutChildren()V

    .line 291
    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    .line 292
    iput v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    .line 294
    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    invoke-virtual {p0, v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->pointToPosition(II)I

    move-result v1

    .line 295
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v2

    sub-int v2, v1, v2

    .line 296
    invoke-virtual {p0, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 297
    if-eqz v2, :cond_1

    .line 300
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    .line 301
    invoke-virtual {p0, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    .line 302
    invoke-static {}, Lorg/askerov/dynamicgrid/DynamicGridView;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 303
    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 304
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    .line 305
    iget-wide v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->n:J

    invoke-direct {p0, v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(J)V

    goto :goto_0

    .line 307
    :cond_3
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 313
    :pswitch_2
    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 314
    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v1

    .line 319
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    .line 320
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    .line 321
    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->k:I

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->j:I

    sub-int/2addr v1, v2

    .line 322
    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->l:I

    iget v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->i:I

    sub-int/2addr v2, v3

    .line 324
    iget-boolean v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->o:Z

    if-eqz v3, :cond_0

    .line 325
    iget-object v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    iget-object v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->f:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v4

    iget v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->h:I

    add-int/2addr v2, v4

    iget-object v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->f:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v4

    iget v4, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->g:I

    add-int/2addr v1, v4

    invoke-virtual {v3, v2, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 327
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->d:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 328
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->invalidate()V

    .line 329
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->f()V

    .line 330
    iput-boolean v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->q:Z

    .line 331
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->b()V

    goto/16 :goto_1

    .line 338
    :pswitch_3
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->c()V

    .line 339
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    invoke-interface {v0}, Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;->a()V

    goto/16 :goto_0

    .line 344
    :pswitch_4
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->e()V

    .line 345
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    invoke-interface {v0}, Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;->a()V

    goto/16 :goto_0

    .line 354
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const v1, 0xff00

    and-int/2addr v0, v1

    shr-int/lit8 v0, v0, 0x8

    .line 356
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    .line 357
    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->p:I

    if-ne v0, v1, :cond_0

    .line 358
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/DynamicGridView;->c()V

    goto/16 :goto_0

    .line 282
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/DynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 395
    move-object v0, p1

    check-cast v0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    iput-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->a:Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    .line 396
    invoke-super {p0, p1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 397
    return-void
.end method

.method public setOnDropListener(Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->u:Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    .line 140
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1

    .prologue
    .line 165
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->A:Landroid/widget/AdapterView$OnItemClickListener;

    .line 166
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->B:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-super {p0, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 167
    return-void
.end method

.method public setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V
    .locals 1

    .prologue
    .line 159
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->y:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 160
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView;->z:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-super {p0, v0}, Landroid/widget/GridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 161
    return-void
.end method
