.class public abstract Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;
.super Landroid/widget/BaseAdapter;
.source "AbstractDynamicGridAdapter.java"


# instance fields
.field d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->d:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(II)V
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 49
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->getItemId(I)J

    move-result-wide v0

    long-to-int v0, v0

    .line 50
    add-int/lit8 v0, v0, 0x1

    .line 51
    iget-object v1, p0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->d:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x1

    return v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 75
    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 76
    :cond_0
    const-wide/16 v0, -0x1

    .line 79
    :goto_0
    return-wide v0

    .line 78
    :cond_1
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method
