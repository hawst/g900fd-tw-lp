.class Lorg/askerov/dynamicgrid/DynamicGridView$1;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field final synthetic a:Lorg/askerov/dynamicgrid/DynamicGridView;


# direct methods
.method constructor <init>(Lorg/askerov/dynamicgrid/DynamicGridView;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;)Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 81
    :cond_1
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-boolean v1, v1, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    if-nez v1, :cond_0

    .line 83
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 84
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1, v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 86
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;)I

    move-result v1

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->c(Lorg/askerov/dynamicgrid/DynamicGridView;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->pointToPosition(II)I

    move-result v0

    .line 87
    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v0, v1

    .line 89
    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v2, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 90
    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v3, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;J)J

    .line 91
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v2, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    invoke-static {v0, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;

    .line 92
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {}, Lorg/askerov/dynamicgrid/DynamicGridView;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 93
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0, v6}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;Z)Z

    .line 97
    :cond_2
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->d(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    .line 99
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;J)V

    .line 101
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->f(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 102
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->f(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    .line 104
    :cond_3
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->g(Lorg/askerov/dynamicgrid/DynamicGridView;)Z

    .line 105
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->h(Lorg/askerov/dynamicgrid/DynamicGridView;)Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 106
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$1;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->h(Lorg/askerov/dynamicgrid/DynamicGridView;)Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;

    move-result-object v0

    invoke-interface {v0}, Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;->b()V

    :cond_4
    move v0, v6

    .line 109
    goto/16 :goto_0
.end method
