.class public abstract Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;
.super Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;
.source "BaseDynamicGridAdapter.java"


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field public e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    .line 20
    iput-object p1, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->e:Landroid/content/Context;

    .line 21
    const/4 v0, 0x3

    iput v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->b:I

    .line 22
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    .line 25
    iput-object p1, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->e:Landroid/content/Context;

    .line 26
    const/4 v0, 0x2

    iput v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->b:I

    .line 27
    invoke-direct {p0, p2}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->b(Ljava/util/List;)V

    .line 28
    return-void
.end method

.method private b(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->getItemId(I)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->d:Ljava/util/HashMap;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 33
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->b:I

    return v0
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->getCount()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 97
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    invoke-static {v0, p1, p2}, Lorg/askerov/dynamicgrid/DynamicGridUtils;->a(Ljava/util/ArrayList;II)V

    .line 98
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->notifyDataSetChanged()V

    .line 100
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lorg/askerov/dynamicgrid/AbstractDynamicGridAdapter;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->notifyDataSetChanged()V

    .line 38
    invoke-direct {p0, p1}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->b(Ljava/util/List;)V

    .line 39
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->notifyDataSetChanged()V

    .line 40
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a(Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->notifyDataSetChanged()V

    .line 52
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0, p1}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a(Ljava/lang/Object;)V

    .line 56
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 57
    invoke-virtual {p0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->notifyDataSetChanged()V

    .line 58
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
