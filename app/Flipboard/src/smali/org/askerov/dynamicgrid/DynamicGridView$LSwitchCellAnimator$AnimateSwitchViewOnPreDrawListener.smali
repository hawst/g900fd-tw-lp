.class Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

.field private final b:I

.field private final c:I


# direct methods
.method constructor <init>(Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;II)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 657
    iput p2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->b:I

    .line 658
    iput p3, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->c:I

    .line 659
    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 663
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 665
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v1, v1, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->k(Lorg/askerov/dynamicgrid/DynamicGridView;)I

    move-result v1

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a(Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 666
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v1, v1, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->l(Lorg/askerov/dynamicgrid/DynamicGridView;)I

    move-result v1

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->b(Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 668
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->b:I

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->c:I

    invoke-static {v0, v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;II)V

    .line 670
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->m(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 671
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v1, v1, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v2, v2, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->e(Lorg/askerov/dynamicgrid/DynamicGridView;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(J)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;Landroid/view/View;)Landroid/view/View;

    .line 673
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$LSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->m(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 674
    const/4 v0, 0x1

    return v0
.end method
