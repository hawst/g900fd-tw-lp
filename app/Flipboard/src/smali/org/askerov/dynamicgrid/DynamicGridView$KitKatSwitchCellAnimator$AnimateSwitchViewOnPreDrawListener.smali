.class Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;
.super Ljava/lang/Object;
.source "DynamicGridView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

.field private final b:Landroid/view/View;

.field private final c:I

.field private final d:I


# direct methods
.method constructor <init>(Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;Landroid/view/View;II)V
    .locals 0

    .prologue
    .line 609
    iput-object p1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    iput-object p2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->b:Landroid/view/View;

    .line 611
    iput p3, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->c:I

    .line 612
    iput p4, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->d:I

    .line 613
    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 3

    .prologue
    .line 617
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-virtual {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 619
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v1, v1, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->k(Lorg/askerov/dynamicgrid/DynamicGridView;)I

    move-result v1

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a(Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 620
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v1, v1, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->l(Lorg/askerov/dynamicgrid/DynamicGridView;)I

    move-result v1

    iget-object v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    invoke-static {v2}, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->b(Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->b(Lorg/askerov/dynamicgrid/DynamicGridView;I)I

    .line 622
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget v1, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->c:I

    iget v2, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->d:I

    invoke-static {v0, v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->a(Lorg/askerov/dynamicgrid/DynamicGridView;II)V

    .line 624
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 626
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->m(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator$AnimateSwitchViewOnPreDrawListener;->a:Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;

    iget-object v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView$KitKatSwitchCellAnimator;->a:Lorg/askerov/dynamicgrid/DynamicGridView;

    invoke-static {v0}, Lorg/askerov/dynamicgrid/DynamicGridView;->m(Lorg/askerov/dynamicgrid/DynamicGridView;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 629
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
