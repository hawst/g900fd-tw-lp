.class final Lcom/google/ads/mediation/admob/AdMobAdapter$a;
.super Lcom/google/android/gms/ads/AdListener;


# instance fields
.field private final a:Lcom/google/ads/mediation/admob/AdMobAdapter;

.field private final b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;


# direct methods
.method public constructor <init>(Lcom/google/ads/mediation/admob/AdMobAdapter;Lcom/google/android/gms/ads/mediation/MediationBannerListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    iput-object p1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    iput-object p2, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    iget-object v1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    invoke-interface {v0}, Lcom/google/android/gms/ads/mediation/MediationBannerListener;->a()V

    return-void
.end method

.method public final a(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    iget-object v1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    invoke-interface {v0, p1}, Lcom/google/android/gms/ads/mediation/MediationBannerListener;->a(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    iget-object v1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    invoke-interface {v0}, Lcom/google/android/gms/ads/mediation/MediationBannerListener;->e()V

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    iget-object v1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    invoke-interface {v0}, Lcom/google/android/gms/ads/mediation/MediationBannerListener;->b()V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    iget-object v1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    invoke-interface {v0}, Lcom/google/android/gms/ads/mediation/MediationBannerListener;->c()V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->b:Lcom/google/android/gms/ads/mediation/MediationBannerListener;

    iget-object v1, p0, Lcom/google/ads/mediation/admob/AdMobAdapter$a;->a:Lcom/google/ads/mediation/admob/AdMobAdapter;

    invoke-interface {v0}, Lcom/google/android/gms/ads/mediation/MediationBannerListener;->d()V

    return-void
.end method
