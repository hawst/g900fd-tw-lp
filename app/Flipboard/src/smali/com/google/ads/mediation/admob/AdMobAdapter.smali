.class public final Lcom/google/ads/mediation/admob/AdMobAdapter;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/ads/mediation/MediationBannerAdapter;
.implements Lcom/google/android/gms/ads/mediation/MediationInterstitialAdapter;


# instance fields
.field private a:Lcom/google/android/gms/ads/AdView;

.field private b:Lcom/google/android/gms/ads/InterstitialAd;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/ads/mediation/MediationAdRequest;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/gms/ads/AdRequest;
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v3, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-interface {p1}, Lcom/google/android/gms/ads/mediation/MediationAdRequest;->a()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    iput-object v0, v4, Lcom/google/android/gms/internal/bg$a;->d:Ljava/util/Date;

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/ads/mediation/MediationAdRequest;->b()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    iput v0, v4, Lcom/google/android/gms/internal/bg$a;->e:I

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/ads/mediation/MediationAdRequest;->c()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    iget-object v5, v5, Lcom/google/android/gms/internal/bg$a;->a:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lcom/google/android/gms/ads/mediation/MediationAdRequest;->d()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v4, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    iput-object v0, v4, Lcom/google/android/gms/internal/bg$a;->f:Landroid/location/Location;

    :cond_3
    invoke-interface {p1}, Lcom/google/android/gms/ads/mediation/MediationAdRequest;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/android/gms/internal/gr;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    iget-object v4, v4, Lcom/google/android/gms/internal/bg$a;->c:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_4
    const-string v0, "tagForChildDirectedTreatment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_5

    const-string v0, "tagForChildDirectedTreatment"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_1
    iget-object v4, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    if-eqz v0, :cond_8

    move v0, v1

    :goto_2
    iput v0, v4, Lcom/google/android/gms/internal/bg$a;->g:I

    :cond_5
    if-eqz p2, :cond_9

    :goto_3
    const-string v0, "gw"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "mad_hac"

    const-string v4, "mad_hac"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "adJson"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "_ad"

    const-string v4, "adJson"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string v0, "_noRefresh"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-class v0, Lcom/google/ads/mediation/admob/AdMobAdapter;

    iget-object v1, v3, Lcom/google/android/gms/ads/AdRequest$Builder;->a:Lcom/google/android/gms/internal/bg$a;

    iget-object v1, v1, Lcom/google/android/gms/internal/bg$a;->b:Landroid/os/Bundle;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/gms/ads/AdRequest;

    invoke-direct {v0, v3, v2}, Lcom/google/android/gms/ads/AdRequest;-><init>(Lcom/google/android/gms/ads/AdRequest$Builder;B)V

    return-object v0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bh;

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bd;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    :cond_1
    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    if-eqz v0, :cond_2

    iput-object v2, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    :cond_2
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to destroy AdView."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/ads/mediation/MediationBannerListener;Landroid/os/Bundle;Lcom/google/android/gms/ads/AdSize;Lcom/google/android/gms/ads/mediation/MediationAdRequest;Landroid/os/Bundle;)V
    .locals 6

    new-instance v0, Lcom/google/android/gms/ads/AdView;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/AdView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Lcom/google/android/gms/ads/AdSize;

    iget v2, p4, Lcom/google/android/gms/ads/AdSize;->h:I

    iget v3, p4, Lcom/google/android/gms/ads/AdSize;->i:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/ads/AdSize;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdSize(Lcom/google/android/gms/ads/AdSize;)V

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    const-string v1, "pubid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdUnitId(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    new-instance v1, Lcom/google/ads/mediation/admob/AdMobAdapter$a;

    invoke-direct {v1, p0, p2}, Lcom/google/ads/mediation/admob/AdMobAdapter$a;-><init>(Lcom/google/ads/mediation/admob/AdMobAdapter;Lcom/google/android/gms/ads/mediation/MediationBannerListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    invoke-static {p1, p5, p6, p3}, Lcom/google/ads/mediation/admob/AdMobAdapter;->a(Landroid/content/Context;Lcom/google/android/gms/ads/mediation/MediationAdRequest;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bh;

    iget-object v1, v1, Lcom/google/android/gms/ads/AdRequest;->b:Lcom/google/android/gms/internal/bg;

    :try_start_0
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    if-nez v0, :cond_8

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->e:[Lcom/google/android/gms/ads/AdSize;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad size and ad unit ID must be set before loadAd is called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "Failed to load ad."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/internal/ay;

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->e:[Lcom/google/android/gms/ads/AdSize;

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/internal/ay;-><init>(Landroid/content/Context;[Lcom/google/android/gms/ads/AdSize;)V

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->f:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/android/gms/internal/bh;->a:Lcom/google/android/gms/internal/cs;

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/gms/internal/au;->a(Landroid/content/Context;Lcom/google/android/gms/internal/ay;Ljava/lang/String;Lcom/google/android/gms/internal/cs;)Lcom/google/android/gms/internal/bd;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/ads/AdListener;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/at;

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/ads/AdListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/at;-><init>(Lcom/google/android/gms/ads/AdListener;)V

    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/bc;)V

    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->i:Lcom/google/android/gms/ads/doubleclick/AppEventListener;

    if-eqz v0, :cond_4

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/ba;

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->i:Lcom/google/android/gms/ads/doubleclick/AppEventListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/ba;-><init>(Lcom/google/android/gms/ads/doubleclick/AppEventListener;)V

    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/bf;)V

    :cond_4
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/em;

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/em;-><init>(Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;)V

    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/eh;)V

    :cond_5
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->k:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    if-eqz v0, :cond_6

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/eq;

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->k:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/eq;-><init>(Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;)V

    iget-object v4, v2, Lcom/google/android/gms/internal/bh;->g:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/el;Ljava/lang/String;)V

    :cond_6
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->l:Lcom/google/android/gms/ads/doubleclick/b;

    if-eqz v0, :cond_7

    iget-object v3, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    new-instance v4, Lcom/google/android/gms/internal/ew;

    iget-object v5, v2, Lcom/google/android/gms/internal/bh;->l:Lcom/google/android/gms/ads/doubleclick/b;

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->h:Landroid/view/ViewGroup;

    check-cast v0, Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/internal/ew;-><init>(Lcom/google/android/gms/ads/doubleclick/b;Lcom/google/android/gms/ads/doubleclick/PublisherAdView;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/et;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_7
    :try_start_2
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bd;->c()Lcom/google/android/gms/dynamic/d;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    if-nez v0, :cond_9

    :cond_8
    :goto_1
    :try_start_3
    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    iget-object v3, v2, Lcom/google/android/gms/internal/bh;->b:Lcom/google/android/gms/internal/ax;

    iget-object v3, v2, Lcom/google/android/gms/internal/bh;->h:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/gms/internal/ax;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bg;)Lcom/google/android/gms/internal/av;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/av;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/gms/internal/bh;->a:Lcom/google/android/gms/internal/cs;

    iget-object v1, v1, Lcom/google/android/gms/internal/bg;->i:Ljava/util/Map;

    iput-object v1, v0, Lcom/google/android/gms/internal/cs;->a:Ljava/util/Map;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :cond_9
    :try_start_4
    iget-object v3, v2, Lcom/google/android/gms/internal/bh;->h:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/e;->a(Lcom/google/android/gms/dynamic/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    :try_start_5
    const-string v3, "Failed to get an ad frame."

    invoke-static {v3, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/ads/mediation/MediationInterstitialListener;Landroid/os/Bundle;Lcom/google/android/gms/ads/mediation/MediationAdRequest;Landroid/os/Bundle;)V
    .locals 6

    new-instance v0, Lcom/google/android/gms/ads/InterstitialAd;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/InterstitialAd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    const-string v1, "pubid"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/ads/InterstitialAd;->a:Lcom/google/android/gms/internal/bi;

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The ad unit ID can only be set once on InterstitialAd."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v1, v0, Lcom/google/android/gms/internal/bi;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v1, Lcom/google/ads/mediation/admob/AdMobAdapter$b;

    invoke-direct {v1, p0, p2}, Lcom/google/ads/mediation/admob/AdMobAdapter$b;-><init>(Lcom/google/ads/mediation/admob/AdMobAdapter;Lcom/google/android/gms/ads/mediation/MediationInterstitialListener;)V

    iget-object v0, v0, Lcom/google/android/gms/ads/InterstitialAd;->a:Lcom/google/android/gms/internal/bi;

    :try_start_0
    iput-object v1, v0, Lcom/google/android/gms/internal/bi;->d:Lcom/google/android/gms/ads/AdListener;

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    new-instance v2, Lcom/google/android/gms/internal/at;

    invoke-direct {v2, v1}, Lcom/google/android/gms/internal/at;-><init>(Lcom/google/android/gms/ads/AdListener;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/bc;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-static {p1, p4, p5, p3}, Lcom/google/ads/mediation/admob/AdMobAdapter;->a(Landroid/content/Context;Lcom/google/android/gms/ads/mediation/MediationAdRequest;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/ads/InterstitialAd;->a:Lcom/google/android/gms/internal/bi;

    iget-object v1, v1, Lcom/google/android/gms/ads/AdRequest;->b:Lcom/google/android/gms/internal/bg;

    :try_start_1
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    if-nez v2, :cond_7

    const-string v2, "loadAd"

    iget-object v3, v0, Lcom/google/android/gms/internal/bi;->f:Ljava/lang/String;

    if-nez v3, :cond_2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/bi;->a(Ljava/lang/String;)V

    :cond_2
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->b:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/internal/ay;

    invoke-direct {v3}, Lcom/google/android/gms/internal/ay;-><init>()V

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->f:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/internal/bi;->a:Lcom/google/android/gms/internal/cs;

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/internal/au;->a(Landroid/content/Context;Lcom/google/android/gms/internal/ay;Ljava/lang/String;Lcom/google/android/gms/internal/cs;)Lcom/google/android/gms/internal/bd;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->d:Lcom/google/android/gms/ads/AdListener;

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/at;

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->d:Lcom/google/android/gms/ads/AdListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/at;-><init>(Lcom/google/android/gms/ads/AdListener;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/bc;)V

    :cond_3
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->h:Lcom/google/android/gms/ads/doubleclick/AppEventListener;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/ba;

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->h:Lcom/google/android/gms/ads/doubleclick/AppEventListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/ba;-><init>(Lcom/google/android/gms/ads/doubleclick/AppEventListener;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/bf;)V

    :cond_4
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->j:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/em;

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->j:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/em;-><init>(Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/eh;)V

    :cond_5
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->i:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    if-eqz v2, :cond_6

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/eq;

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->i:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/eq;-><init>(Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;)V

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->g:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/el;Ljava/lang/String;)V

    :cond_6
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->l:Lcom/google/android/gms/ads/doubleclick/c;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    new-instance v3, Lcom/google/android/gms/internal/ex;

    iget-object v4, v0, Lcom/google/android/gms/internal/bi;->l:Lcom/google/android/gms/ads/doubleclick/c;

    iget-object v5, v0, Lcom/google/android/gms/internal/bi;->k:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/internal/ex;-><init>(Lcom/google/android/gms/ads/doubleclick/c;Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/eu;)V

    :cond_7
    iget-object v2, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    iget-object v3, v0, Lcom/google/android/gms/internal/bi;->c:Lcom/google/android/gms/internal/ax;

    iget-object v3, v0, Lcom/google/android/gms/internal/bi;->b:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/google/android/gms/internal/ax;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bg;)Lcom/google/android/gms/internal/av;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/bd;->a(Lcom/google/android/gms/internal/av;)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v0, v0, Lcom/google/android/gms/internal/bi;->a:Lcom/google/android/gms/internal/cs;

    iget-object v1, v1, Lcom/google/android/gms/internal/bg;->i:Ljava/util/Map;

    iput-object v1, v0, Lcom/google/android/gms/internal/cs;->a:Ljava/util/Map;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_8
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to set the AdListener."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    const-string v1, "Failed to load ad."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bh;

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bd;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to call pause."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    iget-object v0, v0, Lcom/google/android/gms/ads/AdView;->a:Lcom/google/android/gms/internal/bh;

    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/bd;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bd;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to call resume."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final d()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->a:Lcom/google/android/gms/ads/AdView;

    return-object v0
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/ads/mediation/admob/AdMobAdapter;->b:Lcom/google/android/gms/ads/InterstitialAd;

    iget-object v0, v0, Lcom/google/android/gms/ads/InterstitialAd;->a:Lcom/google/android/gms/internal/bi;

    :try_start_0
    const-string v1, "show"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/bi;->a(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    invoke-interface {v0}, Lcom/google/android/gms/internal/bd;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Failed to show interstitial."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
