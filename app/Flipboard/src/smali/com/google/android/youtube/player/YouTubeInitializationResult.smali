.class public final enum Lcom/google/android/youtube/player/YouTubeInitializationResult;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/youtube/player/YouTubeInitializationResult;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum b:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum d:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum e:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum f:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum g:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum h:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum i:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum j:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum k:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public static final enum l:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field private static final synthetic m:[Lcom/google/android/youtube/player/YouTubeInitializationResult;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->a:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "INTERNAL_ERROR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->b:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v5}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "SERVICE_MISSING"

    invoke-direct {v0, v1, v6}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->d:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "SERVICE_VERSION_UPDATE_REQUIRED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->e:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "SERVICE_DISABLED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->f:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "SERVICE_INVALID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->g:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "ERROR_CONNECTING_TO_SERVICE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->h:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "CLIENT_LIBRARY_UPDATE_REQUIRED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->i:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "NETWORK_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->j:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "DEVELOPER_KEY_INVALID"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->k:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    new-instance v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const-string v1, "INVALID_APPLICATION_SIGNATURE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/android/youtube/player/YouTubeInitializationResult;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->l:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/google/android/youtube/player/YouTubeInitializationResult;

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->a:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->b:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->d:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->e:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->f:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->g:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->h:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->i:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->j:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->k:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/youtube/player/YouTubeInitializationResult;->l:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->m:[Lcom/google/android/youtube/player/YouTubeInitializationResult;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/youtube/player/YouTubeInitializationResult;
    .locals 1

    const-class v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;

    return-object v0
.end method

.method public static values()[Lcom/google/android/youtube/player/YouTubeInitializationResult;
    .locals 1

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->m:[Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v0}, [Lcom/google/android/youtube/player/YouTubeInitializationResult;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/youtube/player/YouTubeInitializationResult;

    return-object v0
.end method
