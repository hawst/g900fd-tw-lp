.class public final Lcom/google/android/youtube/player/internal/r$c;
.super Lcom/google/android/youtube/player/internal/r$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/youtube/player/internal/r",
        "<TT;>.b<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

.field public final d:Landroid/os/IBinder;

.field final synthetic e:Lcom/google/android/youtube/player/internal/r;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/player/internal/r;Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/player/internal/r$b;-><init>(Lcom/google/android/youtube/player/internal/r;Ljava/lang/Object;)V

    invoke-static {p2}, Lcom/google/android/youtube/player/internal/r;->a(Ljava/lang/String;)Lcom/google/android/youtube/player/YouTubeInitializationResult;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/player/internal/r$c;->c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    iput-object p3, p0, Lcom/google/android/youtube/player/internal/r$c;->d:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v0, 0x0

    check-cast p1, Ljava/lang/Boolean;

    if-eqz p1, :cond_0

    sget-object v2, Lcom/google/android/youtube/player/internal/r$1;->a:[I

    iget-object v3, p0, Lcom/google/android/youtube/player/internal/r$c;->c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v3}, Lcom/google/android/youtube/player/YouTubeInitializationResult;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    iget-object v0, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    iget-object v1, p0, Lcom/google/android/youtube/player/internal/r$c;->c:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/player/internal/r;->a(Lcom/google/android/youtube/player/YouTubeInitializationResult;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/youtube/player/internal/r$c;->d:Landroid/os/IBinder;

    invoke-interface {v2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    invoke-virtual {v3}, Lcom/google/android/youtube/player/internal/r;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    iget-object v3, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    iget-object v4, p0, Lcom/google/android/youtube/player/internal/r$c;->d:Landroid/os/IBinder;

    invoke-virtual {v3, v4}, Lcom/google/android/youtube/player/internal/r;->a(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/youtube/player/internal/r;->a(Lcom/google/android/youtube/player/internal/r;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v2, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    invoke-static {v2}, Lcom/google/android/youtube/player/internal/r;->d(Lcom/google/android/youtube/player/internal/r;)Landroid/os/IInterface;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v3, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    iget-object v4, v3, Lcom/google/android/youtube/player/internal/r;->b:Ljava/util/ArrayList;

    monitor-enter v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-boolean v2, v3, Lcom/google/android/youtube/player/internal/r;->d:Z

    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    invoke-static {v2}, Lcom/google/android/youtube/player/internal/ac;->a(Z)V

    iget-object v2, v3, Lcom/google/android/youtube/player/internal/r;->a:Landroid/os/Handler;

    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/youtube/player/internal/r;->d:Z

    iget-object v2, v3, Lcom/google/android/youtube/player/internal/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_3

    :goto_2
    invoke-static {v1}, Lcom/google/android/youtube/player/internal/ac;->a(Z)V

    iget-object v2, v3, Lcom/google/android/youtube/player/internal/r;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v0

    :goto_3
    if-ge v1, v5, :cond_4

    iget-boolean v0, v3, Lcom/google/android/youtube/player/internal/r;->e:Z

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/google/android/youtube/player/internal/r;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, v3, Lcom/google/android/youtube/player/internal/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    iget-object v0, v3, Lcom/google/android/youtube/player/internal/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/player/internal/t$a;

    invoke-interface {v0}, Lcom/google/android/youtube/player/internal/t$a;->a()V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_2
    move v2, v0

    goto :goto_1

    :cond_3
    move v1, v0

    goto :goto_2

    :cond_4
    iget-object v0, v3, Lcom/google/android/youtube/player/internal/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, v3, Lcom/google/android/youtube/player/internal/r;->d:Z

    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4

    throw v0
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    :cond_5
    iget-object v0, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/r;->f(Lcom/google/android/youtube/player/internal/r;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    invoke-static {v1}, Lcom/google/android/youtube/player/internal/r;->e(Lcom/google/android/youtube/player/internal/r;)Landroid/content/ServiceConnection;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    iget-object v0, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    invoke-static {v0}, Lcom/google/android/youtube/player/internal/r;->g(Lcom/google/android/youtube/player/internal/r;)Landroid/content/ServiceConnection;

    iget-object v0, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/youtube/player/internal/r;->a(Lcom/google/android/youtube/player/internal/r;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/youtube/player/internal/r$c;->e:Lcom/google/android/youtube/player/internal/r;

    sget-object v1, Lcom/google/android/youtube/player/YouTubeInitializationResult;->b:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/player/internal/r;->a(Lcom/google/android/youtube/player/YouTubeInitializationResult;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
