.class public Lcom/google/android/gms/analytics/z;
.super Ljava/lang/Object;


# static fields
.field private static a:Lcom/google/android/gms/analytics/GoogleAnalytics;


# direct methods
.method public static a()V
    .locals 0

    invoke-static {}, Lcom/google/android/gms/analytics/z;->c()Lcom/google/android/gms/analytics/Logger;

    return-void
.end method

.method public static b()V
    .locals 0

    invoke-static {}, Lcom/google/android/gms/analytics/z;->c()Lcom/google/android/gms/analytics/Logger;

    return-void
.end method

.method private static c()Lcom/google/android/gms/analytics/Logger;
    .locals 1

    sget-object v0, Lcom/google/android/gms/analytics/z;->a:Lcom/google/android/gms/analytics/GoogleAnalytics;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/analytics/GoogleAnalytics;->a()Lcom/google/android/gms/analytics/GoogleAnalytics;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/z;->a:Lcom/google/android/gms/analytics/GoogleAnalytics;

    :cond_0
    sget-object v0, Lcom/google/android/gms/analytics/z;->a:Lcom/google/android/gms/analytics/GoogleAnalytics;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/analytics/z;->a:Lcom/google/android/gms/analytics/GoogleAnalytics;

    iget-object v0, v0, Lcom/google/android/gms/analytics/GoogleAnalytics;->a:Lcom/google/android/gms/analytics/Logger;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
