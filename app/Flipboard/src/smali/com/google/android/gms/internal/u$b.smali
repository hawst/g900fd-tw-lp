.class Lcom/google/android/gms/internal/u$b;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/u$a;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/content/Context;

.field public final d:Lcom/google/android/gms/internal/k;

.field public final e:Lcom/google/android/gms/internal/gt;

.field public f:Lcom/google/android/gms/internal/bc;

.field public g:Lcom/google/android/gms/internal/gg;

.field public h:Lcom/google/android/gms/internal/gg;

.field public i:Lcom/google/android/gms/internal/ay;

.field public j:Lcom/google/android/gms/internal/fz;

.field public k:Lcom/google/android/gms/internal/fz$a;

.field public l:Lcom/google/android/gms/internal/ga;

.field public m:Lcom/google/android/gms/internal/bf;

.field public n:Lcom/google/android/gms/internal/el;

.field public o:Lcom/google/android/gms/internal/eh;

.field public p:Lcom/google/android/gms/internal/et;

.field public q:Lcom/google/android/gms/internal/eu;

.field public r:Lcom/google/android/gms/internal/bt;

.field public s:Lcom/google/android/gms/internal/bu;

.field public t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public u:Lcom/google/android/gms/internal/ee;

.field public v:Lcom/google/android/gms/internal/ge;

.field public w:Landroid/view/View;

.field public x:I

.field public y:Z

.field z:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/gms/internal/ga;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/ay;Ljava/lang/String;Lcom/google/android/gms/internal/gt;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/internal/u$b;->v:Lcom/google/android/gms/internal/ge;

    iput-object v1, p0, Lcom/google/android/gms/internal/u$b;->w:Landroid/view/View;

    iput v0, p0, Lcom/google/android/gms/internal/u$b;->x:I

    iput-boolean v0, p0, Lcom/google/android/gms/internal/u$b;->y:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/u$b;->z:Ljava/util/HashSet;

    iget-boolean v0, p2, Lcom/google/android/gms/internal/ay;->e:Z

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/internal/u$b;->a:Lcom/google/android/gms/internal/u$a;

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/internal/u$b;->i:Lcom/google/android/gms/internal/ay;

    iput-object p3, p0, Lcom/google/android/gms/internal/u$b;->b:Ljava/lang/String;

    iput-object p1, p0, Lcom/google/android/gms/internal/u$b;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/gms/internal/u$b;->e:Lcom/google/android/gms/internal/gt;

    new-instance v0, Lcom/google/android/gms/internal/k;

    new-instance v1, Lcom/google/android/gms/internal/w;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/w;-><init>(Lcom/google/android/gms/internal/u$b;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/k;-><init>(Lcom/google/android/gms/internal/g;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/u$b;->d:Lcom/google/android/gms/internal/k;

    return-void

    :cond_0
    new-instance v0, Lcom/google/android/gms/internal/u$a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/u$a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/u$b;->a:Lcom/google/android/gms/internal/u$a;

    iget-object v0, p0, Lcom/google/android/gms/internal/u$b;->a:Lcom/google/android/gms/internal/u$a;

    iget v1, p2, Lcom/google/android/gms/internal/ay;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/u$a;->setMinimumWidth(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/u$b;->a:Lcom/google/android/gms/internal/u$a;

    iget v1, p2, Lcom/google/android/gms/internal/ay;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/u$a;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/u$b;->a:Lcom/google/android/gms/internal/u$a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/u$a;->setVisibility(I)V

    goto :goto_0
.end method
