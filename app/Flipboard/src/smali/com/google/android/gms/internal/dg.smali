.class public Lcom/google/android/gms/internal/dg;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/internal/gv;

.field final b:Lcom/google/android/gms/internal/bl;

.field c:Landroid/util/DisplayMetrics;

.field d:F

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:[I

.field private final k:Landroid/content/Context;

.field private final l:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/gv;Landroid/content/Context;Lcom/google/android/gms/internal/bl;)V
    .locals 5

    const/4 v4, 0x0

    const/high16 v3, 0x43200000    # 160.0f

    const/4 v0, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/google/android/gms/internal/dg;->e:I

    iput v0, p0, Lcom/google/android/gms/internal/dg;->f:I

    iput v0, p0, Lcom/google/android/gms/internal/dg;->h:I

    iput v0, p0, Lcom/google/android/gms/internal/dg;->i:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/internal/dg;->j:[I

    iput-object p1, p0, Lcom/google/android/gms/internal/dg;->a:Lcom/google/android/gms/internal/gv;

    iput-object p2, p0, Lcom/google/android/gms/internal/dg;->k:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/dg;->b:Lcom/google/android/gms/internal/bl;

    const-string v0, "window"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/internal/dg;->l:Landroid/view/WindowManager;

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    iget-object v0, p0, Lcom/google/android/gms/internal/dg;->l:Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v1, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/google/android/gms/internal/dg;->d:F

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dg;->g:I

    iget-object v0, p0, Lcom/google/android/gms/internal/dg;->k:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/gj;->c(Landroid/content/Context;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v1, v1

    div-float v1, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/internal/dg;->e:I

    iget-object v2, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dg;->f:I

    iget-object v0, p0, Lcom/google/android/gms/internal/dg;->a:Lcom/google/android/gms/internal/gv;

    iget-object v1, p0, Lcom/google/android/gms/internal/dg;->j:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gv;->getLocationOnScreen([I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dg;->a:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0, v4, v4}, Lcom/google/android/gms/internal/gv;->measure(II)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dg;->c:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    div-float v0, v3, v0

    iget-object v1, p0, Lcom/google/android/gms/internal/dg;->a:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gv;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/internal/dg;->h:I

    iget-object v1, p0, Lcom/google/android/gms/internal/dg;->a:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gv;->getMeasuredHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/dg;->i:I

    return-void
.end method
