.class public final Lcom/google/android/gms/common/internal/e$h;
.super Lcom/google/android/gms/common/internal/e$b;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/gms/common/internal/e",
        "<TT;>.b<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final b:I

.field public final c:Landroid/os/Bundle;

.field public final d:Landroid/os/IBinder;

.field final synthetic e:Lcom/google/android/gms/common/internal/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/e;ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/internal/e$b;-><init>(Lcom/google/android/gms/common/internal/e;Ljava/lang/Object;)V

    iput p2, p0, Lcom/google/android/gms/common/internal/e$h;->b:I

    iput-object p3, p0, Lcom/google/android/gms/common/internal/e$h;->d:Landroid/os/IBinder;

    iput-object p4, p0, Lcom/google/android/gms/common/internal/e$h;->c:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    check-cast p1, Ljava/lang/Boolean;

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;I)V

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/google/android/gms/common/internal/e$h;->b:I

    sparse-switch v3, :sswitch_data_0

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->c:Landroid/os/Bundle;

    const-string v3, "pendingIntent"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->d(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/e$f;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->e(Lcom/google/android/gms/common/internal/e;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/g;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/g;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v5}, Lcom/google/android/gms/common/internal/e;->d(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/e$f;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/common/internal/g;->b(Ljava/lang/String;Lcom/google/android/gms/common/internal/e$f;)V

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->f(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/e$f;

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v3, v2}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;I)V

    iget-object v2, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v2, v1}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/f;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    iget v3, p0, Lcom/google/android/gms/common/internal/e$h;->b:I

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/f;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto :goto_0

    :sswitch_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->d:Landroid/os/IBinder;

    invoke-interface {v3}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-virtual {v4}, Lcom/google/android/gms/common/internal/e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    iget-object v4, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    iget-object v5, p0, Lcom/google/android/gms/common/internal/e$h;->d:Landroid/os/IBinder;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/common/internal/e;->a(Landroid/os/IBinder;)Landroid/os/IInterface;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->c(Lcom/google/android/gms/common/internal/e;)Landroid/os/IInterface;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;I)V

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/f;

    move-result-object v4

    iget-object v5, v4, Lcom/google/android/gms/common/internal/f;->b:Ljava/util/ArrayList;

    monitor-enter v5
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v3, v4, Lcom/google/android/gms/common/internal/f;->a:Lcom/google/android/gms/common/internal/f$b;

    iget-object v6, v4, Lcom/google/android/gms/common/internal/f;->b:Ljava/util/ArrayList;

    monitor-enter v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-boolean v3, v4, Lcom/google/android/gms/common/internal/f;->d:Z

    if-nez v3, :cond_5

    move v3, v2

    :goto_2
    invoke-static {v3}, Lcom/google/android/gms/common/internal/o;->a(Z)V

    iget-object v3, v4, Lcom/google/android/gms/common/internal/f;->f:Landroid/os/Handler;

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Landroid/os/Handler;->removeMessages(I)V

    const/4 v3, 0x1

    iput-boolean v3, v4, Lcom/google/android/gms/common/internal/f;->d:Z

    iget-object v3, v4, Lcom/google/android/gms/common/internal/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_2

    move v0, v2

    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/o;->a(Z)V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v3, v4, Lcom/google/android/gms/common/internal/f;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;

    iget-object v7, v4, Lcom/google/android/gms/common/internal/f;->a:Lcom/google/android/gms/common/internal/f$b;

    invoke-interface {v7}, Lcom/google/android/gms/common/internal/f$b;->i()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, v4, Lcom/google/android/gms/common/internal/f;->a:Lcom/google/android/gms/common/internal/f$b;

    invoke-interface {v7}, Lcom/google/android/gms/common/internal/f$b;->d()Z

    move-result v7

    if-eqz v7, :cond_6

    iget-object v7, v4, Lcom/google/android/gms/common/internal/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient$ConnectionCallbacks;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v6

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v5

    throw v0
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->e(Lcom/google/android/gms/common/internal/e;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/g;->a(Landroid/content/Context;)Lcom/google/android/gms/common/internal/g;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-virtual {v3}, Lcom/google/android/gms/common/internal/e;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->d(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/e$f;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/common/internal/g;->b(Ljava/lang/String;Lcom/google/android/gms/common/internal/e$f;)V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->f(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/e$f;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;I)V

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;Landroid/os/IInterface;)Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;)Lcom/google/android/gms/common/internal/f;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/ConnectionResult;

    const/16 v3, 0x8

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/common/ConnectionResult;-><init>(ILandroid/app/PendingIntent;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/internal/f;->a(Lcom/google/android/gms/common/ConnectionResult;)V

    goto/16 :goto_0

    :cond_5
    move v3, v0

    goto/16 :goto_2

    :cond_6
    :try_start_5
    iget-object v0, v4, Lcom/google/android/gms/common/internal/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    iput-boolean v0, v4, Lcom/google/android/gms/common/internal/f;->d:Z

    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/e$h;->e:Lcom/google/android/gms/common/internal/e;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/e;->a(Lcom/google/android/gms/common/internal/e;I)V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "A fatal developer error has occurred. Check the logs for further information."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move-object v0, v1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method
