.class Lcom/google/android/gms/internal/an$2$1;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/webkit/ValueCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/webkit/ValueCallback",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/an$2;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/an$2;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/an$2$1;->a:Lcom/google/android/gms/internal/an$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic onReceiveValue(Ljava/lang/Object;)V
    .locals 5

    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/an$2$1;->a:Lcom/google/android/gms/internal/an$2;

    iget-object v1, v0, Lcom/google/android/gms/internal/an$2;->d:Lcom/google/android/gms/internal/an;

    iget-object v0, p0, Lcom/google/android/gms/internal/an$2$1;->a:Lcom/google/android/gms/internal/an$2;

    iget-object v0, v0, Lcom/google/android/gms/internal/an$2;->b:Lcom/google/android/gms/internal/ak;

    iget-object v2, p0, Lcom/google/android/gms/internal/an$2$1;->a:Lcom/google/android/gms/internal/an$2;

    iget-object v2, v2, Lcom/google/android/gms/internal/an$2;->c:Landroid/webkit/WebView;

    iget-object v3, v0, Lcom/google/android/gms/internal/ak;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget v4, v0, Lcom/google/android/gms/internal/ak;->d:I

    add-int/lit8 v4, v4, -0x1

    iput v4, v0, Lcom/google/android/gms/internal/ak;->d:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "text"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\n"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ak;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/internal/ak;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/android/gms/internal/an;->c:Lcom/google/android/gms/internal/al;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/internal/al;->b(Lcom/google/android/gms/internal/ak;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    :try_start_2
    invoke-virtual {v0, v3}, Lcom/google/android/gms/internal/ak;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/internal/gs;->b()V

    iget-object v1, v1, Lcom/google/android/gms/internal/an;->d:Lcom/google/android/gms/internal/ey;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/ey;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
