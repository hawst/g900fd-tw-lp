.class public final Lcom/google/android/gms/internal/fz$a;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/internal/fi;

.field public final b:Lcom/google/android/gms/internal/fk;

.field public final c:Lcom/google/android/gms/internal/cm;

.field public final d:Lcom/google/android/gms/internal/ay;

.field public final e:I

.field public final f:J

.field public final g:J

.field public final h:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/fi;Lcom/google/android/gms/internal/fk;Lcom/google/android/gms/internal/cm;Lcom/google/android/gms/internal/ay;IJJLorg/json/JSONObject;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/fz$a;->a:Lcom/google/android/gms/internal/fi;

    iput-object p2, p0, Lcom/google/android/gms/internal/fz$a;->b:Lcom/google/android/gms/internal/fk;

    iput-object p3, p0, Lcom/google/android/gms/internal/fz$a;->c:Lcom/google/android/gms/internal/cm;

    iput-object p4, p0, Lcom/google/android/gms/internal/fz$a;->d:Lcom/google/android/gms/internal/ay;

    iput p5, p0, Lcom/google/android/gms/internal/fz$a;->e:I

    iput-wide p6, p0, Lcom/google/android/gms/internal/fz$a;->f:J

    iput-wide p8, p0, Lcom/google/android/gms/internal/fz$a;->g:J

    iput-object p10, p0, Lcom/google/android/gms/internal/fz$a;->h:Lorg/json/JSONObject;

    return-void
.end method
