.class public final Lcom/google/android/gms/internal/fr;
.super Lcom/google/android/gms/internal/fm$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/internal/fr;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/internal/fx;

.field private final e:Lcom/google/android/gms/internal/ci;

.field private final f:Lcom/google/android/gms/internal/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/fr;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/bm;Lcom/google/android/gms/internal/ci;Lcom/google/android/gms/internal/fx;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/fm$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/fr;->c:Landroid/content/Context;

    iput-object p4, p0, Lcom/google/android/gms/internal/fr;->d:Lcom/google/android/gms/internal/fx;

    iput-object p3, p0, Lcom/google/android/gms/internal/fr;->e:Lcom/google/android/gms/internal/ci;

    iput-object p2, p0, Lcom/google/android/gms/internal/fr;->f:Lcom/google/android/gms/internal/bm;

    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/internal/bm;Lcom/google/android/gms/internal/fi;)Lcom/google/android/gms/internal/fk;
    .locals 8

    const/4 v7, 0x0

    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    new-instance v0, Lcom/google/android/gms/internal/fw;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/fw;-><init>(Landroid/content/Context;)V

    iget v1, v0, Lcom/google/android/gms/internal/fw;->l:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    new-instance v0, Lcom/google/android/gms/internal/fk;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Lcom/google/android/gms/internal/ft;

    iget-object v1, p2, Lcom/google/android/gms/internal/fi;->f:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {v3, v1}, Lcom/google/android/gms/internal/ft;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, Lcom/google/android/gms/internal/fi;->c:Lcom/google/android/gms/internal/av;

    iget-object v1, v1, Lcom/google/android/gms/internal/av;->c:Landroid/os/Bundle;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/google/android/gms/internal/fi;->c:Lcom/google/android/gms/internal/av;

    iget-object v1, v1, Lcom/google/android/gms/internal/av;->c:Landroid/os/Bundle;

    const-string v2, "_ad"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {p0, p2, v1}, Lcom/google/android/gms/internal/fs;->a(Landroid/content/Context;Lcom/google/android/gms/internal/fi;Ljava/lang/String;)Lcom/google/android/gms/internal/fk;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v5, p1, Lcom/google/android/gms/internal/bm;->a:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/internal/bm;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/internal/bm;->c:Ljava/lang/String;

    invoke-static {p2, v0, v1, v2}, Lcom/google/android/gms/internal/fs;->a(Lcom/google/android/gms/internal/fi;Lcom/google/android/gms/internal/fw;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/internal/fk;

    invoke-direct {v0, v7}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/google/android/gms/internal/fr$2;

    invoke-direct {v4, v0}, Lcom/google/android/gms/internal/fr$2;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/gms/internal/gr;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/internal/fr$1;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/internal/fr$1;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/fi;Lcom/google/android/gms/internal/ft;Lcom/google/android/gms/internal/gw$a;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :try_start_0
    iget-object v0, v3, Lcom/google/android/gms/internal/ft;->b:Lcom/google/android/gms/internal/gk;

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/fv;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/internal/fk;

    invoke-direct {v0, v7}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/internal/fk;

    invoke-direct {v0, v7}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    goto :goto_0

    :cond_3
    iget v1, v0, Lcom/google/android/gms/internal/fv;->f:I

    const/4 v2, -0x2

    if-eq v1, v2, :cond_4

    new-instance v1, Lcom/google/android/gms/internal/fk;

    iget v0, v0, Lcom/google/android/gms/internal/fv;->f:I

    invoke-direct {v1, v0}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    move-object v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v1, v0, Lcom/google/android/gms/internal/fv;->d:Z

    if-eqz v1, :cond_5

    iget-object v1, p2, Lcom/google/android/gms/internal/fi;->g:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    :cond_5
    iget-object v1, p2, Lcom/google/android/gms/internal/fi;->k:Lcom/google/android/gms/internal/gt;

    iget-object v1, v1, Lcom/google/android/gms/internal/gt;->b:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/internal/fv;->e:Ljava/lang/String;

    invoke-static {p0, v1, v2, v0}, Lcom/google/android/gms/internal/fr;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/fv;)Lcom/google/android/gms/internal/fk;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/fv;)Lcom/google/android/gms/internal/fk;
    .locals 24

    :try_start_0
    new-instance v23, Lcom/google/android/gms/internal/fu;

    invoke-direct/range {v23 .. v23}, Lcom/google/android/gms/internal/fu;-><init>()V

    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    new-instance v3, Ljava/net/URL;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    move-object v4, v3

    move v3, v2

    :goto_0
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v2}, Lcom/google/android/gms/internal/gj;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;)V

    const/4 v5, 0x0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "x-afma-drt-cookie"

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p3, :cond_1

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/gms/internal/fv;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/android/gms/internal/fv;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    array-length v6, v5

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    new-instance v6, Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v6, v5}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V

    :cond_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v6

    const/16 v7, 0xc8

    if-lt v5, v7, :cond_2

    const/16 v7, 0x12c

    if-ge v5, v7, :cond_2

    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v4}, Lcom/google/android/gms/internal/gj;->a(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v6, v4, v5}, Lcom/google/android/gms/internal/fr;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V

    move-object/from16 v0, v23

    iput-object v3, v0, Lcom/google/android/gms/internal/fu;->b:Ljava/lang/String;

    move-object/from16 v0, v23

    iput-object v4, v0, Lcom/google/android/gms/internal/fu;->c:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/google/android/gms/internal/fu;->a(Ljava/util/Map;)V

    new-instance v3, Lcom/google/android/gms/internal/fk;

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/gms/internal/fu;->b:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/android/gms/internal/fu;->c:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/google/android/gms/internal/fu;->d:Ljava/util/List;

    move-object/from16 v0, v23

    iget-object v7, v0, Lcom/google/android/gms/internal/fu;->g:Ljava/util/List;

    move-object/from16 v0, v23

    iget-wide v8, v0, Lcom/google/android/gms/internal/fu;->h:J

    move-object/from16 v0, v23

    iget-boolean v10, v0, Lcom/google/android/gms/internal/fu;->i:Z

    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/google/android/gms/internal/fu;->j:Ljava/util/List;

    move-object/from16 v0, v23

    iget-wide v12, v0, Lcom/google/android/gms/internal/fu;->k:J

    move-object/from16 v0, v23

    iget v14, v0, Lcom/google/android/gms/internal/fu;->l:I

    move-object/from16 v0, v23

    iget-object v15, v0, Lcom/google/android/gms/internal/fu;->a:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/gms/internal/fu;->e:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/gms/internal/fu;->f:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fu;->m:Z

    move/from16 v20, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fu;->n:Z

    move/from16 v21, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fu;->o:Z

    move/from16 v22, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fu;->p:Z

    move/from16 v23, v0

    invoke-direct/range {v3 .. v23}, Lcom/google/android/gms/internal/fk;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JZLjava/util/List;JILjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_1
    return-object v3

    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    invoke-static {v4, v6, v7, v5}, Lcom/google/android/gms/internal/fr;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V

    const/16 v4, 0x12c

    if-lt v5, v4, :cond_4

    const/16 v4, 0x190

    if-ge v5, v4, :cond_4

    const-string v4, "Location"

    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v3, "No location header to follow redirect."

    invoke-static {v3}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/internal/fk;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/fk;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while connecting to ad server: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/internal/fk;

    const/4 v2, 0x2

    invoke-direct {v3, v2}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    goto :goto_1

    :cond_3
    :try_start_5
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    add-int/lit8 v3, v3, 0x1

    const/4 v5, 0x5

    if-le v3, v5, :cond_5

    const-string v3, "Too many redirects."

    invoke-static {v3}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/internal/fk;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/fk;-><init>(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    :cond_4
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received error HTTP response code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/internal/fk;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/internal/fk;-><init>(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_1

    :cond_5
    :try_start_9
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/google/android/gms/internal/fu;->a(Ljava/util/Map;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    :try_start_a
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v3
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/bm;Lcom/google/android/gms/internal/ci;Lcom/google/android/gms/internal/fx;)Lcom/google/android/gms/internal/fr;
    .locals 3

    sget-object v1, Lcom/google/android/gms/internal/fr;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/internal/fr;->b:Lcom/google/android/gms/internal/fr;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fr;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p1, p2, p3}, Lcom/google/android/gms/internal/fr;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/bm;Lcom/google/android/gms/internal/ci;Lcom/google/android/gms/internal/fx;)V

    sput-object v0, Lcom/google/android/gms/internal/fr;->b:Lcom/google/android/gms/internal/fr;

    :cond_0
    sget-object v0, Lcom/google/android/gms/internal/fr;->b:Lcom/google/android/gms/internal/fr;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/internal/gs;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Http Response: {\n  URL:\n    "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n  Headers:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    const v2, 0x186a0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit16 v2, v0, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    add-int/lit16 v0, v0, 0x3e8

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Response Code:\n    "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/internal/gs;->c()V

    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/fi;)Lcom/google/android/gms/internal/fk;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/fr;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/internal/fr;->f:Lcom/google/android/gms/internal/bm;

    iget-object v2, p0, Lcom/google/android/gms/internal/fr;->e:Lcom/google/android/gms/internal/ci;

    iget-object v2, p0, Lcom/google/android/gms/internal/fr;->d:Lcom/google/android/gms/internal/fx;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/internal/fr;->a(Landroid/content/Context;Lcom/google/android/gms/internal/bm;Lcom/google/android/gms/internal/fi;)Lcom/google/android/gms/internal/fk;

    move-result-object v0

    return-object v0
.end method
