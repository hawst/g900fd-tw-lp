.class public Lcom/google/android/gms/internal/kf;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lcom/google/android/gms/internal/kf$a;

.field public static final b:Lcom/google/android/gms/internal/kf$b;

.field public static final c:Lcom/google/android/gms/internal/kf$d;

.field public static final d:Lcom/google/android/gms/internal/kf$c;

.field public static final e:Lcom/google/android/gms/internal/kf$e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/kf$a;

    const-string v1, "created"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kf$a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kf;->a:Lcom/google/android/gms/internal/kf$a;

    new-instance v0, Lcom/google/android/gms/internal/kf$b;

    const-string v1, "lastOpenedTime"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kf$b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kf;->b:Lcom/google/android/gms/internal/kf$b;

    new-instance v0, Lcom/google/android/gms/internal/kf$d;

    const-string v1, "modified"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kf$d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kf;->c:Lcom/google/android/gms/internal/kf$d;

    new-instance v0, Lcom/google/android/gms/internal/kf$c;

    const-string v1, "modifiedByMe"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kf$c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kf;->d:Lcom/google/android/gms/internal/kf$c;

    new-instance v0, Lcom/google/android/gms/internal/kf$e;

    const-string v1, "sharedWithMe"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kf$e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kf;->e:Lcom/google/android/gms/internal/kf$e;

    return-void
.end method
