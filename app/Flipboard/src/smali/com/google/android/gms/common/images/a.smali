.class public abstract Lcom/google/android/gms/common/images/a;
.super Ljava/lang/Object;


# instance fields
.field final a:Lcom/google/android/gms/common/images/a$a;

.field protected b:I

.field protected c:Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;

.field protected d:I


# virtual methods
.method protected abstract a()V
.end method

.method final a(Landroid/content/Context;Landroid/graphics/Bitmap;)V
    .locals 2

    invoke-static {p2}, Lcom/google/android/gms/common/internal/a;->a(Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/gms/common/images/a;->d:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/google/android/gms/internal/ix;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p2

    :cond_0
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;

    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->a:Lcom/google/android/gms/common/images/a$a;

    iget-object v0, v0, Lcom/google/android/gms/common/images/a$a;->a:Landroid/net/Uri;

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/a;->a()V

    return-void
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/internal/iz;)V
    .locals 4

    iget v0, p0, Lcom/google/android/gms/common/images/a;->b:I

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/google/android/gms/common/images/a;->b:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, p0, Lcom/google/android/gms/common/images/a;->d:I

    if-lez v0, :cond_3

    new-instance v3, Lcom/google/android/gms/internal/iz$a;

    iget v0, p0, Lcom/google/android/gms/common/images/a;->d:I

    invoke-direct {v3, v1, v0}, Lcom/google/android/gms/internal/iz$a;-><init>(II)V

    invoke-virtual {p2, v3}, Lcom/google/android/gms/internal/iz;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/common/images/a;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    invoke-static {v2, v0}, Lcom/google/android/gms/internal/ix;->a(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_0
    invoke-virtual {p2, v3, v0}, Lcom/google/android/gms/internal/iz;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/ImageManager$OnImageLoadedListener;

    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->a:Lcom/google/android/gms/common/images/a$a;

    iget-object v0, v0, Lcom/google/android/gms/common/images/a$a;->a:Landroid/net/Uri;

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/a;->a()V

    return-void

    :cond_3
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
