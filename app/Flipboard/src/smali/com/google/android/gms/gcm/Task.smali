.class public abstract Lcom/google/android/gms/gcm/Task;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Z


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/gcm/Task;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/gcm/Task;->b:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/Task;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/gcm/Task;->d:Z

    return-void
.end method
