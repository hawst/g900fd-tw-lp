.class public final Lcom/google/android/gms/internal/ft;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field a:Lcom/google/android/gms/internal/gv;

.field b:Lcom/google/android/gms/internal/gk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/internal/gk",
            "<",
            "Lcom/google/android/gms/internal/fv;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/google/android/gms/internal/by;

.field public final d:Lcom/google/android/gms/internal/by;

.field private final e:Ljava/lang/Object;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ft;->e:Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/internal/gk;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gk;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/ft;->b:Lcom/google/android/gms/internal/gk;

    new-instance v0, Lcom/google/android/gms/internal/ft$1;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/ft$1;-><init>(Lcom/google/android/gms/internal/ft;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ft;->c:Lcom/google/android/gms/internal/by;

    new-instance v0, Lcom/google/android/gms/internal/ft$2;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/ft$2;-><init>(Lcom/google/android/gms/internal/ft;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ft;->d:Lcom/google/android/gms/internal/by;

    iput-object p1, p0, Lcom/google/android/gms/internal/ft;->f:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/ft;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ft;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/ft;)Lcom/google/android/gms/internal/gk;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ft;->b:Lcom/google/android/gms/internal/gk;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/ft;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ft;->f:Ljava/lang/String;

    return-object v0
.end method
