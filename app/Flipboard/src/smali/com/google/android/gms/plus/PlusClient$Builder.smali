.class public Lcom/google/android/gms/plus/PlusClient$Builder;
.super Ljava/lang/Object;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

.field private final c:Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

.field private final d:Lcom/google/android/gms/plus/internal/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->b:Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iput-object p3, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->c:Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    new-instance v0, Lcom/google/android/gms/plus/internal/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/internal/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->d:Lcom/google/android/gms/plus/internal/i;

    return-void
.end method


# virtual methods
.method public final varargs a([Ljava/lang/String;)Lcom/google/android/gms/plus/PlusClient$Builder;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->d:Lcom/google/android/gms/plus/internal/i;

    iget-object v1, v0, Lcom/google/android/gms/plus/internal/i;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/i;->b:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public final a()Lcom/google/android/gms/plus/PlusClient;
    .locals 14

    new-instance v9, Lcom/google/android/gms/plus/PlusClient;

    new-instance v10, Lcom/google/android/gms/plus/internal/e;

    iget-object v11, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->a:Landroid/content/Context;

    iget-object v12, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->b:Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;

    iget-object v13, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->c:Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;

    iget-object v8, p0, Lcom/google/android/gms/plus/PlusClient$Builder;->d:Lcom/google/android/gms/plus/internal/i;

    iget-object v0, v8, Lcom/google/android/gms/plus/internal/i;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "<<default account>>"

    iput-object v0, v8, Lcom/google/android/gms/plus/internal/i;->a:Ljava/lang/String;

    :cond_0
    iget-object v0, v8, Lcom/google/android/gms/plus/internal/i;->b:Ljava/util/ArrayList;

    iget-object v1, v8, Lcom/google/android/gms/plus/internal/i;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/plus/internal/h;

    iget-object v1, v8, Lcom/google/android/gms/plus/internal/i;->a:Ljava/lang/String;

    iget-object v3, v8, Lcom/google/android/gms/plus/internal/i;->c:[Ljava/lang/String;

    iget-object v4, v8, Lcom/google/android/gms/plus/internal/i;->f:[Ljava/lang/String;

    iget-object v5, v8, Lcom/google/android/gms/plus/internal/i;->d:Ljava/lang/String;

    iget-object v6, v8, Lcom/google/android/gms/plus/internal/i;->e:Ljava/lang/String;

    iget-object v7, v8, Lcom/google/android/gms/plus/internal/i;->g:Ljava/lang/String;

    iget-object v8, v8, Lcom/google/android/gms/plus/internal/i;->h:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/h;-><init>(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V

    invoke-direct {v10, v11, v12, v13, v0}, Lcom/google/android/gms/plus/internal/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/GooglePlayServicesClient$ConnectionCallbacks;Lcom/google/android/gms/common/GooglePlayServicesClient$OnConnectionFailedListener;Lcom/google/android/gms/plus/internal/h;)V

    invoke-direct {v9, v10}, Lcom/google/android/gms/plus/PlusClient;-><init>(Lcom/google/android/gms/plus/internal/e;)V

    return-object v9
.end method
