.class public final Lcom/google/android/gms/internal/ew;
.super Lcom/google/android/gms/internal/et$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/doubleclick/b;

.field private final b:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/doubleclick/b;Lcom/google/android/gms/ads/doubleclick/PublisherAdView;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/et$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ew;->a:Lcom/google/android/gms/ads/doubleclick/b;

    iput-object p2, p0, Lcom/google/android/gms/internal/ew;->b:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/es;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ew;->a:Lcom/google/android/gms/ads/doubleclick/b;

    iget-object v0, p0, Lcom/google/android/gms/internal/ew;->b:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    new-instance v0, Lcom/google/android/gms/internal/ev;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/ev;-><init>(Lcom/google/android/gms/internal/es;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/ew;->a:Lcom/google/android/gms/ads/doubleclick/b;

    iget-object v1, p0, Lcom/google/android/gms/internal/ew;->b:Lcom/google/android/gms/ads/doubleclick/PublisherAdView;

    invoke-interface {v0}, Lcom/google/android/gms/ads/doubleclick/b;->a()Z

    move-result v0

    return v0
.end method
