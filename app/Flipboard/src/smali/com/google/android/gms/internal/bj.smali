.class public final Lcom/google/android/gms/internal/bj;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bk;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field public final i:I

.field public final j:Ljava/lang/String;

.field public final k:I

.field public final l:Ljava/lang/String;

.field public final m:I

.field public final n:I

.field public final o:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bk;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bk;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/bj;->CREATOR:Lcom/google/android/gms/internal/bk;

    return-void
.end method

.method constructor <init>(IIIIIIIIILjava/lang/String;ILjava/lang/String;IILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/bj;->a:I

    iput p2, p0, Lcom/google/android/gms/internal/bj;->b:I

    iput p3, p0, Lcom/google/android/gms/internal/bj;->c:I

    iput p4, p0, Lcom/google/android/gms/internal/bj;->d:I

    iput p5, p0, Lcom/google/android/gms/internal/bj;->e:I

    iput p6, p0, Lcom/google/android/gms/internal/bj;->f:I

    iput p7, p0, Lcom/google/android/gms/internal/bj;->g:I

    iput p8, p0, Lcom/google/android/gms/internal/bj;->h:I

    iput p9, p0, Lcom/google/android/gms/internal/bj;->i:I

    iput-object p10, p0, Lcom/google/android/gms/internal/bj;->j:Ljava/lang/String;

    iput p11, p0, Lcom/google/android/gms/internal/bj;->k:I

    iput-object p12, p0, Lcom/google/android/gms/internal/bj;->l:Ljava/lang/String;

    iput p13, p0, Lcom/google/android/gms/internal/bj;->m:I

    iput p14, p0, Lcom/google/android/gms/internal/bj;->n:I

    iput-object p15, p0, Lcom/google/android/gms/internal/bj;->o:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/search/SearchAdRequest;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bj;->a:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->b:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->b:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->c:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->c:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->d:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->d:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->e:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->e:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->f:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->f:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->g:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->g:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->h:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->h:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->i:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->i:I

    iget-object v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->j:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bj;->j:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->k:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->k:I

    iget-object v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bj;->l:Ljava/lang/String;

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->m:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->m:I

    iget v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->n:I

    iput v0, p0, Lcom/google/android/gms/internal/bj;->n:I

    iget-object v0, p1, Lcom/google/android/gms/ads/search/SearchAdRequest;->o:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/internal/bj;->o:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/google/android/gms/internal/bk;->a(Lcom/google/android/gms/internal/bj;Landroid/os/Parcel;)V

    return-void
.end method
