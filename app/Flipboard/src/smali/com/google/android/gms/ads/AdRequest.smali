.class public final Lcom/google/android/gms/ads/AdRequest;
.super Ljava/lang/Object;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/gms/internal/bg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/bg;->a:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/ads/AdRequest;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/ads/AdRequest$Builder;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/bg;

    invoke-static {p1}, Lcom/google/android/gms/ads/AdRequest$Builder;->a(Lcom/google/android/gms/ads/AdRequest$Builder;)Lcom/google/android/gms/internal/bg$a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/bg;-><init>(Lcom/google/android/gms/internal/bg$a;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/AdRequest;->b:Lcom/google/android/gms/internal/bg;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/ads/AdRequest$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/AdRequest;-><init>(Lcom/google/android/gms/ads/AdRequest$Builder;)V

    return-void
.end method
