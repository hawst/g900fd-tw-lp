.class public Lcom/google/android/gms/internal/he;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/hf;


# instance fields
.field final a:I

.field final b:[Lcom/google/android/gms/internal/hi;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/hf;

    invoke-direct {v0}, Lcom/google/android/gms/internal/hf;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/he;->CREATOR:Lcom/google/android/gms/internal/hf;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/internal/hi;Ljava/lang/String;ZLandroid/accounts/Account;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/he;->a:I

    iput-object p2, p0, Lcom/google/android/gms/internal/he;->b:[Lcom/google/android/gms/internal/hi;

    iput-object p3, p0, Lcom/google/android/gms/internal/he;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/google/android/gms/internal/he;->d:Z

    iput-object p5, p0, Lcom/google/android/gms/internal/he;->e:Landroid/accounts/Account;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/he;->CREATOR:Lcom/google/android/gms/internal/hf;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/he;->CREATOR:Lcom/google/android/gms/internal/hf;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/hf;->a(Lcom/google/android/gms/internal/he;Landroid/os/Parcel;I)V

    return-void
.end method
