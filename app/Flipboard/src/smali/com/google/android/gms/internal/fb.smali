.class public Lcom/google/android/gms/internal/fb;
.super Lcom/google/android/gms/internal/gg;

# interfaces
.implements Lcom/google/android/gms/internal/ff$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/fa$a;

.field private final b:Ljava/lang/Object;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/Object;

.field private final e:Lcom/google/android/gms/internal/fi$a;

.field private final f:Lcom/google/android/gms/internal/k;

.field private g:Lcom/google/android/gms/internal/gg;

.field private h:Lcom/google/android/gms/internal/fk;

.field private i:Lcom/google/android/gms/internal/cm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/fi$a;Lcom/google/android/gms/internal/k;Lcom/google/android/gms/internal/fa$a;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/gg;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->b:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->d:Ljava/lang/Object;

    iput-object p4, p0, Lcom/google/android/gms/internal/fb;->a:Lcom/google/android/gms/internal/fa$a;

    iput-object p1, p0, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/fb;->e:Lcom/google/android/gms/internal/fi$a;

    iput-object p3, p0, Lcom/google/android/gms/internal/fb;->f:Lcom/google/android/gms/internal/k;

    return-void
.end method

.method private a(Lcom/google/android/gms/internal/fi;)Lcom/google/android/gms/internal/ay;
    .locals 11

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v0, v0, Lcom/google/android/gms/internal/fk;->m:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    const-string v1, "The ad response must specify one of the supported ad sizes."

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v0, v0, Lcom/google/android/gms/internal/fk;->m:Ljava/lang/String;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse the ad size from the ad response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v2, v2, Lcom/google/android/gms/internal/fk;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    iget-object v0, p1, Lcom/google/android/gms/internal/fi;->d:Lcom/google/android/gms/internal/ay;

    iget-object v6, v0, Lcom/google/android/gms/internal/ay;->h:[Lcom/google/android/gms/internal/ay;

    array-length v7, v6

    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_5

    aget-object v8, v6, v2

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iget v0, v8, Lcom/google/android/gms/internal/ay;->f:I

    const/4 v9, -0x1

    if-ne v0, v9, :cond_2

    iget v0, v8, Lcom/google/android/gms/internal/ay;->g:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    iget v9, v8, Lcom/google/android/gms/internal/ay;->c:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_3

    iget v9, v8, Lcom/google/android/gms/internal/ay;->d:I

    int-to-float v9, v9

    div-float v1, v9, v1

    float-to-int v1, v1

    :goto_2
    if-ne v4, v0, :cond_4

    if-ne v5, v1, :cond_4

    new-instance v0, Lcom/google/android/gms/internal/ay;

    iget-object v1, p1, Lcom/google/android/gms/internal/fi;->d:Lcom/google/android/gms/internal/ay;

    iget-object v1, v1, Lcom/google/android/gms/internal/ay;->h:[Lcom/google/android/gms/internal/ay;

    invoke-direct {v0, v8, v1}, Lcom/google/android/gms/internal/ay;-><init>(Lcom/google/android/gms/internal/ay;[Lcom/google/android/gms/internal/ay;)V

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse the ad size from the ad response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v2, v2, Lcom/google/android/gms/internal/fk;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_2
    iget v0, v8, Lcom/google/android/gms/internal/ay;->f:I

    goto :goto_1

    :cond_3
    iget v1, v8, Lcom/google/android/gms/internal/ay;->c:I

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The ad size from the ad response was not one of the requested sizes: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v2, v2, Lcom/google/android/gms/internal/fk;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/fb;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->d:Ljava/lang/Object;

    return-object v0
.end method

.method private a(J)Z
    .locals 5

    const-wide/32 v0, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/fb;->d:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    const-string v1, "Ad request cancelled."

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/fb;)Lcom/google/android/gms/internal/fa$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->a:Lcom/google/android/gms/internal/fa$a;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 12

    const/4 v10, 0x3

    const/4 v5, -0x2

    const/4 v9, -0x3

    const/4 v8, 0x0

    iget-object v11, p0, Lcom/google/android/gms/internal/fb;->d:Ljava/lang/Object;

    monitor-enter v11

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->f:Lcom/google/android/gms/internal/k;

    iget-object v0, v0, Lcom/google/android/gms/internal/k;->a:Lcom/google/android/gms/internal/g;

    iget-object v1, p0, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/g;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/fi;

    iget-object v2, p0, Lcom/google/android/gms/internal/fb;->e:Lcom/google/android/gms/internal/fi$a;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/internal/fi;-><init>(Lcom/google/android/gms/internal/fi$a;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const-wide/16 v2, -0x1

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    invoke-static {v0, v1, p0}, Lcom/google/android/gms/internal/ff;->a(Landroid/content/Context;Lcom/google/android/gms/internal/fi;Lcom/google/android/gms/internal/ff$a;)Lcom/google/android/gms/internal/gg;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/internal/fb;->b:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->g:Lcom/google/android/gms/internal/gg;

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->g:Lcom/google/android/gms/internal/gg;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    const-string v5, "Could not start the ad request service."

    const/4 v6, 0x0

    invoke-direct {v0, v5, v6}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4

    throw v0
    :try_end_3
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    move-object v4, v8

    :goto_0
    :try_start_4
    iget v5, v0, Lcom/google/android/gms/internal/fb$a;->a:I

    if-eq v5, v10, :cond_0

    const/4 v6, -0x1

    if-ne v5, v6, :cond_8

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/internal/fb$a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/gs;->b(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/gms/internal/fk;

    invoke-direct {v0, v5}, Lcom/google/android/gms/internal/fk;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    :goto_2
    sget-object v0, Lcom/google/android/gms/internal/gr;->a:Landroid/os/Handler;

    new-instance v6, Lcom/google/android/gms/internal/fb$1;

    invoke-direct {v6, p0}, Lcom/google/android/gms/internal/fb$1;-><init>(Lcom/google/android/gms/internal/fb;)V

    invoke-virtual {v0, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-wide v6, v2

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v0, v0, Lcom/google/android/gms/internal/fk;->r:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    if-nez v0, :cond_a

    :try_start_5
    new-instance v10, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v0, v0, Lcom/google/android/gms/internal/fk;->r:Ljava/lang/String;

    invoke-direct {v10, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :goto_4
    :try_start_6
    new-instance v0, Lcom/google/android/gms/internal/fz$a;

    iget-object v2, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v3, p0, Lcom/google/android/gms/internal/fb;->i:Lcom/google/android/gms/internal/cm;

    iget-object v8, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-wide v8, v8, Lcom/google/android/gms/internal/fk;->n:J

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/internal/fz$a;-><init>(Lcom/google/android/gms/internal/fi;Lcom/google/android/gms/internal/fk;Lcom/google/android/gms/internal/cm;Lcom/google/android/gms/internal/ay;IJJLorg/json/JSONObject;)V

    sget-object v1, Lcom/google/android/gms/internal/gr;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/internal/fb$2;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/internal/fb$2;-><init>(Lcom/google/android/gms/internal/fb;Lcom/google/android/gms/internal/fz$a;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v11
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    return-void

    :cond_1
    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_2
    :try_start_8
    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/internal/fb;->a(J)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    const-string v4, "Timed out waiting for ad response."

    const/4 v5, 0x2

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_8
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit v11

    throw v0

    :cond_3
    :try_start_9
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    if-eqz v0, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/internal/fb;->b:Ljava/lang/Object;

    monitor-enter v4
    :try_end_9
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    const/4 v0, 0x0

    :try_start_a
    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->g:Lcom/google/android/gms/internal/gg;

    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget v0, v0, Lcom/google/android/gms/internal/fk;->e:I

    if-eq v0, v5, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget v0, v0, Lcom/google/android/gms/internal/fk;->e:I

    if-eq v0, v9, :cond_4

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "There was a problem getting an ad response. ErrorCode: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget v5, v5, Lcom/google/android/gms/internal/fk;->e:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget v5, v5, Lcom/google/android/gms/internal/fk;->e:I

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :catchall_2
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget v0, v0, Lcom/google/android/gms/internal/fk;->e:I

    if-eq v0, v9, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v0, v0, Lcom/google/android/gms/internal/fk;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    const-string v4, "No fill from ad server."

    const/4 v5, 0x3

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-boolean v4, v4, Lcom/google/android/gms/internal/fk;->u:Z

    invoke-static {v0, v4}, Lcom/google/android/gms/internal/gb;->a(Landroid/content/Context;Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fk;->h:Z
    :try_end_b
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    if-eqz v0, :cond_6

    :try_start_c
    new-instance v0, Lcom/google/android/gms/internal/cm;

    iget-object v4, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v4, v4, Lcom/google/android/gms/internal/fk;->c:Ljava/lang/String;

    invoke-direct {v0, v4}, Lcom/google/android/gms/internal/cm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->i:Lcom/google/android/gms/internal/cm;
    :try_end_c
    .catch Lorg/json/JSONException; {:try_start_c .. :try_end_c} :catch_1
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :cond_6
    :try_start_d
    iget-object v0, v1, Lcom/google/android/gms/internal/fi;->d:Lcom/google/android/gms/internal/ay;

    iget-object v0, v0, Lcom/google/android/gms/internal/ay;->h:[Lcom/google/android/gms/internal/ay;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/google/android/gms/internal/fb;->a(Lcom/google/android/gms/internal/fi;)Lcom/google/android/gms/internal/ay;
    :try_end_d
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    move-result-object v4

    :goto_5
    :try_start_e
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/fk;->v:Z

    invoke-static {}, Lcom/google/android/gms/internal/gb;->a()Lcom/google/android/gms/internal/gb;

    move-result-object v6

    iget-object v7, v6, Lcom/google/android/gms/internal/gb;->c:Ljava/lang/Object;

    monitor-enter v7
    :try_end_e
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_e .. :try_end_e} :catch_2
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    :try_start_f
    iput-boolean v0, v6, Lcom/google/android/gms/internal/gb;->e:Z

    monitor-exit v7
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    :try_start_10
    invoke-static {}, Lcom/google/android/gms/internal/gb;->a()Lcom/google/android/gms/internal/gb;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/internal/fb;->c:Landroid/content/Context;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/internal/gb;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/an;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/internal/an;->isAlive()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/an;->a()V
    :try_end_10
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_10 .. :try_end_10} :catch_2
    .catchall {:try_start_10 .. :try_end_10} :catchall_1

    :cond_7
    move-wide v6, v2

    goto/16 :goto_3

    :catch_1
    move-exception v0

    :try_start_11
    new-instance v0, Lcom/google/android/gms/internal/fb$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not parse mediation config: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v5, v5, Lcom/google/android/gms/internal/fk;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/fb$a;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_11
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    :catchall_3
    move-exception v0

    :try_start_12
    monitor-exit v7

    throw v0
    :try_end_12
    .catch Lcom/google/android/gms/internal/fb$a; {:try_start_12 .. :try_end_12} :catch_2
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    :catch_2
    move-exception v0

    goto/16 :goto_0

    :cond_8
    :try_start_13
    invoke-virtual {v0}, Lcom/google/android/gms/internal/fb$a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    new-instance v0, Lcom/google/android/gms/internal/fk;

    iget-object v6, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-wide v6, v6, Lcom/google/android/gms/internal/fk;->k:J

    invoke-direct {v0, v5, v6, v7}, Lcom/google/android/gms/internal/fk;-><init>(IJ)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    goto/16 :goto_2

    :catch_3
    move-exception v0

    const-string v2, "Error parsing the JSON for Active View."

    invoke-static {v2, v0}, Lcom/google/android/gms/internal/gs;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    :cond_a
    move-object v10, v8

    goto/16 :goto_4

    :cond_b
    move-object v4, v8

    goto :goto_5
.end method

.method public final a(Lcom/google/android/gms/internal/fk;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/fb;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    iput-object p1, p0, Lcom/google/android/gms/internal/fb;->h:Lcom/google/android/gms/internal/fk;

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i_()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/fb;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->g:Lcom/google/android/gms/internal/gg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/fb;->g:Lcom/google/android/gms/internal/gg;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gg;->cancel()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
