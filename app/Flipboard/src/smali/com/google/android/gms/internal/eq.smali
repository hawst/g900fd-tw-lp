.class public final Lcom/google/android/gms/internal/eq;
.super Lcom/google/android/gms/internal/el$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/el$a;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/eq;->a:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/internal/ek;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eq;->a:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    new-instance v0, Lcom/google/android/gms/internal/eo;

    invoke-direct {v0, p1}, Lcom/google/android/gms/internal/eo;-><init>(Lcom/google/android/gms/internal/ek;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/eq;->a:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

    invoke-interface {v0}, Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;->a()Z

    move-result v0

    return v0
.end method
