.class public final Lcom/google/android/gms/R$string;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final accept:I = 0x7f0d000a

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f0d0070

.field public static final common_android_wear_update_text:I = 0x7f0d0071

.field public static final common_android_wear_update_title:I = 0x7f0d0072

.field public static final common_google_play_services_enable_button:I = 0x7f0d0073

.field public static final common_google_play_services_enable_text:I = 0x7f0d0074

.field public static final common_google_play_services_enable_title:I = 0x7f0d0075

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f0d0076

.field public static final common_google_play_services_install_button:I = 0x7f0d0077

.field public static final common_google_play_services_install_text_phone:I = 0x7f0d0078

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0d0079

.field public static final common_google_play_services_install_title:I = 0x7f0d007a

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0d007b

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0d007c

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f0d007d

.field public static final common_google_play_services_network_error_text:I = 0x7f0d007e

.field public static final common_google_play_services_network_error_title:I = 0x7f0d007f

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f0d0080

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f0d0081

.field public static final common_google_play_services_notification_ticker:I = 0x7f0d0082

.field public static final common_google_play_services_unknown_issue:I = 0x7f0d0083

.field public static final common_google_play_services_unsupported_text:I = 0x7f0d0084

.field public static final common_google_play_services_unsupported_title:I = 0x7f0d0085

.field public static final common_google_play_services_update_button:I = 0x7f0d0086

.field public static final common_google_play_services_update_text:I = 0x7f0d0087

.field public static final common_google_play_services_update_title:I = 0x7f0d0088

.field public static final common_open_on_phone:I = 0x7f0d0089

.field public static final common_signin_button_text:I = 0x7f0d008a

.field public static final common_signin_button_text_long:I = 0x7f0d008b

.field public static final create_calendar_message:I = 0x7f0d00b9

.field public static final create_calendar_title:I = 0x7f0d00ba

.field public static final decline:I = 0x7f0d00c2

.field public static final store_picture_message:I = 0x7f0d0313

.field public static final store_picture_title:I = 0x7f0d0314

.field public static final wallet_buy_button_place_holder:I = 0x7f0d0348
