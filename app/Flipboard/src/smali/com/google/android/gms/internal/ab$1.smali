.class Lcom/google/android/gms/internal/ab$1;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/internal/u;

.field final synthetic b:Lcom/google/android/gms/internal/ab;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/internal/u;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/ab;Lcom/google/android/gms/internal/u;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/gms/internal/ab$1;->b:Lcom/google/android/gms/internal/ab;

    iput-object p2, p0, Lcom/google/android/gms/internal/ab$1;->a:Lcom/google/android/gms/internal/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, Lcom/google/android/gms/internal/ab$1;->a:Lcom/google/android/gms/internal/u;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ab$1;->c:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/ab$1;->b:Lcom/google/android/gms/internal/ab;

    invoke-static {v0}, Lcom/google/android/gms/internal/ab;->a(Lcom/google/android/gms/internal/ab;)Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ab$1;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/u;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/internal/ab$1;->b:Lcom/google/android/gms/internal/ab;

    invoke-static {v1}, Lcom/google/android/gms/internal/ab;->b(Lcom/google/android/gms/internal/ab;)Lcom/google/android/gms/internal/av;

    move-result-object v2

    iget-object v1, v0, Lcom/google/android/gms/internal/u;->a:Lcom/google/android/gms/internal/u$b;

    iget-object v1, v1, Lcom/google/android/gms/internal/u$b;->a:Lcom/google/android/gms/internal/u$a;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/u$a;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/internal/gj;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/gms/internal/u;->c:Z

    if-nez v1, :cond_1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/u;->a(Lcom/google/android/gms/internal/av;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "Ad is not visible. Not refreshing ad."

    invoke-static {v1}, Lcom/google/android/gms/internal/gs;->b(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/internal/u;->b:Lcom/google/android/gms/internal/ab;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/ab;->a(Lcom/google/android/gms/internal/av;)V

    goto :goto_0
.end method
