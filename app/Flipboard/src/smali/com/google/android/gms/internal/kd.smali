.class public Lcom/google/android/gms/internal/kd;
.super Ljava/lang/Object;


# static fields
.field public static final A:Lcom/google/android/gms/internal/kd$g;

.field public static final B:Lcom/google/android/gms/internal/kd$h;

.field public static final C:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final D:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final E:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final F:Lcom/google/android/gms/drive/metadata/internal/b;

.field public static final G:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Lcom/google/android/gms/drive/DriveId;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/android/gms/internal/kd$a;

.field public static final d:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final e:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final f:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final g:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final h:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final i:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final j:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final k:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final l:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final m:Lcom/google/android/gms/internal/kd$b;

.field public static final n:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final o:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final p:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final q:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final r:Lcom/google/android/gms/internal/kd$c;

.field public static final s:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final t:Lcom/google/android/gms/drive/metadata/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/b",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final u:Lcom/google/android/gms/drive/metadata/internal/m;

.field public static final v:Lcom/google/android/gms/drive/metadata/internal/m;

.field public static final w:Lcom/google/android/gms/internal/kd$d;

.field public static final x:Lcom/google/android/gms/internal/kd$e;

.field public static final y:Lcom/google/android/gms/internal/kd$f;

.field public static final z:Lcom/google/android/gms/drive/metadata/MetadataField;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/drive/metadata/MetadataField",
            "<",
            "Lcom/google/android/gms/common/data/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const v5, 0x5b8d80

    const v4, 0x419ce0

    sget-object v0, Lcom/google/android/gms/internal/kg;->b:Lcom/google/android/gms/internal/kg;

    sput-object v0, Lcom/google/android/gms/internal/kd;->a:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "alternateLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->b:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kd$a;

    invoke-direct {v0}, Lcom/google/android/gms/internal/kd$a;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/kd;->c:Lcom/google/android/gms/internal/kd$a;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "description"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->d:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "embedLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->e:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "fileExtension"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->f:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/g;

    const-string v1, "fileSize"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->g:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "hasThumbnail"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->h:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "indexableText"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->i:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isAppData"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->j:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isCopyable"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->k:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isEditable"

    const v2, 0x3e8fa0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->l:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kd$b;

    const-string v1, "isPinned"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->m:Lcom/google/android/gms/internal/kd$b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isRestricted"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->n:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isShared"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->o:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isTrashable"

    const v2, 0x432380

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->p:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "isViewed"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->q:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kd$c;

    const-string v1, "mimeType"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->r:Lcom/google/android/gms/internal/kd$c;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "originalFilename"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->s:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/k;

    const-string v1, "ownerNames"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->t:Lcom/google/android/gms/drive/metadata/b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "lastModifyingUser"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->u:Lcom/google/android/gms/drive/metadata/internal/m;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/m;

    const-string v1, "sharingUser"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->v:Lcom/google/android/gms/drive/metadata/internal/m;

    new-instance v0, Lcom/google/android/gms/internal/kd$d;

    const-string v1, "parents"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->w:Lcom/google/android/gms/internal/kd$d;

    new-instance v0, Lcom/google/android/gms/internal/kd$e;

    const-string v1, "quotaBytesUsed"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->x:Lcom/google/android/gms/internal/kd$e;

    new-instance v0, Lcom/google/android/gms/internal/kd$f;

    const-string v1, "starred"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->y:Lcom/google/android/gms/internal/kd$f;

    new-instance v0, Lcom/google/android/gms/internal/kd$1;

    const-string v1, "thumbnail"

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v2

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/internal/kd$1;-><init>(Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->z:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/internal/kd$g;

    const-string v1, "title"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->A:Lcom/google/android/gms/internal/kd$g;

    new-instance v0, Lcom/google/android/gms/internal/kd$h;

    const-string v1, "trashed"

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/kd$h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->B:Lcom/google/android/gms/internal/kd$h;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "webContentLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->C:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "webViewLink"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->D:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "uniqueIdentifier"

    const v2, 0x4c4b40

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->E:Lcom/google/android/gms/drive/metadata/MetadataField;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/b;

    const-string v1, "writersCanShare"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/b;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->F:Lcom/google/android/gms/drive/metadata/internal/b;

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/l;

    const-string v1, "role"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/metadata/internal/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/internal/kd;->G:Lcom/google/android/gms/drive/metadata/MetadataField;

    return-void
.end method
