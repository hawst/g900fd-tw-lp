.class public Lcom/google/android/gms/internal/dk;
.super Lcom/google/android/gms/internal/ds$a;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# static fields
.field private static final i:I


# instance fields
.field final a:Landroid/app/Activity;

.field b:Lcom/google/android/gms/internal/do;

.field c:Lcom/google/android/gms/internal/gv;

.field d:Z

.field e:Z

.field f:Landroid/widget/FrameLayout;

.field g:Landroid/webkit/WebChromeClient$CustomViewCallback;

.field h:Landroid/widget/RelativeLayout;

.field private j:Lcom/google/android/gms/internal/dm;

.field private k:Lcom/google/android/gms/internal/dk$c;

.field private l:Lcom/google/android/gms/internal/dp;

.field private m:Z

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0, v0, v0, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/gms/internal/dk;->i:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/internal/ds$a;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->e:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->m:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->n:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->o:Z

    iput-object p1, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    return-void
.end method

.method static a(IIII)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p0, p1, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/internal/dm;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms.ads.AdActivity"

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.ads.internal.overlay.useClientJar"

    iget-object v2, p1, Lcom/google/android/gms/internal/dm;->n:Lcom/google/android/gms/internal/gt;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/gt;->e:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {v0, p1}, Lcom/google/android/gms/internal/dm;->a(Landroid/content/Intent;Lcom/google/android/gms/internal/dm;)V

    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    instance-of v1, p0, Landroid/app/Activity;

    if-nez v1, :cond_0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private b(Z)V
    .locals 4

    const/4 v3, -0x2

    if-eqz p1, :cond_0

    const/16 v0, 0x32

    :goto_0
    new-instance v1, Lcom/google/android/gms/internal/dp;

    iget-object v2, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/internal/dp;-><init>(Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/google/android/gms/internal/dk;->l:Lcom/google/android/gms/internal/dp;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    if-eqz p1, :cond_1

    const/16 v0, 0xb

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->l:Lcom/google/android/gms/internal/dp;

    iget-object v2, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/dm;->h:Z

    invoke-virtual {v0, v2}, Lcom/google/android/gms/internal/dp;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/google/android/gms/internal/dk;->l:Lcom/google/android/gms/internal/dp;

    invoke-virtual {v0, v2, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    const/16 v0, 0x20

    goto :goto_0

    :cond_1
    const/16 v0, 0x9

    goto :goto_1
.end method

.method private c(Z)V
    .locals 14

    const/16 v3, 0x400

    const/4 v13, -0x1

    const/4 v4, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->requestWindowFeature(I)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/internal/dk;->o:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v1, v1, Lcom/google/android/gms/internal/dm;->q:Lcom/google/android/gms/internal/x;

    iget-boolean v1, v1, Lcom/google/android/gms/internal/x;->c:Z

    if-eqz v1, :cond_2

    :cond_1
    invoke-virtual {v0, v3, v3}, Landroid/view/Window;->setFlags(II)V

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget v1, v1, Lcom/google/android/gms/internal/dm;->k:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/internal/dk;->a(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v1, v3, :cond_3

    invoke-static {}, Lcom/google/android/gms/internal/gs;->a()V

    invoke-static {v0}, Lcom/google/android/gms/internal/gn;->a(Landroid/view/Window;)V

    :cond_3
    new-instance v0, Lcom/google/android/gms/internal/dk$b;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v3, v3, Lcom/google/android/gms/internal/dm;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/dk$b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->o:Z

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    iput-boolean v2, p0, Lcom/google/android/gms/internal/dk;->d:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->e:Lcom/google/android/gms/internal/gv;

    iget-object v0, v0, Lcom/google/android/gms/internal/gv;->a:Lcom/google/android/gms/internal/gw;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gw;->a()Z

    move-result v3

    if-eqz p1, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v1, v1, Lcom/google/android/gms/internal/dm;->e:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/gv;->d()Lcom/google/android/gms/internal/ay;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v5, v5, Lcom/google/android/gms/internal/dm;->n:Lcom/google/android/gms/internal/gt;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/internal/gv;->a(Landroid/content/Context;Lcom/google/android/gms/internal/ay;ZZLcom/google/android/gms/internal/k;Lcom/google/android/gms/internal/gt;)Lcom/google/android/gms/internal/gv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v5, v0, Lcom/google/android/gms/internal/gv;->a:Lcom/google/android/gms/internal/gw;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v8, v0, Lcom/google/android/gms/internal/dm;->f:Lcom/google/android/gms/internal/bw;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v9, v0, Lcom/google/android/gms/internal/dm;->j:Lcom/google/android/gms/internal/dq;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v11, v0, Lcom/google/android/gms/internal/dm;->o:Lcom/google/android/gms/internal/bz;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->e:Lcom/google/android/gms/internal/gv;

    iget-object v0, v0, Lcom/google/android/gms/internal/gv;->a:Lcom/google/android/gms/internal/gw;

    iget-object v12, v0, Lcom/google/android/gms/internal/gw;->n:Lcom/google/android/gms/internal/v;

    move-object v6, v4

    move-object v7, v4

    move v10, v2

    invoke-virtual/range {v5 .. v12}, Lcom/google/android/gms/internal/gw;->a(Lcom/google/android/gms/internal/t;Lcom/google/android/gms/internal/dn;Lcom/google/android/gms/internal/bw;Lcom/google/android/gms/internal/dq;ZLcom/google/android/gms/internal/bz;Lcom/google/android/gms/internal/v;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v0, v0, Lcom/google/android/gms/internal/gv;->a:Lcom/google/android/gms/internal/gw;

    new-instance v1, Lcom/google/android/gms/internal/dk$1;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/dk$1;-><init>(Lcom/google/android/gms/internal/dk;)V

    iput-object v1, v0, Lcom/google/android/gms/internal/gw;->f:Lcom/google/android/gms/internal/gw$a;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->m:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v1, v1, Lcom/google/android/gms/internal/dm;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gv;->loadUrl(Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v1, v0, Lcom/google/android/gms/internal/gv;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p0, v0, Lcom/google/android/gms/internal/gv;->f:Lcom/google/android/gms/internal/dk;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gv;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_4

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->o:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    sget v1, Lcom/google/android/gms/internal/dk;->i:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gv;->setBackgroundColor(I)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0, v1, v13, v13}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;II)V

    if-nez p1, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/gv;->b()V

    :cond_6
    invoke-direct {p0, v3}, Lcom/google/android/gms/internal/dk;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/gv;->g:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0, v2}, Lcom/google/android/gms/internal/dk;->a(Z)V

    :cond_7
    return-void

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    sget v1, Lcom/google/android/gms/internal/dk;->i:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->i:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v5, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v6, v0, Lcom/google/android/gms/internal/dm;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v7, v0, Lcom/google/android/gms/internal/dm;->i:Ljava/lang/String;

    const-string v8, "text/html"

    const-string v9, "UTF-8"

    move-object v10, v4

    invoke-virtual/range {v5 .. v10}, Lcom/google/android/gms/internal/gv;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_a
    new-instance v0, Lcom/google/android/gms/internal/dk$a;

    const-string v1, "No URL or HTML to display in ad overlay."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/dk$a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->e:Lcom/google/android/gms/internal/gv;

    iput-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gv;->setContext(Landroid/content/Context;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private j()V
    .locals 4

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/internal/dk;->n:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    const-string v2, "version"

    iget-object v3, v0, Lcom/google/android/gms/internal/gv;->e:Lcom/google/android/gms/internal/gt;

    iget-object v3, v3, Lcom/google/android/gms/internal/gt;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "onhide"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/internal/gv;->a(Ljava/lang/String;Ljava/util/Map;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->k:Lcom/google/android/gms/internal/dk$c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/gv;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->k:Lcom/google/android/gms/internal/dk$c;

    iget-object v0, v0, Lcom/google/android/gms/internal/dk$c;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    iget-object v2, p0, Lcom/google/android/gms/internal/dk;->k:Lcom/google/android/gms/internal/dk$c;

    iget v2, v2, Lcom/google/android/gms/internal/dk$c;->a:I

    iget-object v3, p0, Lcom/google/android/gms/internal/dk;->k:Lcom/google/android/gms/internal/dk$c;

    iget-object v3, v3, Lcom/google/android/gms/internal/dk$c;->b:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->d:Lcom/google/android/gms/internal/dn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->d:Lcom/google/android/gms/internal/dn;

    invoke-interface {v0}, Lcom/google/android/gms/internal/dn;->r()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget v0, v0, Lcom/google/android/gms/internal/dm;->k:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/dk;->a(I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->f:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->d:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iput-object v2, p0, Lcom/google/android/gms/internal/dk;->f:Landroid/widget/FrameLayout;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->g:Landroid/webkit/WebChromeClient$CustomViewCallback;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->g:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    iput-object v2, p0, Lcom/google/android/gms/internal/dk;->g:Landroid/webkit/WebChromeClient$CustomViewCallback;

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->e:Z

    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setRequestedOrientation(I)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string v1, "com.google.android.gms.ads.internal.overlay.hasResumed"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->m:Z

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/dm;->a(Landroid/content/Intent;)Lcom/google/android/gms/internal/dm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/internal/dk$a;

    const-string v1, "Could not get info for ad overlay."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/dk$a;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/internal/dk$a; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/dk$a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/internal/gs;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->q:Lcom/google/android/gms/internal/x;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->q:Lcom/google/android/gms/internal/x;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/x;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->o:Z

    :goto_1
    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->d:Lcom/google/android/gms/internal/dn;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->d:Lcom/google/android/gms/internal/dn;

    invoke-interface {v0}, Lcom/google/android/gms/internal/dn;->s()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget v0, v0, Lcom/google/android/gms/internal/dm;->l:I

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->c:Lcom/google/android/gms/internal/t;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v0, v0, Lcom/google/android/gms/internal/dm;->c:Lcom/google/android/gms/internal/t;

    invoke-interface {v0}, Lcom/google/android/gms/internal/t;->u()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget v0, v0, Lcom/google/android/gms/internal/dm;->l:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lcom/google/android/gms/internal/dk$a;

    const-string v1, "Could not determine ad overlay type."

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/dk$a;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->o:Z

    goto :goto_1

    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/dk;->c(Z)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/google/android/gms/internal/dk$c;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v1, v1, Lcom/google/android/gms/internal/dm;->e:Lcom/google/android/gms/internal/gv;

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/dk$c;-><init>(Lcom/google/android/gms/internal/gv;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/dk;->k:Lcom/google/android/gms/internal/dk$c;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/dk;->c(Z)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/dk;->c(Z)V

    goto :goto_0

    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->m:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v1, v1, Lcom/google/android/gms/internal/dm;->b:Lcom/google/android/gms/internal/dj;

    iget-object v2, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget-object v2, v2, Lcom/google/android/gms/internal/dm;->j:Lcom/google/android/gms/internal/dq;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/internal/dh;->a(Landroid/content/Context;Lcom/google/android/gms/internal/dj;Lcom/google/android/gms/internal/dq;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V
    :try_end_1
    .catch Lcom/google/android/gms/internal/dk$a; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->l:Lcom/google/android/gms/internal/dp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->l:Lcom/google/android/gms/internal/dp;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/dp;->a(Z)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "com.google.android.gms.ads.internal.overlay.hasResumed"

    iget-boolean v1, p0, Lcom/google/android/gms/internal/dk;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->j:Lcom/google/android/gms/internal/dm;

    iget v0, v0, Lcom/google/android/gms/internal/dm;->l:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/internal/dk;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-static {v0}, Lcom/google/android/gms/internal/gj;->b(Landroid/webkit/WebView;)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->m:Z

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->b:Lcom/google/android/gms/internal/do;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->b:Lcom/google/android/gms/internal/do;

    iget-object v0, v0, Lcom/google/android/gms/internal/do;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/dk;->a()V

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->k:Lcom/google/android/gms/internal/dk$c;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-static {v0}, Lcom/google/android/gms/internal/gj;->a(Landroid/webkit/WebView;)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/internal/dk;->j()V

    return-void
.end method

.method public final f()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/dk;->j()V

    return-void
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->b:Lcom/google/android/gms/internal/do;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->b:Lcom/google/android/gms/internal/do;

    iget-object v1, v0, Lcom/google/android/gms/internal/do;->c:Lcom/google/android/gms/internal/do$a;

    invoke-virtual {v1}, Lcom/google/android/gms/internal/do$a;->cancel()V

    iget-object v0, v0, Lcom/google/android/gms/internal/do;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->c:Lcom/google/android/gms/internal/gv;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/internal/dk;->j()V

    return-void
.end method

.method public final h()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/dk;->d:Z

    return-void
.end method

.method public final i()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/dk;->h:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/gms/internal/dk;->l:Lcom/google/android/gms/internal/dp;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/internal/dk;->b(Z)V

    return-void
.end method
