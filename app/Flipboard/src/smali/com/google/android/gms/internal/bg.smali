.class public final Lcom/google/android/gms/internal/bg;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/ez;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/util/Date;

.field final c:Ljava/lang/String;

.field final d:I

.field final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final f:Landroid/location/Location;

.field final g:Z

.field final h:Landroid/os/Bundle;

.field public final i:Ljava/util/Map;

.field final j:Ljava/lang/String;

.field final k:Lcom/google/android/gms/ads/search/SearchAdRequest;

.field final l:I

.field final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "emulator"

    invoke-static {v0}, Lcom/google/android/gms/internal/gr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/internal/bg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/bg$a;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/bg;-><init>(Lcom/google/android/gms/internal/bg$a;B)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/bg$a;B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->a(Lcom/google/android/gms/internal/bg$a;)Ljava/util/Date;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->b:Ljava/util/Date;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->b(Lcom/google/android/gms/internal/bg$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->c(Lcom/google/android/gms/internal/bg$a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bg;->d:I

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->d(Lcom/google/android/gms/internal/bg$a;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->e:Ljava/util/Set;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->e(Lcom/google/android/gms/internal/bg$a;)Landroid/location/Location;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->f:Landroid/location/Location;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->f(Lcom/google/android/gms/internal/bg$a;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/bg;->g:Z

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->g(Lcom/google/android/gms/internal/bg$a;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->h:Landroid/os/Bundle;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->h(Lcom/google/android/gms/internal/bg$a;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->i:Ljava/util/Map;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->i(Lcom/google/android/gms/internal/bg$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->j:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->k:Lcom/google/android/gms/ads/search/SearchAdRequest;

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->j(Lcom/google/android/gms/internal/bg$a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/internal/bg;->l:I

    invoke-static {p1}, Lcom/google/android/gms/internal/bg$a;->k(Lcom/google/android/gms/internal/bg$a;)Ljava/util/HashSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/bg;->m:Ljava/util/Set;

    return-void
.end method
