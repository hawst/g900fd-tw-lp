.class public Lcom/google/android/gms/internal/bi;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lcom/google/android/gms/internal/cs;

.field public final b:Landroid/content/Context;

.field public final c:Lcom/google/android/gms/internal/ax;

.field public d:Lcom/google/android/gms/ads/AdListener;

.field public e:Lcom/google/android/gms/internal/bd;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/android/gms/ads/doubleclick/AppEventListener;

.field public i:Lcom/google/android/gms/ads/purchase/PlayStorePurchaseListener;

.field public j:Lcom/google/android/gms/ads/purchase/InAppPurchaseListener;

.field public k:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

.field public l:Lcom/google/android/gms/ads/doubleclick/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-static {}, Lcom/google/android/gms/internal/ax;->a()Lcom/google/android/gms/internal/ax;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/internal/bi;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ax;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/internal/ax;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/cs;

    invoke-direct {v0}, Lcom/google/android/gms/internal/cs;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/bi;->a:Lcom/google/android/gms/internal/cs;

    iput-object p1, p0, Lcom/google/android/gms/internal/bi;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/internal/bi;->c:Lcom/google/android/gms/internal/ax;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/bi;->k:Lcom/google/android/gms/ads/doubleclick/PublisherInterstitialAd;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/internal/bi;->e:Lcom/google/android/gms/internal/bd;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The ad unit ID must be set on InterstitialAd before "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is called."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
