.class public Lcom/google/android/apps/analytics/Item;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:D

.field final f:J


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/Item$Builder;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/analytics/Item$Builder;->a(Lcom/google/android/apps/analytics/Item$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Item;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Item$Builder;->b(Lcom/google/android/apps/analytics/Item$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Item;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Item$Builder;->c(Lcom/google/android/apps/analytics/Item$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Item;->e:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/Item$Builder;->d(Lcom/google/android/apps/analytics/Item$Builder;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Item;->f:J

    invoke-static {p1}, Lcom/google/android/apps/analytics/Item$Builder;->e(Lcom/google/android/apps/analytics/Item$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Item;->c:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Item$Builder;->f(Lcom/google/android/apps/analytics/Item$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Item;->d:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/Item$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/analytics/Item;-><init>(Lcom/google/android/apps/analytics/Item$Builder;)V

    return-void
.end method
