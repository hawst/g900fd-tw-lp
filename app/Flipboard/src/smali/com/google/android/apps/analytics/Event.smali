.class Lcom/google/android/apps/analytics/Event;
.super Ljava/lang/Object;


# instance fields
.field final a:J

.field final b:Ljava/lang/String;

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:Z

.field k:Z

.field final l:Ljava/lang/String;

.field final m:Ljava/lang/String;

.field final n:Ljava/lang/String;

.field final o:Ljava/lang/String;

.field final p:I

.field final q:I

.field final r:I

.field s:Lcom/google/android/apps/analytics/CustomVariableBuffer;

.field t:Lcom/google/android/apps/analytics/Transaction;

.field u:Lcom/google/android/apps/analytics/Item;


# direct methods
.method constructor <init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/google/android/apps/analytics/Event;->a:J

    iput-object p3, p0, Lcom/google/android/apps/analytics/Event;->b:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/apps/analytics/Event;->c:I

    iput p5, p0, Lcom/google/android/apps/analytics/Event;->e:I

    iput p6, p0, Lcom/google/android/apps/analytics/Event;->f:I

    iput p7, p0, Lcom/google/android/apps/analytics/Event;->g:I

    iput p8, p0, Lcom/google/android/apps/analytics/Event;->h:I

    iput-object p9, p0, Lcom/google/android/apps/analytics/Event;->l:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/apps/analytics/Event;->m:Ljava/lang/String;

    iput-object p11, p0, Lcom/google/android/apps/analytics/Event;->n:Ljava/lang/String;

    iput-object p12, p0, Lcom/google/android/apps/analytics/Event;->o:Ljava/lang/String;

    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/apps/analytics/Event;->p:I

    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/apps/analytics/Event;->r:I

    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/apps/analytics/Event;->q:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/analytics/Event;->i:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/analytics/Event;->k:Z

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/analytics/Event;Ljava/lang/String;)V
    .locals 19

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/google/android/apps/analytics/Event;->a:J

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/analytics/Event;->c:I

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/apps/analytics/Event;->e:I

    move-object/from16 v0, p1

    iget v9, v0, Lcom/google/android/apps/analytics/Event;->f:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/apps/analytics/Event;->g:I

    move-object/from16 v0, p1

    iget v11, v0, Lcom/google/android/apps/analytics/Event;->h:I

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/apps/analytics/Event;->l:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/google/android/apps/analytics/Event;->m:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/google/android/apps/analytics/Event;->n:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/apps/analytics/Event;->o:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/apps/analytics/Event;->p:I

    move/from16 v16, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/apps/analytics/Event;->q:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/apps/analytics/Event;->r:I

    move/from16 v18, v0

    move-object/from16 v3, p0

    move-object/from16 v6, p2

    invoke-direct/range {v3 .. v18}, Lcom/google/android/apps/analytics/Event;-><init>(JLjava/lang/String;IIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/apps/analytics/Event;->d:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/analytics/Event;->d:I

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/apps/analytics/Event;->i:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/analytics/Event;->i:I

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/analytics/Event;->j:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/analytics/Event;->j:Z

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/apps/analytics/Event;->k:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/analytics/Event;->k:Z

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/analytics/Event;->s:Lcom/google/android/apps/analytics/CustomVariableBuffer;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/analytics/Event;->s:Lcom/google/android/apps/analytics/CustomVariableBuffer;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/analytics/Event;->t:Lcom/google/android/apps/analytics/Transaction;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/analytics/Event;->t:Lcom/google/android/apps/analytics/Transaction;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/analytics/Event;->u:Lcom/google/android/apps/analytics/Item;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/analytics/Event;->u:Lcom/google/android/apps/analytics/Item;

    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/apps/analytics/Event;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " random:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampCurrent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampPrevious:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " timestampFirst:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " visits:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " value:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " category:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/Event;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " action:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/Event;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " label:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/analytics/Event;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " width:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->q:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " height:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/analytics/Event;->r:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
