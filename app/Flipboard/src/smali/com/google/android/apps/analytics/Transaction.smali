.class public Lcom/google/android/apps/analytics/Transaction;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;

.field final c:D

.field final d:D

.field final e:D


# direct methods
.method private constructor <init>(Lcom/google/android/apps/analytics/Transaction$Builder;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->a(Lcom/google/android/apps/analytics/Transaction$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Transaction;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->b(Lcom/google/android/apps/analytics/Transaction$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->c:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->c(Lcom/google/android/apps/analytics/Transaction$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/analytics/Transaction;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->d(Lcom/google/android/apps/analytics/Transaction$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->d:D

    invoke-static {p1}, Lcom/google/android/apps/analytics/Transaction$Builder;->e(Lcom/google/android/apps/analytics/Transaction$Builder;)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/analytics/Transaction;->e:D

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/analytics/Transaction$Builder;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/analytics/Transaction;-><init>(Lcom/google/android/apps/analytics/Transaction$Builder;)V

    return-void
.end method
