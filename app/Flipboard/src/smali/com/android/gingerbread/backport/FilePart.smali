.class public Lcom/android/gingerbread/backport/FilePart;
.super Lcom/android/gingerbread/backport/PartBase;
.source "FilePart.java"


# static fields
.field private static final i:Lflipboard/util/Log;

.field private static final j:[B


# instance fields
.field private k:Lcom/android/gingerbread/backport/PartSource;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-string v0, "report issue"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/FilePart;->i:Lflipboard/util/Log;

    .line 74
    const-string v0, "; filename="

    .line 75
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/FilePart;->j:[B

    .line 74
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 92
    if-nez p3, :cond_0

    const-string p3, "application/octet-stream"

    :cond_0
    if-nez p4, :cond_1

    const-string p4, "ISO-8859-1"

    :cond_1
    const-string v0, "binary"

    invoke-direct {p0, p1, p3, p4, v0}, Lcom/android/gingerbread/backport/PartBase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    iput-object p2, p0, Lcom/android/gingerbread/backport/FilePart;->k:Lcom/android/gingerbread/backport/PartSource;

    .line 103
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 126
    new-instance v0, Lcom/android/gingerbread/backport/FilePartSource;

    invoke-direct {v0, p2}, Lcom/android/gingerbread/backport/FilePartSource;-><init>(Ljava/io/File;)V

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/android/gingerbread/backport/FilePart;-><init>(Ljava/lang/String;Lcom/android/gingerbread/backport/PartSource;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method


# virtual methods
.method protected final a()J
    .locals 2

    .prologue
    .line 249
    sget-object v0, Lcom/android/gingerbread/backport/FilePart;->i:Lflipboard/util/Log;

    .line 250
    iget-object v0, p0, Lcom/android/gingerbread/backport/FilePart;->k:Lcom/android/gingerbread/backport/PartSource;

    invoke-interface {v0}, Lcom/android/gingerbread/backport/PartSource;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method protected final a(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 190
    sget-object v0, Lcom/android/gingerbread/backport/FilePart;->i:Lflipboard/util/Log;

    .line 191
    invoke-super {p0, p1}, Lcom/android/gingerbread/backport/PartBase;->a(Ljava/io/OutputStream;)V

    .line 192
    iget-object v0, p0, Lcom/android/gingerbread/backport/FilePart;->k:Lcom/android/gingerbread/backport/PartSource;

    invoke-interface {v0}, Lcom/android/gingerbread/backport/PartSource;->b()Ljava/lang/String;

    move-result-object v0

    .line 193
    if-eqz v0, :cond_0

    .line 194
    sget-object v1, Lcom/android/gingerbread/backport/FilePart;->j:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 195
    sget-object v1, Lcom/android/gingerbread/backport/FilePart;->c:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 196
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 197
    sget-object v0, Lcom/android/gingerbread/backport/FilePart;->c:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 199
    :cond_0
    return-void
.end method

.method protected final b(Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 209
    sget-object v0, Lcom/android/gingerbread/backport/FilePart;->i:Lflipboard/util/Log;

    .line 210
    invoke-virtual {p0}, Lcom/android/gingerbread/backport/FilePart;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 215
    sget-object v0, Lcom/android/gingerbread/backport/FilePart;->i:Lflipboard/util/Log;

    .line 229
    :goto_0
    return-void

    .line 219
    :cond_0
    const/16 v0, 0x1000

    new-array v0, v0, [B

    .line 220
    iget-object v1, p0, Lcom/android/gingerbread/backport/FilePart;->k:Lcom/android/gingerbread/backport/PartSource;

    invoke-interface {v1}, Lcom/android/gingerbread/backport/PartSource;->c()Ljava/io/InputStream;

    move-result-object v1

    .line 223
    :goto_1
    :try_start_0
    invoke-virtual {v1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_1

    .line 224
    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 228
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0

    :cond_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_0
.end method
