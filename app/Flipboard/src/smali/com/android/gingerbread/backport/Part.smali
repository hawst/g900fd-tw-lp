.class public abstract Lcom/android/gingerbread/backport/Part;
.super Ljava/lang/Object;
.source "Part.java"


# static fields
.field protected static final a:[B

.field protected static final b:[B

.field protected static final c:[B

.field protected static final d:[B

.field protected static final e:[B

.field protected static final f:[B

.field protected static final g:[B

.field protected static final h:[B

.field private static final i:[B


# instance fields
.field private j:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "----------------314159265358979323846"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 68
    sput-object v0, Lcom/android/gingerbread/backport/Part;->a:[B

    sput-object v0, Lcom/android/gingerbread/backport/Part;->i:[B

    .line 74
    const-string v0, "\r\n"

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->b:[B

    .line 80
    const-string v0, "\""

    .line 81
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->c:[B

    .line 87
    const-string v0, "--"

    .line 88
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->d:[B

    .line 94
    const-string v0, "Content-Disposition: form-data; name="

    .line 95
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->e:[B

    .line 101
    const-string v0, "Content-Type: "

    .line 102
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->f:[B

    .line 108
    const-string v0, "; charset="

    .line 109
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->g:[B

    .line 115
    const-string v0, "Content-Transfer-Encoding: "

    .line 116
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    sput-object v0, Lcom/android/gingerbread/backport/Part;->h:[B

    .line 115
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a([Lcom/android/gingerbread/backport/Part;[B)J
    .locals 12

    .prologue
    const-wide/16 v6, -0x1

    const-wide/16 v4, 0x0

    .line 406
    if-nez p0, :cond_0

    .line 407
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parts may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_0
    const/4 v0, 0x0

    move-wide v2, v4

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_3

    .line 412
    aget-object v1, p0, v0

    iput-object p1, v1, Lcom/android/gingerbread/backport/Part;->j:[B

    .line 413
    aget-object v1, p0, v0

    invoke-virtual {v1}, Lcom/android/gingerbread/backport/Part;->a()J

    move-result-wide v8

    cmp-long v8, v8, v4

    if-gez v8, :cond_1

    move-wide v8, v6

    .line 414
    :goto_1
    cmp-long v1, v8, v4

    if-gez v1, :cond_2

    move-wide v0, v6

    .line 423
    :goto_2
    return-wide v0

    .line 413
    :cond_1
    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {v1, v8}, Lcom/android/gingerbread/backport/Part;->c(Ljava/io/OutputStream;)V

    invoke-virtual {v1, v8}, Lcom/android/gingerbread/backport/Part;->a(Ljava/io/OutputStream;)V

    invoke-direct {v1, v8}, Lcom/android/gingerbread/backport/Part;->d(Ljava/io/OutputStream;)V

    invoke-direct {v1, v8}, Lcom/android/gingerbread/backport/Part;->e(Ljava/io/OutputStream;)V

    invoke-static {v8}, Lcom/android/gingerbread/backport/Part;->f(Ljava/io/OutputStream;)V

    sget-object v9, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {v8, v9}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v1}, Lcom/android/gingerbread/backport/Part;->a()J

    move-result-wide v10

    add-long/2addr v8, v10

    goto :goto_1

    .line 417
    :cond_2
    add-long/2addr v2, v8

    .line 410
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 419
    :cond_3
    sget-object v0, Lcom/android/gingerbread/backport/Part;->d:[B

    array-length v0, v0

    int-to-long v0, v0

    add-long/2addr v0, v2

    .line 420
    array-length v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 421
    sget-object v2, Lcom/android/gingerbread/backport/Part;->d:[B

    array-length v2, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 422
    sget-object v2, Lcom/android/gingerbread/backport/Part;->b:[B

    array-length v2, v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    .line 423
    goto :goto_2
.end method

.method public static a(Ljava/io/OutputStream;[Lcom/android/gingerbread/backport/Part;[B)V
    .locals 2

    .prologue
    .line 364
    if-nez p1, :cond_0

    .line 365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parts may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_0
    if-eqz p2, :cond_1

    array-length v0, p2

    if-nez v0, :cond_2

    .line 368
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "partBoundary may not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_2
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_3

    .line 372
    aget-object v1, p1, v0

    iput-object p2, v1, Lcom/android/gingerbread/backport/Part;->j:[B

    .line 373
    aget-object v1, p1, v0

    invoke-virtual {v1, p0}, Lcom/android/gingerbread/backport/Part;->send(Ljava/io/OutputStream;)V

    .line 370
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375
    :cond_3
    sget-object v0, Lcom/android/gingerbread/backport/Part;->d:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 376
    invoke-virtual {p0, p2}, Ljava/io/OutputStream;->write([B)V

    .line 377
    sget-object v0, Lcom/android/gingerbread/backport/Part;->d:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 378
    sget-object v0, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 379
    return-void
.end method

.method private c(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lcom/android/gingerbread/backport/Part;->d:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 200
    iget-object v0, p0, Lcom/android/gingerbread/backport/Part;->j:[B

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/gingerbread/backport/Part;->i:[B

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 201
    sget-object v0, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 202
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/android/gingerbread/backport/Part;->j:[B

    goto :goto_0
.end method

.method private d(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 223
    invoke-virtual {p0}, Lcom/android/gingerbread/backport/Part;->c()Ljava/lang/String;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_0

    .line 225
    sget-object v1, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 226
    sget-object v1, Lcom/android/gingerbread/backport/Part;->f:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 227
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 228
    invoke-virtual {p0}, Lcom/android/gingerbread/backport/Part;->d()Ljava/lang/String;

    move-result-object v0

    .line 229
    if-eqz v0, :cond_0

    .line 230
    sget-object v1, Lcom/android/gingerbread/backport/Part;->g:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 231
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 234
    :cond_0
    return-void
.end method

.method private e(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/android/gingerbread/backport/Part;->e()Ljava/lang/String;

    move-result-object v0

    .line 245
    if-eqz v0, :cond_0

    .line 246
    sget-object v1, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 247
    sget-object v1, Lcom/android/gingerbread/backport/Part;->h:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    .line 248
    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 250
    :cond_0
    return-void
.end method

.method private static f(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 258
    sget-object v0, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 259
    sget-object v0, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    .line 260
    return-void
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method protected abstract a()J
.end method

.method protected a(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 211
    sget-object v0, Lcom/android/gingerbread/backport/Part;->e:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 212
    sget-object v0, Lcom/android/gingerbread/backport/Part;->c:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 213
    invoke-virtual {p0}, Lcom/android/gingerbread/backport/Part;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 214
    sget-object v0, Lcom/android/gingerbread/backport/Part;->c:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 215
    return-void
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method protected abstract b(Ljava/io/OutputStream;)V
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public send(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 295
    invoke-direct {p0, p1}, Lcom/android/gingerbread/backport/Part;->c(Ljava/io/OutputStream;)V

    .line 296
    invoke-virtual {p0, p1}, Lcom/android/gingerbread/backport/Part;->a(Ljava/io/OutputStream;)V

    .line 297
    invoke-direct {p0, p1}, Lcom/android/gingerbread/backport/Part;->d(Ljava/io/OutputStream;)V

    .line 298
    invoke-direct {p0, p1}, Lcom/android/gingerbread/backport/Part;->e(Ljava/io/OutputStream;)V

    .line 299
    invoke-static {p1}, Lcom/android/gingerbread/backport/Part;->f(Ljava/io/OutputStream;)V

    .line 300
    invoke-virtual {p0, p1}, Lcom/android/gingerbread/backport/Part;->b(Ljava/io/OutputStream;)V

    .line 301
    sget-object v0, Lcom/android/gingerbread/backport/Part;->b:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    .line 302
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 334
    invoke-virtual {p0}, Lcom/android/gingerbread/backport/Part;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
