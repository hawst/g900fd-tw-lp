.class Lcom/android/debug/hv/ViewServer$ViewServerWorker;
.super Ljava/lang/Object;
.source "ViewServer.java"

# interfaces
.implements Lcom/android/debug/hv/ViewServer$WindowListener;
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/debug/hv/ViewServer;

.field private b:Ljava/net/Socket;

.field private c:Z

.field private d:Z

.field private final e:[Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/android/debug/hv/ViewServer;Ljava/net/Socket;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 523
    iput-object p1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 521
    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    .line 524
    iput-object p2, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    .line 525
    iput-boolean v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->c:Z

    .line 526
    iput-boolean v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->d:Z

    .line 527
    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 643
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 647
    :try_start_0
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->b(Lcom/android/debug/hv/ViewServer;)Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 649
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 666
    :goto_0
    return-object v0

    .line 649
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0

    .line 655
    :cond_0
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 657
    :try_start_1
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->c(Lcom/android/debug/hv/ViewServer;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 658
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 659
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 663
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 666
    const/4 v0, 0x0

    goto :goto_0

    .line 663
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
.end method

.method private a(Ljava/net/Socket;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 670
    const/4 v1, 0x1

    .line 671
    const/4 v0, 0x0

    .line 674
    :try_start_0
    iget-object v3, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v3}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V

    .line 676
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 677
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-direct {v5, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v4, 0x2000

    invoke-direct {v3, v5, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 679
    :try_start_1
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->c(Lcom/android/debug/hv/ViewServer;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 680
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 681
    const/16 v5, 0x20

    invoke-virtual {v3, v5}, Ljava/io/BufferedWriter;->write(I)V

    .line 682
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 683
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 689
    :catch_0
    move-exception v0

    move-object v0, v3

    .line 691
    :goto_1
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 693
    if-eqz v0, :cond_2

    .line 695
    :try_start_2
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move v0, v2

    .line 702
    :goto_2
    return v0

    .line 686
    :cond_0
    :try_start_3
    const-string v0, "DONE.\n"

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 687
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 691
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 693
    :try_start_4
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    move v0, v1

    .line 698
    goto :goto_2

    .line 697
    :catch_1
    move-exception v0

    move v0, v2

    .line 698
    goto :goto_2

    .line 697
    :catch_2
    move-exception v0

    move v0, v2

    .line 698
    goto :goto_2

    .line 691
    :catchall_0
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    :goto_3
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 693
    if-eqz v3, :cond_1

    .line 695
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 697
    :cond_1
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    goto :goto_4

    .line 691
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 689
    :catch_4
    move-exception v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private a(Ljava/net/Socket;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 588
    .line 589
    const/4 v2, 0x0

    .line 593
    const/16 v3, 0x20

    :try_start_0
    invoke-virtual {p3, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 594
    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 595
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v3

    .line 597
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 598
    const/16 v5, 0x10

    invoke-static {v4, v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v4

    long-to-int v4, v4

    .line 601
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 602
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p3, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 607
    :goto_0
    invoke-direct {p0, v4}, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a(I)Landroid/view/View;

    move-result-object v3

    .line 608
    if-nez v3, :cond_2

    .line 639
    :goto_1
    return v1

    .line 604
    :cond_1
    const-string p3, ""

    goto :goto_0

    .line 613
    :cond_2
    const-class v4, Landroid/view/ViewDebug;

    const-string v5, "dispatchCommand"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/view/View;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-class v8, Ljava/io/OutputStream;

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 615
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 616
    const/4 v5, 0x0

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v6, v7

    const/4 v3, 0x1

    aput-object p2, v6, v3

    const/4 v3, 0x2

    aput-object p3, v6, v3

    const/4 v3, 0x3

    new-instance v7, Lcom/android/debug/hv/ViewServer$UncloseableOuputStream;

    .line 617
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/android/debug/hv/ViewServer$UncloseableOuputStream;-><init>(Ljava/io/OutputStream;)V

    aput-object v7, v6, v3

    .line 616
    invoke-virtual {v4, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 619
    invoke-virtual {p1}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v3

    if-nez v3, :cond_6

    .line 620
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v4, Ljava/io/OutputStreamWriter;

    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 621
    :try_start_1
    const-string v2, "DONE\n"

    invoke-virtual {v3, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 622
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 630
    :goto_2
    if-eqz v3, :cond_3

    .line 632
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_3
    :goto_3
    move v1, v0

    .line 639
    goto :goto_1

    .line 634
    :catch_0
    move-exception v0

    move v0, v1

    .line 635
    goto :goto_3

    .line 625
    :catch_1
    move-exception v0

    .line 626
    :goto_4
    :try_start_3
    const-string v3, "LocalViewServer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not send command "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " with parameters "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 630
    if-eqz v2, :cond_5

    .line 632
    :try_start_4
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    .line 635
    goto :goto_3

    .line 634
    :catch_2
    move-exception v0

    move v0, v1

    .line 635
    goto :goto_3

    .line 630
    :catchall_0
    move-exception v0

    :goto_5
    if-eqz v2, :cond_4

    .line 632
    :try_start_5
    invoke-virtual {v2}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 634
    :cond_4
    :goto_6
    throw v0

    :catch_3
    move-exception v1

    goto :goto_6

    .line 630
    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_5

    .line 625
    :catch_4
    move-exception v0

    move-object v2, v3

    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move-object v3, v2

    goto :goto_2
.end method

.method private b(Ljava/net/Socket;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 706
    const/4 v1, 0x1

    .line 707
    const/4 v0, 0x0

    .line 711
    :try_start_0
    invoke-virtual {p1}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4

    .line 712
    new-instance v3, Ljava/io/BufferedWriter;

    new-instance v5, Ljava/io/OutputStreamWriter;

    invoke-direct {v5, v4}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    const/16 v4, 0x2000

    invoke-direct {v3, v5, v4}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 714
    :try_start_1
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->d(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 718
    :try_start_2
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->b(Lcom/android/debug/hv/ViewServer;)Landroid/view/View;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v4

    .line 720
    :try_start_3
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->d(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 723
    if-eqz v4, :cond_0

    .line 724
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->lock()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 726
    :try_start_4
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0}, Lcom/android/debug/hv/ViewServer;->c(Lcom/android/debug/hv/ViewServer;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v5, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v5}, Lcom/android/debug/hv/ViewServer;->b(Lcom/android/debug/hv/ViewServer;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 728
    :try_start_5
    iget-object v5, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v5}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    .line 731
    invoke-static {v4}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 732
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/io/BufferedWriter;->write(I)V

    .line 733
    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    .line 735
    :cond_0
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Ljava/io/BufferedWriter;->write(I)V

    .line 736
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->flush()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 740
    :try_start_6
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    move v0, v1

    .line 749
    :goto_0
    return v0

    .line 720
    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->d(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 738
    :catch_0
    move-exception v0

    move-object v0, v3

    .line 740
    :goto_1
    if-eqz v0, :cond_2

    .line 742
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    move v0, v2

    .line 745
    goto :goto_0

    .line 728
    :catchall_1
    move-exception v0

    :try_start_9
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;)Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;->readLock()Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantReadWriteLock$ReadLock;->unlock()V

    throw v0
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 740
    :catchall_2
    move-exception v0

    :goto_2
    if-eqz v3, :cond_1

    .line 742
    :try_start_a
    invoke-virtual {v3}, Ljava/io/BufferedWriter;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    .line 744
    :cond_1
    :goto_3
    throw v0

    :catch_1
    move-exception v0

    move v0, v2

    .line 745
    goto :goto_0

    .line 744
    :catch_2
    move-exception v0

    move v0, v2

    .line 745
    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_3

    .line 740
    :catchall_3
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_2

    .line 738
    :catch_4
    move-exception v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private c()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 767
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0, p0}, Lcom/android/debug/hv/ViewServer;->a(Lcom/android/debug/hv/ViewServer;Lcom/android/debug/hv/ViewServer$WindowListener;)V

    .line 768
    const/4 v2, 0x0

    .line 770
    :try_start_0
    new-instance v1, Ljava/io/BufferedWriter;

    new-instance v0, Ljava/io/OutputStreamWriter;

    iget-object v5, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-virtual {v5}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v1, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 771
    :cond_0
    :goto_0
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_5

    .line 774
    iget-object v5, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    monitor-enter v5
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 775
    :goto_1
    :try_start_2
    iget-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->c:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->d:Z

    if-nez v0, :cond_2

    .line 776
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 786
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v5

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 796
    :catch_0
    move-exception v0

    .line 797
    :goto_2
    :try_start_4
    const-string v2, "LocalViewServer"

    const-string v4, "Connection error: "

    invoke-static {v2, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 799
    if-eqz v1, :cond_1

    .line 801
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 806
    :cond_1
    :goto_3
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0, p0}, Lcom/android/debug/hv/ViewServer;->b(Lcom/android/debug/hv/ViewServer;Lcom/android/debug/hv/ViewServer$WindowListener;)V

    .line 808
    :goto_4
    return v3

    .line 778
    :cond_2
    :try_start_6
    iget-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->c:Z

    if-eqz v0, :cond_7

    .line 779
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->c:Z

    move v2, v3

    .line 782
    :goto_5
    iget-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->d:Z

    if-eqz v0, :cond_6

    .line 783
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->d:Z

    move v0, v3

    .line 786
    :goto_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 787
    if-eqz v2, :cond_3

    .line 788
    :try_start_7
    const-string v2, "LIST UPDATE\n"

    invoke-virtual {v1, v2}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 789
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V

    .line 791
    :cond_3
    if-eqz v0, :cond_0

    .line 792
    const-string v0, "FOCUS UPDATE\n"

    invoke-virtual {v1, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 793
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->flush()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_0

    .line 799
    :catchall_1
    move-exception v0

    :goto_7
    if-eqz v1, :cond_4

    .line 801
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    .line 806
    :cond_4
    :goto_8
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v1, p0}, Lcom/android/debug/hv/ViewServer;->b(Lcom/android/debug/hv/ViewServer;Lcom/android/debug/hv/ViewServer$WindowListener;)V

    throw v0

    .line 799
    :cond_5
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedWriter;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1

    .line 806
    :goto_9
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a:Lcom/android/debug/hv/ViewServer;

    invoke-static {v0, p0}, Lcom/android/debug/hv/ViewServer;->b(Lcom/android/debug/hv/ViewServer;Lcom/android/debug/hv/ViewServer$WindowListener;)V

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_8

    .line 799
    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_7

    .line 796
    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :cond_6
    move v0, v4

    goto :goto_6

    :cond_7
    move v2, v4

    goto :goto_5
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 753
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    monitor-enter v1

    .line 754
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->c:Z

    .line 755
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 756
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 760
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    monitor-enter v1

    .line 761
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->d:Z

    .line 762
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->e:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 763
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 530
    const/4 v2, 0x0

    .line 532
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    iget-object v3, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v3, 0x400

    invoke-direct {v1, v0, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 534
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 539
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v3

    .line 540
    const/4 v2, -0x1

    if-ne v3, v2, :cond_2

    .line 542
    const-string v2, ""

    move-object v5, v0

    move-object v0, v2

    move-object v2, v5

    .line 549
    :goto_0
    const-string v3, "PROTOCOL"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 550
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    const-string v3, "4"

    invoke-static {v0, v3}, Lcom/android/debug/hv/ViewServer;->a(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v0

    .line 563
    :goto_1
    if-nez v0, :cond_0

    .line 564
    const-string v0, "LocalViewServer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "An error occurred with the command: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 569
    :cond_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 577
    :goto_2
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    if-eqz v0, :cond_1

    .line 579
    :try_start_3
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 585
    :cond_1
    :goto_3
    return-void

    .line 544
    :cond_2
    const/4 v2, 0x0

    :try_start_4
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 545
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 551
    :cond_3
    const-string v3, "SERVER"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 552
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    const-string v3, "4"

    invoke-static {v0, v3}, Lcom/android/debug/hv/ViewServer;->a(Ljava/net/Socket;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    .line 553
    :cond_4
    const-string v3, "LIST"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 554
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-direct {p0, v0}, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a(Ljava/net/Socket;)Z

    move-result v0

    goto :goto_1

    .line 555
    :cond_5
    const-string v3, "GET_FOCUS"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 556
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-direct {p0, v0}, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b(Ljava/net/Socket;)Z

    move-result v0

    goto :goto_1

    .line 557
    :cond_6
    const-string v3, "AUTOLIST"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 558
    invoke-direct {p0}, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->c()Z

    move-result v0

    goto :goto_1

    .line 560
    :cond_7
    iget-object v3, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-direct {p0, v3, v2, v0}, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->a(Ljava/net/Socket;Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v0

    goto :goto_1

    .line 573
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 580
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 566
    :catch_2
    move-exception v0

    move-object v1, v2

    .line 567
    :goto_4
    :try_start_5
    const-string v2, "LocalViewServer"

    const-string v3, "Connection error: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 569
    if-eqz v1, :cond_8

    .line 571
    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 577
    :cond_8
    :goto_5
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    if-eqz v0, :cond_1

    .line 579
    :try_start_7
    iget-object v0, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_3

    .line 580
    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 573
    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 569
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_9

    .line 571
    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 577
    :cond_9
    :goto_7
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    if-eqz v1, :cond_a

    .line 579
    :try_start_9
    iget-object v1, p0, Lcom/android/debug/hv/ViewServer$ViewServerWorker;->b:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 582
    :cond_a
    :goto_8
    throw v0

    .line 573
    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 580
    :catch_6
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    .line 569
    :catchall_1
    move-exception v0

    goto :goto_6

    .line 566
    :catch_7
    move-exception v0

    goto :goto_4
.end method
