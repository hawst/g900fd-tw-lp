.class Lcom/helpshift/HSMessagesFragment$14;
.super Landroid/os/Handler;
.source "HSMessagesFragment.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/helpshift/HSMessagesFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSMessagesFragment;I)V
    .locals 0

    .prologue
    .line 823
    iput-object p1, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    iput p2, p0, Lcom/helpshift/HSMessagesFragment$14;->a:I

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 825
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->r(Lcom/helpshift/HSMessagesFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/helpshift/HSMessagesFragment$14;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 826
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 827
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    .line 828
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 829
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->c(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/customadapters/MessagesAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 831
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v3}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 832
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v2}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 836
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$14;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0, p1}, Lcom/helpshift/HSMessagesFragment;->a(Lcom/helpshift/HSMessagesFragment;Landroid/os/Message;)V

    .line 837
    return-void

    .line 833
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_0
.end method
