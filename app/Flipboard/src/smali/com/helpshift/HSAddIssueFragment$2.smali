.class Lcom/helpshift/HSAddIssueFragment$2;
.super Landroid/os/Handler;
.source "HSAddIssueFragment.java"


# instance fields
.field final synthetic a:Lcom/helpshift/HSAddIssueFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSAddIssueFragment;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    .prologue
    .line 115
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 116
    const-string v1, "response"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 117
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    const-string v2, "id"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/helpshift/HSAddIssueFragment;->a(Lcom/helpshift/HSAddIssueFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 118
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 119
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 120
    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v2}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v2

    const-string v3, "created_at"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "issuesTs"

    invoke-virtual {v2, v3, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(Lorg/json/JSONArray;)V

    .line 122
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->d(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "username"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->e(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "email"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    const-string v1, ""

    const-string v2, "replyText"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->j(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->f(Lcom/helpshift/HSAddIssueFragment;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 128
    const-string v0, "p"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 130
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->g(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->h(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "foregroundIssue"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v2}, Lcom/helpshift/HSAddIssueFragment;->h(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v3}, Lcom/helpshift/HSAddIssueFragment;->g(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/helpshift/util/AttachmentUtil;->a(Lcom/helpshift/HSStorage;Ljava/lang/String;Ljava/lang/String;Z)Lcom/helpshift/viewstructs/HSMsg;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/HSAddIssueFragment;->a(Lcom/helpshift/HSAddIssueFragment;Lcom/helpshift/viewstructs/HSMsg;)Lcom/helpshift/viewstructs/HSMsg;

    .line 134
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->l(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSApiClient;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->i(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v2}, Lcom/helpshift/HSAddIssueFragment;->j(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v3}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v3

    const-string v4, "identity"

    invoke-virtual {v3, v4}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v4}, Lcom/helpshift/HSAddIssueFragment;->h(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    const-string v6, "sc"

    iget-object v7, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v7}, Lcom/helpshift/HSAddIssueFragment;->k(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/viewstructs/HSMsg;

    move-result-object v7

    iget-object v7, v7, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v8, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v8}, Lcom/helpshift/HSAddIssueFragment;->k(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/viewstructs/HSMsg;

    move-result-object v8

    iget-object v8, v8, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/helpshift/HSApiClient;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->n(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSApiData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->m()V

    .line 144
    :goto_1
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$2;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->m(Lcom/helpshift/HSAddIssueFragment;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 141
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_1
.end method
