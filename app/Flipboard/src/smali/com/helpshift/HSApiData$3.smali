.class Lcom/helpshift/HSApiData$3;
.super Landroid/os/Handler;
.source "HSApiData.java"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic b:Lcom/helpshift/HSApiData;


# direct methods
.method constructor <init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/helpshift/HSApiData$3;->b:Lcom/helpshift/HSApiData;

    iput-object p2, p0, Lcom/helpshift/HSApiData$3;->a:Landroid/os/Handler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 170
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 173
    if-eqz v0, :cond_2

    .line 174
    const-string v1, "response"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 175
    iget-object v1, p0, Lcom/helpshift/HSApiData$3;->b:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1}, Lcom/helpshift/HSStorage;->f()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const-string v2, "bcl"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 176
    iget-object v1, p0, Lcom/helpshift/HSApiData$3;->b:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "bcl"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/Integer;)V

    .line 179
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSApiData$3;->b:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "config"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 180
    const-string v2, "pr"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 181
    const-string v2, "pr"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 182
    if-eqz v1, :cond_1

    const-string v3, "t"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "t"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 184
    iget-object v1, p0, Lcom/helpshift/HSApiData$3;->b:Lcom/helpshift/HSApiData;

    invoke-virtual {v1}, Lcom/helpshift/HSApiData;->f()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 187
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSApiData$3;->b:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "config"

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 190
    iget-object v1, p0, Lcom/helpshift/HSApiData$3;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 191
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 192
    iget-object v0, p0, Lcom/helpshift/HSApiData$3;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 194
    :cond_2
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method
