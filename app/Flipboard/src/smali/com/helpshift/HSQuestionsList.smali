.class public final Lcom/helpshift/HSQuestionsList;
.super Lcom/helpshift/HSActivity;
.source "HSQuestionsList.java"


# instance fields
.field private o:Lcom/helpshift/HSSectionPagerAdapter;

.field private p:Landroid/support/v4/view/ViewPager;

.field private q:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/helpshift/HSActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSQuestionsList;)Lcom/helpshift/HSSectionPagerAdapter;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/helpshift/HSQuestionsList;->o:Lcom/helpshift/HSSectionPagerAdapter;

    return-object v0
.end method


# virtual methods
.method public final onBackPressed()V
    .locals 0

    .prologue
    .line 118
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onBackPressed()V

    .line 119
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v2, 0x400

    const/4 v3, 0x0

    .line 38
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionsList;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "showInFullScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionsList;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 46
    :cond_0
    const-string v0, "layout"

    const-string v1, "hs__questions_list"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestionsList;->setContentView(I)V

    .line 49
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionsList;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sectionPublishId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    new-instance v1, Lcom/helpshift/HSSectionPagerAdapter;

    iget-object v2, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-direct {v1, v2, p0, v0}, Lcom/helpshift/HSSectionPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/helpshift/HSQuestionsList;->o:Lcom/helpshift/HSSectionPagerAdapter;

    .line 52
    const-string v0, "id"

    const-string v1, "hs__sections_pager"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestionsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/helpshift/HSQuestionsList;->p:Landroid/support/v4/view/ViewPager;

    .line 55
    iget-object v0, p0, Lcom/helpshift/HSQuestionsList;->p:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/helpshift/HSQuestionsList;->o:Lcom/helpshift/HSSectionPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 56
    iget-object v0, p0, Lcom/helpshift/HSQuestionsList;->p:Landroid/support/v4/view/ViewPager;

    new-instance v1, Lcom/helpshift/HSQuestionsList$1;

    invoke-direct {v1, p0}, Lcom/helpshift/HSQuestionsList$1;-><init>(Lcom/helpshift/HSQuestionsList;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 62
    iget-object v0, p0, Lcom/helpshift/HSQuestionsList;->p:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/helpshift/HSQuestionsList;->o:Lcom/helpshift/HSSectionPagerAdapter;

    iget v1, v1, Lcom/helpshift/HSSectionPagerAdapter;->b:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 64
    const-string v0, "id"

    const-string v1, "hs__pager_tab_strip"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestionsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/PagerTabStrip;

    move v2, v3

    .line 68
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/view/PagerTabStrip;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 69
    invoke-virtual {v0, v2}, Landroid/support/v4/view/PagerTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 70
    instance-of v4, v1, Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 71
    check-cast v1, Landroid/widget/TextView;

    .line 72
    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 73
    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 74
    iget-object v4, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    invoke-virtual {v4}, Lcom/helpshift/app/ActionBarHelper;->c()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [I

    const v6, 0x1010036

    aput v6, v5, v3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 77
    const v5, 0xffffff

    invoke-virtual {v4, v3, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    .line 78
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 83
    :cond_2
    const-string v1, "hs__actionBarTabIndicatorColor"

    invoke-static {p0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 84
    invoke-virtual {v0, v1}, Landroid/support/v4/view/PagerTabStrip;->setTabIndicatorColor(I)V

    .line 86
    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 87
    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->b()V

    .line 88
    const-string v1, "hs__faq_header"

    invoke-static {p0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelper;->a(Ljava/lang/String;)V

    .line 90
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->a:Ljava/util/Map;

    const-string v1, "hl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    const-string v0, "id"

    const-string v1, "hs__helpshiftActivityFooter"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestionsList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/helpshift/HSQuestionsList;->q:Landroid/widget/ImageView;

    .line 93
    iget-object v1, p0, Lcom/helpshift/HSQuestionsList;->q:Landroid/widget/ImageView;

    sget-object v0, Lcom/helpshift/res/drawable/HSImages;->a:Ljava/util/Map;

    const-string v2, "newHSLogo"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/helpshift/res/drawable/HSDraw;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    iget-object v0, p0, Lcom/helpshift/HSQuestionsList;->q:Landroid/widget/ImageView;

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 98
    :cond_3
    return-void
.end method

.method public final bridge synthetic onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 102
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 103
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionsList;->finish()V

    .line 105
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onResume()V

    .line 114
    return-void
.end method

.method public final bridge synthetic onStart()V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStart()V

    return-void
.end method

.method public final bridge synthetic onStop()V
    .locals 0

    .prologue
    .line 22
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStop()V

    return-void
.end method
