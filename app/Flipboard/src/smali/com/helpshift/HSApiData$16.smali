.class Lcom/helpshift/HSApiData$16;
.super Landroid/os/Handler;
.source "HSApiData.java"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic b:Lcom/helpshift/HSApiData;


# direct methods
.method constructor <init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 1102
    iput-object p1, p0, Lcom/helpshift/HSApiData$16;->b:Lcom/helpshift/HSApiData;

    iput-object p2, p0, Lcom/helpshift/HSApiData$16;->a:Landroid/os/Handler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1105
    iget-object v0, p0, Lcom/helpshift/HSApiData$16;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v11

    .line 1106
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 1108
    if-eqz v0, :cond_0

    .line 1109
    :try_start_0
    const-string v1, "response"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 1110
    new-instance v1, Lcom/helpshift/Faq;

    const-wide/16 v2, 0x0

    const-string v4, "id"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "publish_id"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/helpshift/HSApiData$16;->b:Lcom/helpshift/HSApiData;

    const-string v7, "section_id"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/helpshift/HSApiData;->a(Lcom/helpshift/HSApiData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "title"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "body"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const-string v12, "is_rtl"

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v12, "true"

    if-ne v0, v12, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-direct/range {v1 .. v10}, Lcom/helpshift/Faq;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Boolean;)V

    .line 1118
    iput-object v1, v11, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1119
    iget-object v0, p0, Lcom/helpshift/HSApiData$16;->a:Landroid/os/Handler;

    invoke-virtual {v0, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1120
    iget-object v0, p0, Lcom/helpshift/HSApiData$16;->b:Lcom/helpshift/HSApiData;

    invoke-static {v0}, Lcom/helpshift/HSApiData;->c(Lcom/helpshift/HSApiData;)Lcom/helpshift/HSFaqDataSource;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/helpshift/HSFaqDataSource;->a(Lcom/helpshift/Faq;)Lcom/helpshift/Faq;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1125
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v10

    .line 1110
    goto :goto_0

    .line 1122
    :catch_0
    move-exception v0

    .line 1123
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Exception in getting question "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
