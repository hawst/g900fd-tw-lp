.class final Lcom/helpshift/customviews/HSTextView;
.super Landroid/widget/TextView;
.source "HSTextView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const-string v0, "styleable"

    const-string v1, "HSTextView"

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v0

    const-string v1, "styleable"

    const-string v2, "HSTextView_font"

    invoke-static {p1, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/helpshift/util/HSCustomFont;->a(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;[II)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    const-string v0, "styleable"

    const-string v1, "HSTextView"

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)[I

    move-result-object v0

    const-string v1, "styleable"

    const-string v2, "HSTextView_font"

    invoke-static {p1, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {p0, p1, p2, v0, v1}, Lcom/helpshift/util/HSCustomFont;->a(Landroid/view/View;Landroid/content/Context;Landroid/util/AttributeSet;[II)V

    .line 28
    return-void
.end method
