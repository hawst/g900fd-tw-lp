.class public final Lcom/helpshift/HSApiData;
.super Ljava/lang/Object;
.source "HSApiData.java"


# static fields
.field public static e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/HSFaqSyncStatusEvents;",
            ">;"
        }
    .end annotation
.end field

.field protected static f:Z


# instance fields
.field public a:Lcom/helpshift/HSStorage;

.field public b:Lcom/helpshift/HSApiClient;

.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Faq;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/Iterator;

.field private g:Lcom/helpshift/HSSectionDataSource;

.field private h:Lcom/helpshift/HSFaqDataSource;

.field private final i:I

.field private j:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    .line 67
    const/4 v0, 0x0

    sput-boolean v0, Lcom/helpshift/HSApiData;->f:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v1, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    .line 53
    const/16 v0, 0xa

    iput v0, p0, Lcom/helpshift/HSApiData;->i:I

    .line 56
    iput-object v1, p0, Lcom/helpshift/HSApiData;->d:Ljava/util/Iterator;

    .line 69
    iput-object p1, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    .line 70
    new-instance v0, Lcom/helpshift/HSStorage;

    invoke-direct {v0, p1}, Lcom/helpshift/HSStorage;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    .line 71
    new-instance v0, Lcom/helpshift/HSApiClient;

    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "domain"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "appId"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v4, "apiKey"

    invoke-virtual {v3, v4}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/helpshift/HSApiClient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/HSStorage;)V

    iput-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    .line 75
    new-instance v0, Lcom/helpshift/HSSectionDataSource;

    invoke-direct {v0, p1}, Lcom/helpshift/HSSectionDataSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    .line 76
    new-instance v0, Lcom/helpshift/HSFaqDataSource;

    invoke-direct {v0, p1}, Lcom/helpshift/HSFaqDataSource;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSApiData;->h:Lcom/helpshift/HSFaqDataSource;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSApiData;)Lcom/helpshift/HSSectionDataSource;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    return-object v0
.end method

.method static synthetic a(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x190

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x258

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x1f7

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x1f8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/helpshift/HSApiData;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/helpshift/HSApiData;->r()Ljava/util/ArrayList;

    move-result-object v3

    const-string v1, ""

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    iget-object v4, v0, Lcom/helpshift/Section;->b:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcom/helpshift/Section;->d:Ljava/lang/String;

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, -0x1

    invoke-static {p0, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;I)V

    return-void
.end method

.method private static a(Landroid/os/Handler;I)V
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 353
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 354
    const-string v2, "status"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 356
    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 357
    return-void
.end method

.method private a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 988
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 989
    new-instance v4, Lcom/helpshift/HSApiData$12;

    invoke-direct {v4, p0, p1}, Lcom/helpshift/HSApiData$12;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    .line 1000
    new-instance v5, Lcom/helpshift/HSApiData$13;

    invoke-direct {v5, p0, p2}, Lcom/helpshift/HSApiData$13;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    .line 1010
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v1, "token"

    invoke-virtual {v3, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "profile-id"

    invoke-virtual {v3, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "POST"

    const-string v2, "/update-ua-token/"

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1012
    :cond_0
    return-void
.end method

.method private a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 297
    new-instance v4, Lcom/helpshift/HSApiData$4;

    invoke-direct {v4, p0, p1, p6, p2}, Lcom/helpshift/HSApiData$4;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;Ljava/lang/Boolean;Landroid/os/Handler;)V

    .line 331
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v1, "profile-id"

    invoke-virtual {v3, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "since"

    invoke-virtual {v3, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "mc"

    invoke-virtual {v3, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p7, :cond_0

    const-string v1, "chat-launch-source"

    invoke-virtual {v3, v1, p7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v1, "POST"

    const-string v2, "/my-issues/"

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 332
    return-void
.end method

.method private a(Landroid/os/Handler;Landroid/os/Handler;Ljava/util/HashMap;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 717
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    const-string v1, "POST"

    const-string v2, "/events/"

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 718
    return-void
.end method

.method private a(Landroid/os/Handler;Landroid/os/Handler;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v1, "message-ids"

    invoke-virtual {p3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "source"

    invoke-virtual {v3, v1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "read-at"

    invoke-virtual {v3, v1, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "POST"

    const-string v2, "/events/messages/seen/"

    move-object v4, p1

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1391
    return-void
.end method

.method protected static a(Lcom/helpshift/HSFaqSyncStatusEvents;)V
    .locals 1

    .prologue
    .line 1398
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1399
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    .line 1402
    :cond_0
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1403
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 884
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 885
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 886
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 884
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 888
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/helpshift/HSApiData;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 43
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const/4 v1, 0x0

    sput-object v1, Lcom/helpshift/HSStorage;->c:Ljava/util/HashMap;

    iget-object v1, v0, Lcom/helpshift/HSStorage;->a:Landroid/content/Context;

    const-string v2, "tfidf.db"

    invoke-virtual {v1, v2}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    const-string v1, "dbFlag"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {p0}, Lcom/helpshift/HSApiData;->a()V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Lcom/helpshift/HSSearch;->a(Ljava/util/ArrayList;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    sput-object v0, Lcom/helpshift/HSStorage;->c:Ljava/util/HashMap;

    :try_start_0
    iget-object v2, v1, Lcom/helpshift/HSStorage;->a:Landroid/content/Context;

    const-string v3, "tfidf.db"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v2

    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v3, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->flush()V

    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->close()V

    const-string v0, "dbFlag"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected static b(Lcom/helpshift/HSFaqSyncStatusEvents;)V
    .locals 1

    .prologue
    .line 1407
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 1408
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1410
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/helpshift/HSApiData;)Lcom/helpshift/HSFaqDataSource;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/helpshift/HSApiData;->h:Lcom/helpshift/HSFaqDataSource;

    return-object v0
.end method

.method static synthetic d(Lcom/helpshift/HSApiData;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    return-object v0
.end method

.method private d(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 6

    .prologue
    .line 1102
    new-instance v4, Lcom/helpshift/HSApiData$16;

    invoke-direct {v4, p0, p2}, Lcom/helpshift/HSApiData$16;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    .line 1127
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v1, "GET"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "/faqs/"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "/"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1128
    return-void
.end method

.method private f(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 6

    .prologue
    .line 119
    new-instance v4, Lcom/helpshift/HSApiData$1;

    invoke-direct {v4, p0, p1}, Lcom/helpshift/HSApiData$1;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    .line 150
    new-instance v5, Lcom/helpshift/HSApiData$2;

    invoke-direct {v5, p0, p2}, Lcom/helpshift/HSApiData$2;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    .line 160
    const/4 v0, 0x1

    sput-boolean v0, Lcom/helpshift/HSApiData;->f:Z

    .line 161
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    const-string v1, "GET"

    const-string v2, "/faqs/"

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 162
    return-void
.end method

.method private g(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 269
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->h:Lcom/helpshift/HSFaqDataSource;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSFaqDataSource;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 271
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method protected static k()V
    .locals 0

    .prologue
    .line 1148
    invoke-static {}, Lcom/helpshift/HSService;->a()V

    .line 1149
    return-void
.end method

.method protected static p()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1414
    sput-boolean v0, Lcom/helpshift/HSApiData;->f:Z

    .line 1415
    sget-object v1, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    move v1, v0

    .line 1416
    :goto_0
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1417
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSFaqSyncStatusEvents;

    .line 1418
    if-eqz v0, :cond_0

    .line 1419
    invoke-interface {v0}, Lcom/helpshift/HSFaqSyncStatusEvents;->a()V

    .line 1416
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1423
    :cond_1
    return-void
.end method

.method protected static q()V
    .locals 2

    .prologue
    .line 1427
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 1428
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1429
    sget-object v0, Lcom/helpshift/HSApiData;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSFaqSyncStatusEvents;

    .line 1430
    if-eqz v0, :cond_0

    .line 1431
    invoke-interface {v0}, Lcom/helpshift/HSFaqSyncStatusEvents;->j_()V

    .line 1428
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1435
    :cond_1
    return-void
.end method

.method private r()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 220
    const/4 v1, 0x0

    .line 222
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    invoke-virtual {v0}, Lcom/helpshift/HSSectionDataSource;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method private s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "deviceId"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 853
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 856
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/helpshift/HSApiData;->t()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private t()Ljava/lang/String;
    .locals 3

    .prologue
    .line 870
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 871
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "uuid"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 872
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 876
    iget-object v0, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 880
    :goto_0
    return-object v0

    .line 878
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/HSApiData;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;
    .locals 6

    .prologue
    .line 1239
    new-instance v0, Lcom/helpshift/HSApiData$18;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/helpshift/HSApiData$18;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;ILorg/json/JSONObject;Ljava/lang/String;)V

    .line 1264
    return-object v0
.end method

.method protected final a(Ljava/lang/String;Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 1540
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->l()Lorg/json/JSONObject;

    move-result-object v1

    .line 1541
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1543
    if-nez v1, :cond_0

    .line 1544
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 1550
    :cond_0
    :try_start_0
    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->f:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    if-eq p2, v2, :cond_1

    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->e:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    if-eq p2, v2, :cond_1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    sget-object v3, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->e:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {v3}, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_2

    invoke-virtual {v1, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    sget-object v3, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->f:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {v3}, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 1555
    :cond_1
    invoke-virtual {p2}, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->ordinal()I

    move-result v2

    invoke-virtual {v1, p1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1556
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1557
    iget-object v2, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "issueCSatStates"

    invoke-virtual {v2, v3, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1560
    :cond_2
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 257
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 259
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->h:Lcom/helpshift/HSFaqDataSource;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSFaqDataSource;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method protected final a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Section;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 231
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 232
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSApiData;->a(Lcom/helpshift/Section;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 233
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 236
    :cond_1
    return-object v2
.end method

.method final a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 103
    invoke-direct {p0}, Lcom/helpshift/HSApiData;->r()Ljava/util/ArrayList;

    move-result-object v4

    .line 104
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    move v1, v2

    .line 106
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 107
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    .line 108
    iget-object v0, v0, Lcom/helpshift/Section;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/helpshift/HSApiData;->g(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    move v3, v2

    .line 109
    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 110
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 111
    iget-object v6, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 106
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_1
    return-void
.end method

.method protected final a(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 201
    const/4 v1, 0x0

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    invoke-virtual {v0}, Lcom/helpshift/HSSectionDataSource;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 210
    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 211
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 212
    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 213
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/helpshift/HSApiData;->f(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 217
    return-void

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method protected final a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 365
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 366
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "identity"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 367
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 368
    const-string v1, "success"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 369
    const-string v1, "ts"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 370
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->b()Lorg/json/JSONArray;

    move-result-object v5

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    const-string v1, "messages"

    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-lez v1, :cond_0

    invoke-virtual {v7, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    const-string v9, "id"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "localRscMessage_"

    invoke-virtual {v9, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_1

    const-string v1, "id"

    invoke-virtual {v6, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v6, "created_at"

    invoke-virtual {v8, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v1, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 378
    :goto_2
    return-void

    .line 373
    :cond_3
    const-string v4, ""

    const-string v5, ""

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_2

    .line 376
    :cond_4
    const/16 v0, 0x193

    invoke-static {p2, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;I)V

    goto :goto_2
.end method

.method protected final a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 8

    .prologue
    .line 526
    new-instance v0, Lcom/helpshift/HSApiData$7;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/helpshift/HSApiData$7;-><init>(Lcom/helpshift/HSApiData;Ljava/lang/String;Ljava/lang/Boolean;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 547
    new-instance v6, Lcom/helpshift/HSApiData$8;

    invoke-direct {v6, p0, p3, p4, p2}, Lcom/helpshift/HSApiData$8;-><init>(Lcom/helpshift/HSApiData;Ljava/lang/String;Ljava/lang/Boolean;Landroid/os/Handler;)V

    .line 557
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 558
    iget-object v1, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v2, "POST"

    new-instance v3, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "/faqs/"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "/helpful/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 563
    :goto_0
    return-void

    .line 560
    :cond_0
    iget-object v1, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v2, "POST"

    new-instance v3, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "/faqs/"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "/unhelpful/"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method protected final a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 473
    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v7}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 474
    return-void
.end method

.method protected final a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    .line 483
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 484
    new-instance v0, Lcom/helpshift/HSApiData$6;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move/from16 v6, p7

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/helpshift/HSApiData$6;-><init>(Lcom/helpshift/HSApiData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Handler;)V

    .line 500
    iget-object v1, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    const-string v2, "ca"

    if-ne p5, v2, :cond_1

    const-string p4, "Accepted the solution"

    :cond_0
    :goto_0
    const-string v2, "profile-id"

    invoke-virtual {v4, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "message-text"

    invoke-virtual {v4, v2, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "type"

    invoke-virtual {v4, v2, p5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "refers"

    invoke-virtual {v4, v2, p6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "POST"

    new-instance v3, Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/issues/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/messages/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v5, p1

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 502
    return-void

    .line 500
    :cond_1
    const-string v2, "ncr"

    if-ne p5, v2, :cond_2

    const-string p4, "Did not accept the solution"

    goto :goto_0

    :cond_2
    const-string v2, "ar"

    if-ne p5, v2, :cond_0

    const-string p4, "Accepted review request"

    goto :goto_0
.end method

.method protected final a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 431
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "identity"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 433
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 434
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "deviceId"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    invoke-direct {p0}, Lcom/helpshift/HSApiData;->s()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/Meta;->a(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 435
    :goto_1
    if-eqz p4, :cond_0

    .line 437
    :try_start_0
    const-string v0, "user_info"

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p4}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 439
    :cond_0
    :goto_2
    new-instance v0, Lcom/helpshift/HSApiData$5;

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/HSApiData$5;-><init>(Lcom/helpshift/HSApiData;Lorg/json/JSONObject;Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    iget-object v1, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v2, v1

    move-object v3, p1

    move-object v4, v0

    move-object v6, p3

    invoke-virtual/range {v2 .. v7}, Lcom/helpshift/HSApiClient;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 462
    return-void

    .line 434
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/Meta;->a(Landroid/content/Context;Ljava/lang/Boolean;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    goto :goto_1

    .line 463
    :cond_3
    new-instance v0, Lcom/helpshift/exceptions/IdentityException;

    const-string v1, "Identity not found"

    invoke-direct {v0, v1}, Lcom/helpshift/exceptions/IdentityException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected final a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 7

    .prologue
    const/16 v3, 0x190

    .line 1448
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_3

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_3

    .line 1449
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1450
    invoke-virtual {p0, p3}, Lcom/helpshift/HSApiData;->f(Ljava/lang/String;)Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    move-result-object v0

    .line 1451
    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->c:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->f:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    if-ne v0, v2, :cond_2

    .line 1453
    :cond_0
    new-instance v4, Lcom/helpshift/HSApiData$21;

    invoke-direct {v4, p0, p4, p3}, Lcom/helpshift/HSApiData$21;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;Ljava/lang/String;)V

    .line 1466
    sget-object v0, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->e:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {p0, p3, v0}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;)Ljava/lang/Boolean;

    .line 1468
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1470
    :try_start_0
    const-string v2, "r"

    invoke-virtual {v0, v2, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1471
    const-string v2, "f"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1472
    const-string v2, "id"

    invoke-virtual {v0, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1474
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "csat_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {p0, p5, v2, v3, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;

    move-result-object v0

    .line 1479
    new-instance v5, Lcom/helpshift/HSApiData$22;

    invoke-direct {v5, p0, p3, p4, v0}, Lcom/helpshift/HSApiData$22;-><init>(Lcom/helpshift/HSApiData;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1496
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v2, "rating"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "feedback"

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    const-string v1, "POST"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "/issues/"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "/customer-survey/"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1517
    :goto_1
    return-void

    .line 1499
    :cond_2
    invoke-virtual {p5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1500
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1501
    const-string v2, "status"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1502
    const-string v2, "reason"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "CSat survey already done for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1504
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1505
    invoke-virtual {p5, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 1508
    :cond_3
    invoke-virtual {p5}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1510
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1511
    const-string v2, "status"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1512
    const-string v2, "reason"

    const-string v3, "Rating not in range"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1514
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1515
    invoke-virtual {p5, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 1043
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSSectionDataSource;->a(Ljava/lang/String;)Lcom/helpshift/Section;

    move-result-object v0

    .line 1044
    if-eqz v0, :cond_0

    .line 1045
    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1046
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1047
    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1049
    :cond_0
    new-instance v0, Lcom/helpshift/HSApiData$15;

    invoke-direct {v0, p0, p1, p2}, Lcom/helpshift/HSApiData$15;-><init>(Lcom/helpshift/HSApiData;Ljava/lang/String;Landroid/os/Handler;)V

    .line 1061
    invoke-direct {p0, v0, p3}, Lcom/helpshift/HSApiData;->f(Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1064
    :goto_0
    return-void

    .line 1063
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 506
    const-string v1, "invisible"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 507
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1, v0}, Lcom/helpshift/HSStorage;->a(Lorg/json/JSONObject;)V

    .line 508
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 519
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 520
    const-string v1, "inProgress"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 521
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1, v0}, Lcom/helpshift/HSStorage;->a(Lorg/json/JSONObject;)V

    .line 522
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "apiKey"

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "domain"

    invoke-virtual {v0, v1, p2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "appId"

    invoke-virtual {v0, v1, p3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    new-instance v0, Lcom/helpshift/HSApiClient;

    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-direct {v0, p2, p3, p1, v1}, Lcom/helpshift/HSApiClient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/HSStorage;)V

    iput-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    .line 100
    return-void
.end method

.method final a(Lorg/json/JSONArray;)V
    .locals 13

    .prologue
    .line 668
    invoke-direct {p0}, Lcom/helpshift/HSApiData;->t()Ljava/lang/String;

    move-result-object v3

    .line 669
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 670
    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    const-string v5, "3.5.0"

    .line 671
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "sdkType"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 672
    invoke-direct {p0}, Lcom/helpshift/HSApiData;->s()Ljava/lang/String;

    move-result-object v1

    .line 673
    const/4 v2, 0x0

    .line 674
    sget-object v7, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 675
    sget-object v8, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 676
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iget-object v0, v0, Lcom/helpshift/HSStorage;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/util/Meta;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    .line 677
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "os.version"

    invoke-static {v10}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v10, ":"

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v10, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 678
    iget-object v0, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    const-string v11, "phone"

    invoke-virtual {v0, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 679
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v11

    .line 680
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v12

    .line 683
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 687
    :goto_0
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 688
    const-string v2, "id"

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 689
    if-eqz v0, :cond_0

    .line 690
    const-string v2, "uid"

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 692
    :cond_0
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 693
    const-string v0, "profile-id"

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_1
    const-string v0, "v"

    invoke-virtual {v1, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 696
    const-string v0, "e"

    invoke-virtual {p1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 697
    const-string v0, "s"

    invoke-virtual {v1, v0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    const-string v0, "dm"

    invoke-virtual {v1, v0, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 699
    const-string v0, "os"

    invoke-virtual {v1, v0, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 700
    const-string v0, "av"

    invoke-virtual {v1, v0, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 701
    const-string v0, "rs"

    invoke-virtual {v1, v0, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 702
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 703
    const-string v0, "cc"

    invoke-virtual {v1, v0, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 705
    :cond_2
    const-string v0, "ln"

    invoke-virtual {v1, v0, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 707
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "action_event_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 708
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    const/4 v3, 0x2

    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {p0, v2, v0, v3, v4}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;

    move-result-object v0

    .line 713
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v2, v0, v1}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/util/HashMap;)V

    .line 714
    return-void

    :cond_3
    move-object v0, v2

    goto/16 :goto_0
.end method

.method protected final a(Lcom/helpshift/Section;)Z
    .locals 1

    .prologue
    .line 252
    iget-object v0, p1, Lcom/helpshift/Section;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 253
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method protected final b()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Section;",
            ">;"
        }
    .end annotation

    .prologue
    .line 240
    const/4 v1, 0x0

    .line 241
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 243
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    invoke-virtual {v0}, Lcom/helpshift/HSSectionDataSource;->a()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    invoke-virtual {p0, v0}, Lcom/helpshift/HSApiData;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 248
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method protected final b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v1, 0x0

    .line 566
    iget-object v0, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 567
    invoke-virtual {p0}, Lcom/helpshift/HSApiData;->a()V

    .line 570
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 572
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 574
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v4, "dbFlag"

    invoke-virtual {v0, v4}, Lcom/helpshift/HSStorage;->e(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 575
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-static {}, Lcom/helpshift/HSStorage;->i()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/helpshift/HSSearch;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/ArrayList;

    move-result-object v0

    .line 577
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v3, v5, :cond_1

    .line 578
    invoke-virtual {v0, v1, v5}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 583
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 584
    iget-object v3, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 585
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 588
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 589
    iget-object v0, p0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 590
    iget-object v4, v0, Lcom/helpshift/Faq;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    .line 591
    invoke-virtual {v4, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 592
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 588
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 597
    :cond_4
    return-object v2
.end method

.method protected final b(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 6

    .prologue
    .line 278
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "config"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 280
    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 281
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 282
    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 284
    :cond_0
    new-instance v4, Lcom/helpshift/HSApiData$3;

    invoke-direct {v4, p0, p1}, Lcom/helpshift/HSApiData$3;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    iget-object v0, p0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v1, "GET"

    const-string v2, "/config/"

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 285
    return-void
.end method

.method protected final b(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 1075
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSSectionDataSource;->a(Ljava/lang/String;)Lcom/helpshift/Section;

    move-result-object v0

    .line 1076
    if-eqz v0, :cond_0

    .line 1077
    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1078
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1079
    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1086
    :goto_0
    return-void

    .line 1081
    :cond_0
    invoke-virtual {p3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1082
    invoke-virtual {p3, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1085
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1346
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 1347
    sget-object v1, Lcom/helpshift/util/HSFormat;->f:Ljava/text/DecimalFormat;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    .line 1350
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1, p1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1351
    const-string v2, "messages"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1352
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 1354
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 1355
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 1356
    const-string v7, "origin"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "mobile"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "seen"

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1358
    const-string v7, "id"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1359
    const-string v7, "seen"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1361
    :cond_0
    invoke-virtual {v4, v6}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1364
    :cond_1
    const-string v0, "messages"

    invoke-virtual {v1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1365
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "id"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1367
    :goto_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1373
    :try_start_1
    const-string v1, "mids"

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1374
    const-string v1, "src"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1375
    const-string v1, "at"

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1377
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "msg_seen_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1381
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    const/4 v4, 0x3

    invoke-virtual {p0, v2, v1, v4, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;

    move-result-object v2

    .line 1384
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    move-object v0, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;)V

    .line 1386
    return-void

    :catch_0
    move-exception v1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected final b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 513
    const-string v1, "screenshot"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 514
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1, v0}, Lcom/helpshift/HSStorage;->a(Lorg/json/JSONObject;)V

    .line 515
    return-void
.end method

.method protected final c()V
    .locals 8

    .prologue
    .line 644
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 645
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 647
    :try_start_0
    const-string v2, "ts"

    sget-object v3, Lcom/helpshift/util/HSFormat;->f:Ljava/text/DecimalFormat;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 648
    const-string v2, "t"

    const-string v3, "a"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 649
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 651
    :goto_0
    invoke-virtual {p0, v0}, Lcom/helpshift/HSApiData;->a(Lorg/json/JSONArray;)V

    .line 654
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final c(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 360
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V

    .line 361
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v0, 0x0

    .line 892
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "cachedImages"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 893
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v0

    .line 895
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 896
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 895
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 899
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v3, v1, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 902
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v5, :cond_2

    .line 903
    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-interface {v3, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 905
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 906
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 907
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 906
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 909
    :cond_1
    const/16 v0, 0xa

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v3, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSApiData;->a(Ljava/util/List;)V

    .line 910
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "cachedImages"

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONArray;)V

    .line 921
    :goto_2
    return-void

    .line 912
    :cond_2
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 913
    :goto_3
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 914
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 913
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 916
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "cachedImages"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONArray;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 920
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected final c(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2

    .prologue
    .line 1131
    const/4 v0, 0x0

    .line 1133
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSApiData;->h:Lcom/helpshift/HSFaqDataSource;

    invoke-virtual {v1, p1}, Lcom/helpshift/HSFaqDataSource;->c(Ljava/lang/String;)Lcom/helpshift/Faq;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1135
    :goto_0
    if-nez v0, :cond_0

    .line 1138
    invoke-direct {p0, p1, p2, p3}, Lcom/helpshift/HSApiData;->d(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1145
    :goto_1
    return-void

    .line 1140
    :cond_0
    invoke-virtual {p2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 1141
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1142
    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1143
    invoke-direct {p0, p1, p2, p3}, Lcom/helpshift/HSApiData;->d(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected final d(Ljava/lang/String;)Lcom/helpshift/Section;
    .locals 1

    .prologue
    .line 1069
    iget-object v0, p0, Lcom/helpshift/HSApiData;->g:Lcom/helpshift/HSSectionDataSource;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSSectionDataSource;->a(Ljava/lang/String;)Lcom/helpshift/Section;

    move-result-object v0

    return-object v0
.end method

.method protected final d()Ljava/lang/Boolean;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 724
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "reviewed"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->d(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_1

    .line 725
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->b:Ljava/util/Map;

    const-string v1, "pr"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 726
    sget-object v1, Lcom/helpshift/res/values/HSConfig;->b:Ljava/util/Map;

    const-string v2, "rurl"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 727
    if-eqz v0, :cond_1

    const-string v2, "s"

    invoke-virtual {v0, v2, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 728
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1}, Lcom/helpshift/HSStorage;->g()I

    move-result v1

    .line 729
    const-string v2, "t"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 730
    const-string v3, "i"

    invoke-virtual {v0, v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 731
    if-lez v0, :cond_1

    .line 732
    const-string v3, "l"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-lt v1, v0, :cond_0

    .line 734
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 743
    :goto_0
    return-object v0

    .line 735
    :cond_0
    const-string v3, "s"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    int-to-long v4, v1

    sub-long/2addr v2, v4

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    .line 738
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 743
    :cond_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected final d(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 8

    .prologue
    .line 395
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->c()Ljava/util/HashMap;

    move-result-object v0

    .line 396
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "identity"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 397
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 398
    const-string v1, "success"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->b()Lorg/json/JSONArray;

    move-result-object v0

    .line 400
    invoke-virtual {p1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 401
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 402
    invoke-virtual {p1, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 409
    :goto_0
    return-void

    .line 404
    :cond_0
    const-string v4, ""

    const-string v5, ""

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v7}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    goto :goto_0

    .line 407
    :cond_1
    const/16 v0, 0x193

    invoke-static {p2, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;I)V

    goto :goto_0
.end method

.method protected final e(Ljava/lang/String;)Lorg/json/JSONArray;
    .locals 6

    .prologue
    .line 1189
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "failedMessages"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "issue_id"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "messages"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/helpshift/HSStorage;->a(Lorg/json/JSONArray;Lorg/json/JSONArray;)Lorg/json/JSONArray;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1192
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    goto :goto_1
.end method

.method protected final e()V
    .locals 7

    .prologue
    .line 761
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->g()I

    move-result v0

    .line 762
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1}, Lcom/helpshift/HSStorage;->h()I

    move-result v1

    .line 764
    if-nez v0, :cond_2

    .line 766
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 769
    :goto_0
    add-int/lit8 v0, v0, 0x1

    .line 770
    iget-object v2, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v2, v0}, Lcom/helpshift/HSStorage;->b(I)V

    .line 772
    :try_start_0
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->b:Ljava/util/Map;

    const-string v2, "pr"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "config"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lorg/json/JSONObject;->length()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v2}, Lcom/helpshift/res/values/HSConfig;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 773
    :cond_0
    :goto_1
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->b:Ljava/util/Map;

    const-string v2, "pr"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 774
    if-eqz v0, :cond_1

    .line 775
    const-string v2, "t"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 776
    const-string v2, "l"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 777
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->h()I

    move-result v1

    .line 780
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(I)V

    .line 781
    return-void

    .line 772
    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "JSON Exception:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_0
.end method

.method protected final e(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 630
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 634
    :goto_0
    return-void

    .line 631
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method protected final f(Ljava/lang/String;)Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;
    .locals 2

    .prologue
    .line 1522
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->b:Ljava/util/Map;

    const-string v1, "csat"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1523
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->l()Lorg/json/JSONObject;

    move-result-object v0

    .line 1524
    if-eqz v0, :cond_0

    .line 1526
    :try_start_0
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1527
    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1528
    invoke-static {}, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->values()[Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    move-result-object v1

    aget-object v0, v1, v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1531
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    sget-object v0, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->a:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    goto :goto_0
.end method

.method protected final f()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 786
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1}, Lcom/helpshift/HSStorage;->g()I

    move-result v1

    .line 789
    :try_start_0
    iget-object v2, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "config"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "pr"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 790
    if-eqz v2, :cond_1

    .line 791
    const-string v3, "t"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 792
    const-string v3, "s"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 793
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 797
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1, v0}, Lcom/helpshift/HSStorage;->a(I)V

    .line 798
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->b(I)V

    .line 802
    :cond_1
    :goto_1
    return-void

    .line 794
    :cond_2
    const-string v3, "l"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 801
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected final g()V
    .locals 3

    .prologue
    .line 810
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "reviewed"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 811
    return-void
.end method

.method protected final h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 861
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "uuid"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 862
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 863
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 864
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "uuid"

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :cond_0
    return-object v0
.end method

.method protected final i()V
    .locals 6

    .prologue
    .line 968
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 969
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "deviceToken"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 971
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 973
    :try_start_0
    const-string v3, "profile-id"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 974
    const-string v3, "device-token"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 976
    :goto_0
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "push_token_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {p0, v3, v4, v5, v2}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;

    move-result-object v2

    .line 982
    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v3, v2, v0, v1}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method protected final j()V
    .locals 2

    .prologue
    .line 1032
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/helpshift/HSApiData$14;

    invoke-direct {v1, p0}, Lcom/helpshift/HSApiData$14;-><init>(Lcom/helpshift/HSApiData;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 1037
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setDaemon(Z)V

    .line 1038
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1039
    return-void
.end method

.method protected final l()V
    .locals 3

    .prologue
    .line 1152
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    const-class v2, Lcom/helpshift/HSService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1153
    iget-object v1, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 1154
    return-void
.end method

.method protected final m()V
    .locals 4

    .prologue
    .line 1157
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1158
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1159
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "deviceToken"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1162
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "appConfig"

    invoke-virtual {v0, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1163
    const-string v3, "enableInAppNotification"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1164
    const-string v3, "enableInAppNotification"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1167
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "unreg"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1172
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1173
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "activeConversation"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1174
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1175
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    const-class v2, Lcom/helpshift/HSService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1176
    iget-object v1, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v2, "libraryVersion"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "3.5.0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1178
    invoke-virtual {p0}, Lcom/helpshift/HSApiData;->l()V

    .line 1180
    :cond_1
    iget-object v1, p0, Lcom/helpshift/HSApiData;->j:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1185
    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    :cond_3
    move-object v0, v1

    goto :goto_0

    .line 1183
    :cond_4
    invoke-virtual {p0}, Lcom/helpshift/HSApiData;->l()V

    goto :goto_1
.end method

.method protected final n()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1200
    :try_start_0
    iget-object v3, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "failedMessages"

    invoke-virtual {v3, v1}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_3

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    if-nez v1, :cond_0

    const-string v6, "state"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    const/4 v7, -0x2

    if-le v6, v7, :cond_0

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    move-object v0, v1

    goto :goto_1

    :cond_1
    const-string v0, "failedMessages"

    invoke-virtual {v3, v0, v5}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONArray;)V

    move-object v0, v1

    .line 1202
    :goto_2
    if-nez v0, :cond_2

    .line 1223
    :goto_3
    return-void

    .line 1206
    :cond_2
    new-instance v1, Lcom/helpshift/HSApiData$17;

    invoke-direct {v1, p0}, Lcom/helpshift/HSApiData$17;-><init>(Lcom/helpshift/HSApiData;)V

    .line 1215
    const-string v2, "issue_id"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "body"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "type"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "refers"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "state"

    const/4 v7, 0x0

    invoke-virtual {v0, v2, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    move-object v0, p0

    move-object v2, v1

    invoke-virtual/range {v0 .. v7}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 1222
    :catch_0
    move-exception v0

    goto :goto_3

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method protected final declared-synchronized o()V
    .locals 9

    .prologue
    .line 1290
    monitor-enter p0

    :try_start_0
    new-instance v2, Lcom/helpshift/HSApiData$20;

    invoke-direct {v2, p0}, Lcom/helpshift/HSApiData$20;-><init>(Lcom/helpshift/HSApiData;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1298
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v1, "failedApiCalls"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1300
    iget-object v0, p0, Lcom/helpshift/HSApiData;->d:Ljava/util/Iterator;

    if-nez v0, :cond_0

    .line 1301
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSApiData;->d:Ljava/util/Iterator;

    .line 1304
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSApiData;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1305
    iget-object v0, p0, Lcom/helpshift/HSApiData;->d:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1306
    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 1307
    const-string v3, "p"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 1308
    const-string v3, "t"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 1310
    new-instance v1, Lcom/helpshift/HSApiData$19;

    invoke-direct {v1, p0, v2, v0}, Lcom/helpshift/HSApiData$19;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;Ljava/lang/String;)V

    .line 1311
    invoke-virtual {p0, v2, v0, v3, v6}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1313
    packed-switch v3, :pswitch_data_0

    .line 1342
    :goto_0
    monitor-exit p0

    return-void

    .line 1315
    :pswitch_0
    :try_start_2
    const-string v0, "f"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "h"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 1341
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1319
    :pswitch_1
    const-string v0, "profile-id"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "device-token"

    invoke-virtual {v6, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V

    .line 1322
    :pswitch_2
    invoke-static {v6}, Lcom/helpshift/util/HSJSONUtils;->b(Lorg/json/JSONObject;)Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v1, v2, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/util/HashMap;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1290
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1325
    :pswitch_3
    :try_start_3
    const-string v0, "mids"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const-string v0, "src"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "at"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Lorg/json/JSONArray;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1330
    :pswitch_4
    const-string v0, "r"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const-string v0, "f"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v0, "id"

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object v3, p0

    move-object v7, v1

    move-object v8, v2

    invoke-virtual/range {v3 .. v8}, Lcom/helpshift/HSApiData;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0

    .line 1338
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/HSApiData;->d:Ljava/util/Iterator;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1313
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
