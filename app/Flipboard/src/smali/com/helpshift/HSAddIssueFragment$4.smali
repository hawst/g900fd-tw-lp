.class Lcom/helpshift/HSAddIssueFragment$4;
.super Landroid/os/Handler;
.source "HSAddIssueFragment.java"


# instance fields
.field final synthetic a:Lcom/helpshift/HSAddIssueFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSAddIssueFragment;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 158
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 159
    const-string v1, "response"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 161
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 162
    const-string v2, "type"

    const-string v3, "url"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 163
    const-string v2, "body"

    const-string v3, "meta"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "attachments"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "url"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 164
    const-string v2, "id"

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v3}, Lcom/helpshift/HSAddIssueFragment;->h(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 165
    const-string v2, "m"

    invoke-static {v2, v1}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 168
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-virtual {v1}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v2}, Lcom/helpshift/HSAddIssueFragment;->n(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSApiData;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v3}, Lcom/helpshift/HSAddIssueFragment;->g(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "id"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/helpshift/util/AttachmentUtil;->a(Landroid/app/Activity;Lcom/helpshift/HSApiData;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 173
    :goto_0
    :try_start_1
    const-string v1, "meta"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "refers"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 178
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v2}, Lcom/helpshift/HSAddIssueFragment;->h(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v0}, Lcom/helpshift/HSAddIssueFragment;->n(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSApiData;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v1}, Lcom/helpshift/HSAddIssueFragment;->p(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment$4;->a:Lcom/helpshift/HSAddIssueFragment;

    invoke-static {v2}, Lcom/helpshift/HSAddIssueFragment;->p(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSApiData;->c(Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 184
    :goto_1
    return-void

    .line 183
    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_0

    .line 171
    :catch_2
    move-exception v1

    goto :goto_0
.end method
