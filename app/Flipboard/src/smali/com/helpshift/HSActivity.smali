.class Lcom/helpshift/HSActivity;
.super Lcom/helpshift/app/ActionBarActivity;
.source "HSActivity.java"


# instance fields
.field private final o:I

.field private p:Landroid/os/Bundle;

.field private q:Z

.field private r:Z

.field private s:Landroid/view/MenuItem;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Ljava/lang/Thread;

.field private w:Landroid/os/Handler;

.field private x:Lcom/helpshift/HSStorage;

.field private y:Lcom/helpshift/HSApiData;

.field private z:Landroid/view/Menu;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarActivity;-><init>()V

    .line 30
    const/4 v0, 0x3

    iput v0, p0, Lcom/helpshift/HSActivity;->o:I

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/HSActivity;->z:Landroid/view/Menu;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSActivity;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/helpshift/HSActivity;->w:Landroid/os/Handler;

    return-object p1
.end method

.method private a(Landroid/view/Menu;)V
    .locals 5

    .prologue
    .line 198
    const-string v0, "menu"

    const-string v1, "hs__show_conversation"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 199
    const-string v1, "id"

    const-string v2, "hs__action_report_issue"

    invoke-static {p0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 200
    const-string v2, "id"

    const-string v3, "hs__notifcation_badge"

    invoke-static {p0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 201
    const-string v3, "id"

    const-string v4, "hs__conversation_icon"

    invoke-static {p0, v3, v4}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 203
    invoke-virtual {p0}, Lcom/helpshift/HSActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v4

    .line 204
    invoke-virtual {v4, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 206
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSActivity;->s:Landroid/view/MenuItem;

    .line 207
    iget-object v0, p0, Lcom/helpshift/HSActivity;->s:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/helpshift/view/SimpleMenuItemCompat;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 209
    if-nez v0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 213
    :cond_0
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/helpshift/HSActivity;->t:Landroid/widget/TextView;

    .line 214
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/helpshift/HSActivity;->u:Landroid/widget/TextView;

    .line 216
    iget-object v1, p0, Lcom/helpshift/HSActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 217
    iget-object v1, p0, Lcom/helpshift/HSActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/helpshift/util/HSIcons;->d(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 219
    new-instance v1, Lcom/helpshift/HSActivity$2;

    invoke-direct {v1, p0}, Lcom/helpshift/HSActivity$2;-><init>(Lcom/helpshift/HSActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 225
    invoke-direct {p0}, Lcom/helpshift/HSActivity;->g()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/helpshift/HSActivity;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/helpshift/HSActivity;->g()V

    return-void
.end method

.method static synthetic b(Lcom/helpshift/HSActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/helpshift/HSActivity;->w:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/helpshift/HSActivity;)Lcom/helpshift/HSApiData;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/helpshift/HSActivity;->y:Lcom/helpshift/HSApiData;

    return-object v0
.end method

.method static synthetic d(Lcom/helpshift/HSActivity;)V
    .locals 3

    .prologue
    .line 27
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/helpshift/HSConversation;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "showInFullScreen"

    invoke-static {p0}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "chatLaunchSource"

    const-string v2, "support"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSActivity;->p:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/helpshift/HSActivity;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/helpshift/HSActivity;->w:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/helpshift/HSActivity;->w:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 50
    :cond_0
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 96
    iget-object v0, p0, Lcom/helpshift/HSActivity;->x:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->d()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 97
    iget-object v1, p0, Lcom/helpshift/HSActivity;->t:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 98
    if-lez v0, :cond_1

    .line 99
    iget-object v1, p0, Lcom/helpshift/HSActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 100
    iget-object v1, p0, Lcom/helpshift/HSActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 101
    iget-object v1, p0, Lcom/helpshift/HSActivity;->t:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 104
    iget-object v0, p0, Lcom/helpshift/HSActivity;->t:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected final b(Z)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/helpshift/HSActivity;->s:Landroid/view/MenuItem;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcom/helpshift/HSActivity;->s:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSActivity;->z:Landroid/view/Menu;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/helpshift/HSActivity;->z:Landroid/view/Menu;

    invoke-direct {p0, v0}, Lcom/helpshift/HSActivity;->a(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method protected final e()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/helpshift/HSActivity;->f()V

    .line 56
    iget-object v0, p0, Lcom/helpshift/HSActivity;->x:Lcom/helpshift/HSStorage;

    const-string v1, "activeConversation"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 61
    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/helpshift/HSActivity$1;

    invoke-direct {v1, p0}, Lcom/helpshift/HSActivity$1;-><init>(Lcom/helpshift/HSActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/helpshift/HSActivity;->v:Ljava/lang/Thread;

    .line 92
    iget-object v0, p0, Lcom/helpshift/HSActivity;->v:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 230
    invoke-super {p0, p1, p2, p3}, Lcom/helpshift/app/ActionBarActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 231
    if-eqz p3, :cond_0

    .line 232
    const-string v0, "callFinish"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 233
    if-ne p1, v3, :cond_0

    if-ne p2, v2, :cond_0

    .line 235
    instance-of v1, p0, Lcom/helpshift/HSConversation;

    if-eqz v1, :cond_1

    .line 236
    invoke-virtual {p0}, Lcom/helpshift/HSActivity;->onBackPressed()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "callFinish"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v2, v0}, Lcom/helpshift/HSActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/helpshift/HSActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 111
    invoke-super {p0, p1}, Lcom/helpshift/app/ActionBarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 113
    invoke-virtual {p0}, Lcom/helpshift/HSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSActivity;->p:Landroid/os/Bundle;

    .line 114
    iget-object v0, p0, Lcom/helpshift/HSActivity;->p:Landroid/os/Bundle;

    const-string v1, "showConvOnReportIssue"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSActivity;->r:Z

    .line 115
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSActivity;->y:Lcom/helpshift/HSApiData;

    .line 116
    iget-object v0, p0, Lcom/helpshift/HSActivity;->y:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSActivity;->x:Lcom/helpshift/HSStorage;

    .line 118
    instance-of v0, p0, Lcom/helpshift/HSQuestion;

    if-eqz v0, :cond_0

    .line 119
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->d:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->a:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/helpshift/app/ActionBarActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 159
    iput-object p1, p0, Lcom/helpshift/HSActivity;->z:Landroid/view/Menu;

    .line 160
    iget-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    if-eqz v0, :cond_0

    instance-of v0, p0, Lcom/helpshift/HSConversation;

    if-nez v0, :cond_0

    .line 162
    invoke-direct {p0, p1}, Lcom/helpshift/HSActivity;->a(Landroid/view/Menu;)V

    .line 165
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Lcom/helpshift/app/ActionBarActivity;->onPause()V

    .line 152
    invoke-direct {p0}, Lcom/helpshift/HSActivity;->f()V

    .line 153
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0}, Lcom/helpshift/app/ActionBarActivity;->onResume()V

    .line 132
    instance-of v0, p0, Lcom/helpshift/HSQuestion;

    if-eqz v0, :cond_1

    .line 133
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->d:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    .line 138
    :goto_0
    iget-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    if-nez v0, :cond_2

    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 147
    :cond_0
    :goto_1
    return-void

    .line 135
    :cond_1
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->a:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    goto :goto_0

    .line 140
    :cond_2
    iget-boolean v0, p0, Lcom/helpshift/HSActivity;->q:Z

    if-eqz v0, :cond_0

    instance-of v0, p0, Lcom/helpshift/HSConversation;

    if-nez v0, :cond_0

    .line 141
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 142
    iget-object v0, p0, Lcom/helpshift/HSActivity;->x:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-direct {p0}, Lcom/helpshift/HSActivity;->g()V

    .line 144
    invoke-virtual {p0}, Lcom/helpshift/HSActivity;->e()V

    goto :goto_1
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 179
    invoke-super {p0}, Lcom/helpshift/app/ActionBarActivity;->onStart()V

    .line 180
    invoke-static {p0}, Lcom/helpshift/HSAnalytics;->a(Landroid/app/Activity;)V

    .line 181
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 185
    invoke-super {p0}, Lcom/helpshift/app/ActionBarActivity;->onStop()V

    .line 186
    invoke-static {}, Lcom/helpshift/HSAnalytics;->a()V

    .line 187
    return-void
.end method
