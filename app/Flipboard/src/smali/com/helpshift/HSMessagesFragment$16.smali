.class Lcom/helpshift/HSMessagesFragment$16;
.super Landroid/os/Handler;
.source "HSMessagesFragment.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/helpshift/HSMessagesFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSMessagesFragment;I)V
    .locals 0

    .prologue
    .line 892
    iput-object p1, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    iput p2, p0, Lcom/helpshift/HSMessagesFragment$16;->a:I

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 894
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->r(Lcom/helpshift/HSMessagesFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/helpshift/HSMessagesFragment$16;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 895
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 896
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    .line 897
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 898
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->c(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/customadapters/MessagesAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 900
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v3}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 901
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v2}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 905
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0, p1}, Lcom/helpshift/HSMessagesFragment;->a(Lcom/helpshift/HSMessagesFragment;Landroid/os/Message;)V

    .line 907
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 908
    const-string v1, "type"

    const-string v2, "conversation"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 909
    const-string v1, "r"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 911
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->l(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    const-string v1, "config"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "rurl"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 912
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 913
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 914
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/HSApiData;->g()V

    .line 915
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$16;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1, v0}, Lcom/helpshift/HSMessagesFragment;->a(Lcom/helpshift/HSMessagesFragment;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 920
    :cond_0
    :goto_1
    return-void

    .line 902
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_0

    .line 917
    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_1
.end method
