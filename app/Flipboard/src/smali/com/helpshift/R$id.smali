.class public final Lcom/helpshift/R$id;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final additional_feedback:I = 0x7f0a01b2

.field public static final admin_message:I = 0x7f0a01c3

.field public static final button_containers:I = 0x7f0a01d9

.field public static final button_separator:I = 0x7f0a01c4

.field public static final change:I = 0x7f0a01da

.field public static final csat_dislike_msg:I = 0x7f0a01b5

.field public static final csat_like_msg:I = 0x7f0a01b6

.field public static final csat_message:I = 0x7f0a01b4

.field public static final csat_view_stub:I = 0x7f0a01be

.field public static final divider:I = 0x7f0a012a

.field public static final horizontal_divider:I = 0x7f0a01d8

.field public static final hs__action_add_conversation:I = 0x7f0a03a3

.field public static final hs__action_faq_helpful:I = 0x7f0a01ce

.field public static final hs__action_faq_unhelpful:I = 0x7f0a01cf

.field public static final hs__action_report_issue:I = 0x7f0a03a5

.field public static final hs__action_search:I = 0x7f0a03a4

.field public static final hs__actionbar_compat:I = 0x7f0a01a8

.field public static final hs__actionbar_compat_home:I = 0x7f0a01aa

.field public static final hs__actionbar_compat_item_refresh_progress:I = 0x7f0a000b

.field public static final hs__actionbar_compat_title:I = 0x7f0a000c

.field public static final hs__actionbar_compat_up:I = 0x7f0a01a9

.field public static final hs__attach_screenshot:I = 0x7f0a03a2

.field public static final hs__confirmation:I = 0x7f0a01bb

.field public static final hs__contactUsContainer:I = 0x7f0a01cd

.field public static final hs__contact_us_btn:I = 0x7f0a01d2

.field public static final hs__conversationDetail:I = 0x7f0a01c5

.field public static final hs__conversation_icon:I = 0x7f0a01ab

.field public static final hs__customViewContainer:I = 0x7f0a01d4

.field public static final hs__email:I = 0x7f0a01c8

.field public static final hs__faqs_fragment:I = 0x7f0a01b8

.field public static final hs__fragment_holder:I = 0x7f0a01ae

.field public static final hs__fullscreen_custom_content:I = 0x7f0a01e3

.field public static final hs__helpful_text:I = 0x7f0a01d3

.field public static final hs__helpshiftActivityFooter:I = 0x7f0a01b9

.field public static final hs__messageText:I = 0x7f0a01c1

.field public static final hs__messagesList:I = 0x7f0a01ba

.field public static final hs__newConversationFooter:I = 0x7f0a01af

.field public static final hs__new_conversation:I = 0x7f0a01bc

.field public static final hs__new_conversation_btn:I = 0x7f0a01bf

.field public static final hs__notifcation_badge:I = 0x7f0a01ac

.field public static final hs__pager_tab_strip:I = 0x7f0a01d6

.field public static final hs__question:I = 0x7f0a01d0

.field public static final hs__questionContent:I = 0x7f0a01cb

.field public static final hs__question_container:I = 0x7f0a01c9

.field public static final hs__question_fragment:I = 0x7f0a01ca

.field public static final hs__root:I = 0x7f0a01ad

.field public static final hs__screenshot:I = 0x7f0a01c6

.field public static final hs__search_button:I = 0x7f0a01e1

.field public static final hs__search_query:I = 0x7f0a01df

.field public static final hs__search_query_clear:I = 0x7f0a01e0

.field public static final hs__sectionContainer:I = 0x7f0a01dd

.field public static final hs__sectionFooter:I = 0x7f0a01de

.field public static final hs__sections_pager:I = 0x7f0a01d5

.field public static final hs__sendMessageBtn:I = 0x7f0a01c2

.field public static final hs__unhelpful_text:I = 0x7f0a01d1

.field public static final hs__username:I = 0x7f0a01c7

.field public static final hs__webViewParent:I = 0x7f0a01cc

.field public static final hs__webview_main_content:I = 0x7f0a01e4

.field public static final like_status:I = 0x7f0a01b1

.field public static final option_text:I = 0x7f0a01b7

.field public static final progress_indicator:I = 0x7f0a01e2

.field public static final ratingBar:I = 0x7f0a01b0

.field public static final relativeLayout1:I = 0x7f0a01c0

.field public static final report_issue:I = 0x7f0a01dc

.field public static final screenshotPreview:I = 0x7f0a01d7

.field public static final send:I = 0x7f0a01db

.field public static final submit:I = 0x7f0a01b3

.field public static final textView1:I = 0x7f0a01bd
