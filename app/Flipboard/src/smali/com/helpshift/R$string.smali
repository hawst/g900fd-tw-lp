.class public final Lcom/helpshift/R$string;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final hs__attach_btn:I = 0x7f0d017c

.field public static final hs__attach_screenshot_btn:I = 0x7f0d017d

.field public static final hs__ca_msg:I = 0x7f0d017e

.field public static final hs__change_btn:I = 0x7f0d017f

.field public static final hs__chat_hint:I = 0x7f0d0180

.field public static final hs__confirmation_footer_msg:I = 0x7f0d0181

.field public static final hs__confirmation_msg:I = 0x7f0d0182

.field public static final hs__contact_us_btn:I = 0x7f0d0183

.field public static final hs__conversation_detail_error:I = 0x7f0d0184

.field public static final hs__conversation_end_msg:I = 0x7f0d0185

.field public static final hs__conversation_header:I = 0x7f0d0186

.field public static final hs__conversation_started_message:I = 0x7f0d0187

.field public static final hs__cr_msg:I = 0x7f0d0188

.field public static final hs__csat_additonal_feedback_message:I = 0x7f0d0189

.field public static final hs__csat_dislike_message:I = 0x7f0d018a

.field public static final hs__csat_like_message:I = 0x7f0d018b

.field public static final hs__csat_message:I = 0x7f0d018c

.field public static final hs__csat_option_message:I = 0x7f0d018d

.field public static final hs__csat_submit_toast:I = 0x7f0d018e

.field public static final hs__data_not_found_msg:I = 0x7f0d018f

.field public static final hs__default_notification_content_title:I = 0x7f0d0190

.field public static final hs__dm_video_loading:I = 0x7f0d0191

.field public static final hs__done_btn:I = 0x7f0d0192

.field public static final hs__email_hint:I = 0x7f0d0193

.field public static final hs__email_required_hint:I = 0x7f0d0194

.field public static final hs__empty_section:I = 0x7f0d0195

.field public static final hs__faq_header:I = 0x7f0d0196

.field public static final hs__faq_helpful_msg:I = 0x7f0d0197

.field public static final hs__faq_unhelpful_msg:I = 0x7f0d0198

.field public static final hs__faqs_search_footer:I = 0x7f0d0199

.field public static final hs__faqs_updating:I = 0x7f0d019a

.field public static final hs__faqs_updation_failure:I = 0x7f0d019b

.field public static final hs__faqs_uptodate:I = 0x7f0d019c

.field public static final hs__feedback_button:I = 0x7f0d019d

.field public static final hs__help_header:I = 0x7f0d019e

.field public static final hs__invalid_description_error:I = 0x7f0d019f

.field public static final hs__invalid_email_error:I = 0x7f0d01a0

.field public static final hs__mark_helpful_toast:I = 0x7f0d01a1

.field public static final hs__mark_no:I = 0x7f0d01a2

.field public static final hs__mark_unhelpful_toast:I = 0x7f0d01a3

.field public static final hs__mark_yes:I = 0x7f0d01a4

.field public static final hs__mark_yes_no_question:I = 0x7f0d01a5

.field public static final hs__network_error_msg:I = 0x7f0d01a6

.field public static final hs__network_unavailable_msg:I = 0x7f0d01a7

.field public static final hs__new_conversation_btn:I = 0x7f0d01a8

.field public static final hs__new_conversation_header:I = 0x7f0d01a9

.field public static final hs__new_conversation_hint:I = 0x7f0d01aa

.field public static final hs__new_conversation_msg:I = 0x7f0d01ab

.field public static final hs__notification_content_text:I = 0x7f0d01ac

.field public static final hs__privacy_policy:I = 0x7f0d01ad

.field public static final hs__question_header:I = 0x7f0d01ae

.field public static final hs__question_helpful_message:I = 0x7f0d01af

.field public static final hs__question_unhelpful_message:I = 0x7f0d01b0

.field public static final hs__rate_button:I = 0x7f0d01b1

.field public static final hs__review_accepted_message:I = 0x7f0d01b2

.field public static final hs__review_btn:I = 0x7f0d01b3

.field public static final hs__review_close_button:I = 0x7f0d01b4

.field public static final hs__review_dialog_msg:I = 0x7f0d01b5

.field public static final hs__review_dialog_negative_btn:I = 0x7f0d01b6

.field public static final hs__review_dialog_neutral_btn:I = 0x7f0d01b7

.field public static final hs__review_dialog_positive_btn:I = 0x7f0d01b8

.field public static final hs__review_message:I = 0x7f0d01b9

.field public static final hs__review_request_message:I = 0x7f0d01ba

.field public static final hs__review_title:I = 0x7f0d01bb

.field public static final hs__rsc_progress_msg:I = 0x7f0d01bc

.field public static final hs__screenshot_add:I = 0x7f0d01bd

.field public static final hs__screenshot_cloud_attach_error:I = 0x7f0d01be

.field public static final hs__screenshot_limit_error:I = 0x7f0d01bf

.field public static final hs__screenshot_remove:I = 0x7f0d01c0

.field public static final hs__screenshot_sent_msg:I = 0x7f0d01c1

.field public static final hs__screenshot_upload_error_msg:I = 0x7f0d01c2

.field public static final hs__search_footer:I = 0x7f0d01c3

.field public static final hs__search_hint:I = 0x7f0d01c4

.field public static final hs__search_title:I = 0x7f0d01c5

.field public static final hs__send_msg_btn:I = 0x7f0d01c6

.field public static final hs__sending_fail_msg:I = 0x7f0d01c7

.field public static final hs__sending_msg:I = 0x7f0d01c8

.field public static final hs__solved_btn:I = 0x7f0d01c9

.field public static final hs__submit_conversation_btn:I = 0x7f0d01ca

.field public static final hs__unsolved_btn:I = 0x7f0d01cb

.field public static final hs__username_blank_error:I = 0x7f0d01cc

.field public static final hs__username_hint:I = 0x7f0d01cd
