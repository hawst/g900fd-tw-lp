.class final Lcom/helpshift/HSAnalytics;
.super Ljava/lang/Object;
.source "HSAnalytics.java"


# static fields
.field protected static a:Z

.field private static b:Lcom/helpshift/HSApiData;

.field private static c:I

.field private static d:I

.field private static e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/HSAnalytics;->b:Lcom/helpshift/HSApiData;

    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 33
    sget v0, Lcom/helpshift/HSAnalytics;->d:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/helpshift/HSAnalytics;->d:I

    .line 34
    sget v0, Lcom/helpshift/HSAnalytics;->c:I

    sget v1, Lcom/helpshift/HSAnalytics;->d:I

    if-ne v0, v1, :cond_0

    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lcom/helpshift/HSAnalytics;->e:Z

    .line 36
    const-string v0, "q"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/helpshift/HSAnalytics;->b:Lcom/helpshift/HSApiData;

    invoke-static {}, Lcom/helpshift/HSFunnel;->a()Lorg/json/JSONArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/HSApiData;->a(Lorg/json/JSONArray;)V

    .line 39
    :cond_0
    return-void
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/helpshift/HSAnalytics;->b:Lcom/helpshift/HSApiData;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/helpshift/HSAnalytics;->b:Lcom/helpshift/HSApiData;

    .line 19
    :cond_0
    sget v0, Lcom/helpshift/HSAnalytics;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/helpshift/HSAnalytics;->c:I

    .line 21
    sget-boolean v0, Lcom/helpshift/HSAnalytics;->e:Z

    if-nez v0, :cond_1

    .line 22
    sget-boolean v0, Lcom/helpshift/HSAnalytics;->a:Z

    if-eqz v0, :cond_2

    .line 23
    const-string v0, "d"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 29
    :cond_1
    :goto_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/helpshift/HSAnalytics;->e:Z

    .line 30
    return-void

    .line 25
    :cond_2
    const-string v0, "o"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
