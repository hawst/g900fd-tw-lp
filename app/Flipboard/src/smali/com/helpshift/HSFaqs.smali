.class public final Lcom/helpshift/HSFaqs;
.super Lcom/helpshift/HSActivity;
.source "HSFaqs.java"


# instance fields
.field private o:Lcom/helpshift/HSApiData;

.field private p:Lcom/helpshift/HSStorage;

.field private q:I

.field private r:Ljava/lang/Boolean;

.field private s:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/helpshift/HSActivity;-><init>()V

    .line 24
    const/4 v0, 0x1

    iput v0, p0, Lcom/helpshift/HSFaqs;->q:I

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 58
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lcom/helpshift/HSAnalytics;->a:Z

    .line 61
    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "showConvOnReportIssue"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqs;->r:Ljava/lang/Boolean;

    .line 62
    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "showInFullScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 69
    :cond_0
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSFaqs;->o:Lcom/helpshift/HSApiData;

    .line 70
    iget-object v0, p0, Lcom/helpshift/HSFaqs;->o:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSFaqs;->p:Lcom/helpshift/HSStorage;

    .line 72
    const-string v0, "layout"

    const-string v1, "hs__faqs"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSFaqs;->setContentView(I)V

    .line 76
    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 77
    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->b()V

    .line 78
    const-string v0, "id"

    const-string v1, "hs__helpshiftActivityFooter"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSFaqs;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/helpshift/HSFaqs;->s:Landroid/widget/ImageView;

    .line 81
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->a:Ljava/util/Map;

    const-string v1, "hl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v1, p0, Lcom/helpshift/HSFaqs;->s:Landroid/widget/ImageView;

    sget-object v0, Lcom/helpshift/res/drawable/HSImages;->a:Ljava/util/Map;

    const-string v2, "newHSLogo"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/helpshift/res/drawable/HSDraw;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 84
    iget-object v0, p0, Lcom/helpshift/HSFaqs;->s:Landroid/widget/ImageView;

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 86
    :cond_1
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const-string v1, "menu"

    const-string v2, "hs__faqs_fragment"

    invoke-static {p0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 91
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 96
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 97
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->finish()V

    .line 99
    const/4 v0, 0x1

    .line 101
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 45
    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/helpshift/HSFaqs;->isFinishing()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 49
    invoke-static {}, Lcom/helpshift/util/HSActivityUtil;->a()V

    .line 53
    :cond_0
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onPause()V

    .line 54
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSFaqs;->p:Lcom/helpshift/HSStorage;

    const-string v1, "config"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 33
    invoke-static {v0}, Lcom/helpshift/res/values/HSConfig;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onResume()V

    .line 40
    const-string v0, "l"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 41
    return-void

    .line 35
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method public final bridge synthetic onStart()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStart()V

    return-void
.end method

.method public final bridge synthetic onStop()V
    .locals 0

    .prologue
    .line 18
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStop()V

    return-void
.end method
