.class public final Lcom/helpshift/HSReviewFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "HSReviewFragment.java"


# static fields
.field private static m:Lcom/helpshift/HSAlertToRateAppListener;


# instance fields
.field private final j:Ljava/lang/String;

.field private k:Lcom/helpshift/HSApiData;

.field private l:Lcom/helpshift/HSStorage;

.field private n:Z

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 19
    const-string v0, "HelpShiftDebug"

    iput-object v0, p0, Lcom/helpshift/HSReviewFragment;->j:Ljava/lang/String;

    .line 24
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/helpshift/HSReviewFragment;->n:Z

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSReviewFragment;->o:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSReviewFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/helpshift/HSReviewFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 17
    iput-object p1, p0, Lcom/helpshift/HSReviewFragment;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/helpshift/HSReviewFragment;)Lcom/helpshift/HSStorage;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment;->l:Lcom/helpshift/HSStorage;

    return-object v0
.end method

.method static synthetic b(Lcom/helpshift/HSReviewFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 17
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/helpshift/HSReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/helpshift/HSReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method static synthetic d()V
    .locals 0

    .prologue
    .line 17
    invoke-static {}, Lcom/helpshift/HSReviewFragment;->e()V

    return-void
.end method

.method private static e()V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/helpshift/HSReviewFragment;->m:Lcom/helpshift/HSAlertToRateAppListener;

    if-eqz v0, :cond_0

    .line 71
    sget-object v0, Lcom/helpshift/HSReviewFragment;->m:Lcom/helpshift/HSAlertToRateAppListener;

    .line 73
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/HSReviewFragment;->m:Lcom/helpshift/HSAlertToRateAppListener;

    .line 74
    return-void
.end method


# virtual methods
.method public final c()Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/helpshift/HSReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 34
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 35
    if-eqz v1, :cond_0

    .line 36
    const-string v2, "disableReview"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/helpshift/HSReviewFragment;->n:Z

    .line 37
    const-string v2, "rurl"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/HSReviewFragment;->o:Ljava/lang/String;

    .line 39
    :cond_0
    new-instance v1, Lcom/helpshift/HSApiData;

    invoke-direct {v1, v0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/helpshift/HSReviewFragment;->k:Lcom/helpshift/HSApiData;

    .line 40
    iget-object v1, p0, Lcom/helpshift/HSReviewFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v1, p0, Lcom/helpshift/HSReviewFragment;->l:Lcom/helpshift/HSStorage;

    .line 41
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "hs__review_message"

    invoke-static {v0, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    const-string v2, "hs__review_title"

    invoke-static {v0, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setIcon(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    const/4 v2, -0x1

    const-string v3, "hs__rate_button"

    invoke-static {v0, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/helpshift/HSReviewFragment$1;

    invoke-direct {v4, p0}, Lcom/helpshift/HSReviewFragment$1;-><init>(Lcom/helpshift/HSReviewFragment;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v2, -0x3

    const-string v3, "hs__feedback_button"

    invoke-static {v0, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/helpshift/HSReviewFragment$2;

    invoke-direct {v4, p0}, Lcom/helpshift/HSReviewFragment$2;-><init>(Lcom/helpshift/HSReviewFragment;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v2, -0x2

    const-string v3, "hs__review_close_button"

    invoke-static {v0, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Lcom/helpshift/HSReviewFragment$3;

    invoke-direct {v3, p0}, Lcom/helpshift/HSReviewFragment$3;-><init>(Lcom/helpshift/HSReviewFragment;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    return-object v1
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 56
    const-string v0, "later"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->b(Ljava/lang/String;)V

    .line 57
    sget-object v0, Lcom/helpshift/Helpshift$HS_RATE_ALERT;->c:Lcom/helpshift/Helpshift$HS_RATE_ALERT;

    invoke-static {}, Lcom/helpshift/HSReviewFragment;->e()V

    .line 58
    return-void
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 63
    iget-boolean v0, p0, Lcom/helpshift/HSReviewFragment;->n:Z

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment;->k:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->g()V

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/HSReviewFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 67
    return-void
.end method
