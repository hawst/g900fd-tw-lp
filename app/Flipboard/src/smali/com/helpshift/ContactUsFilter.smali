.class public final Lcom/helpshift/ContactUsFilter;
.super Ljava/lang/Object;
.source "ContactUsFilter.java"


# static fields
.field private static a:Lcom/helpshift/HSStorage;

.field private static b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/ContactUsFilter;->a:Lcom/helpshift/HSStorage;

    .line 12
    sget-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->a:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    sput-object v0, Lcom/helpshift/ContactUsFilter;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/helpshift/ContactUsFilter;->a:Lcom/helpshift/HSStorage;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/helpshift/HSStorage;

    invoke-direct {v0, p0}, Lcom/helpshift/HSStorage;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/helpshift/ContactUsFilter;->a:Lcom/helpshift/HSStorage;

    .line 27
    :cond_0
    return-void
.end method

.method protected static a(Ljava/util/HashMap;)V
    .locals 2

    .prologue
    .line 30
    const-string v0, "enableContactUs"

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 32
    instance-of v1, v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    if-eqz v1, :cond_1

    .line 33
    const-string v0, "enableContactUs"

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    sput-object v0, Lcom/helpshift/ContactUsFilter;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    instance-of v1, v0, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 35
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 36
    sget-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->a:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    sput-object v0, Lcom/helpshift/ContactUsFilter;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    goto :goto_0

    .line 38
    :cond_2
    sget-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    sput-object v0, Lcom/helpshift/ContactUsFilter;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    goto :goto_0
.end method

.method protected static a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 44
    sget-object v2, Lcom/helpshift/ContactUsFilter$1;->b:[I

    sget-object v3, Lcom/helpshift/ContactUsFilter;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    invoke-virtual {v3}, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 70
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 48
    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v2, Lcom/helpshift/ContactUsFilter$1;->a:[I

    invoke-virtual {p0}, Lcom/helpshift/ContactUsFilter$LOCATION;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 58
    :pswitch_3
    sget-object v2, Lcom/helpshift/ContactUsFilter;->a:Lcom/helpshift/HSStorage;

    const-string v3, "activeConversation"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    sget-object v3, Lcom/helpshift/ContactUsFilter;->a:Lcom/helpshift/HSStorage;

    const-string v4, "archivedConversationId"

    invoke-virtual {v3, v4}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 64
    goto :goto_0

    .line 44
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 50
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
