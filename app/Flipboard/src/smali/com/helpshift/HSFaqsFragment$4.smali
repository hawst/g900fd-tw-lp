.class Lcom/helpshift/HSFaqsFragment$4;
.super Landroid/os/Handler;
.source "HSFaqsFragment.java"


# instance fields
.field final synthetic a:Lcom/helpshift/HSFaqsFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSFaqsFragment;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 202
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 203
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 204
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    .line 205
    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v2}, Lcom/helpshift/HSFaqsFragment;->h(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/HSApiData;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/helpshift/HSApiData;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    .line 206
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v0}, Lcom/helpshift/HSFaqsFragment;->i(Lcom/helpshift/HSFaqsFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 208
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v7, :cond_0

    .line 209
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v0}, Lcom/helpshift/HSFaqsFragment;->h(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/HSApiData;

    move-result-object v3

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    iget-object v0, v0, Lcom/helpshift/Section;->d:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 210
    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 211
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 212
    iget-object v3, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v3}, Lcom/helpshift/HSFaqsFragment;->i(Lcom/helpshift/HSFaqsFragment;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/helpshift/Faq;

    iget-object v5, v0, Lcom/helpshift/Faq;->c:Ljava/lang/String;

    iget-object v0, v0, Lcom/helpshift/Faq;->d:Ljava/lang/String;

    const-string v6, "question"

    invoke-direct {v4, v5, v0, v6}, Lcom/helpshift/Faq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 217
    :cond_0
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 218
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    .line 219
    iget-object v3, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v3}, Lcom/helpshift/HSFaqsFragment;->h(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/HSApiData;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/helpshift/HSApiData;->a(Lcom/helpshift/Section;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 220
    iget-object v3, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v3}, Lcom/helpshift/HSFaqsFragment;->i(Lcom/helpshift/HSFaqsFragment;)Ljava/util/List;

    move-result-object v3

    new-instance v4, Lcom/helpshift/Faq;

    iget-object v5, v0, Lcom/helpshift/Section;->c:Ljava/lang/String;

    iget-object v0, v0, Lcom/helpshift/Section;->d:Ljava/lang/String;

    const-string v6, "section"

    invoke-direct {v4, v5, v0, v6}, Lcom/helpshift/Faq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v0}, Lcom/helpshift/HSFaqsFragment;->i(Lcom/helpshift/HSFaqsFragment;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 228
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v0}, Lcom/helpshift/HSFaqsFragment;->i(Lcom/helpshift/HSFaqsFragment;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/helpshift/Faq;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v2}, Lcom/helpshift/HSFaqsFragment;->d(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/HSActivity;

    move-result-object v2

    const-string v3, "hs__faqs_search_footer"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    const-string v4, "empty_status"

    invoke-direct {v1, v2, v3, v4}, Lcom/helpshift/Faq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-virtual {v0}, Lcom/helpshift/HSFaqsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 233
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-virtual {v0, v7, v7}, Landroid/support/v4/app/ListFragment;->a(ZZ)V

    .line 235
    :cond_4
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment$4;->a:Lcom/helpshift/HSFaqsFragment;

    invoke-static {v0}, Lcom/helpshift/HSFaqsFragment;->j(Lcom/helpshift/HSFaqsFragment;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 236
    return-void
.end method
