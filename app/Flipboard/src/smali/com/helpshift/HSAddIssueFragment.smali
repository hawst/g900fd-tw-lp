.class public Lcom/helpshift/HSAddIssueFragment;
.super Landroid/support/v4/app/Fragment;
.source "HSAddIssueFragment.java"


# instance fields
.field private A:Landroid/os/Handler;

.field public a:Landroid/os/Handler;

.field public b:Landroid/os/Handler;

.field private c:Lcom/helpshift/HSActivity;

.field private d:Landroid/os/Bundle;

.field private e:Lcom/helpshift/HSStorage;

.field private f:Lcom/helpshift/HSApiData;

.field private g:Lcom/helpshift/HSApiClient;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:I

.field private k:Landroid/view/MenuItem;

.field private l:Landroid/view/MenuItem;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/EditText;

.field private o:Landroid/widget/EditText;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/Boolean;

.field private s:Landroid/widget/ImageView;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Landroid/widget/ImageButton;

.field private w:Lcom/helpshift/viewstructs/HSMsg;

.field private x:Landroid/os/Handler;

.field private y:Landroid/os/Handler;

.field private z:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    const/4 v0, 0x1

    iput v0, p0, Lcom/helpshift/HSAddIssueFragment;->j:I

    .line 59
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->t:Ljava/lang/String;

    .line 64
    new-instance v0, Lcom/helpshift/HSAddIssueFragment$1;

    invoke-direct {v0, p0}, Lcom/helpshift/HSAddIssueFragment$1;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->x:Landroid/os/Handler;

    .line 112
    new-instance v0, Lcom/helpshift/HSAddIssueFragment$2;

    invoke-direct {v0, p0}, Lcom/helpshift/HSAddIssueFragment$2;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->a:Landroid/os/Handler;

    .line 147
    new-instance v0, Lcom/helpshift/HSAddIssueFragment$3;

    invoke-direct {v0, p0}, Lcom/helpshift/HSAddIssueFragment$3;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->y:Landroid/os/Handler;

    .line 155
    new-instance v0, Lcom/helpshift/HSAddIssueFragment$4;

    invoke-direct {v0, p0}, Lcom/helpshift/HSAddIssueFragment$4;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->z:Landroid/os/Handler;

    .line 188
    new-instance v0, Lcom/helpshift/HSAddIssueFragment$5;

    invoke-direct {v0, p0}, Lcom/helpshift/HSAddIssueFragment$5;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->A:Landroid/os/Handler;

    .line 209
    new-instance v0, Lcom/helpshift/HSAddIssueFragment$6;

    invoke-direct {v0, p0}, Lcom/helpshift/HSAddIssueFragment$6;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSActivity;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/helpshift/HSAddIssueFragment;Lcom/helpshift/viewstructs/HSMsg;)Lcom/helpshift/viewstructs/HSMsg;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/helpshift/HSAddIssueFragment;->w:Lcom/helpshift/viewstructs/HSMsg;

    return-object p1
.end method

.method static synthetic a(Lcom/helpshift/HSAddIssueFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/helpshift/HSAddIssueFragment;->u:Ljava/lang/String;

    return-object p1
.end method

.method private a()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 202
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 203
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 204
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->t:Ljava/lang/String;

    .line 205
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v1, ""

    const-string v2, "screenshotPath"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 207
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 463
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v1, "screenshotPath"

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 466
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 433
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/helpshift/util/AttachmentUtil;->a(Z)V

    .line 434
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-class v2, Lcom/helpshift/ScreenshotPreviewActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 435
    const-string v1, "SCREENSHOT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 436
    const-string v1, "screenshot_text_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 437
    const/16 v1, 0x7fbc

    invoke-virtual {p0, v0, v1}, Lcom/helpshift/HSAddIssueFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 438
    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 563
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSActivity;->c(Z)V

    .line 564
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->k:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 565
    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->k:Landroid/view/MenuItem;

    if-nez p1, :cond_5

    move v0, v1

    :goto_0
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 567
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 568
    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    if-nez p1, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 570
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 571
    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    if-nez p1, :cond_7

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 574
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    if-eqz v0, :cond_4

    .line 575
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    .line 577
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 582
    :cond_4
    :goto_3
    return-void

    :cond_5
    move v0, v2

    .line 565
    goto :goto_0

    :cond_6
    move v0, v2

    .line 568
    goto :goto_1

    :cond_7
    move v0, v2

    .line 571
    goto :goto_2

    .line 579
    :cond_8
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3
.end method

.method private b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/helpshift/HSAddIssueFragment;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Z)V

    return-void
.end method

.method static synthetic b(Lcom/helpshift/HSAddIssueFragment;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 469
    const/4 v0, -0x1

    invoke-static {p1, v0}, Lcom/helpshift/util/AttachmentUtil;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 470
    if-eqz v0, :cond_1

    .line 471
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 472
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 473
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 474
    iput-object p1, p0, Lcom/helpshift/HSAddIssueFragment;->t:Ljava/lang/String;

    .line 475
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2, v2}, Landroid/widget/TextView;->measure(II)V

    .line 479
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    .line 480
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 481
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    int-to-double v2, v0

    const-wide v4, 0x3fe5555555555555L    # 0.6666666666666666

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 482
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    .line 484
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSStorage;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    return-object v0
.end method

.method private c()Ljava/util/HashMap;
    .locals 3

    .prologue
    .line 238
    const/4 v0, 0x0

    .line 240
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-static {}, Lcom/helpshift/IdentityFilter;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 242
    const-string v1, "name"

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 244
    const-string v1, "email"

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 247
    :cond_0
    return-object v0
.end method

.method static synthetic d(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/helpshift/HSAddIssueFragment;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic g(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->z:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic j(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->A:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic k(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/viewstructs/HSMsg;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->w:Lcom/helpshift/viewstructs/HSMsg;

    return-object v0
.end method

.method static synthetic l(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSApiClient;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->g:Lcom/helpshift/HSApiClient;

    return-object v0
.end method

.method static synthetic m(Lcom/helpshift/HSAddIssueFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 39
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->b:Ljava/util/Map;

    const-string v1, "dia"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-class v2, Lcom/helpshift/HSConversation;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "newIssue"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "issueId"

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "decomp"

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->h:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "showConvOnReportIssue"

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->i:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "showInFullScreen"

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    invoke-static {v2}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v1, "chatLaunchSource"

    const-string v2, "support"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "hs__conversation_started_message"

    invoke-static {p0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "callFinish"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    invoke-static {}, Lcom/helpshift/util/HSActivityUtil;->a()V

    goto :goto_0
.end method

.method static synthetic n(Lcom/helpshift/HSAddIssueFragment;)Lcom/helpshift/HSApiData;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    return-object v0
.end method

.method static synthetic o(Lcom/helpshift/HSAddIssueFragment;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->a()V

    return-void
.end method

.method static synthetic p(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->y:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic q(Lcom/helpshift/HSAddIssueFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->x:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic r(Lcom/helpshift/HSAddIssueFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/helpshift/HSAddIssueFragment;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->c()Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lcom/helpshift/HSAddIssueFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic u(Lcom/helpshift/HSAddIssueFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    return-object v0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 442
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 443
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 444
    if-nez p1, :cond_1

    .line 445
    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/helpshift/util/AttachmentUtil;->a(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 446
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 447
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/HSAddIssueFragment;->a(Ljava/lang/String;I)V

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/helpshift/util/AttachmentUtil;->a(Z)V

    .line 451
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SCREENSHOT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 452
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 453
    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Ljava/lang/String;)V

    .line 454
    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 456
    :cond_2
    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->a()V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 587
    const-string v0, "menu"

    const-string v1, "hs__add_conversation_menu"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 590
    const-string v0, "id"

    const-string v1, "hs__action_add_conversation"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->k:Landroid/view/MenuItem;

    .line 593
    const-string v0, "id"

    const-string v1, "hs__attach_screenshot"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    .line 594
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->l:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 595
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->k:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 596
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Z)V

    .line 597
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/16 v4, 0x400

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 271
    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSActivity;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    .line 272
    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    .line 274
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    const-string v1, "showInFullScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Lcom/helpshift/HSActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/view/Window;->setFlags(II)V

    .line 281
    :cond_0
    new-instance v0, Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    invoke-direct {v0, v1}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    .line 282
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    .line 283
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->g:Lcom/helpshift/HSApiClient;

    .line 285
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-static {v0}, Lcom/helpshift/IdentityFilter;->b(Lcom/helpshift/HSStorage;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->r:Ljava/lang/Boolean;

    .line 287
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    const-string v1, "decomp"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->h:Ljava/lang/Boolean;

    .line 288
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    const-string v1, "showConvOnReportIssue"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->i:Ljava/lang/Boolean;

    .line 290
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 291
    sput-boolean v3, Lcom/helpshift/HSAnalytics;->a:Z

    .line 294
    :cond_1
    invoke-virtual {p0, v3}, Lcom/helpshift/HSAddIssueFragment;->setHasOptionsMenu(Z)V

    .line 295
    const-string v0, "layout"

    const-string v1, "hs__new_conversation_fragment"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 401
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 402
    const-string v2, "id"

    const-string v3, "hs__action_add_conversation"

    invoke-static {p0, v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-ne v0, v2, :cond_a

    .line 406
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-static {v3}, Lcom/helpshift/IdentityFilter;->a(Lcom/helpshift/HSStorage;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    iget-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_7

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v4, "hs__conversation_detail_error"

    invoke-static {v2, v4}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/util/HSPattern;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v3, "hs__username_blank_error"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_3
    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->r:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/util/HSPattern;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v3, "hs__invalid_email_error"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 407
    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Lcom/helpshift/HSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 409
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Z)V

    .line 410
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->a:Landroid/os/Handler;

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->x:Landroid/os/Handler;

    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->b()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->c()Ljava/util/HashMap;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/util/HashMap;)V
    :try_end_0
    .catch Lcom/helpshift/exceptions/IdentityException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    :goto_3
    move v0, v6

    .line 429
    :goto_4
    return v0

    .line 406
    :cond_6
    iget-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v5, "username"

    invoke-virtual {v4, v5}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    iget-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v5, "email"

    invoke-virtual {v4, v5}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    invoke-static {v2}, Lcom/helpshift/util/HSPattern;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v4, "hs__invalid_description_error"

    invoke-static {v2, v4}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_4

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/util/HSPattern;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v3, "hs__invalid_email_error"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setError(Ljava/lang/CharSequence;)V

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_2

    .line 413
    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->b:Landroid/os/Handler;

    iget-object v5, p0, Lcom/helpshift/HSAddIssueFragment;->x:Landroid/os/Handler;

    iget-object v7, p0, Lcom/helpshift/HSAddIssueFragment;->p:Ljava/lang/String;

    iget-object v8, p0, Lcom/helpshift/HSAddIssueFragment;->q:Ljava/lang/String;

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->f:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->h()Ljava/lang/String;

    move-result-object v9

    :try_start_1
    const-string v0, "com.crittercism.app.Crittercism"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v4, "getUserUUID"

    const/4 v10, 0x0

    invoke-virtual {v0, v4, v10}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x0

    const/4 v10, 0x0

    invoke-virtual {v0, v4, v10}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v0

    :goto_5
    new-instance v4, Lcom/helpshift/HSApiData$11;

    invoke-direct {v4, v2, v3}, Lcom/helpshift/HSApiData$11;-><init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;)V

    iget-object v0, v2, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    const-string v2, "displayname"

    invoke-virtual {v3, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "email"

    invoke-virtual {v3, v2, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "identifier"

    invoke-virtual {v3, v2, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v1, :cond_9

    const-string v2, "crittercism-id"

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    const-string v1, "POST"

    const-string v2, "/profiles/"

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;Landroid/os/Handler;Landroid/os/Handler;)V

    goto/16 :goto_3

    .line 422
    :cond_a
    const v1, 0x102002c

    if-ne v0, v1, :cond_b

    .line 423
    invoke-virtual {p0}, Lcom/helpshift/HSAddIssueFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    move v0, v6

    .line 424
    goto/16 :goto_4

    .line 425
    :cond_b
    const-string v1, "id"

    const-string v2, "hs__attach_screenshot"

    invoke-static {p0, v1, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_c

    .line 426
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v6}, Lcom/helpshift/util/AttachmentUtil;->a(Z)V

    invoke-virtual {p0, v0, v7}, Lcom/helpshift/HSAddIssueFragment;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v6

    .line 427
    goto/16 :goto_4

    .line 429
    :cond_c
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto/16 :goto_4

    :catch_1
    move-exception v0

    goto :goto_5

    .line 413
    :catch_2
    move-exception v0

    goto :goto_5
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 253
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Z)V

    .line 256
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v1, "conversationPrefillText"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 257
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v2, "activeConversation"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 259
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-direct {p0}, Lcom/helpshift/HSAddIssueFragment;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->j(Ljava/lang/String;)V

    .line 264
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->t:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->a(Ljava/lang/String;)V

    .line 265
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v1, ""

    const-string v2, "foregroundIssue"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    return-void

    .line 260
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    const-string v1, "dropMeta"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/helpshift/util/Meta;->a(Lcom/helpshift/HSCallable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    .line 531
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 532
    const-string v0, "i"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 533
    const-string v0, ""

    .line 534
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const-string v4, "conversationDetailSetTime"

    iget-object v5, v1, Lcom/helpshift/HSStorage;->b:Landroid/content/SharedPreferences;

    invoke-interface {v5, v4, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v4, v2, v8

    if-ltz v4, :cond_0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    const-wide/16 v4, 0x1c20

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    :cond_0
    const-string v2, "conversationDetail"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v2, "conversationDetail"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 535
    iget-object v2, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v3, "conversationPrefillText"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 537
    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    if-eqz v3, :cond_2

    .line 538
    iget-object v3, p0, Lcom/helpshift/HSAddIssueFragment;->d:Landroid/os/Bundle;

    const-string v4, "message"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 539
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 540
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 546
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 547
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 553
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 554
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v1, "screenshotPath"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSAddIssueFragment;->b(Ljava/lang/String;)V

    .line 555
    return-void

    .line 548
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 549
    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 551
    :cond_4
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 301
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 303
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__conversationDetail"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    .line 306
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->m:Landroid/widget/TextView;

    new-instance v1, Lcom/helpshift/HSAddIssueFragment$7;

    invoke-direct {v1, p0}, Lcom/helpshift/HSAddIssueFragment$7;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 319
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__username"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    .line 322
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/HSAddIssueFragment$8;

    invoke-direct {v1, p0}, Lcom/helpshift/HSAddIssueFragment$8;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 335
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__email"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    .line 338
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/HSAddIssueFragment$9;

    invoke-direct {v1, p0}, Lcom/helpshift/HSAddIssueFragment$9;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 351
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    const-string v1, "hs__email_required_hint"

    invoke-static {p0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 355
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-static {}, Lcom/helpshift/IdentityFilter;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    const-string v1, "Anonymous"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 359
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    invoke-static {v0}, Lcom/helpshift/IdentityFilter;->a(Lcom/helpshift/HSStorage;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 360
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setVisibility(I)V

    .line 367
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Lcom/helpshift/HSActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 369
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v0, v0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 370
    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->b()V

    .line 371
    const-string v1, "hs__new_conversation_header"

    invoke-static {p0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelper;->a(Ljava/lang/String;)V

    .line 373
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__screenshot"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    .line 376
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->s:Landroid/widget/ImageView;

    new-instance v1, Lcom/helpshift/HSAddIssueFragment$10;

    invoke-direct {v1, p0}, Lcom/helpshift/HSAddIssueFragment$10;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    const v0, 0x102001a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    .line 384
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->v:Landroid/widget/ImageButton;

    new-instance v1, Lcom/helpshift/HSAddIssueFragment$11;

    invoke-direct {v1, p0}, Lcom/helpshift/HSAddIssueFragment$11;-><init>(Lcom/helpshift/HSAddIssueFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    return-void

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->n:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v2, "username"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 364
    iget-object v0, p0, Lcom/helpshift/HSAddIssueFragment;->o:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/helpshift/HSAddIssueFragment;->e:Lcom/helpshift/HSStorage;

    const-string v2, "email"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
