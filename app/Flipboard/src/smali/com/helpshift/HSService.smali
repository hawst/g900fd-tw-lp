.class public final Lcom/helpshift/HSService;
.super Landroid/app/Service;
.source "HSService.java"


# static fields
.field private static f:Lcom/helpshift/util/HSPolling;


# instance fields
.field private a:Lcom/helpshift/HSApiData;

.field private b:Ljava/lang/Boolean;

.field private c:Lcom/helpshift/HSLifecycleCallbacks;

.field private final d:Landroid/os/IBinder;

.field private e:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 30
    iput-object v0, p0, Lcom/helpshift/HSService;->a:Lcom/helpshift/HSApiData;

    .line 33
    iput-object v0, p0, Lcom/helpshift/HSService;->c:Lcom/helpshift/HSLifecycleCallbacks;

    .line 42
    new-instance v0, Lcom/helpshift/HSService$HSBinder;

    invoke-direct {v0, p0}, Lcom/helpshift/HSService$HSBinder;-><init>(Lcom/helpshift/HSService;)V

    iput-object v0, p0, Lcom/helpshift/HSService;->d:Landroid/os/IBinder;

    .line 82
    new-instance v0, Lcom/helpshift/HSService$1;

    invoke-direct {v0, p0}, Lcom/helpshift/HSService$1;-><init>(Lcom/helpshift/HSService;)V

    iput-object v0, p0, Lcom/helpshift/HSService;->e:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSService;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/helpshift/HSService;->b:Ljava/lang/Boolean;

    return-object p1
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    if-eqz v0, :cond_0

    .line 136
    sget-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    invoke-virtual {v0}, Lcom/helpshift/util/HSPolling;->a()V

    .line 138
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSService;)Z
    .locals 5

    .prologue
    .line 29
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/helpshift/HSService;->c:Lcom/helpshift/HSLifecycleCallbacks;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/helpshift/HSLifecycleCallbacks;->a()Lcom/helpshift/HSLifecycleCallbacks;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSService;->c:Lcom/helpshift/HSLifecycleCallbacks;

    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSService;->c:Lcom/helpshift/HSLifecycleCallbacks;

    invoke-static {}, Lcom/helpshift/HSLifecycleCallbacks;->b()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/helpshift/HSService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/helpshift/HSService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0x64

    if-ne v3, v4, :cond_2

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Lcom/helpshift/util/HSPolling;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    return-object v0
.end method

.method static synthetic b(Lcom/helpshift/HSService;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/helpshift/HSService;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic c(Lcom/helpshift/HSService;)Lcom/helpshift/HSApiData;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/helpshift/HSService;->a:Lcom/helpshift/HSApiData;

    return-object v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/helpshift/HSService;->d:Landroid/os/IBinder;

    return-object v0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 126
    sget-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    if-eqz v0, :cond_0

    .line 127
    sget-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    iget-object v1, v0, Lcom/helpshift/util/HSPolling;->a:Landroid/os/Handler;

    iget-object v0, v0, Lcom/helpshift/util/HSPolling;->f:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 128
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    .line 131
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 132
    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 104
    iget-object v0, p0, Lcom/helpshift/HSService;->a:Lcom/helpshift/HSApiData;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSService;->a:Lcom/helpshift/HSApiData;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSService;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    .line 109
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSService;->b:Ljava/lang/Boolean;

    .line 112
    :cond_1
    sget-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    if-nez v0, :cond_2

    .line 113
    new-instance v0, Lcom/helpshift/util/HSPolling;

    iget-object v1, p0, Lcom/helpshift/HSService;->e:Landroid/os/Handler;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/helpshift/util/HSPolling;-><init>(Landroid/os/Handler;Ljava/lang/Boolean;Landroid/content/Context;)V

    .line 114
    sput-object v0, Lcom/helpshift/HSService;->f:Lcom/helpshift/util/HSPolling;

    iget-object v0, v0, Lcom/helpshift/util/HSPolling;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 117
    :cond_2
    const/4 v0, 0x2

    return v0
.end method
