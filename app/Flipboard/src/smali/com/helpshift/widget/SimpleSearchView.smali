.class public Lcom/helpshift/widget/SimpleSearchView;
.super Landroid/widget/LinearLayout;
.source "SimpleSearchView.java"


# instance fields
.field public a:Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

.field public b:Landroid/widget/EditText;

.field public c:Landroid/widget/ImageButton;

.field public d:Landroid/widget/ImageButton;

.field private e:Landroid/content/Context;

.field private f:Landroid/support/v4/widget/SearchViewCompat$OnQueryTextListenerCompat;

.field private g:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 29
    iput-object p1, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    .line 30
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->g:Landroid/view/inputmethod/InputMethodManager;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    iput-object p1, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    .line 36
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->g:Landroid/view/inputmethod/InputMethodManager;

    .line 37
    return-void
.end method

.method static synthetic a(Lcom/helpshift/widget/SimpleSearchView;)V
    .locals 2

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/helpshift/widget/SimpleSearchView;->b()V

    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->c:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->a:Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

    invoke-interface {v0}, Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;->a()Z

    return-void
.end method

.method static synthetic b(Lcom/helpshift/widget/SimpleSearchView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->d:Landroid/widget/ImageButton;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 93
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/widget/SimpleSearchView$4;

    invoke-direct {v1, p0}, Lcom/helpshift/widget/SimpleSearchView$4;-><init>(Lcom/helpshift/widget/SimpleSearchView;)V

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    return-void
.end method

.method static synthetic c(Lcom/helpshift/widget/SimpleSearchView;)Landroid/support/v4/widget/SearchViewCompat$OnQueryTextListenerCompat;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->f:Landroid/support/v4/widget/SearchViewCompat$OnQueryTextListenerCompat;

    return-object v0
.end method

.method static synthetic d(Lcom/helpshift/widget/SimpleSearchView;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic e(Lcom/helpshift/widget/SimpleSearchView;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/helpshift/widget/SimpleSearchView;->b()V

    return-void
.end method

.method static synthetic f(Lcom/helpshift/widget/SimpleSearchView;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->g:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 103
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->g:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/helpshift/widget/SimpleSearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 104
    return-void
.end method

.method public clearFocus()V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/helpshift/widget/SimpleSearchView;->a()V

    .line 129
    return-void
.end method

.method public getQuery()Ljava/lang/String;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 41
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 42
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    const-string v1, "id"

    const-string v2, "hs__search_query"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/SimpleSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    .line 43
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    const-string v1, "id"

    const-string v2, "hs__search_button"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/SimpleSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->c:Landroid/widget/ImageButton;

    .line 44
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->e:Landroid/content/Context;

    const-string v1, "id"

    const-string v2, "hs__search_query_clear"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/SimpleSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->d:Landroid/widget/ImageButton;

    .line 46
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->c:Landroid/widget/ImageButton;

    new-instance v1, Lcom/helpshift/widget/SimpleSearchView$1;

    invoke-direct {v1, p0}, Lcom/helpshift/widget/SimpleSearchView$1;-><init>(Lcom/helpshift/widget/SimpleSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/widget/SimpleSearchView$2;

    invoke-direct {v1, p0}, Lcom/helpshift/widget/SimpleSearchView$2;-><init>(Lcom/helpshift/widget/SimpleSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 73
    iget-object v0, p0, Lcom/helpshift/widget/SimpleSearchView;->d:Landroid/widget/ImageButton;

    new-instance v1, Lcom/helpshift/widget/SimpleSearchView$3;

    invoke-direct {v1, p0}, Lcom/helpshift/widget/SimpleSearchView$3;-><init>(Lcom/helpshift/widget/SimpleSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method

.method public setOnActionExpandListener(Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/helpshift/widget/SimpleSearchView;->a:Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

    .line 112
    return-void
.end method

.method public setQueryTextListener(Landroid/support/v4/widget/SearchViewCompat$OnQueryTextListenerCompat;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/helpshift/widget/SimpleSearchView;->f:Landroid/support/v4/widget/SearchViewCompat$OnQueryTextListenerCompat;

    .line 108
    return-void
.end method
