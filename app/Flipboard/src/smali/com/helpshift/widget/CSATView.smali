.class public Lcom/helpshift/widget/CSATView;
.super Landroid/widget/RelativeLayout;
.source "CSATView.java"

# interfaces
.implements Landroid/widget/RatingBar$OnRatingBarChangeListener;


# instance fields
.field a:Lcom/helpshift/widget/CSATDialog;

.field b:Lcom/helpshift/widget/CSATView$CSATListener;

.field public c:Landroid/widget/RelativeLayout;

.field private d:Landroid/widget/RatingBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    .line 28
    invoke-direct {p0, p1}, Lcom/helpshift/widget/CSATView;->a(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    .line 33
    invoke-direct {p0, p1}, Lcom/helpshift/widget/CSATView;->a(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    .line 38
    invoke-direct {p0, p1}, Lcom/helpshift/widget/CSATView;->a(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    sget v0, Lcom/helpshift/R$layout;->hs__csat_view:I

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 43
    new-instance v0, Lcom/helpshift/widget/CSATDialog;

    invoke-direct {v0, p1}, Lcom/helpshift/widget/CSATDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/widget/CSATView;->a:Lcom/helpshift/widget/CSATDialog;

    .line 44
    return-void
.end method


# virtual methods
.method protected getRatingBar()Landroid/widget/RatingBar;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/helpshift/widget/CSATView;->d:Landroid/widget/RatingBar;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 48
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 49
    sget v0, Lcom/helpshift/R$id;->ratingBar:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/helpshift/widget/CSATView;->d:Landroid/widget/RatingBar;

    .line 50
    iget-object v0, p0, Lcom/helpshift/widget/CSATView;->d:Landroid/widget/RatingBar;

    invoke-virtual {v0, p0}, Landroid/widget/RatingBar;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    .line 52
    sget v0, Lcom/helpshift/R$id;->csat_dislike_msg:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 53
    sget v1, Lcom/helpshift/R$id;->csat_like_msg:I

    invoke-virtual {p0, v1}, Lcom/helpshift/widget/CSATView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 54
    sget v2, Lcom/helpshift/R$id;->option_text:I

    invoke-virtual {p0, v2}, Lcom/helpshift/widget/CSATView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 56
    invoke-static {v0, v3}, Lcom/helpshift/util/HSColor;->a(Landroid/widget/TextView;F)V

    .line 57
    invoke-static {v1, v3}, Lcom/helpshift/util/HSColor;->a(Landroid/widget/TextView;F)V

    .line 58
    invoke-static {v2, v3}, Lcom/helpshift/util/HSColor;->a(Landroid/widget/TextView;F)V

    .line 60
    sget v0, Lcom/helpshift/R$id;->divider:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/helpshift/widget/CSATView;->c:Landroid/widget/RelativeLayout;

    .line 61
    return-void
.end method

.method public onRatingChanged(Landroid/widget/RatingBar;FZ)V
    .locals 1

    .prologue
    .line 65
    if-eqz p3, :cond_0

    .line 66
    iget-object v0, p0, Lcom/helpshift/widget/CSATView;->a:Lcom/helpshift/widget/CSATDialog;

    invoke-virtual {v0, p0}, Lcom/helpshift/widget/CSATDialog;->a(Lcom/helpshift/widget/CSATView;)V

    .line 68
    :cond_0
    return-void
.end method

.method public setCSATListener(Lcom/helpshift/widget/CSATView$CSATListener;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    .line 90
    return-void
.end method
