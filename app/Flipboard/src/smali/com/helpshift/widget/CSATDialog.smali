.class public Lcom/helpshift/widget/CSATDialog;
.super Landroid/app/Dialog;
.source "CSATDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/helpshift/widget/CSATView;

.field private c:Landroid/widget/RatingBar;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;

.field private f:F

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/helpshift/widget/CSATDialog;->g:Z

    .line 36
    iput-object p1, p0, Lcom/helpshift/widget/CSATDialog;->a:Landroid/content/Context;

    .line 37
    return-void
.end method


# virtual methods
.method protected final a(Lcom/helpshift/widget/CSATView;)V
    .locals 1

    .prologue
    .line 99
    iput-object p1, p0, Lcom/helpshift/widget/CSATDialog;->b:Lcom/helpshift/widget/CSATView;

    .line 100
    invoke-virtual {p1}, Lcom/helpshift/widget/CSATView;->getRatingBar()Landroid/widget/RatingBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RatingBar;->getRating()F

    move-result v0

    iput v0, p0, Lcom/helpshift/widget/CSATDialog;->f:F

    .line 101
    invoke-virtual {p0}, Lcom/helpshift/widget/CSATDialog;->show()V

    .line 102
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 81
    sget v1, Lcom/helpshift/R$id;->submit:I

    if-ne v0, v1, :cond_1

    .line 82
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->b:Lcom/helpshift/widget/CSATView;

    iget-object v1, p0, Lcom/helpshift/widget/CSATDialog;->c:Landroid/widget/RatingBar;

    invoke-virtual {v1}, Landroid/widget/RatingBar;->getRating()F

    move-result v1

    iget-object v2, p0, Lcom/helpshift/widget/CSATDialog;->e:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    if-eqz v3, :cond_0

    iget-object v0, v0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/widget/CSATView$CSATListener;->a(ILjava/lang/String;)V

    .line 83
    :cond_0
    iput-boolean v4, p0, Lcom/helpshift/widget/CSATDialog;->g:Z

    .line 84
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/helpshift/widget/CSATDialog;->a:Landroid/content/Context;

    sget v2, Lcom/helpshift/R$string;->hs__csat_submit_toast:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 85
    invoke-virtual {p0}, Lcom/helpshift/widget/CSATDialog;->dismiss()V

    .line 87
    :cond_1
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    .line 42
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATDialog;->requestWindowFeature(I)Z

    .line 43
    sget v0, Lcom/helpshift/R$layout;->hs__csat_dialog:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATDialog;->setContentView(I)V

    .line 44
    invoke-virtual {p0, p0}, Lcom/helpshift/widget/CSATDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 45
    invoke-virtual {p0, p0}, Lcom/helpshift/widget/CSATDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 47
    sget v0, Lcom/helpshift/R$id;->ratingBar:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RatingBar;

    iput-object v0, p0, Lcom/helpshift/widget/CSATDialog;->c:Landroid/widget/RatingBar;

    .line 48
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->c:Landroid/widget/RatingBar;

    invoke-virtual {v0, p0}, Landroid/widget/RatingBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 50
    sget v0, Lcom/helpshift/R$id;->like_status:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/helpshift/widget/CSATDialog;->d:Landroid/widget/TextView;

    .line 51
    sget v0, Lcom/helpshift/R$id;->additional_feedback:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/helpshift/widget/CSATDialog;->e:Landroid/widget/EditText;

    .line 53
    sget v0, Lcom/helpshift/R$id;->submit:I

    invoke-virtual {p0, v0}, Lcom/helpshift/widget/CSATDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 54
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/helpshift/widget/CSATDialog;->g:Z

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->b:Lcom/helpshift/widget/CSATView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/CSATView;->setVisibility(I)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/helpshift/widget/CSATView;->a:Lcom/helpshift/widget/CSATDialog;

    iget-object v1, v0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/helpshift/widget/CSATView;->b:Lcom/helpshift/widget/CSATView$CSATListener;

    invoke-interface {v0}, Lcom/helpshift/widget/CSATView$CSATListener;->a()V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    const-string v0, "cr"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->b:Lcom/helpshift/widget/CSATView;

    invoke-virtual {v0}, Lcom/helpshift/widget/CSATView;->getRatingBar()Landroid/widget/RatingBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    goto :goto_0
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 59
    const-string v0, "sr"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->c:Landroid/widget/RatingBar;

    iget v1, p0, Lcom/helpshift/widget/CSATDialog;->f:F

    invoke-virtual {v0, v1}, Landroid/widget/RatingBar;->setRating(F)V

    .line 61
    iget v0, p0, Lcom/helpshift/widget/CSATDialog;->f:F

    float-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->d:Landroid/widget/TextView;

    sget v1, Lcom/helpshift/R$string;->hs__csat_like_message:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/helpshift/widget/CSATDialog;->d:Landroid/widget/TextView;

    sget v1, Lcom/helpshift/R$string;->hs__csat_dislike_message:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 92
    sget v1, Lcom/helpshift/R$id;->ratingBar:I

    if-ne v0, v1, :cond_0

    .line 93
    const/4 v0, 0x1

    .line 95
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
