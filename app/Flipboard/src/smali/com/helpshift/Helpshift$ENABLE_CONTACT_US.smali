.class public final enum Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;
.super Ljava/lang/Enum;
.source "Helpshift.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

.field public static final enum b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

.field public static final enum c:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

.field private static final synthetic d:[Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    const-string v1, "ALWAYS"

    invoke-direct {v0, v1, v2}, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->a:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    .line 71
    new-instance v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    const-string v1, "NEVER"

    invoke-direct {v0, v1, v3}, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    .line 77
    new-instance v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    const-string v1, "AFTER_VIEWING_FAQS"

    invoke-direct {v0, v1, v4}, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->c:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    .line 62
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    sget-object v1, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->a:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    aput-object v1, v0, v2

    sget-object v1, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->b:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    aput-object v1, v0, v3

    sget-object v1, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->c:Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    aput-object v1, v0, v4

    sput-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->d:[Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    return-object v0
.end method

.method public static values()[Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->d:[Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    invoke-virtual {v0}, [Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/helpshift/Helpshift$ENABLE_CONTACT_US;

    return-object v0
.end method
