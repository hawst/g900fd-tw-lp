.class public final Lcom/helpshift/HSSection;
.super Lcom/helpshift/HSActivity;
.source "HSSection.java"


# instance fields
.field private o:Lcom/helpshift/HSApiData;

.field private p:Lcom/helpshift/HSStorage;

.field private q:Lcom/helpshift/HSSectionFragment;

.field private r:Ljava/lang/String;

.field private s:Lcom/helpshift/app/ActionBarHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/helpshift/HSActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/16 v1, 0x400

    const/4 v5, 0x1

    .line 76
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 78
    sput-boolean v5, Lcom/helpshift/HSAnalytics;->a:Z

    .line 79
    invoke-virtual {p0}, Lcom/helpshift/HSSection;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 80
    if-eqz v2, :cond_0

    .line 81
    const-string v0, "sectionPublishId"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/HSSection;->r:Ljava/lang/String;

    .line 84
    :cond_0
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSSection;->o:Lcom/helpshift/HSApiData;

    .line 85
    iget-object v0, p0, Lcom/helpshift/HSSection;->o:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSSection;->p:Lcom/helpshift/HSStorage;

    .line 87
    const-string v0, "showInFullScreen"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    invoke-virtual {p0}, Lcom/helpshift/HSSection;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 94
    :cond_1
    const-string v0, "layout"

    const-string v1, "hs__section"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSSection;->setContentView(I)V

    .line 98
    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    iput-object v0, p0, Lcom/helpshift/HSSection;->s:Lcom/helpshift/app/ActionBarHelper;

    .line 99
    iget-object v0, p0, Lcom/helpshift/HSSection;->s:Lcom/helpshift/app/ActionBarHelper;

    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->b()V

    .line 101
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->a:Ljava/util/Map;

    const-string v1, "hl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    const-string v0, "id"

    const-string v1, "hs__sectionFooter"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 105
    invoke-virtual {p0, v0}, Lcom/helpshift/HSSection;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 106
    sget-object v1, Lcom/helpshift/res/drawable/HSImages;->a:Ljava/util/Map;

    const-string v3, "newHSLogo"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/helpshift/res/drawable/HSDraw;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 108
    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 111
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    .line 112
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 113
    new-instance v1, Lcom/helpshift/HSSectionFragment;

    invoke-direct {v1}, Lcom/helpshift/HSSectionFragment;-><init>()V

    iput-object v1, p0, Lcom/helpshift/HSSection;->q:Lcom/helpshift/HSSectionFragment;

    .line 114
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 115
    const-string v3, "sectionPublishId"

    iget-object v4, p0, Lcom/helpshift/HSSection;->r:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    const-string v3, "decomp"

    invoke-virtual {v1, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 118
    iget-object v2, p0, Lcom/helpshift/HSSection;->q:Lcom/helpshift/HSSectionFragment;

    invoke-virtual {v2, v1}, Lcom/helpshift/HSSectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 120
    const-string v1, "id"

    const-string v2, "hs__sectionContainer"

    invoke-static {p0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/helpshift/HSSection;->q:Lcom/helpshift/HSSectionFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 123
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 124
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p0}, Lcom/helpshift/HSSection;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const-string v1, "menu"

    const-string v2, "hs__faqs_fragment"

    invoke-static {p0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 134
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 139
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 140
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 141
    invoke-virtual {p0}, Lcom/helpshift/HSSection;->finish()V

    .line 142
    const/4 v0, 0x1

    .line 144
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 63
    invoke-virtual {p0}, Lcom/helpshift/HSSection;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_0

    .line 65
    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/helpshift/HSSection;->isFinishing()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 67
    invoke-static {}, Lcom/helpshift/util/HSActivityUtil;->a()V

    .line 71
    :cond_0
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onPause()V

    .line 72
    return-void
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSSection;->p:Lcom/helpshift/HSStorage;

    const-string v1, "config"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lorg/json/JSONObject;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    invoke-static {v0}, Lcom/helpshift/res/values/HSConfig;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onResume()V

    .line 59
    return-void

    .line 54
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_0
.end method

.method public final bridge synthetic onStart()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStart()V

    return-void
.end method

.method public final bridge synthetic onStop()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStop()V

    return-void
.end method
