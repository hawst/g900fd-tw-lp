.class public final Lcom/helpshift/HSSectionFragment;
.super Landroid/support/v4/app/ListFragment;
.source "HSSectionFragment.java"

# interfaces
.implements Lcom/helpshift/HSFaqSyncStatusEvents;
.implements Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;
.implements Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Ljava/lang/String;

.field private D:Lcom/helpshift/app/ActionBarHelper;

.field private E:Landroid/os/Handler;

.field private F:Landroid/os/Handler;

.field private i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Faq;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Faq;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/helpshift/HSActivity;

.field private l:Lcom/helpshift/HSApiData;

.field private m:Landroid/widget/ArrayAdapter;

.field private n:Landroid/widget/ArrayAdapter;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Landroid/widget/ListView;

.field private s:Landroid/view/View;

.field private t:Landroid/view/MenuItem;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/Boolean;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->j:Ljava/util/ArrayList;

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->w:Ljava/lang/String;

    .line 53
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    .line 54
    iput-boolean v1, p0, Lcom/helpshift/HSSectionFragment;->y:Z

    .line 57
    iput-boolean v1, p0, Lcom/helpshift/HSSectionFragment;->B:Z

    .line 279
    new-instance v0, Lcom/helpshift/HSSectionFragment$4;

    invoke-direct {v0, p0}, Lcom/helpshift/HSSectionFragment$4;-><init>(Lcom/helpshift/HSSectionFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->E:Landroid/os/Handler;

    .line 288
    new-instance v0, Lcom/helpshift/HSSectionFragment$5;

    invoke-direct {v0, p0}, Lcom/helpshift/HSSectionFragment$5;-><init>(Lcom/helpshift/HSSectionFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->F:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSSectionFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic a(Lcom/helpshift/HSSectionFragment;Lcom/helpshift/Section;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/helpshift/HSSectionFragment;->a(Lcom/helpshift/Section;)V

    return-void
.end method

.method private a(Lcom/helpshift/Section;)V
    .locals 5

    .prologue
    .line 232
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 233
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    iget-object v0, v0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 235
    iget-object v1, p1, Lcom/helpshift/Section;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelper;->a(Ljava/lang/String;)V

    .line 238
    :cond_0
    if-nez p1, :cond_1

    .line 239
    const/16 v0, 0x194

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    invoke-static {v0, v1}, Lcom/helpshift/util/HSErrors;->a(ILandroid/content/Context;)V

    .line 276
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 243
    iget-object v0, p1, Lcom/helpshift/Section;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    .line 244
    iget-object v0, p1, Lcom/helpshift/Section;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->p:Ljava/lang/String;

    .line 254
    invoke-virtual {p0}, Lcom/helpshift/HSSectionFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->B:Z

    if-nez v0, :cond_2

    .line 256
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 257
    const-string v1, "id"

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 258
    const-string v1, "b"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 259
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->B:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    :cond_2
    :goto_1
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 265
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 266
    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 260
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_1

    .line 269
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_4

    .line 270
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    new-instance v1, Lcom/helpshift/Faq;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-string v3, "hs__faqs_search_footer"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "0"

    const-string v4, "empty_status"

    invoke-direct {v1, v2, v3, v4}, Lcom/helpshift/Faq;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    :cond_4
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->m:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Faq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 343
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/helpshift/HSSectionFragment;->z:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 344
    :cond_0
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 349
    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    move v1, v0

    .line 350
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 351
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 352
    iget-object v2, v0, Lcom/helpshift/Faq;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->p:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 353
    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 346
    :cond_2
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 356
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 357
    return-void
.end method

.method static synthetic b(Lcom/helpshift/HSSectionFragment;)Lcom/helpshift/app/ActionBarHelper;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->D:Lcom/helpshift/app/ActionBarHelper;

    return-object v0
.end method

.method static synthetic c(Lcom/helpshift/HSSectionFragment;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/helpshift/HSSectionFragment;->e()V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 387
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 389
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 391
    :try_start_0
    const-string v1, "s"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    :goto_0
    const-string v1, "s"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 396
    iput-object p1, p0, Lcom/helpshift/HSSectionFragment;->v:Ljava/lang/String;

    .line 398
    :cond_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic d(Lcom/helpshift/HSSectionFragment;)Lcom/helpshift/HSActivity;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSSectionFragment;->a(Ljava/util/ArrayList;)V

    .line 340
    return-void
.end method

.method static synthetic e(Lcom/helpshift/HSSectionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->u:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 382
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->D:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelper;->b(Landroid/view/MenuItem;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 383
    invoke-direct {p0, v0}, Lcom/helpshift/HSSectionFragment;->c(Ljava/lang/String;)V

    .line 384
    return-void
.end method

.method static synthetic f(Lcom/helpshift/HSSectionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->w:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSApiData;->d(Ljava/lang/String;)Lcom/helpshift/Section;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSSectionFragment;->a(Lcom/helpshift/Section;)V

    .line 218
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->y:Z

    if-eqz v0, :cond_1

    .line 308
    invoke-direct {p0}, Lcom/helpshift/HSSectionFragment;->e()V

    .line 309
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 313
    :goto_0
    iget-object v1, v0, Lcom/helpshift/Faq;->e:Ljava/lang/String;

    const-string v2, "empty_status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 314
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-class v3, Lcom/helpshift/HSQuestion;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 315
    const-string v2, "questionPublishId"

    iget-object v0, v0, Lcom/helpshift/Faq;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    const-string v0, "decomp"

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 317
    const-string v0, "showInFullScreen"

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    invoke-static {v2}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 318
    invoke-virtual {p0}, Lcom/helpshift/HSSectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 319
    const-string v0, "isRoot"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 320
    invoke-virtual {p0}, Lcom/helpshift/HSSectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 322
    :cond_0
    return-void

    .line 311
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 408
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->w:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/helpshift/HSSectionFragment;->c(Ljava/lang/String;)V

    .line 413
    :goto_0
    invoke-virtual {p0, p1}, Lcom/helpshift/HSSectionFragment;->b(Ljava/lang/String;)V

    .line 414
    const/4 v0, 0x0

    return v0

    .line 411
    :cond_0
    iput-object p1, p0, Lcom/helpshift/HSSectionFragment;->w:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 325
    const/4 v0, 0x0

    .line 326
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/HSSectionFragment;->u:Ljava/lang/String;

    .line 327
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->C:Ljava/lang/String;

    const-string v2, "zh"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->C:Ljava/lang/String;

    const-string v2, "ja"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->C:Ljava/lang/String;

    const-string v2, "ko"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    :cond_0
    const/4 v0, 0x1

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->u:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->u:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_3

    if-nez v0, :cond_3

    .line 332
    :cond_2
    invoke-direct {p0}, Lcom/helpshift/HSSectionFragment;->d()V

    .line 336
    :goto_0
    return-void

    .line 334
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSSectionFragment;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 419
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->v:Ljava/lang/String;

    .line 420
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->w:Ljava/lang/String;

    .line 421
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->z:Z

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    :goto_0
    invoke-direct {p0}, Lcom/helpshift/HSSectionFragment;->d()V

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSSectionFragment;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iput-boolean v4, p0, Lcom/helpshift/HSSectionFragment;->y:Z

    .line 422
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    invoke-virtual {v0, v3}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 423
    return v4

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 428
    invoke-direct {p0}, Lcom/helpshift/HSSectionFragment;->e()V

    .line 429
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->m:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSSectionFragment;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->m:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->y:Z

    .line 430
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->a:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    invoke-virtual {v0, v2}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 433
    :cond_1
    return v2
.end method

.method public final j_()V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 223
    new-instance v1, Lcom/helpshift/HSSectionFragment$3;

    invoke-direct {v1, p0}, Lcom/helpshift/HSSectionFragment$3;-><init>(Lcom/helpshift/HSSectionFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 228
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 63
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 64
    invoke-virtual {p0}, Lcom/helpshift/HSSectionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_1

    .line 66
    const-string v1, "sectionPublishId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    .line 67
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 68
    const-string v1, ""

    iput-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    .line 70
    :cond_0
    sget-object v1, Lcom/helpshift/ContactUsFilter$LOCATION;->b:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v1}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/helpshift/HSSectionFragment;->z:Z

    .line 71
    const-string v1, "decomp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v2, :cond_1

    .line 72
    sput-boolean v2, Lcom/helpshift/HSAnalytics;->a:Z

    .line 73
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    .line 76
    :cond_1
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4

    .prologue
    .line 203
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    const-string v0, "id"

    const-string v1, "hs__action_search"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    .line 205
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 207
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->D:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-string v3, "hs__search_hint"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;Ljava/lang/String;)V

    .line 208
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->D:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V

    .line 209
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->D:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->t:Landroid/view/MenuItem;

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V

    .line 211
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->j()V

    .line 213
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/helpshift/HSSectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSActivity;

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    .line 82
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    iget-object v0, v0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->D:Lcom/helpshift/app/ActionBarHelper;

    .line 83
    new-instance v0, Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    invoke-direct {v0, v1}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    .line 85
    iget-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->z:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-string v1, "layout"

    const-string v2, "hs__search_list_footer"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    .line 93
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-string v1, "layout"

    const-string v2, "hs__simple_list_item_1"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 95
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->i:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v0, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/helpshift/HSSectionFragment;->m:Landroid/widget/ArrayAdapter;

    .line 98
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->j:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v0, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/helpshift/HSSectionFragment;->n:Landroid/widget/ArrayAdapter;

    .line 101
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->m:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSSectionFragment;->a(Landroid/widget/ListAdapter;)V

    .line 102
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->C:Ljava/lang/String;

    .line 103
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-string v1, "layout"

    const-string v2, "hs__no_faqs"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 0

    .prologue
    .line 196
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onDestroy()V

    .line 197
    invoke-static {p0}, Lcom/helpshift/HSApiData;->b(Lcom/helpshift/HSFaqSyncStatusEvents;)V

    .line 198
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 172
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 177
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/helpshift/HSSectionFragment;->A:Z

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 180
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 181
    const-string v2, "id"

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 182
    const-string v2, "b"

    invoke-static {v2, v1}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 183
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/helpshift/HSSectionFragment;->B:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 185
    :cond_1
    :goto_0
    sget-object v1, Lcom/helpshift/ContactUsFilter$LOCATION;->a:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v1}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    iget-boolean v2, p0, Lcom/helpshift/HSSectionFragment;->y:Z

    if-nez v2, :cond_3

    :goto_1
    invoke-virtual {v1, v0}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 191
    :cond_2
    return-void

    .line 189
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 109
    invoke-virtual {p0}, Lcom/helpshift/HSSectionFragment;->k_()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    .line 111
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_1

    .line 112
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->r:Landroid/widget/ListView;

    new-instance v1, Lcom/helpshift/HSSectionFragment$1;

    invoke-direct {v1, p0}, Lcom/helpshift/HSSectionFragment$1;-><init>(Lcom/helpshift/HSSectionFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 122
    iget-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->z:Z

    if-ne v0, v4, :cond_0

    .line 123
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->s:Landroid/view/View;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->k:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "report_issue"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 125
    new-instance v1, Lcom/helpshift/HSSectionFragment$2;

    invoke-direct {v1, p0}, Lcom/helpshift/HSSectionFragment$2;-><init>(Lcom/helpshift/HSSectionFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    :cond_0
    invoke-virtual {p0, v4}, Lcom/helpshift/HSSectionFragment;->setHasOptionsMenu(Z)V

    .line 140
    :cond_1
    invoke-static {p0}, Lcom/helpshift/HSApiData;->a(Lcom/helpshift/HSFaqSyncStatusEvents;)V

    .line 142
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->x:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 143
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->E:Landroid/os/Handler;

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 147
    :goto_0
    return-void

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->l:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->E:Landroid/os/Handler;

    iget-object v3, p0, Lcom/helpshift/HSSectionFragment;->F:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2, v3}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public final setUserVisibleHint(Z)V
    .locals 3

    .prologue
    .line 151
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->setUserVisibleHint(Z)V

    .line 152
    iput-boolean p1, p0, Lcom/helpshift/HSSectionFragment;->A:Z

    .line 158
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 160
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 161
    const-string v1, "id"

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 162
    const-string v1, "b"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/helpshift/HSSectionFragment;->B:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 164
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_0
.end method
