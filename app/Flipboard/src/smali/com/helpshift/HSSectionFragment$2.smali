.class Lcom/helpshift/HSSectionFragment$2;
.super Ljava/lang/Object;
.source "HSSectionFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/helpshift/HSSectionFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSSectionFragment;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v0}, Lcom/helpshift/HSSectionFragment;->c(Lcom/helpshift/HSSectionFragment;)V

    .line 128
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v1}, Lcom/helpshift/HSSectionFragment;->d(Lcom/helpshift/HSSectionFragment;)Lcom/helpshift/HSActivity;

    move-result-object v1

    const-class v2, Lcom/helpshift/HSConversation;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 129
    const-string v1, "message"

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v2}, Lcom/helpshift/HSSectionFragment;->e(Lcom/helpshift/HSSectionFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v1}, Lcom/helpshift/HSSectionFragment;->b(Lcom/helpshift/HSSectionFragment;)Lcom/helpshift/app/ActionBarHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v2}, Lcom/helpshift/HSSectionFragment;->a(Lcom/helpshift/HSSectionFragment;)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;)V

    .line 131
    const-string v1, "showInFullScreen"

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v2}, Lcom/helpshift/HSSectionFragment;->d(Lcom/helpshift/HSSectionFragment;)Lcom/helpshift/HSActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 132
    const-string v1, "showConvOnReportIssue"

    iget-object v2, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-static {v2}, Lcom/helpshift/HSSectionFragment;->d(Lcom/helpshift/HSSectionFragment;)Lcom/helpshift/HSActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/helpshift/HSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "showConvOnReportIssue"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 133
    const-string v1, "chatLaunchSource"

    const-string v2, "support"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 134
    iget-object v1, p0, Lcom/helpshift/HSSectionFragment$2;->a:Lcom/helpshift/HSSectionFragment;

    invoke-virtual {v1}, Lcom/helpshift/HSSectionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 135
    return-void
.end method
