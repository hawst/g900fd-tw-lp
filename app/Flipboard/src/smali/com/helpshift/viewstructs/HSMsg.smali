.class public final Lcom/helpshift/viewstructs/HSMsg;
.super Ljava/lang/Object;
.source "HSMsg.java"


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-string v0, "HelpShiftDebug"

    sput-object v0, Lcom/helpshift/viewstructs/HSMsg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/Boolean;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 23
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    .line 24
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 25
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->l:Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    .line 34
    iput-object p3, p0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    .line 35
    const-string v0, "(?i)(\\n|\\s|^)((?:https?://|market://|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:\'\\\".,<>?\u00ab\u00bb\u201c\u201d\u2018\u2019]))"

    const-string v1, "(?:[0+]?\\d[\\s-.]?\\d{0,3})?[\\s-.]?\\(?\\d{3}\\)?[\\s-.]?\\d{3}[\\s-.]?\\d{4}"

    const-string v2, "$1<a href=\"$2\">$2</a>"

    invoke-virtual {p4, v0, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\n"

    const-string v3, "<br/>"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "<a href=\"(?!\\w*?://)(.*?)\">"

    const-string v3, "<a href=\"http://$1\">"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "<a href=\"(.*?)\">(https?://)?(www\\.)?([^<]{30}).*?</a>"

    const-string v3, "<a href=\"$1\">$4&#8230;</a>"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "<a href=\"tel:$0\">$0</a>"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    .line 36
    iput-object p6, p0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    .line 37
    iput-object p7, p0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    .line 38
    iput p8, p0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    .line 39
    iput-object p9, p0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 41
    const-string v0, "admin"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->l:Ljava/lang/String;

    .line 46
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 49
    :try_start_0
    sget-object v1, Lcom/helpshift/util/HSFormat;->d:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, p5}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 54
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/helpshift/viewstructs/HSMsg;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/helpshift/util/HSFormat;->e:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/viewstructs/HSMsg;->e:Ljava/lang/String;

    .line 55
    return-void

    .line 50
    :catch_0
    move-exception v1

    .line 51
    sget-object v2, Lcom/helpshift/viewstructs/HSMsg;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/text/ParseException;->toString()Ljava/lang/String;

    goto :goto_0
.end method
