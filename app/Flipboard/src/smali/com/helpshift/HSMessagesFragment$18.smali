.class Lcom/helpshift/HSMessagesFragment$18;
.super Landroid/os/Handler;
.source "HSMessagesFragment.java"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/helpshift/HSMessagesFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSMessagesFragment;I)V
    .locals 0

    .prologue
    .line 1021
    iput-object p1, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    iput p2, p0, Lcom/helpshift/HSMessagesFragment$18;->a:I

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->r(Lcom/helpshift/HSMessagesFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/helpshift/HSMessagesFragment$18;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 1024
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/util/HashMap;

    .line 1025
    const-string v2, "response"

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/json/JSONObject;

    .line 1028
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1029
    const-string v3, "type"

    const-string v4, "url"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1030
    const-string v3, "body"

    const-string v4, "meta"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v4, "attachments"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    const-string v4, "url"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1031
    const-string v1, "id"

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v3}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1032
    const-string v1, "m"

    invoke-static {v1, v2}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1034
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-virtual {v1}, Lcom/helpshift/HSMessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v2}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v2

    iget-object v3, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    iget-object v4, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4}, Lcom/helpshift/util/AttachmentUtil;->a(Landroid/app/Activity;Lcom/helpshift/HSApiData;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1042
    :goto_1
    :try_start_2
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    const-string v2, "localRscMessage_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1048
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->l(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSStorage;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v2}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->r(Lcom/helpshift/HSMessagesFragment;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/helpshift/HSMessagesFragment$18;->a:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1064
    :goto_2
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->c(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/customadapters/MessagesAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 1065
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0, p1}, Lcom/helpshift/HSMessagesFragment;->a(Lcom/helpshift/HSMessagesFragment;Landroid/os/Message;)V

    .line 1066
    return-void

    .line 1051
    :cond_0
    const/4 v1, 0x0

    :try_start_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 1052
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    .line 1053
    const-string v1, ""

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    .line 1054
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 1056
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v3}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1057
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v3}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1058
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;

    move-result-object v1

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment$18;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v2}, Lcom/helpshift/HSMessagesFragment;->q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v1, v0, v2, v3}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    .line 1060
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_2

    :catch_1
    move-exception v1

    goto/16 :goto_1

    :catch_2
    move-exception v1

    goto/16 :goto_0
.end method
