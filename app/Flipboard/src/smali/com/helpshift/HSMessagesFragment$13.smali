.class Lcom/helpshift/HSMessagesFragment$13;
.super Ljava/lang/Object;
.source "HSMessagesFragment.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Landroid/widget/ImageButton;

.field final synthetic b:Lcom/helpshift/HSMessagesFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSMessagesFragment;Landroid/widget/ImageButton;)V
    .locals 0

    .prologue
    .line 727
    iput-object p1, p0, Lcom/helpshift/HSMessagesFragment$13;->b:Lcom/helpshift/HSMessagesFragment;

    iput-object p2, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 729
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 732
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 736
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0, v1}, Lcom/helpshift/HSMessagesFragment;->a(Lcom/helpshift/HSMessagesFragment;Z)Z

    .line 737
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 738
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 739
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 740
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->f(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->b(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 746
    :goto_0
    return-void

    .line 742
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 743
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 744
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$13;->b:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->f(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$13;->a:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->c(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
