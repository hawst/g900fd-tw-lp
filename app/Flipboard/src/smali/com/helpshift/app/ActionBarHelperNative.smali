.class public Lcom/helpshift/app/ActionBarHelperNative;
.super Lcom/helpshift/app/ActionBarHelper;
.source "ActionBarHelperNative.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/helpshift/app/ActionBarHelper;-><init>(Landroid/app/Activity;)V

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 87
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 89
    instance-of v0, v0, Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 90
    invoke-static {p1}, Landroid/support/v4/view/MenuItemCompat;->b(Landroid/view/MenuItem;)Z

    .line 92
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/helpshift/app/ActionBarHelperNative$2;

    invoke-direct {v0, p0, p2}, Lcom/helpshift/app/ActionBarHelperNative$2;-><init>(Lcom/helpshift/app/ActionBarHelperNative;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V

    invoke-interface {p1, v0}, Landroid/view/MenuItem;->setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;

    .line 83
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V
    .locals 2

    .prologue
    .line 53
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 55
    instance-of v1, v0, Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    .line 56
    check-cast v0, Landroid/widget/SearchView;

    new-instance v1, Lcom/helpshift/app/ActionBarHelperNative$1;

    invoke-direct {v1, p0, p2}, Lcom/helpshift/app/ActionBarHelperNative$1;-><init>(Lcom/helpshift/app/ActionBarHelperNative;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 68
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 96
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 98
    instance-of v1, v0, Landroid/widget/SearchView;

    if-eqz v1, :cond_0

    .line 99
    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0, p2}, Landroid/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 101
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperNative;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 29
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperNative;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setProgressBarIndeterminateVisibility(Z)V

    .line 44
    return-void
.end method

.method public final b(Landroid/view/MenuItem;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 106
    const-string v1, ""

    .line 108
    instance-of v2, v0, Landroid/widget/SearchView;

    if-eqz v2, :cond_0

    .line 109
    check-cast v0, Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperNative;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 24
    return-void
.end method

.method public final c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperNative;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/view/MenuItem;)V
    .locals 1

    .prologue
    .line 117
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_0

    .line 120
    invoke-interface {p1}, Landroid/view/MenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 122
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperNative;->a:Landroid/app/Activity;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    .line 39
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperNative;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 49
    return-void
.end method
