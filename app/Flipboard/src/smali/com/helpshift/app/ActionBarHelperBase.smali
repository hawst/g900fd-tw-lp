.class public Lcom/helpshift/app/ActionBarHelperBase;
.super Lcom/helpshift/app/ActionBarHelper;
.source "ActionBarHelperBase.java"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field protected b:Z

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/view/LayoutInflater;

.field private j:Z

.field private k:Z

.field private l:Landroid/view/ContextThemeWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/helpshift/app/ActionBarHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/helpshift/app/ActionBarHelperBase;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    invoke-direct {p0, p1}, Lcom/helpshift/app/ActionBarHelper;-><init>(Landroid/app/Activity;)V

    .line 38
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->d:Ljava/util/Set;

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->e:Ljava/util/Map;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->f:Ljava/util/Map;

    .line 41
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->g:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->h:Ljava/util/Map;

    .line 44
    iput-boolean v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->j:Z

    .line 45
    iput-boolean v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->k:Z

    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->l:Landroid/view/ContextThemeWrapper;

    .line 47
    iput-boolean v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->b:Z

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/helpshift/app/ActionBarHelperBase;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->e:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/helpshift/app/ActionBarHelperBase;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->i:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic c(Lcom/helpshift/app/ActionBarHelperBase;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->f:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d(Lcom/helpshift/app/ActionBarHelperBase;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->d:Ljava/util/Set;

    return-object v0
.end method

.method private d(Landroid/view/MenuItem;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 122
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v2

    .line 123
    if-nez v2, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v0, p1

    .line 127
    check-cast v0, Lcom/helpshift/app/SimpleMenuItem;

    invoke-virtual {v0}, Lcom/helpshift/app/SimpleMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_2

    .line 129
    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 133
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 141
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v1, "hs__actionbar_compat_button_width"

    invoke-static {v0, v1}, Lcom/helpshift/util/HSRes;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v3, "hs__actionbarCompatItemBaseStyle"

    invoke-static {v1, v3}, Lcom/helpshift/util/HSRes;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    new-instance v3, Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-direct {v3, v4, v6, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v1, v0, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/helpshift/app/ActionBarHelperBase$2;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/app/ActionBarHelperBase$2;-><init>(Lcom/helpshift/app/ActionBarHelperBase;Landroid/view/MenuItem;)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    check-cast p1, Lcom/helpshift/app/SimpleMenuItem;

    new-instance v0, Lcom/helpshift/app/ActionBarHelperBase$3;

    invoke-direct {v0, p0, v3}, Lcom/helpshift/app/ActionBarHelperBase$3;-><init>(Lcom/helpshift/app/ActionBarHelperBase;Landroid/widget/ImageButton;)V

    iput-object v0, p1, Lcom/helpshift/app/SimpleMenuItem;->a:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemChangedListener;

    goto :goto_0

    .line 135
    :sswitch_0
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->i:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v3, "layout"

    const-string v4, "hs__actionbar_compat_home"

    invoke-static {v1, v3, v4}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1, v2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/helpshift/app/ActionBarHelperBase$HomeView;

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v3, "id"

    const-string v4, "hs__actionbar_compat_up"

    invoke-static {v1, v3, v4}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    if-eqz v0, :cond_0

    new-instance v1, Lcom/helpshift/app/ActionBarHelperBase$1;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/app/ActionBarHelperBase$1;-><init>(Lcom/helpshift/app/ActionBarHelperBase;Landroid/view/MenuItem;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v5}, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->setClickable(Z)V

    invoke-virtual {v0, v5}, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->setFocusable(Z)V

    invoke-interface {p1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v3, v0, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 138
    :sswitch_1
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v1, "hs__actionbarCompatProgressIndicatorStyle"

    invoke-static {v0, v1}, Lcom/helpshift/util/HSRes;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v3, "hs__actionbar_compat_button_width"

    invoke-static {v1, v3}, Lcom/helpshift/util/HSRes;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    iget-object v3, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v4, "hs__actionbar_compat_height"

    invoke-static {v3, v4}, Lcom/helpshift/util/HSRes;->d(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    new-instance v4, Landroid/widget/ProgressBar;

    iget-object v5, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-direct {v4, v5, v6, v0}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/lit8 v3, v0, 0x2

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v5, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sub-int v6, v0, v3

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v5, v6, v1, v0, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->k:Z

    if-eqz v0, :cond_3

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_1
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v1, "id"

    const-string v3, "hs__actionbar_compat_item_refresh_progress"

    invoke-static {v0, v1, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setId(I)V

    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    .line 133
    nop

    :sswitch_data_0
    .sparse-switch
        0x102000d -> :sswitch_1
        0x102002c -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/helpshift/app/ActionBarHelperBase;->c:Ljava/lang/String;

    return-object v0
.end method

.method private h()Landroid/view/ViewGroup;
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v1, "id"

    const-string v2, "hs__actionbar_compat"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 233
    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 2

    .prologue
    .line 238
    new-instance v0, Lcom/helpshift/app/ActionBarHelperBase$WrappedMenuInflater;

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-direct {v0, p0, v1, p1}, Lcom/helpshift/app/ActionBarHelperBase$WrappedMenuInflater;-><init>(Lcom/helpshift/app/ActionBarHelperBase;Landroid/content/Context;Landroid/view/MenuInflater;)V

    return-object v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/app/Activity;->requestWindowFeature(I)Z

    .line 56
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->i:Landroid/view/LayoutInflater;

    .line 57
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const v11, 0x1020016

    const/4 v1, 0x0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/os/Bundle;)V

    .line 62
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v2, "layout"

    const-string v3, "hs__actionbar_compat"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 63
    iget-object v2, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3, v0}, Landroid/view/Window;->setFeatureInt(II)V

    .line 65
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    new-instance v3, Lcom/helpshift/app/SimpleMenu;

    iget-object v4, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-direct {v3, v4}, Lcom/helpshift/app/SimpleMenu;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/helpshift/app/SimpleMenuItem;

    const v5, 0x102002c

    iget-object v6, v2, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v3, v5, v1, v6}, Lcom/helpshift/app/SimpleMenuItem;-><init>(Lcom/helpshift/app/SimpleMenu;IILjava/lang/CharSequence;)V

    iget v5, v2, Landroid/content/pm/ApplicationInfo;->icon:I

    invoke-virtual {v4, v5}, Lcom/helpshift/app/SimpleMenuItem;->setIcon(I)Landroid/view/MenuItem;

    invoke-direct {p0, v4}, Lcom/helpshift/app/ActionBarHelperBase;->d(Landroid/view/MenuItem;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v4, v1, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    new-instance v5, Landroid/widget/TextView;

    iget-object v6, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v9, "attr"

    const-string v10, "hs__actionbarCompatTitleStyle"

    invoke-static {v8, v9, v10}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    invoke-direct {v5, v6, v7, v8}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setId(I)V

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/helpshift/app/SimpleMenuItem;

    const v4, 0x102000d

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->name:Ljava/lang/String;

    invoke-direct {v0, v3, v4, v1, v2}, Lcom/helpshift/app/SimpleMenuItem;-><init>(Lcom/helpshift/app/SimpleMenu;IILjava/lang/CharSequence;)V

    invoke-direct {p0, v0}, Lcom/helpshift/app/ActionBarHelperBase;->d(Landroid/view/MenuItem;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->j:Z

    .line 67
    :cond_0
    new-instance v3, Lcom/helpshift/app/SimpleMenu;

    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-direct {v3, v0}, Lcom/helpshift/app/SimpleMenu;-><init>(Landroid/content/Context;)V

    .line 68
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v0, v1, v3}, Landroid/app/Activity;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    .line 69
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v0, v3}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move v0, v1

    .line 72
    :goto_0
    invoke-virtual {v3}, Lcom/helpshift/app/SimpleMenu;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 73
    invoke-virtual {v3, v0}, Lcom/helpshift/app/SimpleMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 74
    iget-object v4, p0, Lcom/helpshift/app/ActionBarHelperBase;->d:Ljava/util/Set;

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 75
    invoke-direct {p0, v2}, Lcom/helpshift/app/ActionBarHelperBase;->d(Landroid/view/MenuItem;)V

    .line 72
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v2, v1

    .line 80
    :goto_1
    invoke-virtual {v3}, Lcom/helpshift/app/SimpleMenu;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 81
    invoke-virtual {v3, v2}, Lcom/helpshift/app/SimpleMenu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 82
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->d:Ljava/util/Set;

    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 83
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v5, "id"

    const-string v6, "hs__action_search"

    invoke-static {v1, v5, v6}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iget-object v5, p0, Lcom/helpshift/app/ActionBarHelperBase;->g:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    if-ne v0, v1, :cond_3

    iget-object v5, p0, Lcom/helpshift/app/ActionBarHelperBase;->g:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;

    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/helpshift/widget/SimpleSearchView;

    new-instance v5, Lcom/helpshift/app/ActionBarHelperBase$5;

    invoke-direct {v5, p0, v0}, Lcom/helpshift/app/ActionBarHelperBase$5;-><init>(Lcom/helpshift/app/ActionBarHelperBase;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V

    invoke-virtual {v1, v5}, Lcom/helpshift/widget/SimpleSearchView;->setQueryTextListener(Landroid/support/v4/widget/SearchViewCompat$OnQueryTextListenerCompat;)V

    .line 84
    :cond_3
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v4, "id"

    const-string v5, "hs__action_search"

    invoke-static {v1, v4, v5}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iget-object v4, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v5, "id"

    const-string v6, "hs__action_report_issue"

    invoke-static {v4, v5, v6}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v5

    invoke-virtual {v5, v11}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v6, p0, Lcom/helpshift/app/ActionBarHelperBase;->h:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-ne v0, v1, :cond_4

    iget-object v6, p0, Lcom/helpshift/app/ActionBarHelperBase;->h:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;

    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v6

    invoke-virtual {v6, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/helpshift/widget/SimpleSearchView;

    new-instance v6, Lcom/helpshift/app/ActionBarHelperBase$4;

    invoke-direct {v6, p0, v5, v4, v0}, Lcom/helpshift/app/ActionBarHelperBase$4;-><init>(Lcom/helpshift/app/ActionBarHelperBase;Landroid/view/View;Landroid/view/View;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V

    invoke-virtual {v1, v6}, Lcom/helpshift/widget/SimpleSearchView;->setOnActionExpandListener(Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;)V

    .line 80
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_1

    .line 87
    :cond_5
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 483
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v1, "id"

    const-string v2, "hs__action_search"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 484
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/helpshift/widget/SimpleSearchView;

    .line 485
    invoke-virtual {v0}, Lcom/helpshift/widget/SimpleSearchView;->a()V

    iget-object v1, v0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/widget/SimpleSearchView;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/widget/SimpleSearchView;->c:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/widget/SimpleSearchView;->b:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/helpshift/widget/SimpleSearchView;->a:Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

    invoke-interface {v0}, Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;->b()Z

    .line 486
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->h:Ljava/util/Map;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 297
    return-void
.end method

.method public final a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->g:Ljava/util/Map;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 292
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    invoke-virtual {v0, p1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 249
    iget-boolean v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->j:Z

    if-eqz v0, :cond_0

    .line 250
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x1020016

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 251
    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->j:Z

    if-eqz v0, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v2, "id"

    const-string v3, "hs__actionbar_compat_item_refresh_progress"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 275
    if-eqz p1, :cond_1

    .line 276
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 281
    :cond_0
    :goto_0
    iput-boolean p1, p0, Lcom/helpshift/app/ActionBarHelperBase;->k:Z

    .line 282
    return-void

    .line 278
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Landroid/view/MenuItem;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 490
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 491
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v2, "id"

    const-string v3, "hs__action_search"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 493
    const-string v0, ""

    .line 495
    if-ne v1, v2, :cond_0

    .line 496
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/helpshift/widget/SimpleSearchView;

    .line 497
    invoke-virtual {v0}, Lcom/helpshift/widget/SimpleSearchView;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 500
    :cond_0
    return-object v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 244
    return-void
.end method

.method public final c()Landroid/content/Context;
    .locals 5

    .prologue
    .line 259
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->l:Landroid/view/ContextThemeWrapper;

    if-nez v0, :cond_0

    .line 260
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v3, "style"

    const-string v4, "HSActionBarThemedContext"

    invoke-static {v2, v3, v4}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->l:Landroid/view/ContextThemeWrapper;

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->l:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method

.method public final c(Landroid/view/MenuItem;)V
    .locals 4

    .prologue
    .line 505
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 506
    iget-object v1, p0, Lcom/helpshift/app/ActionBarHelperBase;->a:Landroid/app/Activity;

    const-string v2, "id"

    const-string v3, "hs__action_search"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 508
    if-ne v0, v1, :cond_0

    .line 509
    invoke-direct {p0}, Lcom/helpshift/app/ActionBarHelperBase;->h()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/helpshift/widget/SimpleSearchView;

    .line 510
    invoke-virtual {v0}, Lcom/helpshift/widget/SimpleSearchView;->clearFocus()V

    .line 512
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 269
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method protected final f()Z
    .locals 1

    .prologue
    .line 335
    iget-boolean v0, p0, Lcom/helpshift/app/ActionBarHelperBase;->b:Z

    return v0
.end method
