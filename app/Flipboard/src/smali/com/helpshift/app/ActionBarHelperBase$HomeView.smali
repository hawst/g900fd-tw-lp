.class Lcom/helpshift/app/ActionBarHelperBase$HomeView;
.super Landroid/widget/LinearLayout;
.source "ActionBarHelperBase.java"


# instance fields
.field a:Landroid/widget/ImageView;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 461
    iput-object p1, p0, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->b:Landroid/content/Context;

    .line 462
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 465
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 466
    iput-object p1, p0, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->b:Landroid/content/Context;

    .line 467
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 475
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 476
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->b:Landroid/content/Context;

    const-string v1, "id"

    const-string v2, "hs__actionbar_compat_home"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$HomeView;->a:Landroid/widget/ImageView;

    .line 477
    return-void
.end method
