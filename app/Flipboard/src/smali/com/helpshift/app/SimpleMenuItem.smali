.class public Lcom/helpshift/app/SimpleMenuItem;
.super Ljava/lang/Object;
.source "SimpleMenuItem.java"

# interfaces
.implements Landroid/view/MenuItem;


# instance fields
.field a:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemChangedListener;

.field private b:Lcom/helpshift/app/SimpleMenu;

.field private final c:I

.field private final d:I

.field private e:Ljava/lang/CharSequence;

.field private f:Ljava/lang/CharSequence;

.field private g:Landroid/graphics/drawable/Drawable;

.field private h:I

.field private i:Z

.field private j:Landroid/view/View;

.field private k:Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

.field private l:Z


# direct methods
.method public constructor <init>(Lcom/helpshift/app/SimpleMenu;IILjava/lang/CharSequence;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->j:Landroid/view/View;

    .line 20
    iput-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->k:Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;

    .line 22
    iput-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->a:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemChangedListener;

    .line 26
    iput-object p1, p0, Lcom/helpshift/app/SimpleMenuItem;->b:Lcom/helpshift/app/SimpleMenu;

    .line 27
    iput p2, p0, Lcom/helpshift/app/SimpleMenuItem;->c:I

    .line 28
    iput p3, p0, Lcom/helpshift/app/SimpleMenuItem;->d:I

    .line 29
    iput-object p4, p0, Lcom/helpshift/app/SimpleMenuItem;->e:Ljava/lang/CharSequence;

    .line 30
    return-void
.end method


# virtual methods
.method public collapseActionView()Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/helpshift/app/SimpleMenuItem;->l:Z

    .line 251
    const/4 v0, 0x1

    return v0
.end method

.method public expandActionView()Z
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 242
    iput-boolean v0, p0, Lcom/helpshift/app/SimpleMenuItem;->l:Z

    .line 244
    return v0
.end method

.method public getActionProvider()Landroid/view/ActionProvider;
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    return-object v0
.end method

.method public getActionView()Landroid/view/View;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->j:Landroid/view/View;

    return-object v0
.end method

.method public getAlphabeticShortcut()C
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public getGroupId()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->g:Landroid/graphics/drawable/Drawable;

    .line 99
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget v0, p0, Lcom/helpshift/app/SimpleMenuItem;->h:I

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->b:Lcom/helpshift/app/SimpleMenu;

    iget-object v0, v0, Lcom/helpshift/app/SimpleMenu;->a:Landroid/content/res/Resources;

    iget v1, p0, Lcom/helpshift/app/SimpleMenuItem;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/helpshift/app/SimpleMenuItem;->c:I

    return v0
.end method

.method public getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumericShortcut()C
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x0

    return v0
.end method

.method public getOrder()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/helpshift/app/SimpleMenuItem;->d:I

    return v0
.end method

.method public getSubMenu()Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 190
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitleCondensed()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public hasSubMenu()Z
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    return v0
.end method

.method public isActionViewExpanded()Z
    .locals 1

    .prologue
    .line 256
    iget-boolean v0, p0, Lcom/helpshift/app/SimpleMenuItem;->l:Z

    return v0
.end method

.method public isCheckable()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x0

    return v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/helpshift/app/SimpleMenuItem;->i:Z

    return v0
.end method

.method public setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 232
    return-object p0
.end method

.method public setActionView(I)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 222
    return-object p0
.end method

.method public setActionView(Landroid/view/View;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/helpshift/app/SimpleMenuItem;->j:Landroid/view/View;

    .line 216
    return-object p0
.end method

.method public setAlphabeticShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 129
    return-object p0
.end method

.method public setCheckable(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 139
    return-object p0
.end method

.method public setChecked(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 149
    return-object p0
.end method

.method public setEnabled(Z)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 175
    return-object p0
.end method

.method public setIcon(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->g:Landroid/graphics/drawable/Drawable;

    .line 85
    iput p1, p0, Lcom/helpshift/app/SimpleMenuItem;->h:I

    .line 86
    return-object p0
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/helpshift/app/SimpleMenuItem;->h:I

    .line 78
    iput-object p1, p0, Lcom/helpshift/app/SimpleMenuItem;->g:Landroid/graphics/drawable/Drawable;

    .line 79
    return-object p0
.end method

.method public setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 104
    return-object p0
.end method

.method public setNumericShortcut(C)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 119
    return-object p0
.end method

.method public setOnActionExpandListener(Landroid/view/MenuItem$OnActionExpandListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 262
    return-object p0
.end method

.method public setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 195
    return-object p0
.end method

.method public setShortcut(CC)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 114
    return-object p0
.end method

.method public setShowAsAction(I)V
    .locals 0

    .prologue
    .line 206
    return-void
.end method

.method public setShowAsActionFlags(I)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 210
    return-object p0
.end method

.method public setTitle(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->b:Lcom/helpshift/app/SimpleMenu;

    iget-object v0, v0, Lcom/helpshift/app/SimpleMenu;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->e:Ljava/lang/CharSequence;

    .line 56
    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/helpshift/app/SimpleMenuItem;->e:Ljava/lang/CharSequence;

    .line 50
    return-object p0
.end method

.method public setTitleCondensed(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/helpshift/app/SimpleMenuItem;->f:Ljava/lang/CharSequence;

    .line 67
    return-object p0
.end method

.method public setVisible(Z)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 159
    iput-boolean p1, p0, Lcom/helpshift/app/SimpleMenuItem;->i:Z

    .line 161
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->a:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemChangedListener;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/helpshift/app/SimpleMenuItem;->a:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemChangedListener;

    invoke-interface {v0, p1}, Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemChangedListener;->a(Z)V

    .line 165
    :cond_0
    return-object p0
.end method
