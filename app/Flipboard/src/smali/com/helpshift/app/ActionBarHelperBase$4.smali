.class Lcom/helpshift/app/ActionBarHelperBase$4;
.super Ljava/lang/Object;
.source "ActionBarHelperBase.java"

# interfaces
.implements Landroid/support/v4/view/MenuItemCompat$OnActionExpandListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Landroid/view/View;

.field final synthetic c:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;

.field final synthetic d:Lcom/helpshift/app/ActionBarHelperBase;


# direct methods
.method constructor <init>(Lcom/helpshift/app/ActionBarHelperBase;Landroid/view/View;Landroid/view/View;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->d:Lcom/helpshift/app/ActionBarHelperBase;

    iput-object p2, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->b:Landroid/view/View;

    iput-object p4, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->c:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 313
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 314
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->d:Lcom/helpshift/app/ActionBarHelperBase;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/helpshift/app/ActionBarHelperBase;->b:Z

    .line 318
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->c:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;

    invoke-interface {v0}, Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;->b()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 323
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 324
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 325
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 327
    :cond_0
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->d:Lcom/helpshift/app/ActionBarHelperBase;

    iput-boolean v1, v0, Lcom/helpshift/app/ActionBarHelperBase;->b:Z

    .line 328
    iget-object v0, p0, Lcom/helpshift/app/ActionBarHelperBase$4;->c:Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;

    invoke-interface {v0}, Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;->c()Z

    move-result v0

    return v0
.end method
