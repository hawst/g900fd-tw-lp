.class public abstract Lcom/helpshift/app/ActionBarHelper;
.super Ljava/lang/Object;
.source "ActionBarHelper.java"


# instance fields
.field protected a:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/helpshift/app/ActionBarHelper;->a:Landroid/app/Activity;

    .line 19
    return-void
.end method

.method public static a(Landroid/app/Activity;)Lcom/helpshift/app/ActionBarHelper;
    .locals 2

    .prologue
    .line 22
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 23
    new-instance v0, Lcom/helpshift/app/ActionBarHelperNative;

    invoke-direct {v0, p0}, Lcom/helpshift/app/ActionBarHelperNative;-><init>(Landroid/app/Activity;)V

    .line 25
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/helpshift/app/ActionBarHelperBase;

    invoke-direct {v0, p0}, Lcom/helpshift/app/ActionBarHelperBase;-><init>(Landroid/app/Activity;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MenuInflater;)Landroid/view/MenuInflater;
    .locals 0

    .prologue
    .line 34
    return-object p1
.end method

.method public a()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public abstract a(Landroid/view/MenuItem;)V
.end method

.method public abstract a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V
.end method

.method public abstract a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V
.end method

.method public a(Landroid/view/MenuItem;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b(Landroid/view/MenuItem;)Ljava/lang/String;
.end method

.method public abstract b()V
.end method

.method public abstract c()Landroid/content/Context;
.end method

.method public abstract c(Landroid/view/MenuItem;)V
.end method

.method public abstract d()V
.end method

.method public abstract e()V
.end method
