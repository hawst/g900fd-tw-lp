.class public Lcom/helpshift/HelpshiftDb;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "HelpshiftDb.java"


# static fields
.field private static a:Lcom/helpshift/HelpshiftDb;

.field private static b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const/4 v0, 0x0

    sput-object v0, Lcom/helpshift/HelpshiftDb;->a:Lcom/helpshift/HelpshiftDb;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 18
    const-string v0, "__hs__db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 19
    invoke-virtual {p0}, Lcom/helpshift/HelpshiftDb;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    .line 20
    return-void
.end method

.method protected static a(Landroid/content/Context;)Lcom/helpshift/HelpshiftDb;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/helpshift/HelpshiftDb;->a:Lcom/helpshift/HelpshiftDb;

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Lcom/helpshift/HelpshiftDb;

    invoke-direct {v0, p0}, Lcom/helpshift/HelpshiftDb;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/helpshift/HelpshiftDb;->a:Lcom/helpshift/HelpshiftDb;

    .line 26
    :cond_0
    sget-object v0, Lcom/helpshift/HelpshiftDb;->a:Lcom/helpshift/HelpshiftDb;

    return-object v0
.end method

.method public static declared-synchronized b()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 64
    const-class v1, Lcom/helpshift/HelpshiftDb;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/helpshift/HelpshiftDb;->a:Lcom/helpshift/HelpshiftDb;

    if-eqz v0, :cond_0

    .line 65
    sget-object v0, Lcom/helpshift/HelpshiftDb;->a:Lcom/helpshift/HelpshiftDb;

    invoke-virtual {v0}, Lcom/helpshift/HelpshiftDb;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 67
    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/helpshift/HelpshiftDb;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Lcom/helpshift/HSFaqDb;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 59
    sget-object v0, Lcom/helpshift/HelpshiftDb;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0}, Lcom/helpshift/HSSectionDb;->b(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 60
    sget-object v0, Lcom/helpshift/HelpshiftDb;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Lcom/helpshift/HelpshiftDb;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 61
    return-void
.end method

.method public getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/helpshift/HelpshiftDb;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 32
    invoke-super {p0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    sput-object v0, Lcom/helpshift/HelpshiftDb;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 34
    :cond_0
    sget-object v0, Lcom/helpshift/HelpshiftDb;->b:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p1}, Lcom/helpshift/HSSectionDb;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 40
    invoke-static {p1}, Lcom/helpshift/HSFaqDb;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 41
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 45
    const-class v0, Lcom/helpshift/HelpshiftDb;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading database from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", which will destroy all old data"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/Log;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-static {p1, p2, p3}, Lcom/helpshift/HSSectionDb;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 49
    invoke-static {p1, p2, p3}, Lcom/helpshift/HSFaqDb;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 50
    invoke-virtual {p0, p1}, Lcom/helpshift/HelpshiftDb;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 51
    return-void
.end method
