.class public final Lcom/helpshift/HSConversation;
.super Lcom/helpshift/HSActivity;
.source "HSConversation.java"


# instance fields
.field private o:Lcom/helpshift/HSApiData;

.field private p:Lcom/helpshift/HSStorage;

.field private q:Landroid/os/Bundle;

.field private r:I

.field private s:Landroid/support/v4/app/FragmentTransaction;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/helpshift/HSActivity;-><init>()V

    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 91
    iget-object v0, p0, Lcom/helpshift/HSConversation;->s:Landroid/support/v4/app/FragmentTransaction;

    iget v1, p0, Lcom/helpshift/HSConversation;->r:I

    const-class v2, Lcom/helpshift/HSAddIssueFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSConversation;->q:Landroid/os/Bundle;

    invoke-static {p0, v2, v3}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 95
    iget-object v0, p0, Lcom/helpshift/HSConversation;->s:Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 96
    invoke-super {p0}, Lcom/helpshift/HSActivity;->e()V

    .line 97
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lcom/helpshift/HSConversation;->s:Landroid/support/v4/app/FragmentTransaction;

    iget v1, p0, Lcom/helpshift/HSConversation;->r:I

    const-class v2, Lcom/helpshift/HSMessagesFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSConversation;->q:Landroid/os/Bundle;

    invoke-static {p0, v2, v3}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    .line 104
    iget-object v0, p0, Lcom/helpshift/HSConversation;->s:Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 105
    return-void
.end method


# virtual methods
.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 151
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 152
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/helpshift/HSConversation;->setResult(ILandroid/content/Intent;)V

    .line 153
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onBackPressed()V

    .line 154
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSConversation;->o:Lcom/helpshift/HSApiData;

    .line 33
    iget-object v0, p0, Lcom/helpshift/HSConversation;->o:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    .line 35
    invoke-virtual {p0}, Lcom/helpshift/HSConversation;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 36
    iget-object v0, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v1, "activeConversation"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 39
    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->d()V

    .line 40
    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->b()V

    .line 42
    const-string v0, "layout"

    const-string v1, "hs__conversation"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSConversation;->setContentView(I)V

    .line 44
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->a:Ljava/util/Map;

    const-string v1, "hl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    const-string v0, "id"

    const-string v1, "hs__newConversationFooter"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 48
    invoke-virtual {p0, v0}, Lcom/helpshift/HSConversation;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 50
    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 51
    sget-object v1, Lcom/helpshift/res/drawable/HSImages;->a:Ljava/util/Map;

    const-string v4, "newHSLogo"

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/helpshift/res/drawable/HSDraw;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 53
    const v1, 0x106000c

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 54
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 57
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/helpshift/HSConversation;->q:Landroid/os/Bundle;

    .line 58
    const-string v0, "chatLaunchSource"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v1, "decomp"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/helpshift/HSAnalytics;->a:Z

    .line 62
    const-string v1, "id"

    const-string v3, "hs__fragment_holder"

    invoke-static {p0, v1, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/helpshift/HSConversation;->r:I

    .line 63
    iget-object v1, p0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/HSConversation;->s:Landroid/support/v4/app/FragmentTransaction;

    .line 65
    const-string v1, "newConversation"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 66
    invoke-direct {p0}, Lcom/helpshift/HSConversation;->f()V

    .line 73
    :goto_0
    return-void

    .line 67
    :cond_1
    const-string v1, "push"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "inapp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    :cond_2
    invoke-direct {p0}, Lcom/helpshift/HSConversation;->g()V

    goto :goto_0

    .line 71
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v1, "activeConversation"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v2, "archivedConversationId"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v0, p0, Lcom/helpshift/HSConversation;->q:Landroid/os/Bundle;

    const-string v2, "issueId"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/helpshift/HSConversation;->g()V

    goto :goto_0

    :cond_4
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/helpshift/HSConversation;->q:Landroid/os/Bundle;

    const-string v2, "issueId"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/helpshift/HSConversation;->g()V

    goto :goto_0

    :cond_5
    invoke-direct {p0}, Lcom/helpshift/HSConversation;->f()V

    goto :goto_0
.end method

.method public final bridge synthetic onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 20
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "isConversationShowing"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 159
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onDestroy()V

    .line 160
    return-void
.end method

.method protected final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 126
    invoke-virtual {p0}, Lcom/helpshift/HSConversation;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 127
    iget-object v1, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v2, "activeConversation"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    iget-object v2, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v3, "archivedConversationId"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 130
    const-string v3, "newConversation"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 133
    :cond_0
    invoke-static {}, Lcom/helpshift/util/AttachmentUtil;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 135
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/helpshift/HSConversation;->setResult(ILandroid/content/Intent;)V

    .line 136
    invoke-virtual {p0}, Lcom/helpshift/HSConversation;->finish()V

    .line 139
    :cond_1
    invoke-static {p0}, Lcom/helpshift/util/HSActivityUtil;->c(Landroid/app/Activity;)V

    .line 140
    if-eqz v0, :cond_2

    .line 141
    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v4, :cond_2

    invoke-virtual {p0}, Lcom/helpshift/HSConversation;->isFinishing()Z

    move-result v0

    if-ne v0, v4, :cond_2

    .line 143
    invoke-static {}, Lcom/helpshift/util/HSActivityUtil;->a()V

    .line 146
    :cond_2
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onPause()V

    .line 147
    return-void
.end method

.method protected final onResume()V
    .locals 4

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/helpshift/HSConversation;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v2, "activeConversation"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const-string v3, "archivedConversationId"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 113
    const-string v3, "newConversation"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    :cond_0
    invoke-static {p0}, Lcom/helpshift/util/HSActivityUtil;->c(Landroid/app/Activity;)V

    .line 120
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSConversation;->p:Lcom/helpshift/HSStorage;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "isConversationShowing"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 121
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onResume()V

    .line 122
    return-void

    .line 118
    :cond_1
    invoke-static {p0}, Lcom/helpshift/util/HSActivityUtil;->b(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public final bridge synthetic onStart()V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStart()V

    return-void
.end method

.method public final bridge synthetic onStop()V
    .locals 0

    .prologue
    .line 20
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStop()V

    return-void
.end method
