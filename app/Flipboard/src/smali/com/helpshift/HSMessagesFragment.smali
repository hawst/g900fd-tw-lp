.class public Lcom/helpshift/HSMessagesFragment;
.super Landroid/support/v4/app/Fragment;
.source "HSMessagesFragment.java"

# interfaces
.implements Lcom/helpshift/widget/CSATView$CSATListener;


# instance fields
.field private A:Landroid/widget/ImageButton;

.field private B:Landroid/widget/Button;

.field private C:Z

.field private D:Z

.field private E:Z

.field private F:Landroid/view/MenuItem;

.field private G:Landroid/view/ViewStub;

.field private H:Landroid/widget/TextView;

.field private I:Lcom/helpshift/widget/CSATView;

.field private J:Z

.field private K:Landroid/os/Handler;

.field private L:Landroid/os/Handler;

.field private M:Landroid/os/Handler;

.field private N:Landroid/os/Handler;

.field private final a:Landroid/content/BroadcastReceiver;

.field private final b:Landroid/content/BroadcastReceiver;

.field private c:Lcom/helpshift/HSActivity;

.field private d:Landroid/os/Bundle;

.field private e:Ljava/lang/String;

.field private f:Lcom/helpshift/customadapters/MessagesAdapter;

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/viewstructs/HSMsg;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/ListView;

.field private i:Lcom/helpshift/HSStorage;

.field private j:Lcom/helpshift/HSApiClient;

.field private k:Lcom/helpshift/HSApiData;

.field private l:Ljava/lang/String;

.field private m:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final o:I

.field private p:Ljava/lang/Thread;

.field private q:Landroid/os/Handler;

.field private r:Ljava/lang/Boolean;

.field private s:Ljava/lang/Boolean;

.field private t:Ljava/lang/Boolean;

.field private u:Z

.field private v:Landroid/widget/EditText;

.field private w:Landroid/widget/LinearLayout;

.field private x:Landroid/widget/LinearLayout;

.field private y:Landroid/widget/RelativeLayout;

.field private z:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 48
    new-instance v0, Lcom/helpshift/HSMessagesFragment$1;

    invoke-direct {v0, p0}, Lcom/helpshift/HSMessagesFragment$1;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->a:Landroid/content/BroadcastReceiver;

    .line 55
    new-instance v0, Lcom/helpshift/HSMessagesFragment$2;

    invoke-direct {v0, p0}, Lcom/helpshift/HSMessagesFragment$2;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->b:Landroid/content/BroadcastReceiver;

    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->m:Ljava/util/HashSet;

    .line 75
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->n:Ljava/util/HashSet;

    .line 76
    const/4 v0, 0x3

    iput v0, p0, Lcom/helpshift/HSMessagesFragment;->o:I

    .line 82
    iput-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->u:Z

    .line 90
    iput-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->C:Z

    .line 91
    iput-boolean v2, p0, Lcom/helpshift/HSMessagesFragment;->D:Z

    .line 92
    iput-boolean v2, p0, Lcom/helpshift/HSMessagesFragment;->E:Z

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->I:Lcom/helpshift/widget/CSATView;

    .line 97
    iput-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->J:Z

    .line 107
    new-instance v0, Lcom/helpshift/HSMessagesFragment$3;

    invoke-direct {v0, p0}, Lcom/helpshift/HSMessagesFragment$3;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->K:Landroid/os/Handler;

    .line 119
    new-instance v0, Lcom/helpshift/HSMessagesFragment$4;

    invoke-direct {v0, p0}, Lcom/helpshift/HSMessagesFragment$4;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->L:Landroid/os/Handler;

    .line 147
    new-instance v0, Lcom/helpshift/HSMessagesFragment$5;

    invoke-direct {v0, p0}, Lcom/helpshift/HSMessagesFragment$5;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->M:Landroid/os/Handler;

    .line 153
    new-instance v0, Lcom/helpshift/HSMessagesFragment$6;

    invoke-direct {v0, p0}, Lcom/helpshift/HSMessagesFragment$6;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->N:Landroid/os/Handler;

    .line 1155
    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .prologue
    .line 45
    iput-object p1, p0, Lcom/helpshift/HSMessagesFragment;->q:Landroid/os/Handler;

    return-object p1
.end method

.method private a(Landroid/content/res/Configuration;)V
    .locals 7

    .prologue
    const/16 v4, 0x1c

    const/4 v3, 0x6

    const/4 v5, -0x2

    const/4 v2, 0x0

    .line 557
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->B:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 562
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 563
    invoke-direct {p0, v4}, Lcom/helpshift/HSMessagesFragment;->e(I)I

    move-result v3

    .line 564
    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->e(I)I

    move-result v1

    .line 565
    invoke-direct {p0, v4}, Lcom/helpshift/HSMessagesFragment;->e(I)I

    move-result v0

    move v6, v0

    move v0, v1

    move v1, v6

    .line 572
    :goto_0
    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 574
    invoke-virtual {v4, v2, v3, v2, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 575
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->B:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 577
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->I:Lcom/helpshift/widget/CSATView;

    if-eqz v0, :cond_0

    .line 578
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->I:Lcom/helpshift/widget/CSATView;

    iget-object v0, v3, Lcom/helpshift/widget/CSATView;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v2, v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, v3, Lcom/helpshift/widget/CSATView;->c:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 581
    :cond_0
    return-void

    .line 567
    :cond_1
    invoke-direct {p0, v3}, Lcom/helpshift/HSMessagesFragment;->e(I)I

    move-result v1

    .line 568
    invoke-direct {p0, v3}, Lcom/helpshift/HSMessagesFragment;->e(I)I

    move-result v0

    move v3, v1

    move v1, v2

    .line 569
    goto :goto_0
.end method

.method private a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "screenShotDraft"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1239
    const-string v0, "ar"

    invoke-virtual {p5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1240
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    sget-object v1, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->a:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {v0, p3, v1}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;)Ljava/lang/Boolean;

    .line 1242
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1248
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 285
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 286
    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->e()V

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct/range {p0 .. p6}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 45
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    const-string v1, "response"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Lorg/json/JSONArray;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->e()V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Ljava/lang/Boolean;)V
    .locals 7

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->f(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "id"

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "y"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    const-string v5, "ca"

    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->M:Landroid/os/Handler;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->N:Landroid/os/Handler;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v4, ""

    const-string v6, ""

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->b:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;)Ljava/lang/Boolean;

    :goto_1
    return-void

    :cond_0
    const-string v1, "n"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    const-string v5, "ncr"
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 45
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSMessagesFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private a(Lorg/json/JSONArray;)V
    .locals 16

    .prologue
    .line 295
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 296
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->m:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 297
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->n:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    .line 298
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 300
    const/4 v1, 0x0

    move v12, v1

    :goto_0
    if-ge v12, v13, :cond_8

    .line 302
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v14

    .line 303
    const-string v1, "id"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 304
    const-string v1, "type"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 305
    const-string v1, "origin"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 306
    const-string v1, "body"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 307
    const-string v1, "created_at"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 308
    const-string v1, "state"

    const/4 v7, 0x0

    invoke-virtual {v14, v1, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    .line 309
    const-string v1, "inProgress"

    const/4 v7, 0x0

    invoke-virtual {v14, v1, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 311
    const-string v1, "mobile"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "ncr"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/helpshift/HSMessagesFragment;->D:Z

    if-nez v1, :cond_0

    add-int/lit8 v1, v13, -0x1

    if-eq v12, v1, :cond_1

    .line 314
    :cond_0
    const-string v8, ""

    .line 318
    const-string v1, "rsc"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 319
    const-string v1, "screenshot"

    const-string v7, ""

    invoke-virtual {v14, v1, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 320
    const-string v1, "localRscMessage_"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->n:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 322
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    :cond_1
    :goto_1
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    goto/16 :goto_0

    .line 327
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 328
    const-string v7, "meta"

    invoke-virtual {v14, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 330
    if-eqz v7, :cond_3

    .line 331
    const-string v11, "response"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 332
    if-eqz v7, :cond_3

    .line 333
    const-string v1, "state"

    invoke-virtual {v7, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 337
    :cond_3
    const-string v11, ""

    .line 338
    const-string v7, "author"

    invoke-virtual {v14, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 339
    if-eqz v7, :cond_4

    .line 340
    const-string v11, "name"

    invoke-virtual {v7, v11}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 343
    :cond_4
    const-string v7, "invisible"

    invoke-virtual {v14, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_5
    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 345
    const-string v1, "mobile"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "ca"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->m:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 347
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->m:Ljava/util/HashSet;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 348
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    new-instance v1, Lcom/helpshift/viewstructs/HSMsg;

    invoke-direct/range {v1 .. v11}, Lcom/helpshift/viewstructs/HSMsg;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;ILjava/lang/Boolean;Ljava/lang/String;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    const-string v1, "sc"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 352
    const-string v1, "meta"

    invoke-virtual {v14, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 354
    if-eqz v1, :cond_1

    .line 355
    const-string v3, "attachments"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 357
    const-string v4, "refers"

    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 359
    const-string v4, "localRscMessage_"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 360
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/helpshift/HSMessagesFragment;->m:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 361
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/helpshift/HSMessagesFragment;->b(Ljava/lang/String;)V

    .line 362
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v4, v5, v1}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    :cond_7
    :goto_3
    if-eqz v3, :cond_c

    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_c

    if-eqz v1, :cond_c

    .line 370
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 371
    if-eqz v2, :cond_1

    .line 372
    const-string v3, "url"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 373
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 374
    const-string v4, "url"

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    const-string v2, "messageId"

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    const-string v1, "attachId"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 377
    const-string v1, "position"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 378
    new-instance v1, Lcom/helpshift/HSMessagesFragment$DownloadImagesTask;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v2}, Lcom/helpshift/HSMessagesFragment$DownloadImagesTask;-><init>(Lcom/helpshift/HSMessagesFragment;B)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/HashMap;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/helpshift/HSMessagesFragment$DownloadImagesTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v1

    .line 388
    :cond_8
    invoke-virtual/range {p1 .. p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_9

    .line 392
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v1}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 394
    :cond_9
    return-void

    .line 343
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 364
    :cond_b
    :try_start_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/helpshift/HSMessagesFragment;->n:Ljava/util/HashSet;

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 380
    :cond_c
    if-nez v3, :cond_1

    .line 381
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/helpshift/HSMessagesFragment;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/helpshift/HSMessagesFragment;Z)Z
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/helpshift/HSMessagesFragment;->C:Z

    return p1
.end method

.method static synthetic b(Lcom/helpshift/HSMessagesFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->c()V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 398
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 399
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 400
    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 404
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->m:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 405
    return-void
.end method

.method static synthetic c(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/customadapters/MessagesAdapter;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 165
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    invoke-virtual {v1}, Lcom/helpshift/HSStorage;->e()Ljava/util/List;

    move-result-object v1

    .line 166
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 167
    const-string v3, "status"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 168
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v4, "replyText"

    invoke-virtual {v3, v4}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    .line 170
    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "1"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 172
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->d()V

    .line 173
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/helpshift/HSMessagesFragment;->C:Z

    .line 187
    :cond_1
    :goto_0
    return-void

    .line 174
    :cond_2
    const-string v4, "2"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 176
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->C:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    :cond_3
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    invoke-direct {p0, v1}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/view/View;)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_1
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->w:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->x:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_4
    sget v0, Lcom/helpshift/R$string;->hs__confirmation_footer_msg:I

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->d(I)V

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    goto :goto_0

    .line 176
    :cond_5
    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1

    .line 177
    :cond_6
    iget-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->C:Z

    if-nez v1, :cond_7

    if-nez v3, :cond_7

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v2, "screenShotDraft"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->e(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 180
    :cond_7
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->d()V

    goto/16 :goto_0

    .line 182
    :cond_8
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->D:Z

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    invoke-direct {p0, v1}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/view/View;)V

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->w:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->x:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_9
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/helpshift/HSApiData;->f(Ljava/lang/String;)Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    move-result-object v1

    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->b:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    if-eq v1, v2, :cond_a

    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->c:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    if-ne v1, v2, :cond_c

    :cond_a
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->G:Landroid/view/ViewStub;

    if-eqz v1, :cond_b

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->G:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/helpshift/widget/CSATView;

    invoke-virtual {v0, p0}, Lcom/helpshift/widget/CSATView;->setCSATListener(Lcom/helpshift/widget/CSATView$CSATListener;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->G:Landroid/view/ViewStub;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    sget-object v3, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->c:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;)Ljava/lang/Boolean;

    :cond_b
    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->I:Lcom/helpshift/widget/CSATView;

    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/content/res/Configuration;)V

    sget v0, Lcom/helpshift/R$string;->hs__confirmation_footer_msg:I

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->d(I)V

    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/helpshift/HSMessagesFragment;->J:Z

    goto/16 :goto_0

    :cond_c
    sget v0, Lcom/helpshift/R$string;->hs__conversation_end_msg:I

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->d(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method static synthetic d(Lcom/helpshift/HSMessagesFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x1

    .line 267
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    iput-boolean v2, v0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    .line 268
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->w:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->x:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 271
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 274
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->d(I)V

    .line 275
    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->H:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 260
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->H:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 262
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->H:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 264
    :cond_0
    return-void
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 584
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 585
    int-to-float v1, p1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method static synthetic e(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->q:Landroid/os/Handler;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSApiData;->e(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 290
    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->a(Lorg/json/JSONArray;)V

    .line 291
    return-void
.end method

.method static synthetic f(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSActivity;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    return-object v0
.end method

.method static synthetic g(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->K:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic h(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->L:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic i(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSApiData;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    return-object v0
.end method

.method static synthetic k(Lcom/helpshift/HSMessagesFragment;)Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic l(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSStorage;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    return-object v0
.end method

.method static synthetic m(Lcom/helpshift/HSMessagesFragment;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->d()V

    return-void
.end method

.method static synthetic n(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic o(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->M:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic p(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->N:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic q(Lcom/helpshift/HSMessagesFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r(Lcom/helpshift/HSMessagesFragment;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1095
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1099
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-thumbnail"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1100
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v4}, Lcom/helpshift/HSActivity;->getFilesDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1101
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 1102
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1103
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    invoke-virtual {v3, v2}, Lcom/helpshift/HSApiData;->c(Ljava/lang/String;)V

    .line 1104
    invoke-virtual {v1}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1105
    :try_start_1
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v5}, Lcom/helpshift/HSActivity;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v0

    .line 1108
    const/16 v2, 0x400

    :try_start_2
    new-array v2, v2, [B

    .line 1109
    :goto_0
    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_3

    .line 1110
    const/4 v5, 0x0

    invoke-virtual {v0, v2, v5, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1115
    :catchall_0
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    :goto_1
    if-eqz v1, :cond_0

    .line 1116
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1117
    :cond_0
    if-eqz v2, :cond_1

    .line 1118
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0

    :cond_2
    move-object v1, v0

    .line 1115
    :cond_3
    if-eqz v0, :cond_4

    .line 1116
    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 1117
    :cond_4
    if-eqz v1, :cond_5

    .line 1118
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_5
    return-object v4

    .line 1115
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_1

    :catchall_2
    move-exception v2

    move-object v6, v2

    move-object v2, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_1
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 250
    sget v0, Lcom/helpshift/R$string;->hs__conversation_end_msg:I

    invoke-direct {p0, v0}, Lcom/helpshift/HSMessagesFragment;->d(I)V

    .line 251
    return-void
.end method

.method public final a(I)V
    .locals 5

    .prologue
    .line 983
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 986
    :try_start_0
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    const-string v2, "localRscMessage_"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 987
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 994
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v0}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 998
    return-void

    .line 990
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v1, v2, v3, v4}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    const-string v1, ""

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 6

    .prologue
    .line 255
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/HSApiData;->a(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 256
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1192
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, p1}, Lcom/helpshift/HSStorage;->i(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1193
    if-nez v0, :cond_0

    .line 1228
    :goto_0
    return-void

    .line 1197
    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/helpshift/util/HSJSONUtils;->a(Lorg/json/JSONObject;)[Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lorg/json/JSONObject;-><init>(Lorg/json/JSONObject;[Ljava/lang/String;)V

    .line 1200
    const-string v2, "state"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1202
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/helpshift/HSApiData;->e(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 1203
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 1204
    invoke-direct {p0, v2}, Lcom/helpshift/HSMessagesFragment;->a(Lorg/json/JSONArray;)V

    .line 1206
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v2, v2, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v2, v1}, Lcom/helpshift/HSStorage;->b(Lorg/json/JSONObject;)V

    .line 1208
    new-instance v1, Lcom/helpshift/HSMessagesFragment$20;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/HSMessagesFragment$20;-><init>(Lcom/helpshift/HSMessagesFragment;Ljava/lang/String;)V

    .line 1220
    const-string v2, "issue_id"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v2, "body"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v2, "type"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v2, "refers"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v2, "state"

    const/4 v7, 0x0

    invoke-virtual {v0, v2, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const-string v8, "screenShotDraft"

    invoke-virtual {v0, v8, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v0, "ar"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    sget-object v2, Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;->a:Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;

    invoke-virtual {v0, v3, v2}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Lcom/helpshift/HSApiData$HS_ISSUE_CSAT_STATE;)Ljava/lang/Boolean;

    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    move-object v2, v1

    invoke-virtual/range {v0 .. v7}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1227
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 881
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 882
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 883
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 885
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 890
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v0}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 892
    new-instance v1, Lcom/helpshift/HSMessagesFragment$16;

    invoke-direct {v1, p0, p2}, Lcom/helpshift/HSMessagesFragment$16;-><init>(Lcom/helpshift/HSMessagesFragment;I)V

    .line 923
    new-instance v2, Lcom/helpshift/HSMessagesFragment$17;

    invoke-direct {v2, p0, p2}, Lcom/helpshift/HSMessagesFragment$17;-><init>(Lcom/helpshift/HSMessagesFragment;I)V

    .line 941
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "ar"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 943
    return-void

    .line 886
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Boolean;I)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 812
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 813
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 814
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 816
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 821
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v0}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 823
    new-instance v1, Lcom/helpshift/HSMessagesFragment$14;

    invoke-direct {v1, p0, p3}, Lcom/helpshift/HSMessagesFragment$14;-><init>(Lcom/helpshift/HSMessagesFragment;I)V

    .line 840
    new-instance v2, Lcom/helpshift/HSMessagesFragment$15;

    invoke-direct {v2, p0, p3}, Lcom/helpshift/HSMessagesFragment$15;-><init>(Lcom/helpshift/HSMessagesFragment;I)V

    .line 859
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 860
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "ca"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 868
    :goto_1
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 869
    const-string v1, "id"

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 870
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 871
    const-string v1, "y"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 877
    :goto_2
    return-void

    .line 817
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_0

    .line 863
    :cond_0
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v4, ""

    const-string v5, "ncr"

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 873
    :cond_1
    :try_start_2
    const-string v1, "n"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 876
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 428
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->q:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 429
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->q:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 430
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->p:Ljava/lang/Thread;

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v1, "activeConversation"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 436
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v2, "archivedConversationId"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 437
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 472
    :goto_0
    return-void

    .line 441
    :cond_1
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/helpshift/HSMessagesFragment$7;

    invoke-direct {v1, p0}, Lcom/helpshift/HSMessagesFragment$7;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->p:Ljava/lang/Thread;

    .line 471
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->p:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1001
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "screenShotDraft"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1002
    iput-boolean v3, p0, Lcom/helpshift/HSMessagesFragment;->u:Z

    .line 1003
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 1005
    invoke-virtual {p0, v0, p1}, Lcom/helpshift/HSMessagesFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1006
    return-void
.end method

.method public final c(I)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    .line 1009
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1010
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/helpshift/viewstructs/HSMsg;

    .line 1011
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v8, Lcom/helpshift/viewstructs/HSMsg;->i:Ljava/lang/Boolean;

    .line 1012
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v8, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    .line 1014
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v1, v8, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1019
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v0}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 1021
    new-instance v1, Lcom/helpshift/HSMessagesFragment$18;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/HSMessagesFragment$18;-><init>(Lcom/helpshift/HSMessagesFragment;I)V

    .line 1069
    new-instance v2, Lcom/helpshift/HSMessagesFragment$19;

    invoke-direct {v2, p0, p1}, Lcom/helpshift/HSMessagesFragment$19;-><init>(Lcom/helpshift/HSMessagesFragment;I)V

    .line 1088
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->j:Lcom/helpshift/HSApiClient;

    iget-object v4, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v5, ""

    const-string v6, "sc"

    iget-object v7, v8, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v8, v8, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    invoke-virtual/range {v0 .. v8}, Lcom/helpshift/HSApiClient;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    return-void

    .line 1015
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/16 v4, 0x7fbc

    .line 948
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 950
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    .line 951
    if-eq p1, v4, :cond_1

    .line 952
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/helpshift/util/AttachmentUtil;->a(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 953
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 954
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-class v3, Lcom/helpshift/ScreenshotPreviewActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 955
    const-string v2, "SCREENSHOT"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 956
    const-string v0, "screenshot_position"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 957
    invoke-virtual {p0, v1, v4}, Lcom/helpshift/HSMessagesFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 980
    :cond_0
    :goto_0
    return-void

    .line 960
    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "SCREENSHOT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 961
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "screenshot_position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 962
    const/4 v1, 0x0

    .line 964
    if-nez v0, :cond_2

    .line 965
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-static {v0, v3, v2}, Lcom/helpshift/util/AttachmentUtil;->a(Lcom/helpshift/HSStorage;Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/viewstructs/HSMsg;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 966
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-object v0, v1

    .line 972
    :goto_1
    :try_start_2
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v3, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v3, v4, v2}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 976
    :goto_2
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v1}, Lcom/helpshift/customadapters/MessagesAdapter;->notifyDataSetChanged()V

    .line 977
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSMessagesFragment;->c(I)V

    goto :goto_0

    .line 968
    :cond_2
    :try_start_3
    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 969
    :try_start_4
    iput-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 973
    :catch_0
    move-exception v1

    :goto_3
    invoke-virtual {v1}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    goto :goto_3
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 552
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 553
    invoke-direct {p0, p1}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/content/res/Configuration;)V

    .line 554
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1124
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const-string v1, "Copy"

    invoke-interface {p1, v2, v0, v2, v1}, Landroid/view/ContextMenu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1126
    check-cast p2, Landroid/widget/TextView;

    .line 1127
    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1129
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-lt v0, v2, :cond_0

    .line 1130
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 1131
    const-string v2, "Copy Text"

    invoke-static {v2, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 1132
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 1137
    :goto_0
    return-void

    .line 1134
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/text/ClipboardManager;

    .line 1135
    invoke-virtual {v0, v1}, Landroid/text/ClipboardManager;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 2

    .prologue
    .line 1141
    const-string v0, "menu"

    const-string v1, "hs__messages_menu"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1144
    const-string v0, "id"

    const-string v1, "hs__attach_screenshot"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    .line 1145
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 1147
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1149
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1153
    :goto_0
    return-void

    .line 1151
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 526
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSActivity;

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    .line 528
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v0, v2}, Lcom/helpshift/HSActivity;->c(Z)V

    .line 529
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    .line 530
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 532
    new-instance v0, Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-direct {v0, v1}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    .line 533
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    .line 534
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->b:Lcom/helpshift/HSApiClient;

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->j:Lcom/helpshift/HSApiClient;

    .line 536
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    const-string v1, "newIssue"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->r:Ljava/lang/Boolean;

    .line 537
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    const-string v1, "decomp"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->s:Ljava/lang/Boolean;

    .line 538
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    const-string v1, "chatLaunchSource"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->l:Ljava/lang/String;

    .line 539
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    const-string v1, "showConvOnReportIssue"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->t:Ljava/lang/Boolean;

    .line 541
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/helpshift/HSMessagesFragment;->setHasOptionsMenu(Z)V

    .line 543
    sget v0, Lcom/helpshift/R$layout;->hs__messages_list_footer:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->H:Landroid/widget/TextView;

    .line 544
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->H:Landroid/widget/TextView;

    const v1, 0x3f333333    # 0.7f

    invoke-static {v0, v1}, Lcom/helpshift/util/HSColor;->a(Landroid/widget/TextView;F)V

    .line 546
    const-string v0, "layout"

    const-string v1, "hs__messages_fragment"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 775
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 776
    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 777
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 783
    :goto_0
    return v0

    .line 779
    :cond_0
    const-string v2, "id"

    const-string v3, "hs__attach_screenshot"

    invoke-static {p0, v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 780
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/helpshift/HSMessagesFragment;->b(I)V

    goto :goto_0

    .line 783
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 409
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 410
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->q:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->q:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 414
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 418
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "foreground"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 419
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v1, ""

    const-string v2, "foregroundIssue"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    invoke-static {}, Lcom/helpshift/HSApiData;->k()V

    .line 421
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 422
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 423
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "replyText"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 424
    return-void

    .line 415
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 476
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 477
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 479
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 481
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 482
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 483
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 485
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 486
    const-string v1, "com.helpshift.failedMessageRequest"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 487
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 489
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->b()V

    .line 492
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->g(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "foreground"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 498
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    const-string v2, "foregroundIssue"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->k:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    const-string v1, "replyText"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 504
    iget-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->J:Z

    if-nez v1, :cond_0

    .line 505
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 507
    :cond_0
    iget-boolean v1, p0, Lcom/helpshift/HSMessagesFragment;->E:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    iput-boolean v3, p0, Lcom/helpshift/HSMessagesFragment;->C:Z

    .line 511
    :cond_1
    iput-boolean v3, p0, Lcom/helpshift/HSMessagesFragment;->E:Z

    .line 513
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_2

    .line 514
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    .line 516
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 521
    :cond_2
    :goto_1
    return-void

    .line 493
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_0

    .line 518
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->F:Landroid/view/MenuItem;

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 789
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 790
    iget-boolean v0, p0, Lcom/helpshift/HSMessagesFragment;->u:Z

    if-nez v0, :cond_0

    .line 791
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-static {v0}, Lcom/helpshift/HSAnalytics;->a(Landroid/app/Activity;)V

    .line 793
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 794
    const-string v1, "id"

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 795
    const-string v1, "c"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 797
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/helpshift/HSMessagesFragment;->u:Z

    .line 801
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 805
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 806
    iget-boolean v0, p0, Lcom/helpshift/HSMessagesFragment;->u:Z

    if-nez v0, :cond_0

    .line 807
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-static {}, Lcom/helpshift/HSAnalytics;->a()V

    .line 809
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 590
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 592
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__messagesList"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    .line 595
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__messageText"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    .line 598
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "hs__sendMessageBtn"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 601
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__confirmation"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->w:Landroid/widget/LinearLayout;

    .line 604
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__new_conversation"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->x:Landroid/widget/LinearLayout;

    .line 607
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "relativeLayout1"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->y:Landroid/widget/RelativeLayout;

    .line 610
    const v1, 0x1020019

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->z:Landroid/widget/ImageButton;

    .line 611
    const v1, 0x102001a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->A:Landroid/widget/ImageButton;

    .line 612
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__new_conversation_btn"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->B:Landroid/widget/Button;

    .line 615
    invoke-virtual {p0}, Lcom/helpshift/HSMessagesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/helpshift/HSMessagesFragment;->a(Landroid/content/res/Configuration;)V

    .line 616
    sget v1, Lcom/helpshift/R$id;->csat_view_stub:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->G:Landroid/view/ViewStub;

    .line 618
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->B:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v1, v2}, Lcom/helpshift/util/HSIcons;->b(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 620
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->z:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;)V

    .line 621
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->A:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->b(Landroid/graphics/drawable/Drawable;)V

    .line 623
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->z:Landroid/widget/ImageButton;

    new-instance v2, Lcom/helpshift/HSMessagesFragment$8;

    invoke-direct {v2, p0}, Lcom/helpshift/HSMessagesFragment$8;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 634
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->A:Landroid/widget/ImageButton;

    new-instance v2, Lcom/helpshift/HSMessagesFragment$9;

    invoke-direct {v2, p0}, Lcom/helpshift/HSMessagesFragment$9;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 647
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->B:Landroid/widget/Button;

    new-instance v2, Lcom/helpshift/HSMessagesFragment$10;

    invoke-direct {v2, p0}, Lcom/helpshift/HSMessagesFragment$10;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 660
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 661
    new-instance v1, Lcom/helpshift/customadapters/MessagesAdapter;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->g:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/helpshift/customadapters/MessagesAdapter;-><init>(Landroid/support/v4/app/Fragment;Ljava/util/List;)V

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    .line 663
    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 667
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 668
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 669
    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 672
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->d:Landroid/os/Bundle;

    const-string v2, "issueId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    .line 673
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->c()V

    .line 674
    invoke-direct {p0}, Lcom/helpshift/HSMessagesFragment;->e()V

    .line 675
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->h:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->f:Lcom/helpshift/customadapters/MessagesAdapter;

    invoke-virtual {v2}, Lcom/helpshift/customadapters/MessagesAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 677
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->i:Lcom/helpshift/HSStorage;

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    .line 679
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    iget-object v1, v1, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 680
    invoke-virtual {v1}, Lcom/helpshift/app/ActionBarHelper;->b()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 685
    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 686
    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 687
    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 688
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/helpshift/util/HSIcons;->b(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 695
    :goto_1
    new-instance v1, Lcom/helpshift/HSMessagesFragment$11;

    invoke-direct {v1, p0}, Lcom/helpshift/HSMessagesFragment$11;-><init>(Lcom/helpshift/HSMessagesFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 718
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    new-instance v2, Lcom/helpshift/HSMessagesFragment$12;

    invoke-direct {v2, p0, v0}, Lcom/helpshift/HSMessagesFragment$12;-><init>(Lcom/helpshift/HSMessagesFragment;Landroid/widget/ImageButton;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 727
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->v:Landroid/widget/EditText;

    new-instance v2, Lcom/helpshift/HSMessagesFragment$13;

    invoke-direct {v2, p0, v0}, Lcom/helpshift/HSMessagesFragment$13;-><init>(Lcom/helpshift/HSMessagesFragment;Landroid/widget/ImageButton;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 748
    return-void

    .line 681
    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_0

    .line 690
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 691
    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setAlpha(I)V

    .line 692
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment;->c:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/helpshift/util/HSIcons;->c(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
