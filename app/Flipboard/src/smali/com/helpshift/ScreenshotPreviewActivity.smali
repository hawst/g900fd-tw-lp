.class public Lcom/helpshift/ScreenshotPreviewActivity;
.super Landroid/app/Activity;
.source "ScreenshotPreviewActivity.java"

# interfaces
.implements Lcom/helpshift/view/ScreenshotPreviewView$ScreenshotPreviewInterface;


# instance fields
.field private a:Lcom/helpshift/view/ScreenshotPreviewView;

.field private b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 47
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 49
    const/16 v1, 0x7fbc

    invoke-virtual {p0, v0, v1}, Lcom/helpshift/ScreenshotPreviewActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 50
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 38
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 39
    const-string v1, "SCREENSHOT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 40
    const-string v1, "screenshot_position"

    iget-object v2, p0, Lcom/helpshift/ScreenshotPreviewActivity;->b:Landroid/os/Bundle;

    const-string v3, "screenshot_position"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/helpshift/ScreenshotPreviewActivity;->setResult(ILandroid/content/Intent;)V

    .line 42
    invoke-virtual {p0}, Lcom/helpshift/ScreenshotPreviewActivity;->finish()V

    .line 43
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 55
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    const/16 v0, 0x7fbc

    if-ne p1, v0, :cond_0

    .line 56
    invoke-static {p0, p3}, Lcom/helpshift/util/AttachmentUtil;->a(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 57
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/helpshift/ScreenshotPreviewActivity;->a:Lcom/helpshift/view/ScreenshotPreviewView;

    invoke-virtual {v1, v0}, Lcom/helpshift/view/ScreenshotPreviewView;->setScreenshotPreview(Ljava/lang/String;)V

    .line 61
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    new-instance v0, Lcom/helpshift/view/ScreenshotPreviewView;

    invoke-direct {v0, p0}, Lcom/helpshift/view/ScreenshotPreviewView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/ScreenshotPreviewActivity;->a:Lcom/helpshift/view/ScreenshotPreviewView;

    .line 28
    iget-object v0, p0, Lcom/helpshift/ScreenshotPreviewActivity;->a:Lcom/helpshift/view/ScreenshotPreviewView;

    invoke-virtual {p0, v0}, Lcom/helpshift/ScreenshotPreviewActivity;->setContentView(Landroid/view/View;)V

    .line 30
    invoke-virtual {p0}, Lcom/helpshift/ScreenshotPreviewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/ScreenshotPreviewActivity;->b:Landroid/os/Bundle;

    .line 31
    iget-object v0, p0, Lcom/helpshift/ScreenshotPreviewActivity;->a:Lcom/helpshift/view/ScreenshotPreviewView;

    invoke-virtual {v0, p0}, Lcom/helpshift/view/ScreenshotPreviewView;->setScreenshotPreviewInterface(Lcom/helpshift/view/ScreenshotPreviewView$ScreenshotPreviewInterface;)V

    .line 32
    iget-object v0, p0, Lcom/helpshift/ScreenshotPreviewActivity;->a:Lcom/helpshift/view/ScreenshotPreviewView;

    iget-object v1, p0, Lcom/helpshift/ScreenshotPreviewActivity;->b:Landroid/os/Bundle;

    const-string v2, "SCREENSHOT"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/view/ScreenshotPreviewView;->setScreenshotPreview(Ljava/lang/String;)V

    .line 33
    iget-object v0, p0, Lcom/helpshift/ScreenshotPreviewActivity;->a:Lcom/helpshift/view/ScreenshotPreviewView;

    iget-object v1, p0, Lcom/helpshift/ScreenshotPreviewActivity;->b:Landroid/os/Bundle;

    const-string v2, "screenshot_text_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/view/ScreenshotPreviewView;->setSendButtonText(I)V

    .line 34
    return-void
.end method
