.class public final Lcom/helpshift/HSQuestion;
.super Lcom/helpshift/HSActivity;
.source "HSQuestion.java"


# instance fields
.field private o:Lcom/helpshift/HSQuestionFragment;

.field private p:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/helpshift/HSActivity;-><init>()V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/HSQuestion;->o:Lcom/helpshift/HSQuestionFragment;

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 71
    instance-of v0, p1, Lcom/helpshift/HSQuestionFragment;

    if-eqz v0, :cond_0

    .line 72
    check-cast p1, Lcom/helpshift/HSQuestionFragment;

    iput-object p1, p0, Lcom/helpshift/HSQuestion;->o:Lcom/helpshift/HSQuestionFragment;

    .line 74
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 40
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->d()V

    .line 43
    invoke-virtual {p0}, Lcom/helpshift/HSQuestion;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "showInFullScreen"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/helpshift/HSQuestion;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 51
    :cond_0
    const-string v0, "layout"

    const-string v1, "hs__question"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestion;->setContentView(I)V

    .line 55
    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    .line 56
    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->b()V

    .line 58
    sget-object v0, Lcom/helpshift/res/values/HSConfig;->a:Ljava/util/Map;

    const-string v1, "hl"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    const-string v0, "id"

    const-string v1, "hs__helpshiftActivityFooter"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestion;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/helpshift/HSQuestion;->p:Landroid/widget/ImageView;

    .line 61
    iget-object v1, p0, Lcom/helpshift/HSQuestion;->p:Landroid/widget/ImageView;

    sget-object v0, Lcom/helpshift/res/drawable/HSImages;->a:Ljava/util/Map;

    const-string v2, "newHSLogo"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/helpshift/res/drawable/HSDraw;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    iget-object v0, p0, Lcom/helpshift/HSQuestion;->p:Landroid/widget/ImageView;

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 65
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/helpshift/HSQuestion;->c(Z)V

    .line 66
    return-void
.end method

.method public final bridge synthetic onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 15
    invoke-super {p0, p1}, Lcom/helpshift/HSActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 28
    invoke-virtual {p0}, Lcom/helpshift/HSQuestion;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 29
    if-eqz v0, :cond_0

    .line 30
    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lcom/helpshift/HSQuestion;->isFinishing()Z

    move-result v0

    if-ne v0, v2, :cond_0

    .line 32
    invoke-static {}, Lcom/helpshift/util/HSActivityUtil;->a()V

    .line 35
    :cond_0
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onPause()V

    .line 36
    return-void
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 23
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onResume()V

    .line 24
    return-void
.end method

.method public final bridge synthetic onStart()V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStart()V

    return-void
.end method

.method public final bridge synthetic onStop()V
    .locals 0

    .prologue
    .line 15
    invoke-super {p0}, Lcom/helpshift/HSActivity;->onStop()V

    return-void
.end method
