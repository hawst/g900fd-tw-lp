.class public Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;
.super Ljava/lang/Object;
.source "DoubleMetaphone.java"


# instance fields
.field final a:Ljava/lang/StringBuilder;

.field final b:Ljava/lang/StringBuilder;

.field final c:I

.field final synthetic d:Lcom/helpshift/external/DoubleMetaphone;


# direct methods
.method public constructor <init>(Lcom/helpshift/external/DoubleMetaphone;I)V
    .locals 2

    .prologue
    .line 998
    iput-object p1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->d:Lcom/helpshift/external/DoubleMetaphone;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 994
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->d:Lcom/helpshift/external/DoubleMetaphone;

    iget v1, v1, Lcom/helpshift/external/DoubleMetaphone;->a:I

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->a:Ljava/lang/StringBuilder;

    .line 995
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->d:Lcom/helpshift/external/DoubleMetaphone;

    iget v1, v1, Lcom/helpshift/external/DoubleMetaphone;->a:I

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b:Ljava/lang/StringBuilder;

    .line 999
    iput p2, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c:I

    .line 1000
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1035
    iget v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c:I

    iget-object v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1036
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 1037
    iget-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_0
    iget-object v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->a:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1044
    iget v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c:I

    iget-object v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    sub-int/2addr v0, v1

    .line 1045
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v1, v0, :cond_0

    .line 1046
    iget-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1050
    :goto_0
    return-void

    .line 1048
    :cond_0
    iget-object v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method


# virtual methods
.method public final a(C)V
    .locals 0

    .prologue
    .line 1003
    invoke-virtual {p0, p1}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b(C)V

    .line 1004
    invoke-virtual {p0, p1}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c(C)V

    .line 1005
    return-void
.end method

.method public final a(CC)V
    .locals 0

    .prologue
    .line 1008
    invoke-virtual {p0, p1}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b(C)V

    .line 1009
    invoke-virtual {p0, p2}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c(C)V

    .line 1010
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1025
    invoke-direct {p0, p1}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b(Ljava/lang/String;)V

    .line 1026
    invoke-direct {p0, p1}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c(Ljava/lang/String;)V

    .line 1027
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1030
    invoke-direct {p0, p1}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b(Ljava/lang/String;)V

    .line 1031
    invoke-direct {p0, p2}, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c(Ljava/lang/String;)V

    .line 1032
    return-void
.end method

.method public final b(C)V
    .locals 2

    .prologue
    .line 1013
    iget-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iget v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c:I

    if-ge v0, v1, :cond_0

    .line 1014
    iget-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->a:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1016
    :cond_0
    return-void
.end method

.method public final c(C)V
    .locals 2

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    iget v1, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->c:I

    if-ge v0, v1, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/helpshift/external/DoubleMetaphone$DoubleMetaphoneResult;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1022
    :cond_0
    return-void
.end method
