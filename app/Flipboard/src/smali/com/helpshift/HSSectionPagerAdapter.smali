.class public final Lcom/helpshift/HSSectionPagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "HSSectionPagerAdapter.java"


# static fields
.field private static c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Section;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field b:I

.field private d:Lcom/helpshift/HSApiData;

.field private e:Lcom/helpshift/HSStorage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 43
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p2}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSSectionPagerAdapter;->d:Lcom/helpshift/HSApiData;

    .line 44
    iget-object v0, p0, Lcom/helpshift/HSSectionPagerAdapter;->d:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSSectionPagerAdapter;->e:Lcom/helpshift/HSStorage;

    .line 45
    sget-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Lcom/helpshift/HSSectionPagerAdapter;->b:I

    .line 48
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSSectionPagerAdapter;->d:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->b()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    .line 49
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 51
    sget-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    iget-object v0, v0, Lcom/helpshift/Section;->d:Ljava/lang/String;

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 52
    iput v1, p0, Lcom/helpshift/HSSectionPagerAdapter;->b:I

    .line 56
    :cond_0
    iget-object v0, p0, Landroid/support/v4/view/PagerAdapter;->a:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_1
    return-void

    .line 50
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    .line 58
    const-string v1, "HelpShiftDebug"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/helpshift/Log;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 78
    sget-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    .line 80
    new-instance v1, Lcom/helpshift/HSSectionFragment;

    invoke-direct {v1}, Lcom/helpshift/HSSectionFragment;-><init>()V

    .line 82
    iget-object v0, v0, Lcom/helpshift/Section;->d:Ljava/lang/String;

    .line 84
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 85
    const-string v3, "sectionPublishId"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    invoke-virtual {v1, v2}, Lcom/helpshift/HSSectionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 87
    return-object v1
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Section;

    .line 68
    iget-object v0, v0, Lcom/helpshift/Section;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/helpshift/HSSectionPagerAdapter;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
