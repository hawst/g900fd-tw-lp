.class Lcom/helpshift/HSApiData$4;
.super Landroid/os/Handler;
.source "HSApiData.java"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic b:Ljava/lang/Boolean;

.field final synthetic c:Landroid/os/Handler;

.field final synthetic d:Lcom/helpshift/HSApiData;


# direct methods
.method constructor <init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;Ljava/lang/Boolean;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lcom/helpshift/HSApiData$4;->d:Lcom/helpshift/HSApiData;

    iput-object p2, p0, Lcom/helpshift/HSApiData$4;->a:Landroid/os/Handler;

    iput-object p3, p0, Lcom/helpshift/HSApiData$4;->b:Ljava/lang/Boolean;

    iput-object p4, p0, Lcom/helpshift/HSApiData$4;->c:Landroid/os/Handler;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 301
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 302
    const-string v1, "response"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/json/JSONObject;

    .line 303
    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 304
    const-string v2, "issues"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 305
    iget-object v2, p0, Lcom/helpshift/HSApiData$4;->d:Lcom/helpshift/HSApiData;

    iget-object v2, v2, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v3, "issuesTs"

    invoke-virtual {v2, v3, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 308
    iget-object v1, p0, Lcom/helpshift/HSApiData$4;->d:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v1, v0}, Lcom/helpshift/HSStorage;->a(Lorg/json/JSONArray;)V

    .line 311
    :cond_0
    iget-object v1, p0, Lcom/helpshift/HSApiData$4;->a:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    .line 312
    iget-object v2, p0, Lcom/helpshift/HSApiData$4;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 313
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 314
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 315
    iget-object v0, p0, Lcom/helpshift/HSApiData$4;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 327
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSApiData$4;->d:Lcom/helpshift/HSApiData;

    iget-object v0, p0, Lcom/helpshift/HSApiData$4;->c:Landroid/os/Handler;

    invoke-static {v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;)V

    goto :goto_0

    .line 326
    :catch_0
    move-exception v0

    goto :goto_0

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSApiData$4;->d:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    invoke-virtual {v0}, Lcom/helpshift/HSStorage;->b()Lorg/json/JSONArray;

    move-result-object v0

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 322
    iget-object v0, p0, Lcom/helpshift/HSApiData$4;->a:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method
