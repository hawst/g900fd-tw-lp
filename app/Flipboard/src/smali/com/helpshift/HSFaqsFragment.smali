.class public final Lcom/helpshift/HSFaqsFragment;
.super Landroid/support/v4/app/ListFragment;
.source "HSFaqsFragment.java"

# interfaces
.implements Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;
.implements Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Lcom/helpshift/app/ActionBarHelper;

.field private C:Landroid/os/Handler;

.field private D:Landroid/os/Handler;

.field private final i:Ljava/lang/String;

.field private j:Lcom/helpshift/HSActivity;

.field private k:Landroid/os/Bundle;

.field private l:Ljava/lang/String;

.field private m:Lcom/helpshift/HSApiData;

.field private n:Landroid/widget/ArrayAdapter;

.field private o:Landroid/widget/ArrayAdapter;

.field private p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/helpshift/Faq;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/helpshift/Faq;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroid/widget/ListView;

.field private s:Landroid/view/View;

.field private t:Lcom/helpshift/view/HSViewPager;

.field private u:Landroid/view/MenuItem;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 37
    const-string v0, "HelpShiftDebug"

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->i:Ljava/lang/String;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->p:Ljava/util/List;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->q:Ljava/util/List;

    .line 50
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->v:Ljava/lang/String;

    .line 51
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->w:Ljava/lang/String;

    .line 52
    iput-boolean v1, p0, Lcom/helpshift/HSFaqsFragment;->x:Z

    .line 53
    iput-boolean v1, p0, Lcom/helpshift/HSFaqsFragment;->y:Z

    .line 54
    iput-boolean v1, p0, Lcom/helpshift/HSFaqsFragment;->z:Z

    .line 200
    new-instance v0, Lcom/helpshift/HSFaqsFragment$4;

    invoke-direct {v0, p0}, Lcom/helpshift/HSFaqsFragment$4;-><init>(Lcom/helpshift/HSFaqsFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->C:Landroid/os/Handler;

    .line 239
    new-instance v0, Lcom/helpshift/HSFaqsFragment$5;

    invoke-direct {v0, p0}, Lcom/helpshift/HSFaqsFragment$5;-><init>(Lcom/helpshift/HSFaqsFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->D:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSFaqsFragment;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/helpshift/Faq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 267
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/helpshift/HSFaqsFragment;->z:Z

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 268
    :cond_0
    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 273
    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->q:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    move v1, v0

    .line 274
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 275
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 276
    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->q:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->o:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 280
    return-void
.end method

.method static synthetic b(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/app/ActionBarHelper;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 310
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 314
    :try_start_0
    const-string v0, "s"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    :goto_0
    const-string v0, "s"

    invoke-static {v0, v1}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 319
    iput-object p1, p0, Lcom/helpshift/HSFaqsFragment;->v:Ljava/lang/String;

    .line 321
    :cond_0
    return-void

    .line 315
    :catch_0
    move-exception v0

    .line 316
    const-string v2, "HelpShiftDebug"

    const-string v3, "JSONException"

    invoke-static {v2, v3, v0}, Lcom/helpshift/Log;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic c(Lcom/helpshift/HSFaqsFragment;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/helpshift/HSFaqsFragment;->e()V

    return-void
.end method

.method static synthetic d(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/HSActivity;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->m:Lcom/helpshift/HSApiData;

    iget-object v1, v0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->a()V

    :cond_0
    iget-object v0, v0, Lcom/helpshift/HSApiData;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/helpshift/HSFaqsFragment;->a(Ljava/util/ArrayList;)V

    .line 264
    return-void
.end method

.method static synthetic e(Lcom/helpshift/HSFaqsFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->l:Ljava/lang/String;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    invoke-virtual {v0, v1}, Lcom/helpshift/app/ActionBarHelper;->b(Landroid/view/MenuItem;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 306
    invoke-direct {p0, v0}, Lcom/helpshift/HSFaqsFragment;->b(Ljava/lang/String;)V

    .line 307
    return-void
.end method

.method static synthetic f(Lcom/helpshift/HSFaqsFragment;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->k:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic g(Lcom/helpshift/HSFaqsFragment;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->y:Z

    return v0
.end method

.method static synthetic h(Lcom/helpshift/HSFaqsFragment;)Lcom/helpshift/HSApiData;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->m:Lcom/helpshift/HSApiData;

    return-object v0
.end method

.method static synthetic i(Lcom/helpshift/HSFaqsFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcom/helpshift/HSFaqsFragment;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->n:Landroid/widget/ArrayAdapter;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->x:Z

    if-eqz v0, :cond_1

    .line 167
    invoke-direct {p0}, Lcom/helpshift/HSFaqsFragment;->e()V

    .line 168
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->q:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    .line 173
    :goto_0
    iget-object v1, v0, Lcom/helpshift/Faq;->e:Ljava/lang/String;

    const-string v2, "empty_status"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 174
    iget-object v1, v0, Lcom/helpshift/Faq;->e:Ljava/lang/String;

    const-string v2, "section"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 175
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-class v3, Lcom/helpshift/HSQuestionsList;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    const-string v2, "sectionPublishId"

    iget-object v0, v0, Lcom/helpshift/Faq;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-object v0, v1

    .line 182
    :goto_1
    const-string v1, "showInFullScreen"

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    invoke-static {v2}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 183
    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->k:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 184
    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 185
    invoke-virtual {p0}, Lcom/helpshift/HSFaqsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 187
    :cond_0
    return-void

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/Faq;

    goto :goto_0

    .line 178
    :cond_2
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-class v3, Lcom/helpshift/HSQuestion;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 179
    const-string v2, "questionPublishId"

    iget-object v0, v0, Lcom/helpshift/Faq;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    const-string v0, "decomp"

    iget-boolean v2, p0, Lcom/helpshift/HSFaqsFragment;->y:Z

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 330
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 331
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->w:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/helpshift/HSFaqsFragment;->b(Ljava/lang/String;)V

    .line 335
    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->l:Ljava/lang/String;

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->A:Ljava/lang/String;

    const-string v2, "zh"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->A:Ljava/lang/String;

    const-string v2, "ja"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->A:Ljava/lang/String;

    const-string v2, "ko"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->l:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->l:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_3

    if-nez v0, :cond_3

    :cond_1
    invoke-direct {p0}, Lcom/helpshift/HSFaqsFragment;->d()V

    .line 336
    :goto_2
    return v1

    .line 333
    :cond_2
    iput-object p1, p0, Lcom/helpshift/HSFaqsFragment;->w:Ljava/lang/String;

    goto :goto_0

    .line 335
    :cond_3
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->m:Lcom/helpshift/HSApiData;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/helpshift/HSApiData;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/helpshift/HSFaqsFragment;->a(Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 341
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->v:Ljava/lang/String;

    .line 342
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->w:Ljava/lang/String;

    .line 343
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->t:Lcom/helpshift/view/HSViewPager;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->t:Lcom/helpshift/view/HSViewPager;

    invoke-virtual {v0, v3}, Lcom/helpshift/view/HSViewPager;->setPagingEnabled(Z)V

    .line 346
    :cond_0
    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->y:Z

    if-nez v0, :cond_1

    .line 347
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->e()V

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->z:Z

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    :goto_0
    invoke-direct {p0}, Lcom/helpshift/HSFaqsFragment;->d()V

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->o:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSFaqsFragment;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->o:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iput-boolean v4, p0, Lcom/helpshift/HSFaqsFragment;->x:Z

    .line 350
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    invoke-virtual {v0, v3}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 351
    return v4

    .line 349
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 356
    invoke-direct {p0}, Lcom/helpshift/HSFaqsFragment;->e()V

    .line 357
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->t:Lcom/helpshift/view/HSViewPager;

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->t:Lcom/helpshift/view/HSViewPager;

    invoke-virtual {v0, v2}, Lcom/helpshift/view/HSViewPager;->setPagingEnabled(Z)V

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSFaqsFragment;->a(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->x:Z

    .line 361
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->a:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    invoke-virtual {v0, v2}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 364
    :cond_2
    return v2
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 97
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-string v1, "id"

    const-string v2, "pager"

    invoke-static {p0, v1, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/HSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/helpshift/view/HSViewPager;

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->t:Lcom/helpshift/view/HSViewPager;

    .line 98
    return-void
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4

    .prologue
    .line 102
    const-string v0, "id"

    const-string v1, "hs__action_search"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    .line 103
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 105
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-string v3, "hs__search_hint"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;Ljava/lang/String;)V

    .line 106
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$QueryTextActions;)V

    .line 107
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->u:Landroid/view/MenuItem;

    invoke-virtual {v0, v1, p0}, Lcom/helpshift/app/ActionBarHelper;->a(Landroid/view/MenuItem;Lcom/helpshift/view/SimpleMenuItemCompat$MenuItemActions;)V

    .line 109
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->m:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->j()V

    .line 110
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    invoke-virtual {p0}, Lcom/helpshift/HSFaqsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSActivity;

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    .line 62
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Lcom/helpshift/HSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->k:Landroid/os/Bundle;

    .line 63
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->k:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->k:Landroid/os/Bundle;

    const-string v1, "decomp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->y:Z

    .line 65
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->b:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->z:Z

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    iget-object v0, v0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->B:Lcom/helpshift/app/ActionBarHelper;

    .line 69
    new-instance v0, Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    invoke-direct {v0, v1}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->m:Lcom/helpshift/HSApiData;

    .line 71
    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->z:Z

    if-ne v0, v4, :cond_1

    .line 72
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-string v1, "layout"

    const-string v2, "hs__search_list_footer"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    .line 79
    :goto_0
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-string v1, "layout"

    const-string v2, "hs__simple_list_item_1"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 81
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    iget-object v3, p0, Lcom/helpshift/HSFaqsFragment;->p:Ljava/util/List;

    invoke-direct {v1, v2, v0, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/helpshift/HSFaqsFragment;->n:Landroid/widget/ArrayAdapter;

    .line 84
    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    iget-object v3, p0, Lcom/helpshift/HSFaqsFragment;->q:Ljava/util/List;

    invoke-direct {v1, v2, v0, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v1, p0, Lcom/helpshift/HSFaqsFragment;->o:Landroid/widget/ArrayAdapter;

    .line 87
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->n:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Lcom/helpshift/HSFaqsFragment;->a(Landroid/widget/ListAdapter;)V

    .line 88
    invoke-virtual {p0, v4}, Lcom/helpshift/HSFaqsFragment;->setHasOptionsMenu(Z)V

    .line 90
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->A:Ljava/lang/String;

    .line 91
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-string v1, "layout"

    const-string v2, "hs__no_faqs"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 155
    invoke-super {p0}, Landroid/support/v4/app/ListFragment;->onResume()V

    .line 156
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->a:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->x:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/helpshift/HSActivity;->b(Z)V

    .line 159
    :cond_0
    return-void

    .line 157
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 114
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 116
    invoke-virtual {p0}, Lcom/helpshift/HSFaqsFragment;->k_()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    .line 117
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->r:Landroid/widget/ListView;

    new-instance v1, Lcom/helpshift/HSFaqsFragment$1;

    invoke-direct {v1, p0}, Lcom/helpshift/HSFaqsFragment$1;-><init>(Lcom/helpshift/HSFaqsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 127
    iget-boolean v0, p0, Lcom/helpshift/HSFaqsFragment;->z:Z

    if-ne v0, v2, :cond_0

    .line 128
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->s:Landroid/view/View;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "report_issue"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 130
    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->j:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Landroid/widget/Button;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v2, v2, v4

    invoke-static {v1, v2}, Lcom/helpshift/util/HSIcons;->b(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 131
    new-instance v1, Lcom/helpshift/HSFaqsFragment$2;

    invoke-direct {v1, p0}, Lcom/helpshift/HSFaqsFragment$2;-><init>(Lcom/helpshift/HSFaqsFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    invoke-super {p0, v0, v1}, Landroid/support/v4/app/ListFragment;->a(ZZ)V

    .line 147
    iget-object v0, p0, Lcom/helpshift/HSFaqsFragment;->m:Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSFaqsFragment;->C:Landroid/os/Handler;

    iget-object v2, p0, Lcom/helpshift/HSFaqsFragment;->D:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 151
    :goto_0
    return-void

    .line 148
    :catch_0
    move-exception v0

    .line 149
    const-string v1, "HelpShiftDebug"

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/helpshift/Log;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
