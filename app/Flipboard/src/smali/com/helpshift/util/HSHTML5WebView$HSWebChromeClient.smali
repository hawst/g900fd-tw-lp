.class public Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "HSHTML5WebView.java"


# instance fields
.field final synthetic a:Lcom/helpshift/util/HSHTML5WebView;

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/view/View;


# direct methods
.method private constructor <init>(Lcom/helpshift/util/HSHTML5WebView;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/helpshift/util/HSHTML5WebView;B)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;-><init>(Lcom/helpshift/util/HSHTML5WebView;)V

    return-void
.end method


# virtual methods
.method public getDefaultVideoPoster()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 178
    const-string v0, "HelpShiftDebug"

    const-string v1, "here in on getDefaultVideoPoster"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->b:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v1}, Lcom/helpshift/util/HSHTML5WebView;->d(Lcom/helpshift/util/HSHTML5WebView;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "drawable"

    const-string v3, "default_video_poster"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->b:Landroid/graphics/Bitmap;

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getVideoLoadingProgressView()Landroid/view/View;
    .locals 4

    .prologue
    .line 188
    const-string v0, "HelpShiftDebug"

    const-string v1, "here in on getVideoLoadingPregressView"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->d(Lcom/helpshift/util/HSHTML5WebView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 192
    iget-object v1, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v1}, Lcom/helpshift/util/HSHTML5WebView;->d(Lcom/helpshift/util/HSHTML5WebView;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "layout"

    const-string v3, "hs__video_loading_progress"

    invoke-static {v1, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->c:Landroid/view/View;

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->c:Landroid/view/View;

    return-object v0
.end method

.method public onConsoleMessage(Landroid/webkit/ConsoleMessage;)Z
    .locals 3

    .prologue
    .line 172
    const-string v0, "HelpShiftDebug"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/webkit/ConsoleMessage;->lineNumber()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method public onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 2

    .prologue
    .line 211
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-interface {p2, p1, v0, v1}, Landroid/webkit/GeolocationPermissions$Callback;->invoke(Ljava/lang/String;ZZ)V

    .line 212
    return-void
.end method

.method public onHideCustomView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 152
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->b(Lcom/helpshift/util/HSHTML5WebView;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v1}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 161
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;Landroid/view/View;)Landroid/view/View;

    .line 162
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->b(Lcom/helpshift/util/HSHTML5WebView;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->c(Lcom/helpshift/util/HSHTML5WebView;)Landroid/webkit/WebChromeClient$CustomViewCallback;

    move-result-object v0

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 165
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/util/HSHTML5WebView;->setVisibility(I)V

    .line 167
    const-string v0, "HelpShiftDebug"

    const-string v1, "set it to webVew"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->d(Lcom/helpshift/util/HSHTML5WebView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/helpshift/app/ActionBarActivity;

    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x2

    mul-int/lit8 v2, p2, 0x64

    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setFeatureInt(II)V

    .line 207
    return-void
.end method

.method public onReceivedTitle(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->d(Lcom/helpshift/util/HSHTML5WebView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/helpshift/app/ActionBarActivity;

    invoke-virtual {v0, p2}, Lcom/helpshift/app/ActionBarActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 201
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 2

    .prologue
    .line 132
    const-string v0, "HelpShiftDebug"

    const-string v1, "here in on ShowCustomView"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "View\'s class: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/helpshift/util/HSHTML5WebView;->setVisibility(I)V

    .line 136
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "HSHTML5WebView child count: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v1}, Lcom/helpshift/util/HSHTML5WebView;->getChildCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 139
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 140
    invoke-interface {p2}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 148
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->b(Lcom/helpshift/util/HSHTML5WebView;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 145
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0, p1}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;Landroid/view/View;)Landroid/view/View;

    .line 146
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0, p2}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;Landroid/webkit/WebChromeClient$CustomViewCallback;)Landroid/webkit/WebChromeClient$CustomViewCallback;

    .line 147
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->b(Lcom/helpshift/util/HSHTML5WebView;)Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method
