.class Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;
.super Landroid/webkit/WebViewClient;
.source "HSHTML5WebView.java"


# instance fields
.field final synthetic a:Lcom/helpshift/util/HSHTML5WebView;


# direct methods
.method private constructor <init>(Lcom/helpshift/util/HSHTML5WebView;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/helpshift/util/HSHTML5WebView;B)V
    .locals 0

    .prologue
    .line 215
    invoke-direct {p0, p1}, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;-><init>(Lcom/helpshift/util/HSHTML5WebView;)V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 218
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->e(Lcom/helpshift/util/HSHTML5WebView;)Lcom/helpshift/app/ActionBarActivity;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/helpshift/app/ActionBarActivity;->c(Z)V

    .line 219
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->f(Lcom/helpshift/util/HSHTML5WebView;)Lcom/helpshift/HSQuestionFragment;

    move-result-object v0

    iget v1, v0, Lcom/helpshift/HSQuestionFragment;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {v0}, Lcom/helpshift/HSQuestionFragment;->b()V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    iget v1, v0, Lcom/helpshift/HSQuestionFragment;->c:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    invoke-virtual {v0}, Lcom/helpshift/HSQuestionFragment;->a()V

    goto :goto_0

    :cond_2
    iget v1, v0, Lcom/helpshift/HSQuestionFragment;->c:I

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->f:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->g:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/helpshift/HSQuestionFragment;->b:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, v0, Lcom/helpshift/HSQuestionFragment;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 224
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->g(Lcom/helpshift/util/HSHTML5WebView;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, -0xa

    if-ne p2, v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->h(Lcom/helpshift/util/HSHTML5WebView;)Z

    .line 228
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->f(Lcom/helpshift/util/HSHTML5WebView;)Lcom/helpshift/HSQuestionFragment;

    move-result-object v0

    iget-object v0, v0, Lcom/helpshift/HSQuestionFragment;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 230
    :cond_0
    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 259
    const/4 v1, 0x0

    .line 260
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->e(Lcom/helpshift/util/HSHTML5WebView;)Lcom/helpshift/app/ActionBarActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v2

    .line 263
    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_0
    if-eqz v0, :cond_1

    .line 269
    new-instance v1, Ljava/io/File;

    const-string v3, "/"

    const-string v4, "_"

    invoke-virtual {p2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 271
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 273
    :try_start_1
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v2, ""

    const-string v3, ""

    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v2, v3, v4}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 283
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 278
    :cond_0
    iget-object v2, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0}, Lcom/helpshift/util/HSHTML5WebView;->a(Ljava/net/URL;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 279
    iget-object v2, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0, v1}, Lcom/helpshift/util/HSHTML5WebView;->a(Ljava/net/URL;Ljava/io/File;)V

    .line 283
    :cond_1
    :goto_2
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_1

    .line 275
    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 234
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 235
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 236
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 237
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 238
    iget-object v2, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v2}, Lcom/helpshift/util/HSHTML5WebView;->e(Lcom/helpshift/util/HSHTML5WebView;)Lcom/helpshift/app/ActionBarActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/helpshift/app/ActionBarActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 240
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 241
    const-string v3, "p"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 242
    const-string v1, "u"

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 243
    const-string v1, "fl"

    invoke-static {v1, v2}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    :goto_0
    iget-object v1, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v1}, Lcom/helpshift/util/HSHTML5WebView;->e(Lcom/helpshift/util/HSHTML5WebView;)Lcom/helpshift/app/ActionBarActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/helpshift/app/ActionBarActivity;->startActivity(Landroid/content/Intent;)V

    .line 248
    const/4 v0, 0x1

    .line 253
    :goto_1
    return v0

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/helpshift/util/HSHTML5WebView$MyWebViewClient;->a:Lcom/helpshift/util/HSHTML5WebView;

    invoke-static {v0, p2}, Lcom/helpshift/util/HSHTML5WebView;->a(Lcom/helpshift/util/HSHTML5WebView;Ljava/lang/String;)Ljava/lang/String;

    .line 253
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_0
.end method
