.class public final Lcom/helpshift/util/HSIcons;
.super Ljava/lang/Object;
.source "HSIcons.java"


# direct methods
.method public static a(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 15
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 16
    check-cast p0, Lcom/helpshift/app/ActionBarActivity;

    iget-object v0, p0, Lcom/helpshift/app/ActionBarActivity;->n:Lcom/helpshift/app/ActionBarHelper;

    invoke-virtual {v0}, Lcom/helpshift/app/ActionBarHelper;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010036

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 19
    const v1, 0xffffff

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 20
    invoke-static {p1, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 22
    :cond_0
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 35
    if-eqz p0, :cond_0

    instance-of v0, p0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 36
    const-string v0, "#669900"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 37
    invoke-static {p0, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 39
    :cond_0
    return-void
.end method

.method private static a(Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p0, p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 71
    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 25
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 26
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010036

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 29
    const v1, 0xc0c0c0

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 30
    invoke-static {p1, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 32
    :cond_0
    return-void
.end method

.method public static b(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 42
    if-eqz p0, :cond_0

    instance-of v0, p0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "#FF4444"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 44
    invoke-static {p0, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 46
    :cond_0
    return-void
.end method

.method public static c(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 49
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "hs__send_reply_icon_color"

    invoke-static {p0, v0}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 51
    invoke-static {p1, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 53
    :cond_0
    return-void
.end method

.method public static c(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 56
    if-eqz p0, :cond_0

    instance-of v0, p0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "#FFBB33"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 58
    invoke-static {p0, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 60
    :cond_0
    return-void
.end method

.method public static d(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 63
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "hs__notificationIconColor"

    invoke-static {p0, v0}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 65
    invoke-static {p1, v0}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 67
    :cond_0
    return-void
.end method
