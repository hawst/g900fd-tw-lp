.class public final Lcom/helpshift/util/HSPolling;
.super Ljava/lang/Object;
.source "HSPolling.java"


# instance fields
.field public a:Landroid/os/Handler;

.field b:F

.field c:F

.field d:F

.field e:Landroid/content/Context;

.field public f:Ljava/lang/Runnable;

.field private g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Boolean;Landroid/content/Context;)V
    .locals 2

    .prologue
    const v1, 0x459c4000    # 5000.0f

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const v0, 0x476a6000    # 60000.0f

    iput v0, p0, Lcom/helpshift/util/HSPolling;->d:F

    .line 20
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/util/HSPolling;->g:Ljava/lang/Boolean;

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/helpshift/util/HSPolling;->e:Landroid/content/Context;

    .line 56
    new-instance v0, Lcom/helpshift/util/HSPolling$1;

    invoke-direct {v0, p0}, Lcom/helpshift/util/HSPolling$1;-><init>(Lcom/helpshift/util/HSPolling;)V

    iput-object v0, p0, Lcom/helpshift/util/HSPolling;->f:Ljava/lang/Runnable;

    .line 76
    iput-object p1, p0, Lcom/helpshift/util/HSPolling;->a:Landroid/os/Handler;

    .line 77
    iput v1, p0, Lcom/helpshift/util/HSPolling;->b:F

    .line 78
    iput v1, p0, Lcom/helpshift/util/HSPolling;->c:F

    .line 79
    iput-object p2, p0, Lcom/helpshift/util/HSPolling;->g:Ljava/lang/Boolean;

    .line 80
    iput-object p3, p0, Lcom/helpshift/util/HSPolling;->e:Landroid/content/Context;

    .line 81
    return-void
.end method

.method static synthetic a(Lcom/helpshift/util/HSPolling;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/helpshift/util/HSPolling;->a:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b(Lcom/helpshift/util/HSPolling;)F
    .locals 1

    .prologue
    .line 14
    iget v0, p0, Lcom/helpshift/util/HSPolling;->b:F

    return v0
.end method

.method static synthetic c(Lcom/helpshift/util/HSPolling;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lcom/helpshift/util/HSPolling;->g:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 84
    iget v0, p0, Lcom/helpshift/util/HSPolling;->c:F

    iput v0, p0, Lcom/helpshift/util/HSPolling;->b:F

    .line 85
    iget-object v0, p0, Lcom/helpshift/util/HSPolling;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/helpshift/util/HSPolling;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 86
    iget-object v0, p0, Lcom/helpshift/util/HSPolling;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/helpshift/util/HSPolling;->f:Ljava/lang/Runnable;

    iget v2, p0, Lcom/helpshift/util/HSPolling;->b:F

    float-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 87
    return-void
.end method
