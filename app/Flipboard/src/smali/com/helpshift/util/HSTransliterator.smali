.class public Lcom/helpshift/util/HSTransliterator;
.super Ljava/lang/Object;
.source "HSTransliterator.java"


# static fields
.field static a:Lcom/helpshift/util/HSCharacters1;

.field static b:Lcom/helpshift/util/HSCharacters2;

.field static c:Lcom/helpshift/util/HSCharacters3;

.field static d:Lcom/helpshift/util/HSCharacters4;

.field static e:Lcom/helpshift/util/HSCharacters5;

.field static f:Lcom/helpshift/util/HSCharacters6;

.field static g:Lcom/helpshift/util/HSCharacters7;

.field static h:Lcom/helpshift/util/HSCharacters8;

.field static i:Lcom/helpshift/util/HSCharacters9;

.field static j:Lcom/helpshift/util/HSCharacters10;

.field static k:Lcom/helpshift/util/HSCharacters11;

.field static l:Lcom/helpshift/util/HSCharacters12;

.field static m:Lcom/helpshift/util/HSCharacters13;

.field static n:Lcom/helpshift/util/HSCharacters14;

.field static o:Lcom/helpshift/util/HSCharacters15;

.field static p:Lcom/helpshift/util/HSCharacters16;

.field private static q:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    sput-boolean v0, Lcom/helpshift/util/HSTransliterator;->q:Z

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 13

    .prologue
    const/16 v12, 0x33

    const/16 v11, 0x25

    const/16 v10, 0x11

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 74
    :cond_0
    const-string p0, ""

    .line 133
    :cond_1
    :goto_0
    return-object p0

    :cond_2
    move v0, v1

    .line 76
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 78
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 79
    const/16 v4, 0x80

    if-gt v3, v4, :cond_3

    .line 80
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 86
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v4

    .line 87
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 89
    array-length v6, v4

    move v3, v1

    :goto_2
    if-ge v3, v6, :cond_23

    aget-char v0, v4, v3

    .line 90
    const/16 v7, 0x80

    if-ge v0, v7, :cond_5

    .line 91
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 129
    :cond_4
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 93
    :cond_5
    shr-int/lit8 v7, v0, 0x8

    .line 94
    and-int/lit16 v8, v0, 0xff

    .line 96
    if-lez v7, :cond_7

    if-ge v7, v10, :cond_7

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->a:Lcom/helpshift/util/HSCharacters1;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters1;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_6

    array-length v9, v0

    if-ge v8, v9, :cond_6

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_6

    move v0, v2

    :goto_4
    if-eqz v0, :cond_7

    .line 97
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->a:Lcom/helpshift/util/HSCharacters1;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters1;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_6
    move v0, v1

    .line 96
    goto :goto_4

    .line 98
    :cond_7
    if-lt v7, v10, :cond_9

    if-ge v7, v11, :cond_9

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->b:Lcom/helpshift/util/HSCharacters2;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters2;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_8

    array-length v9, v0

    if-ge v8, v9, :cond_8

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_9

    .line 99
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->b:Lcom/helpshift/util/HSCharacters2;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters2;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_8
    move v0, v1

    .line 98
    goto :goto_5

    .line 100
    :cond_9
    if-lt v7, v11, :cond_b

    if-ge v7, v12, :cond_b

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->c:Lcom/helpshift/util/HSCharacters3;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters3;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_a

    array-length v9, v0

    if-ge v8, v9, :cond_a

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    move v0, v2

    :goto_6
    if-eqz v0, :cond_b

    .line 101
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->c:Lcom/helpshift/util/HSCharacters3;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters3;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_a
    move v0, v1

    .line 100
    goto :goto_6

    .line 102
    :cond_b
    if-lt v7, v12, :cond_d

    const/16 v0, 0x55

    if-ge v7, v0, :cond_d

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->d:Lcom/helpshift/util/HSCharacters4;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters4;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_c

    array-length v9, v0

    if-ge v8, v9, :cond_c

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_c

    move v0, v2

    :goto_7
    if-eqz v0, :cond_d

    .line 103
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->d:Lcom/helpshift/util/HSCharacters4;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters4;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_c
    move v0, v1

    .line 102
    goto :goto_7

    .line 104
    :cond_d
    const/16 v0, 0x55

    if-lt v7, v0, :cond_f

    const/16 v0, 0x62

    if-ge v7, v0, :cond_f

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->e:Lcom/helpshift/util/HSCharacters5;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters5;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_e

    array-length v9, v0

    if-ge v8, v9, :cond_e

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_e

    move v0, v2

    :goto_8
    if-eqz v0, :cond_f

    .line 105
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->e:Lcom/helpshift/util/HSCharacters5;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters5;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_e
    move v0, v1

    .line 104
    goto :goto_8

    .line 106
    :cond_f
    const/16 v0, 0x62

    if-lt v7, v0, :cond_11

    const/16 v0, 0x6f

    if-ge v7, v0, :cond_11

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->f:Lcom/helpshift/util/HSCharacters6;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters6;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_10

    array-length v9, v0

    if-ge v8, v9, :cond_10

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_10

    move v0, v2

    :goto_9
    if-eqz v0, :cond_11

    .line 107
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->f:Lcom/helpshift/util/HSCharacters6;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters6;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_10
    move v0, v1

    .line 106
    goto :goto_9

    .line 108
    :cond_11
    const/16 v0, 0x6f

    if-lt v7, v0, :cond_13

    const/16 v0, 0x79

    if-ge v7, v0, :cond_13

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->g:Lcom/helpshift/util/HSCharacters7;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters7;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_12

    array-length v9, v0

    if-ge v8, v9, :cond_12

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_12

    move v0, v2

    :goto_a
    if-eqz v0, :cond_13

    .line 109
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->g:Lcom/helpshift/util/HSCharacters7;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters7;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_12
    move v0, v1

    .line 108
    goto :goto_a

    .line 110
    :cond_13
    const/16 v0, 0x79

    if-lt v7, v0, :cond_15

    const/16 v0, 0x83

    if-ge v7, v0, :cond_15

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->h:Lcom/helpshift/util/HSCharacters8;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters8;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_14

    array-length v9, v0

    if-ge v8, v9, :cond_14

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_14

    move v0, v2

    :goto_b
    if-eqz v0, :cond_15

    .line 111
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->h:Lcom/helpshift/util/HSCharacters8;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters8;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_14
    move v0, v1

    .line 110
    goto :goto_b

    .line 112
    :cond_15
    const/16 v0, 0x83

    if-lt v7, v0, :cond_17

    const/16 v0, 0x8d

    if-ge v7, v0, :cond_17

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->i:Lcom/helpshift/util/HSCharacters9;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters9;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_16

    array-length v9, v0

    if-ge v8, v9, :cond_16

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_16

    move v0, v2

    :goto_c
    if-eqz v0, :cond_17

    .line 113
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->i:Lcom/helpshift/util/HSCharacters9;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters9;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_16
    move v0, v1

    .line 112
    goto :goto_c

    .line 114
    :cond_17
    const/16 v0, 0x8d

    if-lt v7, v0, :cond_19

    const/16 v0, 0x97

    if-ge v7, v0, :cond_19

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->j:Lcom/helpshift/util/HSCharacters10;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters10;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_18

    array-length v9, v0

    if-ge v8, v9, :cond_18

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_18

    move v0, v2

    :goto_d
    if-eqz v0, :cond_19

    .line 115
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->j:Lcom/helpshift/util/HSCharacters10;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters10;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_18
    move v0, v1

    .line 114
    goto :goto_d

    .line 116
    :cond_19
    const/16 v0, 0x97

    if-lt v7, v0, :cond_1b

    const/16 v0, 0xa3

    if-ge v7, v0, :cond_1b

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->k:Lcom/helpshift/util/HSCharacters11;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters11;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_1a

    array-length v9, v0

    if-ge v8, v9, :cond_1a

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1a

    move v0, v2

    :goto_e
    if-eqz v0, :cond_1b

    .line 117
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->k:Lcom/helpshift/util/HSCharacters11;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters11;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_1a
    move v0, v1

    .line 116
    goto :goto_e

    .line 118
    :cond_1b
    const/16 v0, 0xa3

    if-lt v7, v0, :cond_1d

    const/16 v0, 0xb6

    if-ge v7, v0, :cond_1d

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->l:Lcom/helpshift/util/HSCharacters12;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters12;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_1c

    array-length v9, v0

    if-ge v8, v9, :cond_1c

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1c

    move v0, v2

    :goto_f
    if-eqz v0, :cond_1d

    .line 119
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->l:Lcom/helpshift/util/HSCharacters12;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters12;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_1c
    move v0, v1

    .line 118
    goto :goto_f

    .line 120
    :cond_1d
    const/16 v0, 0xb6

    if-lt v7, v0, :cond_1f

    const/16 v0, 0xc0

    if-ge v7, v0, :cond_1f

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->m:Lcom/helpshift/util/HSCharacters13;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters13;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_1e

    array-length v9, v0

    if-ge v8, v9, :cond_1e

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1e

    move v0, v2

    :goto_10
    if-eqz v0, :cond_1f

    .line 121
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->m:Lcom/helpshift/util/HSCharacters13;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters13;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_1e
    move v0, v1

    .line 120
    goto :goto_10

    .line 122
    :cond_1f
    const/16 v0, 0xc0

    if-lt v7, v0, :cond_21

    const/16 v0, 0xcb

    if-ge v7, v0, :cond_21

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->n:Lcom/helpshift/util/HSCharacters14;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters14;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    if-eqz v0, :cond_20

    array-length v9, v0

    if-ge v8, v9, :cond_20

    aget-object v0, v0, v8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_20

    move v0, v2

    :goto_11
    if-eqz v0, :cond_21

    .line 123
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->n:Lcom/helpshift/util/HSCharacters14;

    iget-object v0, v0, Lcom/helpshift/util/HSCharacters14;->a:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    check-cast v0, [Ljava/lang/String;

    aget-object v0, v0, v8

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    :cond_20
    move v0, v1

    .line 122
    goto :goto_11

    .line 124
    :cond_21
    const/16 v0, 0xcb

    if-lt v7, v0, :cond_22

    const/16 v0, 0xd7

    if-ge v7, v0, :cond_22

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->o:Lcom/helpshift/util/HSCharacters15;

    invoke-virtual {v0, v7, v8}, Lcom/helpshift/util/HSCharacters15;->a(II)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 125
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->o:Lcom/helpshift/util/HSCharacters15;

    invoke-virtual {v0, v7, v8}, Lcom/helpshift/util/HSCharacters15;->b(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 126
    :cond_22
    const/16 v0, 0xd7

    if-lt v7, v0, :cond_4

    sget-object v0, Lcom/helpshift/util/HSTransliterator;->o:Lcom/helpshift/util/HSCharacters15;

    invoke-virtual {v0, v7, v8}, Lcom/helpshift/util/HSCharacters15;->a(II)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 127
    sget-object v0, Lcom/helpshift/util/HSTransliterator;->o:Lcom/helpshift/util/HSCharacters15;

    invoke-virtual {v0, v7, v8}, Lcom/helpshift/util/HSCharacters15;->b(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_3

    .line 133
    :cond_23
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 27
    sget-boolean v0, Lcom/helpshift/util/HSTransliterator;->q:Z

    return v0
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 31
    sget-boolean v0, Lcom/helpshift/util/HSTransliterator;->q:Z

    if-nez v0, :cond_0

    .line 32
    new-instance v0, Lcom/helpshift/util/HSCharacters1;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters1;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->a:Lcom/helpshift/util/HSCharacters1;

    .line 33
    new-instance v0, Lcom/helpshift/util/HSCharacters2;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters2;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->b:Lcom/helpshift/util/HSCharacters2;

    .line 34
    new-instance v0, Lcom/helpshift/util/HSCharacters3;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters3;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->c:Lcom/helpshift/util/HSCharacters3;

    .line 35
    new-instance v0, Lcom/helpshift/util/HSCharacters4;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters4;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->d:Lcom/helpshift/util/HSCharacters4;

    .line 36
    new-instance v0, Lcom/helpshift/util/HSCharacters5;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters5;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->e:Lcom/helpshift/util/HSCharacters5;

    .line 37
    new-instance v0, Lcom/helpshift/util/HSCharacters6;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters6;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->f:Lcom/helpshift/util/HSCharacters6;

    .line 38
    new-instance v0, Lcom/helpshift/util/HSCharacters7;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters7;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->g:Lcom/helpshift/util/HSCharacters7;

    .line 39
    new-instance v0, Lcom/helpshift/util/HSCharacters8;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters8;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->h:Lcom/helpshift/util/HSCharacters8;

    .line 40
    new-instance v0, Lcom/helpshift/util/HSCharacters9;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters9;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->i:Lcom/helpshift/util/HSCharacters9;

    .line 41
    new-instance v0, Lcom/helpshift/util/HSCharacters10;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters10;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->j:Lcom/helpshift/util/HSCharacters10;

    .line 42
    new-instance v0, Lcom/helpshift/util/HSCharacters11;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters11;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->k:Lcom/helpshift/util/HSCharacters11;

    .line 43
    new-instance v0, Lcom/helpshift/util/HSCharacters12;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters12;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->l:Lcom/helpshift/util/HSCharacters12;

    .line 44
    new-instance v0, Lcom/helpshift/util/HSCharacters13;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters13;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->m:Lcom/helpshift/util/HSCharacters13;

    .line 45
    new-instance v0, Lcom/helpshift/util/HSCharacters14;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters14;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->n:Lcom/helpshift/util/HSCharacters14;

    .line 46
    new-instance v0, Lcom/helpshift/util/HSCharacters15;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters15;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->o:Lcom/helpshift/util/HSCharacters15;

    .line 47
    new-instance v0, Lcom/helpshift/util/HSCharacters16;

    invoke-direct {v0}, Lcom/helpshift/util/HSCharacters16;-><init>()V

    sput-object v0, Lcom/helpshift/util/HSTransliterator;->p:Lcom/helpshift/util/HSCharacters16;

    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/helpshift/util/HSTransliterator;->q:Z

    .line 50
    :cond_0
    return-void
.end method

.method public static c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->a:Lcom/helpshift/util/HSCharacters1;

    .line 54
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->b:Lcom/helpshift/util/HSCharacters2;

    .line 55
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->c:Lcom/helpshift/util/HSCharacters3;

    .line 56
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->d:Lcom/helpshift/util/HSCharacters4;

    .line 57
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->e:Lcom/helpshift/util/HSCharacters5;

    .line 58
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->f:Lcom/helpshift/util/HSCharacters6;

    .line 59
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->g:Lcom/helpshift/util/HSCharacters7;

    .line 60
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->h:Lcom/helpshift/util/HSCharacters8;

    .line 61
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->i:Lcom/helpshift/util/HSCharacters9;

    .line 62
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->j:Lcom/helpshift/util/HSCharacters10;

    .line 63
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->k:Lcom/helpshift/util/HSCharacters11;

    .line 64
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->l:Lcom/helpshift/util/HSCharacters12;

    .line 65
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->m:Lcom/helpshift/util/HSCharacters13;

    .line 66
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->n:Lcom/helpshift/util/HSCharacters14;

    .line 67
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->o:Lcom/helpshift/util/HSCharacters15;

    .line 68
    sput-object v0, Lcom/helpshift/util/HSTransliterator;->p:Lcom/helpshift/util/HSCharacters16;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/helpshift/util/HSTransliterator;->q:Z

    .line 70
    return-void
.end method
