.class Lcom/helpshift/HSApiData$18;
.super Landroid/os/Handler;
.source "HSApiData.java"


# instance fields
.field final synthetic a:Landroid/os/Handler;

.field final synthetic b:I

.field final synthetic c:Lorg/json/JSONObject;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/helpshift/HSApiData;


# direct methods
.method constructor <init>(Lcom/helpshift/HSApiData;Landroid/os/Handler;ILorg/json/JSONObject;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1239
    iput-object p1, p0, Lcom/helpshift/HSApiData$18;->e:Lcom/helpshift/HSApiData;

    iput-object p2, p0, Lcom/helpshift/HSApiData$18;->a:Landroid/os/Handler;

    iput p3, p0, Lcom/helpshift/HSApiData$18;->b:I

    iput-object p4, p0, Lcom/helpshift/HSApiData$18;->c:Lorg/json/JSONObject;

    iput-object p5, p0, Lcom/helpshift/HSApiData$18;->d:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 1242
    iget-object v0, p0, Lcom/helpshift/HSApiData$18;->a:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1243
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1244
    iget-object v1, p0, Lcom/helpshift/HSApiData$18;->a:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1247
    :try_start_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/HashMap;

    .line 1248
    const-string v1, "status"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1249
    iget-object v1, p0, Lcom/helpshift/HSApiData$18;->e:Lcom/helpshift/HSApiData;

    invoke-static {v0}, Lcom/helpshift/HSApiData;->a(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v0

    .line 1250
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1251
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1252
    const-string v1, "t"

    iget v2, p0, Lcom/helpshift/HSApiData$18;->b:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1253
    const-string v1, "p"

    iget-object v2, p0, Lcom/helpshift/HSApiData$18;->c:Lorg/json/JSONObject;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1254
    iget-object v1, p0, Lcom/helpshift/HSApiData$18;->e:Lcom/helpshift/HSApiData;

    iget-object v1, v1, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iget-object v2, p0, Lcom/helpshift/HSApiData$18;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1260
    :goto_0
    return-void

    .line 1256
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSApiData$18;->e:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iget-object v1, p0, Lcom/helpshift/HSApiData$18;->d:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSStorage;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1259
    :catch_0
    move-exception v0

    goto :goto_0
.end method
