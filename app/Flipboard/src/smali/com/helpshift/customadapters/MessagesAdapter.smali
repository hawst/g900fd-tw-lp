.class public final Lcom/helpshift/customadapters/MessagesAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MessagesAdapter.java"


# instance fields
.field public a:Z

.field private b:Lcom/helpshift/HSMessagesFragment;

.field private c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/helpshift/viewstructs/HSMsg;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Landroid/view/LayoutInflater;

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Ljava/util/List",
            "<",
            "Lcom/helpshift/viewstructs/HSMsg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x1090003

    invoke-direct {p0, v0, v1, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    move-object v0, p1

    .line 54
    check-cast v0, Lcom/helpshift/HSMessagesFragment;

    iput-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->b:Lcom/helpshift/HSMessagesFragment;

    .line 55
    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->d:Ljava/util/List;

    .line 57
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    .line 58
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_txt_admin"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->f:I

    .line 59
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_txt_user"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->g:I

    .line 60
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_confirmation_box"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->h:I

    .line 61
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_confirmation_status"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->i:I

    .line 62
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_request_screenshot"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->j:I

    .line 63
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__local_msg_request_screenshot"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->k:I

    .line 64
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_screenshot_status"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->l:I

    .line 65
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_review_request"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->m:I

    .line 66
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v1, "layout"

    const-string v2, "hs__msg_review_accepted"

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->n:I

    .line 67
    return-void
.end method

.method private a(Landroid/view/View;Lcom/helpshift/viewstructs/HSMsg;ZLcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;)Landroid/view/View;
    .locals 3

    .prologue
    .line 257
    if-nez p1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->i:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 260
    const v0, 0x1020014

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p4, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;->a:Landroid/widget/TextView;

    .line 261
    const v0, 0x1020015

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p4, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;->b:Landroid/widget/TextView;

    .line 262
    invoke-virtual {p1, p4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 267
    :goto_0
    if-eqz p3, :cond_1

    .line 268
    iget-object v0, p4, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v2, "hs__ca_msg"

    invoke-static {v1, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    :goto_1
    iget-object v0, p4, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;->b:Landroid/widget/TextView;

    iget-object v1, p2, Lcom/helpshift/viewstructs/HSMsg;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 274
    return-object p1

    .line 264
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;

    move-object p4, v0

    goto :goto_0

    .line 270
    :cond_1
    iget-object v0, p4, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v2, "hs__cr_msg"

    invoke-static {v1, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/helpshift/customadapters/MessagesAdapter;)Lcom/helpshift/HSMessagesFragment;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->b:Lcom/helpshift/HSMessagesFragment;

    return-object v0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 594
    const-string v0, "\n"

    const-string v1, "<br/>"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 85
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 86
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget v2, v0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    if-eq v2, v1, :cond_2

    :cond_0
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, v0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    const/4 v3, -0x2

    if-le v2, v3, :cond_2

    :cond_1
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v3, "mobile"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 89
    :cond_2
    const/4 v0, 0x2

    .line 111
    :goto_0
    return v0

    .line 90
    :cond_3
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v3, "admin"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 91
    goto :goto_0

    .line 92
    :cond_4
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "cb"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v2, "admin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 93
    const/4 v0, 0x5

    goto :goto_0

    .line 94
    :cond_5
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "rsc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v2, "admin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 95
    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->g:Ljava/lang/String;

    const-string v1, "localRscMessage_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 96
    const/16 v0, 0xe

    goto :goto_0

    .line 98
    :cond_6
    const/16 v0, 0xd

    goto :goto_0

    .line 100
    :cond_7
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "ca"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v2, "mobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 101
    const/4 v0, 0x6

    goto :goto_0

    .line 102
    :cond_8
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "ncr"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v2, "mobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 103
    const/4 v0, 0x7

    goto :goto_0

    .line 104
    :cond_9
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "sc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v2, "mobile"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 105
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 106
    :cond_a
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "rar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v2, "admin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 107
    const/16 v0, 0xb

    goto/16 :goto_0

    .line 108
    :cond_b
    iget-object v1, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v2, "ar"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->c:Ljava/lang/String;

    const-string v1, "mobile"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 109
    const/16 v0, 0xc

    goto/16 :goto_0

    .line 111
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const v8, 0x102000d

    const v5, 0x1020014

    const/4 v3, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 116
    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/viewstructs/HSMsg;

    .line 118
    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0, p1}, Lcom/helpshift/customadapters/MessagesAdapter;->getItemViewType(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :pswitch_0
    move-object p2, v3

    .line 143
    :goto_0
    return-object p2

    .line 121
    :pswitch_1
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;-><init>(B)V

    if-nez p2, :cond_1

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->f:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;->a:Landroid/widget/TextView;

    const v1, 0x1020015

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;->b:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_1
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;->b:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtAdminHolder;

    goto :goto_1

    .line 123
    :pswitch_2
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;-><init>(B)V

    if-nez p2, :cond_3

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->g:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->a:Landroid/widget/TextView;

    const v1, 0x1020015

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->b:Landroid/widget/TextView;

    const v1, 0x1020006

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_2
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget v2, v0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    iget v2, v0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    :cond_2
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->a:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v3, "hs__sending_msg"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;

    goto :goto_2

    :cond_4
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->b:Ljava/lang/String;

    const-string v3, "txt"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget v2, v0, Lcom/helpshift/viewstructs/HSMsg;->f:I

    const/4 v3, -0x2

    if-gt v2, v3, :cond_5

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->a:Landroid/widget/TextView;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$1;

    invoke-direct {v3, p0, v0}, Lcom/helpshift/customadapters/MessagesAdapter$1;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->b:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v3, "hs__sending_fail_msg"

    invoke-static {v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->b:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$TxtUserHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 125
    :pswitch_3
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;-><init>(B)V

    if-nez p2, :cond_6

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->h:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->a:Landroid/widget/TextView;

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->b:Landroid/widget/ProgressBar;

    const v1, 0x1020018

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->c:Landroid/widget/LinearLayout;

    const v1, 0x1020019

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->d:Landroid/widget/ImageButton;

    const v1, 0x102001a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->e:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->e:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->b(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_3
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->a:Landroid/widget/TextView;

    iget-object v3, v0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;

    goto :goto_3

    :cond_7
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->d:Landroid/widget/ImageButton;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$2;

    invoke-direct {v3, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$2;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->e:Landroid/widget/ImageButton;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$3;

    invoke-direct {v3, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$3;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->d:Landroid/widget/ImageButton;

    iget-boolean v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->e:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$CBViewHolder;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_0

    .line 127
    :pswitch_4
    const/4 v1, 0x1

    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;-><init>(B)V

    invoke-direct {p0, p2, v0, v1, v2}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Landroid/view/View;Lcom/helpshift/viewstructs/HSMsg;ZLcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 129
    :pswitch_5
    new-instance v1, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;

    invoke-direct {v1, v6}, Lcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;-><init>(B)V

    invoke-direct {p0, p2, v0, v6, v1}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Landroid/view/View;Lcom/helpshift/viewstructs/HSMsg;ZLcom/helpshift/customadapters/MessagesAdapter$CSViewHolder;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_0

    .line 131
    :pswitch_6
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;-><init>(B)V

    if-nez p2, :cond_9

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->j:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->a:Landroid/widget/TextView;

    const v1, 0x1020019

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v4, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/helpshift/util/HSIcons;->c(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->c:Landroid/widget/ProgressBar;

    const v1, 0x1020003

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->d:Landroid/widget/LinearLayout;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->e:Landroid/widget/ImageView;

    const v1, 0x102001a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->f:Landroid/widget/ImageButton;

    const v1, 0x102001b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->g:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->f:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->b(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x102002b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->h:Landroid/view/View;

    sget v1, Lcom/helpshift/R$id;->admin_message:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->i:Landroid/widget/LinearLayout;

    sget v1, Lcom/helpshift/R$id;->button_separator:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->j:Landroid/view/View;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_4
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/helpshift/viewstructs/HSMsg;->d:Ljava/lang/String;

    invoke-static {v4}, Lcom/helpshift/customadapters/MessagesAdapter;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->h:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    const/16 v2, 0xfa

    invoke-static {v0, v2}, Lcom/helpshift/util/AttachmentUtil;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->e:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->j:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_5
    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->f:Landroid/widget/ImageButton;

    iget-boolean v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->g:Landroid/widget/ImageButton;

    iget-boolean v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;

    goto :goto_4

    :cond_a
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->h:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    const/16 v3, 0xfa

    invoke-static {v2, v3}, Lcom/helpshift/util/AttachmentUtil;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->e:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->j:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->g:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->f:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->f:Landroid/widget/ImageButton;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$4;

    invoke-direct {v3, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$4;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->g:Landroid/widget/ImageButton;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$5;

    invoke-direct {v3, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$5;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_5

    :cond_b
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->h:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    new-instance v4, Lcom/helpshift/customadapters/MessagesAdapter$6;

    invoke-direct {v4, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$6;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_5

    :cond_c
    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->h:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RSCViewHolder;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_5

    .line 133
    :pswitch_7
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;-><init>(B)V

    if-nez p2, :cond_e

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->k:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x102000b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->a:Landroid/widget/LinearLayout;

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->b:Landroid/widget/ProgressBar;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->c:Landroid/widget/ImageView;

    const v1, 0x102001a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->d:Landroid/widget/ImageButton;

    const v1, 0x102001b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->e:Landroid/widget/ImageButton;

    const v1, 0x102002b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->e:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->a(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->d:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->b(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_6
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    const/16 v3, 0xfa

    invoke-static {v2, v3}, Lcom/helpshift/util/AttachmentUtil;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->f:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->d:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_d
    :goto_7
    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->d:Landroid/widget/ImageButton;

    iget-boolean v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->e:Landroid/widget/ImageButton;

    iget-boolean v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->a:Z

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;

    goto :goto_6

    :cond_f
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->f:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->d:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->e:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->d:Landroid/widget/ImageButton;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$7;

    invoke-direct {v3, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$7;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->e:Landroid/widget/ImageButton;

    new-instance v3, Lcom/helpshift/customadapters/MessagesAdapter$8;

    invoke-direct {v3, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$8;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_7

    :cond_10
    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$LocalRSCViewHolder;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_7

    .line 135
    :pswitch_8
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;-><init>(B)V

    if-nez p2, :cond_11

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->l:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->a:Landroid/widget/TextView;

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->b:Landroid/widget/ProgressBar;

    const v1, 0x1020010

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_8
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->a:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v5, "hs__screenshot_sent_msg"

    invoke-static {v4, v5}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_12

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    :cond_11
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;

    goto :goto_8

    :cond_12
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, v0, Lcom/helpshift/viewstructs/HSMsg;->h:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-static {v0, v2}, Lcom/helpshift/util/AttachmentUtil;->a(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, v1, Lcom/helpshift/customadapters/MessagesAdapter$SCViewHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 137
    :pswitch_9
    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;

    invoke-direct {v2, v6}, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;-><init>(B)V

    if-nez p2, :cond_13

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v4, p0, Lcom/helpshift/customadapters/MessagesAdapter;->m:I

    invoke-virtual {v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->a:Landroid/widget/TextView;

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->b:Landroid/widget/ProgressBar;

    const v1, 0x1020019

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->c:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    iget-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->c:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v1}, Lcom/helpshift/util/HSIcons;->c(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x102002b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->d:Landroid/view/View;

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v2

    :goto_9
    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->a:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v4, "hs__review_request_message"

    invoke-static {v3, v4}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_14

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->d:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;

    goto :goto_9

    :cond_14
    iget-object v2, v0, Lcom/helpshift/viewstructs/HSMsg;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_15

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->c:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->d:Landroid/view/View;

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->c:Landroid/widget/ImageButton;

    new-instance v2, Lcom/helpshift/customadapters/MessagesAdapter$9;

    invoke-direct {v2, p0, v0, p1}, Lcom/helpshift/customadapters/MessagesAdapter$9;-><init>(Lcom/helpshift/customadapters/MessagesAdapter;Lcom/helpshift/viewstructs/HSMsg;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_15
    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->c:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$RARViewHolder;->d:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 139
    :pswitch_a
    new-instance v1, Lcom/helpshift/customadapters/MessagesAdapter$ARViewHolder;

    invoke-direct {v1, v6}, Lcom/helpshift/customadapters/MessagesAdapter$ARViewHolder;-><init>(B)V

    if-nez p2, :cond_16

    iget-object v0, p0, Lcom/helpshift/customadapters/MessagesAdapter;->e:Landroid/view/LayoutInflater;

    iget v2, p0, Lcom/helpshift/customadapters/MessagesAdapter;->n:I

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/helpshift/customadapters/MessagesAdapter$ARViewHolder;->a:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v0, v1

    :goto_a
    iget-object v0, v0, Lcom/helpshift/customadapters/MessagesAdapter$ARViewHolder;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/helpshift/customadapters/MessagesAdapter;->c:Landroid/content/Context;

    const-string v2, "hs__review_accepted_message"

    invoke-static {v1, v2}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/customadapters/MessagesAdapter$ARViewHolder;

    goto :goto_a

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0x14

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method
