.class Lcom/helpshift/HSReviewFragment$1;
.super Ljava/lang/Object;
.source "HSReviewFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/helpshift/HSReviewFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSReviewFragment;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    invoke-static {v0}, Lcom/helpshift/HSReviewFragment;->a(Lcom/helpshift/HSReviewFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    iget-object v1, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    invoke-static {v1}, Lcom/helpshift/HSReviewFragment;->b(Lcom/helpshift/HSReviewFragment;)Lcom/helpshift/HSStorage;

    move-result-object v1

    const-string v2, "config"

    invoke-virtual {v1, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "rurl"

    const-string v3, ""

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/HSReviewFragment;->a(Lcom/helpshift/HSReviewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    iget-object v1, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    invoke-static {v1}, Lcom/helpshift/HSReviewFragment;->a(Lcom/helpshift/HSReviewFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/HSReviewFragment;->a(Lcom/helpshift/HSReviewFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    invoke-static {v0}, Lcom/helpshift/HSReviewFragment;->a(Lcom/helpshift/HSReviewFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    iget-object v1, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    invoke-static {v1}, Lcom/helpshift/HSReviewFragment;->a(Lcom/helpshift/HSReviewFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/HSReviewFragment;->b(Lcom/helpshift/HSReviewFragment;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_1
    :goto_0
    const-string v0, "reviewed"

    invoke-static {v0}, Lcom/helpshift/HSFunnel;->b(Ljava/lang/String;)V

    .line 102
    iget-object v0, p0, Lcom/helpshift/HSReviewFragment$1;->a:Lcom/helpshift/HSReviewFragment;

    sget-object v0, Lcom/helpshift/Helpshift$HS_RATE_ALERT;->a:Lcom/helpshift/Helpshift$HS_RATE_ALERT;

    invoke-static {}, Lcom/helpshift/HSReviewFragment;->d()V

    .line 103
    return-void

    .line 98
    :catch_0
    move-exception v0

    .line 99
    const-string v1, "HelpShiftDebug"

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/helpshift/Log;->a(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
