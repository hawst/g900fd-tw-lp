.class Lcom/helpshift/HSMessagesFragment$10;
.super Ljava/lang/Object;
.source "HSMessagesFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/helpshift/HSMessagesFragment;


# direct methods
.method constructor <init>(Lcom/helpshift/HSMessagesFragment;)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/helpshift/HSMessagesFragment$10;->a:Lcom/helpshift/HSMessagesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 650
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$10;->a:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->f(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSActivity;

    move-result-object v1

    const-class v2, Lcom/helpshift/HSConversation;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 651
    const-string v1, "showInFullScreen"

    iget-object v2, p0, Lcom/helpshift/HSMessagesFragment$10;->a:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v2}, Lcom/helpshift/HSMessagesFragment;->f(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 652
    const-string v1, "newConversation"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 653
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$10;->a:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v1}, Lcom/helpshift/HSMessagesFragment;->n(Lcom/helpshift/HSMessagesFragment;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 654
    const-string v1, "isRoot"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 655
    iget-object v1, p0, Lcom/helpshift/HSMessagesFragment$10;->a:Lcom/helpshift/HSMessagesFragment;

    invoke-virtual {v1}, Lcom/helpshift/HSMessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/support/v4/app/FragmentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 656
    iget-object v0, p0, Lcom/helpshift/HSMessagesFragment$10;->a:Lcom/helpshift/HSMessagesFragment;

    invoke-static {v0}, Lcom/helpshift/HSMessagesFragment;->l(Lcom/helpshift/HSMessagesFragment;)Lcom/helpshift/HSStorage;

    move-result-object v0

    const-string v1, ""

    const-string v2, "archivedConversationId"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 657
    return-void
.end method
