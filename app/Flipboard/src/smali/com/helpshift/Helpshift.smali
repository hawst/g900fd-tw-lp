.class public final Lcom/helpshift/Helpshift;
.super Ljava/lang/Object;
.source "Helpshift.java"


# static fields
.field private static a:Lcom/helpshift/HSApiData;

.field private static b:Lcom/helpshift/HSStorage;

.field private static c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 122
    sput-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    .line 123
    sput-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    .line 124
    sput-object v0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    return-void
.end method

.method private static a(Ljava/util/HashMap;)Landroid/os/Bundle;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 916
    invoke-static {p0}, Lcom/helpshift/ContactUsFilter;->a(Ljava/util/HashMap;)V

    .line 917
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 918
    const-string v3, "hs-custom-metadata"

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/helpshift/Helpshift$3;

    invoke-direct {v3, p0}, Lcom/helpshift/Helpshift$3;-><init>(Ljava/util/HashMap;)V

    invoke-static {v3}, Lcom/helpshift/Helpshift;->a(Lcom/helpshift/HSCallable;)V

    .line 920
    :cond_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 921
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, p0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    .line 922
    const-string v4, "gotoCoversationAfterContactUs"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "gotoConversationAfterContactUs"

    invoke-virtual {v3, v4, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 925
    :try_start_0
    const-string v0, "requireEmail"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 926
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "requireEmail"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const-string v5, "requireEmail"

    invoke-virtual {v0, v5, v4}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 928
    :cond_3
    const-string v0, "hideNameAndEmail"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 929
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "hideNameAndEmail"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const-string v5, "hideNameAndEmail"

    invoke-virtual {v0, v5, v4}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 938
    :cond_4
    :goto_0
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "conversationPrefillText"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    :try_start_1
    const-string v0, "conversationPrefillText"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "conversationPrefillText"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "null"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 947
    const-string v0, "hs-custom-metadata"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 948
    const-string v0, "dropMeta"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 951
    :cond_5
    const-string v0, "conversationPrefillText"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 952
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 953
    sget-object v3, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "conversationPrefillText"

    invoke-virtual {v3, v4, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 957
    :cond_6
    :goto_1
    const-string v0, "showConvOnReportIssue"

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 964
    return-object v2

    .line 931
    :catch_0
    move-exception v0

    .line 932
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception parsing config : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method static synthetic a()Lcom/helpshift/HSStorage;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 845
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/helpshift/HSFaqs;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "conversationPrefillText"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Lcom/helpshift/Helpshift;->a(Ljava/util/HashMap;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v0, "showInFullScreen"

    invoke-static {p0}, Lcom/helpshift/util/HSActivityUtil;->a(Landroid/app/Activity;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const-string v0, "decomp"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "isRoot"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {}, Lcom/helpshift/util/HSActivityUtil;->b()V

    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 846
    return-void
.end method

.method public static a(Landroid/app/Application;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 197
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/Helpshift;->a(Landroid/content/Context;)V

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "identity"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v3, "uuid"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "requireEmail"

    invoke-virtual {v3, v4}, Lcom/helpshift/HSStorage;->e(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    sget-object v4, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v5, "hideNameAndEmail"

    invoke-virtual {v4, v5}, Lcom/helpshift/HSStorage;->e(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v4

    sget-object v5, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-virtual {v5}, Lcom/helpshift/HSStorage;->k()Lorg/json/JSONObject;

    move-result-object v5

    sget-object v6, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-virtual {v6}, Lcom/helpshift/HSStorage;->j()Ljava/lang/Float;

    move-result-object v6

    sget-object v7, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v8, "libraryVersion"

    invoke-virtual {v7, v8}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_1

    const-string v8, "3.5.0"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    sget-object v7, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    iget-object v7, v7, Lcom/helpshift/HSStorage;->b:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    sget-object v7, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v8, "identity"

    invoke-virtual {v7, v8, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v7, "uuid"

    invoke-virtual {v0, v7, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "requireEmail"

    invoke-virtual {v0, v2, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "hideNameAndEmail"

    invoke-virtual {v0, v2, v4}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, v5}, Lcom/helpshift/HSStorage;->c(Lorg/json/JSONObject;)V

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-virtual {v0, v6}, Lcom/helpshift/HSStorage;->a(Ljava/lang/Float;)V

    :cond_1
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "3.5.0"

    const-string v3, "libraryVersion"

    invoke-virtual {v0, v3, v2}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "identity"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "sdkType"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_4

    sget-object v3, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v0, "sdkType"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "sdkType"

    invoke-virtual {v3, v4, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    :try_start_0
    sget-object v0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v3, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    sget-object v3, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "applicationVersion"

    invoke-virtual {v3, v4}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v3}, Lcom/helpshift/HSApiData;->f()V

    sget-object v3, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    iget-object v3, v3, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    const-string v4, "reviewed"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v3, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v4, "applicationVersion"

    invoke-virtual {v3, v4, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_2
    :goto_1
    sget-object v0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/res/drawable/HSImages;->a(Landroid/content/Context;)V

    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    const-string v1, "appConfig"

    invoke-virtual {v0, v1, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v0, p1, p2, p3}, Lcom/helpshift/HSApiData;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :try_start_1
    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSApiData;->c(Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_5

    invoke-static {}, Lcom/helpshift/HSLifecycleCallbacks;->a()Lcom/helpshift/HSLifecycleCallbacks;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    invoke-virtual {p0, v0}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 198
    :goto_3
    return-void

    .line 197
    :cond_4
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v3, "android"

    const-string v4, "sdkType"

    invoke-virtual {v0, v4, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->e()V

    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    const-class v2, Lcom/helpshift/HSReview;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    sget-object v1, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_6
    :try_start_2
    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    new-instance v1, Lcom/helpshift/Helpshift$1;

    invoke-direct {v1}, Lcom/helpshift/Helpshift$1;-><init>()V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/HSApiData;->b(Landroid/os/Handler;Landroid/os/Handler;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_4
    sget-object v0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/HelpshiftConnectionUtil;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    const-class v3, Lcom/helpshift/HSRetryService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_7
    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->m()V

    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->c()V

    goto :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lcom/helpshift/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    .line 139
    sput-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    sput-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    .line 140
    invoke-static {p0}, Lcom/helpshift/ContactUsFilter;->a(Landroid/content/Context;)V

    .line 141
    sput-object p0, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    .line 143
    :cond_0
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1014
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/Helpshift;->a(Landroid/content/Context;)V

    .line 1015
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "issue_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1017
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "foregroundIssue"

    invoke-virtual {v0, v2}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1019
    :try_start_0
    sget-object v2, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const/4 v0, 0x1

    const-string v3, "pushData"

    invoke-virtual {v2, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    invoke-virtual {v3, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    const-string v4, "pushData"

    invoke-virtual {v2, v4, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 1020
    sget-object v2, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-virtual {v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1021
    sget-object v2, Lcom/helpshift/Helpshift;->c:Landroid/content/Context;

    const-string v3, "push"

    invoke-static {v2, v1, v0, v3, p1}, Lcom/helpshift/util/HSNotification;->a(Landroid/content/Context;Lorg/json/JSONObject;ILjava/lang/String;Landroid/content/Intent;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1024
    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 478
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/Helpshift;->a(Landroid/content/Context;)V

    .line 479
    if-eqz p1, :cond_0

    .line 480
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v1, "identity"

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 481
    sget-object v1, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "deviceToken"

    invoke-virtual {v1, v2, p1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 484
    sget-object v0, Lcom/helpshift/Helpshift;->a:Lcom/helpshift/HSApiData;

    invoke-virtual {v0}, Lcom/helpshift/HSApiData;->i()V

    .line 486
    :cond_0
    return-void
.end method

.method public static a(Lcom/helpshift/HSCallable;)V
    .locals 2

    .prologue
    .line 802
    invoke-static {p0}, Lcom/helpshift/util/Meta;->a(Lcom/helpshift/HSCallable;)V

    .line 804
    :try_start_0
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-static {}, Lcom/helpshift/util/Meta;->a()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/HSStorage;->c(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 807
    :goto_0
    return-void

    .line 806
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 440
    if-eqz p0, :cond_0

    .line 441
    sget-object v0, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "deviceId"

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 402
    if-nez p0, :cond_3

    .line 403
    const-string v0, ""

    move-object v1, v0

    .line 408
    :goto_0
    if-nez p1, :cond_4

    .line 409
    const-string v0, ""

    .line 414
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 415
    sget-object v2, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v3, ""

    const-string v4, "username"

    invoke-virtual {v2, v4, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    sget-object v2, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v3, ""

    const-string v4, "email"

    invoke-virtual {v2, v4, v3}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/helpshift/util/HSPattern;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 420
    sget-object v2, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v3, "username"

    invoke-virtual {v2, v3, v1}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, Lcom/helpshift/util/HSPattern;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 424
    sget-object v1, Lcom/helpshift/Helpshift;->b:Lcom/helpshift/HSStorage;

    const-string v2, "email"

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/HSStorage;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_2
    return-void

    .line 405
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 411
    :cond_4
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
