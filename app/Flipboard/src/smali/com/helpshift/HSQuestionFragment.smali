.class public final Lcom/helpshift/HSQuestionFragment;
.super Landroid/support/v4/app/Fragment;
.source "HSQuestionFragment.java"


# instance fields
.field public a:Landroid/widget/LinearLayout;

.field public b:Landroid/widget/Button;

.field public c:I

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/Button;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/TextView;

.field public k:Landroid/os/Handler;

.field public l:Landroid/os/Handler;

.field private m:Lcom/helpshift/HSApiData;

.field private n:Lcom/helpshift/HSStorage;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/Boolean;

.field private s:Lcom/helpshift/HSActivity;

.field private t:Lcom/helpshift/util/HSHTML5WebView;

.field private u:Ljava/lang/Boolean;

.field private v:Ljava/lang/Boolean;

.field private w:Lorg/json/JSONObject;

.field private x:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->o:Ljava/lang/String;

    .line 36
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->r:Ljava/lang/Boolean;

    .line 37
    iput v1, p0, Lcom/helpshift/HSQuestionFragment;->c:I

    .line 41
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->v:Ljava/lang/Boolean;

    .line 42
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->d:Ljava/lang/Boolean;

    .line 43
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->e:Ljava/lang/Boolean;

    .line 101
    new-instance v0, Lcom/helpshift/HSQuestionFragment$1;

    invoke-direct {v0, p0}, Lcom/helpshift/HSQuestionFragment$1;-><init>(Lcom/helpshift/HSQuestionFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->k:Landroid/os/Handler;

    .line 110
    new-instance v0, Lcom/helpshift/HSQuestionFragment$2;

    invoke-direct {v0, p0}, Lcom/helpshift/HSQuestionFragment$2;-><init>(Lcom/helpshift/HSQuestionFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->l:Landroid/os/Handler;

    .line 134
    new-instance v0, Lcom/helpshift/HSQuestionFragment$3;

    invoke-direct {v0, p0}, Lcom/helpshift/HSQuestionFragment$3;-><init>(Lcom/helpshift/HSQuestionFragment;)V

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->x:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/helpshift/HSQuestionFragment;)Lcom/helpshift/HSActivity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    return-object v0
.end method

.method static synthetic a(Lcom/helpshift/HSQuestionFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/helpshift/HSQuestionFragment;->v:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic a(Lcom/helpshift/HSQuestionFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/helpshift/HSQuestionFragment;->o:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/helpshift/HSQuestionFragment;Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 25
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    iput-object v1, p0, Lcom/helpshift/HSQuestionFragment;->w:Lorg/json/JSONObject;

    :try_start_0
    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->w:Lorg/json/JSONObject;

    const-string v2, "id"

    invoke-virtual {v1, v2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "f"

    invoke-virtual {v0, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "h"

    invoke-virtual {v0, v1, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->m:Lcom/helpshift/HSApiData;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p3, v2, v0}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Ljava/lang/String;ILorg/json/JSONObject;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->m:Lcom/helpshift/HSApiData;

    invoke-virtual {v1, p1, v0, p3, p4}, Lcom/helpshift/HSApiData;->a(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/Boolean;)V

    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/helpshift/HSQuestionFragment;Lcom/helpshift/Faq;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, -0x1

    const v6, 0xffffff

    const/4 v5, 0x1

    .line 25
    iget-object v0, p1, Lcom/helpshift/Faq;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->q:Ljava/lang/String;

    iget-object v0, p1, Lcom/helpshift/Faq;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->p:Ljava/lang/String;

    iget-object v0, p1, Lcom/helpshift/Faq;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->o:Ljava/lang/String;

    iget-object v0, p1, Lcom/helpshift/Faq;->i:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->r:Ljava/lang/Boolean;

    iget v0, p1, Lcom/helpshift/Faq;->h:I

    iput v0, p0, Lcom/helpshift/HSQuestionFragment;->c:I

    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__webViewParent"

    invoke-static {p0, v2, v3}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/helpshift/HSActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    invoke-virtual {v2}, Lcom/helpshift/HSActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    if-nez v3, :cond_1

    new-instance v3, Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {p0}, Lcom/helpshift/HSQuestionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4, p0}, Lcom/helpshift/util/HSHTML5WebView;-><init>(Landroid/content/Context;Lcom/helpshift/HSQuestionFragment;)V

    iput-object v3, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    iget-object v3, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v3}, Lcom/helpshift/util/HSHTML5WebView;->getLayout()Landroid/widget/FrameLayout;

    move-result-object v3

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v8, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iget-object v3, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v3, v0}, Lcom/helpshift/util/HSHTML5WebView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-gt v3, v4, :cond_0

    sget-object v3, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    :cond_0
    sget-object v3, Landroid/webkit/WebSettings$TextSize;->NORMAL:Landroid/webkit/WebSettings$TextSize;

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setTextSize(Landroid/webkit/WebSettings$TextSize;)V

    :cond_1
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->p:Ljava/lang/String;

    const-string v3, "<iframe"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->p:Ljava/lang/String;

    const-string v3, "https"

    const-string v4, "http"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->p:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    const-string v0, "#%06X"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v5, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    and-int/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "<html dir=\"rtl\">"

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "<head><style type=\"text/css\">img, object, embed { max-width: 100%; }body { margin: 0px 10px 10px 0px; padding: 0; line-height: 1.5; white-space: normal; word-wrap: break-word; color: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; }.title { display:block; margin: -12px 0 6px 0; padding: 0; font-size: 1.3125em; line-height: 1.25 }</style><script language=\"javascript\">var iframe = document.getElementsByTagName (\"iframe\") [0]; if (iframe) { iframe.width = \"100%\"; iframe.style.width = \"100%\"; }document.addEventListener(\'click\',function(event) {if (event.target instanceof HTMLImageElement) { event.preventDefault(); event.stopPropagation(); }}, false);</script>\u200b</head><body><strong class=\'title\'>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/helpshift/HSQuestionFragment;->q:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</strong>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/helpshift/HSQuestionFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "</body></html>"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    const-string v3, "text/html"

    const-string v4, "utf-8"

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, Lcom/helpshift/util/HSHTML5WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "<html>"

    goto :goto_1

    :array_0
    .array-data 4
        0x1010031
        0x1010036
    .end array-data
.end method

.method static synthetic b(Lcom/helpshift/HSQuestionFragment;)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->v:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic b(Lcom/helpshift/HSQuestionFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/helpshift/HSQuestionFragment;->d:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic c(Lcom/helpshift/HSQuestionFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/helpshift/HSQuestionFragment;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v1, "hs__mark_helpful_toast"

    invoke-static {v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v1, "hs__mark_unhelpful_toast"

    invoke-static {v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/helpshift/HSQuestionFragment;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/helpshift/HSQuestionFragment;->e:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic d(Lcom/helpshift/HSQuestionFragment;)Lorg/json/JSONObject;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->w:Lorg/json/JSONObject;

    return-object v0
.end method

.method static synthetic e(Lcom/helpshift/HSQuestionFragment;)V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionFragment;->b()V

    return-void
.end method

.method static synthetic f(Lcom/helpshift/HSQuestionFragment;)V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionFragment;->a()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 85
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->u:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->b:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 89
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 92
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 95
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->g:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 99
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 357
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 358
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/helpshift/HSActivity;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    .line 264
    new-instance v0, Lcom/helpshift/HSApiData;

    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    invoke-direct {v0, v1}, Lcom/helpshift/HSApiData;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->m:Lcom/helpshift/HSApiData;

    .line 265
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->m:Lcom/helpshift/HSApiData;

    iget-object v0, v0, Lcom/helpshift/HSApiData;->a:Lcom/helpshift/HSStorage;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->n:Lcom/helpshift/HSStorage;

    .line 267
    const-string v0, "layout"

    const-string v1, "hs__question_fragment"

    invoke-static {p0, v0, v1}, Lcom/helpshift/util/HSRes;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->freeMemory()V

    .line 348
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->removeAllViews()V

    .line 349
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 350
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->destroy()V

    .line 352
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 353
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 380
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 381
    const v2, 0x102002c

    if-ne v1, v2, :cond_2

    .line 382
    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    iget-object v1, v1, Lcom/helpshift/util/HSHTML5WebView;->b:Landroid/view/View;

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    .line 383
    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    iget-object v1, v1, Lcom/helpshift/util/HSHTML5WebView;->a:Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;

    invoke-virtual {v1}, Lcom/helpshift/util/HSHTML5WebView$HSWebChromeClient;->onHideCustomView()V

    .line 389
    :goto_1
    return v0

    .line 382
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 385
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_1

    .line 389
    :cond_2
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1
.end method

.method public final onPause()V
    .locals 3

    .prologue
    .line 169
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    if-eqz v0, :cond_0

    .line 170
    const-string v0, "android.webkit.WebView"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "onPause"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 184
    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 179
    :catch_1
    move-exception v0

    goto :goto_0

    .line 177
    :catch_2
    move-exception v0

    goto :goto_0

    .line 175
    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->t:Lcom/helpshift/util/HSHTML5WebView;

    invoke-virtual {v0}, Lcom/helpshift/util/HSHTML5WebView;->onResume()V

    .line 153
    :cond_0
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->v:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 156
    const-string v1, "id"

    iget-object v2, p0, Lcom/helpshift/HSQuestionFragment;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 157
    const-string v1, "f"

    invoke-static {v1, v0}, Lcom/helpshift/HSFunnel;->a(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 158
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->v:Ljava/lang/Boolean;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    :cond_1
    :goto_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 164
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 273
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 274
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Lcom/helpshift/HSActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 275
    if-eqz v1, :cond_2

    .line 276
    const-string v0, "questionPublishId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 277
    const-string v0, "isDecomp"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-ne v0, v5, :cond_0

    .line 278
    sput-boolean v5, Lcom/helpshift/HSAnalytics;->a:Z

    .line 280
    :cond_0
    const-string v0, "questionPublishId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 281
    iget-object v2, p0, Lcom/helpshift/HSQuestionFragment;->m:Lcom/helpshift/HSApiData;

    iget-object v3, p0, Lcom/helpshift/HSQuestionFragment;->l:Landroid/os/Handler;

    iget-object v4, p0, Lcom/helpshift/HSQuestionFragment;->x:Landroid/os/Handler;

    invoke-virtual {v2, v0, v3, v4}, Lcom/helpshift/HSApiData;->c(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 283
    :cond_1
    sget-object v0, Lcom/helpshift/ContactUsFilter$LOCATION;->c:Lcom/helpshift/ContactUsFilter$LOCATION;

    invoke-static {v0}, Lcom/helpshift/ContactUsFilter;->a(Lcom/helpshift/ContactUsFilter$LOCATION;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->u:Ljava/lang/Boolean;

    .line 286
    :cond_2
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    invoke-virtual {v0}, Lcom/helpshift/HSActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v2, v5, [I

    const v3, 0x1010031

    aput v3, v2, v6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 289
    const v2, 0xffffff

    invoke-virtual {v0, v6, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 290
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 291
    invoke-virtual {p0}, Lcom/helpshift/HSQuestionFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 293
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__contactUsContainer"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->a:Landroid/widget/LinearLayout;

    .line 295
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__question"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->h:Landroid/widget/TextView;

    .line 296
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__helpful_text"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->i:Landroid/widget/TextView;

    .line 297
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__unhelpful_text"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->j:Landroid/widget/TextView;

    .line 298
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__contact_us_btn"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->b:Landroid/widget/Button;

    .line 300
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    iget-object v2, p0, Lcom/helpshift/HSQuestionFragment;->b:Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v2, v2, v6

    invoke-static {v0, v2}, Lcom/helpshift/util/HSIcons;->b(Landroid/content/Context;Landroid/graphics/drawable/Drawable;)V

    .line 302
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__action_faq_helpful"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->f:Landroid/widget/Button;

    .line 303
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->f:Landroid/widget/Button;

    new-instance v2, Lcom/helpshift/HSQuestionFragment$4;

    invoke-direct {v2, p0}, Lcom/helpshift/HSQuestionFragment$4;-><init>(Lcom/helpshift/HSQuestionFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->s:Lcom/helpshift/HSActivity;

    const-string v2, "id"

    const-string v3, "hs__action_faq_unhelpful"

    invoke-static {v0, v2, v3}, Lcom/helpshift/util/HSRes;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/helpshift/HSQuestionFragment;->g:Landroid/widget/Button;

    .line 315
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->g:Landroid/widget/Button;

    new-instance v2, Lcom/helpshift/HSQuestionFragment$5;

    invoke-direct {v2, p0}, Lcom/helpshift/HSQuestionFragment$5;-><init>(Lcom/helpshift/HSQuestionFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 326
    iget-object v0, p0, Lcom/helpshift/HSQuestionFragment;->b:Landroid/widget/Button;

    new-instance v2, Lcom/helpshift/HSQuestionFragment$6;

    invoke-direct {v2, p0, v1}, Lcom/helpshift/HSQuestionFragment$6;-><init>(Lcom/helpshift/HSQuestionFragment;Landroid/os/Bundle;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    invoke-virtual {p0, v5}, Lcom/helpshift/HSQuestionFragment;->setHasOptionsMenu(Z)V

    .line 339
    return-void
.end method
