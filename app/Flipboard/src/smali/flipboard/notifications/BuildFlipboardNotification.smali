.class public Lflipboard/notifications/BuildFlipboardNotification;
.super Lflipboard/notifications/FLNotification;
.source "BuildFlipboardNotification.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lflipboard/notifications/FLNotification;-><init>(I)V

    .line 23
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/app/Notification;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d010a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d010b

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 31
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v4, v4, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v4, :cond_0

    .line 32
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " beta"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 34
    :cond_0
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lflipboard/activities/FirstRunActivity;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 36
    const-string v5, "extra_first_run_start_page"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 37
    const-string v5, "extra_submit_reminder_notification_usage"

    invoke-virtual {v4, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 38
    const-string v5, "extra_notification_id"

    iget v6, p0, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 39
    iget v5, p0, Lflipboard/notifications/FLNotification;->c:I

    const/high16 v6, 0x10000000

    invoke-static {p1, v5, v4, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 40
    new-instance v5, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v5, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 42
    iput-object v4, v5, Landroid/support/v4/app/NotificationCompat$Builder;->d:Landroid/app/PendingIntent;

    .line 43
    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/support/v4/app/NotificationCompat$Builder;

    .line 44
    invoke-virtual {v5, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 45
    invoke-virtual {v5, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 46
    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->b()Landroid/support/v4/app/NotificationCompat$Builder;

    .line 47
    iget-object v0, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    const/4 v4, -0x1

    iput v4, v0, Landroid/app/Notification;->ledARGB:I

    iget-object v0, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    const/16 v4, 0x320

    iput v4, v0, Landroid/app/Notification;->ledOnMS:I

    iget-object v0, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    const/16 v4, 0x4b0

    iput v4, v0, Landroid/app/Notification;->ledOffMS:I

    iget-object v0, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledOnMS:I

    if-eqz v0, :cond_2

    iget-object v0, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    iget v0, v0, Landroid/app/Notification;->ledOffMS:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v4, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    iget-object v6, v5, Landroid/support/v4/app/NotificationCompat$Builder;->B:Landroid/app/Notification;

    iget v6, v6, Landroid/app/Notification;->flags:I

    and-int/lit8 v6, v6, -0x2

    if-eqz v0, :cond_3

    :goto_1
    or-int v0, v6, v1

    iput v0, v4, Landroid/app/Notification;->flags:I

    .line 50
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v0, :cond_1

    .line 51
    new-instance v1, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    invoke-direct {v1, v5}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;-><init>(Landroid/support/v4/app/NotificationCompat$Builder;)V

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0200f2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 53
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v1, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->a:Landroid/graphics/Bitmap;

    .line 54
    invoke-virtual {v1, v3}, Landroid/support/v4/app/NotificationCompat$BigPictureStyle;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigPictureStyle;

    .line 56
    :cond_1
    invoke-virtual {v5, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 58
    invoke-virtual {v5}, Landroid/support/v4/app/NotificationCompat$Builder;->c()Landroid/app/Notification;

    move-result-object v0

    .line 59
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 60
    return-object v0

    :cond_2
    move v0, v2

    .line 47
    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method
