.class public Lflipboard/notifications/ExpireNotification;
.super Lflipboard/notifications/FLNotification;
.source "ExpireNotification.java"


# instance fields
.field private final e:Lflipboard/notifications/FLNotification;

.field private final f:J


# direct methods
.method public constructor <init>(Lflipboard/notifications/FLNotification;J)V
    .locals 2

    .prologue
    .line 21
    iget v0, p1, Lflipboard/notifications/FLNotification;->c:I

    invoke-direct {p0, v0}, Lflipboard/notifications/FLNotification;-><init>(I)V

    .line 22
    iput-object p1, p0, Lflipboard/notifications/ExpireNotification;->e:Lflipboard/notifications/FLNotification;

    .line 23
    iput-wide p2, p0, Lflipboard/notifications/ExpireNotification;->f:J

    .line 24
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/app/Notification;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lflipboard/notifications/ExpireNotification;->e:Lflipboard/notifications/FLNotification;

    invoke-virtual {v0, p1}, Lflipboard/notifications/FLNotification;->a(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 34
    invoke-super {p0, p1}, Lflipboard/notifications/FLNotification;->b(Landroid/content/Context;)V

    .line 36
    new-instance v0, Landroid/content/Intent;

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-class v2, Lflipboard/notifications/NotificationExpirationReciever;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    const-string v1, "extra_notification_id"

    iget v2, p0, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 38
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 40
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v2, "alarm"

    invoke-virtual {v0, v2}, Lflipboard/app/FlipboardApplication;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 41
    const/4 v2, 0x1

    iget-wide v4, p0, Lflipboard/notifications/ExpireNotification;->f:J

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 42
    return-void
.end method
