.class public Lflipboard/notifications/InboxStyleNotification;
.super Lflipboard/notifications/FLNotification;
.source "InboxStyleNotification.java"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lflipboard/notifications/FLNotification;-><init>(I)V

    .line 41
    array-length v0, p5

    if-gtz v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "rows must contain at least one row"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    iput-object p2, p0, Lflipboard/notifications/InboxStyleNotification;->e:Ljava/lang/String;

    .line 45
    iput-object p3, p0, Lflipboard/notifications/InboxStyleNotification;->h:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Lflipboard/notifications/InboxStyleNotification;->f:Ljava/lang/String;

    .line 47
    iput-object p5, p0, Lflipboard/notifications/InboxStyleNotification;->g:[Ljava/lang/String;

    .line 48
    iput-object p6, p0, Lflipboard/notifications/InboxStyleNotification;->i:Landroid/os/Bundle;

    .line 49
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lflipboard/notifications/FLNotification;-><init>(I)V

    .line 53
    iput-object p2, p0, Lflipboard/notifications/InboxStyleNotification;->e:Ljava/lang/String;

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/notifications/InboxStyleNotification;->h:Ljava/lang/String;

    .line 55
    iput-object p3, p0, Lflipboard/notifications/InboxStyleNotification;->f:Ljava/lang/String;

    .line 56
    iput-object p4, p0, Lflipboard/notifications/InboxStyleNotification;->g:[Ljava/lang/String;

    .line 57
    iput-object p5, p0, Lflipboard/notifications/InboxStyleNotification;->i:Landroid/os/Bundle;

    .line 58
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/app/Notification;
    .locals 9

    .prologue
    const/high16 v4, 0x43960000    # 300.0f

    .line 62
    iget-object v0, p0, Lflipboard/notifications/InboxStyleNotification;->g:[Ljava/lang/String;

    array-length v0, v0

    if-gtz v0, :cond_0

    .line 63
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "rows must contain at least one row"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 67
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->fillInStackTrace()Ljava/lang/Throwable;

    .line 68
    new-instance v1, Lflipboard/service/FlCrashListener;

    invoke-direct {v1}, Lflipboard/service/FlCrashListener;-><init>()V

    invoke-static {v0, v1}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    .line 69
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    .line 73
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/service/FlipboardUrlHandler;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    const-string v1, "extra_notification_usage"

    iget-object v2, p0, Lflipboard/notifications/InboxStyleNotification;->i:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 75
    const-string v1, "extra_notification_id"

    iget v2, p0, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 76
    iget-object v1, p0, Lflipboard/notifications/InboxStyleNotification;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Lflipboard/notifications/InboxStyleNotification;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 79
    :cond_1
    iget v1, p0, Lflipboard/notifications/FLNotification;->c:I

    const/high16 v2, 0x10000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 80
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    iget-object v0, p0, Lflipboard/notifications/InboxStyleNotification;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 84
    new-instance v0, Lflipboard/notifications/FLNotification$NotificationThumb;

    iget-object v1, p0, Lflipboard/notifications/InboxStyleNotification;->h:Ljava/lang/String;

    invoke-direct {v0, v1}, Lflipboard/notifications/FLNotification$NotificationThumb;-><init>(Ljava/lang/String;)V

    .line 85
    invoke-static {v0, v4, v4}, Lflipboard/notifications/InboxStyleNotification;->a(Lflipboard/notifications/FLNotification$NotificationThumb;FF)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 88
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x13

    if-le v0, v4, :cond_4

    .line 89
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v5

    invoke-static {v0, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 91
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 93
    new-instance v6, Landroid/graphics/BitmapShader;

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v8, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v6, v1, v7, v8}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 94
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 95
    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 96
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    invoke-virtual {v4, v1, v6, v7, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 103
    :goto_1
    iget-object v1, p0, Lflipboard/notifications/InboxStyleNotification;->f:Ljava/lang/String;

    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 104
    iput-object v2, v1, Landroid/support/v4/app/NotificationCompat$Builder;->d:Landroid/app/PendingIntent;

    const v2, 0x7f0d013a

    .line 105
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    iget-object v2, p0, Lflipboard/notifications/InboxStyleNotification;->g:[Ljava/lang/String;

    iget-object v4, p0, Lflipboard/notifications/InboxStyleNotification;->g:[Ljava/lang/String;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v2, v2, v4

    .line 106
    invoke-virtual {v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 107
    invoke-virtual {v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v1

    .line 108
    iput-object v0, v1, Landroid/support/v4/app/NotificationCompat$Builder;->g:Landroid/graphics/Bitmap;

    .line 110
    new-instance v1, Landroid/support/v4/app/NotificationCompat$InboxStyle;

    invoke-direct {v1}, Landroid/support/v4/app/NotificationCompat$InboxStyle;-><init>()V

    .line 111
    iget-object v2, p0, Lflipboard/notifications/InboxStyleNotification;->g:[Ljava/lang/String;

    array-length v4, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_3

    aget-object v5, v2, v0

    .line 112
    invoke-virtual {v1, v5}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 100
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020103

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_1

    .line 114
    :cond_3
    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Landroid/support/v4/app/NotificationCompat$Style;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 116
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->c()Landroid/app/Notification;

    move-result-object v0

    .line 117
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
