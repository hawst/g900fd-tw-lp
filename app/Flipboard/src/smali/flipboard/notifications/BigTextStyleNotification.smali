.class public Lflipboard/notifications/BigTextStyleNotification;
.super Lflipboard/notifications/FLNotification;
.source "BigTextStyleNotification.java"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:J

.field private final i:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLandroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lflipboard/notifications/FLNotification;-><init>(I)V

    .line 30
    iput-object p2, p0, Lflipboard/notifications/BigTextStyleNotification;->e:Ljava/lang/String;

    .line 31
    iput-object p3, p0, Lflipboard/notifications/BigTextStyleNotification;->f:Ljava/lang/String;

    .line 32
    iput-object p4, p0, Lflipboard/notifications/BigTextStyleNotification;->g:Ljava/lang/String;

    .line 33
    iput-wide p5, p0, Lflipboard/notifications/BigTextStyleNotification;->h:J

    .line 34
    iput-object p7, p0, Lflipboard/notifications/BigTextStyleNotification;->i:Landroid/os/Bundle;

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/app/Notification;
    .locals 8

    .prologue
    const/high16 v5, 0x43960000    # 300.0f

    .line 39
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/service/FlipboardUrlHandler;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 40
    const-string v1, "extra_notification_usage"

    iget-object v2, p0, Lflipboard/notifications/BigTextStyleNotification;->i:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 41
    const-string v1, "extra_notification_id"

    iget v2, p0, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 42
    iget-object v1, p0, Lflipboard/notifications/BigTextStyleNotification;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, p0, Lflipboard/notifications/BigTextStyleNotification;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 45
    :cond_0
    iget v1, p0, Lflipboard/notifications/FLNotification;->c:I

    const/high16 v2, 0x10000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 46
    new-instance v4, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v4, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 48
    const v0, 0x7f0d013a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 51
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_1

    .line 52
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " beta"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 55
    :cond_1
    const/4 v1, 0x0

    .line 56
    iget-object v3, p0, Lflipboard/notifications/BigTextStyleNotification;->g:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 57
    new-instance v1, Lflipboard/notifications/FLNotification$NotificationThumb;

    iget-object v3, p0, Lflipboard/notifications/BigTextStyleNotification;->g:Ljava/lang/String;

    invoke-direct {v1, v3}, Lflipboard/notifications/FLNotification$NotificationThumb;-><init>(Ljava/lang/String;)V

    .line 60
    :cond_2
    invoke-static {v1, v5, v5}, Lflipboard/notifications/BigTextStyleNotification;->a(Lflipboard/notifications/FLNotification$NotificationThumb;FF)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 62
    iput-object v2, v4, Landroid/support/v4/app/NotificationCompat$Builder;->d:Landroid/app/PendingIntent;

    .line 63
    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/support/v4/app/NotificationCompat$Builder;

    .line 64
    iput-object v1, v4, Landroid/support/v4/app/NotificationCompat$Builder;->g:Landroid/graphics/Bitmap;

    .line 66
    iget-wide v2, p0, Lflipboard/notifications/BigTextStyleNotification;->h:J

    const-wide/16 v6, 0x0

    cmp-long v1, v2, v6

    if-lez v1, :cond_3

    iget-wide v2, p0, Lflipboard/notifications/BigTextStyleNotification;->h:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    .line 67
    :goto_0
    invoke-virtual {v4, v2, v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a(J)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 68
    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 69
    iget-object v0, p0, Lflipboard/notifications/BigTextStyleNotification;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 70
    new-instance v0, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v0, v4}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>(Landroid/support/v4/app/NotificationCompat$Builder;)V

    .line 71
    iget-object v1, p0, Lflipboard/notifications/BigTextStyleNotification;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 72
    iget-object v0, p0, Lflipboard/notifications/BigTextStyleNotification;->e:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 74
    invoke-virtual {v4}, Landroid/support/v4/app/NotificationCompat$Builder;->c()Landroid/app/Notification;

    move-result-object v0

    .line 75
    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 76
    return-object v0

    .line 66
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    goto :goto_0
.end method
