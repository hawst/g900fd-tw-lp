.class public Lflipboard/notifications/FLNotification$NotificationThumb;
.super Lflipboard/io/Download$Observer;
.source "FLNotification.java"


# instance fields
.field final a:Ljava/lang/String;

.field b:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    invoke-direct {p0}, Lflipboard/io/Download$Observer;-><init>()V

    .line 107
    sget-object v0, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 109
    if-eqz p1, :cond_0

    .line 110
    iput-object p1, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->a:Ljava/lang/String;

    .line 111
    iget v0, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private declared-synchronized a(Lflipboard/io/Download;Lflipboard/io/Download$Status;)V
    .locals 3

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p1, Lflipboard/io/Download;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 137
    invoke-virtual {p2}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v0

    sget-object v1, Lflipboard/io/Download$Status;->d:Lflipboard/io/Download$Status;

    invoke-virtual {v1}, Lflipboard/io/Download$Status;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 138
    iget v0, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I

    .line 139
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 141
    :cond_0
    monitor-exit p0

    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()Lflipboard/io/BitmapManager$Handle;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 119
    iget-object v0, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    sget-object v1, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v1, v0, v2, v2}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/io/Download;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v1, v0, v2}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/Download;Z)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 101
    check-cast p1, Lflipboard/io/Download;

    check-cast p2, Lflipboard/io/Download$Status;

    invoke-direct {p0, p1, p2}, Lflipboard/notifications/FLNotification$NotificationThumb;->a(Lflipboard/io/Download;Lflipboard/io/Download$Status;)V

    return-void
.end method

.method public final declared-synchronized a(J)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 146
    monitor-enter p0

    :try_start_0
    sget-object v2, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 148
    iget v2, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    move v0, v1

    .line 172
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 151
    :cond_1
    const/16 v2, 0x32

    :try_start_1
    invoke-virtual {p0, v2}, Lflipboard/notifications/FLNotification$NotificationThumb;->a(I)V

    .line 153
    const/4 v2, 0x0

    .line 154
    iget-object v3, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->a:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 155
    sget-object v2, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 157
    sget-object v2, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    iget-object v3, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->a:Ljava/lang/String;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v2

    .line 158
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    :cond_2
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, p1

    .line 162
    :goto_1
    iget v3, p0, Lflipboard/notifications/FLNotification$NotificationThumb;->b:I

    if-lez v3, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v3, v6, v4

    if-gez v3, :cond_3

    sget-object v3, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v3}, Lflipboard/io/NetworkManager;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 163
    sget-object v3, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v3, v6

    .line 162
    const-wide/16 v6, 0x3e8

    invoke-virtual {p0, v6, v7}, Ljava/lang/Object;->wait(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    .line 166
    :catch_0
    move-exception v1

    .line 167
    :try_start_3
    sget-object v3, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    invoke-virtual {v3, v1}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 168
    if-eqz v2, :cond_0

    .line 171
    :try_start_4
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 172
    invoke-virtual {v2}, Lflipboard/io/Download;->b()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 165
    :cond_3
    if-eqz v2, :cond_4

    :try_start_5
    invoke-virtual {v2}, Lflipboard/io/Download;->a()Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    move v0, v1

    .line 170
    :cond_5
    if-eqz v2, :cond_0

    .line 171
    :try_start_6
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 172
    invoke-virtual {v2}, Lflipboard/io/Download;->b()V

    goto :goto_0

    .line 170
    :catchall_1
    move-exception v0

    if-eqz v2, :cond_6

    .line 171
    invoke-virtual {v2, p0}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 172
    invoke-virtual {v2}, Lflipboard/io/Download;->b()V

    :cond_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method
