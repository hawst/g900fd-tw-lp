.class public Lflipboard/notifications/MediaPlayerNotification;
.super Lflipboard/notifications/FLNotification;
.source "MediaPlayerNotification.java"


# instance fields
.field public e:Landroid/graphics/Bitmap;

.field private f:Z

.field private g:Lflipboard/service/audio/Song;

.field private h:Z


# direct methods
.method public constructor <init>(ZLflipboard/service/audio/Song;Z)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lflipboard/notifications/FLNotification;-><init>(I)V

    .line 46
    iput-boolean p1, p0, Lflipboard/notifications/MediaPlayerNotification;->f:Z

    .line 47
    iput-object p2, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    .line 48
    iput-boolean p3, p0, Lflipboard/notifications/MediaPlayerNotification;->h:Z

    .line 49
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Landroid/app/Notification;
    .locals 11

    .prologue
    const v10, 0x7f02003c

    const/16 v9, 0xc8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/high16 v8, 0x8000000

    .line 53
    sget-object v0, Lflipboard/notifications/MediaPlayerNotification;->b:Lflipboard/util/Log;

    new-array v1, v7, [Ljava/lang/Object;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v0, :cond_2

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    aput-object v0, v1, v6

    .line 55
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/AudioPlayerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 56
    const-string v1, "extra_notification_id"

    iget v2, p0, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 57
    const/high16 v1, 0x10000000

    invoke-static {p1, v6, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 58
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, p1}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    .line 59
    iget-object v0, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v0, v0, Lflipboard/service/audio/Song;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 61
    :goto_1
    new-instance v1, Lflipboard/notifications/FLNotification$NotificationThumb;

    iget-object v4, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v4, v4, Lflipboard/service/audio/Song;->d:Ljava/lang/String;

    invoke-direct {v1, v4}, Lflipboard/notifications/FLNotification$NotificationThumb;-><init>(Ljava/lang/String;)V

    .line 64
    const-wide/16 v4, 0xbb8

    invoke-virtual {v1, v4, v5}, Lflipboard/notifications/FLNotification$NotificationThumb;->a(J)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 65
    invoke-virtual {v1}, Lflipboard/notifications/FLNotification$NotificationThumb;->a()Lflipboard/io/BitmapManager$Handle;

    move-result-object v1

    .line 66
    if-eqz v1, :cond_4

    invoke-virtual {v1, v9, v9, v6}, Lflipboard/io/BitmapManager$Handle;->a(IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_2
    iput-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->e:Landroid/graphics/Bitmap;

    .line 71
    :goto_3
    iput-object v2, v3, Landroid/support/v4/app/NotificationCompat$Builder;->d:Landroid/app/PendingIntent;

    .line 72
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->a()Landroid/support/v4/app/NotificationCompat$Builder;

    .line 73
    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->e:Landroid/graphics/Bitmap;

    iput-object v1, v3, Landroid/support/v4/app/NotificationCompat$Builder;->g:Landroid/graphics/Bitmap;

    .line 74
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->a(J)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 75
    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    :goto_4
    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 76
    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->b:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v1, v1, Lflipboard/service/audio/Song;->b:Ljava/lang/String;

    :goto_5
    invoke-virtual {v3, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 78
    iget-boolean v1, p0, Lflipboard/notifications/MediaPlayerNotification;->f:Z

    if-eqz v1, :cond_0

    .line 79
    invoke-virtual {v3, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->c(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 82
    :cond_0
    new-instance v1, Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    invoke-direct {v1, v3}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;-><init>(Landroid/support/v4/app/NotificationCompat$Builder;)V

    .line 85
    iget-object v2, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v2, v2, Lflipboard/service/audio/Song;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v2, v2, Lflipboard/service/audio/Song;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 86
    const-string v2, "%s\n\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    iget-object v0, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v0, v0, Lflipboard/service/audio/Song;->c:Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-static {v2, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 90
    :cond_1
    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$BigTextStyle;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$BigTextStyle;

    .line 92
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    const-string v1, "audio_action"

    const/16 v2, 0x3e9

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 94
    const v1, 0xf31a

    invoke-static {p1, v1, v0, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 96
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    const-string v2, "audio_action"

    const/16 v4, 0x3ec

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 98
    const v2, 0xf31d

    invoke-static {p1, v2, v1, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 100
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {v2, p1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 101
    const-string v4, "audio_action"

    const/16 v5, 0x3ea

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 102
    const v4, 0xf31b

    invoke-static {p1, v4, v2, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 104
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 105
    const-string v5, "audio_action"

    const/16 v6, 0x3eb

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 106
    const v5, 0xf31c

    invoke-static {p1, v5, v4, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 108
    new-instance v5, Landroid/content/Intent;

    const-class v6, Lflipboard/service/audio/MediaPlayerService;

    invoke-direct {v5, p1, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    const-string v6, "audio_action"

    const/16 v7, 0x3ed

    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 110
    const v6, 0xf31e

    invoke-static {p1, v6, v5, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    .line 112
    iget-boolean v6, p0, Lflipboard/notifications/MediaPlayerNotification;->h:Z

    if-eqz v6, :cond_8

    .line 113
    const v1, 0x7f020042

    const-string v5, ""

    invoke-virtual {v3, v1, v5, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 114
    const v1, 0x7f02003f

    const-string v4, ""

    invoke-virtual {v3, v1, v4, v0}, Landroid/support/v4/app/NotificationCompat$Builder;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 115
    const v0, 0x7f02003d

    const-string v1, ""

    invoke-virtual {v3, v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 122
    :goto_6
    invoke-virtual {v3}, Landroid/support/v4/app/NotificationCompat$Builder;->c()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 53
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 59
    :cond_3
    iget-object v0, p0, Lflipboard/notifications/MediaPlayerNotification;->g:Lflipboard/service/audio/Song;

    iget-object v0, v0, Lflipboard/service/audio/Song;->a:Ljava/lang/String;

    goto/16 :goto_1

    .line 66
    :cond_4
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto/16 :goto_2

    .line 68
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lflipboard/notifications/MediaPlayerNotification;->e:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 75
    :cond_6
    const-string v1, ""

    goto/16 :goto_4

    .line 76
    :cond_7
    const-string v1, ""

    goto/16 :goto_5

    .line 117
    :cond_8
    const v0, 0x7f020043

    const-string v4, ""

    invoke-virtual {v3, v0, v4, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 118
    const v0, 0x7f020040

    const-string v4, ""

    invoke-virtual {v3, v0, v4, v1}, Landroid/support/v4/app/NotificationCompat$Builder;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    .line 119
    const v0, 0x7f02003d

    const-string v1, ""

    invoke-virtual {v3, v0, v1, v2}, Landroid/support/v4/app/NotificationCompat$Builder;->a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    goto :goto_6
.end method
