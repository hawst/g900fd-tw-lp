.class public abstract Lflipboard/notifications/FLNotification;
.super Ljava/lang/Object;
.source "FLNotification.java"


# static fields
.field public static a:I

.field public static final b:Lflipboard/util/Log;


# instance fields
.field public c:I

.field public d:Landroid/app/PendingIntent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x7

    sput v0, Lflipboard/notifications/FLNotification;->a:I

    .line 36
    const-string v0, "notification"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/notifications/FLNotification;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lflipboard/notifications/FLNotification;->c:I

    .line 43
    return-void
.end method

.method protected static a(Lflipboard/notifications/FLNotification$NotificationThumb;FF)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 84
    const/4 v0, 0x0

    .line 85
    if-eqz p0, :cond_0

    const-wide/16 v2, 0x7530

    invoke-virtual {p0, v2, v3}, Lflipboard/notifications/FLNotification$NotificationThumb;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    invoke-virtual {p0}, Lflipboard/notifications/FLNotification$NotificationThumb;->a()Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 87
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v1, p2

    if-gez v1, :cond_1

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v1, p1

    if-gez v1, :cond_1

    .line 88
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 97
    :cond_0
    :goto_0
    return-object v0

    .line 90
    :cond_1
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, p1

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 92
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v1

    float-to-int v2, v2

    .line 93
    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v1

    float-to-int v1, v1

    .line 94
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Lflipboard/io/BitmapManager$Handle;->a(IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 78
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 79
    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 80
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Landroid/app/Notification;
.end method

.method public b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 66
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 67
    invoke-virtual {p0, p1}, Lflipboard/notifications/FLNotification;->c(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v1

    .line 68
    if-eqz v1, :cond_0

    .line 69
    iget v2, p0, Lflipboard/notifications/FLNotification;->c:I

    invoke-virtual {v0, v2, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 71
    :cond_0
    return-void
.end method

.method public final c(Landroid/content/Context;)Landroid/app/Notification;
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lflipboard/notifications/FLNotification;->a(Landroid/content/Context;)Landroid/app/Notification;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    iget-object v1, p0, Lflipboard/notifications/FLNotification;->d:Landroid/app/PendingIntent;

    iput-object v1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 54
    :cond_0
    return-object v0
.end method
