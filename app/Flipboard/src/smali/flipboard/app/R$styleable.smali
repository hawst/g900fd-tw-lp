.class public final Lflipboard/app/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# static fields
.field public static final AbsHListView:[I

.field public static final AbsHListView_android_cacheColorHint:I = 0x3

.field public static final AbsHListView_android_choiceMode:I = 0x4

.field public static final AbsHListView_android_drawSelectorOnTop:I = 0x1

.field public static final AbsHListView_android_listSelector:I = 0x0

.field public static final AbsHListView_android_scrollingCache:I = 0x2

.field public static final AbsHListView_android_smoothScrollbar:I = 0x5

.field public static final AbsHListView_hlv_stackFromRight:I = 0x6

.field public static final AbsHListView_hlv_transcriptMode:I = 0x7

.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final CarouselView:[I

.field public static final CarouselView_itemAspectRatio:I = 0x0

.field public static final CarouselView_itemScale:I = 0x1

.field public static final CarouselView_itemSpacing:I = 0x2

.field public static final CarouselView_showIndicatorStrip:I = 0x3

.field public static final ExpandableHListView:[I

.field public static final ExpandableHListView_hlv_childDivider:I = 0x2

.field public static final ExpandableHListView_hlv_childIndicator:I = 0x4

.field public static final ExpandableHListView_hlv_childIndicatorGravity:I = 0x1

.field public static final ExpandableHListView_hlv_childIndicatorPaddingLeft:I = 0x7

.field public static final ExpandableHListView_hlv_childIndicatorPaddingTop:I = 0x8

.field public static final ExpandableHListView_hlv_groupIndicator:I = 0x3

.field public static final ExpandableHListView_hlv_indicatorGravity:I = 0x0

.field public static final ExpandableHListView_hlv_indicatorPaddingLeft:I = 0x5

.field public static final ExpandableHListView_hlv_indicatorPaddingTop:I = 0x6

.field public static final FLCameleonImageView:[I

.field public static final FLCameleonImageView_checkedColor:I = 0x3

.field public static final FLCameleonImageView_checkedPressedColor:I = 0x4

.field public static final FLCameleonImageView_defaultColor:I = 0x1

.field public static final FLCameleonImageView_pressedColor:I = 0x2

.field public static final FLCameleonImageView_singleState:I = 0x0

.field public static final FLEditText:[I

.field public static final FLEditText_showClearButton:I = 0x0

.field public static final FLExpandableLayout:[I

.field public static final FLExpandableLayout_animationDurationInMillis:I = 0x2

.field public static final FLExpandableLayout_expandHandler:I = 0x0

.field public static final FLExpandableLayout_viewToExpand:I = 0x1

.field public static final FLImageView:[I

.field public static final FLImageView_align:I = 0x3

.field public static final FLImageView_clipRound:I = 0x4

.field public static final FLImageView_fade:I = 0x2

.field public static final FLImageView_placeholder:I = 0x0

.field public static final FLImageView_recycle:I = 0x1

.field public static final FLLabelTextView:[I

.field public static final FLLabelTextView_android_gravity:I = 0x2

.field public static final FLLabelTextView_android_shadowColor:I = 0x4

.field public static final FLLabelTextView_android_shadowDx:I = 0x5

.field public static final FLLabelTextView_android_shadowDy:I = 0x6

.field public static final FLLabelTextView_android_shadowRadius:I = 0x7

.field public static final FLLabelTextView_android_text:I = 0x3

.field public static final FLLabelTextView_android_textColor:I = 0x1

.field public static final FLLabelTextView_android_textSize:I = 0x0

.field public static final FLLabelTextView_fontweight:I = 0x9

.field public static final FLLabelTextView_hideWhenEllipsizing:I = 0x8

.field public static final FLLinearLayout:[I

.field public static final FLLinearLayout_maxHeight:I = 0x0

.field public static final FLStaticTextView:[I

.field public static final FLStaticTextView_android_ellipsize:I = 0x2

.field public static final FLStaticTextView_android_gravity:I = 0x3

.field public static final FLStaticTextView_android_lineSpacingMultiplier:I = 0x10

.field public static final FLStaticTextView_android_lines:I = 0xb

.field public static final FLStaticTextView_android_maxLines:I = 0xa

.field public static final FLStaticTextView_android_padding:I = 0x4

.field public static final FLStaticTextView_android_paddingBottom:I = 0x8

.field public static final FLStaticTextView_android_paddingLeft:I = 0x5

.field public static final FLStaticTextView_android_paddingRight:I = 0x7

.field public static final FLStaticTextView_android_paddingTop:I = 0x6

.field public static final FLStaticTextView_android_shadowColor:I = 0xc

.field public static final FLStaticTextView_android_shadowDx:I = 0xd

.field public static final FLStaticTextView_android_shadowDy:I = 0xe

.field public static final FLStaticTextView_android_shadowRadius:I = 0xf

.field public static final FLStaticTextView_android_text:I = 0x9

.field public static final FLStaticTextView_android_textColor:I = 0x1

.field public static final FLStaticTextView_android_textSize:I = 0x0

.field public static final FLStaticTextView_fontweight:I = 0x14

.field public static final FLStaticTextView_justification:I = 0x13

.field public static final FLStaticTextView_paragraphIndent:I = 0x12

.field public static final FLStaticTextView_textAlignment:I = 0x11

.field public static final FLTextView:[I

.field public static final FLTextView_fontweight:I = 0x0

.field public static final HListView:[I

.field public static final HListView_android_divider:I = 0x1

.field public static final HListView_android_entries:I = 0x0

.field public static final HListView_hlv_dividerWidth:I = 0x2

.field public static final HListView_hlv_footerDividersEnabled:I = 0x4

.field public static final HListView_hlv_headerDividersEnabled:I = 0x3

.field public static final HListView_hlv_measureWithChild:I = 0x7

.field public static final HListView_hlv_overScrollFooter:I = 0x6

.field public static final HListView_hlv_overScrollHeader:I = 0x5

.field public static final IconBar:[I

.field public static final IconBar_iconHeight:I = 0x1

.field public static final IconBar_iconSpacing:I = 0x2

.field public static final IconBar_iconWidth:I = 0x0

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final MetricBar:[I

.field public static final MetricBar_nameFontWeight:I = 0x4

.field public static final MetricBar_nameTextColor:I = 0x0

.field public static final MetricBar_nameTextSize:I = 0x2

.field public static final MetricBar_valueFontWeight:I = 0x5

.field public static final MetricBar_valueTextColor:I = 0x1

.field public static final MetricBar_valueTextSize:I = 0x3

.field public static final StickyListHeadersListView:[I

.field public static final StickyListHeadersListView_android_cacheColorHint:I = 0xd

.field public static final StickyListHeadersListView_android_choiceMode:I = 0x10

.field public static final StickyListHeadersListView_android_clipToPadding:I = 0x8

.field public static final StickyListHeadersListView_android_divider:I = 0xe

.field public static final StickyListHeadersListView_android_dividerHeight:I = 0xf

.field public static final StickyListHeadersListView_android_drawSelectorOnTop:I = 0xa

.field public static final StickyListHeadersListView_android_fadingEdgeLength:I = 0x7

.field public static final StickyListHeadersListView_android_fastScrollAlwaysVisible:I = 0x13

.field public static final StickyListHeadersListView_android_fastScrollEnabled:I = 0x11

.field public static final StickyListHeadersListView_android_listSelector:I = 0x9

.field public static final StickyListHeadersListView_android_overScrollMode:I = 0x12

.field public static final StickyListHeadersListView_android_padding:I = 0x1

.field public static final StickyListHeadersListView_android_paddingBottom:I = 0x5

.field public static final StickyListHeadersListView_android_paddingLeft:I = 0x2

.field public static final StickyListHeadersListView_android_paddingRight:I = 0x4

.field public static final StickyListHeadersListView_android_paddingTop:I = 0x3

.field public static final StickyListHeadersListView_android_requiresFadingEdge:I = 0x14

.field public static final StickyListHeadersListView_android_scrollbarStyle:I = 0x0

.field public static final StickyListHeadersListView_android_scrollbars:I = 0x6

.field public static final StickyListHeadersListView_android_scrollingCache:I = 0xb

.field public static final StickyListHeadersListView_android_transcriptMode:I = 0xc

.field public static final StickyListHeadersListView_hasStickyHeaders:I = 0x15

.field public static final StickyListHeadersListView_isDrawingListUnderStickyHeader:I = 0x16

.field public static final TabStripStatic:[I

.field public static final TabStripStatic_bottomDividerColor:I = 0x1

.field public static final TabStripStatic_bottomDividerWidth:I = 0x3

.field public static final TabStripStatic_indicatorLineMargin:I = 0x5

.field public static final TabStripStatic_itemBottomPadding:I = 0x4

.field public static final TabStripStatic_showDividers:I = 0x2

.field public static final TabStripStatic_underlineColor:I = 0x0

.field public static final Theme_Helpshift:[I

.field public static final Theme_Helpshift_hs__actionBarTabTextStyle:I = 0x2

.field public static final Theme_Helpshift_hs__actionbarCompatItemBaseStyle:I = 0x1

.field public static final Theme_Helpshift_hs__actionbarCompatProgressIndicatorStyle:I = 0x4

.field public static final Theme_Helpshift_hs__actionbarCompatTitleStyle:I = 0x0

.field public static final Theme_Helpshift_hs__selectableItemBackground:I = 0x3

.field public static final WalletFragmentOptions:[I

.field public static final WalletFragmentOptions_appTheme:I = 0x0

.field public static final WalletFragmentOptions_environment:I = 0x1

.field public static final WalletFragmentOptions_fragmentMode:I = 0x3

.field public static final WalletFragmentOptions_fragmentStyle:I = 0x2

.field public static final WalletFragmentStyle:[I

.field public static final WalletFragmentStyle_buyButtonAppearance:I = 0x3

.field public static final WalletFragmentStyle_buyButtonHeight:I = 0x0

.field public static final WalletFragmentStyle_buyButtonText:I = 0x2

.field public static final WalletFragmentStyle_buyButtonWidth:I = 0x1

.field public static final WalletFragmentStyle_maskedWalletDetailsBackground:I = 0x6

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonBackground:I = 0x8

.field public static final WalletFragmentStyle_maskedWalletDetailsButtonTextAppearance:I = 0x7

.field public static final WalletFragmentStyle_maskedWalletDetailsHeaderTextAppearance:I = 0x5

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoImageType:I = 0xa

.field public static final WalletFragmentStyle_maskedWalletDetailsLogoTextColor:I = 0x9

.field public static final WalletFragmentStyle_maskedWalletDetailsTextAppearance:I = 0x4

.field public static final com_facebook_friend_picker_fragment:[I

.field public static final com_facebook_friend_picker_fragment_multi_select:I = 0x0

.field public static final com_facebook_login_view:[I

.field public static final com_facebook_login_view_confirm_logout:I = 0x0

.field public static final com_facebook_login_view_fetch_user_info:I = 0x1

.field public static final com_facebook_login_view_login_text:I = 0x2

.field public static final com_facebook_login_view_logout_text:I = 0x3

.field public static final com_facebook_picker_fragment:[I

.field public static final com_facebook_picker_fragment_done_button_background:I = 0x6

.field public static final com_facebook_picker_fragment_done_button_text:I = 0x4

.field public static final com_facebook_picker_fragment_extra_fields:I = 0x1

.field public static final com_facebook_picker_fragment_show_pictures:I = 0x0

.field public static final com_facebook_picker_fragment_show_title_bar:I = 0x2

.field public static final com_facebook_picker_fragment_title_bar_background:I = 0x5

.field public static final com_facebook_picker_fragment_title_text:I = 0x3

.field public static final com_facebook_place_picker_fragment:[I

.field public static final com_facebook_place_picker_fragment_radius_in_meters:I = 0x0

.field public static final com_facebook_place_picker_fragment_results_limit:I = 0x1

.field public static final com_facebook_place_picker_fragment_search_text:I = 0x2

.field public static final com_facebook_place_picker_fragment_show_search_box:I = 0x3

.field public static final com_facebook_profile_picture_view:[I

.field public static final com_facebook_profile_picture_view_is_cropped:I = 0x1

.field public static final com_facebook_profile_picture_view_preset_size:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x3

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4418
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lflipboard/app/R$styleable;->AbsHListView:[I

    .line 4504
    new-array v0, v5, [I

    fill-array-data v0, :array_1

    sput-object v0, Lflipboard/app/R$styleable;->AdsAttrs:[I

    .line 4565
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lflipboard/app/R$styleable;->CarouselView:[I

    .line 4652
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lflipboard/app/R$styleable;->ExpandableHListView:[I

    .line 4829
    new-array v0, v6, [I

    fill-array-data v0, :array_4

    sput-object v0, Lflipboard/app/R$styleable;->FLCameleonImageView:[I

    .line 4917
    new-array v0, v3, [I

    const v1, 0x7f01001d

    aput v1, v0, v2

    sput-object v0, Lflipboard/app/R$styleable;->FLEditText:[I

    .line 4948
    new-array v0, v5, [I

    fill-array-data v0, :array_5

    sput-object v0, Lflipboard/app/R$styleable;->FLExpandableLayout:[I

    .line 5003
    new-array v0, v6, [I

    fill-array-data v0, :array_6

    sput-object v0, Lflipboard/app/R$styleable;->FLImageView:[I

    .line 5105
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lflipboard/app/R$styleable;->FLLabelTextView:[I

    .line 5196
    new-array v0, v3, [I

    const v1, 0x7f010027

    aput v1, v0, v2

    sput-object v0, Lflipboard/app/R$styleable;->FLLinearLayout:[I

    .line 5265
    const/16 v0, 0x15

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lflipboard/app/R$styleable;->FLStaticTextView:[I

    .line 5447
    new-array v0, v3, [I

    const v1, 0x7f01002a

    aput v1, v0, v2

    sput-object v0, Lflipboard/app/R$styleable;->FLTextView:[I

    .line 5488
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lflipboard/app/R$styleable;->HListView:[I

    .line 5600
    new-array v0, v5, [I

    fill-array-data v0, :array_a

    sput-object v0, Lflipboard/app/R$styleable;->IconBar:[I

    .line 5687
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lflipboard/app/R$styleable;->MapAttrs:[I

    .line 5915
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lflipboard/app/R$styleable;->MetricBar:[I

    .line 6059
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lflipboard/app/R$styleable;->StickyListHeadersListView:[I

    .line 6241
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lflipboard/app/R$styleable;->TabStripStatic:[I

    .line 6355
    new-array v0, v6, [I

    fill-array-data v0, :array_f

    sput-object v0, Lflipboard/app/R$styleable;->Theme_Helpshift:[I

    .line 6425
    new-array v0, v4, [I

    fill-array-data v0, :array_10

    sput-object v0, Lflipboard/app/R$styleable;->WalletFragmentOptions:[I

    .line 6520
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    sput-object v0, Lflipboard/app/R$styleable;->WalletFragmentStyle:[I

    .line 6707
    new-array v0, v3, [I

    const v1, 0x7f010064

    aput v1, v0, v2

    sput-object v0, Lflipboard/app/R$styleable;->com_facebook_friend_picker_fragment:[I

    .line 6740
    new-array v0, v4, [I

    fill-array-data v0, :array_12

    sput-object v0, Lflipboard/app/R$styleable;->com_facebook_login_view:[I

    .line 6821
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_13

    sput-object v0, Lflipboard/app/R$styleable;->com_facebook_picker_fragment:[I

    .line 6931
    new-array v0, v4, [I

    fill-array-data v0, :array_14

    sput-object v0, Lflipboard/app/R$styleable;->com_facebook_place_picker_fragment:[I

    .line 7002
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_15

    sput-object v0, Lflipboard/app/R$styleable;->com_facebook_profile_picture_view:[I

    return-void

    .line 4418
    :array_0
    .array-data 4
        0x10100fb
        0x10100fc
        0x10100fe
        0x1010101
        0x101012b
        0x1010231
        0x7f010006
        0x7f010007
    .end array-data

    .line 4504
    :array_1
    .array-data 4
        0x7f010008
        0x7f010009
        0x7f01000a
    .end array-data

    .line 4565
    :array_2
    .array-data 4
        0x7f01000b
        0x7f01000c
        0x7f01000d
        0x7f01000e
    .end array-data

    .line 4652
    :array_3
    .array-data 4
        0x7f01000f
        0x7f010010
        0x7f010011
        0x7f010012
        0x7f010013
        0x7f010014
        0x7f010015
        0x7f010016
        0x7f010017
    .end array-data

    .line 4829
    :array_4
    .array-data 4
        0x7f010018
        0x7f010019
        0x7f01001a
        0x7f01001b
        0x7f01001c
    .end array-data

    .line 4948
    :array_5
    .array-data 4
        0x7f01001e
        0x7f01001f
        0x7f010020
    .end array-data

    .line 5003
    :array_6
    .array-data 4
        0x7f010021
        0x7f010022
        0x7f010023
        0x7f010024
        0x7f010025
    .end array-data

    .line 5105
    :array_7
    .array-data 4
        0x1010095
        0x1010098
        0x10100af
        0x101014f
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x7f010026
        0x7f01002a
    .end array-data

    .line 5265
    :array_8
    .array-data 4
        0x1010095
        0x1010098
        0x10100ab
        0x10100af
        0x10100d5
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
        0x101014f
        0x1010153
        0x1010154
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x1010218
        0x7f010005
        0x7f010028
        0x7f010029
        0x7f01002a
    .end array-data

    .line 5488
    :array_9
    .array-data 4
        0x10100b2
        0x1010129
        0x7f01002b
        0x7f01002c
        0x7f01002d
        0x7f01002e
        0x7f01002f
        0x7f010030
    .end array-data

    .line 5600
    :array_a
    .array-data 4
        0x7f010031
        0x7f010032
        0x7f010033
    .end array-data

    .line 5687
    :array_b
    .array-data 4
        0x7f010034
        0x7f010035
        0x7f010036
        0x7f010037
        0x7f010038
        0x7f010039
        0x7f01003a
        0x7f01003b
        0x7f01003c
        0x7f01003d
        0x7f01003e
        0x7f01003f
        0x7f010040
        0x7f010041
    .end array-data

    .line 5915
    :array_c
    .array-data 4
        0x7f010042
        0x7f010043
        0x7f010044
        0x7f010045
        0x7f010046
        0x7f010047
    .end array-data

    .line 6059
    :array_d
    .array-data 4
        0x101007f
        0x10100d5
        0x10100d6
        0x10100d7
        0x10100d8
        0x10100d9
        0x10100de
        0x10100e0
        0x10100eb
        0x10100fb
        0x10100fc
        0x10100fe
        0x1010100
        0x1010101
        0x1010129
        0x101012a
        0x101012b
        0x1010226
        0x10102c1
        0x1010335
        0x10103a5
        0x7f010048
        0x7f010049
    .end array-data

    .line 6241
    :array_e
    .array-data 4
        0x7f01004a
        0x7f01004b
        0x7f01004c
        0x7f01004d
        0x7f01004e
        0x7f01004f
    .end array-data

    .line 6355
    :array_f
    .array-data 4
        0x7f010050
        0x7f010051
        0x7f010052
        0x7f010053
        0x7f010054
    .end array-data

    .line 6425
    :array_10
    .array-data 4
        0x7f010055
        0x7f010056
        0x7f010057
        0x7f010058
    .end array-data

    .line 6520
    :array_11
    .array-data 4
        0x7f010059
        0x7f01005a
        0x7f01005b
        0x7f01005c
        0x7f01005d
        0x7f01005e
        0x7f01005f
        0x7f010060
        0x7f010061
        0x7f010062
        0x7f010063
    .end array-data

    .line 6740
    :array_12
    .array-data 4
        0x7f010065
        0x7f010066
        0x7f010067
        0x7f010068
    .end array-data

    .line 6821
    :array_13
    .array-data 4
        0x7f010069
        0x7f01006a
        0x7f01006b
        0x7f01006c
        0x7f01006d
        0x7f01006e
        0x7f01006f
    .end array-data

    .line 6931
    :array_14
    .array-data 4
        0x7f010070
        0x7f010071
        0x7f010072
        0x7f010073
    .end array-data

    .line 7002
    :array_15
    .array-data 4
        0x7f010074
        0x7f010075
    .end array-data
.end method
