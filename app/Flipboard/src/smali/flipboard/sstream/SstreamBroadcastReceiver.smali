.class public Lflipboard/sstream/SstreamBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SstreamBroadcastReceiver.java"


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "sstream"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/sstream/SstreamBroadcastReceiver;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/sstream/SstreamBroadcastReceiver;->b:Ljava/lang/String;

    return-void
.end method

.method private a(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 199
    if-eqz p2, :cond_0

    .line 202
    new-instance v0, Lflipboard/sstream/SstreamBroadcastReceiver$3;

    invoke-direct {v0, p0, p1, p2, p3}, Lflipboard/sstream/SstreamBroadcastReceiver$3;-><init>(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lflipboard/service/User;->a(Lflipboard/service/Flap$TypedResultObserver;)V

    .line 220
    :cond_0
    return-void
.end method

.method private a(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/User;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 111
    invoke-virtual {p1}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 112
    if-eqz p2, :cond_1

    iget-object v0, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    const-string v0, "sstream"

    invoke-direct {p0, p1, p5, v0}, Lflipboard/sstream/SstreamBroadcastReceiver;->a(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v2, "Userid synced from api is the same as app userid. Add new service %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p5, v3, v1

    invoke-static {v0, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-static {v7, p2}, Lflipboard/util/Log;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    sget-object v2, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v3, "Userid synced from api is %s, but app already has userid %s, fire SStream intent to sync app userid %s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v0, v4, v1

    iget-object v0, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x2

    iget-object v1, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    aput-object v1, v4, v0

    invoke-static {v2, v3, v4}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 120
    iget-object v0, p1, Lflipboard/service/User;->d:Ljava/lang/String;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    :cond_2
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v2, "App does not have userid yet. Setting it up ..."

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lflipboard/sstream/SstreamBroadcastReceiver;->b:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 128
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->n()V

    .line 131
    :cond_3
    if-eqz p2, :cond_6

    const-string v0, "0"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 132
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/service/User;)V

    .line 133
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const-string v2, "uid-prefs"

    invoke-virtual {v0, v2, v1}, Lflipboard/app/FlipboardApplication;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 134
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "tuuid"

    invoke-interface {v2, v3, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 135
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "udid"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 136
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "Sstream"

    const-string v3, "Sstream"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iput-object p4, v0, Lflipboard/service/FlipboardManager;->I:Ljava/lang/String;

    .line 137
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "Sstream"

    const-string v3, "Sstream"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    iput-object p3, v0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 138
    :cond_5
    invoke-virtual {p1, p2}, Lflipboard/service/User;->a(Ljava/lang/String;)V

    .line 140
    new-instance v0, Lflipboard/sstream/SstreamBroadcastReceiver$2;

    invoke-direct {v0, p0, p1, p5}, Lflipboard/sstream/SstreamBroadcastReceiver$2;-><init>(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lflipboard/service/User;->a(Lflipboard/util/Observer;)V

    .line 151
    sget-object v0, Lflipboard/sstream/SstreamBroadcastReceiver;->a:Lflipboard/util/Log;

    .line 154
    :cond_6
    if-eqz p6, :cond_0

    invoke-interface {p6}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 156
    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->A()Lflipboard/model/ConfigFirstLaunch;

    move-result-object v2

    .line 160
    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, v2, Lflipboard/model/ConfigFirstLaunch;->SectionsToChooseFrom:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/model/FirstRunSection;

    iget-object v4, v0, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p6, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    new-instance v4, Lflipboard/service/Section;

    iget-object v5, v0, Lflipboard/model/FirstRunSection;->title:Ljava/lang/String;

    invoke-direct {v4, v0, v5}, Lflipboard/service/Section;-><init>(Lflipboard/model/FirstRunSection;Ljava/lang/String;)V

    const/4 v0, 0x1

    const/4 v5, 0x1

    const-string v6, "sstream"

    invoke-virtual {p1, v4, v0, v5, v6}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZZLjava/lang/String;)Lflipboard/service/Section;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lflipboard/service/Section;->B:Z

    add-int/lit8 v0, v1, 0x1

    :goto_2
    move v1, v0

    goto :goto_1

    :cond_7
    if-ge v1, v7, :cond_0

    .line 161
    :cond_8
    const-string v0, "sstreamFallbackDefault"

    invoke-virtual {p1, v2, v1, v0}, Lflipboard/service/User;->a(Lflipboard/model/ConfigFirstLaunch;ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 164
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3}, Lflipboard/sstream/SstreamBroadcastReceiver;->a(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct/range {p0 .. p6}, Lflipboard/sstream/SstreamBroadcastReceiver;->a(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v7, 0x6

    const/4 v10, 0x0

    .line 51
    const-string v0, "uid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    const-string v0, "tuuid"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 54
    const-string v0, "logged_in_to_service"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 55
    const-string v0, "oauth_token"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/sstream/SstreamBroadcastReceiver;->b:Ljava/lang/String;

    .line 57
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 58
    const-string v0, "sync_categories"

    invoke-virtual {p2, v0, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "news_stream_categories"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 67
    :cond_0
    invoke-static {v7, v4}, Lflipboard/util/Log;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lflipboard/sstream/SstreamBroadcastReceiver;->b:Ljava/lang/String;

    invoke-static {v7, v1}, Lflipboard/util/Log;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 69
    sget-object v3, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v7, "api broadcast received, uid: %s, tuuid %s, serviceName %s, oauthToken %s"

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v2, v8, v10

    const/4 v9, 0x1

    aput-object v0, v8, v9

    const/4 v0, 0x2

    aput-object v5, v8, v0

    const/4 v0, 0x3

    aput-object v1, v8, v0

    invoke-static {v3, v7, v8}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 71
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 73
    iget-object v0, p0, Lflipboard/sstream/SstreamBroadcastReceiver;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 75
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v2, "Converting token to userid, session id tuple ..."

    new-array v3, v10, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/sstream/SstreamBroadcastReceiver;->b:Ljava/lang/String;

    new-instance v3, Lflipboard/sstream/SstreamBroadcastReceiver$1;

    invoke-direct {v3, p0, v1, v5, v6}, Lflipboard/sstream/SstreamBroadcastReceiver$1;-><init>(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;Ljava/util/List;)V

    new-instance v4, Lflipboard/service/Flap$ConvertTokenRequest;

    invoke-direct {v4, v0, v1}, Lflipboard/service/Flap$ConvertTokenRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v4, v2, v3}, Lflipboard/service/Flap$ConvertTokenRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ConvertTokenRequest;

    .line 106
    :goto_1
    return-void

    .line 99
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v4, :cond_2

    .line 100
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v3, "Received userid without authtoken, session id tuple, now sync user ..."

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {v0, v3, v7}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v0, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lflipboard/sstream/SstreamBroadcastReceiver;->a(Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    goto :goto_1

    .line 103
    :cond_2
    sget-object v0, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v1, "no user info found to sync user state"

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
