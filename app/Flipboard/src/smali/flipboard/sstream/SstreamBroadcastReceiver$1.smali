.class Lflipboard/sstream/SstreamBroadcastReceiver$1;
.super Ljava/lang/Object;
.source "SstreamBroadcastReceiver.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/service/User;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/util/List;

.field final synthetic d:Lflipboard/sstream/SstreamBroadcastReceiver;


# direct methods
.method constructor <init>(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->d:Lflipboard/sstream/SstreamBroadcastReceiver;

    iput-object p2, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->a:Lflipboard/service/User;

    iput-object p3, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->c:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 82
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    new-array v0, v5, [Ljava/lang/Object;

    aput-object p1, v0, v6

    .line 84
    const-string v0, "userid"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 85
    const-string v0, "udid"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 86
    const-string v0, "tuuid"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 88
    sget-object v0, Lflipboard/util/Log$Level;->b:Lflipboard/util/Log$Level;

    const-string v1, "ConvertToken success. Userid is %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-static {v0, v1, v5}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 90
    iget-object v0, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->d:Lflipboard/sstream/SstreamBroadcastReceiver;

    iget-object v1, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->a:Lflipboard/service/User;

    iget-object v5, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->b:Ljava/lang/String;

    iget-object v6, p0, Lflipboard/sstream/SstreamBroadcastReceiver$1;->c:Ljava/util/List;

    invoke-static/range {v0 .. v6}, Lflipboard/sstream/SstreamBroadcastReceiver;->a(Lflipboard/sstream/SstreamBroadcastReceiver;Lflipboard/service/User;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V

    .line 91
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 95
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "ConvertToken failure %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    sget-object v0, Lflipboard/util/Log$Level;->e:Lflipboard/util/Log$Level;

    const-string v1, "ConvertToken failure %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lflipboard/util/Log;->a(Lflipboard/util/Log$Level;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 97
    return-void
.end method
