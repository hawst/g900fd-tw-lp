.class public Lflipboard/abtest/MurmurHash;
.super Ljava/lang/Object;
.source "MurmurHash.java"


# direct methods
.method private static a(J)I
    .locals 6

    .prologue
    const v4, 0x5bd1e995

    .line 115
    long-to-int v0, p0

    mul-int/2addr v0, v4

    .line 121
    ushr-int/lit8 v1, v0, 0x18

    xor-int/2addr v0, v1

    .line 122
    mul-int/2addr v0, v4

    xor-int/lit8 v0, v0, 0x0

    .line 124
    const/16 v1, 0x20

    shr-long v2, p0, v1

    long-to-int v1, v2

    mul-int/2addr v1, v4

    .line 125
    ushr-int/lit8 v2, v1, 0x18

    xor-int/2addr v1, v2

    .line 126
    mul-int/2addr v0, v4

    .line 127
    mul-int/2addr v1, v4

    xor-int/2addr v0, v1

    .line 129
    ushr-int/lit8 v1, v0, 0xd

    xor-int/2addr v0, v1

    .line 130
    mul-int/2addr v0, v4

    .line 131
    ushr-int/lit8 v1, v0, 0xf

    xor-int/2addr v0, v1

    .line 133
    return v0
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    const v8, 0x5bd1e995

    .line 33
    move-object v0, p0

    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    .line 44
    :goto_1
    return v0

    .line 35
    :cond_0
    instance-of v2, v0, Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 36
    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lflipboard/abtest/MurmurHash;->a(J)I

    move-result v0

    goto :goto_1

    .line 37
    :cond_1
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 38
    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lflipboard/abtest/MurmurHash;->a(J)I

    move-result v0

    goto :goto_1

    .line 39
    :cond_2
    instance-of v2, v0, Ljava/lang/Double;

    if-eqz v2, :cond_3

    .line 40
    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToRawLongBits(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Lflipboard/abtest/MurmurHash;->a(J)I

    move-result v0

    goto :goto_1

    .line 41
    :cond_3
    instance-of v2, v0, Ljava/lang/Float;

    if-eqz v2, :cond_4

    .line 42
    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->floatToRawIntBits(F)I

    move-result v0

    int-to-long v0, v0

    invoke-static {v0, v1}, Lflipboard/abtest/MurmurHash;->a(J)I

    move-result v0

    goto :goto_1

    .line 43
    :cond_4
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_a

    .line 44
    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v3, v2

    xor-int/lit8 v0, v3, -0x1

    shr-int/lit8 v4, v3, 0x2

    :goto_2
    if-ge v1, v4, :cond_5

    shl-int/lit8 v5, v1, 0x2

    add-int/lit8 v6, v5, 0x3

    aget-byte v6, v2, v6

    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v7, v5, 0x2

    aget-byte v7, v2, v7

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    add-int/lit8 v7, v5, 0x1

    aget-byte v7, v2, v7

    and-int/lit16 v7, v7, 0xff

    or-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    aget-byte v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v5, v6

    mul-int/2addr v5, v8

    ushr-int/lit8 v6, v5, 0x18

    xor-int/2addr v5, v6

    mul-int/2addr v5, v8

    mul-int/2addr v0, v8

    xor-int/2addr v0, v5

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    shl-int/lit8 v1, v4, 0x2

    sub-int v1, v3, v1

    if-eqz v1, :cond_9

    const/4 v4, 0x3

    if-lt v1, v4, :cond_6

    add-int/lit8 v4, v3, -0x3

    aget-byte v4, v2, v4

    shl-int/lit8 v4, v4, 0x10

    xor-int/2addr v0, v4

    :cond_6
    const/4 v4, 0x2

    if-lt v1, v4, :cond_7

    add-int/lit8 v4, v3, -0x2

    aget-byte v4, v2, v4

    shl-int/lit8 v4, v4, 0x8

    xor-int/2addr v0, v4

    :cond_7
    if-lez v1, :cond_8

    add-int/lit8 v1, v3, -0x1

    aget-byte v1, v2, v1

    xor-int/2addr v0, v1

    :cond_8
    mul-int/2addr v0, v8

    :cond_9
    ushr-int/lit8 v1, v0, 0xd

    xor-int/2addr v0, v1

    mul-int/2addr v0, v8

    ushr-int/lit8 v1, v0, 0xf

    xor-int/2addr v0, v1

    goto/16 :goto_1

    .line 45
    :cond_a
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
