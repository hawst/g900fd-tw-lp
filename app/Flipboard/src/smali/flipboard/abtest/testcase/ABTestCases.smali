.class public Lflipboard/abtest/testcase/ABTestCases;
.super Ljava/lang/Object;
.source "ABTestCases.java"


# direct methods
.method public static a(IF)I
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/16 v6, 0x64

    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 26
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v2, v2, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 30
    :cond_1
    invoke-static {p1, v3, v4}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v2

    .line 31
    cmpl-float v3, v2, v3

    if-eqz v3, :cond_0

    .line 33
    cmpl-float v3, v2, v4

    if-nez v3, :cond_2

    move v0, v1

    .line 34
    goto :goto_0

    .line 36
    :cond_2
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->H:Ljava/lang/String;

    .line 37
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 38
    rsub-int/lit8 v4, v2, 0x64

    add-int/lit8 v4, v4, 0x1

    .line 39
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5, v3, v6, v4, v6}, Lflipboard/abtest/PseudoRandom;->a(Ljava/lang/Integer;Ljava/lang/String;III)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 40
    :goto_1
    if-ne v1, v0, :cond_5

    const/16 v5, 0x32

    if-gt v2, v5, :cond_5

    .line 41
    add-int/lit8 v1, v4, -0x1

    .line 42
    mul-int/lit8 v2, v2, 0x2

    rsub-int/lit8 v2, v2, 0x64

    add-int/lit8 v2, v2, 0x1

    .line 43
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v4, v3, v6, v2, v1}, Lflipboard/abtest/PseudoRandom;->a(Ljava/lang/Integer;Ljava/lang/String;III)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x3

    .line 45
    :cond_3
    :goto_2
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lflipboard/abtest/Experiments;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    move v1, v0

    .line 39
    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method
