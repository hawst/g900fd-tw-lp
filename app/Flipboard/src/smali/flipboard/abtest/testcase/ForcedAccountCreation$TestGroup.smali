.class public final enum Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;
.super Ljava/lang/Enum;
.source "ForcedAccountCreation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

.field public static final enum b:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

.field public static final enum c:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

.field public static final enum d:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

.field private static final synthetic i:[Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 14
    new-instance v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    const-string v1, "NOT_FORCED_GROUP_1"

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/16 v4, 0xa

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->a:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    .line 15
    new-instance v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    const-string v1, "NOT_FORCED_GROUP_2"

    const/4 v2, 0x1

    const/16 v3, 0x29

    const/16 v4, 0x32

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->b:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    .line 16
    new-instance v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    const-string v1, "FORCED_GROUP_1"

    const/4 v2, 0x2

    const/16 v3, 0x51

    const/16 v4, 0x5a

    const/4 v5, 0x3

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->c:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    .line 17
    new-instance v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    const-string v1, "FORCED_GROUP_2"

    const/4 v2, 0x3

    const/16 v3, 0x5b

    const/16 v4, 0x64

    const/4 v5, 0x4

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->d:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    .line 13
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    const/4 v1, 0x0

    sget-object v2, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->a:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->b:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->c:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->d:Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->i:[Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZ)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->e:I

    .line 26
    iput p4, p0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->f:I

    .line 27
    iput p5, p0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->g:I

    .line 28
    iput-boolean p6, p0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->h:Z

    .line 29
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    return-object v0
.end method

.method public static values()[Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->i:[Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    invoke-virtual {v0}, [Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/abtest/testcase/ForcedAccountCreation$TestGroup;

    return-object v0
.end method
