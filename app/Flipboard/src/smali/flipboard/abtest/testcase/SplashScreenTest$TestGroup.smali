.class public final enum Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;
.super Ljava/lang/Enum;
.source "SplashScreenTest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

.field public static final enum b:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

.field public static final enum c:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

.field public static final enum d:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

.field private static final synthetic i:[Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    const/4 v13, 0x4

    const/4 v12, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 13
    new-instance v0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    const-string v1, "NO_PICKER_1"

    move v5, v3

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->a:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    .line 14
    new-instance v5, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    const-string v6, "NO_PICKER_2"

    const/16 v8, 0x20

    const/16 v9, 0x21

    move v7, v3

    move v10, v4

    move v11, v2

    invoke-direct/range {v5 .. v11}, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v5, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->b:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    .line 15
    new-instance v5, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    const-string v6, "SHOW_PICKER_1"

    const/16 v8, 0x41

    const/16 v9, 0x42

    move v7, v4

    move v10, v12

    move v11, v3

    invoke-direct/range {v5 .. v11}, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v5, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->c:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    .line 16
    new-instance v5, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    const-string v6, "SHOW_PICKER_2"

    const/16 v8, 0x63

    const/16 v9, 0x64

    move v7, v12

    move v10, v13

    move v11, v3

    invoke-direct/range {v5 .. v11}, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v5, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->d:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    .line 12
    new-array v0, v13, [Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    sget-object v1, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->a:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->b:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->c:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->d:Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    aput-object v1, v0, v12

    sput-object v0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->i:[Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZ)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput p3, p0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->e:I

    .line 27
    iput p4, p0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->f:I

    .line 28
    iput p5, p0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->g:I

    .line 29
    iput-boolean p6, p0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->h:Z

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    return-object v0
.end method

.method public static values()[Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->i:[Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    invoke-virtual {v0}, [Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/abtest/testcase/SplashScreenTest$TestGroup;

    return-object v0
.end method
