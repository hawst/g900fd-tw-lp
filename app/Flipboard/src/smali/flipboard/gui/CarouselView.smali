.class public Lflipboard/gui/CarouselView;
.super Lflipboard/gui/FLViewGroup;
.source "CarouselView.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field private a:Landroid/support/v4/view/ViewPager;

.field private b:Lflipboard/gui/PagerIndicatorStrip;

.field private c:Lflipboard/activities/FlipboardActivity;

.field private d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/CarouselView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    invoke-direct {p0, p1, p2}, Lflipboard/gui/CarouselView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 68
    invoke-direct {p0, p1, p2}, Lflipboard/gui/CarouselView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, -0x2

    const/4 v6, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 72
    move-object v0, p1

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iput-object v0, p0, Lflipboard/gui/CarouselView;->c:Lflipboard/activities/FlipboardActivity;

    .line 77
    invoke-static {v6, p1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    .line 79
    if-eqz p2, :cond_1

    .line 80
    sget-object v3, Lflipboard/app/R$styleable;->CarouselView:[I

    invoke-virtual {p1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 81
    invoke-virtual {v5, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    invoke-static {v3, v6, v2}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v4

    .line 82
    const/4 v3, 0x1

    invoke-virtual {v5, v3, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v3

    invoke-static {v3, v6, v2}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v3

    .line 83
    invoke-virtual {v5, v8, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 84
    const/4 v0, 0x3

    invoke-virtual {v5, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    .line 85
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 87
    :goto_0
    iput v4, p0, Lflipboard/gui/CarouselView;->e:F

    .line 88
    iput v3, p0, Lflipboard/gui/CarouselView;->f:F

    .line 91
    invoke-virtual {p0, v1, v1, v1, v1}, Lflipboard/gui/CarouselView;->setPadding(IIII)V

    .line 93
    new-instance v1, Landroid/support/v4/view/ViewPager;

    invoke-direct {v1, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    .line 94
    iget-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 95
    iget-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v8}, Landroid/support/v4/view/ViewPager;->setOverScrollMode(I)V

    .line 96
    iget-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 97
    iget-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0, v1}, Lflipboard/gui/CarouselView;->addView(Landroid/view/View;)V

    .line 99
    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Lflipboard/gui/PagerIndicatorStrip;

    invoke-direct {v0, p1}, Lflipboard/gui/PagerIndicatorStrip;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    .line 101
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lflipboard/gui/PagerIndicatorStrip;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    invoke-virtual {p0, v0}, Lflipboard/gui/CarouselView;->addView(Landroid/view/View;)V

    .line 104
    :cond_0
    return-void

    :cond_1
    move v3, v2

    move v4, v2

    move v2, v0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->a(I)V

    .line 174
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    if-eqz v0, :cond_0

    .line 161
    int-to-float v0, p1

    add-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 162
    iget-object v1, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    invoke-virtual {v1, v0}, Lflipboard/gui/PagerIndicatorStrip;->setSelectedIndex(I)V

    .line 164
    :cond_0
    iget-object v0, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->a(IFI)V

    .line 167
    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    invoke-interface {v0, p1}, Landroid/support/v4/view/ViewPager$OnPageChangeListener;->b(I)V

    .line 181
    :cond_0
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 197
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lflipboard/gui/FLViewGroup;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 189
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/CarouselView;->c:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->s()V

    goto :goto_0

    .line 194
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/CarouselView;->c:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->t()V

    goto :goto_0

    .line 186
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/16 v4, 0x11

    const/4 v3, 0x0

    .line 119
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    sub-int v1, p4, p2

    invoke-static {v0, v3, v3, v1, v4}, Lflipboard/gui/CarouselView;->a(Landroid/view/View;IIII)I

    move-result v0

    .line 120
    sub-int v1, p5, p3

    mul-int/lit8 v2, v0, 0x2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 121
    iget-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    sub-int v2, p4, p2

    invoke-static {v1, v0, v3, v2, v4}, Lflipboard/gui/CarouselView;->a(Landroid/view/View;IIII)I

    .line 122
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 108
    iget v0, p0, Lflipboard/gui/CarouselView;->f:F

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v6, v0

    .line 109
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    if-eqz v0, :cond_0

    .line 110
    iget-object v1, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/CarouselView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 111
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    invoke-virtual {v0}, Lflipboard/gui/PagerIndicatorStrip;->getMeasuredHeight()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    sub-int v0, v6, v0

    .line 113
    :goto_0
    iget-object v1, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/view/ViewPager;->measure(II)V

    .line 114
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLViewGroup;->onMeasure(II)V

    .line 115
    return-void

    :cond_0
    move v0, v6

    goto :goto_0
.end method

.method public setAdapter(Lflipboard/gui/CarouselView$CarouselAdapter;)V
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 130
    iget-object v0, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 131
    return-void
.end method

.method public setIndicatorCount(I)V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    invoke-virtual {v0, p1}, Lflipboard/gui/PagerIndicatorStrip;->setIndicatorCount(I)V

    .line 136
    iget-object v0, p0, Lflipboard/gui/CarouselView;->b:Lflipboard/gui/PagerIndicatorStrip;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/PagerIndicatorStrip;->setSelectedIndex(I)V

    .line 138
    :cond_0
    return-void
.end method

.method public setOnPageChangedListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lflipboard/gui/CarouselView;->d:Landroid/support/v4/view/ViewPager$OnPageChangeListener;

    .line 156
    return-void
.end method

.method public final setPageTransformer$382b7817(Landroid/support/v4/view/ViewPager$PageTransformer;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 147
    iget-object v3, p0, Lflipboard/gui/CarouselView;->a:Landroid/support/v4/view/ViewPager;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-lt v2, v4, :cond_1

    iget-object v2, v3, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/ViewPager$PageTransformer;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-eq v1, v2, :cond_0

    move v0, v1

    :cond_0
    iput-object p1, v3, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/ViewPager$PageTransformer;

    invoke-virtual {v3, v1}, Landroid/support/v4/view/ViewPager;->setChildrenDrawingOrderEnabledCompat(Z)V

    const/4 v1, 0x2

    iput v1, v3, Landroid/support/v4/view/ViewPager;->b:I

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->b()V

    .line 148
    :cond_1
    return-void

    :cond_2
    move v2, v0

    .line 147
    goto :goto_0
.end method
