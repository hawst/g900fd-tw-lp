.class public Lflipboard/gui/personal/TocDrawerListFragment$Holder$$ViewInjector;
.super Ljava/lang/Object;
.source "TocDrawerListFragment$Holder$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/personal/TocDrawerListFragment$Holder;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a036b

    const-string v1, "field \'text\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->a:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a036a

    const-string v1, "field \'image\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->b:Lflipboard/gui/FLImageView;

    .line 14
    return-void
.end method

.method public static reset(Lflipboard/gui/personal/TocDrawerListFragment$Holder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->a:Lflipboard/gui/FLTextView;

    .line 18
    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->b:Lflipboard/gui/FLImageView;

    .line 19
    return-void
.end method
