.class public Lflipboard/gui/personal/ServiceListFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "ServiceListFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/personal/ServiceListFragment;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004c

    const-string v1, "field \'actionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p1, Lflipboard/gui/personal/ServiceListFragment;->a:Lflipboard/gui/actionbar/FLActionBar;

    .line 12
    const v0, 0x7f0a0103

    const-string v1, "field \'serviceList\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/EditableListView;

    iput-object v0, p1, Lflipboard/gui/personal/ServiceListFragment;->b:Lflipboard/gui/EditableListView;

    .line 14
    const v0, 0x7f0a00ff

    const-string v1, "field \'signOutButton\' and method \'onSignOutClicked\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 15
    check-cast v0, Landroid/widget/Button;

    iput-object v0, p1, Lflipboard/gui/personal/ServiceListFragment;->c:Landroid/widget/Button;

    .line 16
    new-instance v0, Lflipboard/gui/personal/ServiceListFragment$$ViewInjector$1;

    invoke-direct {v0, p1}, Lflipboard/gui/personal/ServiceListFragment$$ViewInjector$1;-><init>(Lflipboard/gui/personal/ServiceListFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 24
    return-void
.end method

.method public static reset(Lflipboard/gui/personal/ServiceListFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27
    iput-object v0, p0, Lflipboard/gui/personal/ServiceListFragment;->a:Lflipboard/gui/actionbar/FLActionBar;

    .line 28
    iput-object v0, p0, Lflipboard/gui/personal/ServiceListFragment;->b:Lflipboard/gui/EditableListView;

    .line 29
    iput-object v0, p0, Lflipboard/gui/personal/ServiceListFragment;->c:Landroid/widget/Button;

    .line 30
    return-void
.end method
