.class public Lflipboard/gui/personal/NotificationsFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "NotificationsFragment.java"

# interfaces
.implements Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field a:Landroid/widget/ListView;

.field b:Lflipboard/gui/FLTextView;

.field c:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Lflipboard/gui/ContentDrawerListItemAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lflipboard/gui/personal/NotificationsFragment$AccountLoginViewClickHandler;

.field private i:Lflipboard/gui/personal/NotificationsFragment$SectionsAndAccountObserver;

.field private m:Landroid/widget/FrameLayout;

.field private n:Lflipboard/service/Section;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 53
    iput-boolean v1, p0, Lflipboard/gui/personal/NotificationsFragment;->d:Z

    .line 54
    iput-boolean v1, p0, Lflipboard/gui/personal/NotificationsFragment;->e:Z

    .line 55
    iput v1, p0, Lflipboard/gui/personal/NotificationsFragment;->f:I

    .line 58
    new-instance v0, Lflipboard/gui/personal/NotificationsFragment$SectionsAndAccountObserver;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/personal/NotificationsFragment$SectionsAndAccountObserver;-><init>(Lflipboard/gui/personal/NotificationsFragment;B)V

    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->i:Lflipboard/gui/personal/NotificationsFragment$SectionsAndAccountObserver;

    .line 74
    return-void
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 85
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 87
    if-eqz v0, :cond_2

    .line 88
    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 89
    const v0, 0x7f0300e1

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 90
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 91
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setEnabled(Z)V

    .line 92
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v0, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 93
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    new-array v3, v3, [I

    const v4, 0x7f08000b

    aput v4, v3, v5

    invoke-virtual {v0, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 94
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->d()Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->n:Lflipboard/service/Section;

    .line 96
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->n:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 97
    new-instance v3, Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-direct {v3, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v3, p0, Lflipboard/gui/personal/NotificationsFragment;->g:Lflipboard/gui/ContentDrawerListItemAdapter;

    .line 98
    invoke-direct {p0}, Lflipboard/gui/personal/NotificationsFragment;->b()V

    .line 99
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->n:Lflipboard/service/Section;

    invoke-direct {p0, v0}, Lflipboard/gui/personal/NotificationsFragment;->a(Lflipboard/service/Section;)V

    .line 100
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->a:Landroid/widget/ListView;

    iget-object v3, p0, Lflipboard/gui/personal/NotificationsFragment;->g:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 101
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 103
    :cond_0
    iput-object v2, p0, Lflipboard/gui/personal/NotificationsFragment;->h:Lflipboard/gui/personal/NotificationsFragment$AccountLoginViewClickHandler;

    move-object v0, v1

    .line 113
    :goto_0
    return-object v0

    .line 105
    :cond_1
    const v0, 0x7f0300eb

    invoke-virtual {p1, v0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 107
    const v0, 0x7f0a00f0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 108
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v3, 0x7f0d0230

    invoke-virtual {v2, v3}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    new-instance v0, Lflipboard/gui/personal/NotificationsFragment$AccountLoginViewClickHandler;

    invoke-direct {v0, p0}, Lflipboard/gui/personal/NotificationsFragment$AccountLoginViewClickHandler;-><init>(Lflipboard/gui/personal/NotificationsFragment;)V

    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->h:Lflipboard/gui/personal/NotificationsFragment$AccountLoginViewClickHandler;

    .line 110
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->h:Lflipboard/gui/personal/NotificationsFragment$AccountLoginViewClickHandler;

    invoke-static {v0, v1}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/personal/NotificationsFragment;Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lflipboard/gui/personal/NotificationsFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/personal/NotificationsFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->m:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 322
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lflipboard/activities/GenericFragmentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 323
    const-string v1, "fragment_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 324
    const v1, 0x7f0d00ac

    invoke-virtual {p0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 325
    const-string v2, "fragment_title"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 326
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 329
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget v1, v0, Lflipboard/service/User;->o:I

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput v1, v0, Lflipboard/service/User;->o:I

    sget-object v1, Lflipboard/service/User$Message;->k:Lflipboard/service/User$Message;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lflipboard/service/User;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 330
    :cond_0
    return-void
.end method

.method private a(Lflipboard/service/Section;)V
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 132
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 133
    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 135
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 136
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 140
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/model/ConfigSetting;->DisablePeerToPeerSharing:Z

    if-nez v1, :cond_0

    .line 142
    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    const v2, 0x7f0d02f0

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    const v2, 0x7f0d00ac

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v3}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 150
    if-eqz v0, :cond_3

    .line 151
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 153
    iget-object v4, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 154
    iget-object v4, v0, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    const-string v10, "sharedwith"

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 155
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 157
    :cond_2
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    :cond_3
    new-instance v10, Lflipboard/objs/ConfigSection;

    invoke-direct {v10}, Lflipboard/objs/ConfigSection;-><init>()V

    .line 165
    const-string v0, "content_guide_shared_with_you"

    iput-object v0, v10, Lflipboard/objs/ConfigSection;->bR:Ljava/lang/String;

    .line 166
    const-string v1, "auth/flipboard/curator/magazine/sharedwithyou"

    .line 167
    iput-object v1, v10, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    .line 168
    const v0, 0x7f0d0344

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v10, v0}, Lflipboard/objs/ContentDrawerListItemBase;->a(Ljava/lang/String;)V

    .line 171
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 172
    if-nez v0, :cond_4

    .line 174
    new-instance v0, Lflipboard/service/Section;

    const-string v2, ""

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 175
    iput-boolean v5, v0, Lflipboard/service/Section;->k:Z

    .line 176
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 179
    :cond_4
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    invoke-virtual {p1}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-nez v0, :cond_5

    .line 182
    new-instance v0, Lflipboard/gui/personal/NotificationsFragment$2;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/personal/NotificationsFragment$2;-><init>(Lflipboard/gui/personal/NotificationsFragment;Lflipboard/service/Section;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    :cond_5
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_6

    move v5, v6

    .line 202
    :cond_6
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 203
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/model/ConfigSetting;->DisablePeerToPeerSharing:Z

    if-nez v1, :cond_7

    .line 204
    invoke-interface {v0, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 206
    :cond_7
    invoke-interface {v0, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 207
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/personal/NotificationsFragment$3;

    invoke-direct {v2, p0, v0, v5, v7}, Lflipboard/gui/personal/NotificationsFragment$3;-><init>(Lflipboard/gui/personal/NotificationsFragment;Ljava/util/List;ZLandroid/content/res/Resources;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 220
    return-void
.end method

.method static synthetic b(Lflipboard/gui/personal/NotificationsFragment;)Lflipboard/gui/ContentDrawerListItemAdapter;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->g:Lflipboard/gui/ContentDrawerListItemAdapter;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 333
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->n:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 334
    iput-boolean v1, p0, Lflipboard/gui/personal/NotificationsFragment;->d:Z

    .line 335
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->n:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->d(Z)Z

    .line 336
    return-void
.end method

.method static synthetic c(Lflipboard/gui/personal/NotificationsFragment;)V
    .locals 2

    .prologue
    .line 52
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/NotificationsFragment$1;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/NotificationsFragment$1;-><init>(Lflipboard/gui/personal/NotificationsFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 340
    invoke-direct {p0}, Lflipboard/gui/personal/NotificationsFragment;->b()V

    .line 341
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 52
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    invoke-virtual {p1}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v3

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iput-boolean v1, p0, Lflipboard/gui/personal/NotificationsFragment;->d:Z

    :cond_1
    if-eqz v3, :cond_2

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {p1, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Ljava/util/List;)V

    :cond_2
    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_4

    invoke-direct {p0, p1}, Lflipboard/gui/personal/NotificationsFragment;->a(Lflipboard/service/Section;)V

    iget-boolean v0, p0, Lflipboard/gui/personal/NotificationsFragment;->e:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v0, :cond_3

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/NotificationsFragment$5;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/NotificationsFragment$5;-><init>(Lflipboard/gui/personal/NotificationsFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_3
    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lflipboard/gui/personal/NotificationsFragment;->f:I

    if-ne v0, v1, :cond_4

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/NotificationsFragment$4;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/NotificationsFragment$4;-><init>(Lflipboard/gui/personal/NotificationsFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_4
    :goto_1
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lflipboard/gui/personal/NotificationsFragment;->e:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lflipboard/gui/personal/NotificationsFragment;->e:Z

    iget-object v0, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/NotificationsFragment;->f:I

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 306
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 307
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/personal/NotificationsFragment;->i:Lflipboard/gui/personal/NotificationsFragment$SectionsAndAccountObserver;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 308
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lflipboard/gui/personal/NotificationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->m:Landroid/widget/FrameLayout;

    .line 80
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->m:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lflipboard/gui/personal/NotificationsFragment;->m:Landroid/widget/FrameLayout;

    invoke-direct {p0, p1, v1}, Lflipboard/gui/personal/NotificationsFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 81
    iget-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->m:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 313
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 314
    iget-boolean v0, p0, Lflipboard/gui/personal/NotificationsFragment;->d:Z

    if-eqz v0, :cond_0

    .line 315
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->d()Lflipboard/service/Section;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 316
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/personal/NotificationsFragment;->d:Z

    .line 318
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/personal/NotificationsFragment;->i:Lflipboard/gui/personal/NotificationsFragment$SectionsAndAccountObserver;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V

    .line 319
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 225
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    .line 226
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v2

    if-nez v2, :cond_0

    if-eq v1, v4, :cond_0

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 229
    :cond_1
    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 230
    check-cast v0, Lflipboard/objs/ConfigSection;

    .line 231
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/ConfigSection;)V

    .line 232
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 233
    invoke-virtual {p0}, Lflipboard/gui/personal/NotificationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_0

    .line 235
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 236
    const-string v3, "source"

    const-string v4, "userSectionList"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    invoke-virtual {v1, v0, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 238
    invoke-virtual {p0, v0}, Lflipboard/gui/personal/NotificationsFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 241
    :cond_2
    check-cast v0, Lflipboard/objs/FeedItem;

    .line 242
    iget-object v1, v0, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    const-string v2, "follow"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 243
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedSectionLink;

    .line 244
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 245
    new-instance v2, Lflipboard/service/Section;

    invoke-direct {v2, v1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 246
    new-instance v1, Lflipboard/activities/SocialCardFragment;

    invoke-direct {v1}, Lflipboard/activities/SocialCardFragment;-><init>()V

    .line 247
    invoke-virtual {v1, v0}, Lflipboard/activities/SocialCardFragment;->a(Lflipboard/objs/FeedItem;)V

    .line 248
    iput-object v2, v1, Lflipboard/activities/SocialCardFragment;->m:Lflipboard/service/Section;

    .line 249
    iput-boolean v4, v1, Lflipboard/activities/SocialCardFragment;->p:Z

    .line 250
    const v0, 0x7f0e0018

    invoke-virtual {v1, v0}, Lflipboard/activities/SocialCardFragment;->a(I)V

    .line 251
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 252
    sget-object v2, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v2, :cond_3

    .line 253
    iput-boolean v4, v1, Lflipboard/activities/SocialCardFragment;->u:Z

    .line 257
    :goto_1
    invoke-virtual {p0}, Lflipboard/gui/personal/NotificationsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->b()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f0a004e

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const-string v1, "social_card"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->a(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    goto/16 :goto_0

    .line 255
    :cond_3
    iput-boolean v3, v1, Lflipboard/activities/SocialCardFragment;->u:Z

    goto :goto_1

    .line 258
    :cond_4
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 259
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 260
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    .line 261
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 262
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 263
    const-string v2, "source"

    const-string v3, "contentGuide"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v2, "rootGroupIdentifier"

    const-string v3, "notifications"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    invoke-virtual {p0}, Lflipboard/gui/personal/NotificationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 266
    invoke-virtual {p0, v0}, Lflipboard/gui/personal/NotificationsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
