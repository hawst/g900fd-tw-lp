.class public Lflipboard/gui/personal/TocSubEmptyStateView$$ViewInjector;
.super Ljava/lang/Object;
.source "TocSubEmptyStateView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/personal/TocSubEmptyStateView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004f

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/personal/TocSubEmptyStateView;->a:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a0180

    const-string v1, "field \'bodyTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/personal/TocSubEmptyStateView;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a017f

    const-string v1, "field \'backgroundImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/personal/TocSubEmptyStateView;->c:Lflipboard/gui/FLImageView;

    .line 16
    const v0, 0x7f0a0181

    const-string v1, "field \'buttonView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p1, Lflipboard/gui/personal/TocSubEmptyStateView;->d:Lflipboard/gui/FLButton;

    .line 18
    const v0, 0x7f0a0375

    const-string v1, "field \'peopleIcon\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p1, Lflipboard/gui/personal/TocSubEmptyStateView;->e:Landroid/widget/FrameLayout;

    .line 20
    return-void
.end method

.method public static reset(Lflipboard/gui/personal/TocSubEmptyStateView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    iput-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->a:Lflipboard/gui/FLTextView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->b:Lflipboard/gui/FLTextView;

    .line 25
    iput-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->c:Lflipboard/gui/FLImageView;

    .line 26
    iput-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->d:Lflipboard/gui/FLButton;

    .line 27
    iput-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->e:Landroid/widget/FrameLayout;

    .line 28
    return-void
.end method
