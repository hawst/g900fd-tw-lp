.class Lflipboard/gui/personal/TocGridFragment$3;
.super Ljava/lang/Object;
.source "TocGridFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/personal/TocGridFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/personal/TocGridFragment;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v0}, Lflipboard/gui/personal/TocGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->b(Lflipboard/gui/personal/TocGridFragment;)Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b(I)Lflipboard/service/Section;

    move-result-object v1

    .line 158
    iget-object v0, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v0, :cond_1

    .line 159
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v2}, Lflipboard/gui/personal/TocGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 160
    const-string v2, "service"

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->m:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const-string v1, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginSocialPane"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 162
    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v1, v0}, Lflipboard/gui/personal/TocGridFragment;->startActivity(Landroid/content/Intent;)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    const-string v2, "topic_picker"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 165
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v1}, Lflipboard/gui/personal/TocGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0xb

    invoke-static {v1, v2, v3}, Lflipboard/activities/GenericFragmentActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TocGridFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 167
    :cond_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 168
    sget-object v2, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v2, :cond_3

    .line 169
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->c(Lflipboard/gui/personal/TocGridFragment;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v1}, Lflipboard/gui/personal/TocGridFragment;->b(Lflipboard/gui/personal/TocGridFragment;)Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b(I)Lflipboard/service/Section;

    move-result-object v1

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->a:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Ljava/lang/String;)V

    goto :goto_0

    .line 170
    :cond_3
    sget-object v2, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v2, :cond_0

    .line 171
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 172
    const-string v0, "source"

    const-string v3, "contentGuide"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    const-string v0, "rootGroupIdentifier"

    const-string v3, "profile"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$3;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v0}, Lflipboard/gui/personal/TocGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ContentDrawerActivity;

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    goto :goto_0
.end method
