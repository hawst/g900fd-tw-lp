.class public Lflipboard/gui/personal/TabStripStatic;
.super Landroid/widget/LinearLayout;
.source "TabStripStatic.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field a:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field e:Landroid/content/res/TypedArray;

.field private f:I

.field private g:I

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:I

.field private k:F

.field private l:I

.field private m:Landroid/support/v4/view/ViewPager;

.field private n:Landroid/graphics/Paint;

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 41
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    .line 42
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    .line 42
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    .line 60
    invoke-virtual {p0, v3}, Lflipboard/gui/personal/TabStripStatic;->setOrientation(I)V

    .line 61
    invoke-virtual {p0, v3}, Lflipboard/gui/personal/TabStripStatic;->setWillNotDraw(Z)V

    .line 63
    sget-object v0, Lflipboard/app/R$styleable;->TabStripStatic:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    .line 65
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->h:Landroid/graphics/Paint;

    .line 66
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->h:Landroid/graphics/Paint;

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 67
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    .line 68
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->h:Landroid/graphics/Paint;

    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 70
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->l:I

    .line 72
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->n:Landroid/graphics/Paint;

    .line 73
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->n:Landroid/graphics/Paint;

    iget-object v1, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    const/4 v2, 0x3

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    .line 75
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->n:Landroid/graphics/Paint;

    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 76
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->n:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 78
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->i:Landroid/graphics/Paint;

    .line 80
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->i:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    const/high16 v0, 0x41000000    # 8.0f

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->k:F

    .line 82
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->i:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 83
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 85
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    .line 42
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    .line 44
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    .line 89
    return-void
.end method

.method static synthetic a(Lflipboard/gui/personal/TabStripStatic;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->m:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(IZ)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 157
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 159
    instance-of v1, v0, Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 160
    if-eqz p2, :cond_1

    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->d:I

    .line 161
    :goto_0
    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1}, Lflipboard/util/ColorFilterUtil;->b(I)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 164
    :cond_0
    return-void

    .line 160
    :cond_1
    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->c:I

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public final a(IFI)V
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 140
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v1

    iput v1, p0, Lflipboard/gui/personal/TabStripStatic;->f:I

    .line 141
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->g:I

    .line 142
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->j:I

    .line 143
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->invalidate()V

    .line 145
    int-to-float v0, p1

    add-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 146
    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    if-eq v0, v1, :cond_1

    .line 147
    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 148
    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lflipboard/gui/personal/TabStripStatic;->a(IZ)V

    .line 150
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lflipboard/gui/personal/TabStripStatic;->a(IZ)V

    .line 151
    iput v0, p0, Lflipboard/gui/personal/TabStripStatic;->o:I

    .line 153
    :cond_1
    return-void
.end method

.method public final a(Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;Landroid/support/v4/view/ViewPager;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 92
    iput-object p1, p0, Lflipboard/gui/personal/TabStripStatic;->a:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

    .line 93
    iput-object p2, p0, Lflipboard/gui/personal/TabStripStatic;->m:Landroid/support/v4/view/ViewPager;

    .line 94
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->removeAllViews()V

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    .line 96
    invoke-virtual {p1}, Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;->c()I

    move-result v4

    .line 97
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090135

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 98
    const/4 v1, 0x2

    if-le v4, v1, :cond_0

    .line 99
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :cond_0
    move v2, v3

    .line 101
    :goto_0
    if-ge v2, v4, :cond_2

    .line 103
    invoke-virtual {p1, v2}, Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;->c(I)I

    move-result v1

    .line 104
    if-nez v1, :cond_1

    .line 107
    invoke-virtual {p1, v2}, Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;->b(I)Ljava/lang/CharSequence;

    move-result-object v5

    .line 108
    new-instance v1, Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;)V

    .line 109
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v6, v6, Lflipboard/service/FlipboardManager;->w:Landroid/graphics/Typeface;

    invoke-virtual {v1, v6}, Lflipboard/gui/FLTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 110
    const/16 v6, 0x11

    invoke-virtual {v1, v6}, Lflipboard/gui/FLTextView;->setGravity(I)V

    .line 111
    invoke-virtual {v1, v5}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    invoke-virtual {v1, v3, v0}, Lflipboard/gui/FLTextView;->a(II)V

    .line 124
    :goto_1
    iget-object v5, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    const/4 v6, 0x4

    invoke-virtual {v5, v6, v3}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v5

    invoke-virtual {v1, v3, v3, v3, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 126
    new-instance v5, Lflipboard/gui/personal/TabStripStatic$1;

    invoke-direct {v5, p0, v2}, Lflipboard/gui/personal/TabStripStatic$1;-><init>(Lflipboard/gui/personal/TabStripStatic;I)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    iget-object v5, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    invoke-direct {v5, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 133
    const/high16 v6, 0x3f800000    # 1.0f

    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 134
    invoke-virtual {p0, v1, v5}, Lflipboard/gui/personal/TabStripStatic;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 101
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 117
    :cond_1
    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 118
    invoke-virtual {p1, v2}, Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;->c(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    .line 136
    :cond_2
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 168
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 172
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 174
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/personal/TabStripStatic;->h:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    div-float/2addr v1, v6

    sub-float v2, v0, v1

    .line 175
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->e:Landroid/content/res/TypedArray;

    const/4 v1, 0x5

    iget v3, p0, Lflipboard/gui/personal/TabStripStatic;->l:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 176
    iget-object v1, p0, Lflipboard/gui/personal/TabStripStatic;->a:Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;

    invoke-virtual {v1}, Lflipboard/gui/personal/TabStripStatic$ImageFragmentPagerAdapter;->c()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    .line 177
    iget v1, p0, Lflipboard/gui/personal/TabStripStatic;->f:I

    iget v3, p0, Lflipboard/gui/personal/TabStripStatic;->j:I

    add-int/2addr v1, v3

    int-to-float v1, v1

    add-float/2addr v1, v0

    iget v3, p0, Lflipboard/gui/personal/TabStripStatic;->f:I

    iget v4, p0, Lflipboard/gui/personal/TabStripStatic;->g:I

    add-int/2addr v3, v4

    iget v4, p0, Lflipboard/gui/personal/TabStripStatic;->j:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    sub-float/2addr v3, v0

    iget-object v5, p0, Lflipboard/gui/personal/TabStripStatic;->h:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 179
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/personal/TabStripStatic;->n:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    div-float/2addr v1, v6

    sub-float v2, v0, v1

    .line 180
    const/4 v1, 0x0

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lflipboard/gui/personal/TabStripStatic;->n:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->i:Landroid/graphics/Paint;

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 185
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    add-int/lit8 v0, v7, -0x1

    if-ge v6, v0, :cond_1

    .line 186
    iget-object v0, p0, Lflipboard/gui/personal/TabStripStatic;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 187
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 188
    int-to-float v1, v0

    iget v2, p0, Lflipboard/gui/personal/TabStripStatic;->k:F

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/personal/TabStripStatic;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lflipboard/gui/personal/TabStripStatic;->k:F

    sub-float v4, v0, v4

    iget-object v5, p0, Lflipboard/gui/personal/TabStripStatic;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 185
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 191
    :cond_1
    return-void
.end method
