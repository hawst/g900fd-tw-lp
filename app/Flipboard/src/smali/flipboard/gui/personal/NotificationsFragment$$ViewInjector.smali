.class public Lflipboard/gui/personal/NotificationsFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "NotificationsFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/personal/NotificationsFragment;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a0046

    const-string v1, "field \'listView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p1, Lflipboard/gui/personal/NotificationsFragment;->a:Landroid/widget/ListView;

    .line 12
    const v0, 0x7f0a028d

    const-string v1, "field \'emptyStateText\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/personal/NotificationsFragment;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a028c

    const-string v1, "field \'swipeRefreshLayout\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v0, p1, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 16
    return-void
.end method

.method public static reset(Lflipboard/gui/personal/NotificationsFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->a:Landroid/widget/ListView;

    .line 20
    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->b:Lflipboard/gui/FLTextView;

    .line 21
    iput-object v0, p0, Lflipboard/gui/personal/NotificationsFragment;->c:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 22
    return-void
.end method
