.class Lflipboard/gui/personal/TocGridFragment$5;
.super Ljava/lang/Object;
.source "TocGridFragment.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Lflipboard/gui/personal/TocGridFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/personal/TocGridFragment;)V
    .locals 0

    .prologue
    .line 226
    iput-object p1, p0, Lflipboard/gui/personal/TocGridFragment$5;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 229
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 230
    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lflipboard/json/FLObject;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/json/FLObject;

    check-cast v1, Lflipboard/json/FLObject;

    .line 232
    if-eqz v1, :cond_6

    .line 233
    const-string v2, "mediumURL"

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 234
    if-nez v2, :cond_1

    .line 235
    const-string v2, "largeURL"

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 237
    :cond_1
    if-nez v2, :cond_2

    .line 238
    const-string v2, "xlargeURL"

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 240
    :cond_2
    if-nez v2, :cond_5

    .line 241
    const-string v2, "smallURL"

    invoke-virtual {v1, v2, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 244
    :goto_1
    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 245
    iget-object v2, v0, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    .line 246
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->i()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 247
    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v1

    .line 250
    :cond_3
    invoke-static {v1}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 251
    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$5;->a:Lflipboard/gui/personal/TocGridFragment;

    iget-object v2, v2, Lflipboard/gui/personal/TocGridFragment;->a:Ljava/util/Map;

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 254
    :cond_4
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/TocGridFragment$5$1;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/TocGridFragment$5$1;-><init>(Lflipboard/gui/personal/TocGridFragment$5;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 260
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$5;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->e(Lflipboard/gui/personal/TocGridFragment;)V

    .line 261
    return-void

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v1, v3

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 266
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$5;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/32 v4, 0x927c0

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/gui/personal/TocGridFragment;->a(Lflipboard/gui/personal/TocGridFragment;J)J

    .line 267
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$5;->a:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->e(Lflipboard/gui/personal/TocGridFragment;)V

    .line 268
    return-void
.end method
