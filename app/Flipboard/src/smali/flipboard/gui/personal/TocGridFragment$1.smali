.class Lflipboard/gui/personal/TocGridFragment$1;
.super Ljava/lang/Object;
.source "TocGridFragment.java"

# interfaces
.implements Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;


# instance fields
.field a:Z

.field final synthetic b:Landroid/widget/ImageButton;

.field final synthetic c:Lflipboard/gui/personal/TocGridFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/personal/TocGridFragment;Landroid/widget/ImageButton;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    iput-object p2, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 103
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->a(Lflipboard/gui/personal/TocGridFragment;)Lorg/askerov/dynamicgrid/DynamicGridView;

    move-result-object v0

    iget-boolean v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->a(Lflipboard/gui/personal/TocGridFragment;)Lorg/askerov/dynamicgrid/DynamicGridView;

    move-result-object v0

    iput-boolean v4, v0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    .line 105
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xe1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 106
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-wide/16 v2, 0x3e8

    new-instance v1, Lflipboard/gui/personal/TocGridFragment$1$1;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/TocGridFragment$1$1;-><init>(Lflipboard/gui/personal/TocGridFragment$1;)V

    invoke-virtual {v0, v2, v3, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 111
    iget-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->a:Z

    if-eqz v0, :cond_1

    .line 112
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 114
    iget-object v0, v1, Lflipboard/service/User;->e:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    iget v2, v2, Lflipboard/gui/personal/TocGridFragment;->c:I

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 115
    invoke-virtual {v1, v0}, Lflipboard/service/User;->d(Lflipboard/service/Section;)Z

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    iget v1, v1, Lflipboard/gui/personal/TocGridFragment;->c:I

    add-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    iget v2, v2, Lflipboard/gui/personal/TocGridFragment;->b:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2, v4}, Lflipboard/service/User;->a(IIZ)Z

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 130
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->a(Lflipboard/gui/personal/TocGridFragment;)Lorg/askerov/dynamicgrid/DynamicGridView;

    move-result-object v0

    iget-boolean v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView;->b:Z

    if-eqz v0, :cond_0

    .line 131
    iget-boolean v1, p0, Lflipboard/gui/personal/TocGridFragment$1;->a:Z

    .line 132
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getRight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getTop()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getBottom()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->a:Z

    .line 133
    iget-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->a:Z

    if-eq v1, v0, :cond_0

    .line 134
    iget-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->a:Z

    if-eqz v0, :cond_2

    .line 135
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    const v1, 0x7f0200d9

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 141
    :cond_0
    :goto_1
    return-void

    .line 132
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 137
    :cond_2
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    const v1, 0x7f0200d8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getWidth()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTranslationX(F)V

    .line 126
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$1;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xe1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 127
    return-void
.end method
