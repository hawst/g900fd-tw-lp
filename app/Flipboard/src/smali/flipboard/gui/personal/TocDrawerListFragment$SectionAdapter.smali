.class Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;
.super Landroid/widget/BaseAdapter;
.source "TocDrawerListFragment.java"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/gui/personal/TocDrawerListFragment;


# direct methods
.method private constructor <init>(Lflipboard/gui/personal/TocDrawerListFragment;)V
    .locals 1

    .prologue
    .line 150
    iput-object p1, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 154
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/personal/TocDrawerListFragment;B)V
    .locals 0

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;-><init>(Lflipboard/gui/personal/TocDrawerListFragment;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/service/Section;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 158
    sget-object v0, Lflipboard/gui/personal/TocDrawerListFragment$3;->a:[I

    iget-object v2, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    iget-object v2, v2, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    invoke-virtual {v2}, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown filter: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    iget-object v2, v2, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :pswitch_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 162
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v3

    if-nez v3, :cond_0

    .line 163
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 168
    :pswitch_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 169
    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lflipboard/service/Section;->B()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 170
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 175
    :pswitch_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 176
    invoke-virtual {v0}, Lflipboard/service/Section;->y()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 177
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 184
    :cond_3
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    iget-object v0, v0, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    sget-object v2, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->b:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    iget-object v0, v0, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    sget-object v2, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->c:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    if-ne v0, v2, :cond_5

    .line 185
    :cond_4
    new-instance v0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter$1;

    invoke-direct {v0, p0}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter$1;-><init>(Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 192
    :cond_5
    iput-object v1, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a:Ljava/util/List;

    .line 193
    invoke-virtual {p0}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->notifyDataSetChanged()V

    .line 194
    return-void

    .line 158
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0, p1}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a(I)Lflipboard/service/Section;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 205
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 215
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 219
    invoke-virtual {p0, p1}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a(I)Lflipboard/service/Section;

    move-result-object v2

    .line 222
    if-eqz p2, :cond_0

    .line 224
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;

    .line 225
    iget-object v1, v0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    .line 237
    :goto_0
    iget-object v1, v0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    iget-object v1, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    iget-object v1, v1, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    sget-object v3, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->b:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    if-ne v1, v3, :cond_2

    .line 239
    iget-object v0, v0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->b:Lflipboard/gui/FLImageView;

    iget-object v1, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    invoke-virtual {v1}, Lflipboard/objs/TOCSection;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 244
    :goto_1
    return-object p2

    .line 227
    :cond_0
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    invoke-virtual {v0}, Lflipboard/gui/personal/TocDrawerListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->b:Lflipboard/gui/personal/TocDrawerListFragment;

    iget-object v1, v1, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    sget-object v3, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->b:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    if-ne v1, v3, :cond_1

    .line 229
    const v1, 0x7f030143

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 233
    :goto_2
    new-instance v1, Lflipboard/gui/personal/TocDrawerListFragment$Holder;

    invoke-direct {v1}, Lflipboard/gui/personal/TocDrawerListFragment$Holder;-><init>()V

    .line 234
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 235
    invoke-static {v1, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object p2, v0

    move-object v0, v1

    goto :goto_0

    .line 231
    :cond_1
    const v1, 0x7f030149

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    .line 242
    :cond_2
    iget-object v0, v0, Lflipboard/gui/personal/TocDrawerListFragment$Holder;->b:Lflipboard/gui/FLImageView;

    iget-object v1, v2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->a:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    return v0
.end method
