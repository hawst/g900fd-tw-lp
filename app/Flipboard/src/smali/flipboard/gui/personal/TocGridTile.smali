.class public Lflipboard/gui/personal/TocGridTile;
.super Landroid/widget/FrameLayout;
.source "TocGridTile.java"


# instance fields
.field public a:Lflipboard/gui/FLTextView;

.field public b:Lflipboard/gui/FLImageView;

.field public c:Lflipboard/gui/FLStaticTextView;

.field private d:F

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/personal/TocGridTile;->d:F

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/personal/TocGridTile;->d:F

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/personal/TocGridTile;->d:F

    .line 35
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 40
    const v0, 0x7f0a0235

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/TocGridTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/personal/TocGridTile;->a:Lflipboard/gui/FLTextView;

    .line 41
    const v0, 0x7f0a023c

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/TocGridTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/personal/TocGridTile;->b:Lflipboard/gui/FLImageView;

    .line 42
    const v0, 0x7f0a023d

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/TocGridTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/personal/TocGridTile;->c:Lflipboard/gui/FLStaticTextView;

    .line 43
    const v0, 0x7f0a0212

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/TocGridTile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/TocGridTile;->e:Landroid/view/View;

    .line 44
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 53
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/personal/TocGridTile;->d:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 54
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 55
    invoke-super {p0, p1, v1}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 57
    iget-object v2, p0, Lflipboard/gui/personal/TocGridTile;->e:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 58
    iget-object v2, p0, Lflipboard/gui/personal/TocGridTile;->e:Landroid/view/View;

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    .line 60
    :cond_0
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lflipboard/gui/personal/TocGridTile;->d:F

    .line 48
    return-void
.end method
