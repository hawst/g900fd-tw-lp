.class public Lflipboard/gui/personal/TocSubEmptyStateView;
.super Landroid/widget/FrameLayout;
.source "TocSubEmptyStateView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected a:Lflipboard/gui/FLTextView;

.field protected b:Lflipboard/gui/FLTextView;

.field protected c:Lflipboard/gui/FLImageView;

.field protected d:Lflipboard/gui/FLButton;

.field protected e:Landroid/widget/FrameLayout;

.field private f:Lflipboard/gui/personal/TocDrawerListFragment$Filter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 89
    sget-object v0, Lflipboard/gui/personal/TocSubEmptyStateView$1;->a:[I

    iget-object v1, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->f:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    invoke-virtual {v1}, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    :goto_0
    return-void

    .line 91
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/gui/personal/TocSubEmptyStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    const/16 v2, 0xb

    invoke-static {v0, v1, v2}, Lflipboard/activities/GenericFragmentActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/personal/TocSubEmptyStateView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 95
    :pswitch_1
    invoke-virtual {p0}, Lflipboard/gui/personal/TocSubEmptyStateView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 96
    invoke-virtual {p0}, Lflipboard/gui/personal/TocSubEmptyStateView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lflipboard/activities/GenericFragmentActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 97
    invoke-virtual {p0}, Lflipboard/gui/personal/TocSubEmptyStateView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 51
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 52
    iget-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->d:Lflipboard/gui/FLButton;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    return-void
.end method

.method public setType(Lflipboard/gui/personal/TocDrawerListFragment$Filter;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 58
    iput-object p1, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->f:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    .line 62
    const-string v0, ""

    .line 63
    sget-object v2, Lflipboard/gui/personal/TocSubEmptyStateView$1;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v2, v1

    move v3, v1

    .line 79
    :goto_0
    iget-object v4, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v4, v3}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 80
    iget-object v3, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v3, v1}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 81
    iget-object v1, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->d:Lflipboard/gui/FLButton;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLButton;->setText(I)V

    .line 83
    iget-object v1, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 84
    return-void

    .line 65
    :pswitch_0
    const v3, 0x7f0d028e

    .line 66
    const v1, 0x7f0d028d

    .line 67
    const v2, 0x7f0d00f0

    .line 68
    iget-object v0, p0, Lflipboard/gui/personal/TocSubEmptyStateView;->e:Landroid/widget/FrameLayout;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 69
    const-string v0, "http://cdn.flipboard.com/emptystates/empty-topics-background.jpg"

    goto :goto_0

    .line 72
    :pswitch_1
    const v3, 0x7f0d028c

    .line 73
    const v1, 0x7f0d028b

    .line 74
    const v2, 0x7f0d00ef

    .line 75
    const-string v0, "http://cdn.flipboard.com/emptystates/empty-following-background.jpg"

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
