.class public Lflipboard/gui/personal/SocialNetworksFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "SocialNetworksFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lflipboard/service/RemoteWatchedFile$Observer;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lflipboard/service/RemoteWatchedFile$Observer;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lflipboard/gui/ContentDrawerListItemAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lflipboard/service/RemoteWatchedFile;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/personal/SocialNetworksFragment;)Lflipboard/gui/ContentDrawerListItemAdapter;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->a:Lflipboard/gui/ContentDrawerListItemAdapter;

    return-object v0
.end method

.method static a(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigService;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 103
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 104
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 105
    iget-boolean v3, v0, Lflipboard/objs/ConfigService;->X:Z

    if-eqz v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v6, "flipboard"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 106
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 110
    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 114
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v7, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 115
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 116
    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v7, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v9

    .line 117
    sget-object v3, Lflipboard/service/ContentDrawerHandler;->j:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v10, v3, [Ljava/lang/Object;

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v3, v10, v2

    if-eqz v9, :cond_2

    move v3, v4

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v10, v4

    .line 118
    if-nez v9, :cond_3

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v10, "googlereader"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 119
    invoke-interface {v5, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move v3, v2

    .line 117
    goto :goto_2

    .line 120
    :cond_3
    if-eqz v9, :cond_5

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v10, "flipboard"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 121
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->m()Lflipboard/objs/ConfigService;

    move-result-object v3

    .line 122
    iget-object v10, v9, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v10}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v3, Lflipboard/objs/ConfigService;->bR:Ljava/lang/String;

    .line 123
    iput-boolean v4, v3, Lflipboard/objs/ConfigService;->bV:Z

    .line 124
    iget-object v9, v9, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v9, v9, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v3, Lflipboard/objs/ConfigService;->bQ:Ljava/lang/String;

    .line 126
    if-nez v1, :cond_4

    .line 127
    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    sget-object v9, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v10, 0x7f0d035a

    invoke-virtual {v9, v10}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9, v11}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v4

    .line 130
    :cond_4
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-interface {v5, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :cond_5
    move v0, v1

    move v1, v0

    .line 133
    goto :goto_1

    .line 136
    :cond_6
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 137
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    const v1, 0x7f0d001e

    invoke-virtual {v0, v1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    invoke-direct {v1, v0, v11}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    invoke-interface {v6, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 141
    :cond_7
    return-object v6
.end method

.method static synthetic b(Lflipboard/gui/personal/SocialNetworksFragment;)Ljava/util/List;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 41
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/User$Message;->g:Lflipboard/service/User$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/User$Message;->h:Lflipboard/service/User$Message;

    if-ne p2, v0, :cond_1

    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/SocialNetworksFragment$2;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/SocialNetworksFragment$2;-><init>(Lflipboard/gui/personal/SocialNetworksFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 94
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 5

    .prologue
    .line 77
    :try_start_0
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->j()Lflipboard/objs/ConfigServices;

    move-result-object v0

    .line 78
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/objs/ConfigServices;->b:Ljava/util/List;

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->c:Ljava/util/List;

    .line 79
    iget-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->c:Ljava/util/List;

    invoke-static {v0}, Lflipboard/gui/personal/SocialNetworksFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 80
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/personal/SocialNetworksFragment$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/personal/SocialNetworksFragment$1;-><init>(Lflipboard/gui/personal/SocialNetworksFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    return-void

    .line 86
    :catch_0
    move-exception v0

    .line 87
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "failed to parse services.json: %-E"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 88
    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 98
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 51
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 57
    const v1, 0x7f0300c0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 58
    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 59
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 60
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-direct {v2, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v2, p0, Lflipboard/gui/personal/SocialNetworksFragment;->a:Lflipboard/gui/ContentDrawerListItemAdapter;

    .line 61
    iget-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->a:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "services.json"

    invoke-virtual {v0, v2, p0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->b:Lflipboard/service/RemoteWatchedFile;

    .line 63
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 64
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 65
    return-object v1
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroyView()V

    .line 71
    iget-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->b:Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0, p0}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 72
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 73
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/personal/SocialNetworksFragment;->a:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, p3}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigService;

    .line 146
    invoke-virtual {p0}, Lflipboard/gui/personal/SocialNetworksFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 147
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    .line 148
    if-nez v2, :cond_1

    .line 149
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f0d0218

    invoke-virtual {p0, v1}, Lflipboard/gui/personal/SocialNetworksFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 166
    :goto_0
    return-void

    .line 149
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/personal/SocialNetworksFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/ServiceLoginActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "service"

    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "extra_content_discovery_from_source"

    const-string v2, "usageSocialLoginOriginSocialPane"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lflipboard/gui/personal/SocialNetworksFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 150
    :cond_1
    if-eqz v1, :cond_2

    const-string v3, "key_social_networks_is_settings"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    :cond_2
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/personal/SocialNetworksFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lflipboard/activities/ServiceSettingsActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 152
    const-string v3, "account_name"

    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 153
    const-string v3, "account_image"

    iget-object v4, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v4}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 154
    const-string v3, "account_username"

    iget-object v2, v2, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v2, v2, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const-string v2, "account_id"

    iget-object v0, v0, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    invoke-virtual {p0, v1}, Lflipboard/gui/personal/SocialNetworksFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 158
    :cond_3
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 159
    const-string v2, "google+"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 160
    const-string v1, "googleplus"

    .line 162
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getMyLists?service="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 163
    invoke-virtual {p0}, Lflipboard/gui/personal/SocialNetworksFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v0, v1, v3}, Lflipboard/util/ActivityUtil;->a(Landroid/content/Context;Lflipboard/objs/ConfigService;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
