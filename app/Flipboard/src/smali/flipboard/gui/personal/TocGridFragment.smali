.class public Lflipboard/gui/personal/TocGridFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "TocGridFragment.java"

# interfaces
.implements Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;
.implements Lflipboard/util/Observer;


# instance fields
.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:I

.field c:I

.field private d:Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

.field private e:J

.field private f:Lorg/askerov/dynamicgrid/DynamicGridView;

.field private g:Landroid/support/v4/widget/SwipeRefreshLayout;

.field private h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 74
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment;->h:Z

    .line 77
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->a:Ljava/util/Map;

    .line 78
    return-void
.end method

.method static synthetic a(Lflipboard/gui/personal/TocGridFragment;J)J
    .locals 1

    .prologue
    .line 59
    iput-wide p1, p0, Lflipboard/gui/personal/TocGridFragment;->e:J

    return-wide p1
.end method

.method static synthetic a(Lflipboard/gui/personal/TocGridFragment;)Lorg/askerov/dynamicgrid/DynamicGridView;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->f:Lorg/askerov/dynamicgrid/DynamicGridView;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/personal/TocGridFragment;)Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->d:Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/personal/TocGridFragment;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/personal/TocGridFragment;)Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment;->h:Z

    return v0
.end method

.method static synthetic e(Lflipboard/gui/personal/TocGridFragment;)V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->g:Landroid/support/v4/widget/SwipeRefreshLayout;

    iget-boolean v0, v0, Landroid/support/v4/widget/SwipeRefreshLayout;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/TocGridFragment$8;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/TocGridFragment$8;-><init>(Lflipboard/gui/personal/TocGridFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lflipboard/gui/personal/TocGridFragment;)Landroid/support/v4/widget/SwipeRefreshLayout;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->g:Landroid/support/v4/widget/SwipeRefreshLayout;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 466
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment;->h:Z

    .line 467
    invoke-virtual {p0}, Lflipboard/gui/personal/TocGridFragment;->b()V

    .line 468
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 59
    check-cast p1, Lflipboard/service/User;

    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->d:Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/TocGridFragment$4;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/personal/TocGridFragment$4;-><init>(Lflipboard/gui/personal/TocGridFragment;Lflipboard/service/User;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method final b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 208
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 209
    iget-wide v4, p0, Lflipboard/gui/personal/TocGridFragment;->e:J

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x927c0

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment;->h:Z

    if-eqz v0, :cond_2

    .line 210
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lflipboard/gui/personal/TocGridFragment;->e:J

    .line 211
    iput-boolean v1, p0, Lflipboard/gui/personal/TocGridFragment;->h:Z

    .line 212
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 213
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    .line 214
    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v0, v1

    .line 216
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 218
    add-int/lit8 v1, v0, 0x32

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 219
    invoke-interface {v2, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 220
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v5, Lflipboard/gui/personal/TocGridFragment$5;

    invoke-direct {v5, p0}, Lflipboard/gui/personal/TocGridFragment$5;-><init>(Lflipboard/gui/personal/TocGridFragment;)V

    invoke-virtual {v3, v4, v1, v5}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/User;Ljava/util/List;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$ImageRequest;

    .line 217
    add-int/lit8 v0, v0, 0x32

    goto :goto_1

    .line 223
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 83
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 84
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 88
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 89
    const v0, 0x7f03013e

    const/4 v1, 0x0

    invoke-static {v2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 90
    const v0, 0x7f0a0360

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 91
    const v1, 0x7f0a035f

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lorg/askerov/dynamicgrid/DynamicGridView;

    iput-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->f:Lorg/askerov/dynamicgrid/DynamicGridView;

    .line 92
    new-instance v4, Ljava/util/ArrayList;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 94
    invoke-interface {v4, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 95
    const v1, 0x7f0a028c

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/widget/SwipeRefreshLayout;

    iput-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->g:Landroid/support/v4/widget/SwipeRefreshLayout;

    .line 96
    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->g:Landroid/support/v4/widget/SwipeRefreshLayout;

    const/4 v5, 0x1

    new-array v5, v5, [I

    const v6, 0x7f08000b

    aput v6, v5, v7

    invoke-virtual {v1, v5}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 97
    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->g:Landroid/support/v4/widget/SwipeRefreshLayout;

    invoke-virtual {v1, p0}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroid/support/v4/widget/SwipeRefreshLayout$OnRefreshListener;)V

    .line 98
    new-instance v1, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    invoke-direct {v1, p0, v2, v4}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;-><init>(Lflipboard/gui/personal/TocGridFragment;Landroid/content/Context;Ljava/util/List;)V

    iput-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->d:Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    .line 99
    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->f:Lorg/askerov/dynamicgrid/DynamicGridView;

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment;->d:Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;

    invoke-virtual {v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 100
    iget-object v1, p0, Lflipboard/gui/personal/TocGridFragment;->f:Lorg/askerov/dynamicgrid/DynamicGridView;

    new-instance v2, Lflipboard/gui/personal/TocGridFragment$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/personal/TocGridFragment$1;-><init>(Lflipboard/gui/personal/TocGridFragment;Landroid/widget/ImageButton;)V

    invoke-virtual {v1, v2}, Lorg/askerov/dynamicgrid/DynamicGridView;->setOnDropListener(Lorg/askerov/dynamicgrid/DynamicGridView$OnDropListener;)V

    .line 144
    invoke-virtual {p0}, Lflipboard/gui/personal/TocGridFragment;->b()V

    .line 145
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->f:Lorg/askerov/dynamicgrid/DynamicGridView;

    new-instance v1, Lflipboard/gui/personal/TocGridFragment$2;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/TocGridFragment$2;-><init>(Lflipboard/gui/personal/TocGridFragment;)V

    invoke-virtual {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 153
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment;->f:Lorg/askerov/dynamicgrid/DynamicGridView;

    new-instance v1, Lflipboard/gui/personal/TocGridFragment$3;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/TocGridFragment$3;-><init>(Lflipboard/gui/personal/TocGridFragment;)V

    invoke-virtual {v0, v1}, Lorg/askerov/dynamicgrid/DynamicGridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 182
    return-object v3
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 188
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 189
    return-void
.end method
