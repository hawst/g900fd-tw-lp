.class Lflipboard/gui/personal/ServiceListFragment$1;
.super Ljava/lang/Object;
.source "ServiceListFragment.java"

# interfaces
.implements Lflipboard/service/Flap$SectionListObserver;


# instance fields
.field final synthetic a:Lflipboard/gui/ContentDrawerView;

.field final synthetic b:Lflipboard/objs/ConfigService;

.field final synthetic c:Lflipboard/gui/personal/ServiceListFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/personal/ServiceListFragment;Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lflipboard/gui/personal/ServiceListFragment$1;->c:Lflipboard/gui/personal/ServiceListFragment;

    iput-object p2, p0, Lflipboard/gui/personal/ServiceListFragment$1;->a:Lflipboard/gui/ContentDrawerView;

    iput-object p3, p0, Lflipboard/gui/personal/ServiceListFragment$1;->b:Lflipboard/objs/ConfigService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Lflipboard/service/Account;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;",
            "Ljava/lang/String;",
            "Lflipboard/service/Account;",
            ")V"
        }
    .end annotation

    .prologue
    .line 154
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/ServiceListFragment$1$1;

    invoke-direct {v1, p0, p3, p2, p1}, Lflipboard/gui/personal/ServiceListFragment$1$1;-><init>(Lflipboard/gui/personal/ServiceListFragment$1;Lflipboard/service/Account;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 167
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 130
    check-cast p1, Lflipboard/objs/SectionListResult;

    invoke-virtual {p1}, Lflipboard/objs/SectionListResult;->a()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v0, p0, Lflipboard/gui/personal/ServiceListFragment$1;->a:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->c()V

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment$1;->b:Lflipboard/objs/ConfigService;

    iget-object v1, v1, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    const-string v2, "flipboard"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v3, "flipboard"

    invoke-virtual {v2, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lflipboard/gui/personal/ServiceListFragment$1;->a(Ljava/util/List;Ljava/lang/String;Lflipboard/service/Account;)V

    goto :goto_0

    :cond_2
    iget-object v1, p1, Lflipboard/objs/SectionListResult;->j:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lflipboard/gui/personal/ServiceListFragment$1;->a(Ljava/util/List;Ljava/lang/String;Lflipboard/service/Account;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lflipboard/gui/personal/ServiceListFragment$1;->a:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->c()V

    .line 151
    return-void
.end method
