.class public Lflipboard/gui/personal/TabPagerAdapter;
.super Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;
.source "TabPagerAdapter.java"


# instance fields
.field private b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lflipboard/activities/FlipboardFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 33
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/TabPagerAdapter;->b:Landroid/util/SparseArray;

    .line 37
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 24
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to create fragment for invalid position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-instance v0, Lflipboard/gui/toc/TocSubTabsFragment;

    invoke-direct {v0}, Lflipboard/gui/toc/TocSubTabsFragment;-><init>()V

    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "enable_scrolling"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "settings_topic_feed"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lflipboard/service/Section;

    const-string v1, "auth/flipboard/flirt/tmix"

    move-object v3, v2

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    move-object v1, v0

    :goto_1
    if-eqz v6, :cond_1

    invoke-static {v1, v7}, Lflipboard/gui/section/SectionFragmentScrolling;->a(Lflipboard/service/Section;Z)Lflipboard/gui/section/SectionFragmentScrolling;

    move-result-object v0

    goto :goto_0

    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->h:Lflipboard/service/Section;

    move-object v1, v0

    goto :goto_1

    :cond_1
    new-instance v0, Lflipboard/gui/section/SectionFragment;

    invoke-direct {v0}, Lflipboard/gui/section/SectionFragment;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2, v7}, Landroid/os/Bundle;-><init>(I)V

    const-string v3, "source"

    sget-object v4, Lflipboard/objs/UsageEventV2$SectionNavFrom;->d:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v4}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Landroid/os/Bundle;-><init>(I)V

    const-string v4, "sid"

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "extra_content_discovery_from_source"

    invoke-virtual {v3, v1, v2}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardFragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lflipboard/gui/discovery/DiscoveryFragment;

    invoke-direct {v0}, Lflipboard/gui/discovery/DiscoveryFragment;-><init>()V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lflipboard/gui/personal/NotificationsFragment;

    invoke-direct {v0}, Lflipboard/gui/personal/NotificationsFragment;-><init>()V

    goto :goto_0

    :pswitch_4
    invoke-static {}, Lflipboard/util/ActivityUtil;->a()Lflipboard/activities/FlipboardFragment;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 137
    invoke-super {p0, p1, p2}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardFragment;

    .line 138
    iget-object v1, p0, Lflipboard/gui/personal/TabPagerAdapter;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 139
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lflipboard/gui/personal/TabPagerAdapter;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 145
    invoke-super {p0, p1, p2, p3}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 146
    return-void
.end method

.method public final synthetic b(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 24
    const/4 v0, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "Following"

    goto :goto_0

    :pswitch_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "settings_topic_feed"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Topic Feed"

    goto :goto_0

    :cond_0
    const-string v0, "Home"

    goto :goto_0

    :pswitch_2
    const-string v0, "Search"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x5

    return v0
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 46
    packed-switch p1, :pswitch_data_0

    .line 63
    const/4 v0, -0x1

    .line 66
    :goto_0
    return v0

    .line 48
    :pswitch_0
    const v0, 0x7f020218

    .line 49
    goto :goto_0

    .line 51
    :pswitch_1
    const v0, 0x7f020217

    .line 52
    goto :goto_0

    .line 54
    :pswitch_2
    const v0, 0x7f02021b

    .line 55
    goto :goto_0

    .line 57
    :pswitch_3
    const v0, 0x7f020219

    .line 58
    goto :goto_0

    .line 60
    :pswitch_4
    const v0, 0x7f02021a

    .line 61
    goto :goto_0

    .line 46
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final d(I)Lflipboard/activities/FlipboardFragment;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lflipboard/gui/personal/TabPagerAdapter;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardFragment;

    return-object v0
.end method
