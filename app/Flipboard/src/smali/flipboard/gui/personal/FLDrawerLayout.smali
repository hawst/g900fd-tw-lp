.class public Lflipboard/gui/personal/FLDrawerLayout;
.super Landroid/support/v4/widget/DrawerLayout;
.source "FLDrawerLayout.java"


# instance fields
.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/activities/FlipboardActivity$OnBackPressedListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/FLDrawerLayout;->i:Ljava/util/List;

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/FLDrawerLayout;->i:Ljava/util/List;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v4/widget/DrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/personal/FLDrawerLayout;->i:Ljava/util/List;

    .line 34
    return-void
.end method

.method public static b(Landroid/support/v4/widget/DrawerLayout;)Z
    .locals 1

    .prologue
    .line 66
    if-eqz p0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/support/v4/widget/DrawerLayout;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    .line 52
    const/4 v0, 0x0

    .line 53
    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    .line 54
    iget-object v1, p0, Lflipboard/gui/personal/FLDrawerLayout;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 55
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez v0, :cond_0

    .line 56
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity$OnBackPressedListener;

    invoke-interface {v0}, Lflipboard/activities/FlipboardActivity$OnBackPressedListener;->c()Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_0
    if-nez v0, :cond_1

    .line 60
    invoke-super {p0, p1, p2}, Landroid/support/v4/widget/DrawerLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 62
    :cond_1
    return v0
.end method
