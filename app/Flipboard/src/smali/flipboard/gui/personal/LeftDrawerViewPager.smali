.class public Lflipboard/gui/personal/LeftDrawerViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "LeftDrawerViewPager.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;ZIII)Z
    .locals 1

    .prologue
    .line 28
    instance-of v0, p1, Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 29
    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    .line 30
    iget-boolean v0, v0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    if-nez v0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 34
    :goto_0
    return v0

    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/support/v4/view/ViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    goto :goto_0
.end method
