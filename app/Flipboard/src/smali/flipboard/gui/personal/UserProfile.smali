.class public Lflipboard/gui/personal/UserProfile;
.super Landroid/widget/RelativeLayout;
.source "UserProfile.java"


# instance fields
.field a:Lflipboard/gui/FLLabelTextView;

.field b:Lflipboard/gui/FLTextView;

.field c:Lflipboard/gui/FLImageView;

.field d:Lflipboard/gui/FLButton;

.field e:Lflipboard/gui/FLImageView;

.field private f:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 69
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 71
    if-eqz v0, :cond_1

    .line 72
    iget-object v1, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 73
    invoke-virtual {p0}, Lflipboard/gui/personal/UserProfile;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0049

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v4, v4, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 74
    iget-object v3, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v3, v3, Lflipboard/objs/UserService;->s:Ljava/lang/String;

    .line 76
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 77
    const-string v2, " "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 78
    iget-object v2, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v2}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 79
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    invoke-virtual {v0}, Lflipboard/objs/UserService;->a()Ljava/lang/String;

    move-result-object v0

    .line 80
    iget-object v2, p0, Lflipboard/gui/personal/UserProfile;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 83
    :cond_0
    iget-object v0, p0, Lflipboard/gui/personal/UserProfile;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/personal/UserProfile;->a:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "settings_topic_feed"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 87
    if-nez v0, :cond_1

    .line 88
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0xa

    .line 89
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->h:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;I)Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 91
    iget-object v1, p0, Lflipboard/gui/personal/UserProfile;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 96
    :cond_1
    return-void
.end method

.method static synthetic a(Lflipboard/gui/personal/UserProfile;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lflipboard/gui/personal/UserProfile;->a()V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 100
    new-instance v0, Lflipboard/gui/personal/UserProfile$2;

    invoke-direct {v0, p0}, Lflipboard/gui/personal/UserProfile$2;-><init>(Lflipboard/gui/personal/UserProfile;)V

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->f:Lflipboard/util/Observer;

    .line 111
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/personal/UserProfile;->f:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 112
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 113
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 117
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/personal/UserProfile;->f:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->f:Lflipboard/util/Observer;

    .line 119
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 120
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 51
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/UserProfile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->c:Lflipboard/gui/FLImageView;

    .line 52
    const v0, 0x7f0a02a8

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/UserProfile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->b:Lflipboard/gui/FLTextView;

    .line 53
    const v0, 0x7f0a02a7

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/UserProfile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->a:Lflipboard/gui/FLLabelTextView;

    .line 54
    const v0, 0x7f0a02a5

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/UserProfile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->e:Lflipboard/gui/FLImageView;

    .line 55
    const v0, 0x7f0a038c

    invoke-virtual {p0, v0}, Lflipboard/gui/personal/UserProfile;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/personal/UserProfile;->d:Lflipboard/gui/FLButton;

    .line 57
    iget-object v0, p0, Lflipboard/gui/personal/UserProfile;->d:Lflipboard/gui/FLButton;

    new-instance v1, Lflipboard/gui/personal/UserProfile$1;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/UserProfile$1;-><init>(Lflipboard/gui/personal/UserProfile;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    invoke-direct {p0}, Lflipboard/gui/personal/UserProfile;->a()V

    .line 65
    return-void
.end method
