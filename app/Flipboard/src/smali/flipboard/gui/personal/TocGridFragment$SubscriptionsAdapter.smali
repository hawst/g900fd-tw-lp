.class Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;
.super Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;
.source "TocGridFragment.java"


# instance fields
.field a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final b:[I

.field final synthetic c:Lflipboard/gui/personal/TocGridFragment;

.field private final f:Z


# direct methods
.method public constructor <init>(Lflipboard/gui/personal/TocGridFragment;Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 287
    iput-object p1, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    .line 288
    invoke-direct {p0, p2, p3}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 279
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b:[I

    .line 289
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->f:Z

    .line 290
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->a:Ljava/util/Map;

    .line 291
    invoke-direct {p0}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b()V

    .line 292
    return-void

    .line 289
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 279
    :array_0
    .array-data 4
        0x7f020221
        0x7f020222
        0x7f020223
        0x7f020224
        0x7f020225
    .end array-data
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 295
    iget-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->f:Z

    if-eqz v0, :cond_0

    .line 296
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 297
    new-instance v0, Lflipboard/service/Section;

    const-string v1, "topic_picker"

    const v4, 0x7f0d0152

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 298
    iget-object v1, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/TOCSection;->J:Ljava/lang/String;

    .line 299
    invoke-super {p0, v0}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->b(Ljava/lang/Object;)V

    .line 301
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 390
    invoke-super {p0, p1, p2}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a(II)V

    .line 391
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    iput p2, v0, Lflipboard/gui/personal/TocGridFragment;->b:I

    .line 392
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 305
    invoke-super {p0, p1}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a(Ljava/util/List;)V

    .line 306
    invoke-direct {p0}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b()V

    .line 307
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 397
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 385
    invoke-super {p0, p1}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Section;

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 272
    invoke-virtual {p0, p1}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b(I)Lflipboard/service/Section;

    move-result-object v0

    return-object v0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 316
    iget-boolean v1, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->f:Z

    if-eqz v1, :cond_0

    .line 317
    invoke-virtual {p0}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    .line 319
    :cond_0
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 326
    invoke-virtual {p0, p1}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b(I)Lflipboard/service/Section;

    move-result-object v1

    .line 327
    invoke-virtual {p0, p1}, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->getItemViewType(I)I

    move-result v0

    .line 328
    if-ne v0, v5, :cond_2

    .line 329
    if-nez p2, :cond_1

    .line 330
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v0}, Lflipboard/gui/personal/TocGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0300c2

    invoke-static {v0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/personal/TocGridTile;

    .line 334
    :goto_0
    iget-object v2, v0, Lflipboard/gui/personal/TocGridTile;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    move-object p2, v0

    .line 380
    :cond_0
    :goto_1
    return-object p2

    .line 332
    :cond_1
    check-cast p2, Lflipboard/gui/personal/TocGridTile;

    move-object v0, p2

    goto :goto_0

    .line 336
    :cond_2
    if-nez p2, :cond_5

    .line 337
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v0}, Lflipboard/gui/personal/TocGridFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f0300c3

    invoke-static {v0, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/personal/TocGridTile;

    move-object p2, v0

    .line 343
    :goto_2
    iget-object v0, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-boolean v0, v0, Lflipboard/service/Section$Meta;->b:Z

    if-eqz v0, :cond_7

    .line 344
    iget-object v0, p2, Lflipboard/gui/personal/TocGridTile;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 345
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 346
    iget-object v0, v2, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "#"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Lflipboard/objs/ConfigService;->I:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 347
    :goto_3
    invoke-virtual {p2, v0}, Lflipboard/gui/personal/TocGridTile;->setBackgroundColor(I)V

    .line 348
    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->h()Ljava/lang/String;

    move-result-object v0

    .line 349
    if-nez v0, :cond_3

    .line 350
    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->e:Landroid/content/Context;

    const v2, 0x7f0d032b

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 352
    :cond_3
    iget-object v2, p2, Lflipboard/gui/personal/TocGridTile;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 353
    iget-object v0, p2, Lflipboard/gui/personal/TocGridTile;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    :cond_4
    :goto_4
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-static {v0}, Lflipboard/gui/personal/TocGridFragment;->a(Lflipboard/gui/personal/TocGridFragment;)Lorg/askerov/dynamicgrid/DynamicGridView;

    move-result-object v0

    iget-boolean v0, v0, Lorg/askerov/dynamicgrid/DynamicGridView;->c:Z

    if-nez v0, :cond_0

    .line 376
    invoke-virtual {p2, v4}, Lflipboard/gui/personal/TocGridTile;->setVisibility(I)V

    goto :goto_1

    .line 339
    :cond_5
    check-cast p2, Lflipboard/gui/personal/TocGridTile;

    .line 340
    iget-object v0, p2, Lflipboard/gui/personal/TocGridTile;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->a()V

    goto :goto_2

    .line 346
    :cond_6
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v0}, Lflipboard/gui/personal/TocGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f08005b

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_3

    .line 355
    :cond_7
    invoke-virtual {p2, v3}, Lflipboard/gui/personal/TocGridTile;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 356
    invoke-virtual {p2, v1}, Lflipboard/gui/personal/TocGridTile;->setTag(Ljava/lang/Object;)V

    .line 357
    invoke-static {p2, v5, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;ZI)V

    .line 358
    iget-object v0, p2, Lflipboard/gui/personal/TocGridTile;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/service/Section;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->a:Ljava/util/Map;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 361
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->a:Ljava/util/Map;

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 362
    iget-object v2, p2, Lflipboard/gui/personal/TocGridTile;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/service/Section;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    :goto_5
    iget-object v2, p2, Lflipboard/gui/personal/TocGridTile;->b:Lflipboard/gui/FLImageView;

    iget-object v3, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    invoke-virtual {v3}, Lflipboard/gui/personal/TocGridFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLImageView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 368
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->c:Lflipboard/gui/personal/TocGridFragment;

    iget-object v0, v0, Lflipboard/gui/personal/TocGridFragment;->a:Ljava/util/Map;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 369
    if-eqz v0, :cond_4

    .line 370
    iget-object v1, p2, Lflipboard/gui/personal/TocGridTile;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto :goto_4

    .line 364
    :cond_8
    iget-object v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b:[I

    iget-object v2, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->b:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    rem-int v2, p1, v2

    aget v0, v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 365
    iget-object v2, p2, Lflipboard/gui/personal/TocGridTile;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 311
    iget-boolean v0, p0, Lflipboard/gui/personal/TocGridFragment$SubscriptionsAdapter;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
