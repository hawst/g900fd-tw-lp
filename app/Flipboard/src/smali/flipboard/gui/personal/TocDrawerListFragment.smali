.class public Lflipboard/gui/personal/TocDrawerListFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "TocDrawerListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/activities/FlipboardFragment;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

.field b:Lflipboard/activities/FlipboardActivity$OnBackPressedListener;

.field private c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

.field private d:Lflipboard/gui/EditableListView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 97
    new-instance v0, Lflipboard/gui/personal/TocDrawerListFragment$1;

    invoke-direct {v0, p0}, Lflipboard/gui/personal/TocDrawerListFragment$1;-><init>(Lflipboard/gui/personal/TocDrawerListFragment;)V

    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->b:Lflipboard/activities/FlipboardActivity$OnBackPressedListener;

    .line 54
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 55
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/service/User;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 58
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/personal/TocDrawerListFragment;)Lflipboard/gui/EditableListView;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    return-object v0
.end method

.method public static a(Lflipboard/gui/personal/TocDrawerListFragment$Filter;)Lflipboard/gui/personal/TocDrawerListFragment;
    .locals 3

    .prologue
    .line 61
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 62
    const-string v1, "filter"

    invoke-virtual {p0}, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v1, Lflipboard/gui/personal/TocDrawerListFragment;

    invoke-direct {v1}, Lflipboard/gui/personal/TocDrawerListFragment;-><init>()V

    .line 64
    invoke-virtual {v1, v0}, Lflipboard/gui/personal/TocDrawerListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 65
    return-object v1
.end method

.method static synthetic b(Lflipboard/gui/personal/TocDrawerListFragment;)Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 35
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/service/User$Message;->c:Lflipboard/service/User$Message;

    if-ne p2, v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/personal/TocDrawerListFragment$2;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/TocDrawerListFragment$2;-><init>(Lflipboard/gui/personal/TocDrawerListFragment;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lflipboard/gui/personal/TocDrawerListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "filter"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->valueOf(Ljava/lang/String;)Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 77
    const v0, 0x7f0300c0

    invoke-static {v2, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/EditableListView;

    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    .line 78
    new-instance v0, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;-><init>(Lflipboard/gui/personal/TocDrawerListFragment;B)V

    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    .line 79
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/service/User;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a(Ljava/util/List;)V

    .line 80
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    iget-object v1, p0, Lflipboard/gui/personal/TocDrawerListFragment;->c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 81
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    sget-object v1, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->c:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    sget-object v1, Lflipboard/gui/personal/TocDrawerListFragment$Filter;->b:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    if-ne v0, v1, :cond_1

    .line 85
    :cond_0
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 86
    const v0, 0x7f030147

    invoke-static {v2, v0, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/personal/TocSubEmptyStateView;

    .line 87
    iget-object v2, p0, Lflipboard/gui/personal/TocDrawerListFragment;->a:Lflipboard/gui/personal/TocDrawerListFragment$Filter;

    invoke-virtual {v0, v2}, Lflipboard/gui/personal/TocSubEmptyStateView;->setType(Lflipboard/gui/personal/TocDrawerListFragment$Filter;)V

    .line 88
    iget-object v2, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    invoke-virtual {v2, v0}, Lflipboard/gui/EditableListView;->setEmptyView(Landroid/view/View;)V

    .line 89
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 90
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    move-object v0, v1

    .line 93
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->d:Lflipboard/gui/EditableListView;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 117
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 118
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 119
    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 120
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroyView()V

    .line 112
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    .line 113
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lflipboard/gui/personal/TocDrawerListFragment;->c:Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;

    invoke-virtual {v0, p3}, Lflipboard/gui/personal/TocDrawerListFragment$SectionAdapter;->a(I)Lflipboard/service/Section;

    move-result-object v1

    .line 136
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    sget-object v2, Lflipboard/objs/UsageEventV2$SectionNavFrom;->a:Lflipboard/objs/UsageEventV2$SectionNavFrom;

    invoke-virtual {v2}, Lflipboard/objs/UsageEventV2$SectionNavFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 137
    return-void
.end method
