.class public Lflipboard/gui/personal/ServiceListFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "ServiceListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final d:Landroid/os/Bundle;


# instance fields
.field a:Lflipboard/gui/actionbar/FLActionBar;

.field b:Lflipboard/gui/EditableListView;

.field c:Landroid/widget/Button;

.field private e:Ljava/lang/String;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 57
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 62
    sput-object v0, Lflipboard/gui/personal/ServiceListFragment;->d:Landroid/os/Bundle;

    const-string v1, "source"

    const-string v2, "toc"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lflipboard/gui/personal/ServiceListFragment;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    const-string v1, "service_id"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "pageKey"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v1, "title"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    new-instance v1, Lflipboard/gui/personal/ServiceListFragment;

    invoke-direct {v1}, Lflipboard/gui/personal/ServiceListFragment;-><init>()V

    .line 71
    invoke-virtual {v1, v0}, Lflipboard/gui/personal/ServiceListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 72
    return-object v1
.end method

.method static synthetic a(Lflipboard/gui/personal/ServiceListFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 77
    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f030047

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ContentDrawerView;

    .line 78
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 79
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->c:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 80
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->a:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1, v3, v4}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 82
    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 83
    const-string v1, "service_id"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    .line 84
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v6

    .line 85
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v1, p0}, Lflipboard/gui/EditableListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 87
    const-string v1, "title"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 88
    if-nez v1, :cond_2

    .line 89
    invoke-virtual {v6}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 92
    :goto_0
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->a:Lflipboard/gui/actionbar/FLActionBar;

    const v7, 0x7f0a00fe

    invoke-virtual {v1, v7}, Lflipboard/gui/actionbar/FLActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    .line 93
    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    const-string v2, "googlereader"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/model/ConfigSetting;->GoogleReaderDisabled:Z

    if-eqz v1, :cond_0

    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lflipboard/gui/personal/ServiceListFragment;->f:Z

    .line 96
    iget-boolean v1, p0, Lflipboard/gui/personal/ServiceListFragment;->f:Z

    if-eqz v1, :cond_1

    .line 97
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->c:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0d026a

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 102
    :goto_2
    const-string v1, "pageKey"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v3

    new-instance v4, Lflipboard/gui/personal/ServiceListFragment$1;

    invoke-direct {v4, p0, v0, v6}, Lflipboard/gui/personal/ServiceListFragment$1;-><init>(Lflipboard/gui/personal/ServiceListFragment;Lflipboard/gui/ContentDrawerView;Lflipboard/objs/ConfigService;)V

    invoke-virtual {v3, v2, v6, v1, v4}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)V

    .line 104
    return-object v0

    :cond_0
    move v1, v4

    .line 95
    goto :goto_1

    .line 99
    :cond_1
    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->c:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0d02f4

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lflipboard/gui/personal/ServiceListFragment;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, p3}, Lflipboard/gui/EditableListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    move-object v1, v0

    .line 110
    check-cast v1, Lflipboard/objs/SectionListItem;

    .line 111
    iget-object v2, v1, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v3, "feed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 112
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v1, Lflipboard/objs/SectionListItem;->p:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    .line 113
    if-nez v1, :cond_2

    .line 114
    invoke-static {v0}, Lflipboard/service/Section;->a(Lflipboard/objs/ContentDrawerListItem;)Lflipboard/service/Section;

    move-result-object v0

    .line 115
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 117
    :goto_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lflipboard/activities/SectionTabletActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 118
    const-string v2, "extra_content_discovery_from_source"

    sget-object v3, Lflipboard/gui/personal/ServiceListFragment;->d:Landroid/os/Bundle;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 119
    const-string v2, "sid"

    invoke-virtual {v0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 120
    invoke-virtual {p0, v1}, Lflipboard/gui/personal/ServiceListFragment;->startActivity(Landroid/content/Intent;)V

    .line 126
    :cond_0
    :goto_1
    return-void

    .line 122
    :cond_1
    iget-object v0, v1, Lflipboard/objs/SectionListItem;->a:Ljava/lang/String;

    const-string v2, "folder"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 123
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v1, Lflipboard/objs/SectionListItem;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 124
    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, v1, Lflipboard/objs/SectionListItem;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lflipboard/objs/SectionListItem;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v3, v1}, Lflipboard/util/ActivityUtil;->a(Landroid/content/Context;Lflipboard/objs/ConfigService;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public onSignOutClicked()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 173
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    :goto_0
    return-void

    .line 177
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 178
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    .line 179
    invoke-virtual {p0}, Lflipboard/gui/personal/ServiceListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 181
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 182
    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 184
    iget-boolean v3, p0, Lflipboard/gui/personal/ServiceListFragment;->f:Z

    if-eqz v3, :cond_1

    .line 185
    const v1, 0x7f0d00a2

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 186
    const v1, 0x7f0d00a1

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 187
    const v1, 0x7f0d026a

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 188
    new-instance v1, Lflipboard/gui/personal/ServiceListFragment$2;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/ServiceListFragment$2;-><init>(Lflipboard/gui/personal/ServiceListFragment;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 228
    :goto_1
    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "sign_out"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 198
    :cond_1
    const v3, 0x7f0d00a5

    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 199
    const v3, 0x7f0d02f4

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 200
    iget-object v3, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lflipboard/gui/personal/ServiceListFragment;->e:Ljava/lang/String;

    const-string v4, "flipboard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 201
    const v1, 0x7f0d00a3

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 202
    new-instance v1, Lflipboard/gui/personal/ServiceListFragment$3;

    invoke-direct {v1, p0, v0}, Lflipboard/gui/personal/ServiceListFragment$3;-><init>(Lflipboard/gui/personal/ServiceListFragment;Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto :goto_1

    .line 216
    :cond_2
    const v3, 0x7f0d00a4

    invoke-virtual {v0, v3}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 217
    new-instance v1, Lflipboard/gui/personal/ServiceListFragment$4;

    invoke-direct {v1, p0}, Lflipboard/gui/personal/ServiceListFragment$4;-><init>(Lflipboard/gui/personal/ServiceListFragment;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto :goto_1
.end method
