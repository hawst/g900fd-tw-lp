.class public Lflipboard/gui/FlipFragment$MagazineAdapter;
.super Landroid/widget/BaseAdapter;
.source "FlipFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Magazine;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic b:Lflipboard/gui/FlipFragment;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lflipboard/gui/FlipFragment;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 137
    iput-object p1, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 135
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->a:Ljava/util/List;

    .line 138
    iput-object p2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->c:Landroid/content/Context;

    .line 139
    iput-object p3, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->d:Ljava/lang/String;

    .line 140
    return-void
.end method

.method private a(I)Lflipboard/objs/Magazine;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lflipboard/gui/FlipFragment$MagazineAdapter;->a(I)Lflipboard/objs/Magazine;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 157
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 164
    if-eqz p2, :cond_0

    .line 166
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FlipFragment$MagazineHolder;

    .line 167
    iget-object v1, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    .line 176
    :goto_0
    invoke-direct {p0, p1}, Lflipboard/gui/FlipFragment$MagazineAdapter;->a(I)Lflipboard/objs/Magazine;

    move-result-object v1

    .line 177
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v3, v1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, v1, Lflipboard/objs/Magazine;->b:Ljava/lang/String;

    const-string v4, "private"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->c:Landroid/widget/ImageView;

    const v3, 0x7f020155

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    iget-boolean v2, v1, Lflipboard/objs/Magazine;->n:Z

    if-eqz v2, :cond_3

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->a:Lflipboard/gui/FLImageView;

    iget v3, v1, Lflipboard/objs/Magazine;->o:I

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setBackgroundResource(I)V

    :goto_2
    iget-object v0, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->b:Landroid/widget/TextView;

    iget-object v1, v1, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    return-object p2

    .line 169
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->c:Landroid/content/Context;

    const v1, 0x7f0300d6

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 170
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0x190

    invoke-direct {v0, v1, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    new-instance v0, Lflipboard/gui/FlipFragment$MagazineHolder;

    invoke-direct {v0}, Lflipboard/gui/FlipFragment$MagazineHolder;-><init>()V

    .line 172
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 173
    invoke-static {v0, p2}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    goto :goto_0

    .line 177
    :cond_1
    iget-object v3, v1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    if-eqz v3, :cond_2

    iget-object v3, v1, Lflipboard/objs/Magazine;->i:Lflipboard/objs/Author;

    iget-object v3, v3, Lflipboard/objs/Author;->e:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->c:Landroid/widget/ImageView;

    const v3, 0x7f02015a

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_3
    iget-object v2, v0, Lflipboard/gui/FlipFragment$MagazineHolder;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v1, Lflipboard/objs/Magazine;->t:Lflipboard/objs/Image;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    goto :goto_2
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 206
    invoke-direct {p0, p3}, Lflipboard/gui/FlipFragment$MagazineAdapter;->a(I)Lflipboard/objs/Magazine;

    move-result-object v5

    .line 208
    iget-object v0, v5, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, v5, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    const-string v1, "auth/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 209
    iget-object v0, v5, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 214
    :goto_0
    new-instance v4, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->p:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->f:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v4, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 218
    iget-object v1, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v1}, Lflipboard/gui/FlipFragment;->a(Lflipboard/gui/FlipFragment;)Lflipboard/service/Section;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 219
    iget-object v1, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v1}, Lflipboard/gui/FlipFragment;->a(Lflipboard/gui/FlipFragment;)Lflipboard/service/Section;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v1

    .line 220
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v2, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 221
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v3}, Lflipboard/gui/FlipFragment;->a(Lflipboard/gui/FlipFragment;)Lflipboard/service/Section;

    move-result-object v3

    iget-object v3, v3, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v3, v3, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 225
    :goto_1
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v5}, Lflipboard/objs/Magazine;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v2, Lflipboard/objs/UsageEventV2$EventDataType;->c:Lflipboard/objs/UsageEventV2$EventDataType;

    :goto_2
    invoke-virtual {v4, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 226
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->g:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, v5, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 227
    sget-object v3, Lflipboard/objs/UsageEventV2$CommonEventData;->h:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, v5, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    if-nez v2, :cond_4

    const-string v2, ""

    :goto_3
    invoke-virtual {v4, v3, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 228
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->e:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 229
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v2}, Lflipboard/gui/FlipFragment;->b(Lflipboard/gui/FlipFragment;)Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 231
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v0}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 232
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v0}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    .line 233
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v2}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 234
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v2}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->z()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 235
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v2}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 236
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v2}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 237
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->j:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v2}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v2

    iget-object v2, v2, Lflipboard/objs/FeedItem;->bU:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 239
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v0}, Lflipboard/gui/FlipFragment;->b(Lflipboard/gui/FlipFragment;)Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    move-result-object v0

    sget-object v2, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->b:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    if-ne v0, v2, :cond_0

    .line 240
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v0}, Lflipboard/gui/FlipFragment;->c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->al()Ljava/lang/String;

    move-result-object v0

    .line 241
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 247
    :cond_0
    :goto_4
    iget-boolean v0, v5, Lflipboard/objs/Magazine;->m:Z

    if-eqz v0, :cond_1

    .line 248
    const-string v0, "default_"

    iget-object v2, v5, Lflipboard/objs/Magazine;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 249
    sget-object v2, Lflipboard/objs/UsageEventV2$CommonEventData;->f:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v4, v2, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 253
    :cond_1
    invoke-static {v4, v5, v1, v6}, Lflipboard/util/ShareHelper;->a(Lflipboard/objs/UsageEventV2;Lflipboard/objs/Magazine;Ljava/lang/String;Ljava/lang/String;)Lflipboard/service/Flap$CancellableJSONResultObserver;

    move-result-object v0

    .line 254
    iget-object v1, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    iget-object v1, v1, Lflipboard/gui/FlipFragment;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 255
    iget-object v4, v5, Lflipboard/objs/Magazine;->r:Ljava/lang/String;

    .line 256
    iget-object v1, v5, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 257
    iget-object v5, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->d:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lflipboard/util/ShareHelper;->a(Lflipboard/service/Flap$JSONResultObserver;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lflipboard/service/Section;)V

    .line 258
    iget-object v0, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->b:Lflipboard/gui/FlipFragment;

    invoke-static {v0}, Lflipboard/gui/FlipFragment;->d(Lflipboard/gui/FlipFragment;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    iget-object v0, v0, Lflipboard/activities/FlipboardActivity;->aa:Lcom/squareup/otto/Bus;

    new-instance v1, Lflipboard/gui/FlipFragment$FlipFinishedEvent;

    invoke-direct {v1}, Lflipboard/gui/FlipFragment$FlipFinishedEvent;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/otto/Bus;->c(Ljava/lang/Object;)V

    .line 259
    return-void

    .line 211
    :cond_2
    iget-object v0, v5, Lflipboard/objs/Magazine;->d:Ljava/lang/String;

    goto/16 :goto_0

    .line 225
    :cond_3
    sget-object v2, Lflipboard/objs/UsageEventV2$EventDataType;->d:Lflipboard/objs/UsageEventV2$EventDataType;

    goto/16 :goto_2

    .line 227
    :cond_4
    iget-object v2, v5, Lflipboard/objs/Magazine;->l:Ljava/lang/String;

    goto/16 :goto_3

    .line 244
    :cond_5
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FlipFragment$MagazineAdapter;->d:Ljava/lang/String;

    invoke-virtual {v4, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-object v3, v6

    goto :goto_4

    :cond_6
    move-object v1, v6

    goto/16 :goto_1
.end method
