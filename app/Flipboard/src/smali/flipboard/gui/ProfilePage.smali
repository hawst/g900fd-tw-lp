.class public Lflipboard/gui/ProfilePage;
.super Landroid/view/ViewGroup;
.source "ProfilePage.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/gui/FLImageView;

.field b:Landroid/widget/LinearLayout;

.field c:Lflipboard/gui/FLImageView;

.field d:Lflipboard/gui/FLLabelTextView;

.field e:Lflipboard/gui/FLTextView;

.field f:Lflipboard/gui/FLButton;

.field g:Lflipboard/gui/FLTextView;

.field h:Lflipboard/gui/FLTextView;

.field i:Lflipboard/gui/FLTextView;

.field j:Lflipboard/gui/FLTextView;

.field k:Lflipboard/service/Section;

.field private l:Landroid/view/View;

.field private m:Lflipboard/objs/FeedItem;

.field private n:Lflipboard/gui/MagazineThumbView;

.field private o:Lflipboard/gui/MagazineThumbView;

.field private p:Lflipboard/gui/MagazineThumbView;

.field private q:Landroid/widget/LinearLayout;

.field private r:Landroid/graphics/Paint;

.field private s:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/FlipboardManager;",
            "Lflipboard/service/FlipboardManager$SectionsAndAccountMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method private a(Lflipboard/objs/SidebarGroup;I)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 363
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, p2

    .line 364
    :goto_0
    if-ge p2, v4, :cond_4

    .line 365
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 366
    if-eqz v0, :cond_5

    .line 367
    iget-object v2, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v3

    .line 368
    :goto_1
    if-nez p2, :cond_2

    .line 369
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->n:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 370
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->n:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 371
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->n:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 372
    add-int/lit8 v0, v1, 0x1

    .line 364
    :goto_2
    add-int/lit8 p2, p2, 0x1

    move v1, v0

    goto :goto_0

    .line 367
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 373
    :cond_2
    if-ne p2, v3, :cond_3

    .line 374
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->o:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 375
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->o:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 376
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->o:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 377
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 378
    :cond_3
    const/4 v5, 0x2

    if-ne p2, v5, :cond_5

    .line 379
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->p:Lflipboard/gui/MagazineThumbView;

    iget-object v6, v0, Lflipboard/objs/FeedItem;->J:Lflipboard/objs/Image;

    invoke-virtual {v5, v6, v2}, Lflipboard/gui/MagazineThumbView;->a(Lflipboard/objs/Image;Z)V

    .line 380
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->p:Lflipboard/gui/MagazineThumbView;

    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lflipboard/gui/MagazineThumbView;->setTitle(Ljava/lang/String;)V

    .line 381
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->p:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v2, v0}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    .line 382
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 386
    :cond_4
    return v1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lflipboard/gui/ProfilePage;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->k:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/ProfilePage;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->m:Lflipboard/objs/FeedItem;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 262
    iput-object p1, p0, Lflipboard/gui/ProfilePage;->k:Lflipboard/service/Section;

    .line 263
    iput-object p2, p0, Lflipboard/gui/ProfilePage;->m:Lflipboard/objs/FeedItem;

    .line 265
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    .line 266
    if-eqz v0, :cond_2

    .line 267
    iget-object v1, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    if-eqz v1, :cond_4

    .line 268
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    iget-object v3, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 281
    :goto_0
    iget-object v3, v0, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    .line 282
    if-eqz v3, :cond_2

    .line 283
    iget-object v0, v3, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v0, :cond_6

    .line 284
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 288
    :goto_1
    iget-object v0, v3, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :cond_0
    iget-object v0, v3, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, v3, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    .line 292
    :goto_2
    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->m:Ljava/lang/String;

    .line 293
    :goto_3
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 294
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0049

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 296
    :cond_1
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0342

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v2

    aput-object v1, v6, v7

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v1, v3, Lflipboard/objs/FeedSectionLink;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 299
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 305
    :goto_4
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 306
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    new-instance v1, Lflipboard/gui/ProfilePage$4;

    invoke-direct {v1, p0}, Lflipboard/gui/ProfilePage$4;-><init>(Lflipboard/gui/ProfilePage;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 327
    :cond_2
    iget-object v0, p1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v0, Lflipboard/service/Section$Meta;->h:Ljava/util/List;

    .line 328
    if-eqz v1, :cond_10

    .line 330
    const/4 v0, 0x0

    .line 331
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    move-object v2, v0

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 332
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "magazines"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 333
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    if-eqz v1, :cond_d

    .line 334
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SidebarGroup$Metrics;

    .line 335
    iget-object v4, v1, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    if-eqz v4, :cond_b

    iget-object v4, v1, Lflipboard/objs/SidebarGroup$Metrics;->b:Ljava/lang/String;

    .line 336
    :goto_7
    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    if-eqz v7, :cond_c

    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    const-string v8, "magazineCount"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 337
    iget-object v7, p0, Lflipboard/gui/ProfilePage;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v7, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 338
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->j:Lflipboard/gui/FLTextView;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 271
    :cond_4
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v1

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v3

    mul-int/2addr v1, v3

    mul-int/lit8 v1, v1, 0x4

    div-int/lit8 v1, v1, 0xa

    .line 272
    iget-object v3, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    invoke-static {v3, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;I)Lflipboard/objs/FeedItem;

    move-result-object v1

    .line 273
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 274
    iget-object v3, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v3, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 275
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    iput-object v1, v0, Lflipboard/service/Section$Meta;->j:Lflipboard/objs/Image;

    .line 276
    invoke-virtual {p1}, Lflipboard/service/Section;->w()V

    goto/16 :goto_0

    .line 278
    :cond_5
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->l:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 286
    :cond_6
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    const v1, 0x7f02005a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_1

    .line 291
    :cond_7
    const-string v0, ""

    goto/16 :goto_2

    .line 292
    :cond_8
    const-string v1, ""

    goto/16 :goto_3

    .line 300
    :cond_9
    invoke-virtual {p1}, Lflipboard/service/Section;->s()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 301
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 303
    :cond_a
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d02fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 335
    :cond_b
    const-string v4, "0"

    goto/16 :goto_7

    .line 339
    :cond_c
    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    if-eqz v7, :cond_3

    iget-object v7, v1, Lflipboard/objs/SidebarGroup$Metrics;->a:Ljava/lang/String;

    const-string v8, "readers"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 340
    iget-object v7, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v7, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$Metrics;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 345
    :cond_d
    invoke-direct {p0, v0, v3}, Lflipboard/gui/ProfilePage;->a(Lflipboard/objs/SidebarGroup;I)I

    move-result v0

    move v3, v0

    goto/16 :goto_5

    .line 346
    :cond_e
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->a:Ljava/lang/String;

    const-string v4, "contributor"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_11

    :goto_8
    move-object v2, v0

    .line 350
    goto/16 :goto_5

    .line 351
    :cond_f
    if-eqz v2, :cond_10

    .line 352
    invoke-direct {p0, v2, v3}, Lflipboard/gui/ProfilePage;->a(Lflipboard/objs/SidebarGroup;I)I

    .line 355
    :cond_10
    return-void

    :cond_11
    move-object v0, v2

    goto :goto_8
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->m:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 125
    new-instance v0, Lflipboard/gui/ProfilePage$3;

    invoke-direct {v0, p0}, Lflipboard/gui/ProfilePage$3;-><init>(Lflipboard/gui/ProfilePage;)V

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->s:Lflipboard/util/Observer;

    .line 157
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/ProfilePage;->s:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/util/Observer;)V

    .line 158
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 159
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 163
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/ProfilePage;->s:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Lflipboard/util/Observer;)V

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->s:Lflipboard/util/Observer;

    .line 165
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 166
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 244
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 245
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 246
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 247
    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v7

    .line 248
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getHeight()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    mul-int/lit8 v2, v6, 0x2

    sub-int v4, v1, v2

    .line 249
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v4, v1

    iget-object v2, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v8, v1, v2

    .line 251
    int-to-float v1, v0

    int-to-float v2, v8

    int-to-float v3, v0

    int-to-float v4, v4

    iget-object v5, p0, Lflipboard/gui/ProfilePage;->r:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 253
    mul-int/lit8 v0, v6, 0x2

    sub-int v0, v8, v0

    int-to-float v0, v0

    sub-float v2, v0, v7

    .line 254
    int-to-float v1, v6

    .line 255
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getWidth()I

    move-result v0

    sub-int/2addr v0, v6

    int-to-float v3, v0

    .line 257
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->r:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 258
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 71
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 72
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    .line 73
    const v0, 0x7f0a02a5

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    .line 74
    const v0, 0x7f0a02b1

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    .line 75
    const v0, 0x7f0a02a7

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    .line 76
    const v0, 0x7f0a02a8

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    .line 77
    const v0, 0x7f0a0260

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    .line 78
    const v0, 0x7f0a02a6

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->l:Landroid/view/View;

    .line 79
    const v0, 0x7f0a02ac

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    .line 80
    const v0, 0x7f0a02ad

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    .line 81
    const v0, 0x7f0a02af

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->i:Lflipboard/gui/FLTextView;

    .line 82
    const v0, 0x7f0a02b0

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->j:Lflipboard/gui/FLTextView;

    .line 83
    const v0, 0x7f0a02ae

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    .line 84
    const v0, 0x7f0a02b2

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->n:Lflipboard/gui/MagazineThumbView;

    .line 85
    const v0, 0x7f0a02b3

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->o:Lflipboard/gui/MagazineThumbView;

    .line 86
    const v0, 0x7f0a02b4

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->p:Lflipboard/gui/MagazineThumbView;

    .line 88
    new-instance v0, Lflipboard/gui/ProfilePage$1;

    invoke-direct {v0, p0}, Lflipboard/gui/ProfilePage$1;-><init>(Lflipboard/gui/ProfilePage;)V

    .line 97
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->n:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v2}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 98
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->n:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v0}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->o:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v2}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 101
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->o:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v0}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->p:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v2}, Lflipboard/gui/MagazineThumbView;->setImage(Lflipboard/objs/Image;)V

    .line 104
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->p:Lflipboard/gui/MagazineThumbView;

    invoke-virtual {v1, v0}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    new-instance v1, Lflipboard/gui/ProfilePage$2;

    invoke-direct {v1, p0}, Lflipboard/gui/ProfilePage$2;-><init>(Lflipboard/gui/ProfilePage;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/ProfilePage;->r:Landroid/graphics/Paint;

    .line 117
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->r:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080052

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 118
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->r:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 119
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->r:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/ProfilePage;->setWillNotDraw(Z)V

    .line 121
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 216
    sub-int v0, p4, p2

    .line 217
    div-int/lit8 v1, v0, 0x2

    .line 218
    sub-int v2, p5, p3

    .line 219
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f09007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 222
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    invoke-virtual {v4, v8, v8, v5, v6}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 223
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->l:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->l:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v4, v8, v5, v0, v6}, Landroid/view/View;->layout(IIII)V

    .line 224
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v2, v4

    .line 225
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v6

    add-int/lit8 v6, v6, 0x0

    iget-object v7, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v5, v8, v4, v6, v7}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 226
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    add-int/lit8 v6, v3, 0x0

    iget-object v7, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v4, v7

    add-int/lit8 v8, v3, 0x0

    iget-object v9, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v5, v6, v7, v8, v4}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 227
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v2, v4

    mul-int/lit8 v5, v3, 0x2

    sub-int/2addr v4, v5

    .line 228
    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 229
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v6, v1, v6

    iget-object v7, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v4, v7

    iget-object v8, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v1

    invoke-virtual {v5, v6, v7, v8, v4}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 230
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 231
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v6, v1, v6

    iget-object v7, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v7

    sub-int v7, v4, v7

    iget-object v8, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v1, v8

    invoke-virtual {v5, v6, v7, v1, v4}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 233
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v2, v1

    iget-object v2, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    mul-int/lit8 v2, v3, 0x2

    sub-int/2addr v1, v2

    .line 234
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    add-int/lit8 v4, v3, 0x0

    add-int/lit8 v5, v3, 0x0

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v2, v4, v1, v5, v6}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 235
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int/2addr v1, v3

    .line 236
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    add-int/lit8 v4, v3, 0x0

    add-int/lit8 v5, v3, 0x0

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v2, v4, v1, v5, v6}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 237
    iget-object v1, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    sub-int/2addr v1, v3

    .line 238
    iget-object v2, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    iget-object v4, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    invoke-virtual {v4}, Lflipboard/gui/FLButton;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    sub-int/2addr v4, v3

    iget-object v5, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    invoke-virtual {v5}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v1, v5

    sub-int/2addr v0, v3

    invoke-virtual {v2, v4, v5, v0, v1}, Lflipboard/gui/FLButton;->layout(IIII)V

    .line 239
    iget-object v0, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    add-int/lit8 v2, v3, 0x0

    iget-object v4, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v1, v4

    add-int/lit8 v3, v3, 0x0

    iget-object v5, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v3, v5

    invoke-virtual {v0, v2, v4, v3, v1}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 240
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    .line 170
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 171
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 172
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 173
    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 174
    mul-int/lit8 v3, v2, 0x2

    sub-int v3, v0, v3

    .line 175
    mul-int/lit8 v4, v2, 0x4

    sub-int v4, v0, v4

    div-int/lit8 v4, v4, 0x3

    mul-int/lit8 v5, v2, 0x2

    add-int/2addr v4, v5

    .line 178
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v5, v6, v4}, Landroid/widget/LinearLayout;->measure(II)V

    .line 179
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v1, v4

    sub-int/2addr v4, v2

    .line 181
    div-int/lit8 v5, v3, 0x2

    .line 182
    iget-object v6, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    const/high16 v7, -0x80000000

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    const/high16 v8, -0x80000000

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 183
    iget-object v6, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    const/high16 v7, -0x80000000

    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    iget-object v8, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v8

    sub-int v8, v4, v8

    const/high16 v9, -0x80000000

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    invoke-virtual {v6, v7, v8}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 185
    const/4 v6, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 186
    iget-object v7, p0, Lflipboard/gui/ProfilePage;->g:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v7

    iget-object v8, p0, Lflipboard/gui/ProfilePage;->h:Lflipboard/gui/FLTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v7, v8

    .line 188
    iget-object v8, p0, Lflipboard/gui/ProfilePage;->q:Landroid/widget/LinearLayout;

    sub-int/2addr v5, v6

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    mul-int/lit8 v9, v2, 0x4

    add-int/2addr v9, v7

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v8, v5, v9}, Landroid/widget/LinearLayout;->measure(II)V

    .line 189
    sub-int/2addr v4, v7

    mul-int/lit8 v5, v2, 0x2

    sub-int/2addr v4, v5

    .line 191
    sub-int/2addr v4, v6

    mul-int/lit8 v5, v2, 0x2

    sub-int/2addr v4, v5

    .line 192
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 193
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    const/high16 v6, -0x80000000

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/high16 v7, -0x80000000

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 195
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v4, v2

    .line 196
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 197
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    const/high16 v6, -0x80000000

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/high16 v7, -0x80000000

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 199
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v4, v5

    mul-int/lit8 v5, v2, 0x3

    sub-int/2addr v4, v5

    .line 200
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 201
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->a:Lflipboard/gui/FLImageView;

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 203
    sub-int/2addr v4, v2

    .line 204
    const/4 v5, 0x0

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 205
    iget-object v5, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    const/high16 v6, -0x80000000

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    const/high16 v6, -0x80000000

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v5, v3, v4}, Lflipboard/gui/FLButton;->measure(II)V

    .line 207
    const/4 v3, 0x1

    const/high16 v4, 0x42700000    # 60.0f

    invoke-virtual {p0}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 208
    iget-object v4, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v4, v5, v3}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 209
    iget-object v3, p0, Lflipboard/gui/ProfilePage;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/ProfilePage;->f:Lflipboard/gui/FLButton;

    invoke-virtual {v4}, Lflipboard/gui/FLButton;->getMeasuredHeight()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    add-int/2addr v2, v3

    .line 210
    iget-object v3, p0, Lflipboard/gui/ProfilePage;->l:Landroid/view/View;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Landroid/view/View;->measure(II)V

    .line 211
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/ProfilePage;->setMeasuredDimension(II)V

    .line 212
    return-void
.end method
