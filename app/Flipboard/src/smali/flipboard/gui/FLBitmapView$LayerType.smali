.class final enum Lflipboard/gui/FLBitmapView$LayerType;
.super Ljava/lang/Enum;
.source "FLBitmapView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLBitmapView$LayerType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLBitmapView$LayerType;

.field public static final enum b:Lflipboard/gui/FLBitmapView$LayerType;

.field public static final enum c:Lflipboard/gui/FLBitmapView$LayerType;

.field private static final synthetic d:[Lflipboard/gui/FLBitmapView$LayerType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1020
    new-instance v0, Lflipboard/gui/FLBitmapView$LayerType;

    const-string v1, "none"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLBitmapView$LayerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLBitmapView$LayerType;->a:Lflipboard/gui/FLBitmapView$LayerType;

    new-instance v0, Lflipboard/gui/FLBitmapView$LayerType;

    const-string v1, "software"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLBitmapView$LayerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLBitmapView$LayerType;->b:Lflipboard/gui/FLBitmapView$LayerType;

    new-instance v0, Lflipboard/gui/FLBitmapView$LayerType;

    const-string v1, "hardware"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/FLBitmapView$LayerType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLBitmapView$LayerType;->c:Lflipboard/gui/FLBitmapView$LayerType;

    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/gui/FLBitmapView$LayerType;

    sget-object v1, Lflipboard/gui/FLBitmapView$LayerType;->a:Lflipboard/gui/FLBitmapView$LayerType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLBitmapView$LayerType;->b:Lflipboard/gui/FLBitmapView$LayerType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/FLBitmapView$LayerType;->c:Lflipboard/gui/FLBitmapView$LayerType;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/gui/FLBitmapView$LayerType;->d:[Lflipboard/gui/FLBitmapView$LayerType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1020
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLBitmapView$LayerType;
    .locals 1

    .prologue
    .line 1020
    const-class v0, Lflipboard/gui/FLBitmapView$LayerType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBitmapView$LayerType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLBitmapView$LayerType;
    .locals 1

    .prologue
    .line 1020
    sget-object v0, Lflipboard/gui/FLBitmapView$LayerType;->d:[Lflipboard/gui/FLBitmapView$LayerType;

    invoke-virtual {v0}, [Lflipboard/gui/FLBitmapView$LayerType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLBitmapView$LayerType;

    return-object v0
.end method
