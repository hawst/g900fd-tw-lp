.class public Lflipboard/gui/FLSearchView$CollapsedItemList;
.super Lflipboard/objs/SearchResultItem;
.source "FLSearchView.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/objs/SearchResultItem;",
        "Ljava/lang/Comparable",
        "<",
        "Lflipboard/objs/SearchResultItem;",
        ">;"
    }
.end annotation


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultItem;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultItem;",
            ">;"
        }
    .end annotation
.end field

.field c:Z

.field final synthetic d:Lflipboard/gui/FLSearchView;


# direct methods
.method public constructor <init>(Lflipboard/gui/FLSearchView;Ljava/lang/String;FF)V
    .locals 1

    .prologue
    .line 1322
    iput-object p1, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->d:Lflipboard/gui/FLSearchView;

    invoke-direct {p0}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 1317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    .line 1318
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    .line 1323
    iput-object p2, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->F:Ljava/lang/String;

    .line 1324
    iput p3, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->H:F

    .line 1325
    iput p4, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->E:F

    .line 1326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->c:Z

    .line 1327
    sget-object v0, Lflipboard/gui/FLSearchView$CollapsedItemList;->g:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->x:Ljava/lang/String;

    .line 1328
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1342
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->c:Z

    if-eqz v0, :cond_1

    .line 1343
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->c:Z

    .line 1346
    iget-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1347
    iget-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->d:Lflipboard/gui/FLSearchView;

    iget-object v0, v0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0, p0}, Lflipboard/gui/SearchListAdapter;->b(Lflipboard/objs/SearchResultItem;)I

    move-result v0

    .line 1350
    iget-object v1, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    iget-object v2, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->d:Lflipboard/gui/FLSearchView;

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1351
    iget-object v1, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 1352
    iget-object v2, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1353
    iget-object v2, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->d:Lflipboard/gui/FLSearchView;

    iget-object v4, v2, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v4, v1, v0}, Lflipboard/gui/SearchListAdapter;->a(ILflipboard/objs/SearchResultItem;)V

    move v1, v2

    .line 1354
    goto :goto_0

    .line 1355
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1356
    iget-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->d:Lflipboard/gui/FLSearchView;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1358
    :cond_1
    return-void
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 1314
    check-cast p1, Lflipboard/objs/SearchResultItem;

    iget v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->H:F

    iget v1, p1, Lflipboard/objs/SearchResultItem;->H:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->H:F

    iget v1, p1, Lflipboard/objs/SearchResultItem;->H:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLSearchView$CollapsedItemList;->F:Ljava/lang/String;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
