.class Lflipboard/gui/KenBurnsImageView$CoverImageView;
.super Lflipboard/gui/FLImageView;
.source "KenBurnsImageView.java"


# instance fields
.field f:Lflipboard/objs/Image;

.field final synthetic g:Lflipboard/gui/KenBurnsImageView;


# direct methods
.method constructor <init>(Lflipboard/gui/KenBurnsImageView;Landroid/content/Context;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 104
    iput-object p1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    .line 105
    invoke-direct {p0, p2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;)V

    .line 106
    sget-object v0, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {p0, v0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 107
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->setScaling(Z)V

    .line 108
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 109
    return-void
.end method


# virtual methods
.method protected final a(Lflipboard/io/BitmapManager$Handle;)V
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 121
    new-instance v11, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x1

    invoke-direct {v11, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    .line 124
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 125
    new-instance v1, Lflipboard/gui/KenBurnsImageView$CoverImageView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView$1;-><init>(Lflipboard/gui/KenBurnsImageView$CoverImageView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 147
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 149
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 153
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->f:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->f:Lflipboard/objs/Image;

    iget v0, v0, Lflipboard/objs/Image;->f:I

    int-to-float v0, v0

    .line 155
    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->f:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->g:I

    int-to-float v1, v1

    move v2, v1

    move v1, v0

    .line 160
    :goto_0
    const/4 v0, 0x0

    .line 161
    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    .line 162
    div-float v0, v1, v2

    .line 164
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 165
    sub-float v4, v0, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 169
    cmpl-float v5, v3, v0

    if-lez v5, :cond_3

    .line 170
    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->getWidth()I

    move-result v5

    int-to-float v5, v5

    div-float v1, v5, v1

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 175
    :goto_1
    iget-object v2, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    invoke-static {v2}, Lflipboard/gui/KenBurnsImageView;->b(Lflipboard/gui/KenBurnsImageView;)Lflipboard/gui/KenBurnsImageView$AnimationType;

    move-result-object v2

    sget-object v5, Lflipboard/gui/KenBurnsImageView$AnimationType;->a:Lflipboard/gui/KenBurnsImageView$AnimationType;

    if-eq v2, v5, :cond_6

    float-to-double v6, v4

    const-wide v8, 0x3fc999999999999aL    # 0.2

    cmpg-double v2, v6, v8

    if-ltz v2, :cond_1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v4, v2

    if-gez v2, :cond_6

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide v6, 0x3fc999999999999aL    # 0.2

    cmpl-double v2, v4, v6

    if-lez v2, :cond_6

    .line 177
    :cond_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 178
    const-wide/16 v2, 0x1388

    move-wide v12, v2

    .line 182
    :goto_2
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    add-double/2addr v2, v4

    double-to-float v4, v2

    .line 183
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v2, v6

    if-lez v0, :cond_5

    .line 187
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    add-float v2, v4, v1

    add-float/2addr v4, v1

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    move v3, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 191
    :goto_3
    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v2, Lflipboard/gui/KenBurnsImageView$AnimationType;->a:Lflipboard/gui/KenBurnsImageView$AnimationType;

    invoke-static {v1, v2}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$AnimationType;)Lflipboard/gui/KenBurnsImageView$AnimationType;

    .line 228
    :goto_4
    invoke-virtual {v0, v12, v13}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 229
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 230
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 231
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 232
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    .line 234
    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->setFillAfter(Z)V

    .line 235
    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->setFillBefore(Z)V

    .line 236
    invoke-virtual {p0, v11}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 237
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->setVisibility(I)V

    .line 240
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->P:Landroid/os/Handler;

    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    iget-object v1, v1, Lflipboard/gui/KenBurnsImageView;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 241
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    iget-object v1, v1, Lflipboard/gui/KenBurnsImageView;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    add-long/2addr v2, v12

    const-wide/16 v4, 0xc8

    sub-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;J)V

    .line 242
    return-void

    .line 157
    :cond_2
    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v0

    int-to-float v0, v0

    .line 158
    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v1

    int-to-float v1, v1

    move v2, v1

    move v1, v0

    goto/16 :goto_0

    .line 172
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->getHeight()I

    move-result v5

    int-to-float v5, v5

    div-float v2, v5, v2

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    goto/16 :goto_1

    .line 180
    :cond_4
    const-wide/16 v2, 0x1b58

    move-wide v12, v2

    goto/16 :goto_2

    .line 189
    :cond_5
    new-instance v2, Landroid/view/animation/ScaleAnimation;

    add-float v3, v4, v1

    add-float v5, v4, v1

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v9, 0x1

    const/high16 v10, 0x3f000000    # 0.5f

    move v4, v1

    move v6, v1

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    move-object v0, v2

    goto :goto_3

    .line 194
    :cond_6
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v2}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 195
    const-wide/16 v12, 0x157c

    .line 200
    :goto_5
    cmpl-float v0, v0, v3

    if-lez v0, :cond_9

    .line 202
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    invoke-static {v0}, Lflipboard/gui/KenBurnsImageView;->c(Lflipboard/gui/KenBurnsImageView;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    move-result-object v0

    sget-object v2, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    if-ne v0, v2, :cond_8

    .line 203
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v6, v1, v0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 204
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v3, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->a:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    invoke-static {v0, v3}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$TranslationDirection;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    move-object v9, v2

    .line 209
    :goto_6
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/4 v5, 0x1

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 210
    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v2, Lflipboard/gui/KenBurnsImageView$AnimationType;->b:Lflipboard/gui/KenBurnsImageView$AnimationType;

    invoke-static {v1, v2}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$AnimationType;)Lflipboard/gui/KenBurnsImageView$AnimationType;

    .line 222
    :goto_7
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v9, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 223
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 224
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 225
    invoke-virtual {v11, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object v0, v9

    goto/16 :goto_4

    .line 197
    :cond_7
    const-wide/16 v12, 0x2328

    goto :goto_5

    .line 206
    :cond_8
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v4, v1, v0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 207
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v3, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    invoke-static {v0, v3}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$TranslationDirection;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    move-object v9, v2

    goto :goto_6

    .line 212
    :cond_9
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    invoke-static {v0}, Lflipboard/gui/KenBurnsImageView;->c(Lflipboard/gui/KenBurnsImageView;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    move-result-object v0

    sget-object v2, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    if-ne v0, v2, :cond_a

    .line 213
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v10, v1, v0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 214
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v3, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->a:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    invoke-static {v0, v3}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$TranslationDirection;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    move-object v9, v2

    .line 219
    :goto_8
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 220
    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v2, Lflipboard/gui/KenBurnsImageView$AnimationType;->c:Lflipboard/gui/KenBurnsImageView$AnimationType;

    invoke-static {v1, v2}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$AnimationType;)Lflipboard/gui/KenBurnsImageView$AnimationType;

    goto :goto_7

    .line 216
    :cond_a
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v8, v1, v0

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 217
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->g:Lflipboard/gui/KenBurnsImageView;

    sget-object v3, Lflipboard/gui/KenBurnsImageView$TranslationDirection;->b:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    invoke-static {v0, v3}, Lflipboard/gui/KenBurnsImageView;->a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$TranslationDirection;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    move-object v9, v2

    goto :goto_8
.end method

.method public setImage(Lflipboard/objs/Image;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lflipboard/gui/KenBurnsImageView$CoverImageView;->f:Lflipboard/objs/Image;

    .line 115
    invoke-super {p0, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 116
    return-void
.end method
