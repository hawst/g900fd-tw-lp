.class Lflipboard/gui/FLSearchBar$1;
.super Ljava/lang/Object;
.source "FLSearchBar.java"

# interfaces
.implements Lflipboard/service/Flap$SearchObserver;


# instance fields
.field final synthetic a:Lflipboard/gui/FLSearchBar;


# direct methods
.method constructor <init>(Lflipboard/gui/FLSearchBar;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lflipboard/gui/FLSearchBar$1;->a:Lflipboard/gui/FLSearchBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 68
    return-void
.end method

.method public final a(Ljava/lang/String;Lflipboard/objs/SearchResultItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 52
    sget-object v0, Lflipboard/gui/FLSearchBar;->b:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    aput-object p2, v0, v3

    .line 53
    iget-object v0, p0, Lflipboard/gui/FLSearchBar$1;->a:Lflipboard/gui/FLSearchBar;

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->d:Lflipboard/gui/FLSearchBar$Input;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    aput-object p1, v2, v4

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchBar;->a(Lflipboard/gui/FLSearchBar$Input;[Ljava/lang/Object;)V

    .line 54
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;IJ)V"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lflipboard/gui/FLSearchBar$1;->a:Lflipboard/gui/FLSearchBar;

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->d:Lflipboard/gui/FLSearchBar$Input;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchBar;->a(Lflipboard/gui/FLSearchBar$Input;[Ljava/lang/Object;)V

    .line 64
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lflipboard/gui/FLSearchBar$1;->a:Lflipboard/gui/FLSearchBar;

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->d:Lflipboard/gui/FLSearchBar$Input;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchBar;->a(Lflipboard/gui/FLSearchBar$Input;[Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 72
    sget-object v0, Lflipboard/gui/FLSearchBar;->b:Lflipboard/util/Log;

    const-string v1, "search error: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/FLSearchBar$1;->a:Lflipboard/gui/FLSearchBar;

    invoke-static {v0}, Lflipboard/gui/FLSearchBar;->a(Lflipboard/gui/FLSearchBar;)Lflipboard/gui/FLSearchBar$SearchResultObserver;

    move-result-object v0

    new-instance v1, Ljava/lang/Exception;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, p3, p4}, Lflipboard/gui/FLSearchBar$SearchResultObserver;->a(J)V

    .line 74
    return-void
.end method
