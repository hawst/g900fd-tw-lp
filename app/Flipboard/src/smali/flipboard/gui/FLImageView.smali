.class public Lflipboard/gui/FLImageView;
.super Landroid/widget/FrameLayout;
.source "FLImageView.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;


# static fields
.field public static d:Lflipboard/util/Log;

.field private static v:Landroid/graphics/Paint;


# instance fields
.field a:Lflipboard/gui/FLImageView$Listener;

.field b:Z

.field c:Lflipboard/gui/FLBitmapView;

.field e:Z

.field private f:Landroid/widget/FrameLayout;

.field private g:Landroid/widget/FrameLayout;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:I

.field private l:Ljava/lang/String;

.field private m:Lflipboard/objs/Image;

.field private n:Landroid/graphics/PointF;

.field private o:Lflipboard/io/Download;

.field private p:Lflipboard/io/Download$Observer;

.field private q:Z

.field private r:Z

.field private s:Lflipboard/gui/FLBusyView;

.field private t:Z

.field private u:Landroid/widget/ImageButton;

.field private final w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 75
    const-string v0, "image"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/FLImageView;->k:I

    .line 94
    new-instance v0, Lflipboard/gui/FLBitmapView;

    invoke-direct {v0, p1, p0}, Lflipboard/gui/FLBitmapView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    .line 95
    sget-object v0, Lflipboard/app/R$styleable;->FLImageView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, v3}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLBitmapView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 97
    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lflipboard/gui/FLBitmapView;->setRecycle(Z)V

    .line 98
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/gui/FLImageView;->e:Z

    .line 99
    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-boolean v2, p0, Lflipboard/gui/FLImageView;->e:Z

    invoke-virtual {v1, v2}, Lflipboard/gui/FLBitmapView;->setFade(Z)V

    .line 100
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/gui/FLImageView;->w:Z

    .line 101
    iget-boolean v1, p0, Lflipboard/gui/FLImageView;->w:Z

    invoke-virtual {p0, v1}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    .line 102
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "animate_gifs"

    sget-boolean v3, Lflipboard/activities/SettingsFragment;->a:Z

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/gui/FLImageView;->i:Z

    .line 104
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 106
    if-eqz v1, :cond_0

    .line 107
    const-string v0, "fit"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    sget-object v1, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 113
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->addView(Landroid/view/View;)V

    .line 114
    return-void

    .line 109
    :cond_1
    const-string v0, "fill"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    sget-object v1, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V
    .locals 3

    .prologue
    .line 118
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/FLImageView;->k:I

    .line 119
    new-instance v0, Lflipboard/gui/FLBitmapView;

    invoke-direct {v0, p1, p0}, Lflipboard/gui/FLBitmapView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    .line 120
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {p2}, Lflipboard/gui/FLImageView;->getPlaceholder()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 121
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, p2, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-boolean v1, v1, Lflipboard/gui/FLBitmapView;->j:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setRecycle(Z)V

    .line 122
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-boolean v1, p2, Lflipboard/gui/FLImageView;->e:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setFade(Z)V

    .line 123
    iget-boolean v0, p2, Lflipboard/gui/FLImageView;->e:Z

    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->e:Z

    .line 124
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {p2}, Lflipboard/gui/FLImageView;->getAlign()Lflipboard/gui/FLImageView$Align;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, p2, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-boolean v1, v1, Lflipboard/gui/FLBitmapView;->i:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setAllowPreloadOnUIThread(Z)V

    .line 126
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, p2, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, v1, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setSize(Landroid/graphics/Point;)V

    .line 127
    iget-boolean v0, p2, Lflipboard/gui/FLImageView;->w:Z

    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->w:Z

    .line 128
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-boolean v0, v0, Lflipboard/gui/FLBitmapView;->h:Z

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    .line 129
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "animate_gifs"

    sget-boolean v2, Lflipboard/activities/SettingsFragment;->a:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->i:Z

    .line 130
    invoke-virtual {p2}, Lflipboard/gui/FLImageView;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->setId(I)V

    .line 131
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->addView(Landroid/view/View;)V

    .line 132
    return-void
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 461
    const/high16 v0, -0x80000000

    if-ne p1, v0, :cond_0

    .line 462
    const/4 v0, 0x1

    .line 467
    :goto_0
    return v0

    .line 464
    :cond_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 465
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->b:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    const/16 v0, 0x64

    goto :goto_0

    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 467
    :cond_2
    const/16 v1, 0xa

    if-gez p1, :cond_3

    mul-int/lit8 v0, p1, 0x2

    rsub-int/lit8 v0, v0, 0x5a

    :goto_1
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_3
    rsub-int/lit8 v0, p1, 0x64

    goto :goto_1
.end method

.method static synthetic a(Lflipboard/gui/FLImageView;)Lflipboard/io/Download$Observer;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLImageView;->a(Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLImageView;Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLImageView;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private declared-synchronized a(Lflipboard/io/BitmapManager$Handle;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 472
    monitor-enter p0

    :try_start_0
    sget-object v1, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    .line 474
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/gui/FLImageView;->n:Landroid/graphics/PointF;

    .line 475
    if-eqz p1, :cond_0

    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    if-eqz v1, :cond_0

    .line 476
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getOriginalSize()Landroid/graphics/Point;

    move-result-object v0

    .line 477
    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    if-eqz v1, :cond_0

    .line 478
    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    invoke-virtual {v1}, Lflipboard/objs/Image;->e()Landroid/graphics/PointF;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/FLImageView;->n:Landroid/graphics/PointF;

    .line 483
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/FLImageView$2;

    invoke-direct {v2, p0, p1, v0}, Lflipboard/gui/FLImageView$2;-><init>(Lflipboard/gui/FLImageView;Lflipboard/io/BitmapManager$Handle;Landroid/graphics/Point;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 489
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    monitor-exit p0

    return-void

    .line 472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 334
    monitor-enter p0

    :try_start_0
    sget-object v1, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 335
    if-eqz p1, :cond_0

    const-string v1, "http"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 336
    sget-object v1, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    move-object p1, v0

    .line 339
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->l:Ljava/lang/String;

    if-nez v0, :cond_4

    if-eqz p1, :cond_3

    .line 340
    :goto_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->a()V

    .line 341
    iput-object p1, p0, Lflipboard/gui/FLImageView;->l:Ljava/lang/String;

    .line 342
    iput-object p2, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    .line 343
    iput-object p3, p0, Lflipboard/gui/FLImageView;->n:Landroid/graphics/PointF;

    .line 345
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_1

    .line 346
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    .line 349
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    if-eqz v0, :cond_2

    .line 350
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    .line 351
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    :cond_2
    if-nez p1, :cond_5

    .line 454
    :cond_3
    :goto_1
    monitor-exit p0

    return-void

    .line 339
    :cond_4
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->l:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    goto :goto_0

    .line 357
    :cond_5
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getOriginalSize()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setSize(Landroid/graphics/Point;)V

    .line 359
    sget-object v0, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    .line 360
    new-instance v0, Lflipboard/gui/FLImageView$1;

    invoke-direct {v0, p0, p4}, Lflipboard/gui/FLImageView$1;-><init>(Lflipboard/gui/FLImageView;Ljava/lang/String;)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    .line 446
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V

    .line 450
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_3

    .line 451
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v1, v1, Lflipboard/gui/FLBitmapView;->k:I

    invoke-direct {p0, v1}, Lflipboard/gui/FLImageView;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/io/Download$Observer;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;ZZ)V
    .locals 3

    .prologue
    .line 497
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/gui/FLImageView;->d:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v2, v2, Lflipboard/gui/FLBitmapView;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 498
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->r:Z

    if-eq v0, p2, :cond_0

    .line 499
    iput-boolean p2, p0, Lflipboard/gui/FLImageView;->r:Z

    .line 500
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v0, v0, Lflipboard/gui/FLBitmapView;->k:I

    if-nez v0, :cond_0

    .line 501
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLImageView$3;

    invoke-direct {v1, p0}, Lflipboard/gui/FLImageView$3;-><init>(Lflipboard/gui/FLImageView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 509
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->t:Z

    if-eq v0, p3, :cond_1

    .line 510
    iput-boolean p3, p0, Lflipboard/gui/FLImageView;->t:Z

    .line 511
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLImageView$4;

    invoke-direct {v1, p0}, Lflipboard/gui/FLImageView$4;-><init>(Lflipboard/gui/FLImageView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    :cond_1
    monitor-exit p0

    return-void

    .line 497
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lflipboard/gui/FLImageView;)Lflipboard/io/Download$Observer;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 4

    .prologue
    .line 547
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLImageView:updateBusy"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 549
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->r:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v0, v0, Lflipboard/gui/FLBitmapView;->k:I

    if-nez v0, :cond_2

    .line 550
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    if-nez v0, :cond_1

    .line 551
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    .line 554
    new-instance v0, Lflipboard/gui/FLBusyView;

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x10300c8

    invoke-direct {v0, v1, v2, v3}, Lflipboard/gui/FLBusyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    .line 555
    iget-object v0, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 556
    iget-object v0, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->addView(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 563
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 557
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0}, Lflipboard/gui/FLBusyView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 560
    :cond_2
    :try_start_2
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0}, Lflipboard/gui/FLBusyView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 561
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private b(II)Z
    .locals 3

    .prologue
    const/16 v2, 0x3c

    .line 541
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lflipboard/util/AndroidUtil;->a(ILandroid/content/Context;)I

    move-result v0

    .line 542
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p2, v1}, Lflipboard/util/AndroidUtil;->a(ILandroid/content/Context;)I

    move-result v1

    .line 543
    if-le v0, v2, :cond_0

    if-le v1, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lflipboard/gui/FLImageView;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    return-object v0
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 579
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLImageView:updateTap"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 581
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->t:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->b:Z

    if-nez v0, :cond_2

    .line 582
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    if-nez v0, :cond_1

    .line 583
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    .line 584
    new-instance v0, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    .line 585
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 586
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    .line 587
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const v1, 0x7f020171

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 588
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 589
    iget-object v0, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 590
    iget-object v0, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->addView(Landroid/view/View;)V

    .line 592
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    new-instance v1, Lflipboard/gui/FLImageView$5;

    invoke-direct {v1, p0}, Lflipboard/gui/FLImageView$5;-><init>(Lflipboard/gui/FLImageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 612
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 602
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 604
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 605
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 579
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 607
    :cond_2
    :try_start_2
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 608
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 609
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 610
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 616
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 617
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 624
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 620
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->b:Z

    .line 621
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v1, v1, Lflipboard/gui/FLBitmapView;->k:I

    invoke-direct {p0, v1}, Lflipboard/gui/FLImageView;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/io/Download$Observer;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 616
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lflipboard/gui/FLImageView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->i:Z

    return v0
.end method

.method static synthetic e(Lflipboard/gui/FLImageView;)Z
    .locals 1

    .prologue
    .line 38
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->j:Z

    return v0
.end method

.method static synthetic f(Lflipboard/gui/FLImageView;)Lflipboard/gui/FLBusyView;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    return-object v0
.end method

.method static synthetic g(Lflipboard/gui/FLImageView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lflipboard/gui/FLImageView;)Lflipboard/objs/Image;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    return-object v0
.end method

.method static synthetic i(Lflipboard/gui/FLImageView;)Landroid/graphics/PointF;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->n:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic j(Lflipboard/gui/FLImageView;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lflipboard/gui/FLImageView;->b()V

    return-void
.end method

.method static synthetic k(Lflipboard/gui/FLImageView;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lflipboard/gui/FLImageView;->c()V

    return-void
.end method

.method static synthetic l(Lflipboard/gui/FLImageView;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic m(Lflipboard/gui/FLImageView;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic n(Lflipboard/gui/FLImageView;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lflipboard/gui/FLImageView;->d()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_0

    .line 185
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    .line 188
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    if-eqz v0, :cond_1

    .line 189
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    .line 192
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->l:Ljava/lang/String;

    .line 193
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    .line 194
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->n:Landroid/graphics/PointF;

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->h:Z

    .line 196
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_2

    .line 197
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setProgress(I)V

    .line 198
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    .line 200
    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->w:Z

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    .line 201
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1, p1, p2}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLBitmapView;->setSize(Landroid/graphics/Point;)V

    .line 271
    return-void
.end method

.method public final a(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/FLBitmapView;->a(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 330
    return-void
.end method

.method public a(Lflipboard/io/BitmapManager$Handle;)V
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    invoke-interface {v0, p1}, Lflipboard/gui/FLImageView$Listener;->a(Lflipboard/io/BitmapManager$Handle;)V

    .line 700
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->b:Z

    .line 701
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/PointF;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 240
    invoke-direct {p0, p1, v0, p2, v0}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method public final declared-synchronized a(ZI)V
    .locals 4

    .prologue
    .line 824
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "animate_gifs"

    sget-boolean v2, Lflipboard/activities/SettingsFragment;->a:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 825
    iget-boolean v1, p0, Lflipboard/gui/FLImageView;->i:Z

    if-eq v0, v1, :cond_0

    .line 826
    iput-boolean v0, p0, Lflipboard/gui/FLImageView;->i:Z

    .line 827
    iget-object v0, p0, Lflipboard/gui/FLImageView;->n:Landroid/graphics/PointF;

    .line 828
    iget-object v1, p0, Lflipboard/gui/FLImageView;->l:Ljava/lang/String;

    .line 829
    iget-object v2, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    .line 830
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->a()V

    .line 831
    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    .line 833
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_1

    .line 834
    iget-object v1, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz p1, :cond_2

    invoke-direct {p0, p2}, Lflipboard/gui/FLImageView;->a(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/io/Download$Observer;->a(I)V

    .line 836
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/FLBitmapView;->a(ZI)V

    .line 837
    invoke-direct {p0}, Lflipboard/gui/FLImageView;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 838
    monitor-exit p0

    return-void

    .line 834
    :cond_2
    const/4 v0, 0x5

    goto :goto_0

    .line 824
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v4, 0x40400000    # 3.0f

    const/4 v1, 0x0

    .line 712
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 714
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 753
    :cond_0
    :goto_0
    return-void

    .line 717
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 718
    const-string v2, "enable_image_selection_overlay"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 719
    if-eqz v0, :cond_0

    .line 720
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 722
    sget-object v2, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    if-nez v2, :cond_2

    .line 723
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 724
    sput-object v2, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v0, v3

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 725
    sget-object v0, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    const v2, -0xff0001

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 726
    sget-object v0, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    const/high16 v2, 0x40a00000    # 5.0f

    const/high16 v3, -0x1000000

    invoke-virtual {v0, v2, v4, v4, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 728
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    .line 729
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    .line 730
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 731
    const v0, 0x4305547b    # 133.33f

    invoke-static {v0, v3}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    if-ge v4, v0, :cond_3

    const/4 v0, 0x1

    .line 732
    :goto_1
    const/high16 v2, 0x41000000    # 8.0f

    invoke-static {v2, v3}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v2

    .line 733
    const v6, 0x420551ec    # 33.33f

    invoke-static {v6, v3}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v3

    .line 734
    if-eqz v0, :cond_4

    move v0, v2

    .line 735
    :goto_2
    int-to-float v2, v0

    .line 736
    int-to-float v0, v0

    .line 737
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "V: "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v2, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 738
    sget-object v3, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->descent()F

    move-result v3

    sget-object v4, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v4

    sub-float/2addr v3, v4

    .line 739
    iget-object v4, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget-object v4, v4, Lflipboard/objs/Image;->k:Ljava/lang/String;

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    :goto_3
    if-ge v1, v5, :cond_5

    aget-object v6, v4, v1

    .line 740
    add-float/2addr v0, v3

    .line 741
    sget-object v7, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 739
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    move v0, v1

    .line 731
    goto :goto_1

    :cond_4
    move v0, v3

    .line 734
    goto :goto_2

    .line 743
    :cond_5
    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, v1, Lflipboard/gui/FLBitmapView;->f:Landroid/graphics/Point;

    .line 744
    if-eqz v1, :cond_6

    .line 745
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "A: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-float v4, v0, v3

    sget-object v5, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 747
    :cond_6
    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget-object v1, v1, Lflipboard/gui/FLBitmapView;->g:Landroid/graphics/Point;

    .line 748
    if-eqz v1, :cond_0

    .line 749
    add-float/2addr v0, v3

    .line 750
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Sc: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "x"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-float/2addr v0, v3

    sget-object v3, Lflipboard/gui/FLImageView;->v:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public getAlign()Lflipboard/gui/FLImageView$Align;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->getAlign()Lflipboard/gui/FLImageView$Align;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getAspectRatio()F
    .locals 2

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 160
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    if-eqz v1, :cond_0

    .line 161
    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->f:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->g:I

    if-lez v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v0, v0, Lflipboard/objs/Image;->f:I

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 164
    :cond_0
    monitor-exit p0

    return v0

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getImageRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized getOriginalSize()Landroid/graphics/Point;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 148
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    if-eqz v1, :cond_0

    .line 149
    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->f:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->g:I

    if-lez v1, :cond_0

    new-instance v0, Landroid/graphics/Point;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v1, v1, Lflipboard/objs/Image;->f:I

    iget-object v2, p0, Lflipboard/gui/FLImageView;->m:Lflipboard/objs/Image;

    iget v2, v2, Lflipboard/objs/Image;->g:I

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :cond_0
    monitor-exit p0

    return-object v0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getPageOffset()I
    .locals 1

    .prologue
    .line 842
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x80000000

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lflipboard/gui/ContainerView;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public getPlaceholder()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->getPlaceholder()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized onAttachedToWindow()V
    .locals 3

    .prologue
    .line 789
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 790
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    const/4 v1, 0x1

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getPageOffset()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLBitmapView;->a(ZI)V

    .line 791
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v0, v0, Lflipboard/gui/FLBitmapView;->k:I

    if-nez v0, :cond_0

    .line 792
    invoke-direct {p0}, Lflipboard/gui/FLImageView;->b()V

    .line 794
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_1

    .line 795
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    iget v1, v1, Lflipboard/gui/FLBitmapView;->k:I

    invoke-direct {p0, v1}, Lflipboard/gui/FLImageView;->a(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/io/Download$Observer;->a(I)V

    .line 797
    :cond_1
    invoke-direct {p0}, Lflipboard/gui/FLImageView;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 798
    monitor-exit p0

    return-void

    .line 789
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 806
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    if-eqz v0, :cond_0

    .line 807
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    iget-object v1, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    invoke-virtual {v0, v1}, Lflipboard/io/Download;->b(Lflipboard/io/Download$Observer;)V

    .line 808
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->p:Lflipboard/io/Download$Observer;

    .line 810
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    if-eqz v0, :cond_1

    .line 811
    iget-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    invoke-virtual {v0}, Lflipboard/io/Download;->b()V

    .line 812
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLImageView;->o:Lflipboard/io/Download;

    .line 814
    :cond_1
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 815
    monitor-exit p0

    return-void

    .line 806
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 706
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 708
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 656
    sub-int v2, p4, p2

    .line 657
    sub-int v3, p5, p3

    .line 658
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, v1, v1, v2, v3}, Lflipboard/gui/FLBitmapView;->layout(IIII)V

    .line 660
    iget-object v0, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    .line 664
    iget-boolean v4, p0, Lflipboard/gui/FLImageView;->q:Z

    if-eqz v4, :cond_3

    .line 665
    add-int/lit8 v4, v0, 0xa

    sub-int v4, v2, v4

    .line 666
    add-int/lit8 v5, v0, 0xa

    sub-int v5, v3, v5

    .line 667
    iget-object v6, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    add-int v7, v4, v0

    add-int/2addr v0, v5

    invoke-virtual {v6, v4, v5, v7, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 674
    :goto_0
    invoke-direct {p0, v2, v3}, Lflipboard/gui/FLImageView;->b(II)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    .line 675
    :goto_1
    iget-object v4, p0, Lflipboard/gui/FLImageView;->s:Lflipboard/gui/FLBusyView;

    invoke-virtual {v4, v0}, Lflipboard/gui/FLBusyView;->setIndeterminate(Z)V

    .line 677
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 679
    iget-object v0, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    .line 681
    div-int/lit8 v4, v2, 0x2

    div-int/lit8 v5, v0, 0x2

    sub-int/2addr v4, v5

    .line 682
    div-int/lit8 v5, v3, 0x2

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v5, v6

    .line 683
    iget-object v6, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    add-int v7, v4, v0

    add-int v8, v5, v0

    invoke-virtual {v6, v4, v5, v7, v8}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 685
    if-gt v0, v2, :cond_1

    if-le v0, v3, :cond_2

    .line 686
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 689
    :cond_2
    return-void

    .line 669
    :cond_3
    div-int/lit8 v4, v2, 0x2

    div-int/lit8 v5, v0, 0x2

    sub-int/2addr v4, v5

    .line 670
    div-int/lit8 v5, v3, 0x2

    div-int/lit8 v6, v0, 0x2

    sub-int/2addr v5, v6

    .line 671
    iget-object v6, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    add-int v7, v4, v0

    add-int/2addr v0, v5

    invoke-virtual {v6, v4, v5, v7, v0}, Landroid/widget/FrameLayout;->layout(IIII)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 674
    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/16 v0, 0x1e

    const/high16 v6, 0x40000000    # 2.0f

    .line 631
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 632
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 634
    iget-object v1, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_1

    .line 635
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v1, v4}, Lflipboard/util/AndroidUtil;->a(ILandroid/content/Context;)I

    move-result v4

    const/16 v1, 0xf

    invoke-direct {p0, v2, v3}, Lflipboard/gui/FLImageView;->b(II)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_0
    :goto_0
    int-to-float v0, v0

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    .line 636
    iget-object v1, p0, Lflipboard/gui/FLImageView;->f:Landroid/widget/FrameLayout;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/FrameLayout;->measure(II)V

    .line 638
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_3

    .line 639
    const/4 v0, 0x2

    const/high16 v1, 0x42040000    # 33.0f

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v0, v1, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    mul-int/lit8 v1, v0, 0x5

    if-le v2, v1, :cond_2

    mul-int/lit8 v1, v0, 0x5

    if-le v3, v1, :cond_2

    mul-int/lit8 v0, v0, 0x2

    .line 640
    :cond_2
    iget-object v1, p0, Lflipboard/gui/FLImageView;->g:Landroid/widget/FrameLayout;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->measure(II)V

    .line 642
    :cond_3
    iget v0, p0, Lflipboard/gui/FLImageView;->k:I

    if-ltz v0, :cond_5

    .line 643
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lflipboard/gui/FLImageView;->k:I

    if-le v3, v0, :cond_5

    .line 644
    :cond_4
    iget v0, p0, Lflipboard/gui/FLImageView;->k:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 647
    :cond_5
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/FLBitmapView;->measure(II)V

    .line 648
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0}, Lflipboard/gui/FLBitmapView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v1}, Lflipboard/gui/FLBitmapView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLImageView;->setMeasuredDimension(II)V

    .line 649
    return-void

    .line 635
    :cond_6
    const/16 v5, 0x4b

    if-gt v4, v5, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public setAlign(Lflipboard/gui/FLImageView$Align;)V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 279
    return-void
.end method

.method public setAllowPreloadOnUIThread(Z)V
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setAllowPreloadOnUIThread(Z)V

    .line 287
    return-void
.end method

.method public setBitmap(I)V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setBitmap(I)V

    .line 292
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 296
    return-void
.end method

.method public setBusyBottomRight(Z)V
    .locals 0

    .prologue
    .line 136
    iput-boolean p1, p0, Lflipboard/gui/FLImageView;->q:Z

    .line 137
    return-void
.end method

.method public setClipRound(Z)V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setClipRound(Z)V

    .line 320
    return-void
.end method

.method public setDisableLoadingView(Z)V
    .locals 0

    .prologue
    .line 846
    iput-boolean p1, p0, Lflipboard/gui/FLImageView;->j:Z

    .line 847
    return-void
.end method

.method public setFade(Z)V
    .locals 1

    .prologue
    .line 323
    iput-boolean p1, p0, Lflipboard/gui/FLImageView;->e:Z

    .line 324
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setFade(Z)V

    .line 325
    return-void
.end method

.method public setImage(Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 244
    if-nez p1, :cond_0

    .line 245
    invoke-virtual {p0, v0, v0}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 267
    :goto_0
    return-void

    .line 249
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    if-lez v1, :cond_1

    .line 250
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p1, v1, v2}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 254
    :goto_1
    if-nez v2, :cond_2

    .line 255
    invoke-virtual {p0, v0, v0}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    goto :goto_0

    .line 252
    :cond_1
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v1, v1, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v2, v2, Lflipboard/app/FlipboardApplication;->e:I

    invoke-virtual {p1, v1, v2}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    .line 260
    :cond_2
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v3

    .line 262
    if-eqz v3, :cond_3

    .line 263
    invoke-virtual {v3}, Lflipboard/objs/Image;->e()Landroid/graphics/PointF;

    move-result-object v1

    .line 264
    invoke-virtual {v3}, Lflipboard/objs/Image;->i()Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_2
    invoke-direct {p0, v2, v3, v1, v0}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_2
.end method

.method public setImage(Lflipboard/objs/Image;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    if-eqz p1, :cond_0

    .line 211
    invoke-virtual {p1}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lflipboard/objs/Image;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v2, v2, v1}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    .line 213
    :cond_0
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 236
    invoke-direct {p0, p1, v0, v0, v0}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method public setImageBestFit(Lflipboard/objs/Image;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 217
    if-nez p1, :cond_0

    .line 218
    invoke-direct {p0, v2, v2, v2, v2}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    .line 232
    :goto_0
    return-void

    .line 221
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_1

    .line 222
    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lflipboard/objs/Image;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 226
    :goto_1
    if-nez v0, :cond_2

    .line 227
    invoke-direct {p0, v2, v2, v2, v2}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    goto :goto_0

    .line 224
    :cond_1
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v0, v0, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v1, v1, Lflipboard/app/FlipboardApplication;->e:I

    invoke-virtual {p1, v0, v1}, Lflipboard/objs/Image;->a(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 229
    :cond_2
    invoke-virtual {p1}, Lflipboard/objs/Image;->e()Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p1}, Lflipboard/objs/Image;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, p1, v1, v2}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Lflipboard/objs/Image;Landroid/graphics/PointF;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 316
    return-void
.end method

.method public setListener(Lflipboard/gui/FLImageView$Listener;)V
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lflipboard/gui/FLImageView;->h:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 173
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lflipboard/gui/FLImageView$Listener;->a(Lflipboard/io/BitmapManager$Handle;)V

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    iput-object p1, p0, Lflipboard/gui/FLImageView;->a:Lflipboard/gui/FLImageView$Listener;

    goto :goto_0
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 140
    iput p1, p0, Lflipboard/gui/FLImageView;->k:I

    .line 141
    return-void
.end method

.method public setPlaceholder(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setPlaceholder(Landroid/graphics/drawable/Drawable;)V

    .line 275
    return-void
.end method

.method public setRecycle(Z)V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setRecycle(Z)V

    .line 304
    return-void
.end method

.method public setScaling(Z)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lflipboard/gui/FLImageView;->c:Lflipboard/gui/FLBitmapView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLBitmapView;->setScaling(Z)V

    .line 308
    return-void
.end method
