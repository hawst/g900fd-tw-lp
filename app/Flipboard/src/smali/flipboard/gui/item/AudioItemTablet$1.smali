.class Lflipboard/gui/item/AudioItemTablet$1;
.super Ljava/lang/Object;
.source "AudioItemTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/item/AudioItemTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/AudioItemTablet;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 227
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->b:Lflipboard/service/audio/FLAudioManager;

    if-nez v0, :cond_0

    .line 251
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    if-ne v0, v1, :cond_1

    .line 232
    sget-object v0, Lflipboard/gui/item/AudioItemTablet;->c:Lflipboard/util/Log;

    .line 233
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->b:Lflipboard/service/audio/FLAudioManager;

    const-string v1, "bufferingFromTimeline"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->b(Ljava/lang/String;)V

    .line 234
    const-string v0, "timeline_buffering"

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Ljava/lang/String;)V

    .line 245
    :goto_1
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/AudioItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lflipboard/activities/SectionTabletActivity;

    if-eqz v0, :cond_3

    .line 246
    sget-object v0, Lflipboard/gui/item/AudioItemTablet;->c:Lflipboard/util/Log;

    .line 247
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/AudioItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/SectionTabletActivity;

    invoke-virtual {v0}, Lflipboard/activities/SectionTabletActivity;->j()V

    goto :goto_0

    .line 235
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    if-ne v0, v1, :cond_2

    .line 236
    sget-object v0, Lflipboard/gui/item/AudioItemTablet;->c:Lflipboard/util/Log;

    .line 237
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->b:Lflipboard/service/audio/FLAudioManager;

    const-string v1, "pauseFromTimeline"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;)V

    .line 238
    const-string v0, "timeline_play"

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 240
    :cond_2
    sget-object v0, Lflipboard/gui/item/AudioItemTablet;->c:Lflipboard/util/Log;

    .line 241
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->b:Lflipboard/service/audio/FLAudioManager;

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v1, v1, Lflipboard/gui/item/AudioItemTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet$1;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v2, v2, Lflipboard/gui/item/AudioItemTablet;->e:Lflipboard/service/Section;

    const-string v3, "playFromTimeline"

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 242
    const-string v0, "timeline_pause"

    invoke-static {v0}, Lflipboard/service/audio/FLAudioManager;->c(Ljava/lang/String;)V

    goto :goto_1

    .line 249
    :cond_3
    sget-object v0, Lflipboard/gui/item/AudioItemTablet;->c:Lflipboard/util/Log;

    goto :goto_0
.end method
