.class Lflipboard/gui/item/RssDetailView$4;
.super Lflipboard/gui/FLWebChromeClient;
.source "RssDetailView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/item/RssDetailView;


# direct methods
.method constructor <init>(Lflipboard/gui/item/RssDetailView;)V
    .locals 0

    .prologue
    .line 261
    iput-object p1, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-direct {p0}, Lflipboard/gui/FLWebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onHideCustomView()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLVideoDialogFragment;->e()V

    .line 293
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLVideoDialogFragment;->b()V

    .line 295
    :cond_0
    return-void
.end method

.method public onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 267
    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-nez v1, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v1

    if-nez v1, :cond_2

    .line 271
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    new-instance v2, Lflipboard/gui/dialog/FLVideoDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLVideoDialogFragment;-><init>()V

    invoke-static {v1, v2}, Lflipboard/gui/item/RssDetailView;->a(Lflipboard/gui/item/RssDetailView;Lflipboard/gui/dialog/FLVideoDialogFragment;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    .line 272
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v1

    new-instance v2, Lflipboard/gui/item/RssDetailView$4$1;

    invoke-direct {v2, p0}, Lflipboard/gui/item/RssDetailView$4$1;-><init>(Lflipboard/gui/item/RssDetailView$4;)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 283
    :cond_2
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v1

    iget-object v2, v1, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    if-eqz v2, :cond_3

    iget-object v1, v1, Lflipboard/gui/dialog/FLVideoDialogFragment;->l:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v1}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 284
    :goto_1
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$4;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;

    move-result-object v1

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "video_fragment"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLVideoDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    :cond_3
    iput-object p1, v1, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    iput-object p2, v1, Lflipboard/gui/dialog/FLVideoDialogFragment;->l:Landroid/webkit/WebChromeClient$CustomViewCallback;

    goto :goto_1
.end method
