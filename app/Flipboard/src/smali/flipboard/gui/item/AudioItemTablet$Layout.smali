.class final enum Lflipboard/gui/item/AudioItemTablet$Layout;
.super Ljava/lang/Enum;
.source "AudioItemTablet.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/item/AudioItemTablet$Layout;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/item/AudioItemTablet$Layout;

.field public static final enum b:Lflipboard/gui/item/AudioItemTablet$Layout;

.field private static final synthetic c:[Lflipboard/gui/item/AudioItemTablet$Layout;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 95
    new-instance v0, Lflipboard/gui/item/AudioItemTablet$Layout;

    const-string v1, "IMAGE_TOP"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/item/AudioItemTablet$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/item/AudioItemTablet$Layout;->a:Lflipboard/gui/item/AudioItemTablet$Layout;

    new-instance v0, Lflipboard/gui/item/AudioItemTablet$Layout;

    const-string v1, "IMAGE_LEFT"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/item/AudioItemTablet$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/item/AudioItemTablet$Layout;->b:Lflipboard/gui/item/AudioItemTablet$Layout;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/item/AudioItemTablet$Layout;

    sget-object v1, Lflipboard/gui/item/AudioItemTablet$Layout;->a:Lflipboard/gui/item/AudioItemTablet$Layout;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/item/AudioItemTablet$Layout;->b:Lflipboard/gui/item/AudioItemTablet$Layout;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/item/AudioItemTablet$Layout;->c:[Lflipboard/gui/item/AudioItemTablet$Layout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/item/AudioItemTablet$Layout;
    .locals 1

    .prologue
    .line 95
    const-class v0, Lflipboard/gui/item/AudioItemTablet$Layout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/AudioItemTablet$Layout;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/item/AudioItemTablet$Layout;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lflipboard/gui/item/AudioItemTablet$Layout;->c:[Lflipboard/gui/item/AudioItemTablet$Layout;

    invoke-virtual {v0}, [Lflipboard/gui/item/AudioItemTablet$Layout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/item/AudioItemTablet$Layout;

    return-object v0
.end method
