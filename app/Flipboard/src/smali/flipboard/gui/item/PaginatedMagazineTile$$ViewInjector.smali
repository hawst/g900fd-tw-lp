.class public Lflipboard/gui/item/PaginatedMagazineTile$$ViewInjector;
.super Ljava/lang/Object;
.source "PaginatedMagazineTile$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/item/PaginatedMagazineTile;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004f

    const-string v1, "field \'titleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->a:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a0261

    const-string v1, "field \'subTitleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a0299

    invoke-virtual {p0, p2, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    .line 16
    const v0, 0x7f0a0262

    const-string v1, "field \'whiteBorderView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    iput-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    .line 18
    const v0, 0x7f0a0268

    invoke-virtual {p0, p2, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 19
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    .line 20
    const v0, 0x7f0a01a3

    invoke-virtual {p0, p2, v0}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;I)Landroid/view/View;

    move-result-object v0

    .line 21
    check-cast v0, Lflipboard/gui/FollowButton;

    iput-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    .line 22
    return-void
.end method

.method public static reset(Lflipboard/gui/item/PaginatedMagazineTile;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    iput-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->a:Lflipboard/gui/FLTextView;

    .line 26
    iput-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->b:Lflipboard/gui/FLTextView;

    .line 27
    iput-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    .line 28
    iput-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    .line 29
    iput-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    .line 30
    iput-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    .line 31
    return-void
.end method
