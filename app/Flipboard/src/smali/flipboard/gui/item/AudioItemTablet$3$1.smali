.class Lflipboard/gui/item/AudioItemTablet$3$1;
.super Ljava/lang/Object;
.source "AudioItemTablet.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/io/BitmapManager$Handle;

.field final synthetic b:Lflipboard/gui/item/AudioItemTablet$3;


# direct methods
.method constructor <init>(Lflipboard/gui/item/AudioItemTablet$3;Lflipboard/io/BitmapManager$Handle;)V
    .locals 0

    .prologue
    .line 358
    iput-object p1, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iput-object p2, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 361
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v0}, Lflipboard/gui/item/AudioItemTablet;->d(Lflipboard/gui/item/AudioItemTablet;)Lflipboard/gui/FLImageView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 362
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->a:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->a:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 363
    :goto_0
    if-eqz v0, :cond_2

    .line 364
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v1, v1, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v1}, Lflipboard/gui/item/AudioItemTablet;->d(Lflipboard/gui/item/AudioItemTablet;)Lflipboard/gui/FLImageView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 365
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v1, v1, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v1}, Lflipboard/gui/item/AudioItemTablet;->d(Lflipboard/gui/item/AudioItemTablet;)Lflipboard/gui/FLImageView;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v2, v2, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v2, v2, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v2}, Lflipboard/gui/item/AudioItemTablet;->e(Lflipboard/gui/item/AudioItemTablet;)I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v3, v3, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v3, v3, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v4, v4, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v4, v4, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v2, v3, v4}, Lflipboard/gui/item/AudioItemTablet;->a(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 375
    :cond_0
    :goto_1
    return-void

    .line 362
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 368
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v0, v0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getPlaceholder()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 369
    if-eqz v0, :cond_0

    .line 370
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v1, v1, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v1}, Lflipboard/gui/item/AudioItemTablet;->d(Lflipboard/gui/item/AudioItemTablet;)Lflipboard/gui/FLImageView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 371
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v1, v1, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v1}, Lflipboard/gui/item/AudioItemTablet;->d(Lflipboard/gui/item/AudioItemTablet;)Lflipboard/gui/FLImageView;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v2, v2, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v0}, Lflipboard/gui/item/AudioItemTablet;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v2, v2, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    invoke-static {v2}, Lflipboard/gui/item/AudioItemTablet;->e(Lflipboard/gui/item/AudioItemTablet;)I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v3, v3, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v3, v3, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet$3$1;->b:Lflipboard/gui/item/AudioItemTablet$3;

    iget-object v4, v4, Lflipboard/gui/item/AudioItemTablet$3;->a:Lflipboard/gui/item/AudioItemTablet;

    iget-object v4, v4, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v0, v2, v3, v4}, Lflipboard/gui/item/AudioItemTablet;->a(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
