.class Lflipboard/gui/item/FlipmagDetailViewTablet$5;
.super Ljava/lang/Object;
.source "FlipmagDetailViewTablet.java"

# interfaces
.implements Lflipboard/service/Flap$JSONResultObserver;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/gui/item/FlipmagDetailViewTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/FlipmagDetailViewTablet;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->b:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iput-object p2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lflipboard/json/FLObject;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 542
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v3

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 543
    const-string v0, "success"

    invoke-virtual {p1, v0, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sourceMagazineURL"

    invoke-virtual {p1, v0}, Lflipboard/json/FLObject;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sourceMagazineURL"

    invoke-virtual {p1, v0, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 544
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->b:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->b(Lflipboard/gui/item/FlipmagDetailViewTablet;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 545
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->b:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->b(Lflipboard/gui/item/FlipmagDetailViewTablet;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->a:Ljava/lang/String;

    const-string v3, "sourceMagazineURL"

    const/4 v4, 0x0

    invoke-virtual {p1, v3, v4}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 546
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    :cond_0
    return-void

    .line 546
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 552
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$5;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 553
    return-void
.end method
