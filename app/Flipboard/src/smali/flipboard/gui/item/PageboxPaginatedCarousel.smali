.class public Lflipboard/gui/item/PageboxPaginatedCarousel;
.super Lflipboard/gui/FLViewGroup;
.source "PageboxPaginatedCarousel.java"

# interfaces
.implements Lflipboard/gui/item/PageboxView;
.implements Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;
.implements Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/FLViewGroup;",
        "Lflipboard/gui/item/PageboxView;",
        "Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener",
        "<",
        "Lflipboard/objs/FeedItem;",
        ">;",
        "Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter",
        "<",
        "Lflipboard/objs/FeedItem;",
        ">;"
    }
.end annotation


# static fields
.field public static a:I


# instance fields
.field protected b:Lflipboard/gui/FLTextView;

.field protected c:Lflipboard/gui/FLImageView;

.field protected d:Lflipboard/gui/CarouselView;

.field e:Lflipboard/objs/SidebarGroup$RenderHints;

.field f:Ljava/lang/String;

.field g:I

.field private h:Lflipboard/gui/section/scrolling/component/CarouselComponent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/section/scrolling/component/CarouselComponent",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;"
        }
    .end annotation
.end field

.field private i:Lflipboard/objs/FeedItem;

.field private j:Lflipboard/service/Section;

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x5

    sput v0, Lflipboard/gui/item/PageboxPaginatedCarousel;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/PageboxPaginatedCarousel;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->F:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->j:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->s:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v2, v2, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->v:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget v2, v2, Lflipboard/objs/SidebarGroup$RenderHints;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->o:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v0, v1, p1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    return-void
.end method

.method private b(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 190
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->h:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iget-object v1, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->b:Lflipboard/gui/section/scrolling/component/CarouselComponent$CarouselAdapter;

    .line 191
    if-eqz v1, :cond_2

    .line 192
    sget v0, Lflipboard/gui/item/PageboxPaginatedCarousel;->a:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1}, Lflipboard/gui/CarouselView$CarouselAdapter;->c()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->l:I

    .line 193
    :goto_0
    iget v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->l:I

    if-gt p1, v0, :cond_2

    .line 194
    invoke-virtual {v1, p1}, Lflipboard/gui/CarouselView$CarouselAdapter;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 195
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getMeasuredWidth()I

    move-result v2

    if-lez v2, :cond_1

    .line 196
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 200
    :goto_1
    if-eqz v0, :cond_0

    .line 201
    sget-object v2, Lflipboard/io/DownloadManager;->b:Lflipboard/io/DownloadManager;

    invoke-virtual {v2, v0, v4, v4}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    .line 193
    :cond_0
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 198
    :cond_1
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v2, v2, Lflipboard/app/FlipboardApplication;->d:I

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget v3, v3, Lflipboard/app/FlipboardApplication;->e:I

    invoke-virtual {v0, v2, v3}, Lflipboard/objs/FeedItem;->a(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 207
    :cond_2
    return-void
.end method

.method private static getItemViewType$54e235d8()I
    .locals 1

    .prologue
    .line 185
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(I)I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic a(Ljava/lang/Object;ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 31
    check-cast p1, Lflipboard/objs/FeedItem;

    if-eqz p3, :cond_0

    check-cast p3, Lflipboard/gui/item/PaginatedMagazineTile;

    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->j:Lflipboard/service/Section;

    invoke-virtual {p3, v0, p1}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    new-instance v0, Lflipboard/gui/item/PageboxPaginatedCarousel$1;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/item/PageboxPaginatedCarousel$1;-><init>(Lflipboard/gui/item/PageboxPaginatedCarousel;Lflipboard/objs/FeedItem;)V

    invoke-virtual {p3, v0}, Lflipboard/gui/item/PaginatedMagazineTile;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p3

    :cond_0
    invoke-virtual {p4}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300e5

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/PaginatedMagazineTile;

    move-object p3, v0

    goto :goto_0
.end method

.method public final synthetic a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 31
    check-cast p2, Lflipboard/objs/FeedItem;

    if-eqz p2, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    add-int/lit8 v0, p1, 0x2

    iget v1, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->l:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->l:I

    invoke-direct {p0, v0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->b(I)V

    :cond_0
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 130
    iput-object p2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->i:Lflipboard/objs/FeedItem;

    .line 131
    iput-object p1, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->j:Lflipboard/service/Section;

    .line 132
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->j:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->j:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->k:I

    .line 137
    :goto_0
    return-void

    .line 135
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->k:I

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->i:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 68
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 69
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 70
    new-instance v0, Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iget-object v1, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->d:Lflipboard/gui/CarouselView;

    invoke-direct {v0, v1, p0}, Lflipboard/gui/section/scrolling/component/CarouselComponent;-><init>(Lflipboard/gui/CarouselView;Lflipboard/gui/section/scrolling/component/CarouselComponent$ViewAdapter;)V

    iput-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->h:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    .line 71
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->h:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    iput-object p0, v0, Lflipboard/gui/section/scrolling/component/CarouselComponent;->c:Lflipboard/gui/section/scrolling/component/CarouselComponent$OnPageSelectedListener;

    .line 72
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->c:Lflipboard/gui/FLImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, 0x7f080049

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->d:Lflipboard/gui/CarouselView;

    new-instance v1, Lflipboard/gui/PaginatedPagerTransformer;

    invoke-direct {v1}, Lflipboard/gui/PaginatedPagerTransformer;-><init>()V

    invoke-virtual {v0, v1}, Lflipboard/gui/CarouselView;->setPageTransformer$382b7817(Landroid/support/v4/view/ViewPager$PageTransformer;)V

    .line 74
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 94
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingLeft()I

    move-result v0

    .line 95
    sub-int v1, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    .line 96
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->k:I

    add-int/2addr v2, v3

    .line 98
    iget-object v3, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->c:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-static {v3, v4, v0, v1, v5}, Lflipboard/gui/item/PageboxPaginatedCarousel;->a(Landroid/view/View;IIII)I

    .line 100
    iget-object v3, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    const/4 v4, 0x3

    invoke-static {v3, v2, v0, v1, v4}, Lflipboard/gui/item/PageboxPaginatedCarousel;->a(Landroid/view/View;IIII)I

    move-result v3

    add-int/2addr v2, v3

    .line 102
    iget-object v3, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->d:Lflipboard/gui/CarouselView;

    invoke-static {v3, v2, v0, v1, v5}, Lflipboard/gui/item/PageboxPaginatedCarousel;->a(Landroid/view/View;IIII)I

    .line 103
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 79
    iget-object v1, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/item/PageboxPaginatedCarousel;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 81
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 82
    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 83
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxPaginatedCarousel;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->k:I

    sub-int/2addr v1, v2

    .line 84
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 86
    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, p1, p2}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 87
    iget-object v2, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->d:Lflipboard/gui/CarouselView;

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/CarouselView;->measure(II)V

    .line 88
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLViewGroup;->onMeasure(II)V

    .line 90
    return-void
.end method

.method public setPagebox(Lflipboard/objs/SidebarGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 107
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    .line 108
    iget-object v1, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->h:Lflipboard/gui/section/scrolling/component/CarouselComponent;

    invoke-virtual {v1, v0}, Lflipboard/gui/section/scrolling/component/CarouselComponent;->a(Ljava/util/List;)V

    .line 111
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 112
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 113
    iget-object v1, v0, Lflipboard/objs/FeedItem;->cq:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 114
    iget-object v1, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cq:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_0
    :goto_0
    invoke-direct {p0, v2}, Lflipboard/gui/item/PageboxPaginatedCarousel;->b(I)V

    .line 123
    invoke-virtual {p1}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    .line 124
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->j:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->f:Ljava/lang/String;

    .line 125
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->g:I

    .line 126
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
