.class Lflipboard/gui/item/CoverSectionItemTablet$3;
.super Ljava/lang/Object;
.source "CoverSectionItemTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/item/CoverSectionItemTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/CoverSectionItemTablet;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lflipboard/gui/item/CoverSectionItemTablet$3;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$3;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_0

    .line 174
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 175
    check-cast v0, Ljava/lang/String;

    .line 176
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 177
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$3;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    instance-of v1, v0, Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_0

    .line 179
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_2

    .line 180
    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet$3;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    iget-object v2, v1, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    move-object v1, v0

    check-cast v1, Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v1

    check-cast v0, Lflipboard/objs/FeedItem;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {p1, v2, v1, v0, v3}, Lflipboard/util/SocialHelper;->a(Landroid/view/View;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    goto :goto_0

    .line 182
    :cond_2
    check-cast v0, Lflipboard/objs/FeedItem;

    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet$3;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    iget-object v2, v1, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet$3;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v1}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lflipboard/activities/FlipboardActivity;

    sget-object v3, Lflipboard/objs/UsageEventV2$SocialCardNavFrom;->b:Lflipboard/objs/UsageEventV2$SocialCardNavFrom;

    invoke-static {v0, v2, v1, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/UsageEventV2$SocialCardNavFrom;)V

    goto :goto_0
.end method
