.class public Lflipboard/gui/item/EducationModuleView$$ViewInjector;
.super Ljava/lang/Object;
.source "EducationModuleView$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/item/EducationModuleView;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a004f

    const-string v1, "field \'titleTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/item/EducationModuleView;->a:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a0180

    const-string v1, "field \'bodyTextView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/item/EducationModuleView;->b:Lflipboard/gui/FLTextView;

    .line 14
    const v0, 0x7f0a017f

    const-string v1, "field \'backgroundImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/item/EducationModuleView;->c:Lflipboard/gui/FLImageView;

    .line 16
    const v0, 0x7f0a0181

    const-string v1, "field \'buttonView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p1, Lflipboard/gui/item/EducationModuleView;->d:Lflipboard/gui/FLButton;

    .line 18
    return-void
.end method

.method public static reset(Lflipboard/gui/item/EducationModuleView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 21
    iput-object v0, p0, Lflipboard/gui/item/EducationModuleView;->a:Lflipboard/gui/FLTextView;

    .line 22
    iput-object v0, p0, Lflipboard/gui/item/EducationModuleView;->b:Lflipboard/gui/FLTextView;

    .line 23
    iput-object v0, p0, Lflipboard/gui/item/EducationModuleView;->c:Lflipboard/gui/FLImageView;

    .line 24
    iput-object v0, p0, Lflipboard/gui/item/EducationModuleView;->d:Lflipboard/gui/FLButton;

    .line 25
    return-void
.end method
