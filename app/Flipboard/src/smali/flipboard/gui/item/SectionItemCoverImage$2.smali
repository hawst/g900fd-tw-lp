.class Lflipboard/gui/item/SectionItemCoverImage$2;
.super Ljava/lang/Object;
.source "SectionItemCoverImage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/item/SectionItemCoverImage;


# direct methods
.method constructor <init>(Lflipboard/gui/item/SectionItemCoverImage;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 192
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v0, v0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bO:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 199
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 200
    const-string v0, "source"

    const-string v1, "sectionItem"

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const-string v0, "originSectionIdentifier"

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v1, v1, Lflipboard/gui/item/SectionItemCoverImage;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v1, v1, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v0, v0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "section"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v0, v0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v0, v0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 204
    new-instance v0, Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v1, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v1, v1, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v2, v2, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v2, v2, Lflipboard/objs/FeedSection;->d:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v3, v3, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->b:Ljava/lang/String;

    iget-object v4, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v4, v4, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-virtual {v4}, Lflipboard/objs/FeedSection;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v5, v5, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-boolean v5, v5, Lflipboard/objs/FeedSection;->e:Z

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 205
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    if-nez v1, :cond_1

    .line 206
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 208
    :cond_1
    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    invoke-virtual {v1}, Lflipboard/gui/item/SectionItemCoverImage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    invoke-virtual {v1}, Lflipboard/gui/item/SectionItemCoverImage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 212
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    invoke-virtual {v0}, Lflipboard/gui/item/SectionItemCoverImage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FeedActivity;

    .line 213
    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    iget-object v1, v1, Lflipboard/gui/item/SectionItemCoverImage;->a:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage$2;->a:Lflipboard/gui/item/SectionItemCoverImage;

    invoke-virtual {v2}, Lflipboard/gui/item/SectionItemCoverImage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/activities/FeedActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
