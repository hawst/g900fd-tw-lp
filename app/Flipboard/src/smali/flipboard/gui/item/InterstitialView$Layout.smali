.class public final enum Lflipboard/gui/item/InterstitialView$Layout;
.super Ljava/lang/Enum;
.source "InterstitialView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/item/InterstitialView$Layout;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/item/InterstitialView$Layout;

.field public static final enum b:Lflipboard/gui/item/InterstitialView$Layout;

.field public static final enum c:Lflipboard/gui/item/InterstitialView$Layout;

.field private static final synthetic d:[Lflipboard/gui/item/InterstitialView$Layout;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    new-instance v0, Lflipboard/gui/item/InterstitialView$Layout;

    const-string v1, "FULL_BLEED"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/item/InterstitialView$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    new-instance v0, Lflipboard/gui/item/InterstitialView$Layout;

    const-string v1, "IMAGE_BOTTOM"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/item/InterstitialView$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/item/InterstitialView$Layout;->b:Lflipboard/gui/item/InterstitialView$Layout;

    new-instance v0, Lflipboard/gui/item/InterstitialView$Layout;

    const-string v1, "IMAGE_MIDDLE"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/item/InterstitialView$Layout;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/item/InterstitialView$Layout;->c:Lflipboard/gui/item/InterstitialView$Layout;

    .line 59
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/gui/item/InterstitialView$Layout;

    sget-object v1, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/item/InterstitialView$Layout;->b:Lflipboard/gui/item/InterstitialView$Layout;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/item/InterstitialView$Layout;->c:Lflipboard/gui/item/InterstitialView$Layout;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/gui/item/InterstitialView$Layout;->d:[Lflipboard/gui/item/InterstitialView$Layout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/item/InterstitialView$Layout;
    .locals 1

    .prologue
    .line 59
    const-class v0, Lflipboard/gui/item/InterstitialView$Layout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/InterstitialView$Layout;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/item/InterstitialView$Layout;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lflipboard/gui/item/InterstitialView$Layout;->d:[Lflipboard/gui/item/InterstitialView$Layout;

    invoke-virtual {v0}, [Lflipboard/gui/item/InterstitialView$Layout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/item/InterstitialView$Layout;

    return-object v0
.end method
