.class public Lflipboard/gui/item/CoverSectionItemTablet;
.super Lflipboard/gui/FLRelativeLayout;
.source "CoverSectionItemTablet.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# instance fields
.field a:Lflipboard/service/Section;

.field b:Lflipboard/objs/FeedItem;

.field c:Lflipboard/gui/FLImageView;

.field d:Lflipboard/gui/FLImageView;

.field e:Lflipboard/gui/FLTextIntf;

.field f:Lflipboard/gui/FLTextIntf;

.field g:Lflipboard/gui/FLTextIntf;

.field h:Landroid/view/ViewGroup$LayoutParams;

.field i:Z

.field j:Landroid/graphics/drawable/Drawable;

.field k:Landroid/graphics/drawable/Drawable;

.field l:Lflipboard/gui/FLTextIntf;

.field m:Lflipboard/objs/Image;

.field o:Lflipboard/gui/FLLabelTextView;

.field private p:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;",
            ">;"
        }
    .end annotation
.end field

.field private q:Landroid/view/ViewGroup;

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:Landroid/widget/Button;

.field private v:Landroid/widget/Button;

.field private w:Lflipboard/gui/FLButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 87
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/CoverSectionItemTablet;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/item/CoverSectionItemTablet;Lflipboard/objs/CommentaryResult;)V
    .locals 16

    .prologue
    .line 37
    const/4 v9, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v8, v0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    new-instance v1, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;

    iget-object v3, v11, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    iget-object v4, v11, Lflipboard/objs/FeedItem;->as:Ljava/lang/String;

    iget-object v5, v11, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    iget-object v2, v2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v7, v2, Lflipboard/service/Section$Meta;->i:Lflipboard/objs/FeedSectionLink;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/Image;ZLflipboard/objs/FeedSectionLink;)V

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    const/4 v1, 0x0

    if-eqz p1, :cond_19

    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-eqz v2, :cond_19

    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    move v2, v1

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/CommentaryResult$Item;

    iget-object v3, v1, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    iget v1, v1, Lflipboard/objs/CommentaryResult$Item;->t:I

    add-int v10, v2, v1

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v1, "contributor"

    iget-object v2, v8, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    iget-object v1, v8, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lflipboard/objs/FeedSectionLink;

    new-instance v1, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;

    iget-object v3, v8, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    iget-object v4, v8, Lflipboard/objs/CommentaryResult$Item$Commentary;->f:Ljava/lang/String;

    iget-object v5, v8, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;Ljava/lang/String;Ljava/lang/String;Lflipboard/objs/Image;ZLflipboard/objs/FeedSectionLink;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    if-eqz v2, :cond_18

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v2, Lflipboard/service/User;->d:Ljava/lang/String;

    iget-object v3, v8, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    :goto_3
    move-object v9, v1

    goto :goto_2

    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->l:Lflipboard/util/Log;

    const-string v2, "User avatar on magazine cover was not clickable because section.getMeta().profileSectionLink was null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v2, v10

    goto :goto_1

    :cond_2
    move v4, v2

    :goto_4
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    if-eqz p1, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    if-eqz v2, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    move-object/from16 v0, p1

    iget-object v1, v0, Lflipboard/objs/CommentaryResult;->a:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/CommentaryResult$Item;

    iget v1, v1, Lflipboard/objs/CommentaryResult$Item;->b:I

    :cond_3
    if-nez v1, :cond_a

    const-string v1, "Comments"

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->w:Lflipboard/gui/FLButton;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->w:Lflipboard/gui/FLButton;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->w:Lflipboard/gui/FLButton;

    new-instance v2, Lflipboard/gui/item/CoverSectionItemTablet$6;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lflipboard/gui/item/CoverSectionItemTablet$6;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    const v1, 0x7f0a0223

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v10, Landroid/widget/FrameLayout$LayoutParams;

    const v3, 0x7f09001f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v5, 0x7f09001f

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v10, v3, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const v6, 0x7f090115

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v6, 0x0

    invoke-virtual {v10, v3, v5, v2, v6}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f030028

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v10}, Lflipboard/gui/FLImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v3, v11, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v3, :cond_c

    iget-object v3, v11, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    :goto_6
    const/4 v3, 0x1

    new-instance v5, Lflipboard/gui/item/CoverSectionItemTablet$7;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lflipboard/gui/item/CoverSectionItemTablet$7;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;)V

    invoke-virtual {v2, v5}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v5, 0x0

    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v6, 0x0

    iget-object v2, v11, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, v11, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/4 v6, 0x1

    iget-object v5, v11, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    :cond_5
    if-eqz v9, :cond_17

    iget-object v2, v9, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->c:Lflipboard/objs/Image;

    if-eqz v2, :cond_6

    iget-object v2, v9, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->c:Lflipboard/objs/Image;

    invoke-virtual {v2}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f030028

    const/4 v8, 0x0

    invoke-static {v2, v3, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v10}, Lflipboard/gui/FLImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    const/4 v3, 0x2

    invoke-virtual {v2, v7}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    new-instance v7, Lflipboard/gui/item/CoverSectionItemTablet$8;

    move-object/from16 v0, p0

    invoke-direct {v7, v0, v9}, Lflipboard/gui/item/CoverSectionItemTablet$8;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;)V

    invoke-virtual {v2, v7}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    move v2, v3

    iget-object v3, v9, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    if-eqz v3, :cond_16

    if-nez v6, :cond_d

    iget-object v3, v9, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_7
    add-int/lit8 v5, v6, 0x1

    iget-object v3, v9, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    move-object v15, v3

    move v3, v2

    move-object v2, v15

    :goto_8
    const/4 v6, 0x1

    move v7, v6

    move v8, v5

    move-object v6, v2

    move v5, v3

    :goto_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_f

    const/4 v2, 0x6

    if-lt v5, v2, :cond_7

    const/4 v2, 0x4

    if-ge v8, v2, :cond_f

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;

    if-eq v3, v9, :cond_8

    iget-object v2, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    if-eqz v2, :cond_8

    const/4 v2, 0x4

    if-ge v8, v2, :cond_8

    if-nez v8, :cond_e

    iget-object v2, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :goto_a
    iget-object v6, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    add-int/lit8 v8, v8, 0x1

    :cond_8
    if-eq v3, v9, :cond_9

    iget-object v2, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->c:Lflipboard/objs/Image;

    if-eqz v2, :cond_9

    iget-object v2, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->c:Lflipboard/objs/Image;

    invoke-virtual {v2}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->c:Lflipboard/objs/Image;

    invoke-virtual {v2}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    const v13, 0x7f030028

    const/4 v14, 0x0

    invoke-static {v2, v13, v14}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v10}, Lflipboard/gui/FLImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v2, v11}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    new-instance v11, Lflipboard/gui/item/CoverSectionItemTablet$9;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v3}, Lflipboard/gui/item/CoverSectionItemTablet$9;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;)V

    invoke-virtual {v2, v11}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    move v2, v5

    add-int/lit8 v3, v7, 0x1

    move v5, v2

    move v7, v3

    goto :goto_9

    :cond_a
    const/4 v2, 0x1

    if-ne v1, v2, :cond_b

    const-string v1, "1 Comment"

    goto/16 :goto_5

    :cond_b
    const-string v2, "%d comments"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_5

    :cond_c
    const v3, 0x7f02005a

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_6

    :cond_d
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, ", "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v9, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_7

    :cond_e
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, ", "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lflipboard/gui/item/CoverSectionItemTablet$MagazineContributor;->a:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_a

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->p:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_10

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    iget-boolean v2, v2, Lflipboard/objs/FeedItem;->ck:Z

    if-eqz v2, :cond_12

    :cond_10
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x7f0a0220

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0324

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lflipboard/gui/item/CoverSectionItemTablet;->g:Lflipboard/gui/FLTextIntf;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v2, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    :cond_11
    :goto_b
    return-void

    :cond_12
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x7f0a0220

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    const v1, 0x7f0a0227

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->f()Z

    move-result v2

    if-nez v2, :cond_13

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    :cond_13
    const/4 v1, 0x4

    if-le v4, v1, :cond_14

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0291

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x1

    add-int/lit8 v4, v4, -0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/item/CoverSectionItemTablet;->g:Lflipboard/gui/FLTextIntf;

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    :cond_14
    if-eqz v6, :cond_11

    invoke-virtual {v12, v6}, Ljava/lang/StringBuffer;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    if-le v1, v2, :cond_15

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->length()I

    move-result v2

    invoke-virtual {v12, v1, v2}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->g:Lflipboard/gui/FLTextIntf;

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0292

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_b

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lflipboard/gui/item/CoverSectionItemTablet;->g:Lflipboard/gui/FLTextIntf;

    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_b

    :cond_16
    move v3, v2

    move-object v2, v5

    move v5, v6

    goto/16 :goto_8

    :cond_17
    move-object v2, v5

    move v5, v6

    goto/16 :goto_8

    :cond_18
    move-object v1, v9

    goto/16 :goto_3

    :cond_19
    move v4, v1

    goto/16 :goto_4
.end method

.method static synthetic a(Lflipboard/gui/item/CoverSectionItemTablet;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lflipboard/gui/item/CoverSectionItemTablet;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->DisableMagazineLikes:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 361
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 362
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v3

    iget-boolean v3, v3, Lflipboard/objs/FeedItem;->ad:Z

    and-int/2addr v0, v3

    .line 365
    :cond_0
    if-eqz v0, :cond_5

    .line 369
    if-eqz p1, :cond_2

    .line 370
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020183

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 374
    :goto_1
    iget-object v3, p0, Lflipboard/gui/item/CoverSectionItemTablet;->u:Landroid/widget/Button;

    invoke-virtual {v3, v0, v4, v4, v4}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 375
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->u:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 377
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 382
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_6

    .line 383
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget v0, v0, Lflipboard/objs/CommentaryResult$Item;->c:I

    move v3, v0

    .line 386
    :goto_2
    if-lez v3, :cond_4

    .line 388
    if-ne v3, v1, :cond_3

    .line 389
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d01e2

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 393
    :goto_3
    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 394
    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 396
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 404
    :goto_4
    return-void

    :cond_1
    move v0, v2

    .line 359
    goto/16 :goto_0

    .line 372
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f020184

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 391
    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d01e1

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 398
    :cond_4
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_4

    .line 401
    :cond_5
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->u:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 402
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_4

    :cond_6
    move v3, v2

    goto :goto_2
.end method

.method private a(Lflipboard/objs/FeedItem;Landroid/view/ViewGroup;ZZ)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 324
    iget-object v0, p1, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    const-string v1, "sectionCover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 325
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030105

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 326
    const v0, 0x7f0a004f

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 327
    const v1, 0x7f0a016d

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    .line 328
    iget-object v4, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 329
    if-eqz p3, :cond_0

    .line 330
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900da

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-interface {v0, v2, v4}, Lflipboard/gui/FLTextIntf;->a(II)V

    .line 332
    :cond_0
    invoke-static {p1}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 333
    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 334
    if-eqz p4, :cond_1

    .line 335
    const v0, 0x7f0a012a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 337
    :cond_1
    const/4 v0, 0x1

    .line 339
    :goto_0
    return v0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 350
    const/4 v0, 0x0

    .line 351
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 352
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->af:Z

    .line 354
    :cond_0
    invoke-direct {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->a(Z)V

    .line 355
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 409
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 410
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 411
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    .line 412
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 413
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-eq v0, v4, :cond_0

    .line 415
    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 418
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_2

    .line 419
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->l:Lflipboard/gui/FLTextIntf;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 422
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 468
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 469
    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/item/CoverSectionItemTablet$5;

    invoke-direct {v2, p0}, Lflipboard/gui/item/CoverSectionItemTablet$5;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;)V

    invoke-virtual {v1, v0, v2}, Lflipboard/service/FlipboardManager;->c(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 489
    return-void
.end method

.method final a(Lflipboard/objs/FeedSectionLink;Lflipboard/service/Section;)V
    .locals 3

    .prologue
    .line 717
    if-eqz p1, :cond_0

    .line 718
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 719
    const-string v1, "source"

    const-string v2, "mag_cover"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 720
    const-string v1, "originSectionIdentifier"

    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, p1}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 723
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 3

    .prologue
    .line 449
    invoke-direct {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->c()V

    .line 453
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 454
    if-eqz p1, :cond_0

    iget-object v0, p1, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_0

    .line 455
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    .line 456
    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    iget-object v2, p1, Lflipboard/objs/HasCommentaryItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    invoke-virtual {v2, v0}, Lflipboard/objs/CommentaryResult$Item;->a(Lflipboard/service/Account;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lflipboard/objs/FeedItem;->a(Z)V

    .line 463
    :cond_0
    invoke-direct {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->b()V

    .line 464
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 223
    iput-object p1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    .line 224
    iput-object p2, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    .line 225
    iget-object v0, p2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedSection;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 228
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    .line 229
    invoke-direct {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->c()V

    .line 230
    invoke-direct {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->b()V

    .line 232
    iget-object v0, p2, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    if-eqz v0, :cond_a

    .line 233
    iget-object v0, p2, Lflipboard/objs/FeedItem;->N:Lflipboard/objs/Image;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    .line 240
    :cond_1
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    if-nez v0, :cond_2

    iget-object v0, p2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 241
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 242
    iget-object v0, v0, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    .line 244
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_3

    .line 245
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 248
    :cond_3
    invoke-virtual {p1}, Lflipboard/service/Section;->f()Z

    move-result v0

    if-nez v0, :cond_4

    .line 249
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->t:Landroid/view/View;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 252
    :cond_4
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 253
    iget-object v4, p0, Lflipboard/gui/item/CoverSectionItemTablet;->d:Lflipboard/gui/FLImageView;

    new-instance v5, Lflipboard/gui/item/CoverSectionItemTablet$4;

    invoke-direct {v5, p0, p1}, Lflipboard/gui/item/CoverSectionItemTablet$4;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;Lflipboard/service/Section;)V

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 263
    iget-object v4, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v4, :cond_c

    .line 264
    iget-object v4, p0, Lflipboard/gui/item/CoverSectionItemTablet;->d:Lflipboard/gui/FLImageView;

    sget-object v5, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 265
    iget-object v4, p0, Lflipboard/gui/item/CoverSectionItemTablet;->d:Lflipboard/gui/FLImageView;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v4, v0}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 266
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->d:Lflipboard/gui/FLImageView;

    invoke-static {v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 271
    :goto_1
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 272
    if-eqz v0, :cond_5

    .line 273
    iget-object v4, p0, Lflipboard/gui/item/CoverSectionItemTablet;->e:Lflipboard/gui/FLTextIntf;

    invoke-interface {v4, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 277
    :cond_5
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->f:Lflipboard/gui/FLTextIntf;

    iget-object v4, p2, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    invoke-interface {v0, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 279
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    .line 280
    if-eqz v0, :cond_6

    .line 281
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0324

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 282
    iget-object v5, p0, Lflipboard/gui/item/CoverSectionItemTablet;->g:Lflipboard/gui/FLTextIntf;

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v4, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 285
    :cond_6
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    iget-object v0, v0, Lflipboard/objs/Image;->j:Ljava/lang/String;

    .line 286
    :goto_2
    if-nez v0, :cond_7

    iget-object v3, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    if-eqz v3, :cond_7

    .line 287
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    invoke-static {v0}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 289
    :cond_7
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_8

    .line 290
    iget-object v3, p0, Lflipboard/gui/item/CoverSectionItemTablet;->o:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0294

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    :cond_8
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b0008

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    .line 295
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_13

    .line 296
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    iget-object v3, p0, Lflipboard/gui/item/CoverSectionItemTablet;->q:Landroid/view/ViewGroup;

    sget-object v4, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v4, v4, Lflipboard/app/FlipboardApplication;->f:Z

    invoke-direct {p0, v0, v3, v4, v2}, Lflipboard/gui/item/CoverSectionItemTablet;->a(Lflipboard/objs/FeedItem;Landroid/view/ViewGroup;ZZ)Z

    move v0, v1

    .line 300
    :goto_3
    invoke-virtual {p1}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v7

    move v5, v2

    move v4, v0

    .line 301
    :goto_4
    if-ge v4, v6, :cond_f

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_f

    .line 302
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 304
    iget-object v3, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    if-eqz v3, :cond_9

    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v3, :cond_12

    iget-object v3, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    iget-object v8, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    iget-object v8, v8, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    .line 305
    :cond_9
    iget-object v8, p0, Lflipboard/gui/item/CoverSectionItemTablet;->q:Landroid/view/ViewGroup;

    add-int/lit8 v3, v6, -0x1

    if-ne v4, v3, :cond_e

    move v3, v1

    :goto_5
    invoke-direct {p0, v0, v8, v2, v3}, Lflipboard/gui/item/CoverSectionItemTablet;->a(Lflipboard/objs/FeedItem;Landroid/view/ViewGroup;ZZ)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 306
    add-int/lit8 v0, v4, 0x1

    .line 309
    :goto_6
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v4, v0

    .line 310
    goto :goto_4

    .line 234
    :cond_a
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_b

    .line 235
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    .line 236
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->o:Lflipboard/gui/FLLabelTextView;

    iget-object v4, p2, Lflipboard/objs/FeedItem;->bF:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLLabelTextView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 237
    :cond_b
    iget-object v0, p1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 238
    iget-object v0, p1, Lflipboard/service/Section;->s:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    goto/16 :goto_0

    .line 268
    :cond_c
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->d:Lflipboard/gui/FLImageView;

    const/16 v4, 0x8

    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_1

    :cond_d
    move-object v0, v3

    .line 285
    goto/16 :goto_2

    :cond_e
    move v3, v2

    .line 305
    goto :goto_5

    .line 311
    :cond_f
    if-nez v4, :cond_10

    .line 312
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->r:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 314
    :cond_10
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v4, :cond_11

    .line 315
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->s:Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setVisibility(I)V

    .line 319
    :cond_11
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->a()V

    .line 320
    return-void

    :cond_12
    move v0, v4

    goto :goto_6

    :cond_13
    move v0, v2

    goto/16 :goto_3
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 426
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "CoverSectionItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 427
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    iget-boolean v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->i:Z

    if-eqz v0, :cond_0

    .line 429
    iput-boolean v3, p0, Lflipboard/gui/item/CoverSectionItemTablet;->i:Z

    .line 430
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->m:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 435
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->i:Z

    if-nez v0, :cond_0

    .line 436
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->i:Z

    .line 437
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->removeView(Landroid/view/View;)V

    .line 438
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    .line 439
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 440
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 441
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet;->h:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v3, v1}, Lflipboard/gui/item/CoverSectionItemTablet;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 6

    .prologue
    const/16 v5, 0x11

    const/4 v4, 0x5

    const/4 v3, 0x0

    const/16 v2, 0xb

    .line 91
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 93
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    .line 94
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->h:Landroid/view/ViewGroup$LayoutParams;

    .line 95
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->j:Landroid/graphics/drawable/Drawable;

    .line 96
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->k:Landroid/graphics/drawable/Drawable;

    .line 97
    const v0, 0x7f0a0221

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->d:Lflipboard/gui/FLImageView;

    .line 98
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->e:Lflipboard/gui/FLTextIntf;

    .line 99
    const v0, 0x7f0a0201

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->f:Lflipboard/gui/FLTextIntf;

    .line 100
    const v0, 0x7f0a0224

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->g:Lflipboard/gui/FLTextIntf;

    .line 101
    const v0, 0x7f0a0225

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->l:Lflipboard/gui/FLTextIntf;

    .line 102
    const v0, 0x7f0a022e

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->o:Lflipboard/gui/FLLabelTextView;

    .line 103
    const v0, 0x7f0a022b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->q:Landroid/view/ViewGroup;

    .line 104
    const v0, 0x7f0a022c

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->r:Landroid/view/View;

    .line 105
    const v0, 0x7f0a022a

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->s:Landroid/view/View;

    .line 106
    const v0, 0x7f0a0222

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->t:Landroid/view/View;

    .line 107
    const v0, 0x7f0a0228

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->u:Landroid/widget/Button;

    .line 108
    const v0, 0x7f0a0229

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    .line 109
    const v0, 0x7f0a0226

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->w:Lflipboard/gui/FLButton;

    .line 111
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-nez v0, :cond_0

    .line 112
    const v0, 0x7f0a022d

    invoke-virtual {p0, v0}, Lflipboard/gui/item/CoverSectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->o:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_1

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    :goto_0
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v5, :cond_2

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    :goto_1
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 115
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->u:Landroid/widget/Button;

    new-instance v1, Lflipboard/gui/item/CoverSectionItemTablet$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/CoverSectionItemTablet$1;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->v:Landroid/widget/Button;

    new-instance v1, Lflipboard/gui/item/CoverSectionItemTablet$2;

    invoke-direct {v1, p0}, Lflipboard/gui/item/CoverSectionItemTablet$2;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    invoke-direct {p0}, Lflipboard/gui/item/CoverSectionItemTablet;->b()V

    .line 169
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet;->o:Lflipboard/gui/FLLabelTextView;

    new-instance v1, Lflipboard/gui/item/CoverSectionItemTablet$3;

    invoke-direct {v1, p0}, Lflipboard/gui/item/CoverSectionItemTablet$3;-><init>(Lflipboard/gui/item/CoverSectionItemTablet;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    return-void

    .line 112
    :cond_1
    invoke-virtual {v0, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1
.end method
