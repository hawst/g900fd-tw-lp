.class Lflipboard/gui/item/WebDetailView$1;
.super Lflipboard/util/FLWebViewClient;
.source "WebDetailView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/item/WebDetailView;


# direct methods
.method constructor <init>(Lflipboard/gui/item/WebDetailView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lflipboard/gui/item/WebDetailView$1;->a:Lflipboard/gui/item/WebDetailView;

    invoke-direct {p0, p2}, Lflipboard/util/FLWebViewClient;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lflipboard/gui/item/WebDetailView$1;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 101
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    sget-object v0, Lflipboard/gui/item/WebDetailView$1;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 123
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView$1;->a:Lflipboard/gui/item/WebDetailView;

    invoke-static {v0}, Lflipboard/gui/item/WebDetailView;->b(Lflipboard/gui/item/WebDetailView;)V

    .line 125
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 115
    sget-object v0, Lflipboard/gui/item/WebDetailView$1;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 116
    invoke-super {p0, p1, p2, p3}, Lflipboard/util/FLWebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 117
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView$1;->a:Lflipboard/gui/item/WebDetailView;

    invoke-static {v0}, Lflipboard/gui/item/WebDetailView;->a(Lflipboard/gui/item/WebDetailView;)V

    .line 118
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 129
    sget-object v0, Lflipboard/gui/item/WebDetailView$1;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    .line 130
    invoke-super {p0, p1, p2, p3, p4}, Lflipboard/util/FLWebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView$1;->a:Lflipboard/gui/item/WebDetailView;

    invoke-static {v0}, Lflipboard/gui/item/WebDetailView;->b(Lflipboard/gui/item/WebDetailView;)V

    .line 132
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    const/4 v0, 0x1

    .line 110
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
