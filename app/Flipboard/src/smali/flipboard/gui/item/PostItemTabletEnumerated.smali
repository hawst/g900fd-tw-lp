.class public Lflipboard/gui/item/PostItemTabletEnumerated;
.super Landroid/view/ViewGroup;
.source "PostItemTabletEnumerated.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/gui/FLStaticTextView;

.field c:Lflipboard/gui/FLStaticTextView;

.field d:Lflipboard/gui/FLStaticTextView;

.field private e:I

.field private f:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    .line 32
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    iget v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    iget v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    iget v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    iget v3, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lflipboard/gui/item/PostItemTabletEnumerated;->setPadding(IIII)V

    .line 35
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 48
    iput-object p2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->a:Lflipboard/objs/FeedItem;

    .line 49
    invoke-virtual {p0, p2}, Lflipboard/gui/item/PostItemTabletEnumerated;->setTag(Ljava/lang/Object;)V

    .line 51
    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    iget-object v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->c:Lflipboard/gui/FLStaticTextView;

    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->e(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    iget v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->e:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 40
    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 41
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PostItemTabletEnumerated;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    .line 42
    const v0, 0x7f0a020f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PostItemTabletEnumerated;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->c:Lflipboard/gui/FLStaticTextView;

    .line 43
    const v0, 0x7f0a0215

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PostItemTabletEnumerated;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    .line 44
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 95
    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getPaddingLeft()I

    move-result v2

    .line 96
    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getPaddingTop()I

    move-result v0

    .line 97
    iget-object v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v1

    add-int v3, v2, v1

    .line 98
    iget-object v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 99
    iget-object v4, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4, v2, v0, v3, v1}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 100
    iget-object v4, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->f:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

    sget-object v5, Lflipboard/gui/item/PostItemTabletEnumerated$Layout;->a:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

    if-ne v4, v5, :cond_0

    .line 101
    iget v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    add-int/2addr v1, v3

    .line 105
    :goto_0
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    .line 106
    iget-object v3, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    iget-object v4, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v3, v1, v0, v4, v2}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 107
    iget v0, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    add-int/2addr v0, v2

    .line 108
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->c:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 109
    return-void

    :cond_0
    move v0, v1

    move v1, v2

    .line 103
    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/high16 v5, -0x80000000

    .line 71
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 72
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 73
    invoke-virtual {p0, v0, v2}, Lflipboard/gui/item/PostItemTabletEnumerated;->setMeasuredDimension(II)V

    .line 75
    iget-object v1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 76
    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getPaddingRight()I

    move-result v1

    sub-int v1, v0, v1

    .line 77
    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getPaddingTop()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lflipboard/gui/item/PostItemTabletEnumerated;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    .line 79
    sget-object v2, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v2, v2, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v2, :cond_0

    .line 81
    sget-object v2, Lflipboard/gui/item/PostItemTabletEnumerated$Layout;->a:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

    iput-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->f:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

    .line 82
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v2

    iget v3, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 88
    :goto_0
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 89
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    iget v3, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->g:I

    add-int/2addr v2, v3

    sub-int/2addr v0, v2

    .line 90
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->c:Lflipboard/gui/FLStaticTextView;

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 91
    return-void

    .line 84
    :cond_0
    sget-object v2, Lflipboard/gui/item/PostItemTabletEnumerated$Layout;->b:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

    iput-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->f:Lflipboard/gui/item/PostItemTabletEnumerated$Layout;

    .line 85
    iget-object v2, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    goto :goto_0
.end method

.method public setNumber(I)V
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lflipboard/gui/item/PostItemTabletEnumerated;->e:I

    .line 60
    return-void
.end method
