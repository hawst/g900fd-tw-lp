.class public Lflipboard/gui/item/ListItemTablet$ListItem;
.super Landroid/view/ViewGroup;
.source "ListItemTablet.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/ViewGroup;",
        "Lflipboard/gui/item/TabletItem;",
        "Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;",
        "Ljava/lang/Comparable",
        "<",
        "Lflipboard/gui/item/ListItemTablet$ListItem;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lflipboard/objs/FeedItem;

.field private b:Lflipboard/gui/FLImageView;

.field private c:Lflipboard/gui/FLLabelTextView;

.field private d:Lflipboard/gui/FLLabelTextView;

.field private e:Lflipboard/gui/FLStaticTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 161
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/ListItemTablet$ListItem;)Lflipboard/gui/FLStaticTextView;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    .line 247
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->a:Lflipboard/objs/FeedItem;

    iget-wide v2, v1, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/util/JavaUtil;->d(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 248
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-static {v1, v2}, Lflipboard/gui/SocialFormatter;->a(Landroid/content/Context;Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    .line 249
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 250
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " \u00b7 "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    :cond_0
    iget-object v1, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 257
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/ListItemTablet$ListItem$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/ListItemTablet$ListItem$1;-><init>(Lflipboard/gui/item/ListItemTablet$ListItem;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 263
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 227
    iput-object p2, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->a:Lflipboard/objs/FeedItem;

    .line 228
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 229
    invoke-virtual {p0, p2}, Lflipboard/gui/item/ListItemTablet$ListItem;->setTag(Ljava/lang/Object;)V

    .line 230
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v0

    .line 231
    if-nez v0, :cond_0

    .line 232
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v0

    .line 234
    :cond_0
    iget-object v1, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 236
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->a()V

    .line 238
    return-void
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 150
    check-cast p1, Lflipboard/gui/item/ListItemTablet$ListItem;

    invoke-virtual {p1}, Lflipboard/gui/item/ListItemTablet$ListItem;->getLineCount()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getLineCount()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method getLineCount()I
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMaxLines()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getLineCount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 267
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 268
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->a:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 271
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 165
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 166
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 167
    const v0, 0x7f0a01ff

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    .line 168
    const v0, 0x7f0a0200

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    .line 169
    const v0, 0x7f0a0201

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    .line 170
    const v0, 0x7f0a0202

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    .line 171
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 209
    sub-int v0, p4, p2

    .line 210
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 211
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingTop()I

    move-result v2

    .line 212
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingLeft()I

    move-result v3

    .line 214
    iget-object v4, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v3

    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v4, v3, v2, v5, v6}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 215
    iget-object v4, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v1, v3

    .line 217
    iget-object v3, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v4, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v1, v2, v4, v5}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 218
    iget-object v3, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingRight()I

    move-result v4

    sub-int v4, v0, v4

    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingRight()I

    move-result v5

    sub-int/2addr v0, v5

    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v0, v5}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 220
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v2

    .line 222
    iget-object v2, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v3, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 223
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v9, 0x0

    const/high16 v8, -0x80000000

    .line 175
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 177
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 178
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 179
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09007d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 180
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingLeft()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 181
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingTop()I

    move-result v4

    sub-int v4, v1, v4

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    .line 182
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iget v6, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    iget-object v7, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    iget v7, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 184
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v3, v5

    sub-int/2addr v3, v2

    invoke-static {v9, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 186
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 187
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 188
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    sub-int v6, v3, v2

    if-le v5, v6, :cond_0

    .line 189
    sub-int v5, v3, v2

    div-int/lit8 v5, v5, 0x2

    .line 190
    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    if-ge v6, v5, :cond_1

    .line 191
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v3, v6

    sub-int v2, v6, v2

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v2, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 199
    :cond_0
    :goto_0
    iget-object v2, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v4, v2

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 200
    iget-object v4, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 202
    iget-object v3, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 204
    sub-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/item/ListItemTablet$ListItem;->setMeasuredDimension(II)V

    .line 205
    return-void

    .line 192
    :cond_1
    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    if-ge v6, v5, :cond_2

    .line 193
    iget-object v5, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    iget-object v6, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v6

    sub-int v6, v3, v6

    sub-int v2, v6, v2

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v5, v2, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    goto :goto_0

    .line 195
    :cond_2
    iget-object v2, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->c:Lflipboard/gui/FLLabelTextView;

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v2, v6, v7}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 196
    iget-object v2, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->d:Lflipboard/gui/FLLabelTextView;

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v2, v5, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    goto :goto_0
.end method

.method setLineCount(I)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lflipboard/gui/item/ListItemTablet$ListItem;->e:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLStaticTextView;->setMaxLines(I)V

    .line 276
    return-void
.end method
