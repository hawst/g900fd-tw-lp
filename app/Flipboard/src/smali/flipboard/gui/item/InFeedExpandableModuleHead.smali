.class public Lflipboard/gui/item/InFeedExpandableModuleHead;
.super Landroid/widget/LinearLayout;
.source "InFeedExpandableModuleHead.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field private a:Lflipboard/gui/FLImageView;

.field private b:Lflipboard/gui/FLTextView;

.field private c:Lflipboard/gui/FLTextView;

.field private d:Lflipboard/gui/FLTextView;

.field private e:Landroid/graphics/Paint;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 89
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 90
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 91
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 92
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 93
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 94
    new-instance v2, Lflipboard/objs/FeedSectionLink;

    invoke-direct {v2, v1}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/objs/FeedItem;)V

    .line 95
    const/4 v1, 0x0

    .line 97
    invoke-virtual {v2}, Lflipboard/objs/FeedSectionLink;->c()Z

    move-result v3

    .line 100
    invoke-virtual {v2}, Lflipboard/objs/FeedSectionLink;->b()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v2}, Lflipboard/objs/FeedSectionLink;->c()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 101
    :cond_0
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 102
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/objs/FeedSectionLink;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 103
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 104
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 105
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->d:Lflipboard/gui/FLTextView;

    iget-object v4, v2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->a:Lflipboard/gui/FLImageView;

    .line 116
    :cond_1
    :goto_0
    if-eqz v1, :cond_2

    .line 117
    new-instance v4, Lflipboard/gui/item/InFeedExpandableModuleHead$1;

    invoke-direct {v4, p0, v2}, Lflipboard/gui/item/InFeedExpandableModuleHead$1;-><init>(Lflipboard/gui/item/InFeedExpandableModuleHead;Lflipboard/objs/FeedSectionLink;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    :cond_2
    const v1, 0x7f0a01e8

    invoke-virtual {p0, v1}, Lflipboard/gui/item/InFeedExpandableModuleHead;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FollowButton;

    .line 132
    invoke-virtual {v1, v2}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    .line 133
    if-eqz v3, :cond_3

    .line 134
    sget-object v2, Lflipboard/objs/UsageEventV2$FollowFrom;->a:Lflipboard/objs/UsageEventV2$FollowFrom;

    invoke-virtual {v1, v2}, Lflipboard/gui/FollowButton;->setFrom(Lflipboard/objs/UsageEventV2$FollowFrom;)V

    .line 140
    :cond_3
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->c:Lflipboard/gui/FLTextView;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    return-void

    .line 107
    :cond_4
    invoke-virtual {v2}, Lflipboard/objs/FeedSectionLink;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v6}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 109
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 110
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->b:Lflipboard/gui/FLTextView;

    iget-object v4, v2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v6}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 112
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->b:Lflipboard/gui/FLTextView;

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 78
    invoke-virtual {p0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->getWidth()I

    move-result v6

    .line 79
    iget-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getBottom()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v7, v0, 0x2

    .line 80
    iget-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getLeft()I

    move-result v0

    iget v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->f:I

    sub-int/2addr v0, v1

    .line 81
    iget-object v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getRight()I

    move-result v1

    iget v2, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->f:I

    add-int v8, v1, v2

    .line 83
    iget v1, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->f:I

    int-to-float v1, v1

    int-to-float v2, v7

    int-to-float v3, v0

    int-to-float v4, v7

    iget-object v5, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 84
    int-to-float v1, v8

    int-to-float v2, v7

    iget v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->f:I

    sub-int v0, v6, v0

    int-to-float v3, v0

    int-to-float v4, v7

    iget-object v5, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 85
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 58
    const v0, 0x7f0a016c

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->c:Lflipboard/gui/FLTextView;

    .line 59
    const v0, 0x7f0a01e5

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->a:Lflipboard/gui/FLImageView;

    .line 60
    const v0, 0x7f0a01e6

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->b:Lflipboard/gui/FLTextView;

    .line 61
    const v0, 0x7f0a01e7

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->d:Lflipboard/gui/FLTextView;

    .line 62
    invoke-virtual {p0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->f:I

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080037

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lflipboard/gui/item/InFeedExpandableModuleHead;->e:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InFeedExpandableModuleHead;->setWillNotDraw(Z)V

    .line 64
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 146
    return-void
.end method
