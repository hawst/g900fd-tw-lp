.class public Lflipboard/gui/item/PageboxPaginatedCarousel$$ViewInjector;
.super Ljava/lang/Object;
.source "PageboxPaginatedCarousel$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/item/PageboxPaginatedCarousel;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a0209

    const-string v1, "field \'pageboxCarouselTitleView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p1, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    .line 12
    const v0, 0x7f0a0207

    const-string v1, "field \'carouselBackgroundImageView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p1, Lflipboard/gui/item/PageboxPaginatedCarousel;->c:Lflipboard/gui/FLImageView;

    .line 14
    const v0, 0x7f0a0208

    const-string v1, "field \'carouselView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/CarouselView;

    iput-object v0, p1, Lflipboard/gui/item/PageboxPaginatedCarousel;->d:Lflipboard/gui/CarouselView;

    .line 16
    return-void
.end method

.method public static reset(Lflipboard/gui/item/PageboxPaginatedCarousel;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->b:Lflipboard/gui/FLTextView;

    .line 20
    iput-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->c:Lflipboard/gui/FLImageView;

    .line 21
    iput-object v0, p0, Lflipboard/gui/item/PageboxPaginatedCarousel;->d:Lflipboard/gui/CarouselView;

    .line 22
    return-void
.end method
