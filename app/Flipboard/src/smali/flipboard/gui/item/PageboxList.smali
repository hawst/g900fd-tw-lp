.class public Lflipboard/gui/item/PageboxList;
.super Landroid/view/ViewGroup;
.source "PageboxList.java"

# interfaces
.implements Lflipboard/gui/item/PageboxView;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field private b:Lflipboard/gui/FLLabelTextView;

.field private c:Lflipboard/gui/FLImageView;

.field private d:Landroid/view/View;

.field private e:Lflipboard/objs/SidebarGroup$RenderHints;

.field private f:Landroid/view/View;

.field private g:Lflipboard/service/Section;

.field private h:Landroid/view/View;

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final j:I

.field private k:Landroid/view/View;

.field private l:Lflipboard/gui/FLStaticTextView;

.field private m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->i:Ljava/util/List;

    .line 39
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxList;->j:I

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 57
    iput-object p2, p0, Lflipboard/gui/item/PageboxList;->a:Lflipboard/objs/FeedItem;

    .line 58
    iput-object p1, p0, Lflipboard/gui/item/PageboxList;->g:Lflipboard/service/Section;

    .line 59
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 46
    const v0, 0x7f0a017f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    .line 47
    const v0, 0x7f0a00fd

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    .line 48
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->b:Lflipboard/gui/FLLabelTextView;

    .line 49
    const v0, 0x7f0a0066

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->c:Lflipboard/gui/FLImageView;

    .line 50
    const v0, 0x7f0a012a

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->d:Landroid/view/View;

    .line 51
    const v0, 0x7f0a0206

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    .line 52
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    const v1, 0x7f0a0041

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->l:Lflipboard/gui/FLStaticTextView;

    .line 53
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 161
    sub-int v2, p5, p3

    .line 162
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getPaddingLeft()I

    move-result v3

    .line 163
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getPaddingTop()I

    move-result v0

    .line 164
    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget-object v5, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 165
    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    iget-object v5, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v1, v3, v0, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 166
    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 168
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    if-lez v5, :cond_0

    .line 169
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v0, v3, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 170
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 174
    goto :goto_0

    .line 175
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/item/PageboxList;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-boolean v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->k:Z

    if-eqz v0, :cond_2

    .line 176
    :cond_1
    iget v0, p0, Lflipboard/gui/item/PageboxList;->j:I

    sub-int v0, v2, v0

    .line 177
    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    iget-object v2, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v0, v2

    iget-object v4, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v1, v3, v2, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 179
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 124
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 125
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 126
    invoke-virtual {p0, v0, v2}, Lflipboard/gui/item/PageboxList;->setMeasuredDimension(II)V

    .line 128
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getPaddingBottom()I

    move-result v0

    sub-int v0, v2, v0

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getPaddingTop()I

    move-result v2

    sub-int v5, v0, v2

    .line 129
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 131
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->measure(II)V

    .line 132
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 133
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    invoke-virtual {v0, v7, v6}, Landroid/view/View;->measure(II)V

    .line 134
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    invoke-virtual {v0, v7, v6}, Landroid/view/View;->measure(II)V

    .line 137
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    move v4, v1

    move v0, v1

    .line 139
    :goto_0
    iget-object v3, p0, Lflipboard/gui/item/PageboxList;->i:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    if-nez v0, :cond_2

    .line 140
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 141
    invoke-virtual {v0, v7, v6}, Landroid/view/View;->measure(II)V

    .line 142
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    .line 143
    if-le v3, v5, :cond_1

    const/4 v2, 0x1

    .line 144
    :goto_1
    if-eqz v2, :cond_0

    .line 145
    invoke-static {v1, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 146
    invoke-virtual {v0, v8, v8}, Landroid/view/View;->measure(II)V

    .line 147
    iput-boolean v1, p0, Lflipboard/gui/item/PageboxList;->m:Z

    .line 149
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v0, v2

    move v2, v3

    .line 150
    goto :goto_0

    :cond_1
    move v2, v1

    .line 143
    goto :goto_1

    .line 151
    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/item/PageboxList;->m:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-boolean v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->k:Z

    if-eqz v0, :cond_4

    .line 152
    :cond_3
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 156
    :goto_2
    return-void

    .line 154
    :cond_4
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public setPagebox(Lflipboard/objs/SidebarGroup;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 63
    invoke-virtual {p1}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    .line 64
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->f:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->d:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 68
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->h:Lflipboard/objs/Image;

    if-eqz v0, :cond_a

    .line 69
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->h:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    if-eqz v0, :cond_7

    .line 72
    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v7}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->b:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 81
    :cond_1
    :goto_1
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 82
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->d:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 84
    :cond_2
    iget-object v0, p1, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/objs/FeedItem;

    .line 85
    const-string v0, "pageboxList"

    iput-object v0, v1, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    .line 86
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxList;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0300a9

    invoke-static {v0, v4, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 87
    const v0, 0x7f0a006b

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 88
    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v5

    .line 89
    if-eqz v5, :cond_3

    iget-object v6, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-boolean v6, v6, Lflipboard/objs/SidebarGroup$RenderHints;->l:Z

    if-eqz v6, :cond_3

    .line 90
    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v0, v7}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 93
    :cond_3
    const v0, 0x7f0a004f

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 94
    iget-object v5, v1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v5, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v5, v5, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    if-eqz v5, :cond_4

    .line 96
    iget-object v5, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v5, v5, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    invoke-static {v5}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 99
    :cond_4
    iget-object v5, v1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    if-eqz v5, :cond_5

    iget-object v5, v1, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    iget-object v6, p0, Lflipboard/gui/item/PageboxList;->g:Lflipboard/service/Section;

    iget-object v6, v6, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v6, v6, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 100
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, v5, Lflipboard/service/FlipboardManager;->z:Landroid/graphics/Typeface;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLStaticTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 101
    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    :cond_5
    const v0, 0x7f0a012a

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 104
    iget-object v5, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v5, v5, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 105
    iget-object v5, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v5, v5, Lflipboard/objs/SidebarGroup$RenderHints;->e:Ljava/lang/String;

    invoke-static {v5}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    .line 107
    :cond_6
    invoke-virtual {v4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 108
    invoke-virtual {p0, v4}, Lflipboard/gui/item/PageboxList;->addView(Landroid/view/View;)V

    .line 109
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->i:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 76
    :cond_7
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 78
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->g:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    goto/16 :goto_1

    .line 111
    :cond_8
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 112
    iget-object v0, p0, Lflipboard/gui/item/PageboxList;->l:Lflipboard/gui/FLStaticTextView;

    iget-object v1, p0, Lflipboard/gui/item/PageboxList;->e:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->f:Ljava/lang/String;

    invoke-static {v1}, Lflipboard/util/ColorFilterUtil;->a(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setTextColor(I)V

    .line 114
    :cond_9
    return-void

    :cond_a
    move-object v0, v2

    goto/16 :goto_0
.end method
