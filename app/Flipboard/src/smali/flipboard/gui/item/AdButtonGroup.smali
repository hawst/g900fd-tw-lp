.class public Lflipboard/gui/item/AdButtonGroup;
.super Landroid/view/ViewGroup;
.source "AdButtonGroup.java"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/graphics/Paint;

.field private final e:I

.field private final f:I

.field private final g:Landroid/graphics/RectF;

.field private h:Lflipboard/objs/Ad$VideoInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 43
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lflipboard/gui/item/AdButtonGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/item/AdButtonGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, -0x1

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->c:Ljava/util/List;

    .line 35
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    .line 39
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->g:Landroid/graphics/RectF;

    .line 52
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 53
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 54
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090012

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 55
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    .line 56
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/AdButtonGroup;->f:I

    .line 57
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    .line 58
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    const v1, 0x7f02010c

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 60
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AdButtonGroup;->addView(Landroid/view/View;)V

    .line 61
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/AdButtonGroup;)Lflipboard/objs/Ad$VideoInfo;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->h:Lflipboard/objs/Ad$VideoInfo;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Lflipboard/service/Section;Lflipboard/objs/Ad$ButtonInfo;Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/service/Section;",
            "Lflipboard/objs/Ad$ButtonInfo;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 147
    invoke-virtual {p0, v1}, Lflipboard/gui/item/AdButtonGroup;->removeView(Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 146
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 149
    :cond_0
    :try_start_1
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 150
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 152
    if-eqz p2, :cond_4

    iget-object v1, p2, Lflipboard/objs/Ad$ButtonInfo;->a:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 153
    sget-object v1, Lflipboard/service/FLAdManager;->a:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    .line 154
    iget-object v1, p2, Lflipboard/objs/Ad$ButtonInfo;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lflipboard/objs/Ad$Button;

    move-object v3, v0

    .line 155
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03000f

    const/4 v7, 0x0

    invoke-static {v1, v2, v7}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v1, 0x7f0a0040

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    const v2, 0x7f0a0041

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLLabelTextView;

    iget-object v8, v3, Lflipboard/objs/Ad$Button;->d:Ljava/lang/String;

    if-eqz v8, :cond_1

    iget-object v8, v3, Lflipboard/objs/Ad$Button;->d:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :goto_2
    iget-object v1, v3, Lflipboard/objs/Ad$Button;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    const/16 v8, 0xff

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v9

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v10

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v11

    invoke-static {v8, v9, v10, v11}, Landroid/graphics/Color;->argb(IIII)I

    move-result v8

    invoke-virtual {v2, v8}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    iget-object v8, v3, Lflipboard/objs/Ad$Button;->e:Ljava/lang/String;

    invoke-virtual {v2, v8}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v2, Lflipboard/gui/item/AdButtonGroup$1;

    move-object/from16 v0, p3

    invoke-direct {v2, p0, v3, v0, p1}, Lflipboard/gui/item/AdButtonGroup$1;-><init>(Lflipboard/gui/item/AdButtonGroup;Lflipboard/objs/Ad$Button;Ljava/util/List;Lflipboard/service/Section;)V

    invoke-virtual {v7, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    new-instance v8, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v9, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/16 v10, 0x8

    new-array v10, v10, [F

    const/4 v11, 0x0

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x1

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x2

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x3

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x4

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x5

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x6

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x7

    iget v12, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v12, v12

    aput v12, v10, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v9, v10, v11, v12}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-direct {v8, v9}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    const/16 v10, 0x32

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v11

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v12

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    invoke-static {v10, v11, v12, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    sget-object v9, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v9}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v9, 0x0

    const v10, 0x10100a7

    aput v10, v1, v9

    invoke-virtual {v2, v1, v8}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->c:Ljava/util/List;

    iget-object v2, v3, Lflipboard/objs/Ad$Button;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-virtual {p0, v7}, Lflipboard/gui/item/AdButtonGroup;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 155
    :cond_1
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 160
    :cond_2
    iget-boolean v1, p2, Lflipboard/objs/Ad$ButtonInfo;->b:Z

    .line 162
    :goto_3
    iget-object v2, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    if-eqz v1, :cond_3

    move v1, v4

    :goto_4
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 163
    monitor-exit p0

    return-void

    :cond_3
    move v1, v5

    .line 162
    goto :goto_4

    :cond_4
    move v1, v2

    goto :goto_3
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 134
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 135
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 136
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 137
    iget-object v4, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v4, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xff

    :goto_1
    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 139
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v5, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    .line 140
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->g:Landroid/graphics/RectF;

    iget v1, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v1, v1

    iget v4, p0, Lflipboard/gui/item/AdButtonGroup;->e:I

    int-to-float v4, v4

    iget-object v5, p0, Lflipboard/gui/item/AdButtonGroup;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v4, v5}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 135
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 138
    :cond_0
    const/16 v1, 0x80

    goto :goto_1

    .line 142
    :cond_1
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 143
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 107
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    .line 108
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 111
    :cond_0
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 112
    if-lez v1, :cond_2

    .line 113
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    .line 114
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 115
    add-int/lit8 v1, v1, -0x1

    iget v4, p0, Lflipboard/gui/item/AdButtonGroup;->f:I

    mul-int/2addr v4, v1

    .line 117
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 118
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 119
    goto :goto_0

    .line 120
    :cond_1
    sub-int v0, v3, v4

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 123
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 124
    sub-int v5, v2, v4

    div-int/lit8 v5, v5, 0x2

    .line 125
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v1

    .line 126
    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v5, v6, v4}, Landroid/view/View;->layout(IIII)V

    .line 127
    iget v0, p0, Lflipboard/gui/item/AdButtonGroup;->f:I

    add-int/2addr v0, v6

    move v1, v0

    .line 128
    goto :goto_1

    .line 130
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v6, -0x80000000

    const/high16 v7, 0x40000000    # 2.0f

    .line 69
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 70
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 71
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/item/AdButtonGroup;->setMeasuredDimension(II)V

    .line 73
    iget-object v3, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 74
    iget-object v3, p0, Lflipboard/gui/item/AdButtonGroup;->a:Landroid/view/View;

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/view/View;->measure(II)V

    .line 77
    :cond_0
    iget-object v3, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    .line 78
    if-lez v3, :cond_2

    .line 79
    add-int/lit8 v4, v3, -0x1

    iget v5, p0, Lflipboard/gui/item/AdButtonGroup;->f:I

    mul-int/2addr v4, v5

    .line 80
    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-virtual {p0}, Lflipboard/gui/item/AdButtonGroup;->getPaddingRight()I

    move-result v5

    sub-int/2addr v1, v5

    .line 82
    sub-int/2addr v1, v4

    div-int/2addr v1, v3

    .line 85
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 86
    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 90
    iget-object v1, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    move v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 91
    invoke-virtual {v0, v3, v4}, Landroid/view/View;->measure(II)V

    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 93
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 94
    goto :goto_0

    .line 97
    :cond_1
    invoke-static {v2, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 98
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 99
    iget-object v0, p0, Lflipboard/gui/item/AdButtonGroup;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 100
    invoke-virtual {v0, v2, v1}, Landroid/view/View;->measure(II)V

    goto :goto_1

    .line 103
    :cond_2
    return-void
.end method

.method public setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lflipboard/gui/item/AdButtonGroup;->h:Lflipboard/objs/Ad$VideoInfo;

    .line 65
    return-void
.end method
