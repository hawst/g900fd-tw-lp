.class public Lflipboard/gui/item/AudioItemTablet;
.super Landroid/view/ViewGroup;
.source "AudioItemTablet.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# static fields
.field public static c:Lflipboard/util/Log;


# instance fields
.field protected final a:Lflipboard/service/FlipboardManager;

.field protected b:Lflipboard/service/audio/FLAudioManager;

.field d:Lflipboard/objs/FeedItem;

.field e:Lflipboard/service/Section;

.field f:Lflipboard/gui/FLImageView;

.field g:Landroid/widget/ImageButton;

.field h:Lflipboard/gui/FLLabelTextView;

.field i:Lflipboard/gui/FLTextView;

.field j:Lflipboard/gui/FLBusyView;

.field k:Lflipboard/gui/section/Attribution;

.field protected l:Lflipboard/service/audio/FLAudioManager$AudioState;

.field private m:Landroid/view/ViewGroup$LayoutParams;

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Z

.field private p:Lflipboard/gui/item/AudioItemTablet$Layout;

.field private final q:I

.field private r:Lflipboard/gui/FLImageView;

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    sput-object v0, Lflipboard/gui/item/AudioItemTablet;->c:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->a:Lflipboard/service/FlipboardManager;

    .line 57
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/AudioItemTablet;->o:Z

    .line 70
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->setClipToPadding(Z)V

    .line 71
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->b:Lflipboard/service/audio/FLAudioManager;

    .line 72
    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    .line 73
    return-void
.end method

.method static a(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 410
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    .line 411
    int-to-float v2, p3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 412
    int-to-float v3, p2

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 413
    invoke-virtual {v0, v3, v2, v1, v1}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 414
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    div-int/lit8 v4, p3, 0x2

    int-to-float v4, v4

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 416
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 417
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 419
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v9, p0, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 422
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 423
    new-instance v0, Landroid/graphics/LinearGradient;

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v4, v2

    const/high16 v2, 0x50000000

    sub-int v5, p1, v2

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v3, v1

    move v6, p1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    .line 425
    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 427
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v4, v0

    move-object v0, v9

    move v2, v1

    move-object v5, v10

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 429
    return-object v8
.end method

.method static a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 434
    instance-of v0, p0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 435
    check-cast p0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 443
    :goto_0
    return-object v0

    .line 438
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 439
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 440
    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {p0, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 441
    invoke-virtual {p0, v1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/item/AudioItemTablet;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f02004a

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_0
    :goto_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->a:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void

    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f02004b

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/item/AudioItemTablet;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    sget-object v1, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f02004e

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    :cond_1
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->b:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void

    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f02004f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic c(Lflipboard/gui/item/AudioItemTablet;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f020044

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_0
    :goto_0
    sget-object v0, Lflipboard/service/audio/FLAudioManager$AudioState;->c:Lflipboard/service/audio/FLAudioManager$AudioState;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->l:Lflipboard/service/audio/FLAudioManager$AudioState;

    return-void

    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f020045

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method

.method static synthetic d(Lflipboard/gui/item/AudioItemTablet;)Lflipboard/gui/FLImageView;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/item/AudioItemTablet;)I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lflipboard/gui/item/AudioItemTablet;->s:I

    return v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const v4, 0x7f0800aa

    .line 194
    iput-object p1, p0, Lflipboard/gui/item/AudioItemTablet;->e:Lflipboard/service/Section;

    .line 195
    iput-object p2, p0, Lflipboard/gui/item/AudioItemTablet;->d:Lflipboard/objs/FeedItem;

    .line 197
    invoke-virtual {p0, p2}, Lflipboard/gui/item/AudioItemTablet;->setTag(Ljava/lang/Object;)V

    .line 198
    iget v0, p2, Lflipboard/objs/FeedItem;->bz:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 199
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getContext()Landroid/content/Context;

    iget v1, p2, Lflipboard/objs/FeedItem;->bz:F

    float-to-int v1, v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Lflipboard/util/JavaUtil;->a(J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 204
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-static {p2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/section/Attribution;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 207
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 208
    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/AudioItemTablet;->s:I

    .line 209
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lflipboard/gui/section/Attribution;->setInverted(Z)V

    .line 210
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setTextColor(I)V

    .line 211
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 212
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    const v1, 0x7f02004f

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 213
    iget v0, p0, Lflipboard/gui/item/AudioItemTablet;->s:I

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->setBackgroundColor(I)V

    .line 217
    :goto_1
    return-void

    .line 201
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_0

    .line 215
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/AudioItemTablet;->s:I

    goto :goto_1
.end method

.method public final a(Lflipboard/service/audio/FLAudioManager$AudioState;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 265
    new-instance v0, Lflipboard/gui/item/AudioItemTablet$2;

    invoke-direct {v0, p0, p2, p1}, Lflipboard/gui/item/AudioItemTablet$2;-><init>(Lflipboard/gui/item/AudioItemTablet;Lflipboard/objs/FeedItem;Lflipboard/service/audio/FLAudioManager$AudioState;)V

    .line 293
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 294
    return-void
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 346
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "AudioItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 347
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iget-boolean v0, p0, Lflipboard/gui/item/AudioItemTablet;->o:Z

    if-eqz v0, :cond_0

    .line 349
    iput-boolean v3, p0, Lflipboard/gui/item/AudioItemTablet;->o:Z

    .line 350
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->K:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 351
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    new-instance v1, Lflipboard/gui/item/AudioItemTablet$3;

    invoke-direct {v1, p0}, Lflipboard/gui/item/AudioItemTablet$3;-><init>(Lflipboard/gui/item/AudioItemTablet;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/item/AudioItemTablet;->o:Z

    if-nez v0, :cond_0

    .line 392
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/AudioItemTablet;->o:Z

    .line 393
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->removeView(Landroid/view/View;)V

    .line 394
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    .line 395
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 396
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->m:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v3, v1}, Lflipboard/gui/item/AudioItemTablet;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 397
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->removeView(Landroid/view/View;)V

    .line 398
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    .line 399
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0, v3}, Lflipboard/gui/item/AudioItemTablet;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->d:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 78
    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 79
    const v0, 0x7f0a01f4

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    .line 80
    const v0, 0x7f0a01f5

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    .line 81
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 84
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->m:Landroid/view/ViewGroup$LayoutParams;

    .line 85
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->n:Landroid/graphics/drawable/Drawable;

    .line 86
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    .line 87
    const v0, 0x7f0a01f7

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    .line 88
    const v0, 0x7f0a01f6

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    .line 89
    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    .line 90
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AudioItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/Attribution;

    iput-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    .line 92
    new-instance v0, Lflipboard/gui/item/AudioItemTablet$1;

    invoke-direct {v0, p0}, Lflipboard/gui/item/AudioItemTablet$1;-><init>(Lflipboard/gui/item/AudioItemTablet;)V

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    .line 140
    sub-int v0, p4, p2

    .line 141
    sub-int v1, p5, p3

    .line 142
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 143
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    const/4 v4, 0x0

    invoke-interface {v3, v4, v2, v0, v1}, Lflipboard/gui/section/Attribution;->layout(IIII)V

    .line 145
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->p:Lflipboard/gui/item/AudioItemTablet$Layout;

    sget-object v4, Lflipboard/gui/item/AudioItemTablet$Layout;->a:Lflipboard/gui/item/AudioItemTablet$Layout;

    if-ne v3, v4, :cond_1

    .line 146
    iget v3, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    sub-int/2addr v2, v3

    .line 147
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v2, v3

    .line 148
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    iget v5, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    iget v6, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    iget-object v7, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4, v5, v3, v6, v2}, Lflipboard/gui/FLTextView;->layout(IIII)V

    .line 150
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    .line 151
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    .line 153
    div-int/lit8 v1, v1, 0x2

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v1, v5

    .line 154
    iget v5, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    sub-int/2addr v3, v5

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 155
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    sub-int v5, v1, v5

    invoke-virtual {v3, v2, v5, v4, v1}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 156
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v3

    if-eq v3, v8, :cond_0

    .line 157
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {v3, v2, v1, v4, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 160
    :cond_0
    sub-int v2, v0, v4

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    .line 161
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v1, v3

    .line 162
    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    iget-object v6, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v5, v2, v3, v6, v1}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 163
    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    iget-object v6, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    invoke-virtual {v6}, Lflipboard/gui/FLBusyView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v2

    invoke-virtual {v5, v2, v3, v6, v1}, Lflipboard/gui/FLBusyView;->layout(IIII)V

    .line 165
    sub-int/2addr v0, v4

    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    .line 167
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v2}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v3, v2

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v0, v2, v4, v3}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 190
    :goto_0
    return-void

    .line 169
    :cond_1
    iget v1, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    .line 170
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    .line 171
    iget v3, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 172
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    iget v5, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    invoke-virtual {v4, v1, v5, v2, v3}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 173
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v4

    if-eq v4, v8, :cond_2

    .line 174
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {v4, v1, v3, v2, v5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 177
    :cond_2
    iget v1, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    sub-int/2addr v0, v1

    .line 178
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v3, v1

    .line 179
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    .line 180
    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v5, v4, v1, v0, v3}, Landroid/widget/ImageButton;->layout(IIII)V

    .line 181
    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    invoke-virtual {v5, v4, v1, v0, v3}, Lflipboard/gui/FLBusyView;->layout(IIII)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 185
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v1, v4

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v0, v4, v5, v1}, Lflipboard/gui/FLLabelTextView;->layout(IIII)V

    .line 187
    iget v0, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    add-int/2addr v0, v2

    .line 188
    iget-object v1, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    iget v2, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget v4, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Lflipboard/gui/FLTextView;->layout(IIII)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, -0x80000000

    .line 108
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 109
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 110
    invoke-virtual {p0}, Lflipboard/gui/item/AudioItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 111
    int-to-float v3, v1

    div-float v2, v3, v2

    float-to-int v2, v2

    .line 113
    const/16 v3, 0x1f4

    if-ge v2, v3, :cond_1

    sget-object v2, Lflipboard/gui/item/AudioItemTablet$Layout;->b:Lflipboard/gui/item/AudioItemTablet$Layout;

    iput-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->p:Lflipboard/gui/item/AudioItemTablet$Layout;

    .line 114
    :goto_0
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageButton;->measure(II)V

    .line 115
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->j:Lflipboard/gui/FLBusyView;

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v4}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v4

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLBusyView;->measure(II)V

    .line 116
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 117
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-interface {v2, v3, v4}, Lflipboard/gui/section/Attribution;->measure(II)V

    .line 118
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->k:Lflipboard/gui/section/Attribution;

    invoke-interface {v2}, Lflipboard/gui/section/Attribution;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 119
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->p:Lflipboard/gui/item/AudioItemTablet$Layout;

    sget-object v4, Lflipboard/gui/item/AudioItemTablet$Layout;->a:Lflipboard/gui/item/AudioItemTablet$Layout;

    if-ne v3, v4, :cond_2

    .line 120
    iget v3, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 121
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 123
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v3

    iget v4, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    sub-int/2addr v2, v3

    .line 124
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v4, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 125
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v4, v3, v2}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 132
    :goto_1
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 133
    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->r:Lflipboard/gui/FLImageView;

    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v3

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v4

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 135
    :cond_0
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/item/AudioItemTablet;->setMeasuredDimension(II)V

    .line 136
    return-void

    .line 113
    :cond_1
    sget-object v2, Lflipboard/gui/item/AudioItemTablet$Layout;->a:Lflipboard/gui/item/AudioItemTablet$Layout;

    iput-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->p:Lflipboard/gui/item/AudioItemTablet$Layout;

    iget-object v2, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    const/4 v3, 0x1

    const/16 v4, 0x1c

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLTextView;->a(II)V

    goto/16 :goto_0

    .line 127
    :cond_2
    iget v3, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    sub-int/2addr v2, v3

    .line 128
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getMeasuredWidth()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v4, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 129
    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v3, v5}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 130
    iget-object v3, p0, Lflipboard/gui/item/AudioItemTablet;->i:Lflipboard/gui/FLTextView;

    iget-object v4, p0, Lflipboard/gui/item/AudioItemTablet;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v0, v4

    iget v5, p0, Lflipboard/gui/item/AudioItemTablet;->q:I

    mul-int/lit8 v5, v5, 0x3

    sub-int/2addr v4, v5

    invoke-static {v4, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->g:Landroid/widget/ImageButton;

    invoke-virtual {v5}, Landroid/widget/ImageButton;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v2, v5

    iget-object v5, p0, Lflipboard/gui/item/AudioItemTablet;->h:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLLabelTextView;->getMeasuredHeight()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v4, v2}, Lflipboard/gui/FLTextView;->measure(II)V

    goto/16 :goto_1
.end method
