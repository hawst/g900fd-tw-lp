.class public Lflipboard/gui/item/RssDetailView;
.super Lflipboard/gui/item/InflateableDetailView;
.source "RssDetailView.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# static fields
.field public static a:Lflipboard/util/Log;

.field private static final b:Lflipboard/regex/Regex;


# instance fields
.field private c:Z

.field private d:Lflipboard/gui/FLWebView;

.field private e:Lflipboard/gui/dialog/FLVideoDialogFragment;

.field private f:Lflipboard/gui/FLBusyView;

.field private g:Landroid/view/View;

.field private h:Lflipboard/service/FlipboardManager;

.field private i:Lflipboard/service/RemoteWatchedFile;

.field private j:Z

.field private k:Z

.field private l:J

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v13, 0x1

    const/4 v4, 0x0

    .line 38
    const-string v0, "rssdetail"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/item/RssDetailView;->a:Lflipboard/util/Log;

    .line 56
    new-instance v0, Lflipboard/regex/Regex;

    invoke-direct {v0}, Lflipboard/regex/Regex;-><init>()V

    .line 57
    sput-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{language}"

    invoke-virtual {v0, v1, v13}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 58
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{custom}"

    invoke-virtual {v0, v1, v14}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 59
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{title}"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 60
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{feedTitle}"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 61
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{date}"

    const/4 v2, 0x5

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 62
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{sourceURL}"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 63
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{author}"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 64
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "{text}"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 65
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "<head>"

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 66
    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    const-string v1, "({|<)|[^{<]+"

    invoke-virtual {v0, v1, v4}, Lflipboard/regex/Regex;->a(Ljava/lang/String;I)Z

    .line 67
    sget-object v6, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    iget-boolean v0, v6, Lflipboard/regex/Regex;->c:Z

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Regex already compiled"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    iget-object v7, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_1
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_2

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    new-instance v3, Ljava/util/BitSet;

    invoke-direct {v3}, Ljava/util/BitSet;-><init>()V

    invoke-virtual {v0, v3}, Lflipboard/regex/Regex$State;->b(Ljava/util/BitSet;)V

    iput-object v3, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {v0}, Lflipboard/regex/Regex$State;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->set(I)V

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_2
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_3

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    iget-object v0, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->and(Ljava/util/BitSet;)V

    move v0, v1

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v6}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v1

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    iget-object v0, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    iput-object v0, v1, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    iget-object v0, v1, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    invoke-virtual {v8, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v3, v4

    :goto_3
    iget-object v0, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_e

    iget-object v0, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/regex/Regex$State;

    iget-object v9, v1, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    new-instance v10, Ljava/util/BitSet;

    invoke-direct {v10}, Ljava/util/BitSet;-><init>()V

    new-instance v11, Ljava/util/BitSet;

    invoke-direct {v11}, Ljava/util/BitSet;-><init>()V

    invoke-virtual {v9}, Ljava/util/BitSet;->length()I

    move-result v0

    move v2, v4

    :goto_4
    add-int/lit8 v5, v0, -0x1

    if-lez v0, :cond_6

    invoke-virtual {v9, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    iget v12, v0, Lflipboard/regex/Regex$State;->b:I

    if-eqz v12, :cond_5

    if-eqz v2, :cond_4

    iget v12, v0, Lflipboard/regex/Regex$State;->b:I

    if-ge v12, v2, :cond_5

    :cond_4
    iget v2, v0, Lflipboard/regex/Regex$State;->b:I

    :cond_5
    invoke-virtual {v0, v10, v11}, Lflipboard/regex/Regex$State;->a(Ljava/util/BitSet;Ljava/util/BitSet;)V

    move v0, v5

    goto :goto_4

    :cond_6
    invoke-virtual {v10, v11}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    iput v2, v1, Lflipboard/regex/Regex$State;->b:I

    invoke-virtual {v10}, Ljava/util/BitSet;->length()I

    move-result v0

    :goto_5
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_a

    invoke-virtual {v10, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_15

    new-instance v12, Ljava/util/BitSet;

    invoke-direct {v12}, Ljava/util/BitSet;-><init>()V

    invoke-virtual {v9}, Ljava/util/BitSet;->length()I

    move-result v0

    :goto_6
    add-int/lit8 v5, v0, -0x1

    if-lez v0, :cond_7

    invoke-virtual {v9, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    invoke-virtual {v0, v2, v12}, Lflipboard/regex/Regex$State;->a(ILjava/util/BitSet;)V

    move v0, v5

    goto :goto_6

    :cond_7
    invoke-virtual {v12}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v8, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    if-nez v0, :cond_8

    invoke-virtual {v6}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    invoke-virtual {v8, v12, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v12, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    :cond_8
    invoke-virtual {v1, v0, v2}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$State;I)V

    :cond_9
    move v0, v2

    goto :goto_5

    :cond_a
    invoke-virtual {v11, v10}, Ljava/util/BitSet;->or(Ljava/util/BitSet;)V

    new-instance v5, Ljava/util/BitSet;

    invoke-direct {v5}, Ljava/util/BitSet;-><init>()V

    invoke-virtual {v9}, Ljava/util/BitSet;->length()I

    move-result v0

    :goto_7
    add-int/lit8 v2, v0, -0x1

    if-lez v0, :cond_b

    invoke-virtual {v9, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    invoke-virtual {v0, v5}, Lflipboard/regex/Regex$State;->a(Ljava/util/BitSet;)V

    move v0, v2

    goto :goto_7

    :cond_b
    invoke-virtual {v5}, Ljava/util/BitSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {v8, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    if-nez v0, :cond_c

    invoke-virtual {v6}, Lflipboard/regex/Regex;->a()Lflipboard/regex/Regex$State;

    move-result-object v0

    invoke-virtual {v8, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v5, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    :cond_c
    invoke-virtual {v1, v0, v11}, Lflipboard/regex/Regex$State;->a(Lflipboard/regex/Regex$State;Ljava/util/BitSet;)V

    :cond_d
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_3

    :cond_e
    new-instance v2, Ljava/util/BitSet;

    invoke-direct {v2}, Ljava/util/BitSet;-><init>()V

    move v1, v4

    :goto_8
    iget-object v0, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_11

    iget-object v0, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    iget v3, v0, Lflipboard/regex/Regex$State;->b:I

    if-lez v3, :cond_f

    iget v3, v0, Lflipboard/regex/Regex$State;->b:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    :cond_f
    const/4 v3, 0x0

    iput-object v3, v0, Lflipboard/regex/Regex$State;->c:Ljava/util/BitSet;

    iget-object v0, v6, Lflipboard/regex/Regex;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$State;

    invoke-virtual {v0, v14}, Lflipboard/regex/Regex$State;->a(I)Lflipboard/regex/Regex$State;

    move-result-object v0

    if-eqz v0, :cond_10

    iget v0, v0, Lflipboard/regex/Regex$State;->b:I

    if-nez v0, :cond_10

    const-string v0, "compiled"

    invoke-virtual {v6, v0}, Lflipboard/regex/Regex;->a(Ljava/lang/String;)V

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "invalid action after $ (state %d)"

    new-array v5, v13, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v4

    invoke-virtual {v0, v3, v5}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_8

    :cond_11
    move v1, v4

    :goto_9
    iget-object v0, v6, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_13

    invoke-virtual {v2, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_12

    sget-object v3, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v5, "pattern never matched: %s"

    new-array v7, v13, [Ljava/lang/Object;

    iget-object v0, v6, Lflipboard/regex/Regex;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/regex/Regex$Action;

    iget-object v0, v0, Lflipboard/regex/Regex$Action;->a:Ljava/lang/String;

    aput-object v0, v7, v4

    invoke-virtual {v3, v5, v7}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_12
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    :cond_13
    iput-boolean v13, v6, Lflipboard/regex/Regex;->c:Z

    goto/16 :goto_0

    :cond_14
    move v0, v2

    goto/16 :goto_7

    :cond_15
    move v0, v2

    goto/16 :goto_5

    :cond_16
    move v0, v5

    goto/16 :goto_6

    :cond_17
    move v0, v5

    goto/16 :goto_4
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 90
    invoke-direct {p0, p1, p2}, Lflipboard/gui/item/InflateableDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 75
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    .line 78
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    iput-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->k:Z

    .line 80
    iput-wide v2, p0, Lflipboard/gui/item/RssDetailView;->l:J

    .line 81
    iput-wide v2, p0, Lflipboard/gui/item/RssDetailView;->m:J

    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/item/RssDetailView;->l:J

    .line 93
    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->c()V

    .line 96
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 98
    invoke-direct {p0}, Lflipboard/gui/item/RssDetailView;->e()V

    .line 151
    :goto_0
    return-void

    .line 102
    :cond_0
    const-string v0, "flipboard_settings"

    invoke-virtual {p1, v0, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 105
    const-string v1, "override_rss_html"

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    iget-object v2, v2, Lflipboard/model/ConfigSetting;->FeedTemplateHTMLURLString:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    if-nez v1, :cond_2

    .line 110
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 111
    invoke-static {p1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iget-object v2, p2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 120
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLWebView;->setScrollBarStyle(I)V

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v2, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lflipboard/gui/item/RssDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/gui/item/RssDetailView;->f:Lflipboard/gui/FLBusyView;

    const v0, 0x7f0a016a

    invoke-virtual {p0, v0}, Lflipboard/gui/item/RssDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/RssDetailView;->g:Landroid/view/View;

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    const-string v0, "RSSDetailView:showLoadingIndicator"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->g:Landroid/view/View;

    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->d()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    const/high16 v3, 0x10a0000

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_4
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/RssDetailView$3;

    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lflipboard/gui/item/RssDetailView$3;-><init>(Lflipboard/gui/item/RssDetailView;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    new-instance v0, Lflipboard/gui/item/RssDetailView$4;

    invoke-direct {v0, p0}, Lflipboard/gui/item/RssDetailView$4;-><init>(Lflipboard/gui/item/RssDetailView;)V

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->FeedTemplateHTMLURLString:Ljava/lang/String;

    if-eq v1, v0, :cond_6

    .line 125
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->N:Lflipboard/io/DownloadManager;

    invoke-virtual {v0, v1, v5, v5}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;ZZ)Lflipboard/io/Download;

    move-result-object v0

    .line 126
    new-instance v2, Lflipboard/gui/item/RssDetailView$1;

    invoke-direct {v2, p0, v1}, Lflipboard/gui/item/RssDetailView$1;-><init>(Lflipboard/gui/item/RssDetailView;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lflipboard/io/Download;->a(Lflipboard/io/Download$Observer;)V

    goto/16 :goto_0

    .line 120
    :cond_5
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->f:Lflipboard/gui/FLBusyView;

    goto :goto_1

    .line 149
    :cond_6
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, v1, p0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    goto/16 :goto_0
.end method

.method static synthetic a(Lflipboard/gui/item/RssDetailView;J)J
    .locals 1

    .prologue
    .line 36
    iput-wide p1, p0, Lflipboard/gui/item/RssDetailView;->m:J

    return-wide p1
.end method

.method static synthetic a(Lflipboard/gui/item/RssDetailView;Lflipboard/gui/dialog/FLVideoDialogFragment;)Lflipboard/gui/dialog/FLVideoDialogFragment;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lflipboard/gui/item/RssDetailView;->e:Lflipboard/gui/dialog/FLVideoDialogFragment;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/item/RssDetailView;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lflipboard/gui/item/RssDetailView;->e()V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/RssDetailView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/RssDetailView$2;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/item/RssDetailView$2;-><init>(Lflipboard/gui/item/RssDetailView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/item/RssDetailView;Ljava/lang/String;)Ljava/lang/StringBuilder;
    .locals 8

    .prologue
    .line 36
    new-instance v1, Lflipboard/regex/Tokenizer;

    invoke-direct {v1, p1}, Lflipboard/regex/Tokenizer;-><init>(Ljava/lang/String;)V

    sget-object v0, Lflipboard/gui/item/RssDetailView;->b:Lflipboard/regex/Regex;

    iget-boolean v2, v0, Lflipboard/regex/Regex;->c:Z

    if-nez v2, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Regex not compiled: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v2, v1, Lflipboard/regex/Tokenizer;->a:Ljava/util/Stack;

    invoke-virtual {v2, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    :goto_0
    invoke-virtual {v1}, Lflipboard/regex/Tokenizer;->a()V

    iget v0, v1, Lflipboard/regex/Tokenizer;->c:I

    const/4 v3, -0x1

    if-eq v0, v3, :cond_6

    iget v0, v1, Lflipboard/regex/Tokenizer;->c:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, v1, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lflipboard/gui/item/RssDetailView;->getCustomClasses()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->C:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->C:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_2

    :pswitch_5
    invoke-virtual {p0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v3, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-wide v4, v3, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v0, v4, v5}, Lflipboard/util/JavaUtil;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/HttpUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    const-string v3, "/#"

    const-string v4, "/%23"

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_3

    :pswitch_7
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_4
    const-string v0, ""

    goto :goto_4

    :pswitch_8
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->A:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :pswitch_9
    iget-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->c:Z

    if-nez v0, :cond_5

    const-string v0, "<head><style type=\"text/css\">@font-face{font-family: HelveticaNeueLTW1G-MdCn; src: url(file:///android_asset/fonts/HelveticaNeueLTW1G-MdCn.otf)}@font-face{font-family: HelveticaNeueLTW1G-LtCn; src: url(file:///android_asset/fonts/HelveticaNeueLTW1G-LtCn.otf)}@font-face{font-family: HelveticaNeueLTW1G-Cn; src: url(file:///android_asset/fonts/HelveticaNeueLTW1G-Cn.otf)}@font-face{font-family: HelveticaNeueLTW1G-BdCn; src: url(file:///android_asset/fonts/HelveticaNeueLTW1G-BdCn.otf)}</style>"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->c:Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, v1, Lflipboard/regex/Tokenizer;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_6
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method static synthetic b(Lflipboard/gui/item/RssDetailView;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->j:Z

    return v0
.end method

.method static synthetic c(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/dialog/FLVideoDialogFragment;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->e:Lflipboard/gui/dialog/FLVideoDialogFragment;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/item/RssDetailView;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->k:Z

    return v0
.end method

.method static synthetic e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 510
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->g:Landroid/view/View;

    .line 511
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 512
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/item/RssDetailView$8;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/item/RssDetailView$8;-><init>(Lflipboard/gui/item/RssDetailView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 531
    :cond_0
    return-void

    .line 510
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->f:Lflipboard/gui/FLBusyView;

    goto :goto_0
.end method

.method static synthetic f(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/FLWebView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    return-object v0
.end method

.method static synthetic g(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method private getCustomClasses()Ljava/lang/String;
    .locals 2

    .prologue
    .line 466
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 467
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aR:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 468
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 473
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 475
    :cond_1
    const-string v1, "fullscreen"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 487
    invoke-super {p0}, Lflipboard/gui/item/InflateableDetailView;->a()V

    .line 488
    iget-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->k:Z

    if-nez v0, :cond_0

    .line 489
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/gui/item/RssDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 490
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 491
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 493
    :cond_0
    const v0, 0x7f0a0168

    invoke-virtual {p0, v0}, Lflipboard/gui/item/RssDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLWebView;

    iput-object v0, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    .line 494
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 406
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/RssDetailView$6;

    invoke-direct {v1, p0}, Lflipboard/gui/item/RssDetailView$6;-><init>(Lflipboard/gui/item/RssDetailView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 413
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0, p0}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 416
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 2

    .prologue
    .line 359
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "javascript:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 360
    new-instance v1, Lflipboard/gui/item/RssDetailView$5;

    invoke-direct {v1, p0, p2, v0}, Lflipboard/gui/item/RssDetailView$5;-><init>(Lflipboard/gui/item/RssDetailView;[BLjava/lang/StringBuilder;)V

    invoke-static {v1}, Lflipboard/util/AndroidUtil;->a(Landroid/os/AsyncTask;)V

    .line 402
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->h:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/RssDetailView$7;

    invoke-direct {v1, p0}, Lflipboard/gui/item/RssDetailView$7;-><init>(Lflipboard/gui/item/RssDetailView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 427
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    if-eqz v0, :cond_0

    .line 428
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0, p0}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 430
    :cond_0
    return-void
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 482
    iget-boolean v0, p0, Lflipboard/gui/item/RssDetailView;->k:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030064

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f030063

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 6

    .prologue
    .line 437
    invoke-super {p0}, Lflipboard/gui/item/InflateableDetailView;->onDetachedFromWindow()V

    .line 438
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->i:Lflipboard/service/RemoteWatchedFile;

    invoke-virtual {v0, p0}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 441
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView;->d:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->stopLoading()V

    .line 444
    iget-wide v0, p0, Lflipboard/gui/item/RssDetailView;->l:J

    iget-wide v2, p0, Lflipboard/gui/item/RssDetailView;->m:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 446
    const-string v0, "rssview_open_total_succeed"

    iget-wide v2, p0, Lflipboard/gui/item/RssDetailView;->m:J

    iget-wide v4, p0, Lflipboard/gui/item/RssDetailView;->l:J

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 450
    :goto_0
    return-void

    .line 448
    :cond_1
    const-string v0, "rssview_open_total_fail"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lflipboard/gui/item/RssDetailView;->l:J

    sub-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    goto :goto_0
.end method
