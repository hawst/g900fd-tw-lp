.class public Lflipboard/gui/item/SectionItemTablet;
.super Lflipboard/gui/FLRelativeLayout;
.source "SectionItemTablet.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field b:Lflipboard/objs/FeedItem;

.field c:Lflipboard/service/Section;

.field d:Lflipboard/gui/item/SectionItemCoverImage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "section-item"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/item/SectionItemTablet;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const/4 v3, -0x2

    const/4 v5, 0x0

    .line 38
    iput-object p2, p0, Lflipboard/gui/item/SectionItemTablet;->b:Lflipboard/objs/FeedItem;

    .line 39
    iput-object p1, p0, Lflipboard/gui/item/SectionItemTablet;->c:Lflipboard/service/Section;

    .line 40
    invoke-virtual {p0, p2}, Lflipboard/gui/item/SectionItemTablet;->setTag(Ljava/lang/Object;)V

    .line 41
    iget-object v1, p0, Lflipboard/gui/item/SectionItemTablet;->d:Lflipboard/gui/item/SectionItemCoverImage;

    iput-object p1, v1, Lflipboard/gui/item/SectionItemCoverImage;->a:Lflipboard/service/Section;

    iput-object p2, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v5, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->f:Lflipboard/gui/FLTextIntf;

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLStaticTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    invoke-virtual {v1, p2}, Lflipboard/gui/item/SectionItemCoverImage;->setImageOnView(Lflipboard/objs/FeedItem;)V

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v0, :cond_4

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->e:Lflipboard/gui/FLImageView;

    iget-object v2, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->e:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->f:Lflipboard/gui/FLTextIntf;

    iget-object v2, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    invoke-static {v2}, Lflipboard/gui/section/ItemDisplayUtil;->a(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->g:Lflipboard/gui/FLTextIntf;

    invoke-virtual {v1}, Lflipboard/gui/item/SectionItemCoverImage;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d0324

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    invoke-virtual {v1}, Lflipboard/gui/item/SectionItemCoverImage;->a()V

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->ae()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    invoke-virtual {v0, v1}, Lflipboard/objs/FeedSection;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    :cond_2
    new-instance v0, Lflipboard/gui/item/SectionItemCoverImage$2;

    invoke-direct {v0, v1}, Lflipboard/gui/item/SectionItemCoverImage$2;-><init>(Lflipboard/gui/item/SectionItemCoverImage;)V

    iput-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->l:Landroid/view/View$OnClickListener;

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    iget-object v2, v1, Lflipboard/gui/item/SectionItemCoverImage;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->m:Lflipboard/gui/FollowButton;

    iget-object v1, v1, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->C()Lflipboard/objs/FeedSectionLink;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    .line 43
    iget-boolean v0, p2, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_3

    .line 44
    const v0, 0x7f0a0204

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 46
    :cond_3
    return-void

    .line 41
    :cond_4
    iget-object v0, v1, Lflipboard/gui/item/SectionItemCoverImage;->e:Lflipboard/gui/FLImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lflipboard/gui/item/SectionItemTablet;->b:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 31
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 32
    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 33
    const v0, 0x7f0a022f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/SectionItemCoverImage;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemTablet;->d:Lflipboard/gui/item/SectionItemCoverImage;

    .line 34
    return-void
.end method
