.class public Lflipboard/gui/item/ListItemTablet;
.super Landroid/view/ViewGroup;
.source "ListItemTablet.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:I

.field private b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    .line 44
    iget-object v0, p2, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 45
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0300a5

    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/item/ListItemTablet$ListItem;

    .line 46
    iget-object v3, p0, Lflipboard/gui/item/ListItemTablet;->b:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_0

    .line 48
    iget-object v3, p0, Lflipboard/gui/item/ListItemTablet;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Lflipboard/gui/item/ListItemTablet$ListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    :cond_0
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->F()Lflipboard/objs/FeedItem;

    move-result-object v3

    .line 53
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->G()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-gtz v4, :cond_2

    :cond_1
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->H()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 54
    :cond_2
    invoke-virtual {v1, p1, v3}, Lflipboard/gui/item/ListItemTablet$ListItem;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 55
    invoke-virtual {p0, v1}, Lflipboard/gui/item/ListItemTablet;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 57
    :cond_3
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "Got invalid status item from server: %s in feed %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v0, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v5, v5, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 60
    :cond_4
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 126
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 127
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingTop()I

    move-result v0

    iget v1, p0, Lflipboard/gui/item/ListItemTablet;->a:I

    add-int/2addr v1, v0

    .line 129
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 130
    invoke-virtual {p0, v0}, Lflipboard/gui/item/ListItemTablet;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 131
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v1

    invoke-virtual {v3, v4, v1, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 132
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    iget v4, p0, Lflipboard/gui/item/ListItemTablet;->a:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 129
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    .line 69
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 71
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 72
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 73
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 74
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingLeft()I

    move-result v0

    sub-int v0, v4, v0

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingRight()I

    move-result v1

    sub-int v7, v0, v1

    .line 75
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingTop()I

    move-result v0

    sub-int v0, v5, v0

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getPaddingBottom()I

    move-result v1

    sub-int v3, v0, v1

    .line 76
    mul-int/lit8 v0, v5, 0x2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getChildCount()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    div-int v8, v0, v1

    .line 77
    const/4 v1, 0x0

    .line 78
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 81
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 82
    invoke-virtual {p0, v1}, Lflipboard/gui/item/ListItemTablet;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet$ListItem;

    .line 83
    const/high16 v10, -0x80000000

    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    const/high16 v11, -0x80000000

    invoke-static {v8, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v0, v10, v11}, Lflipboard/gui/item/ListItemTablet$ListItem;->measure(II)V

    .line 84
    invoke-virtual {v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v2, v10

    .line 85
    if-lez v1, :cond_0

    .line 86
    add-int/2addr v2, v6

    .line 88
    :cond_0
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 92
    :cond_1
    if-le v2, v3, :cond_4

    .line 93
    invoke-static {v9}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 95
    sub-int v0, v2, v3

    int-to-float v1, v0

    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet$ListItem;

    invoke-static {v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->a(Lflipboard/gui/item/ListItemTablet$ListItem;)Lflipboard/gui/FLStaticTextView;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getLineHeight()F

    move-result v0

    div-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v1, v0

    .line 96
    :cond_2
    if-lez v1, :cond_3

    .line 97
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet$ListItem;

    invoke-virtual {v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getLineCount()I

    move-result v10

    .line 98
    const/4 v0, 0x1

    if-le v10, v0, :cond_3

    .line 99
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    if-lez v1, :cond_2

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet$ListItem;

    invoke-virtual {v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getLineCount()I

    move-result v0

    if-ne v0, v10, :cond_2

    .line 102
    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet$ListItem;

    add-int/lit8 v11, v10, -0x1

    invoke-virtual {v0, v11}, Lflipboard/gui/item/ListItemTablet$ListItem;->setLineCount(I)V

    .line 103
    add-int/lit8 v0, v1, -0x1

    .line 101
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 108
    :cond_3
    const/4 v0, 0x0

    move v1, v0

    move v2, v3

    :goto_2
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 109
    invoke-virtual {p0, v1}, Lflipboard/gui/item/ListItemTablet;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/ListItemTablet$ListItem;

    .line 110
    const/high16 v3, -0x80000000

    invoke-static {v7, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v9

    const/high16 v10, -0x80000000

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v0, v3, v9}, Lflipboard/gui/item/ListItemTablet$ListItem;->measure(II)V

    .line 111
    const/4 v3, 0x0

    invoke-virtual {v0}, Lflipboard/gui/item/ListItemTablet$ListItem;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v2, v0

    sub-int/2addr v0, v6

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 114
    :cond_4
    sub-int v2, v3, v2

    .line 116
    :cond_5
    if-le v2, v6, :cond_6

    .line 117
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int v0, v2, v0

    iput v0, p0, Lflipboard/gui/item/ListItemTablet;->a:I

    .line 118
    iget v0, p0, Lflipboard/gui/item/ListItemTablet;->a:I

    mul-int/lit8 v1, v6, 0x3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/ListItemTablet;->a:I

    .line 121
    :cond_6
    invoke-virtual {p0, v4, v5}, Lflipboard/gui/item/ListItemTablet;->setMeasuredDimension(II)V

    .line 122
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 138
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iput-object p1, p0, Lflipboard/gui/item/ListItemTablet;->b:Landroid/view/View$OnClickListener;

    .line 141
    invoke-virtual {p0}, Lflipboard/gui/item/ListItemTablet;->getChildCount()I

    move-result v1

    .line 142
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 143
    invoke-virtual {p0, v0}, Lflipboard/gui/item/ListItemTablet;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method
