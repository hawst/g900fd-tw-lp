.class public Lflipboard/gui/item/PageboxGrid;
.super Landroid/view/ViewGroup;
.source "PageboxGrid.java"

# interfaces
.implements Lflipboard/gui/item/PageboxView;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field private b:Lflipboard/objs/SidebarGroup$RenderHints;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final d:I

.field private final e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:Lflipboard/gui/FLLabelTextView;

.field private j:Lflipboard/objs/SidebarGroup;

.field private k:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    .line 38
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxGrid;->d:I

    .line 39
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900eb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxGrid;->g:I

    .line 40
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    .line 41
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/PageboxGrid;)Lflipboard/objs/SidebarGroup;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/item/PageboxGrid;)Ljava/util/List;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 54
    iput-object p2, p0, Lflipboard/gui/item/PageboxGrid;->a:Lflipboard/objs/FeedItem;

    .line 55
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 47
    const v0, 0x7f0a00fd

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxGrid;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    .line 48
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxGrid;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/item/PageboxGrid;->i:Lflipboard/gui/FLLabelTextView;

    .line 49
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 154
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingLeft()I

    move-result v2

    .line 155
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingTop()I

    move-result v0

    .line 156
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v2

    iget-object v4, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 158
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 160
    :cond_0
    const/4 v1, 0x0

    move v3, v2

    move v2, v0

    .line 162
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 163
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 164
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    .line 165
    add-int/lit8 v1, v1, 0x1

    .line 166
    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->f:I

    rem-int v4, v1, v4

    if-nez v4, :cond_1

    .line 167
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget v3, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    .line 168
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingLeft()I

    move-result v2

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    move v3, v0

    .line 172
    goto :goto_0

    .line 173
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 117
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 118
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 119
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/item/PageboxGrid;->setMeasuredDimension(II)V

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingRight()I

    move-result v3

    sub-int v3, v0, v3

    .line 122
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingTop()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 124
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->b:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-boolean v1, v1, Lflipboard/objs/SidebarGroup$RenderHints;->k:Z

    if-eqz v1, :cond_2

    .line 125
    :cond_1
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v1, v4, v6}, Landroid/view/View;->measure(II)V

    .line 126
    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    add-int/2addr v1, v4

    sub-int/2addr v0, v1

    .line 130
    :cond_2
    iget v1, p0, Lflipboard/gui/item/PageboxGrid;->d:I

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    add-int/2addr v1, v4

    div-int v1, v3, v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/PageboxGrid;->f:I

    .line 131
    iget v1, p0, Lflipboard/gui/item/PageboxGrid;->g:I

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    add-int/2addr v1, v4

    div-int v1, v0, v1

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/PageboxGrid;->h:I

    .line 132
    iget v1, p0, Lflipboard/gui/item/PageboxGrid;->f:I

    add-int/lit8 v1, v1, -0x1

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    mul-int/2addr v1, v4

    sub-int v1, v3, v1

    iget v3, p0, Lflipboard/gui/item/PageboxGrid;->f:I

    div-int/2addr v1, v3

    .line 133
    iget v3, p0, Lflipboard/gui/item/PageboxGrid;->h:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    mul-int/2addr v3, v4

    sub-int v3, v0, v3

    iget v4, p0, Lflipboard/gui/item/PageboxGrid;->h:I

    div-int v6, v3, v4

    .line 135
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 136
    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    move v1, v2

    move v3, v2

    move v4, v0

    .line 140
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    if-nez v3, :cond_4

    .line 141
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 142
    invoke-virtual {v0, v7, v8}, Landroid/view/View;->measure(II)V

    .line 143
    add-int/lit8 v0, v1, 0x1

    .line 144
    iget v1, p0, Lflipboard/gui/item/PageboxGrid;->f:I

    rem-int v1, v0, v1

    if-nez v1, :cond_5

    .line 145
    iget v1, p0, Lflipboard/gui/item/PageboxGrid;->e:I

    add-int/2addr v1, v6

    sub-int v3, v4, v1

    .line 146
    if-ge v3, v6, :cond_3

    move v1, v5

    :goto_1
    move v4, v3

    move v3, v1

    move v1, v0

    .line 148
    goto :goto_0

    :cond_3
    move v1, v2

    .line 146
    goto :goto_1

    .line 149
    :cond_4
    return-void

    :cond_5
    move v1, v3

    move v3, v4

    goto :goto_1
.end method

.method public setPagebox(Lflipboard/objs/SidebarGroup;)V
    .locals 6

    .prologue
    .line 64
    iput-object p1, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    .line 65
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    invoke-virtual {v0}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/PageboxGrid;->b:Lflipboard/objs/SidebarGroup$RenderHints;

    .line 66
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->i:Lflipboard/gui/FLLabelTextView;

    iget-object v1, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    iget-object v1, v1, Lflipboard/objs/SidebarGroup;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 70
    const-string v1, "pageboxGrid"

    iput-object v1, v0, Lflipboard/objs/FeedItem;->bK:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxGrid;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0300a7

    const/4 v4, 0x0

    invoke-static {v1, v2, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 72
    const v1, 0x7f0a004f

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    .line 73
    const v2, 0x7f0a0040

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    .line 74
    iget-object v5, v0, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v1, v5}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 76
    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 77
    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_0

    .line 78
    const v0, 0x7f0a0204

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 80
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->c:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    invoke-virtual {p0, v4}, Lflipboard/gui/item/PageboxGrid;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/PageboxGrid;->j:Lflipboard/objs/SidebarGroup;

    new-instance v1, Lflipboard/gui/item/PageboxGrid$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/PageboxGrid$1;-><init>(Lflipboard/gui/item/PageboxGrid;)V

    invoke-virtual {v0, v1}, Lflipboard/objs/SidebarGroup;->a(Lflipboard/service/Flap$TypedResultObserver;)V

    .line 112
    return-void
.end method
