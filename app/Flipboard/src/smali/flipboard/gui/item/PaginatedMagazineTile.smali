.class public Lflipboard/gui/item/PaginatedMagazineTile;
.super Lflipboard/gui/FLViewGroup;
.source "PaginatedMagazineTile.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field public a:Lflipboard/gui/FLTextView;

.field public b:Lflipboard/gui/FLTextView;

.field public c:Lflipboard/gui/FLLabelTextView;

.field public d:Landroid/view/View;

.field public e:Lflipboard/gui/FLTextView;

.field public f:Lflipboard/gui/FollowButton;

.field private g:Lflipboard/objs/FeedItem;

.field private h:I

.field private i:F

.field private j:F

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;)V

    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    invoke-direct {p0, p1, p2}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 68
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    invoke-direct {p0, p1, p2}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const v1, 0x3f4ccccd    # 0.8f

    const/4 v4, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 78
    .line 80
    if-eqz p2, :cond_0

    .line 81
    sget-object v2, Lflipboard/app/R$styleable;->CarouselView:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 82
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-static {v1, v4, v0}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v1

    .line 83
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v2

    invoke-static {v2, v4, v0}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    .line 85
    :cond_0
    iput v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->i:F

    .line 86
    iput v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->j:F

    .line 87
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 179
    iput-object p2, p0, Lflipboard/gui/item/PaginatedMagazineTile;->g:Lflipboard/objs/FeedItem;

    .line 181
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->a:Lflipboard/gui/FLTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0324

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->A()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->by:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    new-instance v1, Lflipboard/objs/FeedSectionLink;

    iget-object v2, p0, Lflipboard/gui/item/PaginatedMagazineTile;->g:Lflipboard/objs/FeedItem;

    invoke-direct {v1, v2}, Lflipboard/objs/FeedSectionLink;-><init>(Lflipboard/objs/FeedItem;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setSectionLink(Lflipboard/objs/FeedSectionLink;)V

    .line 188
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, v5}, Lflipboard/gui/FollowButton;->setInverted(Z)V

    .line 189
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    invoke-virtual {v0}, Lflipboard/gui/FollowButton;->a()V

    .line 190
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    sget-object v1, Lflipboard/objs/UsageEventV2$FollowFrom;->a:Lflipboard/objs/UsageEventV2$FollowFrom;

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setFrom(Lflipboard/objs/UsageEventV2$FollowFrom;)V

    .line 192
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->g:Lflipboard/objs/FeedItem;

    iget-boolean v0, v0, Lflipboard/objs/FeedItem;->bN:Z

    if-eqz v0, :cond_4

    .line 194
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d030b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 200
    :cond_2
    :goto_1
    return-void

    .line 182
    :cond_3
    const-string v0, ""

    goto :goto_0

    .line 197
    :cond_4
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->g:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Lflipboard/gui/FLViewGroup;->onFinishInflate()V

    .line 92
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 93
    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->h:I

    .line 94
    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020108

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->k:I

    .line 95
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingLeft()I

    move-result v2

    .line 138
    sub-int v0, p4, p2

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 139
    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingTop()I

    move-result v4

    sub-int v0, p5, p3

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 140
    sub-int/2addr v0, v4

    .line 147
    new-array v1, v7, [Landroid/view/View;

    const/4 v5, 0x0

    iget-object v6, p0, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    aput-object v6, v1, v5

    invoke-static {v1}, Lflipboard/gui/item/PaginatedMagazineTile;->a([Landroid/view/View;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 150
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    if-eqz v1, :cond_2

    .line 151
    iget v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->k:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 153
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 155
    iget-object v5, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v5}, Lflipboard/gui/FLTextView;->getLineHeight()I

    move-result v5

    mul-int/lit8 v5, v5, 0x3

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v5, v6

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v5

    sub-int/2addr v1, v0

    .line 158
    :cond_0
    div-int/lit8 v0, v1, 0x2

    add-int/2addr v0, v4

    .line 159
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    invoke-static {v1, v0, v2, v3, v7}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/view/View;IIII)I

    move-result v1

    add-int/2addr v1, v0

    .line 161
    iget v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->h:I

    add-int/2addr v4, v0

    .line 163
    iget-object v5, p0, Lflipboard/gui/item/PaginatedMagazineTile;->a:Lflipboard/gui/FLTextView;

    invoke-static {v5, v4, v2, v3, v7}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/view/View;IIII)I

    move-result v5

    add-int/2addr v4, v5

    .line 165
    iget-object v5, p0, Lflipboard/gui/item/PaginatedMagazineTile;->b:Lflipboard/gui/FLTextView;

    invoke-static {v5, v4, v2, v3, v7}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/view/View;IIII)I

    .line 167
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    .line 168
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v0, v4

    iget v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->h:I

    sub-int/2addr v0, v4

    .line 169
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    invoke-static {v4, v0, v2, v3}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/view/View;III)I

    .line 172
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    invoke-static {v0, v1, v2, v3, v7}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/view/View;IIII)I

    move-result v0

    add-int/2addr v0, v1

    .line 173
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    invoke-static {v1, v0, v2, v3, v7}, Lflipboard/gui/item/PaginatedMagazineTile;->a(Landroid/view/View;IIII)I

    .line 174
    return-void

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    const/4 v7, 0x0

    .line 99
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingLeft()I

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingRight()I

    .line 100
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/item/PaginatedMagazineTile;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    .line 102
    iget v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->j:F

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-int v2, v0

    .line 103
    iget v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->i:F

    int-to-float v1, v2

    mul-float/2addr v0, v1

    float-to-int v3, v0

    .line 106
    iget v0, p0, Lflipboard/gui/item/PaginatedMagazineTile;->h:I

    mul-int/lit8 v0, v0, 0x4

    sub-int v1, v3, v0

    .line 107
    mul-int/lit8 v0, v1, 0x5

    div-int/lit8 v0, v0, 0x4

    .line 108
    if-lt v0, v2, :cond_0

    .line 109
    mul-int/lit8 v0, v2, 0x4

    div-int/lit8 v0, v0, 0x5

    .line 110
    mul-int/lit8 v1, v0, 0x4

    div-int/lit8 v1, v1, 0x5

    .line 112
    :cond_0
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/view/View;->measure(II)V

    .line 115
    iget v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->h:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v1, v4

    .line 116
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->a:Lflipboard/gui/FLTextView;

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 118
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLLabelTextView;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_1

    .line 119
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->c:Lflipboard/gui/FLLabelTextView;

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lflipboard/gui/FLLabelTextView;->measure(II)V

    .line 121
    :cond_1
    iget-object v4, p0, Lflipboard/gui/item/PaginatedMagazineTile;->b:Lflipboard/gui/FLTextView;

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v1, v5}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 123
    sub-int v0, v2, v0

    .line 124
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    if-eqz v1, :cond_2

    .line 125
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v1, v2, v4}, Lflipboard/gui/FollowButton;->measure(II)V

    .line 126
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    invoke-virtual {v1}, Lflipboard/gui/FollowButton;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 129
    :cond_2
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    if-eqz v1, :cond_3

    .line 130
    iget-object v1, p0, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    iget v2, p0, Lflipboard/gui/item/PaginatedMagazineTile;->h:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v3, v2

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/FLTextView;->measure(II)V

    .line 132
    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/item/PaginatedMagazineTile;->setMeasuredDimension(II)V

    .line 133
    return-void
.end method
