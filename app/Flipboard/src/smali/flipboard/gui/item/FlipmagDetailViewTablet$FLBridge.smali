.class Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;
.super Ljava/lang/Object;
.source "FlipmagDetailViewTablet.java"


# instance fields
.field final synthetic a:Lflipboard/gui/item/FlipmagDetailViewTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/FlipmagDetailViewTablet;)V
    .locals 0

    .prologue
    .line 1170
    iput-object p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dumpHTML(Ljava/lang/String;)V
    .locals 1
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1191
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    .line 1192
    return-void
.end method

.method public log(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1174
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1175
    return-void
.end method

.method public parseHTML(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1196
    invoke-virtual {p0, p1}, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->parseHTMLForLinks(Ljava/lang/String;)V

    .line 1197
    return-void
.end method

.method public parseHTMLForLinks(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1201
    const/4 v0, 0x0

    .line 1203
    :goto_0
    const-string v1, "<a href=\""

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    .line 1204
    add-int/lit8 v1, v0, 0x9

    .line 1205
    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    const-string v0, "\""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->b(Lflipboard/gui/item/FlipmagDetailViewTablet;Ljava/lang/String;)V

    goto :goto_0

    .line 1207
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->d(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    .line 1208
    return-void
.end method

.method public setContentHeight(Ljava/lang/String;)V
    .locals 3
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1179
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1180
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget v2, v2, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Landroid/util/FloatMath;->ceil(F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->setContentHeight(I)V

    .line 1181
    return-void
.end method

.method public setContentWidth(Ljava/lang/String;)V
    .locals 4
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 1185
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1186
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget v2, v2, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    invoke-virtual {v0, v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->setContentWidth(I)V

    .line 1187
    return-void
.end method
