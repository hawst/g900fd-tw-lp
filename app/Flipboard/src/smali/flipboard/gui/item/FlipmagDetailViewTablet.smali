.class public Lflipboard/gui/item/FlipmagDetailViewTablet;
.super Lflipboard/gui/FLViewFlipper;
.source "FlipmagDetailViewTablet.java"

# interfaces
.implements Lflipboard/gui/item/DetailView;


# static fields
.field public static final b:Lflipboard/util/Log;


# instance fields
.field private A:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/audio/FLAudioManager;",
            "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private B:Landroid/view/View;

.field private C:Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;

.field private D:Z

.field private E:I

.field private F:Ljava/lang/String;

.field public final a:Lflipboard/service/FlipboardManager;

.field public c:Lflipboard/gui/FLWebView;

.field protected d:Lflipboard/objs/FeedItem;

.field e:F

.field f:F

.field protected g:Z

.field protected h:Z

.field protected i:F

.field protected j:F

.field protected k:F

.field protected l:F

.field protected m:F

.field protected n:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public o:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/String;

.field private q:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private s:Z

.field private t:Landroid/content/SharedPreferences;

.field private u:Lflipboard/io/UsageEvent;

.field private v:Lflipboard/gui/FLBusyView;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:I

.field private final z:Lflipboard/service/audio/FLAudioManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-string v0, "flipmag"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;-><init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;B)V

    .line 139
    return-void
.end method

.method private constructor <init>(Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;B)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 143
    invoke-direct {p0, p1}, Lflipboard/gui/FLViewFlipper;-><init>(Landroid/content/Context;)V

    .line 87
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->a:Lflipboard/service/FlipboardManager;

    .line 101
    iput-boolean v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->s:Z

    .line 112
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    iput-boolean v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->g:Z

    .line 114
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    .line 115
    iput v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->j:F

    .line 116
    iput v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->k:F

    .line 117
    iput v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->l:F

    .line 118
    iput v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->m:F

    .line 123
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->a:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->z()Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    .line 127
    iput-boolean v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->D:Z

    .line 129
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    .line 145
    iput-object p2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    .line 148
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->n:Ljava/util/Map;

    .line 151
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_8

    .line 152
    const v0, 0x7f03005f

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 153
    const v0, 0x7f0a016b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/SocialBarTablet;

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    .line 154
    const v0, 0x7f0a0169

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->x:Landroid/view/View;

    .line 162
    :goto_0
    const v0, 0x7f0a0090

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->v:Lflipboard/gui/FLBusyView;

    .line 163
    const v0, 0x7f0a016a

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->w:Landroid/view/View;

    .line 164
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 165
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->a:Lflipboard/service/FlipboardManager;

    const-string v0, "FlipmagDetailViewTablet:showLoadingIndicator"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->w:Landroid/view/View;

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 167
    :cond_1
    iput-boolean v6, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->h:Z

    .line 169
    iget-object v2, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    .line 173
    iget-object v0, p2, Lflipboard/objs/FeedItem;->W:Ljava/lang/String;

    .line 177
    iget-boolean v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->g:Z

    if-nez v1, :cond_2

    const-string v1, "&formFactor=phone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 178
    const-string v1, "&formFactor=phone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    .line 182
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    iput-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->t:Landroid/content/SharedPreferences;

    .line 184
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->t:Landroid/content/SharedPreferences;

    const-string v3, "use_flipmag_proxy"

    invoke-interface {v1, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 185
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->t:Landroid/content/SharedPreferences;

    const-string v3, "flipmag_proxy_server"

    const-string v4, "http://cdn.flipboard.com/flipmag-beta/flipmag"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "cdn.flipboard.com"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_a

    const-string v1, "http://cdn.flipboard.com/flipmag"

    const-string v3, "http://cdn.flipboard.com/flipmag-beta/flipmag"

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 187
    :goto_2
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->t:Landroid/content/SharedPreferences;

    const-string v3, "flipmag_to_flipmag_links"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 188
    iput-boolean v6, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->s:Z

    .line 191
    :cond_3
    const-string v0, "status"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    if-eqz v1, :cond_7

    .line 192
    const v0, 0x7f0a0168

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLWebView;

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$1;

    invoke-direct {v2, p0}, Lflipboard/gui/item/FlipmagDetailViewTablet$1;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLWebView;->setHorizontalScrollBarEnabled(Z)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLWebView;->setVerticalScrollBarEnabled(Z)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, v5, v5, v5, v5}, Lflipboard/gui/FLWebView;->setPadding(IIII)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    const/high16 v2, 0x2000000

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setScrollBarStyle(I)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLWebView;->setHapticFeedbackEnabled(Z)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$2;

    invoke-direct {v2, p0}, Lflipboard/gui/item/FlipmagDetailViewTablet$2;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    sget-object v2, Landroid/webkit/WebSettings$PluginState;->OFF:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    invoke-virtual {v0, v5}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    :cond_4
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setCacheMode(I)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    new-instance v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;

    invoke-direct {v0, p0}, Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->C:Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->C:Lflipboard/gui/item/FlipmagDetailViewTablet$FLBridge;

    const-string v3, "FLBridgeAndroid"

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$ConsoleJavaScriptInterface;

    invoke-direct {v2, p0}, Lflipboard/gui/item/FlipmagDetailViewTablet$ConsoleJavaScriptInterface;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    const-string v3, "console"

    invoke-virtual {v0, v2, v3}, Lflipboard/gui/FLWebView;->addJavascriptInterface(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$3;

    invoke-direct {v2, p0}, Lflipboard/gui/item/FlipmagDetailViewTablet$3;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 195
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_6

    const-string v2, "nytimes"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string v2, "ft"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 197
    :cond_5
    const-string v0, "generic.html"

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_6

    .line 198
    const-string v2, "generic.html"

    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v0

    const-string v3, "nytimes"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string v0, "nyt.html"

    :goto_3
    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 202
    :cond_6
    iput-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->F:Ljava/lang/String;

    .line 204
    :cond_7
    return-void

    .line 156
    :cond_8
    const v0, 0x7f03005e

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 157
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 158
    sget-object v1, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->a:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 159
    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    goto/16 :goto_0

    .line 165
    :cond_9
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->v:Lflipboard/gui/FLBusyView;

    goto/16 :goto_1

    .line 185
    :cond_a
    const-string v3, "/flipmag?url="

    invoke-virtual {v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-ltz v3, :cond_b

    add-int/lit8 v3, v3, 0xd

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/?url=tools/articleproxy/proxy.php%3Furl%3D"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_2

    .line 198
    :cond_c
    const-string v0, "financialtimes.html"

    goto :goto_3

    :cond_d
    move-object v1, v0

    goto/16 :goto_2
.end method

.method private a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 688
    move v1, v0

    .line 689
    :goto_0
    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 690
    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 691
    if-ge v2, p1, :cond_0

    .line 692
    add-int/lit8 v1, v1, 0x1

    .line 689
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 695
    :cond_1
    return v1
.end method

.method static synthetic a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/item/FlipmagDetailViewTablet;Lflipboard/util/Observer;)Lflipboard/util/Observer;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->A:Lflipboard/util/Observer;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/item/FlipmagDetailViewTablet;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->p:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lflipboard/gui/item/FlipmagDetailViewTablet;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->q:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/item/FlipmagDetailViewTablet;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method public static c()V
    .locals 0

    .prologue
    .line 766
    return-void
.end method

.method static synthetic c(Lflipboard/gui/item/FlipmagDetailViewTablet;)Z
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->s:Z

    return v0
.end method

.method static synthetic d(Lflipboard/gui/item/FlipmagDetailViewTablet;)V
    .locals 6

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->q:Ljava/util/Map;

    if-nez v2, :cond_2

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->q:Ljava/util/Map;

    :cond_2
    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$5;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet$5;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;Ljava/lang/String;)V

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lflipboard/service/Flap$SourceMagazineURLRequest;

    invoke-direct {v5, v3, v4}, Lflipboard/service/Flap$SourceMagazineURLRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v5, v0, v2}, Lflipboard/service/Flap$SourceMagazineURLRequest;->a(Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)Lflipboard/service/Flap$SourceMagazineURLRequest;

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 587
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->w:Landroid/view/View;

    .line 588
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 589
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->a:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/item/FlipmagDetailViewTablet$6;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet$6;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 608
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->h:Z

    .line 609
    return-void

    .line 587
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->v:Lflipboard/gui/FLBusyView;

    goto :goto_0
.end method

.method static synthetic e(Lflipboard/gui/item/FlipmagDetailViewTablet;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->e()V

    return-void
.end method

.method static synthetic f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    return-object v0
.end method

.method static synthetic g(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->A:Lflipboard/util/Observer;

    return-object v0
.end method

.method static synthetic h(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/io/UsageEvent;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 706
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 707
    if-eqz v0, :cond_1

    .line 708
    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 747
    :cond_0
    :goto_0
    return-void

    .line 710
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 712
    invoke-direct {p0, p2}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(I)I

    move-result v0

    sub-int v1, p2, v0

    .line 713
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_4

    .line 714
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->k:F

    neg-float v0, v0

    int-to-float v2, v1

    mul-float/2addr v0, v2

    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 722
    :goto_1
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLWebView;->draw(Landroid/graphics/Canvas;)V

    .line 724
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 726
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 727
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->n:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->n:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "hidden"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 728
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 729
    const/4 v0, 0x0

    .line 730
    if-lez v1, :cond_5

    .line 731
    const/4 v0, 0x1

    move v1, v0

    .line 734
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 735
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 736
    int-to-float v0, v0

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 737
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    check-cast v0, Lflipboard/gui/SocialBarTablet;

    iget-boolean v2, v0, Lflipboard/gui/SocialBarTablet;->g:Z

    iput-boolean v1, v0, Lflipboard/gui/SocialBarTablet;->g:Z

    invoke-virtual {v0, p1}, Lflipboard/gui/SocialBarTablet;->draw(Landroid/graphics/Canvas;)V

    iput-boolean v2, v0, Lflipboard/gui/SocialBarTablet;->g:Z

    .line 738
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 741
    :cond_3
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->x:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 742
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->x:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 716
    :cond_4
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 717
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->j:F

    add-float/2addr v3, v4

    invoke-virtual {p1, v5, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 718
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->j:F

    neg-float v0, v0

    int-to-float v2, v1

    mul-float/2addr v0, v2

    .line 719
    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    goto/16 :goto_1

    :cond_5
    move v1, v0

    goto :goto_2
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1327
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v0, :cond_0

    .line 1328
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    iget-boolean v0, v0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    .line 1330
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getCurrentViewIndex()I
    .locals 1

    .prologue
    .line 1283
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->y:I

    return v0
.end method

.method public getDimensionModifier()F
    .locals 1

    .prologue
    .line 613
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    return v0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 1278
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method public getNextViewIndex()I
    .locals 1

    .prologue
    .line 773
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->E:I

    return v0
.end method

.method public getNumberOfPages()I
    .locals 2

    .prologue
    .line 755
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 756
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->m:F

    iget v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->k:F

    div-float/2addr v0, v1

    .line 760
    :goto_0
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 758
    :cond_0
    iget v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->l:F

    iget v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->j:F

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 633
    invoke-super {p0}, Lflipboard/gui/FLViewFlipper;->onDetachedFromWindow()V

    .line 634
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->A:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->A:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->c(Lflipboard/util/Observer;)V

    .line 638
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->onResume()V

    .line 639
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 563
    invoke-super/range {p0 .. p5}, Lflipboard/gui/FLViewFlipper;->onLayout(ZIIII)V

    .line 564
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getChildCount()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 565
    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    sub-int v4, p4, p2

    sub-int v5, p5, p3

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 567
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 568
    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->F:Ljava/lang/String;

    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->e()V

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 569
    :goto_1
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->F:Ljava/lang/String;

    .line 571
    :cond_1
    return-void

    .line 568
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->e:F

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->ceil(F)F

    move-result v0

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->f:F

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->j:F

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->k:F

    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v3}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x2

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v3}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x3

    iget v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->e:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    iget v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->f:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->j:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v0, v1

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/DetailActivity;

    iget-object v0, v0, Lflipboard/activities/DetailActivity;->q:Lflipboard/io/UsageEvent;

    iput-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    :cond_3
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->c(Landroid/app/Activity;)Landroid/graphics/Point;

    move-result-object v0

    iget v3, v0, Landroid/graphics/Point;->x:I

    iget v4, v0, Landroid/graphics/Point;->y:I

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v5, "screensize"

    const-string v6, "%dx%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v5, "screensize-aspectratio"

    const-string v6, "%.2f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    int-to-float v9, v4

    int-to-float v10, v3

    div-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v5, "websize"

    const-string v6, "%dx%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v9}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v9}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v5, "websize-aspectratio"

    const-string v6, "%.2f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v9}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v10}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v5, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v6, "orientation"

    if-eqz v1, :cond_11

    const-string v0, "portrait"

    :goto_3
    invoke-virtual {v5, v6, v0}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v5, "viewport"

    const-string v6, "%dx%d"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->e:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    iget v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->f:F

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->u:Lflipboard/io/UsageEvent;

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v5, "viewport-aspectratio"

    const-string v6, "%.2f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->f:F

    iget v10, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->e:F

    div-float/2addr v9, v10

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->b(Landroid/app/Activity;)I

    move-result v5

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getHeight()I

    move-result v0

    sub-int v0, v4, v0

    sub-int v7, v0, v5

    const/4 v0, 0x1

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x10

    if-lt v8, v9, :cond_4

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v8, v8, 0xf

    const/4 v9, 0x3

    if-ne v8, v9, :cond_4

    if-eqz v1, :cond_12

    const/4 v0, 0x0

    :cond_4
    :goto_4
    sget-object v8, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v9

    const/4 v3, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v3

    sget-object v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    sget-object v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getWidth()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getHeight()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    sget-object v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v8, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v8}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    const/4 v4, 0x1

    iget-object v8, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v8}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    sget-object v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    sget-object v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v4

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v3}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v4}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v8, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v8}, Lflipboard/gui/FLWebView;->getHeight()I

    move-result v8

    add-int/2addr v8, v7

    add-int/2addr v8, v5

    mul-int v9, v6, v0

    add-int/2addr v8, v9

    int-to-float v8, v8

    iget-object v9, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    invoke-virtual {v9}, Lflipboard/gui/FLWebView;->getWidth()I

    move-result v9

    sub-int v7, v9, v7

    sub-int v5, v7, v5

    mul-int/2addr v0, v6

    sub-int v0, v5, v0

    int-to-float v0, v0

    sget-object v5, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v5, v7

    const/4 v7, 0x1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v5, v7

    sget-object v5, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v5, v7

    const/4 v7, 0x1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v5, v7

    iget v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float/2addr v3, v5

    iget v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float/2addr v4, v5

    iget v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float v5, v8, v5

    iget v7, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float/2addr v0, v7

    const-string v7, "{ width: %s, height: %s }"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v8, v9

    const/4 v3, 0x1

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v8, v3

    invoke-static {v7, v8}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "{ width: %s, height: %s }"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v7, v8

    const/4 v5, 0x1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v7, v5

    invoke-static {v4, v7}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v0, "window.flipboardPageSizes = { portrait: %s, landscape: %s };"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v5, v7

    const/4 v7, 0x1

    aput-object v4, v5, v7

    invoke-static {v0, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->e:F

    iget v7, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->f:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_5

    const-string v0, "window.flipboardPageSizes = { landscape: %s, portrait: %s };"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v3, v5, v7

    const/4 v3, 0x1

    aput-object v4, v5, v3

    invoke-static {v0, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_5
    sget-object v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardOrientation = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_13

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardMagazine = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardCloseAction = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardSignalTapToClose = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardNeedsPageReady = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardBridgeEnabled = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardShowImageEnabled = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardNativeFlip = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardUsageEnabled = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardUseClickEventsInsteadOfTaps = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardNativeAdManagerEnabled = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "window.flipboardSocialBarHeight = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-float v0, v6

    iget v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    div-float/2addr v0, v1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    invoke-static {v0, v1}, Lflipboard/util/MeteringHelper;->a(Landroid/content/Context;Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->t:Landroid/content/SharedPreferences;

    const-string v1, "show_hitrects_for_ads"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "window.flipboardShowClickRect = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aF:Lflipboard/objs/FeedArticle;

    iget-object v0, v0, Lflipboard/objs/FeedArticle;->d:Ljava/lang/String;

    const-string v1, "ft"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const-string v1, "ft"

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->h:Ljava/lang/String;

    const-string v1, "window.flipboardFtEid = "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_14

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "\'"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v0, "window.articleViewIsVisible = "

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->d()Z

    move-result v0

    if-eqz v0, :cond_15

    iget-boolean v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->D:Z

    if-nez v0, :cond_15

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->D:Z

    const-string v0, "true"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_7
    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v0, :cond_16

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_8
    const-string v1, "window.flipboardServiceSubscriptionStatus = "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v0, :cond_17

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_17

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "\'"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_25

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v0, :cond_25

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v4, v0, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v5, v4}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v4

    iget-boolean v5, v4, Lflipboard/objs/ConfigService;->bO:Z

    if-eqz v5, :cond_a

    iget-boolean v5, v4, Lflipboard/objs/ConfigService;->bx:Z

    if-eqz v5, :cond_a

    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v5}, Lflipboard/app/FlipboardApplication;->m()Z

    move-result v5

    iget-object v6, v4, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    invoke-virtual {v1, v6}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v6

    sget-boolean v1, Lflipboard/service/FlipboardManager;->t:Z

    if-eqz v1, :cond_18

    if-eqz v6, :cond_9

    iget-object v1, v6, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-boolean v1, v1, Lflipboard/objs/UserService;->r:Z

    if-eqz v1, :cond_18

    :cond_9
    const/4 v1, 0x1

    :goto_a
    if-eqz v6, :cond_20

    invoke-virtual {v6}, Lflipboard/service/Account;->g()Z

    move-result v7

    if-eqz v7, :cond_1c

    invoke-virtual {v6}, Lflipboard/service/Account;->h()Z

    move-result v6

    if-nez v6, :cond_a

    if-eqz v5, :cond_1a

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v1, :cond_19

    iget-object v0, v4, Lflipboard/objs/ConfigService;->bp:Ljava/lang/String;

    :goto_b
    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_a
    :goto_c
    if-eqz v0, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_b

    const-string v1, "window.flipboardEndOfArticleHTML = "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v0, "window.flipboardAudio = true;"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_c

    const-string v0, "window.flipboardArticleMarkup=\""

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->i()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    if-eqz v0, :cond_f

    const-string v0, "window.flipboardAudioUrl=\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->h()Lflipboard/service/audio/Song;

    move-result-object v0

    iget-object v0, v0, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\';"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x1

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v1

    if-eqz v1, :cond_26

    const/4 v0, 0x3

    :cond_d
    :goto_d
    const-string v1, "window.flipboardAudioState="

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v0, 0x0

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->d()Z

    move-result v1

    if-eqz v1, :cond_e

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->a()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    :cond_e
    const-string v1, "window.flipboardAudioTime="

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/FlipmagDetailViewTablet$4;

    invoke-direct {v1, p0, v3, v2}, Lflipboard/gui/item/FlipmagDetailViewTablet$4;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet;Ljava/lang/StringBuilder;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_10
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_2

    :cond_11
    const-string v0, "landscape"

    goto/16 :goto_3

    :cond_12
    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_13
    const/16 v0, 0x5a

    goto/16 :goto_5

    :cond_14
    const-string v0, "null"

    goto/16 :goto_6

    :cond_15
    const-string v0, "false"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_7

    :cond_16
    const/4 v0, 0x0

    goto/16 :goto_8

    :cond_17
    const-string v0, "null"

    goto/16 :goto_9

    :cond_18
    const/4 v1, 0x0

    goto/16 :goto_a

    :cond_19
    iget-object v0, v4, Lflipboard/objs/ConfigService;->bw:Ljava/lang/String;

    goto/16 :goto_b

    :cond_1a
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v1, :cond_1b

    iget-object v0, v4, Lflipboard/objs/ConfigService;->bo:Ljava/lang/String;

    :goto_e
    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_1b
    iget-object v0, v4, Lflipboard/objs/ConfigService;->bv:Ljava/lang/String;

    goto :goto_e

    :cond_1c
    if-eqz v5, :cond_1e

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v1, :cond_1d

    iget-object v0, v4, Lflipboard/objs/ConfigService;->bn:Ljava/lang/String;

    :goto_f
    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_1d
    iget-object v0, v4, Lflipboard/objs/ConfigService;->bu:Ljava/lang/String;

    goto :goto_f

    :cond_1e
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v1, :cond_1f

    iget-object v0, v4, Lflipboard/objs/ConfigService;->bm:Ljava/lang/String;

    :goto_10
    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_1f
    iget-object v0, v4, Lflipboard/objs/ConfigService;->bt:Ljava/lang/String;

    goto :goto_10

    :cond_20
    sget-boolean v0, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v0, :cond_21

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    const-string v1, "<div class=\'fl-signin-callout ipad case1\'><div class=\'message1\'><strong>Sign in with your NYTimes.com subscription account</strong> for full access to <span class=\'no-break\'>The New York Times</span>.</div><a class=\'fl-signin-button fl-button\' href=\'flipboard://showSignIn?service=nytimes&from=flipmagEndOfArticleHTML\'>Sign In to The New York Times</a><div class=\'message2\'>Not a subscriber? <a href=\'flipboard://openUrl?url=%s\'>Download the NYTimes for Kindle Fire</a> to subscribe via Amazon Appstore.</div></div>"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v0, Lflipboard/model/ConfigSetting;->NYTSubscribeLinkKindle:Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-static {v1, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_21
    if-eqz v5, :cond_23

    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v1, :cond_22

    iget-object v0, v4, Lflipboard/objs/ConfigService;->bl:Ljava/lang/String;

    :goto_11
    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_12
    if-nez v0, :cond_a

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v4, Lflipboard/objs/ConfigService;->bq:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_c

    :cond_22
    iget-object v0, v4, Lflipboard/objs/ConfigService;->bs:Ljava/lang/String;

    goto :goto_11

    :cond_23
    sget-object v5, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    if-eqz v1, :cond_24

    iget-object v0, v4, Lflipboard/objs/ConfigService;->bk:Ljava/lang/String;

    :goto_13
    invoke-virtual {v5, v0}, Lflipboard/service/FlipboardManager;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_12

    :cond_24
    iget-object v0, v4, Lflipboard/objs/ConfigService;->br:Ljava/lang/String;

    goto :goto_13

    :cond_25
    const/4 v0, 0x0

    goto/16 :goto_c

    :cond_26
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->z:Lflipboard/service/audio/FLAudioManager;

    invoke-virtual {v1}, Lflipboard/service/audio/FLAudioManager;->e()Z

    move-result v1

    if-eqz v1, :cond_d

    const/4 v0, 0x2

    goto/16 :goto_d
.end method

.method public setContentHeight(I)V
    .locals 1

    .prologue
    .line 208
    int-to-float v0, p1

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->l:F

    .line 209
    return-void
.end method

.method public setContentWidth(I)V
    .locals 1

    .prologue
    .line 213
    int-to-float v0, p1

    iput v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->m:F

    .line 214
    return-void
.end method

.method public setCurrentViewIndex(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1293
    iput p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->y:I

    .line 1294
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->o:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1295
    invoke-direct {p0, p1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(I)I

    move-result v1

    .line 1296
    sub-int v2, p1, v1

    .line 1297
    if-eqz v0, :cond_1

    .line 1298
    add-int/lit8 v0, v1, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->setDisplayedChild(I)V

    .line 1323
    :cond_0
    :goto_0
    return-void

    .line 1300
    :cond_1
    invoke-virtual {p0, v4}, Lflipboard/gui/item/FlipmagDetailViewTablet;->setDisplayedChild(I)V

    .line 1301
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "javascript:FLDidFlipToPageAtIndex("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 1302
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_2

    .line 1303
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    iget v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->k:F

    int-to-float v3, v2

    mul-float/2addr v1, v3

    float-to-int v1, v1

    invoke-virtual {v0, v1, v4}, Lflipboard/gui/FLWebView;->scrollTo(II)V

    .line 1307
    :cond_2
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    .line 1308
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->B:Landroid/view/View;

    check-cast v0, Lflipboard/gui/SocialBarTablet;

    .line 1309
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->n:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->n:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "hidden"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1310
    invoke-static {v0, v5}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 1311
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->x:Landroid/view/View;

    invoke-static {v0, v5}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto :goto_0

    .line 1313
    :cond_3
    invoke-static {v0, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 1314
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->x:Landroid/view/View;

    invoke-static {v1, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 1315
    if-nez v2, :cond_4

    .line 1316
    invoke-virtual {v0, v4}, Lflipboard/gui/SocialBarTablet;->a(Z)V

    goto :goto_0

    .line 1318
    :cond_4
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/SocialBarTablet;->a(Z)V

    goto :goto_0
.end method

.method public setNextViewIndex(I)V
    .locals 0

    .prologue
    .line 769
    iput p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet;->E:I

    .line 770
    return-void
.end method
