.class Lflipboard/gui/item/CoverSectionItemTablet$1;
.super Ljava/lang/Object;
.source "CoverSectionItemTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/item/CoverSectionItemTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/CoverSectionItemTablet;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 118
    const/4 v1, 0x0

    .line 119
    iget-object v2, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v2}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v2}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v2

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 120
    iget-object v1, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v1}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v1

    move-object v2, v1

    .line 123
    :goto_0
    if-eqz v2, :cond_1

    .line 128
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1}, Lflipboard/service/User;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 129
    iget-boolean v1, v2, Lflipboard/objs/FeedItem;->af:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    move v1, v0

    .line 133
    :goto_1
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v3, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    iget-object v3, v3, Lflipboard/gui/item/CoverSectionItemTablet;->a:Lflipboard/service/Section;

    invoke-static {v2, v0, v3}, Lflipboard/util/SocialHelper;->a(Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    .line 136
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$1;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-static {v0, v1}, Lflipboard/gui/item/CoverSectionItemTablet;->a(Lflipboard/gui/item/CoverSectionItemTablet;Z)V

    .line 139
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->h:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v3, Lflipboard/objs/UsageEventV2$EventCategory;->e:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v3}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 140
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->e:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 141
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v3, v2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v3, v3, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 142
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v2, v2, Lflipboard/objs/FeedSection;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 143
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 147
    :goto_2
    return-void

    .line 145
    :cond_1
    const-string v0, "unwanted.magazine_like_tapped_with_no_item"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move v1, v0

    goto :goto_1

    :cond_3
    move-object v2, v1

    goto :goto_0
.end method
