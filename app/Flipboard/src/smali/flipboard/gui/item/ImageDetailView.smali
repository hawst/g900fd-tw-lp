.class public Lflipboard/gui/item/ImageDetailView;
.super Lflipboard/gui/item/InflateableDetailView;
.source "ImageDetailView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field b:Landroid/graphics/Matrix;

.field c:Landroid/graphics/Matrix;

.field d:I

.field e:J

.field f:Ljava/util/TimerTask;

.field g:Landroid/graphics/PointF;

.field h:J

.field i:Landroid/graphics/PointF;

.field j:Landroid/graphics/PointF;

.field k:F

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;

.field n:Lflipboard/gui/actionbar/FLActionBar;

.field private final q:F

.field private final r:F

.field private s:Lflipboard/gui/FLImageView;

.field private t:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "imagedetail"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/item/ImageDetailView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lflipboard/gui/item/InflateableDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 38
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->q:F

    .line 39
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->r:F

    .line 44
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 61
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    .line 63
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->i:Landroid/graphics/PointF;

    .line 64
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    .line 65
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->k:F

    .line 74
    invoke-virtual {p2}, Lflipboard/objs/FeedItem;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->l:Ljava/lang/String;

    .line 75
    if-eqz p2, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->m:Ljava/lang/String;

    .line 78
    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->c()V

    .line 79
    return-void

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, v1}, Lflipboard/gui/item/InflateableDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 38
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->q:F

    .line 39
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->r:F

    .line 44
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 61
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    .line 63
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->i:Landroid/graphics/PointF;

    .line 64
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    .line 65
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->k:F

    .line 84
    iput-object p2, p0, Lflipboard/gui/item/ImageDetailView;->l:Ljava/lang/String;

    .line 85
    iput-object v1, p0, Lflipboard/gui/item/ImageDetailView;->m:Ljava/lang/String;

    .line 88
    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->c()V

    .line 89
    return-void
.end method

.method private a(FF)F
    .locals 4

    .prologue
    .line 320
    invoke-direct {p0}, Lflipboard/gui/item/ImageDetailView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 321
    neg-float v1, p2

    iget v2, v0, Landroid/graphics/RectF;->left:F

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    add-float/2addr v0, v3

    mul-float/2addr v0, p2

    sub-float v0, v2, v0

    invoke-static {p1, v1, v0}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    return v0
.end method

.method private static a(Landroid/view/MotionEvent;)F
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 360
    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    .line 361
    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    .line 362
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method static synthetic a(Lflipboard/gui/item/ImageDetailView;)Lflipboard/gui/FLImageView;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    return-object v0
.end method

.method private a(FFFFFF)V
    .locals 10

    .prologue
    .line 332
    sget-object v0, Lflipboard/gui/item/ImageDetailView;->a:Lflipboard/util/Log;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static/range {p6 .. p6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    .line 333
    cmpl-float v0, p4, p1

    if-nez v0, :cond_0

    cmpl-float v0, p5, p2

    if-nez v0, :cond_0

    cmpl-float v0, p6, p3

    if-eqz v0, :cond_1

    .line 334
    :cond_0
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 335
    sget-object v9, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v0, Lflipboard/gui/item/ImageDetailView$3;

    move-object v1, p0

    move v3, p4

    move v4, p1

    move v5, p5

    move v6, p2

    move/from16 v7, p6

    move v8, p3

    invoke-direct/range {v0 .. v8}, Lflipboard/gui/item/ImageDetailView$3;-><init>(Lflipboard/gui/item/ImageDetailView;Landroid/view/animation/Interpolator;FFFFFF)V

    invoke-virtual {v9, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 355
    :cond_1
    return-void
.end method

.method private static a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 368
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    add-float/2addr v0, v1

    .line 369
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    add-float/2addr v1, v2

    .line 370
    div-float/2addr v0, v3

    div-float/2addr v1, v3

    invoke-virtual {p0, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 371
    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/high16 v0, 0x40a00000    # 5.0f

    .line 283
    const/16 v1, 0x9

    new-array v3, v1, [F

    .line 284
    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v3}, Landroid/graphics/Matrix;->getValues([F)V

    .line 285
    const/4 v1, 0x0

    aget v1, v3, v1

    .line 286
    const/4 v2, 0x2

    aget v2, v3, v2

    .line 287
    const/4 v5, 0x5

    aget v3, v3, v5

    .line 291
    cmpg-float v5, v1, v4

    if-gtz v5, :cond_0

    move v5, v6

    .line 308
    :goto_0
    if-eqz p1, :cond_2

    move-object v0, p0

    .line 309
    invoke-direct/range {v0 .. v6}, Lflipboard/gui/item/ImageDetailView;->a(FFFFFF)V

    .line 316
    :goto_1
    return-void

    .line 296
    :cond_0
    cmpl-float v4, v1, v0

    if-lez v4, :cond_1

    .line 298
    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v4, v2

    .line 299
    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    div-float/2addr v4, v1

    mul-float/2addr v4, v0

    sub-float v4, v5, v4

    invoke-direct {p0, v4, v0}, Lflipboard/gui/item/ImageDetailView;->a(FF)F

    move-result v5

    .line 300
    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sub-float/2addr v4, v3

    .line 301
    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    div-float/2addr v4, v1

    mul-float/2addr v4, v0

    sub-float v4, v6, v4

    invoke-direct {p0, v4, v0}, Lflipboard/gui/item/ImageDetailView;->b(FF)F

    move-result v6

    move v4, v0

    .line 302
    goto :goto_0

    .line 304
    :cond_1
    invoke-direct {p0, v2, v1}, Lflipboard/gui/item/ImageDetailView;->a(FF)F

    move-result v5

    .line 305
    invoke-direct {p0, v3, v1}, Lflipboard/gui/item/ImageDetailView;->b(FF)F

    move-result v6

    move v4, v1

    goto :goto_0

    .line 311
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 312
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 313
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 314
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_1
.end method

.method private b(FF)F
    .locals 4

    .prologue
    .line 326
    invoke-direct {p0}, Lflipboard/gui/item/ImageDetailView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    .line 327
    neg-float v1, p2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    mul-float/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    add-float/2addr v0, v3

    mul-float/2addr v0, p2

    sub-float v0, v2, v0

    invoke-static {p1, v1, v0}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v0

    return v0
.end method

.method private b(Landroid/view/MotionEvent;)F
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 378
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    .line 379
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v2, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    .line 380
    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    invoke-static {v0}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v0

    return v0
.end method

.method private c(Landroid/view/MotionEvent;)J
    .locals 4

    .prologue
    .line 388
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/gui/item/ImageDetailView;->h:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method private getImageRect()Landroid/graphics/RectF;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 269
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->t:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->t:Landroid/graphics/RectF;

    .line 271
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->t:Landroid/graphics/RectF;

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->t:Landroid/graphics/RectF;

    .line 276
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->t:Landroid/graphics/RectF;

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 98
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ImageDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->n:Lflipboard/gui/actionbar/FLActionBar;

    .line 99
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->n:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->setInverted(Z)V

    .line 100
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->n:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 101
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->n:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->g()V

    .line 103
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ImageDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    .line 104
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    new-instance v1, Lflipboard/gui/item/ImageDetailView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/ImageDetailView$1;-><init>(Lflipboard/gui/item/ImageDetailView;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    .line 119
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setFade(Z)V

    .line 120
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setScaling(Z)V

    .line 121
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    sget-object v1, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 126
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/ImageDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 127
    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->m:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->m:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 128
    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 394
    const/4 v0, 0x0

    return v0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 93
    const v0, 0x7f030060

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/high16 v4, 0x40800000    # 4.0f

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 143
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-le v0, v7, :cond_0

    move v7, v1

    .line 264
    :goto_0
    return v7

    .line 148
    :cond_0
    iget v3, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 151
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v5, v0, 0xff

    .line 152
    packed-switch v5, :pswitch_data_0

    .line 259
    :cond_1
    :goto_1
    :pswitch_0
    if-eq v5, v2, :cond_2

    .line 260
    sget-object v0, Lflipboard/gui/item/ImageDetailView;->a:Lflipboard/util/Log;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    iget v1, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    const/4 v1, 0x4

    iget-object v2, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapRadius(F)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    .line 263
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0

    .line 155
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 156
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v0, v4, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 157
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    iput-wide v8, p0, Lflipboard/gui/item/ImageDetailView;->h:J

    .line 159
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-eq v0, v12, :cond_3

    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-eq v0, v2, :cond_3

    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    const/4 v4, 0x4

    if-ne v0, v4, :cond_4

    :cond_3
    move v0, v2

    :goto_2
    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    goto :goto_1

    :cond_4
    move v0, v7

    goto :goto_2

    .line 163
    :pswitch_2
    invoke-static {p2}, Lflipboard/gui/item/ImageDetailView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->k:F

    .line 164
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->k:F

    const/high16 v4, 0x41200000    # 10.0f

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    .line 165
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 166
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->i:Landroid/graphics/PointF;

    invoke-static {v0, p2}, Lflipboard/gui/item/ImageDetailView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 167
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v0, v4, v6}, Landroid/graphics/PointF;->set(FF)V

    .line 168
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    iput-wide v8, p0, Lflipboard/gui/item/ImageDetailView;->h:J

    .line 169
    iput v12, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    goto/16 :goto_1

    .line 171
    :cond_5
    iput v2, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    goto/16 :goto_1

    .line 176
    :pswitch_3
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-eq v0, v7, :cond_6

    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-ne v0, v2, :cond_9

    invoke-direct {p0, p2}, Lflipboard/gui/item/ImageDetailView;->c(Landroid/view/MotionEvent;)J

    move-result-wide v8

    const-wide/16 v10, 0xc8

    cmp-long v0, v8, v10

    if-gez v0, :cond_9

    invoke-direct {p0, p2}, Lflipboard/gui/item/ImageDetailView;->b(Landroid/view/MotionEvent;)F

    move-result v0

    const/high16 v6, 0x42480000    # 50.0f

    cmpg-float v0, v0, v6

    if-gez v0, :cond_9

    .line 181
    :cond_6
    iput v1, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 182
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-wide v10, p0, Lflipboard/gui/item/ImageDetailView;->e:J

    sub-long/2addr v8, v10

    const-wide/16 v10, 0xfa

    cmp-long v0, v8, v10

    if-gez v0, :cond_8

    .line 183
    const/16 v0, 0x9

    new-array v0, v0, [F

    iget-object v3, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->getValues([F)V

    aget v1, v0, v1

    aget v2, v0, v2

    const/4 v3, 0x5

    aget v3, v0, v3

    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, v1, v0

    if-nez v0, :cond_7

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    div-float/2addr v0, v1

    mul-float/2addr v0, v4

    sub-float v0, v5, v0

    invoke-direct {p0, v0, v4}, Lflipboard/gui/item/ImageDetailView;->a(FF)F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/ImageDetailView;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v6, v6

    div-float/2addr v0, v1

    mul-float/2addr v0, v4

    sub-float v0, v6, v0

    invoke-direct {p0, v0, v4}, Lflipboard/gui/item/ImageDetailView;->b(FF)F

    move-result v6

    :goto_3
    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lflipboard/gui/item/ImageDetailView;->a(FFFFFF)V

    .line 184
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->f:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    goto/16 :goto_0

    .line 183
    :cond_7
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    goto :goto_3

    .line 186
    :cond_8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/item/ImageDetailView;->e:J

    .line 187
    new-instance v0, Lflipboard/gui/item/ImageDetailView$2;

    invoke-direct {v0, p0}, Lflipboard/gui/item/ImageDetailView$2;-><init>(Lflipboard/gui/item/ImageDetailView;)V

    iput-object v0, p0, Lflipboard/gui/item/ImageDetailView;->f:Ljava/util/TimerTask;

    .line 204
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView;->f:Ljava/util/TimerTask;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 207
    :cond_9
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-eq v0, v2, :cond_a

    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    const/4 v4, 0x4

    if-eq v0, v4, :cond_a

    .line 210
    iput v1, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 213
    :cond_a
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->s:Lflipboard/gui/FLImageView;

    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 214
    invoke-direct {p0, v7}, Lflipboard/gui/item/ImageDetailView;->a(Z)V

    goto/16 :goto_1

    .line 219
    :pswitch_4
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-ne v0, v12, :cond_1

    .line 222
    const/4 v0, 0x4

    iput v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 226
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 227
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-nez v0, :cond_b

    move v0, v7

    .line 228
    :goto_4
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-virtual {v4, v6, v0}, Landroid/graphics/PointF;->set(FF)V

    .line 229
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v8

    iput-wide v8, p0, Lflipboard/gui/item/ImageDetailView;->h:J

    goto/16 :goto_1

    :cond_b
    move v0, v1

    .line 227
    goto :goto_4

    .line 234
    :pswitch_5
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-ne v0, v7, :cond_d

    invoke-direct {p0, p2}, Lflipboard/gui/item/ImageDetailView;->c(Landroid/view/MotionEvent;)J

    move-result-wide v8

    const-wide/16 v10, 0xc8

    cmp-long v0, v8, v10

    if-gez v0, :cond_c

    invoke-direct {p0, p2}, Lflipboard/gui/item/ImageDetailView;->b(Landroid/view/MotionEvent;)F

    move-result v0

    const/high16 v4, 0x42480000    # 50.0f

    cmpl-float v0, v0, v4

    if-ltz v0, :cond_d

    .line 237
    :cond_c
    iput v2, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    .line 239
    :cond_d
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-eq v0, v2, :cond_e

    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    const/4 v4, 0x4

    if-ne v0, v4, :cond_10

    .line 241
    :cond_e
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 242
    iget-object v0, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iget-object v6, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v6

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    iget-object v8, p0, Lflipboard/gui/item/ImageDetailView;->g:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v8

    invoke-virtual {v0, v4, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 254
    :cond_f
    :goto_5
    invoke-direct {p0, v1}, Lflipboard/gui/item/ImageDetailView;->a(Z)V

    goto/16 :goto_1

    .line 244
    :cond_10
    iget v0, p0, Lflipboard/gui/item/ImageDetailView;->d:I

    if-ne v0, v12, :cond_1

    .line 246
    invoke-static {p2}, Lflipboard/gui/item/ImageDetailView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    .line 247
    const/high16 v4, 0x41200000    # 10.0f

    cmpl-float v4, v0, v4

    if-lez v4, :cond_f

    .line 248
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    iget-object v6, p0, Lflipboard/gui/item/ImageDetailView;->c:Landroid/graphics/Matrix;

    invoke-virtual {v4, v6}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 249
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    invoke-static {v4, p2}, Lflipboard/gui/item/ImageDetailView;->a(Landroid/graphics/PointF;Landroid/view/MotionEvent;)V

    .line 250
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    iget-object v6, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lflipboard/gui/item/ImageDetailView;->i:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v8

    iget-object v8, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget-object v9, p0, Lflipboard/gui/item/ImageDetailView;->i:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    invoke-virtual {v4, v6, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 251
    iget v4, p0, Lflipboard/gui/item/ImageDetailView;->k:F

    div-float/2addr v0, v4

    .line 252
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    iget-object v6, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lflipboard/gui/item/ImageDetailView;->j:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v0, v0, v6, v8}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_5

    .line 152
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
