.class public Lflipboard/gui/item/VideoItemTablet;
.super Lflipboard/gui/FLRelativeLayout;
.source "VideoItemTablet.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/objs/FeedItem;

.field b:Lflipboard/gui/FLImageView;

.field c:Lflipboard/gui/section/AttributionServiceInfo;

.field private d:Z

.field private e:Landroid/view/ViewGroup$LayoutParams;

.field private f:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/VideoItemTablet;->d:Z

    .line 33
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoItemTablet;->setClipToPadding(Z)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 1

    .prologue
    .line 48
    iput-object p2, p0, Lflipboard/gui/item/VideoItemTablet;->a:Lflipboard/objs/FeedItem;

    .line 49
    invoke-virtual {p0, p2}, Lflipboard/gui/item/VideoItemTablet;->setTag(Ljava/lang/Object;)V

    .line 51
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p2}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 55
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->c:Lflipboard/gui/section/AttributionServiceInfo;

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/AttributionServiceInfo;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 56
    return-void
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 65
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "VideoItemTablet:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 66
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    iget-boolean v0, p0, Lflipboard/gui/item/VideoItemTablet;->d:Z

    if-eqz v0, :cond_0

    .line 68
    iput-boolean v3, p0, Lflipboard/gui/item/VideoItemTablet;->d:Z

    .line 69
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/VideoItemTablet;->a:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/item/VideoItemTablet;->d:Z

    if-nez v0, :cond_0

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/VideoItemTablet;->d:Z

    .line 74
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoItemTablet;->removeView(Landroid/view/View;)V

    .line 75
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/item/VideoItemTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    .line 76
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/VideoItemTablet;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/VideoItemTablet;->e:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0, v0, v3, v1}, Lflipboard/gui/item/VideoItemTablet;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->a:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 39
    invoke-virtual {p0}, Lflipboard/gui/item/VideoItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 40
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    .line 41
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->e:Landroid/view/ViewGroup$LayoutParams;

    .line 42
    iget-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->f:Landroid/graphics/drawable/Drawable;

    .line 43
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoItemTablet;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionServiceInfo;

    iput-object v0, p0, Lflipboard/gui/item/VideoItemTablet;->c:Lflipboard/gui/section/AttributionServiceInfo;

    .line 44
    return-void
.end method
