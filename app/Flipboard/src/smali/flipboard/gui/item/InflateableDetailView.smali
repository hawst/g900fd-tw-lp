.class public abstract Lflipboard/gui/item/InflateableDetailView;
.super Lflipboard/gui/ContainerView;
.source "InflateableDetailView.java"

# interfaces
.implements Lflipboard/gui/item/DetailView;


# instance fields
.field public final o:Lflipboard/objs/FeedItem;

.field public final p:Lflipboard/objs/ConfigService;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 25
    iput-object p2, p0, Lflipboard/gui/item/InflateableDetailView;->o:Lflipboard/objs/FeedItem;

    .line 26
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lflipboard/gui/item/InflateableDetailView;->p:Lflipboard/objs/ConfigService;

    .line 27
    return-void

    .line 26
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p2, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 36
    invoke-virtual {p0}, Lflipboard/gui/item/InflateableDetailView;->getLayoutId()I

    move-result v0

    .line 37
    invoke-virtual {p0}, Lflipboard/gui/item/InflateableDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    invoke-virtual {p0}, Lflipboard/gui/item/InflateableDetailView;->a()V

    .line 39
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lflipboard/gui/item/InflateableDetailView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/flipping/FlippingContainer;

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lflipboard/gui/item/InflateableDetailView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lflipboard/gui/flipping/FlippingContainer;

    iget-boolean v0, v0, Lflipboard/gui/flipping/FlippingContainer;->a:Z

    .line 76
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lflipboard/gui/item/InflateableDetailView;->o:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected abstract getLayoutId()I
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 55
    invoke-virtual {p0}, Lflipboard/gui/item/InflateableDetailView;->getChildCount()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 56
    invoke-virtual {p0, v0}, Lflipboard/gui/item/InflateableDetailView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    sub-int v2, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method
