.class Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;
.super Lflipboard/util/FLWebViewClient;
.source "FlipmagDetailViewTablet.java"


# instance fields
.field final synthetic a:Lflipboard/gui/item/FlipmagDetailViewTablet;


# direct methods
.method public constructor <init>(Lflipboard/gui/item/FlipmagDetailViewTablet;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 779
    iput-object p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    .line 780
    invoke-direct {p0, p2}, Lflipboard/util/FLWebViewClient;-><init>(Landroid/content/Context;)V

    .line 781
    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1166
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "fonts"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Landroid/net/Uri;Landroid/webkit/WebView;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 1010
    invoke-super {p0, p1, p2, p3}, Lflipboard/util/FLWebViewClient;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/webkit/WebView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1141
    :cond_0
    :goto_0
    return v2

    .line 1014
    :cond_1
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 1016
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v3}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/gui/FLWebView;->getContentHeight()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget v4, v4, Lflipboard/gui/item/FlipmagDetailViewTablet;->i:F

    mul-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {v1, v3}, Lflipboard/gui/item/FlipmagDetailViewTablet;->setContentHeight(I)V

    .line 1018
    const-string v1, "pagination-did-finish"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1019
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->e(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    .line 1021
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLBridgeAndroid.setContentWidth(document.body.scrollWidth)"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 1023
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->c(Lflipboard/gui/item/FlipmagDetailViewTablet;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1024
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->c:Lflipboard/gui/FLWebView;

    const-string v1, "javascript:FLBridgeAndroid.parseHTML(document.getElementsByTagName(\'html\')[0].innerHTML)"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 1026
    :cond_2
    const-string v1, "pageready"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1027
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Page ready"

    invoke-virtual {v1, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 1029
    sget-object v1, Lflipboard/usage/UsageTracker;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    sget-object v3, Lflipboard/usage/UsageTracker;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v0, v2

    .line 1030
    :cond_3
    if-eqz v0, :cond_0

    sget-wide v0, Lflipboard/usage/UsageTracker;->a:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_0

    .line 1031
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v4, Lflipboard/usage/UsageTracker;->a:J

    sub-long v4, v0, v4

    .line 1032
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aH:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_4

    const-string v0, "flipmag_load_with_aml"

    .line 1033
    :goto_1
    invoke-static {v0, v4, v5}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;J)V

    .line 1034
    sput-wide v6, Lflipboard/usage/UsageTracker;->a:J

    goto/16 :goto_0

    .line 1032
    :cond_4
    const-string v0, "flipmag_load_no_aml"

    goto :goto_1

    .line 1036
    :cond_5
    const-string v1, "//log?msg="

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1037
    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    goto/16 :goto_0

    .line 1042
    :cond_6
    const-string v1, "//play-video"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1043
    const/16 v0, 0xc

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1044
    const-string v0, "video"

    .line 1046
    const-string v3, ".mp4"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "html5"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1047
    :cond_7
    const-string v0, "h264"

    .line 1049
    :cond_8
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "flvideo://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1050
    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v3}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lflipboard/activities/VideoActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1051
    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v3, v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v3, v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v3, v3, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v3, :cond_9

    .line 1052
    const-string v3, "sid"

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v4, v4, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1054
    :cond_9
    const-string v3, "uri"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1055
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1058
    :cond_a
    const-string v1, "//showHTML"

    invoke-virtual {p1, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1059
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    new-array v1, v2, [Ljava/lang/Object;

    aput-object p1, v1, v0

    .line 1060
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v3}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "flipmag_show_html"

    const-string v4, "//showHTML?url="

    const-string v5, ""

    invoke-virtual {p1, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 1063
    :cond_b
    const-string v0, "//showImage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1066
    if-eqz p2, :cond_c

    .line 1068
    :try_start_0
    const-string v0, "url"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1072
    :goto_2
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v0, v3

    const/4 v3, 0x1

    aput-object v1, v0, v3

    .line 1073
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 1074
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lflipboard/activities/DetailActivity;

    invoke-direct {v3, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1075
    const-string v4, "detail_image_url"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1076
    const-string v1, "sid"

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v4, v4, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1077
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 1078
    const v1, 0x7f040001

    const v3, 0x7f040004

    invoke-virtual {v0, v1, v3}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1079
    :catch_0
    move-exception v0

    .line 1080
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1070
    :cond_c
    :try_start_1
    const-string v0, "//showImage?url="

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move-object v1, v0

    goto :goto_2

    .line 1083
    :cond_d
    const-string v0, "//update-pagecount?pageCount="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1085
    const/16 v0, 0x1d

    :try_start_2
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1086
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3

    .line 1087
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLBridgeAndroid.setContentHeight(document.body.scrollHeight)"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 1088
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLBridgeAndroid.setContentWidth(document.body.scrollWidth)"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 1089
    :catch_1
    move-exception v0

    .line 1091
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1094
    :cond_e
    const-string v0, "//update-pageindex?pageIndex="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1096
    const/16 v0, 0x1d

    :try_start_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1097
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 1098
    :catch_2
    move-exception v0

    .line 1100
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1103
    :cond_f
    const-string v0, "//update-snapshot?pageIndex="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1105
    const/16 v0, 0x1c

    :try_start_4
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 1106
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v3

    .line 1108
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {}, Lflipboard/gui/item/FlipmagDetailViewTablet;->c()V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 1110
    :catch_3
    move-exception v0

    .line 1112
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1115
    :cond_10
    const-string v0, "//update-stats?stats="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1117
    :try_start_5
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->h(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/io/UsageEvent;

    move-result-object v0

    iget-object v0, v0, Lflipboard/io/UsageEvent;->f:Lflipboard/json/FLObject;

    const-string v1, "magStats"

    new-instance v3, Lflipboard/json/JSONParser;

    const/16 v4, 0x15

    invoke-virtual {p1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-static {v4, v5}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lflipboard/json/JSONParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lflipboard/json/JSONParser;->Q()Lflipboard/json/FLObject;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lflipboard/json/FLObject;->a(Ljava/lang/String;Ljava/lang/Object;)Lflipboard/json/FLObject;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    .line 1118
    :catch_4
    move-exception v0

    .line 1119
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1121
    :cond_11
    const-string v0, "//set-socialbar-styles?barstyles="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1124
    :try_start_6
    new-instance v1, Lorg/json/JSONObject;

    const/16 v0, 0x21

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "UTF-8"

    invoke-static {v0, v3}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1125
    invoke-virtual {v1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 1126
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1127
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1128
    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v4, v4, Lflipboard/gui/item/FlipmagDetailViewTablet;->n:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_3

    .line 1130
    :catch_5
    move-exception v0

    .line 1131
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1133
    :cond_12
    const-string v0, "//request-jsonp?url="

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1135
    const/16 v0, 0x14

    :try_start_7
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1136
    sget-object v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v1, v3
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    .line 1137
    :catch_6
    move-exception v0

    .line 1138
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/webkit/WebView;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 850
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    aput-object p1, v0, v6

    .line 852
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->a(Ljava/lang/String;Landroid/webkit/WebView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 998
    :cond_0
    :goto_0
    return v7

    .line 856
    :cond_1
    const-string v0, "//audio"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 857
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "flipboard:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 859
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    aput-object v4, v0, v6

    .line 862
    const-string v0, "audioURL"

    invoke-virtual {v4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 865
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    new-array v0, v7, [Ljava/lang/Object;

    aput-object v5, v0, v6

    .line 868
    if-nez v5, :cond_4

    const-string v0, "audioURL="

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 869
    const-string v0, "audioURL="

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x9

    .line 870
    if-ltz v0, :cond_e

    .line 871
    const-string v2, "&"

    invoke-virtual {p1, v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 872
    if-ltz v2, :cond_3

    .line 873
    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 886
    :goto_1
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    new-array v0, v8, [Ljava/lang/Object;

    aput-object v5, v0, v6

    aput-object v2, v0, v7

    .line 889
    const-string v0, "action"

    invoke-virtual {v4, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 892
    if-nez v0, :cond_d

    const-string v3, "//audio?title="

    invoke-virtual {p1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 893
    const-string v3, "action="

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x9

    .line 894
    if-ltz v3, :cond_d

    .line 895
    const-string v0, "&"

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    .line 896
    if-ltz v0, :cond_6

    .line 897
    invoke-virtual {p1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 904
    :goto_2
    sget-object v0, Lflipboard/service/audio/FLAudioManager;->b:Lflipboard/util/Log;

    new-array v0, v8, [Ljava/lang/Object;

    aput-object v5, v0, v6

    aput-object v3, v0, v7

    .line 907
    if-nez v2, :cond_2

    if-nez v3, :cond_2

    .line 909
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-object v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v5}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d0040

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 912
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    if-nez v0, :cond_7

    .line 915
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 916
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 917
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v3, "audio/*"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 918
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 919
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLAudioPlaybackDidFinish()"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 921
    const-string v0, "audio play"

    const-string v1, "sourceURL"

    invoke-static {v0, v1, v2}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 875
    :cond_3
    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v2, v0

    .line 878
    goto/16 :goto_1

    :cond_4
    if-eqz v5, :cond_5

    .line 880
    :try_start_0
    const-string v0, "UTF-8"

    invoke-static {v5, v0}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 883
    goto/16 :goto_1

    .line 881
    :catch_0
    move-exception v0

    .line 882
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    :cond_5
    move-object v2, v1

    goto/16 :goto_1

    .line 899
    :cond_6
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_2

    .line 927
    :cond_7
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->g(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/util/Observer;

    move-result-object v0

    if-nez v0, :cond_8

    .line 928
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iget-object v5, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    new-instance v6, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;

    invoke-direct {v6, p0, v2}, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;-><init>(Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;Ljava/lang/String;)V

    invoke-static {v5, v6}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;Lflipboard/util/Observer;)Lflipboard/util/Observer;

    move-result-object v5

    invoke-virtual {v0, v5}, Lflipboard/service/audio/FLAudioManager;->b(Lflipboard/util/Observer;)V

    .line 977
    :cond_8
    if-nez v3, :cond_a

    .line 978
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aD:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_9

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 981
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v3, v3, Lflipboard/gui/item/FlipmagDetailViewTablet;->a:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v4, v4, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v3

    const-string v4, "playFromFlipmag"

    invoke-virtual {v0, v1, v3, v4}, Lflipboard/service/audio/FLAudioManager;->a(Lflipboard/objs/FeedItem;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 986
    :goto_4
    const-string v0, "audio play"

    const-string v1, "sourceURL"

    invoke-static {v0, v1, v2}, Lflipboard/io/UsageEvent;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 983
    :cond_9
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v4, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "artist"

    invoke-virtual {v4, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "playFromFlipmag"

    invoke-virtual {v0, v2, v1, v3, v4}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    .line 988
    :cond_a
    const-string v0, "pause"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 989
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v1, "pauseFromFlipmag"

    invoke-virtual {v0, v1}, Lflipboard/service/audio/FLAudioManager;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 990
    :cond_b
    const-string v0, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 991
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->f(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/service/audio/FLAudioManager;

    move-result-object v0

    const-string v3, "playFromFlipmag"

    invoke-virtual {v0}, Lflipboard/service/audio/FLAudioManager;->m()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v0, v0, Lflipboard/service/audio/FLAudioManager;->c:Lflipboard/service/audio/MediaPlayerService;

    iput-object v3, v0, Lflipboard/service/audio/MediaPlayerService;->h:Ljava/lang/String;

    iget-object v3, v0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    if-eqz v3, :cond_c

    iget-object v3, v0, Lflipboard/service/audio/MediaPlayerService;->d:Lflipboard/service/audio/Song;

    iget-object v3, v3, Lflipboard/service/audio/Song;->e:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v0}, Lflipboard/service/audio/MediaPlayerService;->a()V

    goto/16 :goto_0

    :cond_c
    const-string v3, "resumeStream"

    invoke-virtual {v0, v2, v1, v1, v3}, Lflipboard/service/audio/MediaPlayerService;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    move-object v3, v0

    goto/16 :goto_2

    :cond_e
    move-object v0, v1

    goto/16 :goto_3
.end method

.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1005
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1006
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 785
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 793
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:if(typeof FLBridgeAndroid !== \"undefined\"){if(FLBridgeAndroid.setContentHeight){FLBridgeAndroid.setContentHeight(document.body.scrollHeight)}}"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 795
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:if(typeof FLBridgeAndroid !== \"undefined\"){if(FLBridgeAndroid.setContentWidth){FLBridgeAndroid.setContentWidth(document.body.scrollWidth)}}"

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 797
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->c(Lflipboard/gui/item/FlipmagDetailViewTablet;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 798
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->d(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    .line 800
    :cond_0
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 804
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->b:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    aput-object p3, v0, v3

    .line 805
    invoke-super {p0, p1, p2, p3, p4}, Lflipboard/util/FLWebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 806
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    aput-object p3, v0, v3

    .line 807
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->e(Lflipboard/gui/item/FlipmagDetailViewTablet;)V

    .line 808
    return-void
.end method

.method public shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1146
    const-string v0, "/assets/fonts/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1148
    :try_start_0
    const-string v0, "/"

    invoke-virtual {p2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1149
    invoke-direct {p0, v1}, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1150
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v2, "text/css"

    const-string v3, "UTF-8"

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v4}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fonts/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V

    .line 1161
    :goto_0
    return-object v0

    .line 1152
    :cond_0
    const-string v0, ".otf"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ".otf"

    const-string v2, ".ttf"

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1153
    :goto_1
    invoke-direct {p0, v1}, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1154
    new-instance v0, Landroid/webkit/WebResourceResponse;

    const-string v2, "text/css"

    const-string v3, "UTF-8"

    iget-object v4, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v4}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fonts/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v0, v2, v3, v1}, Landroid/webkit/WebResourceResponse;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1157
    :catch_0
    move-exception v0

    .line 1158
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 1161
    :cond_1
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->shouldInterceptRequest(Landroid/webkit/WebView;Ljava/lang/String;)Landroid/webkit/WebResourceResponse;

    move-result-object v0

    goto :goto_0

    .line 1152
    :cond_2
    :try_start_1
    const-string v0, ".ttf"

    const-string v2, ".otf"

    invoke-virtual {v1, v0, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 821
    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->b:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-boolean v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet;->h:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    .line 824
    if-eqz p2, :cond_0

    const-string v0, "flipCart=1"

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 825
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget v1, v0, Lflipboard/service/FlipboardManager;->an:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lflipboard/service/FlipboardManager;->an:I

    .line 827
    new-instance v0, Lflipboard/objs/UsageEventV2;

    sget-object v1, Lflipboard/objs/UsageEventV2$EventAction;->s:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v0, v1, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 828
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->l:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v2, v2, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->aa()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 829
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->p:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v2, v2, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 830
    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->b:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v2, v2, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 831
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 834
    :cond_0
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 844
    :cond_1
    :goto_0
    return v3

    .line 839
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-boolean v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet;->h:Z

    if-nez v0, :cond_1

    .line 843
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->c(Lflipboard/gui/item/FlipmagDetailViewTablet;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->b(Lflipboard/gui/item/FlipmagDetailViewTablet;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->b(Lflipboard/gui/item/FlipmagDetailViewTablet;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lflipboard/util/AndroidUtil;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    iget-object v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet;->d:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-static {v0, p2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
