.class public Lflipboard/gui/item/WebDetailView;
.super Lflipboard/gui/item/InflateableDetailView;
.source "WebDetailView.java"

# interfaces
.implements Lflipboard/gui/FLWebView$TouchListener;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field public b:Lflipboard/gui/FLWebView;

.field c:Lflipboard/gui/ServicePromptView;

.field d:Z

.field private e:J

.field private f:Lflipboard/gui/FLBusyView;

.field private g:Landroid/view/View;

.field private h:Lflipboard/gui/actionbar/FLActionBar;

.field private final i:Z

.field private final j:Lflipboard/service/FlipboardManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "webdetailview"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/item/WebDetailView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/item/InflateableDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 46
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    iput-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->i:Z

    .line 48
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    .line 71
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->c()V

    .line 72
    invoke-direct {p0}, Lflipboard/gui/item/WebDetailView;->f()V

    .line 73
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lflipboard/gui/item/InflateableDetailView;-><init>(Landroid/content/Context;Lflipboard/objs/FeedItem;)V

    .line 46
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    iput-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->i:Z

    .line 48
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    .line 59
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->c()V

    .line 62
    const-string v0, "status"

    iget-object v1, p2, Lflipboard/objs/FeedItem;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 63
    invoke-direct {p0}, Lflipboard/gui/item/WebDetailView;->f()V

    .line 64
    iget-object v0, p2, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/WebDetailView;->a(Ljava/lang/String;)V

    .line 66
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/WebDetailView;)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    const-string v0, "WebDetailView:showLoadingIndicator"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->g:Landroid/view/View;

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x10a0000

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->f:Lflipboard/gui/FLBusyView;

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/item/WebDetailView;)V
    .locals 0

    .prologue
    .line 35
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->e()V

    return-void
.end method

.method static synthetic c(Lflipboard/gui/item/WebDetailView;)Lflipboard/service/FlipboardManager;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    return-object v0
.end method

.method private f()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 78
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 80
    const v1, 0x7f0a0168

    invoke-virtual {p0, v1}, Lflipboard/gui/item/WebDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLWebView;

    iput-object v1, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    .line 81
    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v1}, Lflipboard/gui/FLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 84
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 85
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    .line 86
    invoke-virtual {v1, v4}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 87
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 88
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 89
    sget-object v2, Landroid/webkit/WebSettings$PluginState;->ON:Landroid/webkit/WebSettings$PluginState;

    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setPluginState(Landroid/webkit/WebSettings$PluginState;)V

    .line 90
    invoke-virtual {v1, v3}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 91
    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v1, v4}, Lflipboard/gui/FLWebView;->setScrollBarStyle(I)V

    .line 93
    const v1, 0x7f0a0090

    invoke-virtual {p0, v1}, Lflipboard/gui/item/WebDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLBusyView;

    iput-object v1, p0, Lflipboard/gui/item/WebDetailView;->f:Lflipboard/gui/FLBusyView;

    .line 94
    const v1, 0x7f0a016a

    invoke-virtual {p0, v1}, Lflipboard/gui/item/WebDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/item/WebDetailView;->g:Landroid/view/View;

    .line 96
    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    new-instance v2, Lflipboard/gui/item/WebDetailView$1;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/item/WebDetailView$1;-><init>(Lflipboard/gui/item/WebDetailView;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 137
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLWebView;->setTouchListener(Lflipboard/gui/FLWebView$TouchListener;)V

    .line 140
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    iget-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->i:Z

    if-nez v0, :cond_0

    .line 188
    const v0, 0x7f0a004c

    invoke-virtual {p0, v0}, Lflipboard/gui/item/WebDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p0, Lflipboard/gui/item/WebDetailView;->h:Lflipboard/gui/actionbar/FLActionBar;

    .line 189
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->h:Lflipboard/gui/actionbar/FLActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 190
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->h:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800aa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBar;->setBackgroundColor(I)V

    .line 194
    :cond_0
    const v0, 0x7f0a02fc

    invoke-virtual {p0, v0}, Lflipboard/gui/item/WebDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ServicePromptView;

    .line 195
    if-eqz v0, :cond_2

    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->o:Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lflipboard/gui/ServicePromptView;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    iput-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    .line 202
    :cond_1
    :goto_0
    return-void

    .line 198
    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->i:Z

    if-nez v0, :cond_1

    .line 199
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->h:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    goto :goto_0
.end method

.method public final a(Lflipboard/gui/FLWebView;FF)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x12c

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 246
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    if-eqz v0, :cond_0

    .line 248
    iget-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    invoke-virtual {v0}, Lflipboard/gui/ServicePromptView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    cmpg-float v0, p2, v2

    if-gez v0, :cond_1

    .line 250
    iput-boolean v3, p0, Lflipboard/gui/item/WebDetailView;->d:Z

    .line 251
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    invoke-virtual {v1}, Lflipboard/gui/ServicePromptView;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v2, v1}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 252
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 253
    invoke-virtual {v0, v3}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 254
    invoke-virtual {p1, v0}, Lflipboard/gui/FLWebView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 255
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/WebDetailView$2;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/item/WebDetailView$2;-><init>(Lflipboard/gui/item/WebDetailView;Lflipboard/gui/FLWebView;)V

    invoke-virtual {v0, v4, v5, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    invoke-virtual {v0}, Lflipboard/gui/ServicePromptView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x43160000    # 150.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    cmpl-float v0, p3, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lflipboard/gui/FLWebView;->getScrollY()I

    move-result v0

    if-nez v0, :cond_0

    .line 273
    iput-boolean v3, p0, Lflipboard/gui/item/WebDetailView;->d:Z

    .line 274
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/ServicePromptView;->setVisibility(I)V

    .line 275
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    invoke-virtual {v1}, Lflipboard/gui/ServicePromptView;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    int-to-float v1, v1

    invoke-direct {v0, v2, v2, v1, v2}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 276
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 277
    invoke-virtual {v0, v3}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 278
    new-instance v1, Lflipboard/gui/item/WebDetailView$3;

    invoke-direct {v1, p0}, Lflipboard/gui/item/WebDetailView$3;-><init>(Lflipboard/gui/item/WebDetailView;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 293
    invoke-virtual {p1, v0}, Lflipboard/gui/FLWebView;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 151
    sget-object v0, Lflipboard/gui/item/WebDetailView;->a:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 153
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    .line 159
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 157
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->e()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 206
    invoke-virtual {p0}, Lflipboard/gui/item/WebDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 207
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v0

    .line 208
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 210
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lflipboard/gui/actionbar/FLActionBar;->i:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v4, p0, Lflipboard/gui/item/WebDetailView;->e:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x190

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    :cond_1
    move v0, v1

    .line 222
    :goto_0
    return v0

    .line 215
    :cond_2
    iput-wide v2, p0, Lflipboard/gui/item/WebDetailView;->e:J

    .line 216
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->goBack()V

    .line 218
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 222
    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 312
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->g:Landroid/view/View;

    .line 313
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 314
    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->j:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/item/WebDetailView$4;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/item/WebDetailView$4;-><init>(Lflipboard/gui/item/WebDetailView;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 333
    :cond_0
    return-void

    .line 312
    :cond_1
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->f:Lflipboard/gui/FLBusyView;

    goto :goto_0
.end method

.method protected getLayoutId()I
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lflipboard/gui/item/WebDetailView;->i:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030064

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f030063

    goto :goto_0
.end method

.method public getWebView()Lflipboard/gui/FLWebView;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 173
    invoke-super {p0}, Lflipboard/gui/item/InflateableDetailView;->onDetachedFromWindow()V

    .line 174
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->b:Lflipboard/gui/FLWebView;

    invoke-virtual {v0}, Lflipboard/gui/FLWebView;->stopLoading()V

    .line 177
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 227
    if-eqz p1, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->o:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    iget-object v1, p0, Lflipboard/gui/item/WebDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->bp:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v1}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v1

    iget-boolean v3, v2, Lflipboard/objs/ConfigService;->bO:Z

    if-eqz v3, :cond_0

    iget-boolean v2, v2, Lflipboard/objs/ConfigService;->bx:Z

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lflipboard/service/Account;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v4}, Lflipboard/gui/ServicePromptView;->setVisibility(I)V

    :cond_0
    invoke-virtual {v0}, Lflipboard/gui/ServicePromptView;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_2

    const/4 v0, 0x1

    .line 230
    :goto_0
    if-nez v0, :cond_1

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/item/WebDetailView;->c:Lflipboard/gui/ServicePromptView;

    .line 234
    :cond_1
    return-void

    .line 229
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
