.class Lflipboard/gui/item/ImageDetailView$3;
.super Ljava/lang/Object;
.source "ImageDetailView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field final synthetic b:F

.field final synthetic c:Landroid/view/animation/Interpolator;

.field final synthetic d:F

.field final synthetic e:F

.field final synthetic f:F

.field final synthetic g:F

.field final synthetic h:F

.field final synthetic i:F

.field final synthetic j:Lflipboard/gui/item/ImageDetailView;


# direct methods
.method constructor <init>(Lflipboard/gui/item/ImageDetailView;Landroid/view/animation/Interpolator;FFFFFF)V
    .locals 2

    .prologue
    .line 335
    iput-object p1, p0, Lflipboard/gui/item/ImageDetailView$3;->j:Lflipboard/gui/item/ImageDetailView;

    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lflipboard/gui/item/ImageDetailView$3;->b:F

    iput-object p2, p0, Lflipboard/gui/item/ImageDetailView$3;->c:Landroid/view/animation/Interpolator;

    iput p3, p0, Lflipboard/gui/item/ImageDetailView$3;->d:F

    iput p4, p0, Lflipboard/gui/item/ImageDetailView$3;->e:F

    iput p5, p0, Lflipboard/gui/item/ImageDetailView$3;->f:F

    iput p6, p0, Lflipboard/gui/item/ImageDetailView$3;->g:F

    iput p7, p0, Lflipboard/gui/item/ImageDetailView$3;->h:F

    iput p8, p0, Lflipboard/gui/item/ImageDetailView$3;->i:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 336
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lflipboard/gui/item/ImageDetailView$3;->a:J

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 340
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lflipboard/gui/item/ImageDetailView$3;->a:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    iget v1, p0, Lflipboard/gui/item/ImageDetailView$3;->b:F

    div-float/2addr v0, v1

    invoke-static {v7, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 341
    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView$3;->c:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    .line 342
    iget v2, p0, Lflipboard/gui/item/ImageDetailView$3;->d:F

    iget v3, p0, Lflipboard/gui/item/ImageDetailView$3;->e:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v1

    .line 343
    iget v3, p0, Lflipboard/gui/item/ImageDetailView$3;->f:F

    iget v4, p0, Lflipboard/gui/item/ImageDetailView$3;->g:F

    sub-float/2addr v3, v4

    mul-float/2addr v3, v1

    .line 344
    iget v4, p0, Lflipboard/gui/item/ImageDetailView$3;->h:F

    iget v5, p0, Lflipboard/gui/item/ImageDetailView$3;->i:F

    sub-float/2addr v4, v5

    mul-float/2addr v1, v4

    .line 345
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView$3;->j:Lflipboard/gui/item/ImageDetailView;

    iget-object v4, v4, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v4}, Landroid/graphics/Matrix;->reset()V

    .line 346
    iget-object v4, p0, Lflipboard/gui/item/ImageDetailView$3;->j:Lflipboard/gui/item/ImageDetailView;

    iget-object v4, v4, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    iget v5, p0, Lflipboard/gui/item/ImageDetailView$3;->e:F

    add-float/2addr v5, v2

    iget v6, p0, Lflipboard/gui/item/ImageDetailView$3;->e:F

    add-float/2addr v2, v6

    invoke-virtual {v4, v5, v2}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 347
    iget-object v2, p0, Lflipboard/gui/item/ImageDetailView$3;->j:Lflipboard/gui/item/ImageDetailView;

    iget-object v2, v2, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    iget v4, p0, Lflipboard/gui/item/ImageDetailView$3;->g:F

    add-float/2addr v3, v4

    iget v4, p0, Lflipboard/gui/item/ImageDetailView$3;->i:F

    add-float/2addr v1, v4

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 348
    iget-object v1, p0, Lflipboard/gui/item/ImageDetailView$3;->j:Lflipboard/gui/item/ImageDetailView;

    invoke-static {v1}, Lflipboard/gui/item/ImageDetailView;->a(Lflipboard/gui/item/ImageDetailView;)Lflipboard/gui/FLImageView;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/ImageDetailView$3;->j:Lflipboard/gui/item/ImageDetailView;

    iget-object v2, v2, Lflipboard/gui/item/ImageDetailView;->b:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 349
    cmpg-float v0, v0, v7

    if-gez v0, :cond_0

    .line 350
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0, p0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 352
    :cond_0
    return-void
.end method
