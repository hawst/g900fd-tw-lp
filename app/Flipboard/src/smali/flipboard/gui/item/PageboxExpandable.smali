.class public Lflipboard/gui/item/PageboxExpandable;
.super Lflipboard/gui/FLExpandableLayout;
.source "PageboxExpandable.java"

# interfaces
.implements Lflipboard/gui/section/scrolling/PhoneItemView;


# instance fields
.field private a:Lflipboard/gui/item/InFeedExpandableModuleHead;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lflipboard/gui/FLExpandableLayout;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLExpandableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLExpandableLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    const v5, 0x7f0a01e9

    const/16 v4, 0x8

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 42
    if-eqz p2, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    iget-object v0, p2, Lflipboard/objs/FeedItem;->bL:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup;

    .line 44
    iget-object v1, p0, Lflipboard/gui/item/PageboxExpandable;->a:Lflipboard/gui/item/InFeedExpandableModuleHead;

    invoke-virtual {v1, p1, p2}, Lflipboard/gui/item/InFeedExpandableModuleHead;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 45
    iget-object v1, v0, Lflipboard/objs/SidebarGroup;->e:Ljava/util/List;

    .line 46
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->p:Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 47
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SidebarGroup$RenderHints;

    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->p:Lflipboard/objs/SidebarGroup$RenderHints;

    .line 50
    iget-object v1, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v3, "pageboxList"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 51
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxExpandable;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030131

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 52
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/item/GenericSuggestedFollowItem;

    .line 53
    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->setListRowCount(I)V

    .line 54
    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/item/GenericSuggestedFollowItem;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 55
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    .line 62
    :goto_0
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxExpandable;->addView(Landroid/view/View;)V

    .line 64
    invoke-virtual {p0, v2, v0}, Lflipboard/gui/item/PageboxExpandable;->a(Landroid/view/View;Landroid/view/View;)V

    .line 68
    :cond_0
    return-void

    .line 56
    :cond_1
    iget-object v0, v0, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    const-string v1, "pageboxCarousel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 57
    invoke-virtual {p0}, Lflipboard/gui/item/PageboxExpandable;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300cc

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/scrolling/MagazineCarouselItem;

    .line 59
    invoke-virtual {v0, p1, p2}, Lflipboard/gui/section/scrolling/MagazineCarouselItem;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    .line 60
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lflipboard/gui/FLExpandableLayout;->onFinishInflate()V

    .line 73
    const v0, 0x7f0a01ea

    invoke-virtual {p0, v0}, Lflipboard/gui/item/PageboxExpandable;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/InFeedExpandableModuleHead;

    iput-object v0, p0, Lflipboard/gui/item/PageboxExpandable;->a:Lflipboard/gui/item/InFeedExpandableModuleHead;

    .line 74
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0

    .prologue
    .line 79
    return-void
.end method
