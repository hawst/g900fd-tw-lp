.class public Lflipboard/gui/item/AdItem;
.super Lflipboard/gui/FLRelativeLayout;
.source "AdItem.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# static fields
.field static final synthetic e:Z

.field private static final f:Landroid/graphics/Paint;


# instance fields
.field a:Lflipboard/gui/FLImageView;

.field b:Z

.field c:Lflipboard/objs/Ad$Asset;

.field d:Lflipboard/objs/FeedItem;

.field private g:Lflipboard/gui/item/AdButtonGroup;

.field private h:Lflipboard/service/Section;

.field private i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/graphics/RectF;",
            "Lflipboard/objs/Ad$HotSpot;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lflipboard/objs/Ad$ButtonInfo;

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private l:Lflipboard/objs/Ad$VideoInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 30
    const-class v0, Lflipboard/gui/item/AdItem;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    sput-boolean v0, Lflipboard/gui/item/AdItem;->e:Z

    .line 32
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 34
    sput-object v0, Lflipboard/gui/item/AdItem;->f:Landroid/graphics/Paint;

    const/high16 v1, -0x10000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    sget-object v0, Lflipboard/gui/item/AdItem;->f:Landroid/graphics/Paint;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 36
    sget-object v0, Lflipboard/gui/item/AdItem;->f:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 37
    sget-object v0, Lflipboard/gui/item/AdItem;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 38
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    invoke-virtual {p0, v0}, Lflipboard/gui/item/AdItem;->setClipToPadding(Z)V

    .line 56
    invoke-virtual {p0, v0}, Lflipboard/gui/item/AdItem;->setWillNotDraw(Z)V

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/FLAdManager$AdAsset;II)V
    .locals 5

    .prologue
    .line 61
    iget-object v0, p1, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v0, v0, Lflipboard/objs/Ad;->c:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/item/AdItem;->k:Ljava/util/List;

    .line 62
    invoke-virtual {p1, p2, p3}, Lflipboard/service/FLAdManager$AdAsset;->a(II)V

    .line 63
    iget-object v0, p1, Lflipboard/service/FLAdManager$AdAsset;->b:Lflipboard/objs/Ad$Asset;

    iput-object v0, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    .line 64
    iget-object v0, p1, Lflipboard/service/FLAdManager$AdAsset;->a:Lflipboard/objs/Ad;

    iget-object v0, v0, Lflipboard/objs/Ad;->f:Lflipboard/objs/Ad$ButtonInfo;

    iput-object v0, p0, Lflipboard/gui/item/AdItem;->j:Lflipboard/objs/Ad$ButtonInfo;

    .line 65
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    iget-object v0, v0, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    invoke-virtual {v0, p2, p3}, Lflipboard/objs/Ad$Asset;->a(II)F

    move-result v1

    .line 68
    new-instance v0, Ljava/util/HashMap;

    iget-object v2, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    iget-object v2, v2, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/item/AdItem;->i:Ljava/util/Map;

    .line 69
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    iget-object v0, v0, Lflipboard/objs/Ad$Asset;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$HotSpot;

    .line 70
    iget-object v3, p0, Lflipboard/gui/item/AdItem;->i:Ljava/util/Map;

    iget-object v4, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    invoke-virtual {v4, v0, v1, p2, p3}, Lflipboard/objs/Ad$Asset;->a(Lflipboard/objs/Ad$HotSpot;FII)Landroid/graphics/RectF;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f000000    # 0.5f

    .line 107
    iput-object p2, p0, Lflipboard/gui/item/AdItem;->d:Lflipboard/objs/FeedItem;

    .line 108
    iput-object p1, p0, Lflipboard/gui/item/AdItem;->h:Lflipboard/service/Section;

    .line 109
    sget-boolean v0, Lflipboard/gui/item/AdItem;->e:Z

    if-nez v0, :cond_0

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 110
    :cond_0
    sget-boolean v0, Lflipboard/gui/item/AdItem;->e:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 111
    :cond_1
    sget-boolean v0, Lflipboard/gui/item/AdItem;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 112
    :cond_2
    invoke-virtual {p0, p2}, Lflipboard/gui/item/AdItem;->setTag(Ljava/lang/Object;)V

    .line 113
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLImageView;->setScaling(Z)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->a:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->c:Lflipboard/objs/Ad$Asset;

    iget-object v1, v1, Lflipboard/objs/Ad$Asset;->a:Ljava/lang/String;

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLImageView;->a(Ljava/lang/String;Landroid/graphics/PointF;)V

    .line 115
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->i:Ljava/util/Map;

    if-eqz v0, :cond_3

    invoke-static {}, Lflipboard/service/FLAdManager;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 118
    iget-object v1, p0, Lflipboard/gui/item/AdItem;->a:Lflipboard/gui/FLImageView;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    sget-object v2, Lflipboard/gui/item/AdItem;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/FLImageView;->a(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 119
    :cond_3
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->j:Lflipboard/objs/Ad$ButtonInfo;

    if-eqz v0, :cond_4

    .line 123
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->g:Lflipboard/gui/item/AdButtonGroup;

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->j:Lflipboard/objs/Ad$ButtonInfo;

    iget-object v2, p0, Lflipboard/gui/item/AdItem;->k:Ljava/util/List;

    invoke-virtual {v0, p1, v1, v2}, Lflipboard/gui/item/AdButtonGroup;->a(Lflipboard/service/Section;Lflipboard/objs/Ad$ButtonInfo;Ljava/util/List;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->g:Lflipboard/gui/item/AdButtonGroup;

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->l:Lflipboard/objs/Ad$VideoInfo;

    invoke-virtual {v0, v1}, Lflipboard/gui/item/AdButtonGroup;->setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V

    .line 125
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->g:Lflipboard/gui/item/AdButtonGroup;

    invoke-virtual {v0, v4}, Lflipboard/gui/item/AdButtonGroup;->setVisibility(I)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_4
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->g:Lflipboard/gui/item/AdButtonGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/item/AdButtonGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->d:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 89
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 90
    invoke-virtual {p0}, Lflipboard/gui/item/AdItem;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0, p0}, Landroid/app/Activity;->registerForContextMenu(Landroid/view/View;)V

    .line 91
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AdItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/AdItem;->a:Lflipboard/gui/FLImageView;

    .line 92
    const v0, 0x7f0a01f0

    invoke-virtual {p0, v0}, Lflipboard/gui/item/AdItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/item/AdButtonGroup;

    iput-object v0, p0, Lflipboard/gui/item/AdItem;->g:Lflipboard/gui/item/AdButtonGroup;

    .line 93
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/item/AdItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02021f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 94
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->a:Lflipboard/gui/FLImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setAllowPreloadOnUIThread(Z)V

    .line 95
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/AdItem$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/AdItem$1;-><init>(Lflipboard/gui/item/AdItem;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 103
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 138
    .line 139
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v3

    .line 172
    :goto_1
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lflipboard/gui/FLRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v3, v2

    :cond_2
    return v3

    .line 141
    :pswitch_1
    iput-boolean v2, p0, Lflipboard/gui/item/AdItem;->b:Z

    move v0, v3

    .line 142
    goto :goto_1

    .line 144
    :pswitch_2
    iget-boolean v0, p0, Lflipboard/gui/item/AdItem;->b:Z

    if-eqz v0, :cond_0

    .line 145
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-nez v0, :cond_7

    .line 146
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->i:Ljava/util/Map;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/item/AdItem;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Ad$HotSpot;

    .line 147
    :goto_2
    if-eqz v0, :cond_7

    .line 148
    iget-boolean v1, v0, Lflipboard/objs/Ad$HotSpot;->i:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->l:Lflipboard/objs/Ad$VideoInfo;

    if-eqz v1, :cond_6

    .line 149
    iget-object v0, v0, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->k:Ljava/util/List;

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 151
    iget-object v0, p0, Lflipboard/gui/item/AdItem;->l:Lflipboard/objs/Ad$VideoInfo;

    iget-object v0, v0, Lflipboard/objs/Ad$VideoInfo;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 152
    invoke-virtual {p0}, Lflipboard/gui/item/AdItem;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->l:Lflipboard/objs/Ad$VideoInfo;

    invoke-static {v0, v1}, Lflipboard/util/VideoUtil;->a(Landroid/content/Context;Lflipboard/objs/Ad$VideoInfo;)V

    :cond_4
    move v0, v2

    .line 163
    :goto_3
    iput-boolean v3, p0, Lflipboard/gui/item/AdItem;->b:Z

    goto :goto_1

    .line 146
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 156
    :cond_6
    iget-object v1, v0, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 157
    invoke-virtual {p0}, Lflipboard/gui/item/AdItem;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v4, p0, Lflipboard/gui/item/AdItem;->h:Lflipboard/service/Section;

    iget-object v5, v0, Lflipboard/objs/Ad$HotSpot;->f:Ljava/lang/String;

    invoke-static {v1, v4, v5}, Lflipboard/service/FLAdManager;->a(Landroid/content/Context;Lflipboard/service/Section;Ljava/lang/String;)V

    .line 158
    iget-object v0, v0, Lflipboard/objs/Ad$HotSpot;->g:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/item/AdItem;->k:Ljava/util/List;

    invoke-static {v0, v1}, Lflipboard/service/FLAdManager;->a(Ljava/lang/String;Ljava/util/List;)V

    move v0, v2

    .line 159
    goto :goto_3

    .line 167
    :pswitch_3
    iget-boolean v0, p0, Lflipboard/gui/item/AdItem;->b:Z

    if-eqz v0, :cond_0

    .line 168
    iput-boolean v3, p0, Lflipboard/gui/item/AdItem;->b:Z

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto :goto_3

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public setVideoInfo(Lflipboard/objs/Ad$VideoInfo;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lflipboard/gui/item/AdItem;->l:Lflipboard/objs/Ad$VideoInfo;

    .line 85
    return-void
.end method
