.class Lflipboard/gui/item/CoverSectionItemTablet$6;
.super Ljava/lang/Object;
.source "CoverSectionItemTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/item/CoverSectionItemTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/CoverSectionItemTablet;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lflipboard/gui/item/CoverSectionItemTablet$6;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 542
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 543
    const-string v1, "fragment_type"

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 544
    const-string v1, "fragment_title"

    iget-object v2, p0, Lflipboard/gui/item/CoverSectionItemTablet$6;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v2}, Lflipboard/gui/item/CoverSectionItemTablet;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d01fd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    const-string v1, "feedItemId"

    iget-object v2, p0, Lflipboard/gui/item/CoverSectionItemTablet$6;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    iget-object v2, v2, Lflipboard/gui/item/CoverSectionItemTablet;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v2}, Lflipboard/objs/FeedItem;->ac()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-class v2, Lflipboard/activities/GenericFragmentActivity;

    invoke-virtual {v1, v2, v0}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 547
    return-void
.end method
