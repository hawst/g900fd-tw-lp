.class public Lflipboard/gui/item/VideoDetailTabletView;
.super Lflipboard/gui/FLRelativeLayout;
.source "VideoDetailTabletView.java"

# interfaces
.implements Lflipboard/gui/item/DetailView;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field private d:Lflipboard/gui/FLImageView;

.field private e:Lflipboard/objs/FeedItem;

.field private f:Lflipboard/gui/FLTextIntf;

.field private g:Lflipboard/gui/FLTextIntf;

.field private h:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "videodetail"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/item/VideoDetailTabletView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method static synthetic a(Lflipboard/gui/item/VideoDetailTabletView;)Lflipboard/gui/FLImageView;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    return-object v0
.end method


# virtual methods
.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->e:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 57
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoDetailTabletView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    .line 58
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    new-instance v1, Lflipboard/gui/item/VideoDetailTabletView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/VideoDetailTabletView$1;-><init>(Lflipboard/gui/item/VideoDetailTabletView;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setListener(Lflipboard/gui/FLImageView$Listener;)V

    .line 81
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setFade(Z)V

    .line 82
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setScaling(Z)V

    .line 83
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    sget-object v1, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 85
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoDetailTabletView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->f:Lflipboard/gui/FLTextIntf;

    .line 86
    const v0, 0x7f0a016d

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoDetailTabletView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->g:Lflipboard/gui/FLTextIntf;

    .line 87
    const v0, 0x7f0a016e

    invoke-virtual {p0, v0}, Lflipboard/gui/item/VideoDetailTabletView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->h:Landroid/view/View;

    .line 88
    return-void
.end method

.method public setImageUrl(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    iput-object p1, p0, Lflipboard/gui/item/VideoDetailTabletView;->b:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public setItem(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 38
    iput-object p1, p0, Lflipboard/gui/item/VideoDetailTabletView;->e:Lflipboard/objs/FeedItem;

    .line 39
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->b:Ljava/lang/String;

    .line 40
    if-eqz p1, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->c:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 42
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->f:Lflipboard/gui/FLTextIntf;

    iget-object v1, p0, Lflipboard/gui/item/VideoDetailTabletView;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 43
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 44
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    iget-object v0, p0, Lflipboard/gui/item/VideoDetailTabletView;->g:Lflipboard/gui/FLTextIntf;

    invoke-static {p1}, Lflipboard/gui/section/ItemDisplayUtil;->c(Lflipboard/objs/FeedItem;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 46
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
