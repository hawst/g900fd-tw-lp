.class public Lflipboard/gui/item/InterstitialView;
.super Landroid/view/ViewGroup;
.source "InterstitialView.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/ViewGroup;",
        "Lflipboard/gui/item/TabletItem;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lflipboard/gui/FLTextView;

.field public b:Lflipboard/gui/section/AttributionSmall;

.field public c:I

.field public d:I

.field public e:Lflipboard/service/Section;

.field public f:Lflipboard/gui/item/InterstitialView$Layout;

.field public g:Z

.field private h:I

.field private i:Lflipboard/gui/FLTextView;

.field private j:Lflipboard/gui/FLTextView;

.field private k:Lflipboard/gui/FLImageView;

.field private l:Landroid/widget/ImageView;

.field private m:Landroid/view/View;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:Lflipboard/gui/SocialFormatter;

.field private s:Lflipboard/objs/FeedItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 56
    sget-object v0, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/item/InterstitialView;->g:Z

    .line 65
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-direct {v0, p1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->r:Lflipboard/gui/SocialFormatter;

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    sget-object v0, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/item/InterstitialView;->g:Z

    .line 70
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-direct {v0, p1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->r:Lflipboard/gui/SocialFormatter;

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    sget-object v0, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/item/InterstitialView;->g:Z

    .line 75
    new-instance v0, Lflipboard/gui/SocialFormatter;

    invoke-direct {v0, p1}, Lflipboard/gui/SocialFormatter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->r:Lflipboard/gui/SocialFormatter;

    .line 76
    return-void
.end method

.method private static a(Landroid/view/View;II)I
    .locals 5

    .prologue
    .line 296
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 306
    :goto_0
    return p2

    .line 300
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 301
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v1, p2, v1

    .line 302
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 303
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, p1

    .line 304
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {p0, v3, v2, v4, v1}, Landroid/view/View;->layout(IIII)V

    .line 306
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int p2, v2, v0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;IIII)I
    .locals 5

    .prologue
    .line 261
    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 283
    :goto_0
    return p3

    .line 265
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 266
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    .line 268
    and-int/lit8 v1, p4, 0x7

    sparse-switch v1, :sswitch_data_0

    .line 277
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, p2

    .line 279
    :goto_1
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v3, p3

    .line 280
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v3

    .line 281
    add-int/2addr v2, v1

    invoke-virtual {p0, v1, v3, v2, v4}, Landroid/view/View;->layout(IIII)V

    .line 283
    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int p3, v4, v0

    goto :goto_0

    .line 270
    :sswitch_0
    sub-int v1, p1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    .line 271
    goto :goto_1

    .line 273
    :sswitch_1
    sub-int v1, p1, v2

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v1, v3

    .line 274
    goto :goto_1

    .line 268
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Landroid/view/View;IIIZ)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 213
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    .line 224
    :goto_0
    return v1

    .line 217
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 218
    if-eqz p5, :cond_1

    move v2, v1

    .line 219
    :goto_1
    if-eqz p5, :cond_2

    .line 220
    :goto_2
    add-int/lit8 v2, v2, 0x0

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    invoke-static {p2, v2, v3}, Lflipboard/gui/item/InterstitialView;->getChildMeasureSpec(III)I

    move-result v2

    .line 221
    add-int/2addr v1, p4

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    invoke-static {p3, v1, v3}, Lflipboard/gui/item/InterstitialView;->getChildMeasureSpec(III)I

    move-result v1

    .line 222
    invoke-virtual {p1, v2, v1}, Landroid/view/View;->measure(II)V

    .line 224
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v0

    goto :goto_0

    .line 218
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingRight()I

    move-result v3

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    goto :goto_1

    .line 219
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v3

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v3

    goto :goto_2
.end method

.method private a(Lflipboard/objs/FeedItem;I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 439
    iget-boolean v1, p1, Lflipboard/objs/FeedItem;->an:Z

    if-eqz v1, :cond_1

    move v1, v0

    :goto_0
    if-nez v1, :cond_7

    .line 441
    const/4 v0, -0x1

    .line 455
    :cond_0
    :goto_1
    return v0

    .line 439
    :cond_1
    iget v1, p1, Lflipboard/objs/FeedItem;->ak:I

    if-lez v1, :cond_2

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->g()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->i()Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v0

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/Image;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v0

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/objs/Image;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    move v1, v0

    goto :goto_0

    :cond_6
    const/4 v1, 0x1

    goto :goto_0

    .line 442
    :cond_7
    if-eqz p2, :cond_0

    .line 446
    invoke-virtual {p1}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v1

    .line 448
    iget v0, p0, Lflipboard/gui/item/InterstitialView;->h:I

    invoke-virtual {v1}, Lflipboard/objs/Image;->a()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 450
    div-int/lit8 v2, p2, 0x14

    rsub-int/lit8 v2, v2, 0x1

    mul-int/2addr v0, v2

    .line 451
    invoke-virtual {v1}, Lflipboard/objs/Image;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 453
    div-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method

.method static synthetic a(Lflipboard/gui/item/InterstitialView;Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lflipboard/gui/item/InterstitialView;->a(Lflipboard/service/Section;)V

    return-void
.end method

.method private a(Lflipboard/service/Section;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/16 v11, 0x14

    const/16 v10, 0x8

    const/4 v5, 0x0

    .line 341
    if-eqz p1, :cond_0

    .line 342
    iget-object v7, p1, Lflipboard/service/Section;->u:Ljava/util/List;

    .line 343
    if-nez v7, :cond_1

    .line 345
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "InterstitialView:updateItem"

    new-instance v2, Lflipboard/gui/item/InterstitialView$2;

    invoke-direct {v2, p0, p1}, Lflipboard/gui/item/InterstitialView$2;-><init>(Lflipboard/gui/item/InterstitialView;Lflipboard/service/Section;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 352
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    const/4 v2, -0x1

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v1, v5

    move-object v3, v6

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    if-ge v1, v11, :cond_5

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v4, v1

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    if-ge v4, v11, :cond_2

    invoke-direct {p0, v0, v4}, Lflipboard/gui/item/InterstitialView;->a(Lflipboard/objs/FeedItem;I)I

    move-result v1

    if-le v1, v2, :cond_12

    move v12, v1

    move-object v1, v0

    move v0, v12

    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v3, v1

    move v2, v0

    goto :goto_2

    :cond_2
    move v1, v4

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0, v1}, Lflipboard/gui/item/InterstitialView;->a(Lflipboard/objs/FeedItem;I)I

    move-result v4

    if-le v4, v2, :cond_4

    move v2, v4

    move-object v3, v0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    if-nez v3, :cond_6

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    move-object v3, v0

    .line 355
    :cond_6
    invoke-virtual {p0, v3}, Lflipboard/gui/item/InterstitialView;->setTag(Ljava/lang/Object;)V

    .line 356
    iput-object v3, p0, Lflipboard/gui/item/InterstitialView;->s:Lflipboard/objs/FeedItem;

    .line 358
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->i()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 359
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->e()Lflipboard/objs/Image;

    move-result-object v0

    .line 360
    if-eqz v0, :cond_8

    .line 361
    iget-object v1, v0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lflipboard/objs/Image;->h:Ljava/lang/String;

    const-string v2, "nocrop"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 362
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    sget-object v2, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 364
    :cond_7
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    iget v2, v0, Lflipboard/objs/Image;->f:I

    iget v0, v0, Lflipboard/objs/Image;->g:I

    invoke-virtual {v1, v2, v0}, Lflipboard/gui/FLImageView;->a(II)V

    .line 365
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/FeedItem;)V

    .line 367
    :cond_8
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 372
    :goto_4
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->j:Lflipboard/gui/FLTextView;

    iget-object v2, p0, Lflipboard/gui/item/InterstitialView;->r:Lflipboard/gui/SocialFormatter;

    if-eqz v7, :cond_9

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_c

    :cond_9
    const-string v0, ""

    :goto_5
    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 374
    if-eqz v3, :cond_a

    .line 375
    invoke-virtual {v3}, Lflipboard/objs/FeedItem;->I()Ljava/lang/String;

    move-result-object v6

    .line 377
    :cond_a
    if-eqz v6, :cond_11

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_11

    .line 378
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v6}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v5}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 384
    :goto_6
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->b:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0, p1, v3}, Lflipboard/gui/section/AttributionSmall;->a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    goto/16 :goto_0

    .line 369
    :cond_b
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_4

    .line 372
    :cond_c
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v8, Ljava/util/HashSet;

    invoke-direct {v8}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_d
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->O()Z

    move-result v9

    if-eqz v9, :cond_10

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aE:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_e
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    invoke-virtual {v2, v0, v4, v8}, Lflipboard/gui/SocialFormatter;->a(Lflipboard/objs/FeedItem;Ljava/lang/StringBuilder;Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_f
    :goto_7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_10
    invoke-virtual {v2, v0, v4, v8}, Lflipboard/gui/SocialFormatter;->a(Lflipboard/objs/FeedItem;Ljava/lang/StringBuilder;Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_d

    goto :goto_7

    .line 381
    :cond_11
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v10}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    goto :goto_6

    :cond_12
    move v0, v2

    move-object v1, v3

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 32
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/InterstitialView$1;

    invoke-direct {v1, p0, p3, p1}, Lflipboard/gui/item/InterstitialView$1;-><init>(Lflipboard/gui/item/InterstitialView;Ljava/lang/Object;Lflipboard/service/Section;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 145
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 160
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 155
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->s:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 311
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 312
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->e:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->e:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 314
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->e:Lflipboard/service/Section;

    invoke-direct {p0, v0}, Lflipboard/gui/item/InterstitialView;->a(Lflipboard/service/Section;)V

    .line 316
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 320
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 321
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->e:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->e:Lflipboard/service/Section;

    invoke-virtual {v0, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 324
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const v2, 0x7f080050

    .line 80
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    .line 81
    const v0, 0x7f0a0066

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->l:Landroid/widget/ImageView;

    .line 82
    const v0, 0x7f0a01fc

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    .line 83
    const v0, 0x7f0a01fd

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->i:Lflipboard/gui/FLTextView;

    .line 84
    const v0, 0x7f0a01fe

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->j:Lflipboard/gui/FLTextView;

    .line 85
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/section/AttributionSmall;

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->b:Lflipboard/gui/section/AttributionSmall;

    .line 86
    const v0, 0x7f0a0065

    invoke-virtual {p0, v0}, Lflipboard/gui/item/InterstitialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/InterstitialView;->m:Landroid/view/View;

    .line 88
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 89
    const v1, 0x7f08004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/InterstitialView;->n:I

    .line 90
    const v1, 0x7f08004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/InterstitialView;->c:I

    .line 91
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/InterstitialView;->o:I

    .line 92
    const v1, 0x7f080051

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/InterstitialView;->d:I

    .line 93
    const v1, 0x7f08007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lflipboard/gui/item/InterstitialView;->p:I

    .line 94
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/item/InterstitialView;->q:I

    .line 96
    invoke-static {}, Lflipboard/util/AndroidUtil;->e()I

    move-result v0

    invoke-static {}, Lflipboard/util/AndroidUtil;->d()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/item/InterstitialView;->h:I

    .line 97
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/4 v1, 0x0

    .line 229
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingLeft()I

    move-result v2

    .line 230
    sub-int v3, p4, p2

    .line 231
    sub-int v0, v3, v2

    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    .line 233
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingTop()I

    move-result v4

    .line 234
    iget-object v5, p0, Lflipboard/gui/item/InterstitialView;->l:Landroid/widget/ImageView;

    const/4 v6, 0x3

    invoke-static {v5, v0, v2, v4, v6}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIII)I

    move-result v5

    .line 235
    iget-object v6, p0, Lflipboard/gui/item/InterstitialView;->i:Lflipboard/gui/FLTextView;

    invoke-static {v6, v0, v2, v4, v7}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIII)I

    move-result v4

    .line 236
    iget-object v6, p0, Lflipboard/gui/item/InterstitialView;->j:Lflipboard/gui/FLTextView;

    invoke-static {v6, v0, v2, v4, v7}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIII)I

    move-result v0

    .line 238
    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 240
    sub-int v4, p5, p3

    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    .line 241
    iget-object v5, p0, Lflipboard/gui/item/InterstitialView;->b:Lflipboard/gui/section/AttributionSmall;

    invoke-static {v5, v2, v4}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;II)I

    move-result v4

    .line 242
    iget-object v5, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    invoke-static {v5, v2, v4}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;II)I

    .line 245
    iget-object v2, p0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    sget-object v4, Lflipboard/gui/item/InterstitialView$Layout;->a:Lflipboard/gui/item/InterstitialView$Layout;

    if-ne v2, v4, :cond_0

    move v0, v1

    .line 246
    :cond_0
    iget-object v2, p0, Lflipboard/gui/item/InterstitialView;->m:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/item/InterstitialView;->m:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, p5, v4

    invoke-virtual {v2, v1, v4, v3, p5}, Landroid/view/View;->layout(IIII)V

    .line 248
    iget-object v2, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v1, v0, v3, p5}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 249
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 165
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, Lflipboard/gui/item/InterstitialView;->getDefaultSize(II)I

    move-result v11

    .line 166
    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v0, p2}, Lflipboard/gui/item/InterstitialView;->getDefaultSize(II)I

    move-result v0

    .line 167
    invoke-virtual {p0, v11, v0}, Lflipboard/gui/item/InterstitialView;->setMeasuredDimension(II)V

    .line 169
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->l:Landroid/widget/ImageView;

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIIZ)I

    .line 175
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->i:Lflipboard/gui/FLTextView;

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIIZ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 176
    iget-object v6, p0, Lflipboard/gui/item/InterstitialView;->j:Lflipboard/gui/FLTextView;

    add-int/lit8 v9, v0, 0x0

    move-object v5, p0

    move v7, p1

    move v8, p2

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIIZ)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    iget-object v6, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    add-int/lit8 v9, v0, 0x0

    move-object v5, p0

    move v7, p1

    move v8, p2

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIIZ)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 178
    iget-object v6, p0, Lflipboard/gui/item/InterstitialView;->b:Lflipboard/gui/section/AttributionSmall;

    add-int v9, v0, v1

    move-object v5, p0

    move v7, p1

    move v8, p2

    move v10, v4

    invoke-direct/range {v5 .. v10}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIIZ)I

    move-result v2

    add-int/2addr v1, v2

    .line 180
    iget-object v2, p0, Lflipboard/gui/item/InterstitialView;->l:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/item/InterstitialView;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 183
    sget-object v2, Lflipboard/gui/item/InterstitialView$3;->a:[I

    iget-object v3, p0, Lflipboard/gui/item/InterstitialView;->f:Lflipboard/gui/item/InterstitialView$Layout;

    invoke-virtual {v3}, Lflipboard/gui/item/InterstitialView$Layout;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 195
    :goto_0
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    const/4 v5, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/item/InterstitialView;->a(Landroid/view/View;IIIZ)I

    .line 197
    iget-object v0, p0, Lflipboard/gui/item/InterstitialView;->b:Lflipboard/gui/section/AttributionSmall;

    invoke-virtual {v0}, Lflipboard/gui/section/AttributionSmall;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLTextView;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->k:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 199
    iget-object v1, p0, Lflipboard/gui/item/InterstitialView;->m:Landroid/view/View;

    invoke-static {v11, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->measure(II)V

    .line 200
    return-void

    .line 185
    :pswitch_0
    add-int v4, v0, v1

    .line 186
    goto :goto_0

    :pswitch_1
    move v4, v0

    .line 188
    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
