.class public Lflipboard/gui/item/SectionItemCoverImage;
.super Lflipboard/gui/FLRelativeLayout;
.source "SectionItemCoverImage.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# instance fields
.field a:Lflipboard/service/Section;

.field b:Lflipboard/objs/FeedItem;

.field c:Landroid/view/ViewGroup;

.field d:Lflipboard/gui/FLImageView;

.field e:Lflipboard/gui/FLImageView;

.field f:Lflipboard/gui/FLTextIntf;

.field g:Lflipboard/gui/FLTextIntf;

.field h:Lflipboard/gui/FLTextIntf;

.field i:Landroid/view/ViewGroup$LayoutParams;

.field j:Z

.field k:Landroid/graphics/drawable/Drawable;

.field l:Landroid/view/View$OnClickListener;

.field m:Lflipboard/gui/FollowButton;

.field private o:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/item/SectionItemCoverImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/item/SectionItemCoverImage;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 73
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 74
    const v1, 0x7f0300ba

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 76
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 226
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 229
    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->h:Lflipboard/gui/FLTextIntf;

    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v1, v0}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 233
    :cond_0
    return-void
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/item/SectionItemCoverImage$1;

    invoke-direct {v1, p0}, Lflipboard/gui/item/SectionItemCoverImage$1;-><init>(Lflipboard/gui/item/SectionItemCoverImage;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 119
    return-void
.end method

.method public final a(ZI)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 237
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "SectionItemCoverImage:onPageOffsetChange"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 238
    invoke-static {p1, p2}, Lflipboard/util/AndroidUtil;->a(ZI)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    iget-boolean v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->j:Z

    if-eqz v0, :cond_0

    .line 240
    iput-boolean v3, p0, Lflipboard/gui/item/SectionItemCoverImage;->j:Z

    .line 241
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->setImageOnView(Lflipboard/objs/FeedItem;)V

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    iget-boolean v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->j:Z

    if-nez v0, :cond_0

    .line 245
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->j:Z

    .line 246
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 247
    new-instance v0, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    .line 248
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 249
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 250
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 251
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->i:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1, v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 91
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 94
    const v0, 0x7f0a0230

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    .line 96
    const v0, 0x7f0a0040

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    .line 97
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->i:Landroid/view/ViewGroup$LayoutParams;

    .line 98
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->k:Landroid/graphics/drawable/Drawable;

    .line 99
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLImageView;->getForeground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->o:Landroid/graphics/drawable/Drawable;

    .line 100
    const v0, 0x7f0a006b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->e:Lflipboard/gui/FLImageView;

    .line 101
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->f:Lflipboard/gui/FLTextIntf;

    .line 102
    const v0, 0x7f0a0233

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->g:Lflipboard/gui/FLTextIntf;

    .line 103
    const v0, 0x7f0a008b

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->h:Lflipboard/gui/FLTextIntf;

    .line 104
    const v0, 0x7f0a01a3

    invoke-virtual {p0, v0}, Lflipboard/gui/item/SectionItemCoverImage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FollowButton;

    iput-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->m:Lflipboard/gui/FollowButton;

    .line 105
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->m:Lflipboard/gui/FollowButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setInverted(Z)V

    .line 106
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 142
    invoke-super/range {p0 .. p5}, Lflipboard/gui/FLRelativeLayout;->onLayout(ZIIII)V

    .line 144
    sub-int v0, p4, p2

    .line 145
    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 148
    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getPaddingTop()I

    move-result v1

    .line 149
    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v0

    .line 150
    iget-object v3, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v1

    .line 152
    iget-object v4, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/view/ViewGroup;->layout(IIII)V

    .line 154
    return-void
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x0

    .line 123
    invoke-super {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;->onMeasure(II)V

    .line 125
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 126
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 128
    int-to-float v2, v0

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v2, v3

    .line 129
    const/high16 v3, 0x438c0000    # 280.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 130
    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->f:Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900f2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-interface {v2, v5, v3}, Lflipboard/gui/FLTextIntf;->a(II)V

    .line 135
    :goto_0
    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->c:Landroid/view/ViewGroup;

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/view/ViewGroup;->measure(II)V

    .line 137
    return-void

    .line 132
    :cond_0
    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->f:Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/gui/item/SectionItemCoverImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900f3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-interface {v2, v5, v3}, Lflipboard/gui/FLTextIntf;->a(II)V

    .line 133
    iget-object v2, p0, Lflipboard/gui/item/SectionItemCoverImage;->e:Lflipboard/gui/FLImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method setImageOnView(Lflipboard/objs/FeedItem;)V
    .locals 2

    .prologue
    .line 258
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->J()Lflipboard/objs/Image;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    iget-object v1, p0, Lflipboard/gui/item/SectionItemCoverImage;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 284
    :goto_0
    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lflipboard/gui/item/SectionItemCoverImage;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->j:Lflipboard/objs/FeedSection;

    iget-object v0, v0, Lflipboard/objs/FeedSection;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lflipboard/gui/item/SectionItemCoverImage$3;

    invoke-direct {v1, p0}, Lflipboard/gui/item/SectionItemCoverImage$3;-><init>(Lflipboard/gui/item/SectionItemCoverImage;)V

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Ljava/util/List;Lflipboard/service/Flap$TypedResultObserver;)V

    goto :goto_0
.end method
