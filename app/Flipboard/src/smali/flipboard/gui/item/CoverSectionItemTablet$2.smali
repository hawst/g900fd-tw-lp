.class Lflipboard/gui/item/CoverSectionItemTablet$2;
.super Ljava/lang/Object;
.source "CoverSectionItemTablet.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/item/CoverSectionItemTablet;


# direct methods
.method constructor <init>(Lflipboard/gui/item/CoverSectionItemTablet;)V
    .locals 0

    .prologue
    .line 150
    iput-object p1, p0, Lflipboard/gui/item/CoverSectionItemTablet$2;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 152
    .line 153
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$2;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$2;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$2;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getItem()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    .line 157
    :goto_0
    if-eqz v0, :cond_1

    .line 158
    new-instance v2, Lflipboard/gui/section/MagazineLikesDialog;

    invoke-direct {v2}, Lflipboard/gui/section/MagazineLikesDialog;-><init>()V

    .line 159
    iput-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog;->j:Lflipboard/objs/FeedItem;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog;->l:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lflipboard/gui/section/MagazineLikesDialog;->m:Z

    iput-object v1, v2, Lflipboard/gui/section/MagazineLikesDialog;->n:Ljava/lang/String;

    iget-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog;->k:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    if-eqz v0, :cond_0

    iget-object v0, v2, Lflipboard/gui/section/MagazineLikesDialog;->k:Lflipboard/gui/section/MagazineLikesDialog$Adapter;

    invoke-virtual {v0}, Lflipboard/gui/section/MagazineLikesDialog$Adapter;->notifyDataSetChanged()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, v2, Lflipboard/gui/section/MagazineLikesDialog;->o:Z

    invoke-virtual {v2}, Lflipboard/gui/section/MagazineLikesDialog;->e()V

    .line 160
    iget-object v0, p0, Lflipboard/gui/item/CoverSectionItemTablet$2;->a:Lflipboard/gui/item/CoverSectionItemTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/CoverSectionItemTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 161
    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "edit_contributors"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/section/MagazineLikesDialog;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 165
    :goto_1
    return-void

    .line 163
    :cond_1
    const-string v0, "unwanted.magazine_like_count_tapped_with_no_item"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
