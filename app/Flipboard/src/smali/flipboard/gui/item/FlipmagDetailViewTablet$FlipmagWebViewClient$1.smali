.class Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;
.super Ljava/lang/Object;
.source "FlipmagDetailViewTablet.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/audio/FLAudioManager;",
        "Lflipboard/service/audio/FLAudioManager$AudioMessage;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;


# direct methods
.method constructor <init>(Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 929
    iput-object p1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iput-object p2, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 929
    check-cast p2, Lflipboard/service/audio/FLAudioManager$AudioMessage;

    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet$7;->b:[I

    invoke-virtual {p2}, Lflipboard/service/audio/FLAudioManager$AudioMessage;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {}, Lflipboard/gui/item/FlipmagDetailViewTablet;->c()V

    return-void

    :pswitch_0
    check-cast p3, Lflipboard/service/audio/FLMediaPlayer$PlayerState;

    sget-object v0, Lflipboard/util/FLWebViewClient;->b:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    aput-object p3, v0, v4

    sget-object v0, Lflipboard/gui/item/FlipmagDetailViewTablet$7;->a:[I

    invoke-virtual {p3}, Lflipboard/service/audio/FLMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    sget-object v0, Lflipboard/util/FLWebViewClient;->b:Lflipboard/util/Log;

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLAudioBufferingDidStart(\"%s\");"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLAudioPlaybackDidStart(\"%s\");"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLAudioPlaybackDidPause(\"%s\");"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-static {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->a(Lflipboard/gui/item/FlipmagDetailViewTablet;)Lflipboard/gui/FLWebView;

    move-result-object v0

    const-string v1, "javascript:FLAudioPlaybackDidFinish(\"%s\");"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->a:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v0, v0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v0}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iget-boolean v1, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v1, :cond_0

    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0041

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient$1;->b:Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;

    iget-object v1, v1, Lflipboard/gui/item/FlipmagDetailViewTablet$FlipmagWebViewClient;->a:Lflipboard/gui/item/FlipmagDetailViewTablet;

    invoke-virtual {v1}, Lflipboard/gui/item/FlipmagDetailViewTablet;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0042

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
