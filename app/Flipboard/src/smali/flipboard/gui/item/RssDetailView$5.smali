.class Lflipboard/gui/item/RssDetailView$5;
.super Landroid/os/AsyncTask;
.source "RssDetailView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/StringBuilder;",
        "Ljava/lang/StringBuilder;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:[B

.field final synthetic b:Ljava/lang/StringBuilder;

.field final synthetic c:Lflipboard/gui/item/RssDetailView;


# direct methods
.method constructor <init>(Lflipboard/gui/item/RssDetailView;[BLjava/lang/StringBuilder;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    iput-object p2, p0, Lflipboard/gui/item/RssDetailView$5;->a:[B

    iput-object p3, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/StringBuilder;
    .locals 5

    .prologue
    .line 365
    const/4 v1, 0x0

    .line 368
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/item/RssDetailView$5;->a:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    invoke-static {v0, v2}, Lflipboard/gui/item/RssDetailView;->b(Lflipboard/gui/item/RssDetailView;Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 372
    :try_start_1
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    const-string v2, "window.flipboardShowImageEnabled = true;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 373
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    const-string v2, "window.flipboardFormFactor = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->d(Lflipboard/gui/item/RssDetailView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 375
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    const-string v2, "\'tablet\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    :goto_0
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 384
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 385
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v1, v2}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 388
    :cond_0
    :goto_1
    return-object v0

    .line 377
    :cond_1
    :try_start_2
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    const-string v2, "\'phone\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 380
    :catch_0
    move-exception v1

    :goto_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 384
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 385
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v1, v2}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    goto :goto_1

    .line 384
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 385
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v1}, Lflipboard/gui/item/RssDetailView;->e(Lflipboard/gui/item/RssDetailView;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v1, v2}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    :cond_2
    throw v0

    .line 380
    :catch_1
    move-exception v0

    move-object v4, v0

    move-object v0, v1

    move-object v1, v4

    goto :goto_2
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 360
    invoke-direct {p0}, Lflipboard/gui/item/RssDetailView$5;->a()Ljava/lang/StringBuilder;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 360
    check-cast p1, Ljava/lang/StringBuilder;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    iget-object v0, v0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    iget-object v0, v0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    iget-object v1, v1, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    iget-object v0, v0, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v1, v0, Lflipboard/objs/FeedItem;->B:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->f(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/FLWebView;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$5;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLWebView;->loadUrl(Ljava/lang/String;)V

    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$5;->c:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->f(Lflipboard/gui/item/RssDetailView;)Lflipboard/gui/FLWebView;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "text/html"

    const-string v4, "utf-8"

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/FLWebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move-object v1, v5

    goto :goto_0
.end method
