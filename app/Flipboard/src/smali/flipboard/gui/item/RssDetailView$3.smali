.class Lflipboard/gui/item/RssDetailView$3;
.super Lflipboard/util/FLWebViewClient;
.source "RssDetailView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/item/RssDetailView;


# direct methods
.method constructor <init>(Lflipboard/gui/item/RssDetailView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-direct {p0, p2}, Lflipboard/util/FLWebViewClient;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Landroid/net/Uri;Landroid/webkit/WebView;)Z
    .locals 8

    .prologue
    const v7, 0x7f040004

    const v6, 0x7f040001

    const/4 v5, 0x1

    .line 221
    invoke-super {p0, p1, p2, p3}, Lflipboard/util/FLWebViewClient;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/webkit/WebView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    :goto_0
    return v5

    .line 225
    :cond_0
    const-string v0, "//showHTML"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 226
    const-string v0, "%3Cimg"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 227
    const-string v0, "src%3D%22"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x9

    const-string v1, "%22%20"

    const-string v2, "src%3D%22"

    invoke-virtual {p1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v1}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v3}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "detail_image_url"

    invoke-static {v0}, Lflipboard/util/HttpUtil;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 229
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v6, v7}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 247
    :cond_1
    :goto_1
    const-string v0, "javascript:FLBridge.setReady(true);"

    invoke-virtual {p3, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_2
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v2}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "flipmag_show_html"

    const-string v3, "//showHTML?url="

    const-string v4, ""

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 234
    :cond_3
    const-string v0, "//showImage"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    if-eqz p2, :cond_4

    .line 238
    const-string v0, "url"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_2
    sget-object v1, Lflipboard/gui/item/RssDetailView$3;->b:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    aput-object v0, v1, v5

    .line 243
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v1}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v3}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lflipboard/activities/DetailActivityStayOnRotation;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "detail_image_url"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 244
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v6, v7}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    goto :goto_1

    .line 240
    :cond_4
    const-string v0, "//showImage?url="

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method protected final a(Ljava/lang/String;Landroid/webkit/WebView;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 254
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->a(Ljava/lang/String;Landroid/webkit/WebView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    :goto_0
    return v1

    .line 257
    :cond_0
    const-string v0, "javascript:FLBridge.setReady(true);"

    invoke-virtual {p2, v0}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 216
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 209
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->a(Lflipboard/gui/item/RssDetailView;)V

    .line 210
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lflipboard/gui/item/RssDetailView;->a(Lflipboard/gui/item/RssDetailView;J)J

    .line 211
    sget-object v0, Lflipboard/gui/item/RssDetailView$3;->b:Lflipboard/util/Log;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v2}, Lflipboard/gui/item/RssDetailView;->b(Lflipboard/gui/item/RssDetailView;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 212
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 203
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-static {v0}, Lflipboard/gui/item/RssDetailView;->a(Lflipboard/gui/item/RssDetailView;)V

    .line 204
    sget-object v0, Lflipboard/gui/item/RssDetailView$3;->b:Lflipboard/util/Log;

    const-string v1, "error received %s - %s - %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    const/4 v3, 0x2

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 183
    invoke-super {p0, p1, p2}, Lflipboard/util/FLWebViewClient;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    :goto_0
    return v4

    .line 188
    :cond_0
    const-string v0, "/%23"

    const-string v1, "/#"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 192
    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$3;->c:Landroid/content/Context;

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    iget-object v0, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v0}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 195
    :cond_1
    iget-object v1, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    invoke-virtual {v1}, Lflipboard/gui/item/RssDetailView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/item/RssDetailView$3;->a:Lflipboard/gui/item/RssDetailView;

    iget-object v2, v2, Lflipboard/gui/item/RssDetailView;->o:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
