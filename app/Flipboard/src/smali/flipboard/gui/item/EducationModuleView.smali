.class public Lflipboard/gui/item/EducationModuleView;
.super Landroid/widget/FrameLayout;
.source "EducationModuleView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lflipboard/gui/item/PageboxView;


# instance fields
.field protected a:Lflipboard/gui/FLTextView;

.field protected b:Lflipboard/gui/FLTextView;

.field protected c:Lflipboard/gui/FLImageView;

.field protected d:Lflipboard/gui/FLButton;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 103
    iget-object v0, p0, Lflipboard/gui/item/EducationModuleView;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 104
    iget-object v2, p0, Lflipboard/gui/item/EducationModuleView;->e:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 121
    :cond_1
    :goto_1
    return-void

    .line 104
    :sswitch_0
    const-string v3, "pageboxCreateAccount"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "pageboxAddService"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "pageboxFindFriends"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 106
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lflipboard/activities/CreateAccountActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 107
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 110
    :pswitch_1
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0021

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 111
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lflipboard/activities/GenericFragmentActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 115
    :pswitch_2
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v1, v0, v2}, Lflipboard/activities/GenericFragmentActivity;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 117
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 104
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4c18dce0 -> :sswitch_2
        -0x32a239eb -> :sswitch_0
        -0x2b6b8b30 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 54
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Landroid/view/View;)V

    .line 55
    iget-object v0, p0, Lflipboard/gui/item/EducationModuleView;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lflipboard/gui/item/EducationModuleView;->d:Lflipboard/gui/FLButton;

    invoke-virtual {v0, p0}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method

.method public setPagebox(Lflipboard/objs/SidebarGroup;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-virtual {p1}, Lflipboard/objs/SidebarGroup;->a()Lflipboard/objs/SidebarGroup$RenderHints;

    move-result-object v3

    .line 62
    iget-object v0, v3, Lflipboard/objs/SidebarGroup$RenderHints;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/item/EducationModuleView;->e:Ljava/lang/String;

    .line 63
    invoke-virtual {p0}, Lflipboard/gui/item/EducationModuleView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 66
    iget-object v2, p0, Lflipboard/gui/item/EducationModuleView;->e:Ljava/lang/String;

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 85
    :goto_1
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    iget-object v2, p0, Lflipboard/gui/item/EducationModuleView;->a:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v1, p0, Lflipboard/gui/item/EducationModuleView;->d:Lflipboard/gui/FLButton;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/item/EducationModuleView;->c:Lflipboard/gui/FLImageView;

    iget-object v1, v3, Lflipboard/objs/SidebarGroup$RenderHints;->i:Lflipboard/objs/Image;

    invoke-virtual {v0, v1}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 90
    return-void

    .line 66
    :sswitch_0
    const-string v5, "pageboxCreateAccount"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v5, "pageboxAddService"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v5, "pageboxFindFriends"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 68
    :pswitch_0
    const v2, 0x7f0d00d7

    .line 69
    const v0, 0x7f0d00d6

    .line 71
    iget-object v5, p0, Lflipboard/gui/item/EducationModuleView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v5, v1}, Lflipboard/gui/FLTextView;->setVisibility(I)V

    .line 72
    const v1, 0x7f0d00d5

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 74
    iget-object v5, p0, Lflipboard/gui/item/EducationModuleView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v5, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 75
    goto :goto_1

    .line 77
    :pswitch_1
    const v1, 0x7f0d00d3

    .line 78
    const v0, 0x7f0d00d4

    .line 79
    goto :goto_1

    .line 81
    :pswitch_2
    const v1, 0x7f0d00d8

    .line 82
    const v0, 0x7f0d00d9

    goto :goto_1

    .line 66
    :sswitch_data_0
    .sparse-switch
        -0x4c18dce0 -> :sswitch_2
        -0x32a239eb -> :sswitch_0
        -0x2b6b8b30 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
