.class public Lflipboard/gui/NoContentView;
.super Landroid/widget/LinearLayout;
.source "NoContentView.java"


# instance fields
.field private a:Lflipboard/service/Section;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Lflipboard/gui/FLBusyView;

.field private e:Lflipboard/gui/FLButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method


# virtual methods
.method public setLoading(Z)V
    .locals 6

    .prologue
    const v5, 0x7f09007c

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 49
    const v0, 0x7f0a0289

    invoke-virtual {p0, v0}, Lflipboard/gui/NoContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    .line 50
    const v0, 0x7f0a028a

    invoke-virtual {p0, v0}, Lflipboard/gui/NoContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    .line 51
    const v0, 0x7f0a0288

    invoke-virtual {p0, v0}, Lflipboard/gui/NoContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    .line 52
    const v0, 0x7f0a028b

    invoke-virtual {p0, v0}, Lflipboard/gui/NoContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    .line 54
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 55
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0218

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 56
    iget-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    if-eqz p1, :cond_3

    .line 60
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    const v1, 0x7f0d01e4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 61
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 62
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 64
    :cond_2
    iget-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    goto :goto_0

    .line 68
    :cond_3
    iget-object v0, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->o:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->o:Ljava/lang/String;

    const-string v1, "sharedWithYou"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 69
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/NoContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090138

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 70
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0226

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 71
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 72
    invoke-virtual {p0}, Lflipboard/gui/NoContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/NoContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 73
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    if-eqz v0, :cond_4

    .line 74
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 108
    :cond_4
    :goto_1
    iget-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lflipboard/gui/NoContentView;->d:Lflipboard/gui/FLBusyView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLBusyView;->setVisibility(I)V

    goto :goto_0

    .line 76
    :cond_5
    iget-object v0, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->o:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->o:Ljava/lang/String;

    const-string v1, "flipsByFriends"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 77
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0135

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 78
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_6

    .line 79
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_6
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    if-eqz v0, :cond_4

    .line 82
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v4}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    const v1, 0x7f0d00ef

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(I)V

    .line 84
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    new-instance v1, Lflipboard/gui/NoContentView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/NoContentView$1;-><init>(Lflipboard/gui/NoContentView;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 91
    :cond_7
    iget-object v0, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    iget-object v0, v0, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v0, v0, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 92
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    iget-object v1, v1, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v1, v1, Lflipboard/service/Section$Meta;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    if-eqz v0, :cond_8

    .line 94
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 96
    :cond_8
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 97
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 100
    :cond_9
    iget-object v0, p0, Lflipboard/gui/NoContentView;->b:Landroid/widget/TextView;

    const v1, 0x7f0d0327

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 101
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    if-eqz v0, :cond_a

    .line 102
    iget-object v0, p0, Lflipboard/gui/NoContentView;->e:Lflipboard/gui/FLButton;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 104
    :cond_a
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    .line 105
    iget-object v0, p0, Lflipboard/gui/NoContentView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public setSection(Lflipboard/service/Section;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lflipboard/gui/NoContentView;->a:Lflipboard/service/Section;

    .line 45
    return-void
.end method
