.class public Lflipboard/gui/FLToggleButton;
.super Landroid/widget/ToggleButton;
.source "FLToggleButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/FLToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const-string v0, ""

    invoke-virtual {p0, v0}, Lflipboard/gui/FLToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    .line 39
    const-string v0, ""

    invoke-virtual {p0, v0}, Lflipboard/gui/FLToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    .line 40
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 4

    .prologue
    .line 44
    .line 45
    invoke-virtual {p0}, Landroid/widget/ToggleButton;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 46
    const-class v1, Landroid/view/View;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {p0}, Lflipboard/gui/FLToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 48
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/FLToggleButton$1;

    invoke-direct {v3, p0, v0, p0, v1}, Lflipboard/gui/FLToggleButton$1;-><init>(Lflipboard/gui/FLToggleButton;Landroid/view/ViewParent;Landroid/widget/ToggleButton;I)V

    invoke-virtual {v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/Runnable;)V

    .line 65
    :cond_0
    return-void
.end method
