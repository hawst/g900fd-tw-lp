.class Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;
.super Landroid/graphics/drawable/ShapeDrawable;
.source "FLBusyView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/FLBusyView;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/RectF;

.field private e:F


# direct methods
.method public constructor <init>(Lflipboard/gui/FLBusyView;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 33
    iput-object p1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->a:Lflipboard/gui/FLBusyView;

    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    .line 34
    invoke-virtual {p1}, Lflipboard/gui/FLBusyView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 36
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->b:Landroid/graphics/Paint;

    .line 37
    iget-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 38
    iget-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->b:Landroid/graphics/Paint;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 39
    iget-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 41
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->c:Landroid/graphics/Paint;

    .line 42
    iget-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 43
    iget-object v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 45
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    .line 47
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    const-wide v2, 0x3fb70a3d70a3d70aL    # 0.09

    mul-double/2addr v0, v2

    const-wide v2, 0x3f9eb851eb851eb8L    # 0.03

    add-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->e:F

    .line 48
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    int-to-float v1, v1

    .line 57
    const/high16 v2, 0x40800000    # 4.0f

    sub-float/2addr v1, v2

    .line 59
    iget-object v2, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    div-float v4, v1, v5

    sub-float/2addr v3, v4

    iput v3, v2, Landroid/graphics/RectF;->top:F

    .line 60
    iget-object v2, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v5

    div-float v3, v1, v5

    sub-float/2addr v0, v3

    iput v0, v2, Landroid/graphics/RectF;->left:F

    .line 61
    iget-object v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    iget-object v2, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v1

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    .line 62
    iget-object v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    iget-object v2, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 66
    iget-object v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    iget-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 70
    iget v0, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->e:F

    invoke-virtual {p0}, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->getLevel()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x461c4000    # 10000.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->e:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 71
    const/high16 v1, 0x43b40000    # 360.0f

    mul-float v3, v0, v1

    .line 73
    iget-object v1, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->d:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    const/4 v4, 0x1

    iget-object v5, p0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;->c:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 74
    return-void
.end method
