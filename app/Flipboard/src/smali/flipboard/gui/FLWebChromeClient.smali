.class public Lflipboard/gui/FLWebChromeClient;
.super Landroid/webkit/WebChromeClient;
.source "FLWebChromeClient.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z
    .locals 4

    .prologue
    .line 21
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/webkit/WebView$WebViewTransport;

    .line 22
    new-instance v1, Lflipboard/gui/FLWebView;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lflipboard/gui/FLWebView;-><init>(Landroid/content/Context;)V

    .line 23
    new-instance v2, Lflipboard/util/FLWebViewClient;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lflipboard/util/FLWebViewClient;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lflipboard/gui/FLWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 24
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$WebViewTransport;->setWebView(Landroid/webkit/WebView;)V

    .line 25
    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    .line 26
    const/4 v0, 0x1

    return v0
.end method

.method public onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V
    .locals 0

    .prologue
    .line 31
    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onShowCustomView(Landroid/view/View;Landroid/webkit/WebChromeClient$CustomViewCallback;)V

    .line 33
    invoke-interface {p2}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 34
    return-void
.end method
