.class public Lflipboard/gui/FLSliderHider;
.super Ljava/lang/Object;
.source "FLSliderHider.java"


# static fields
.field private static final b:Landroid/animation/TimeInterpolator;


# instance fields
.field public a:F

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lflipboard/gui/FLSliderHider$Slider;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lflipboard/gui/FLSliderHider$Slider;",
            ">;"
        }
    .end annotation
.end field

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private j:Z

.field private k:Z

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lflipboard/gui/FLSliderHider;->b:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSliderHider;->c:Ljava/util/Map;

    .line 40
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSliderHider;->d:Ljava/util/Set;

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSliderHider;->e:Ljava/util/Set;

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLSliderHider;->a:F

    .line 48
    iput-boolean v1, p0, Lflipboard/gui/FLSliderHider;->j:Z

    .line 50
    iput-boolean v1, p0, Lflipboard/gui/FLSliderHider;->k:Z

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/FLSliderHider;->l:I

    .line 424
    return-void
.end method

.method private a(IFF)V
    .locals 4

    .prologue
    .line 127
    iget v0, p0, Lflipboard/gui/FLSliderHider;->i:F

    .line 128
    iget v1, p0, Lflipboard/gui/FLSliderHider;->f:F

    iget v2, p0, Lflipboard/gui/FLSliderHider;->g:F

    invoke-static {p3, v1, v2}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v1

    .line 129
    iget v2, p0, Lflipboard/gui/FLSliderHider;->l:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lflipboard/gui/FLSliderHider;->l:I

    if-ne v2, p1, :cond_1

    :cond_0
    cmpl-float v0, v1, v0

    if-eqz v0, :cond_1

    .line 130
    iput v1, p0, Lflipboard/gui/FLSliderHider;->i:F

    .line 131
    iget v0, p0, Lflipboard/gui/FLSliderHider;->i:F

    iget v1, p0, Lflipboard/gui/FLSliderHider;->f:F

    sub-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/FLSliderHider;->g:F

    iget v2, p0, Lflipboard/gui/FLSliderHider;->f:F

    sub-float/2addr v1, v2

    div-float v1, v0, v1

    .line 132
    iget-object v0, p0, Lflipboard/gui/FLSliderHider;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSliderHider$Slider;

    .line 133
    invoke-virtual {v0, v1}, Lflipboard/gui/FLSliderHider$Slider;->a(F)V

    goto :goto_0

    .line 137
    :cond_1
    iget v0, p0, Lflipboard/gui/FLSliderHider;->h:F

    .line 138
    sub-float v1, p3, p2

    .line 139
    add-float/2addr v1, v0

    iget v2, p0, Lflipboard/gui/FLSliderHider;->f:F

    iget v3, p0, Lflipboard/gui/FLSliderHider;->g:F

    invoke-static {v1, v2, v3}, Lflipboard/util/JavaUtil;->a(FFF)F

    move-result v1

    iput v1, p0, Lflipboard/gui/FLSliderHider;->h:F

    .line 140
    iget v1, p0, Lflipboard/gui/FLSliderHider;->h:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_2

    .line 141
    iget v0, p0, Lflipboard/gui/FLSliderHider;->h:F

    iget v1, p0, Lflipboard/gui/FLSliderHider;->f:F

    sub-float/2addr v0, v1

    iget v1, p0, Lflipboard/gui/FLSliderHider;->g:F

    iget v2, p0, Lflipboard/gui/FLSliderHider;->f:F

    sub-float/2addr v1, v2

    div-float v1, v0, v1

    .line 142
    iget-object v0, p0, Lflipboard/gui/FLSliderHider;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSliderHider$Slider;

    .line 143
    invoke-virtual {v0, v1}, Lflipboard/gui/FLSliderHider$Slider;->a(F)V

    goto :goto_1

    .line 146
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLSliderHider;->f:F

    .line 84
    neg-float v0, p1

    iput v0, p0, Lflipboard/gui/FLSliderHider;->g:F

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLSliderHider;->j:Z

    .line 86
    return-void
.end method

.method public final a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-static {p1}, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->a(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)I

    move-result v2

    invoke-static {p1}, Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;->b(Lflipboard/gui/FLSliderHider$OnListViewScrollEvent;)Landroid/util/SparseIntArray;

    move-result-object v3

    iget-boolean v0, p0, Lflipboard/gui/FLSliderHider;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLSliderHider;->c:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;

    if-nez v0, :cond_0

    new-instance v0, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v4}, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;-><init>(Lflipboard/gui/FLSliderHider;B)V

    iget-object v4, p0, Lflipboard/gui/FLSliderHider;->c:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {v0}, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->a(Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;)I

    move-result v4

    int-to-float v4, v4

    invoke-static {v0, v3}, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->a(Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;Landroid/util/SparseIntArray;)V

    invoke-static {v0}, Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;->a(Lflipboard/gui/FLSliderHider$ScrollOffsetTracker;)I

    move-result v0

    int-to-float v3, v0

    iget v0, p0, Lflipboard/gui/FLSliderHider;->a:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    invoke-direct {p0, v2, v4, v3}, Lflipboard/gui/FLSliderHider;->a(IFF)V

    .line 98
    :cond_1
    :goto_0
    return-void

    .line 97
    :cond_2
    iget v0, p0, Lflipboard/gui/FLSliderHider;->a:F

    neg-float v0, v0

    cmpg-float v0, v4, v0

    if-gez v0, :cond_4

    iget v0, p0, Lflipboard/gui/FLSliderHider;->a:F

    add-float/2addr v0, v4

    :goto_1
    iget v4, p0, Lflipboard/gui/FLSliderHider;->a:F

    neg-float v4, v4

    cmpg-float v4, v3, v4

    if-gez v4, :cond_3

    iget v1, p0, Lflipboard/gui/FLSliderHider;->a:F

    add-float/2addr v1, v3

    :cond_3
    invoke-direct {p0, v2, v0, v1}, Lflipboard/gui/FLSliderHider;->a(IFF)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lflipboard/gui/FLSliderHider$Slider;)V
    .locals 2

    .prologue
    .line 68
    invoke-static {p1}, Lflipboard/gui/FLSliderHider$Slider;->a(Lflipboard/gui/FLSliderHider$Slider;)Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    move-result-object v0

    sget-object v1, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->a:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    if-ne v0, v1, :cond_0

    .line 70
    iget-object v0, p0, Lflipboard/gui/FLSliderHider;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 75
    :goto_0
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLSliderHider;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
