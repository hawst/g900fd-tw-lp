.class public Lflipboard/gui/FLToast;
.super Landroid/widget/Toast;
.source "FLToast.java"


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lflipboard/gui/FLToast;-><init>(Landroid/content/Context;Ljava/lang/String;B)V

    .line 49
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-direct {p0, p1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    .line 31
    const v0, 0x7f030139

    iget-object v1, p0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    iget-object v0, p0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLToast;->setView(Landroid/view/View;)V

    .line 34
    iget-object v0, p0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0a0040

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lflipboard/gui/FLToast;->b:Landroid/widget/ImageView;

    .line 35
    iget-object v0, p0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    const v1, 0x7f0a0041

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    .line 38
    invoke-virtual {p0, v2}, Lflipboard/gui/FLToast;->a(I)V

    .line 39
    invoke-virtual {p0, p2}, Lflipboard/gui/FLToast;->setText(Ljava/lang/CharSequence;)V

    .line 40
    const/16 v0, 0x10

    invoke-virtual {p0, v0, v2, v2}, Lflipboard/gui/FLToast;->setGravity(III)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLToast;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public static a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 53
    if-eqz p0, :cond_0

    .line 54
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLToast$1;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/FLToast$1;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 61
    :cond_0
    return-void
.end method

.method public static b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    if-eqz p0, :cond_0

    .line 65
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLToast$2;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/FLToast$2;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 72
    :cond_0
    return-void
.end method

.method public static c(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    if-eqz p0, :cond_0

    .line 76
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLToast$3;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/FLToast$3;-><init>(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 84
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lflipboard/gui/FLToast;->b:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 112
    if-eqz p1, :cond_1

    .line 113
    iget-object v0, p0, Lflipboard/gui/FLToast;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/FLToast;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLToast;->b:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Lflipboard/gui/FLToast;->a(I)V

    .line 124
    invoke-virtual {p0, p2}, Lflipboard/gui/FLToast;->setText(Ljava/lang/CharSequence;)V

    .line 125
    invoke-virtual {p0}, Lflipboard/gui/FLToast;->show()V

    .line 126
    return-void
.end method

.method public setText(I)V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 89
    if-eqz p1, :cond_1

    .line 90
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 100
    if-eqz p1, :cond_1

    .line 101
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLToast;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public show()V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0}, Landroid/widget/Toast;->show()V

    .line 139
    iget-object v0, p0, Lflipboard/gui/FLToast;->a:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 140
    return-void
.end method
