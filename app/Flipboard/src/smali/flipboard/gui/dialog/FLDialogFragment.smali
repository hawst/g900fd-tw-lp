.class public abstract Lflipboard/gui/dialog/FLDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "FLDialogFragment.java"


# instance fields
.field public A:Ljava/lang/String;

.field public B:Lflipboard/gui/dialog/FLDialogResponse;

.field public C:Z

.field public D:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 26
    iput-boolean v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    .line 31
    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLDialogFragment;->setRetainInstance(Z)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLDialogFragment:dismiss"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 85
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 86
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 87
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->a()V

    .line 89
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLDialogFragment:show"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 70
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 79
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 73
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_0

    .line 74
    throw v0

    .line 76
    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lnet/hockeyapp/android/ExceptionHandler;->a(Ljava/lang/Throwable;Lnet/hockeyapp/android/CrashManagerListener;)V

    goto :goto_0
.end method

.method public final a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    if-eqz p1, :cond_0

    iget-boolean v0, p1, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p1, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {p0, v0, p2}, Lflipboard/gui/dialog/FLDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 57
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 94
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLDialogFragment:dismissAllowingStateLoss"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 96
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lflipboard/activities/FlipboardActivity;->P:Z

    if-eqz v0, :cond_0

    .line 97
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->b()V

    .line 99
    :cond_0
    return-void
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0, p1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public l_()Z
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->D:Z

    .line 146
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 147
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 105
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p0}, Lflipboard/gui/dialog/FLDialogResponse;->d(Landroid/support/v4/app/DialogFragment;)V

    .line 108
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/dialog/FLDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 126
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDestroyView()V

    .line 127
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->D:Z

    .line 152
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onDetach()V

    .line 153
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 114
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p0}, Lflipboard/gui/dialog/FLDialogResponse;->c(Landroid/support/v4/app/DialogFragment;)V

    .line 117
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    .line 172
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLDialogFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const v1, 0x7f0a004c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 173
    :goto_0
    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->a()V

    .line 176
    :cond_0
    return-void

    .line 172
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 132
    const-string v0, "launched_by_flipboard_activity"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 133
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->startActivity(Landroid/content/Intent;)V

    .line 134
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 139
    const-string v0, "launched_by_flipboard_activity"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 140
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/DialogFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 141
    return-void
.end method
