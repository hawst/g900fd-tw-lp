.class public Lflipboard/gui/dialog/FLProgressDialog;
.super Landroid/app/Dialog;
.source "FLProgressDialog.java"


# instance fields
.field a:J

.field protected b:I

.field protected c:I

.field protected d:I

.field final e:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/app/Activity;I)V
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 57
    const v0, 0x7f0e0029

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 20
    iput v1, p0, Lflipboard/gui/dialog/FLProgressDialog;->b:I

    .line 21
    iput v1, p0, Lflipboard/gui/dialog/FLProgressDialog;->c:I

    .line 22
    iput v1, p0, Lflipboard/gui/dialog/FLProgressDialog;->d:I

    .line 59
    const v0, 0x7f0300f2

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressDialog;->setContentView(I)V

    .line 60
    const v0, 0x7f0a0041

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    const v0, 0x7f0a02ba

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog;->e:Landroid/widget/ImageButton;

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressDialog;->setCancelable(Z)V

    .line 65
    return-void
.end method

.method static synthetic a(Lflipboard/gui/dialog/FLProgressDialog;)V
    .locals 0

    .prologue
    .line 17
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    return-void
.end method


# virtual methods
.method public show()V
    .locals 2

    .prologue
    .line 82
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/dialog/FLProgressDialog$2;

    invoke-direct {v1, p0}, Lflipboard/gui/dialog/FLProgressDialog$2;-><init>(Lflipboard/gui/dialog/FLProgressDialog;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method
