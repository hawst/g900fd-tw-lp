.class Lflipboard/gui/dialog/FLAlertDialog$Builder$3;
.super Ljava/lang/Object;
.source "FLAlertDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;

.field final synthetic b:Landroid/content/DialogInterface$OnClickListener;

.field final synthetic c:Lflipboard/gui/dialog/FLAlertDialog$Builder;


# direct methods
.method constructor <init>(Lflipboard/gui/dialog/FLAlertDialog$Builder;Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->c:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iput-object p2, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->a:Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;

    iput-object p3, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->b:Landroid/content/DialogInterface$OnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->a:Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;

    iput p3, v0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->c:I

    .line 206
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->a:Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->notifyDataSetChanged()V

    .line 207
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->c:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v0, v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->c:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 208
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->b:Landroid/content/DialogInterface$OnClickListener;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->b:Landroid/content/DialogInterface$OnClickListener;

    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;->c:Lflipboard/gui/dialog/FLAlertDialog$Builder;

    iget-object v1, v1, Lflipboard/gui/dialog/FLAlertDialog$Builder;->c:Landroid/app/AlertDialog;

    invoke-interface {v0, v1, p3}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    .line 211
    :cond_0
    return-void
.end method
