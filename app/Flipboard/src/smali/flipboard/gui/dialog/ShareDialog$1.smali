.class Lflipboard/gui/dialog/ShareDialog$1;
.super Ljava/lang/Object;
.source "ShareDialog.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/content/pm/ResolveInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Landroid/content/pm/PackageManager;

.field final synthetic b:Lflipboard/gui/dialog/ShareDialog;


# direct methods
.method constructor <init>(Lflipboard/gui/dialog/ShareDialog;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lflipboard/gui/dialog/ShareDialog$1;->b:Lflipboard/gui/dialog/ShareDialog;

    iput-object p2, p0, Lflipboard/gui/dialog/ShareDialog$1;->a:Landroid/content/pm/PackageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 90
    check-cast p1, Landroid/content/pm/ResolveInfo;

    check-cast p2, Landroid/content/pm/ResolveInfo;

    iget-boolean v0, p1, Landroid/content/pm/ResolveInfo;->isDefault:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p2, Landroid/content/pm/ResolveInfo;->isDefault:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    iget v0, p1, Landroid/content/pm/ResolveInfo;->priority:I

    iget v1, p2, Landroid/content/pm/ResolveInfo;->priority:I

    sub-int/2addr v0, v1

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$1;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {p1, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/dialog/ShareDialog$1;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {p2, v1}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    :cond_1
    return v0

    :cond_2
    iget v0, p1, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    iget v1, p2, Landroid/content/pm/ResolveInfo;->preferredOrder:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
