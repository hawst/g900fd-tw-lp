.class public Lflipboard/gui/dialog/FLAlertDialog;
.super Landroid/app/AlertDialog;
.source "FLAlertDialog.java"


# instance fields
.field a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 41
    const/high16 v0, 0x7f0e0000

    invoke-direct {p0, p1, v0}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;I)V

    .line 42
    iput-object p2, p0, Lflipboard/gui/dialog/FLAlertDialog;->a:Landroid/view/View;

    .line 43
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Landroid/view/View;B)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lflipboard/gui/dialog/FLAlertDialog;-><init>(Landroid/content/Context;Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 66
    :try_start_0
    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 47
    invoke-virtual {p0, v1}, Lflipboard/gui/dialog/FLAlertDialog;->requestWindowFeature(I)Z

    .line 48
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    .line 49
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLAlertDialog;->setContentView(Landroid/view/View;)V

    .line 50
    invoke-virtual {p0, v1}, Lflipboard/gui/dialog/FLAlertDialog;->setCancelable(Z)V

    .line 51
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLAlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const v1, 0x20008

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 52
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 57
    :try_start_0
    invoke-super {p0}, Landroid/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
