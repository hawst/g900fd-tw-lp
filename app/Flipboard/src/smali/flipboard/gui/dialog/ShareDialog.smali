.class public Lflipboard/gui/dialog/ShareDialog;
.super Lflipboard/gui/dialog/FLAlertDialog;
.source "ShareDialog.java"


# static fields
.field static b:Landroid/widget/ListView;


# instance fields
.field c:Landroid/os/Bundle;

.field d:Lflipboard/service/Section;

.field e:Lflipboard/objs/FeedItem;

.field private f:Lflipboard/activities/FlipboardActivity;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lflipboard/gui/dialog/FLAlertDialog;-><init>(Landroid/content/Context;)V

    move-object v0, p1

    .line 55
    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iput-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->f:Lflipboard/activities/FlipboardActivity;

    .line 56
    const v0, 0x7f03011c

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->a:Landroid/view/View;

    .line 57
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->a:Landroid/view/View;

    const v1, 0x7f0a0312

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    sput-object v0, Lflipboard/gui/dialog/ShareDialog;->b:Landroid/widget/ListView;

    .line 58
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->a:Landroid/view/View;

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 59
    const v1, 0x7f0d02ed

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    return-void
.end method

.method static synthetic a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->f:Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->d:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog;->e:Lflipboard/objs/FeedItem;

    return-object v0
.end method
