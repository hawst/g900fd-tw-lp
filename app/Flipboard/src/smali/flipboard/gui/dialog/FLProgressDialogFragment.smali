.class public Lflipboard/gui/dialog/FLProgressDialogFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "FLProgressDialogFragment.java"


# instance fields
.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final c()Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {p0}, Lflipboard/gui/dialog/FLProgressDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/dialog/FLProgressDialogFragment;->A:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/dialog/FLProgressDialog;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 23
    iget-boolean v1, p0, Lflipboard/gui/dialog/FLProgressDialogFragment;->j:Z

    if-eqz v1, :cond_0

    .line 24
    iget-object v1, v0, Lflipboard/gui/dialog/FLProgressDialog;->e:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v1, v0, Lflipboard/gui/dialog/FLProgressDialog;->e:Landroid/widget/ImageButton;

    new-instance v2, Lflipboard/gui/dialog/FLProgressDialog$1;

    invoke-direct {v2, v0}, Lflipboard/gui/dialog/FLProgressDialog$1;-><init>(Lflipboard/gui/dialog/FLProgressDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    :cond_0
    iget-boolean v1, p0, Lflipboard/gui/dialog/FLProgressDialogFragment;->C:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 27
    return-object v0
.end method
