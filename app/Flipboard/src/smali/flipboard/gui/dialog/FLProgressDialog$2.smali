.class Lflipboard/gui/dialog/FLProgressDialog$2;
.super Ljava/lang/Object;
.source "FLProgressDialog.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/dialog/FLProgressDialog;


# direct methods
.method constructor <init>(Lflipboard/gui/dialog/FLProgressDialog;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 87
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/FLProgressDialog;->a(Lflipboard/gui/dialog/FLProgressDialog;)V

    .line 89
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 90
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    iget v0, v0, Lflipboard/gui/dialog/FLProgressDialog;->b:I

    if-ne v0, v3, :cond_0

    .line 93
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLProgressDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 94
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    neg-int v0, v0

    div-int/lit8 v0, v0, 0x4

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 96
    :cond_0
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    iget v0, v0, Lflipboard/gui/dialog/FLProgressDialog;->c:I

    if-eq v0, v3, :cond_1

    .line 97
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    iget v0, v0, Lflipboard/gui/dialog/FLProgressDialog;->c:I

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 99
    :cond_1
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    iget v0, v0, Lflipboard/gui/dialog/FLProgressDialog;->d:I

    if-eq v0, v3, :cond_2

    .line 100
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    iget v0, v0, Lflipboard/gui/dialog/FLProgressDialog;->d:I

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 102
    :cond_2
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/FLProgressDialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1

    .line 108
    :goto_0
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressDialog$2;->a:Lflipboard/gui/dialog/FLProgressDialog;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lflipboard/gui/dialog/FLProgressDialog;->a:J

    .line 109
    return-void

    .line 104
    :catch_0
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0

    .line 106
    :catch_1
    move-exception v0

    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    goto :goto_0
.end method
