.class public Lflipboard/gui/dialog/FLDialogAdapter;
.super Ljava/lang/Object;
.source "FLDialogAdapter.java"

# interfaces
.implements Lflipboard/gui/dialog/FLDialogResponse;


# instance fields
.field e:Lflipboard/gui/dialog/FLDialogResponse;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>(B)V

    .line 19
    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p1}, Lflipboard/gui/dialog/FLDialogResponse;->a(Landroid/support/v4/app/DialogFragment;)V

    .line 37
    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/app/DialogFragment;I)V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p1, p2}, Lflipboard/gui/dialog/FLDialogResponse;->a(Landroid/support/v4/app/DialogFragment;I)V

    .line 51
    :cond_0
    return-void
.end method

.method public b(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p1}, Lflipboard/gui/dialog/FLDialogResponse;->b(Landroid/support/v4/app/DialogFragment;)V

    .line 30
    :cond_0
    return-void
.end method

.method public c(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p1}, Lflipboard/gui/dialog/FLDialogResponse;->c(Landroid/support/v4/app/DialogFragment;)V

    .line 65
    :cond_0
    return-void
.end method

.method public d(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p1}, Lflipboard/gui/dialog/FLDialogResponse;->d(Landroid/support/v4/app/DialogFragment;)V

    .line 58
    :cond_0
    return-void
.end method

.method public e(Landroid/support/v4/app/DialogFragment;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Lflipboard/gui/dialog/FLDialogAdapter;->e:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-interface {v0, p1}, Lflipboard/gui/dialog/FLDialogResponse;->e(Landroid/support/v4/app/DialogFragment;)V

    .line 44
    :cond_0
    return-void
.end method
