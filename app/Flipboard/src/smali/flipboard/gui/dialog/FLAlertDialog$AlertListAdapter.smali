.class Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;
.super Landroid/widget/BaseAdapter;
.source "FLAlertDialog.java"


# instance fields
.field final a:I

.field final b:[Ljava/lang/CharSequence;

.field c:I

.field final d:Landroid/view/LayoutInflater;


# direct methods
.method constructor <init>(Landroid/content/Context;I[Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 296
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 297
    iput p2, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->a:I

    .line 298
    iput-object p3, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->b:[Ljava/lang/CharSequence;

    .line 299
    iput p4, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->c:I

    .line 300
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->d:Landroid/view/LayoutInflater;

    .line 301
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->b:[Ljava/lang/CharSequence;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->b:[Ljava/lang/CharSequence;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 313
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 317
    if-eqz p2, :cond_3

    .line 318
    :goto_0
    const v0, 0x7f0a0041

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 319
    if-eqz v0, :cond_0

    .line 320
    iget-object v2, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->b:[Ljava/lang/CharSequence;

    aget-object v2, v2, p1

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 322
    :cond_0
    const v0, 0x7f0a0056

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    .line 323
    if-eqz v0, :cond_2

    .line 324
    iget v2, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->c:I

    if-ne p1, v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-interface {v0, v1}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 326
    :cond_2
    return-object p2

    .line 317
    :cond_3
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->d:Landroid/view/LayoutInflater;

    iget v2, p0, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;->a:I

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
