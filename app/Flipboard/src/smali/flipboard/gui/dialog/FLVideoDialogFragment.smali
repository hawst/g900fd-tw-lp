.class public Lflipboard/gui/dialog/FLVideoDialogFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "FLVideoDialogFragment.java"


# instance fields
.field j:Landroid/view/ViewGroup;

.field public k:Landroid/view/View;

.field public l:Landroid/webkit/WebChromeClient$CustomViewCallback;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final e()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->j:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->l:Landroid/webkit/WebChromeClient$CustomViewCallback;

    invoke-interface {v0}, Landroid/webkit/WebChromeClient$CustomViewCallback;->onCustomViewHidden()V

    .line 65
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 27
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f0e0018

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLVideoDialogFragment;->a(I)V

    .line 29
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLVideoDialogFragment;->setRetainInstance(Z)V

    .line 30
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 34
    const v0, 0x7f030152

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 35
    const v0, 0x7f0a038f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->j:Landroid/view/ViewGroup;

    .line 36
    iget-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->j:Landroid/view/ViewGroup;

    iget-object v2, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    :cond_0
    return-object v1
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 69
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLVideoDialogFragment;->e()V

    .line 70
    invoke-super {p0}, Lflipboard/gui/dialog/FLDialogFragment;->onStop()V

    .line 71
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lflipboard/gui/dialog/FLVideoDialogFragment;->k:Landroid/view/View;

    if-nez v0, :cond_0

    .line 44
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLVideoDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->a()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentTransaction;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->c()I

    .line 46
    :cond_0
    return-void
.end method
