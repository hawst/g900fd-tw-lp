.class public Lflipboard/gui/dialog/FLProgressBarDialog;
.super Landroid/app/Dialog;
.source "FLProgressBarDialog.java"


# instance fields
.field private final a:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 18
    const v0, 0x7f0e0029

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 20
    const v0, 0x7f0300f1

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressBarDialog;->setContentView(I)V

    .line 21
    const v0, 0x7f0a0041

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressBarDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 22
    const v0, 0x7f0a0257

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressBarDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lflipboard/gui/dialog/FLProgressBarDialog;->a:Landroid/widget/ProgressBar;

    .line 23
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressBarDialog;->a:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 24
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressBarDialog;->a:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 26
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLProgressBarDialog;->setCancelable(Z)V

    .line 27
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lflipboard/gui/dialog/FLProgressBarDialog;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 32
    return-void
.end method
