.class Lflipboard/gui/dialog/ShareDialog$3;
.super Ljava/lang/Object;
.source "ShareDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/gui/dialog/ShareDialog;


# direct methods
.method constructor <init>(Lflipboard/gui/dialog/ShareDialog;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    iput-object p2, p0, Lflipboard/gui/dialog/ShareDialog$3;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 149
    new-instance v7, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 150
    const/high16 v0, 0x80000

    invoke-virtual {v7, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 151
    const/4 v2, 0x0

    .line 152
    const/4 v1, 0x0

    .line 153
    const/4 v0, 0x0

    .line 154
    iget-object v3, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v3}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "flipboard.extra.reference.type"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 155
    iget-object v2, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v2}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "flipboard.extra.reference.type"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 156
    const-string v2, "section"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 157
    const-string v4, "status"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 158
    iget-object v1, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v1}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d032f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 159
    iget-object v3, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v3}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "flipboard.extra.reference.author"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 160
    iget-object v4, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v4}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "flipboard.extra.reference.service"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 161
    const-string v5, "twitter"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 162
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v8, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v8}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0d032e

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    const/4 v3, 0x2

    aput-object v4, v5, v3

    invoke-static {v1, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    .line 185
    :goto_0
    if-eqz v0, :cond_0

    .line 186
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    :cond_0
    const-string v1, ""

    .line 190
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 191
    const/4 v2, 0x0

    .line 192
    if-eqz v0, :cond_e

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "mail"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 193
    const/4 v2, 0x1

    .line 194
    const/4 v0, 0x0

    .line 195
    iget-object v5, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v5}, Lflipboard/gui/dialog/ShareDialog;->b(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/activities/FlipboardActivity;

    move-result-object v5

    invoke-virtual {v5}, Lflipboard/activities/FlipboardActivity;->g()Lflipboard/gui/flipping/FlippingBitmap;

    move-result-object v5

    .line 196
    if-eqz v5, :cond_1

    .line 197
    iget-object v6, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v6}, Lflipboard/gui/dialog/ShareDialog;->b(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/activities/FlipboardActivity;

    move-result-object v6

    invoke-virtual {v6}, Lflipboard/activities/FlipboardActivity;->getExternalCacheDir()Ljava/io/File;

    move-result-object v6

    .line 198
    if-eqz v6, :cond_1

    .line 199
    new-instance v0, Ljava/io/File;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "/cover.jpg"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 200
    iget-object v6, v5, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    const/16 v8, 0x258

    if-le v6, v8, :cond_d

    .line 201
    iget-object v6, v5, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    const/16 v8, 0x258

    iget-object v9, v5, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    mul-int/lit16 v9, v9, 0x258

    iget-object v10, v5, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    div-int/2addr v9, v10

    const/4 v10, 0x1

    invoke-static {v6, v8, v9, v10}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    .line 202
    invoke-static {v6, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    .line 203
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->recycle()V

    .line 207
    :goto_1
    sget-object v6, Lflipboard/gui/flipping/ViewScreenshotCreator;->b:Lflipboard/gui/flipping/ViewScreenshotCreator;

    invoke-virtual {v6, v5}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Lflipboard/gui/flipping/FlippingBitmap;)V

    .line 208
    const-string v5, "android.intent.extra.STREAM"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 209
    const/4 v0, 0x1

    .line 212
    :cond_1
    if-nez v0, :cond_15

    .line 213
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "flipboard.extra.reference.excerpt"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 214
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "flipboard.extra.reference.excerpt"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_2

    .line 216
    const-string v1, "text/html"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    const-string v1, "\u00ad"

    const-string v5, ""

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v5, 0x12c

    if-le v1, v5, :cond_2

    .line 221
    const-string v1, "%s..."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v9, 0x129

    invoke-virtual {v0, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-static {v1, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_2
    move v1, v2

    .line 231
    :goto_3
    iget-object v2, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v2}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "flipboard.extra.reference.link"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 232
    iget-object v2, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v2}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v2

    const-string v5, "flipboard.extra.reference.link"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 233
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_11

    .line 234
    :cond_3
    if-eqz v1, :cond_10

    .line 235
    if-eqz v4, :cond_f

    .line 236
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v4, 0x7f0d004c

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v3, 0x1

    aput-object v2, v4, v3

    const/4 v3, 0x2

    aput-object v2, v4, v3

    invoke-static {v0, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 258
    :cond_4
    :goto_4
    if-eqz v1, :cond_14

    .line 259
    iget-object v1, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v1}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d00da

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "http://flpbd.it/now"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 260
    const-string v2, "android.intent.extra.TEXT"

    const-string v3, "%s<br/><br/>%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v7, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    .line 264
    :goto_5
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 265
    new-instance v6, Lflipboard/objs/UserState$TargetAuthor;

    invoke-direct {v6}, Lflipboard/objs/UserState$TargetAuthor;-><init>()V

    .line 266
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iput-object v0, v6, Lflipboard/objs/UserState$TargetAuthor;->d:Ljava/lang/String;

    .line 267
    const-string v1, "shared"

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/ShareDialog;->c(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/service/Section;

    move-result-object v4

    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/ShareDialog;->d(Lflipboard/gui/dialog/ShareDialog;)Lflipboard/objs/FeedItem;

    move-result-object v5

    invoke-static/range {v1 .. v6}, Lflipboard/io/UsageEvent;->a(Ljava/lang/String;JLflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UserState$TargetAuthor;)V

    .line 268
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 269
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/ShareDialog;->dismiss()V

    .line 270
    return-void

    .line 164
    :cond_5
    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v8, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v8}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0d0312

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    const/4 v3, 0x2

    aput-object v4, v5, v3

    invoke-static {v1, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    .line 166
    goto/16 :goto_0

    .line 167
    :cond_6
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "flipboard.extra.reference.title"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 168
    if-eqz v0, :cond_7

    .line 169
    :goto_6
    const-string v4, "%s: %s"

    .line 170
    const-string v5, "post"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 171
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v5}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d003a

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-static {v4, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    .line 168
    :cond_7
    const-string v0, ""

    goto :goto_6

    .line 172
    :cond_8
    const-string v5, "video"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 173
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v5}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d0343

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-static {v4, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    .line 174
    :cond_9
    const-string v5, "image"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 175
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v5}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d0247

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-static {v4, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    .line 176
    :cond_a
    const-string v5, "album"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 177
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v5}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d002b

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-static {v4, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    .line 178
    :cond_b
    const-string v5, "audio"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 179
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v5}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0d003e

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v3

    const/4 v3, 0x1

    aput-object v0, v1, v3

    invoke-static {v4, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    .line 180
    :cond_c
    if-eqz v2, :cond_16

    .line 181
    iget-object v1, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v1}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d0359

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    .line 205
    :cond_d
    iget-object v6, v5, Lflipboard/gui/flipping/FlippingBitmap;->a:Landroid/graphics/Bitmap;

    invoke-static {v6, v0}, Lflipboard/gui/flipping/ViewScreenshotCreator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)V

    goto/16 :goto_1

    .line 227
    :cond_e
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-static {v0}, Lflipboard/gui/dialog/ShareDialog;->a(Lflipboard/gui/dialog/ShareDialog;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "flipboard.extra.reference.title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    const-string v1, "text/plain"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move v1, v2

    goto/16 :goto_3

    .line 238
    :cond_f
    const-string v0, "<a href=%s>%s</a>"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :cond_10
    move-object v0, v2

    .line 241
    goto/16 :goto_4

    .line 243
    :cond_11
    const/4 v5, 0x0

    invoke-static {v0, v5}, Lflipboard/util/JavaUtil;->b(Ljava/lang/String;I)Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 244
    if-eqz v1, :cond_13

    .line 245
    if-eqz v4, :cond_12

    .line 246
    iget-object v4, p0, Lflipboard/gui/dialog/ShareDialog$3;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v4}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0d004c

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v2, v5, v3

    const/4 v3, 0x2

    aput-object v2, v5, v3

    invoke-static {v4, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 247
    const-string v3, "%s<br/><br/>%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v0, v4, v2

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 249
    :cond_12
    const-string v3, "%s<br/><br/><a href=%s>%s</a>"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    const/4 v0, 0x2

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 252
    :cond_13
    const-string v3, "%s\n\n%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v2, v4, v0

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    .line 262
    :cond_14
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v7, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto/16 :goto_5

    :cond_15
    move-object v0, v1

    goto/16 :goto_2

    :cond_16
    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0

    :cond_17
    move-object v3, v0

    move v4, v2

    move-object v0, v1

    goto/16 :goto_0
.end method
