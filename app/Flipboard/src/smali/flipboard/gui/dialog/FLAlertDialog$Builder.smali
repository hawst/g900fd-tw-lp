.class public Lflipboard/gui/dialog/FLAlertDialog$Builder;
.super Landroid/app/AlertDialog$Builder;
.source "FLAlertDialog.java"


# instance fields
.field final a:Landroid/content/Context;

.field public final b:Landroid/view/View;

.field c:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 84
    iput-object p1, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    .line 87
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 88
    const v1, 0x7f030010

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    .line 89
    return-void
.end method

.method private a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;II)Landroid/app/AlertDialog$Builder;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 258
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a0052

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 262
    :cond_0
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    invoke-virtual {v0, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 263
    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 264
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 265
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 266
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder$4;

    invoke-direct {v1, p0, p2, p4}, Lflipboard/gui/dialog/FLAlertDialog$Builder$4;-><init>(Lflipboard/gui/dialog/FLAlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;I)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    :cond_1
    return-object p0
.end method


# virtual methods
.method public create()Landroid/app/AlertDialog;
    .locals 4

    .prologue
    .line 284
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog;

    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    iget-object v2, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lflipboard/gui/dialog/FLAlertDialog;-><init>(Landroid/content/Context;Landroid/view/View;B)V

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->c:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a0046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 149
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 150
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 151
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 152
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder$1;

    invoke-direct {v1, p0, p2}, Lflipboard/gui/dialog/FLAlertDialog$Builder$1;-><init>(Lflipboard/gui/dialog/FLAlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 162
    :cond_0
    return-object p0
.end method

.method public setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 5

    .prologue
    .line 174
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a0046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 175
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 176
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 177
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;

    iget-object v2, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    const v3, 0x7f030011

    const/4 v4, -0x1

    invoke-direct {v1, v2, v3, p1, v4}, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 178
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$Builder$2;

    invoke-direct {v1, p0, p2}, Lflipboard/gui/dialog/FLAlertDialog$Builder$2;-><init>(Lflipboard/gui/dialog/FLAlertDialog$Builder;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 188
    :cond_0
    return-object p0
.end method

.method public setMessage(I)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a0050

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 118
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 119
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 120
    invoke-interface {v0, p1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    return-object p0
.end method

.method public setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 250
    const v0, 0x7f0a0053

    const/4 v1, -0x2

    invoke-direct {p0, p1, p2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;II)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 238
    const v0, 0x7f0a0054

    const/4 v1, -0x3

    invoke-direct {p0, p1, p2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;II)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 226
    const v0, 0x7f0a0055

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;II)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a0046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 198
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 199
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 200
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;

    iget-object v2, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    const v3, 0x7f030012

    invoke-direct {v1, v2, v3, p1, p2}, Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/CharSequence;I)V

    .line 201
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 202
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;

    invoke-direct {v2, p0, v1, p3}, Lflipboard/gui/dialog/FLAlertDialog$Builder$3;-><init>(Lflipboard/gui/dialog/FLAlertDialog$Builder;Lflipboard/gui/dialog/FLAlertDialog$AlertListAdapter;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 214
    :cond_0
    return-object p0
.end method

.method public setTitle(I)Landroid/app/AlertDialog$Builder;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    return-object v0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 101
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 102
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 103
    invoke-interface {v0, p1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_0
    return-object p0
.end method

.method public setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lflipboard/gui/dialog/FLAlertDialog$Builder;->b:Landroid/view/View;

    const v1, 0x7f0a0051

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 131
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 132
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 133
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 135
    :cond_0
    return-object p0
.end method
