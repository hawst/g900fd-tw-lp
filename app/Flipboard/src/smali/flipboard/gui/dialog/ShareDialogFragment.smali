.class public Lflipboard/gui/dialog/ShareDialogFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "ShareDialogFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final c()Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 12
    new-instance v1, Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {p0}, Lflipboard/gui/dialog/ShareDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Lflipboard/gui/dialog/ShareDialog;-><init>(Landroid/content/Context;)V

    .line 13
    invoke-virtual {p0}, Lflipboard/gui/dialog/ShareDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, v1, Lflipboard/gui/dialog/ShareDialog;->c:Landroid/os/Bundle;

    const-string v2, "extra_section_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3, v2}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    iput-object v2, v1, Lflipboard/gui/dialog/ShareDialog;->d:Lflipboard/service/Section;

    :cond_0
    const-string v2, "extra_current_item"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, v1, Lflipboard/gui/dialog/ShareDialog;->d:Lflipboard/service/Section;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lflipboard/gui/dialog/ShareDialog;->d:Lflipboard/service/Section;

    invoke-virtual {v2, v0}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, v1, Lflipboard/gui/dialog/ShareDialog;->e:Lflipboard/objs/FeedItem;

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "text/plain"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v2, 0x80000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v1}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    invoke-virtual {v1}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v5, 0x7f0d013a

    invoke-virtual {v0, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    invoke-virtual {v0, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    new-instance v0, Lflipboard/gui/dialog/ShareDialog$1;

    invoke-direct {v0, v1, v2}, Lflipboard/gui/dialog/ShareDialog$1;-><init>(Lflipboard/gui/dialog/ShareDialog;Landroid/content/pm/PackageManager;)V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object v0, Lflipboard/gui/dialog/ShareDialog;->b:Landroid/widget/ListView;

    new-instance v2, Lflipboard/gui/dialog/ShareDialog$2;

    invoke-direct {v2, v1, v3}, Lflipboard/gui/dialog/ShareDialog$2;-><init>(Lflipboard/gui/dialog/ShareDialog;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    sget-object v0, Lflipboard/gui/dialog/ShareDialog;->b:Landroid/widget/ListView;

    new-instance v2, Lflipboard/gui/dialog/ShareDialog$3;

    invoke-direct {v2, v1, v3}, Lflipboard/gui/dialog/ShareDialog$3;-><init>(Lflipboard/gui/dialog/ShareDialog;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 14
    return-object v1
.end method
