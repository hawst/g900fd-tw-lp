.class Lflipboard/gui/dialog/ShareDialog$2;
.super Landroid/widget/BaseAdapter;
.source "ShareDialog.java"


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/gui/dialog/ShareDialog;


# direct methods
.method constructor <init>(Lflipboard/gui/dialog/ShareDialog;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lflipboard/gui/dialog/ShareDialog$2;->b:Lflipboard/gui/dialog/ShareDialog;

    iput-object p2, p0, Lflipboard/gui/dialog/ShareDialog$2;->a:Ljava/util/List;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private a(I)Landroid/content/pm/ResolveInfo;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$2;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$2;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lflipboard/gui/dialog/ShareDialog$2;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 124
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 129
    .line 131
    if-nez p2, :cond_0

    .line 132
    iget-object v0, p0, Lflipboard/gui/dialog/ShareDialog$2;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v0}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f03011e

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 133
    new-instance v1, Lflipboard/gui/dialog/ShareDialog$Holder;

    invoke-direct {v1}, Lflipboard/gui/dialog/ShareDialog$Holder;-><init>()V

    .line 134
    const v0, 0x7f0a0313

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lflipboard/gui/dialog/ShareDialog$Holder;->a:Landroid/widget/ImageView;

    .line 135
    const v0, 0x7f0a0314

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, v1, Lflipboard/gui/dialog/ShareDialog$Holder;->b:Lflipboard/gui/FLLabelTextView;

    .line 136
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 138
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/dialog/ShareDialog$Holder;

    .line 139
    iget-object v1, v0, Lflipboard/gui/dialog/ShareDialog$Holder;->a:Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lflipboard/gui/dialog/ShareDialog$2;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v2

    iget-object v3, p0, Lflipboard/gui/dialog/ShareDialog$2;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v3}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 140
    iget-object v0, v0, Lflipboard/gui/dialog/ShareDialog$Holder;->b:Lflipboard/gui/FLLabelTextView;

    invoke-direct {p0, p1}, Lflipboard/gui/dialog/ShareDialog$2;->a(I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/dialog/ShareDialog$2;->b:Lflipboard/gui/dialog/ShareDialog;

    invoke-virtual {v2}, Lflipboard/gui/dialog/ShareDialog;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 141
    return-object p2
.end method
