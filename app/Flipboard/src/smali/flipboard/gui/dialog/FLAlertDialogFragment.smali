.class public Lflipboard/gui/dialog/FLAlertDialogFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "FLAlertDialogFragment.java"


# instance fields
.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field n:[Ljava/lang/String;

.field o:I

.field public p:Landroid/widget/ListAdapter;

.field public q:[Ljava/lang/CharSequence;

.field public r:I

.field public s:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->n:[Ljava/lang/String;

    .line 75
    iput p2, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->o:I

    .line 76
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0, p1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public final c()Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 90
    new-instance v0, Lflipboard/gui/dialog/FLAlertDialog$Builder;

    invoke-virtual {p0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 91
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 94
    :cond_0
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->A:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 95
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 100
    :cond_1
    iget v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->r:I

    if-lez v1, :cond_2

    .line 101
    invoke-virtual {p0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->r:I

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    .line 103
    :cond_2
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 104
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 107
    :cond_3
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->n:[Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 108
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->n:[Ljava/lang/String;

    iget v2, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->o:I

    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment$1;

    invoke-direct {v3, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment$1;-><init>(Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 118
    :cond_4
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->p:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_5

    .line 119
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->p:Landroid/widget/ListAdapter;

    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment$2;

    invoke-direct {v2, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment$2;-><init>(Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 129
    :cond_5
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_6

    .line 130
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->q:[Ljava/lang/CharSequence;

    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment$3;

    invoke-direct {v2, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment$3;-><init>(Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 141
    :cond_6
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 142
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment$4;

    invoke-direct {v2, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment$4;-><init>(Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 152
    :cond_7
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 153
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment$5;

    invoke-direct {v2, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment$5;-><init>(Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 163
    :cond_8
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 164
    iget-object v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment$6;

    invoke-direct {v2, p0}, Lflipboard/gui/dialog/FLAlertDialogFragment$6;-><init>(Lflipboard/gui/dialog/FLAlertDialogFragment;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 174
    :cond_9
    invoke-virtual {v0}, Lflipboard/gui/dialog/FLAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 175
    iget-boolean v1, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->C:Z

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 176
    return-object v0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0, p1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->k:Ljava/lang/String;

    .line 45
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0, p1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->l:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v0, p1}, Lflipboard/app/FlipboardApplication;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/dialog/FLAlertDialogFragment;->m:Ljava/lang/String;

    .line 65
    return-void
.end method
