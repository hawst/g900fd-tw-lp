.class public Lflipboard/gui/FLSearchView;
.super Lflipboard/gui/FLRelativeLayout;
.source "FLSearchView.java"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/FLRelativeLayout;",
        "Landroid/text/TextWatcher;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Landroid/widget/TextView$OnEditorActionListener;",
        "Ljava/util/Comparator",
        "<",
        "Lflipboard/objs/SearchResultItem;",
        ">;"
    }
.end annotation


# static fields
.field public static a:Ljava/lang/String;

.field public static final b:Lflipboard/util/Log;


# instance fields
.field final A:I

.field final B:I

.field final C:I

.field D:Ljava/util/TimerTask;

.field E:Lflipboard/gui/ContentDrawerListItemAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field F:Lflipboard/gui/SearchListAdapter;

.field G:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field H:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultItem;",
            ">;"
        }
    .end annotation
.end field

.field I:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field J:I

.field K:I

.field L:J

.field M:Lflipboard/gui/FLSearchView$FLSearchViewListener;

.field N:I

.field private O:Landroid/widget/ListView;

.field private P:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private Q:Z

.field private final R:Lflipboard/service/FlipboardManager$RootScreenStyle;

.field private S:Landroid/view/View;

.field private T:Ljava/lang/String;

.field public c:J

.field d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/gui/FLSearchView$CollapsedItemList;",
            ">;"
        }
    .end annotation
.end field

.field protected f:Lflipboard/objs/SearchResultItem;

.field protected g:Lflipboard/objs/SearchResultItem;

.field protected h:Lflipboard/objs/SearchResultItem;

.field protected i:Lflipboard/gui/FLSearchView$SearchResultAction;

.field protected j:Landroid/widget/EditText;

.field protected k:Landroid/view/View;

.field protected l:Landroid/widget/ViewFlipper;

.field protected m:Landroid/widget/ProgressBar;

.field protected o:Lflipboard/service/FLSearchManager;

.field protected p:Lflipboard/service/Flap$SearchObserver;

.field protected q:Lflipboard/service/FLSearchManager$LocalSearchObserver;

.field protected r:Lflipboard/activities/FlipboardActivity;

.field s:Ljava/lang/String;

.field t:I

.field u:I

.field v:Z

.field w:Z

.field x:Ljava/lang/String;

.field y:Ljava/lang/String;

.field z:Lflipboard/gui/FLSearchView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "search_text"

    sput-object v0, Lflipboard/gui/FLSearchView;->a:Ljava/lang/String;

    .line 51
    const-string v0, "flsearchview"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLSearchView;->b:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/FLSearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 217
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/FLSearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 212
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x3

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 141
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    iput-wide v6, p0, Lflipboard/gui/FLSearchView;->c:J

    .line 86
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    .line 102
    iput v1, p0, Lflipboard/gui/FLSearchView;->t:I

    iput v1, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 104
    iput-object v5, p0, Lflipboard/gui/FLSearchView;->x:Ljava/lang/String;

    sget-object v0, Lflipboard/objs/UsageEventV2$SearchNavFrom;->b:Lflipboard/objs/UsageEventV2$SearchNavFrom;

    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2$SearchNavFrom;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->y:Ljava/lang/String;

    .line 107
    sget-object v0, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 110
    iput v2, p0, Lflipboard/gui/FLSearchView;->A:I

    .line 111
    iput v2, p0, Lflipboard/gui/FLSearchView;->B:I

    .line 112
    const/4 v0, 0x4

    iput v0, p0, Lflipboard/gui/FLSearchView;->C:I

    .line 123
    iput v1, p0, Lflipboard/gui/FLSearchView;->J:I

    .line 124
    iput v1, p0, Lflipboard/gui/FLSearchView;->K:I

    .line 125
    iput-wide v6, p0, Lflipboard/gui/FLSearchView;->L:J

    .line 143
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 145
    const v1, 0x7f030102

    .line 146
    iget-object v2, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v3, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v2, v3, :cond_0

    .line 147
    const v1, 0x7f030103

    .line 149
    :cond_0
    invoke-virtual {v0, v1, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 150
    const v1, 0x7f0300ff

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->S:Landroid/view/View;

    .line 151
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->S:Landroid/view/View;

    new-instance v1, Lflipboard/gui/FLSearchView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/FLSearchView$1;-><init>(Lflipboard/gui/FLSearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    .line 167
    iput-boolean v4, p0, Lflipboard/gui/FLSearchView;->Q:Z

    .line 168
    new-instance v0, Lflipboard/service/FLSearchManager;

    iget-boolean v1, p0, Lflipboard/gui/FLSearchView;->Q:Z

    invoke-direct {v0, p1, v1}, Lflipboard/service/FLSearchManager;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    .line 169
    new-instance v0, Lflipboard/gui/FLSearchView$2;

    invoke-direct {v0, p0}, Lflipboard/gui/FLSearchView$2;-><init>(Lflipboard/gui/FLSearchView;)V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    .line 200
    new-instance v0, Lflipboard/gui/FLSearchView$3;

    invoke-direct {v0, p0}, Lflipboard/gui/FLSearchView$3;-><init>(Lflipboard/gui/FLSearchView;)V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->q:Lflipboard/service/FLSearchManager$LocalSearchObserver;

    .line 207
    return-void
.end method

.method private static a(Lflipboard/objs/SearchResultItem;)F
    .locals 2

    .prologue
    .line 1005
    iget-object v0, p0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v1, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v1, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    .line 1006
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1007
    :cond_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    .line 1011
    :goto_0
    return v0

    .line 1008
    :cond_1
    iget-object v0, p0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v1, Lflipboard/objs/SearchResultItem;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1009
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0

    .line 1011
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/FLSearchView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->S:Landroid/view/View;

    return-object v0
.end method

.method private a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 826
    .line 829
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    iget-object v0, v0, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    iget-object v0, v0, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->i:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 832
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    iget-object v0, v0, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    sget-object v1, Lflipboard/objs/SearchResultItem;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 833
    const/4 v0, 0x2

    move v2, v0

    .line 843
    :cond_1
    :goto_0
    const/4 v3, 0x0

    .line 844
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/objs/SearchResultItem;

    .line 845
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    .line 846
    iget-object v0, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    .line 847
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 848
    iget-object v7, v1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    invoke-virtual {v7, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 849
    if-nez v3, :cond_7

    .line 850
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 852
    :goto_2
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_3
    move-object v3, v0

    .line 854
    goto :goto_1

    .line 857
    :cond_4
    if-eqz v3, :cond_5

    .line 858
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 859
    iget-object v3, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 863
    :cond_5
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 864
    new-instance v0, Lflipboard/objs/SearchResultCategory;

    invoke-direct {v0}, Lflipboard/objs/SearchResultCategory;-><init>()V

    .line 865
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    iput-object v1, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    .line 866
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0d028f

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    .line 867
    invoke-interface {p1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 869
    :cond_6
    return-void

    :cond_7
    move-object v0, v3

    goto :goto_2

    :cond_8
    move-object v0, v3

    goto :goto_3

    :cond_9
    move v2, v1

    goto/16 :goto_0
.end method

.method static synthetic b(Lflipboard/gui/FLSearchView;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    return-object v0
.end method

.method private b(Lflipboard/objs/SearchResultItem;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1145
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLSearchView:addSearchResult"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 1147
    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->G:Ljava/util/HashSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->G:Ljava/util/HashSet;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 1234
    :goto_0
    return v0

    .line 1162
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 1163
    goto :goto_0

    .line 1170
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->e:Ljava/util/Map;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchView$CollapsedItemList;

    .line 1171
    if-nez v0, :cond_6

    .line 1172
    new-instance v0, Lflipboard/gui/FLSearchView$CollapsedItemList;

    iget-object v1, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    iget v3, p1, Lflipboard/objs/SearchResultItem;->H:F

    iget v4, p1, Lflipboard/objs/SearchResultItem;->E:F

    invoke-direct {v0, p0, v1, v3, v4}, Lflipboard/gui/FLSearchView$CollapsedItemList;-><init>(Lflipboard/gui/FLSearchView;Ljava/lang/String;FF)V

    .line 1173
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->e:Ljava/util/Map;

    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1174
    new-instance v1, Lflipboard/objs/SearchResultItem;

    invoke-direct {v1}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 1175
    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    .line 1176
    sget-object v3, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 1177
    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    .line 1178
    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    .line 1179
    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    .line 1180
    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    iput-object v3, v1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    .line 1181
    iget v3, p1, Lflipboard/objs/SearchResultItem;->H:F

    iput v3, v1, Lflipboard/objs/SearchResultItem;->H:F

    .line 1182
    iget v3, p1, Lflipboard/objs/SearchResultItem;->E:F

    iput v3, v1, Lflipboard/objs/SearchResultItem;->E:F

    .line 1183
    iget-object v3, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->d:Ljava/util/HashSet;

    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1185
    iget-object v3, p0, Lflipboard/gui/FLSearchView;->d:Ljava/util/HashSet;

    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1186
    new-instance v3, Lflipboard/objs/SearchResultItem;

    invoke-direct {v3}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 1187
    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    .line 1188
    sget-object v4, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 1189
    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    .line 1190
    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->G:Ljava/lang/String;

    .line 1191
    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    .line 1192
    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->D:Ljava/lang/String;

    .line 1193
    iget v4, p1, Lflipboard/objs/SearchResultItem;->H:F

    iput v4, v3, Lflipboard/objs/SearchResultItem;->H:F

    .line 1194
    iget v4, p1, Lflipboard/objs/SearchResultItem;->E:F

    iput v4, v3, Lflipboard/objs/SearchResultItem;->E:F

    .line 1195
    iget-object v4, p1, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->L:Ljava/lang/String;

    .line 1196
    iget-object v4, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v4, v3}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    .line 1198
    :cond_2
    iget-object v3, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v3, v1}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    move-object v1, v0

    .line 1200
    :goto_1
    sget-object v0, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    iput-object v0, p1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 1201
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    .line 1202
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1204
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget v3, p0, Lflipboard/gui/FLSearchView;->J:I

    invoke-virtual {v0, p0, v3}, Lflipboard/gui/SearchListAdapter;->a(Ljava/util/Comparator;I)V

    .line 1205
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1212
    iget-boolean v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->c:Z

    if-eqz v0, :cond_4

    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x3

    if-le v0, v3, :cond_4

    .line 1214
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1216
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x4

    if-le v0, v3, :cond_4

    .line 1218
    :goto_2
    const/4 v0, 0x2

    if-ge v2, v0, :cond_3

    .line 1219
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    iget-object v3, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 1220
    iget-object v3, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1221
    iget-object v3, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v3, v0}, Lflipboard/gui/SearchListAdapter;->b(Lflipboard/objs/SearchResultItem;)I

    .line 1218
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1223
    :cond_3
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1224
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    .line 1234
    :cond_4
    :goto_3
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 1228
    :cond_5
    iget-object v0, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    iget-object v2, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 1229
    iget-object v1, v1, Lflipboard/gui/FLSearchView$CollapsedItemList;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1230
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v1, v0}, Lflipboard/gui/SearchListAdapter;->b(Lflipboard/objs/SearchResultItem;)I

    goto :goto_3

    :cond_6
    move-object v1, v0

    goto/16 :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_0

    .line 467
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-eqz v0, :cond_0

    .line 468
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 471
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 475
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_1

    .line 476
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 477
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 479
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 483
    :cond_1
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 873
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->m:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 874
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->clearAnimation()V

    .line 875
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->m:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 879
    :goto_0
    return-void

    .line 877
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->g:Lflipboard/objs/SearchResultItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/SearchListAdapter;->b(Lflipboard/objs/SearchResultItem;)I

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 883
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_1

    .line 884
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->I:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 885
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->I:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 887
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 890
    :cond_1
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->e()V

    goto :goto_0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->D:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 912
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->D:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 913
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->D:Ljava/util/TimerTask;

    .line 915
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 919
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLSearchView:clearSearchResults"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 920
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 921
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->P:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 922
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 923
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->f()V

    .line 924
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->a()V

    .line 926
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->g()V

    .line 928
    iput v1, p0, Lflipboard/gui/FLSearchView;->J:I

    .line 929
    iput v1, p0, Lflipboard/gui/FLSearchView;->K:I

    .line 930
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 931
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 933
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1240
    const-string v0, "state_state"

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    invoke-virtual {v1}, Lflipboard/gui/FLSearchView$State;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1241
    sget-object v0, Lflipboard/gui/FLSearchView;->a:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1242
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 491
    check-cast p1, Lflipboard/gui/FLTextIntf;

    .line 492
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 493
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-interface {p1}, Lflipboard/gui/FLTextIntf;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->append(Ljava/lang/CharSequence;)V

    .line 494
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 495
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 496
    return-void
.end method

.method final varargs a(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 513
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLSearchView$8;

    invoke-direct {v1, p0, p1, p2}, Lflipboard/gui/FLSearchView$8;-><init>(Lflipboard/gui/FLSearchView;Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 520
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 394
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    sget-object v3, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    if-eq v0, v3, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v0, v3, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->e()V

    sget-object v0, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->e()V

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->M:Lflipboard/gui/FLSearchView$FLSearchViewListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->M:Lflipboard/gui/FLSearchView$FLSearchViewListener;

    invoke-interface {v0}, Lflipboard/gui/FLSearchView$FLSearchViewListener;->a()V

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 1082
    sget-object v0, Lflipboard/gui/FLSearchView$Input;->b:Lflipboard/gui/FLSearchView$Input;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLSearchView;->b(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 1083
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 400
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 402
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1247
    const-string v0, "state_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1248
    const-string v0, "state_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/FLSearchView$State;->valueOf(Ljava/lang/String;)Lflipboard/gui/FLSearchView$State;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 1250
    :cond_0
    sget-object v0, Lflipboard/gui/FLSearchView;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1251
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    .line 1252
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1254
    :cond_1
    return-void
.end method

.method final varargs b(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 524
    iget-object v6, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 526
    sget-object v0, Lflipboard/gui/FLSearchView$11;->a:[I

    invoke-virtual {p1}, Lflipboard/gui/FLSearchView$Input;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 813
    :cond_0
    :goto_0
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    if-eq v6, v0, :cond_1

    .line 814
    sget-object v0, Lflipboard/gui/FLSearchView;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v6, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 815
    sget-object v0, Lflipboard/gui/FLSearchView;->b:Lflipboard/util/Log;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Search query is: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v1, v1, Lflipboard/service/FLSearchManager;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 816
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    sget-object v1, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    if-ne v0, v1, :cond_23

    .line 817
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->g()V

    .line 822
    :cond_1
    :goto_1
    return-void

    .line 529
    :pswitch_0
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 530
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 532
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->h()V

    .line 533
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    sget-object v1, Lflipboard/gui/FLSearchView$State;->b:Lflipboard/gui/FLSearchView$State;

    if-ne v0, v1, :cond_0

    .line 534
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 536
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    invoke-static {v1}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 537
    sget-object v0, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    goto :goto_0

    .line 538
    :cond_2
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 539
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 540
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 546
    :cond_3
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->i()V

    .line 547
    sget-object v0, Lflipboard/gui/FLSearchView$State;->c:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 550
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v0, :cond_4

    .line 551
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->b:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 553
    :cond_4
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 562
    :pswitch_1
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->h()V

    .line 563
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 564
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_8

    .line 565
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, p0, Lflipboard/gui/FLSearchView;->L:J

    .line 566
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    invoke-static {v0}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 567
    sget-object v0, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    goto/16 :goto_0

    .line 570
    :cond_5
    iget-boolean v1, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v1, :cond_7

    .line 572
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->i()V

    .line 574
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    iput-object v0, v1, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    .line 575
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    sget-object v2, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    iput-object v2, v1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 577
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "flipsearch/article/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    .line 578
    sget-boolean v1, Lflipboard/service/FlipboardManager;->o:Z

    if-nez v1, :cond_6

    .line 579
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v2, p0, Lflipboard/gui/FLSearchView;->f:Lflipboard/objs/SearchResultItem;

    invoke-virtual {v1, v2}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    .line 580
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v2, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    invoke-virtual {v1, v2}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    .line 581
    iget v1, p0, Lflipboard/gui/FLSearchView;->J:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lflipboard/gui/FLSearchView;->J:I

    .line 583
    :cond_6
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->d()V

    .line 585
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v2, p0, Lflipboard/gui/FLSearchView;->q:Lflipboard/service/FLSearchManager$LocalSearchObserver;

    invoke-virtual {v1, v0, v2}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/FLSearchManager$LocalSearchObserver;)V

    .line 586
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->g:Lflipboard/objs/SearchResultItem;

    invoke-virtual {v0, v1}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    .line 588
    :cond_7
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 589
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 591
    sget-object v0, Lflipboard/gui/FLSearchView$State;->b:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 592
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->h()V

    new-instance v0, Lflipboard/gui/FLSearchView$10;

    invoke-direct {v0, p0}, Lflipboard/gui/FLSearchView$10;-><init>(Lflipboard/gui/FLSearchView;)V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->D:Ljava/util/TimerTask;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->D:Ljava/util/TimerTask;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto/16 :goto_0

    .line 595
    :cond_8
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->i()V

    goto/16 :goto_0

    .line 602
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    sget-object v1, Lflipboard/gui/FLSearchView$State;->b:Lflipboard/gui/FLSearchView$State;

    if-ne v0, v1, :cond_0

    .line 603
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 605
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    invoke-static {v1}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 606
    sget-object v0, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    goto/16 :goto_0

    .line 608
    :cond_9
    sget-object v0, Lflipboard/gui/FLSearchView$State;->c:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 609
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->m:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_a

    .line 610
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->m:Landroid/widget/ProgressBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 613
    :cond_a
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 614
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v0, :cond_b

    .line 615
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->b:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 617
    :cond_b
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v2, p0, Lflipboard/gui/FLSearchView;->q:Lflipboard/service/FLSearchManager$LocalSearchObserver;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/FLSearchManager$LocalSearchObserver;)V

    .line 618
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 626
    :pswitch_3
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    if-ne v0, v1, :cond_0

    .line 627
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v0, v0, Lflipboard/service/FLSearchManager;->c:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 628
    const/4 v0, 0x2

    aget-object v0, p2, v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 629
    iget-boolean v1, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v1, :cond_f

    .line 630
    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    const-string v2, "medium"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 631
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "FLSearchView:addMediumSearchResult"

    invoke-static {v1}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->G:Ljava/util/HashSet;

    iget-object v2, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_c
    iget v1, p0, Lflipboard/gui/FLSearchView;->K:I

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    sget-object v1, Lflipboard/objs/SearchResultItem;->o:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v1, v0}, Lflipboard/gui/SearchListAdapter;->a(Lflipboard/objs/SearchResultItem;)V

    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    instance-of v1, v1, Ljava/lang/String;

    if-eqz v1, :cond_d

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    :goto_2
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->G:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget v1, p0, Lflipboard/gui/FLSearchView;->J:I

    invoke-virtual {v0, p0, v1}, Lflipboard/gui/SearchListAdapter;->a(Ljava/util/Comparator;I)V

    iget v0, p0, Lflipboard/gui/FLSearchView;->K:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/FLSearchView;->K:I

    goto/16 :goto_0

    :cond_d
    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 633
    :cond_e
    invoke-direct {p0, v0}, Lflipboard/gui/FLSearchView;->b(Lflipboard/objs/SearchResultItem;)Z

    goto/16 :goto_0

    .line 637
    :cond_f
    invoke-direct {p0, v0}, Lflipboard/gui/FLSearchView;->b(Lflipboard/objs/SearchResultItem;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 638
    iget v0, p0, Lflipboard/gui/FLSearchView;->u:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 642
    :cond_10
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iget v1, p0, Lflipboard/gui/FLSearchView;->N:I

    if-ge v0, v1, :cond_0

    .line 643
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    new-instance v1, Lflipboard/gui/FLSearchView$9;

    invoke-direct {v1, p0}, Lflipboard/gui/FLSearchView$9;-><init>(Lflipboard/gui/FLSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 659
    :pswitch_4
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    if-ne v0, v1, :cond_0

    .line 660
    const/4 v0, 0x1

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    const/4 v0, 0x2

    aget-object v0, p2, v0

    check-cast v0, Ljava/util/ArrayList;

    .line 663
    const/4 v1, 0x1

    iput-boolean v1, p0, Lflipboard/gui/FLSearchView;->w:Z

    .line 664
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, p0, Lflipboard/gui/FLSearchView;->t:I

    .line 665
    const/4 v1, 0x0

    iput v1, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 666
    const/4 v1, 0x0

    iput-boolean v1, p0, Lflipboard/gui/FLSearchView;->v:Z

    .line 668
    array-length v1, p2

    const/4 v2, 0x3

    if-le v1, v2, :cond_15

    .line 670
    sget-object v1, Lflipboard/gui/FLSearchView$ResultType;->b:Lflipboard/gui/FLSearchView$ResultType;

    invoke-virtual {v1}, Lflipboard/gui/FLSearchView$ResultType;->name()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/FLSearchView;->x:Ljava/lang/String;

    .line 672
    const/4 v1, 0x3

    aget-object v1, p2, v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 673
    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_14

    .line 674
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    .line 675
    iget-object v1, v0, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    .line 676
    iget-object v2, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    if-eqz v2, :cond_13

    .line 677
    if-nez v1, :cond_11

    .line 678
    iget-object v2, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_11

    .line 679
    iget-object v1, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SearchResultItem;

    iget-object v1, v1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 682
    :cond_11
    iget-object v2, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v2, v1}, Lflipboard/gui/SearchListAdapter;->a(Ljava/lang/String;)I

    move-result v1

    .line 683
    if-lez v1, :cond_12

    .line 684
    iget-object v2, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v2, v1}, Lflipboard/gui/SearchListAdapter;->a(I)Lflipboard/objs/SearchResultItem;

    .line 685
    iget-object v2, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v3, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    iget-object v4, v2, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v4, v1, v3}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    invoke-virtual {v2}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 686
    iget-object v2, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iput v2, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 688
    :cond_12
    iget-object v2, v0, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    if-eqz v2, :cond_13

    .line 689
    new-instance v2, Lflipboard/objs/SearchResultItem;

    invoke-direct {v2}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 690
    sget-object v3, Lflipboard/objs/SearchResultItem;->n:Ljava/lang/String;

    iput-object v3, v2, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 691
    iget-object v3, v0, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    iput-object v3, v2, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    .line 692
    iget-object v3, v0, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    iput-object v3, v2, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    .line 693
    iget-object v3, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v0, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v3, v0, v2}, Lflipboard/gui/SearchListAdapter;->a(ILflipboard/objs/SearchResultItem;)V

    .line 699
    :cond_13
    :goto_3
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 697
    :cond_14
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/SearchListAdapter;->a(I)Lflipboard/objs/SearchResultItem;

    goto :goto_3

    .line 701
    :cond_15
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v1}, Lflipboard/gui/SearchListAdapter;->a()V

    .line 703
    sget-object v1, Lflipboard/gui/FLSearchView$ResultType;->a:Lflipboard/gui/FLSearchView$ResultType;

    invoke-virtual {v1}, Lflipboard/gui/FLSearchView$ResultType;->name()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/FLSearchView;->x:Ljava/lang/String;

    .line 705
    if-eqz v0, :cond_1b

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1b

    .line 708
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 709
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/SearchResultCategory;

    iget-object v1, v1, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->h:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x1

    :goto_4
    iput-boolean v1, p0, Lflipboard/gui/FLSearchView;->v:Z

    .line 711
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_16

    .line 712
    invoke-direct {p0, v0}, Lflipboard/gui/FLSearchView;->a(Ljava/util/List;)V

    .line 715
    :cond_16
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_17
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultCategory;

    .line 716
    new-instance v3, Lflipboard/objs/SearchResultItem;

    invoke-direct {v3}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 717
    iget-object v4, v0, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    .line 718
    sget-object v4, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 721
    iget-object v4, v0, Lflipboard/objs/SearchResultCategory;->b:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    .line 722
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    iget-object v3, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    if-eqz v3, :cond_18

    .line 724
    iget-object v3, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 725
    iget v3, p0, Lflipboard/gui/FLSearchView;->u:I

    iget-object v4, v0, Lflipboard/objs/SearchResultCategory;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    iput v3, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 727
    :cond_18
    iget-object v3, v0, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    if-eqz v3, :cond_17

    .line 728
    new-instance v3, Lflipboard/objs/SearchResultItem;

    invoke-direct {v3}, Lflipboard/objs/SearchResultItem;-><init>()V

    .line 729
    sget-object v4, Lflipboard/objs/SearchResultItem;->n:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    .line 730
    iget-object v4, v0, Lflipboard/objs/SearchResultCategory;->d:Ljava/lang/String;

    iput-object v4, v3, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    .line 731
    iget-object v0, v0, Lflipboard/objs/SearchResultCategory;->a:Ljava/lang/String;

    iput-object v0, v3, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    .line 732
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 709
    :cond_19
    const/4 v1, 0x0

    goto :goto_4

    .line 737
    :cond_1a
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget-object v1, v0, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 738
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_1b

    .line 739
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 742
    :cond_1b
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_0

    .line 749
    :pswitch_5
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    if-ne v0, v1, :cond_0

    .line 750
    const/4 v0, 0x1

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 751
    sget-object v0, Lflipboard/gui/FLSearchView$State;->d:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 753
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v0, 0x2

    aget-object v0, p2, v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v8, v2, v0

    .line 754
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->s:Ljava/lang/String;

    .line 756
    array-length v0, p2

    const/4 v1, 0x3

    if-le v0, v1, :cond_1c

    .line 757
    const/4 v0, 0x3

    aget-object v0, p2, v0

    instance-of v0, v0, Ljava/lang/Throwable;

    if-eqz v0, :cond_1c

    .line 758
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLSearchView;->w:Z

    .line 762
    :cond_1c
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->f()V

    .line 763
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 765
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v0, v0, Lflipboard/service/FLSearchManager;->d:Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->b:Lflipboard/service/Flap$SearchType;

    if-ne v0, v1, :cond_1e

    .line 767
    iget v0, p0, Lflipboard/gui/FLSearchView;->J:I

    iget v1, p0, Lflipboard/gui/FLSearchView;->K:I

    add-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/FLSearchView;->J:I

    .line 769
    iget v0, p0, Lflipboard/gui/FLSearchView;->K:I

    const/16 v1, 0xf

    if-ge v0, v1, :cond_1d

    .line 771
    sget-object v0, Lflipboard/gui/FLSearchView$State;->c:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 772
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v1, v0, Lflipboard/service/FLSearchManager;->c:Ljava/lang/String;

    .line 773
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->a:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    .line 785
    :cond_1d
    :goto_6
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->s:Ljava/lang/String;

    iget v0, p0, Lflipboard/gui/FLSearchView;->u:I

    iget-boolean v2, p0, Lflipboard/gui/FLSearchView;->v:Z

    iget v3, p0, Lflipboard/gui/FLSearchView;->t:I

    iget-object v4, p0, Lflipboard/gui/FLSearchView;->x:Ljava/lang/String;

    iget-object v5, p0, Lflipboard/gui/FLSearchView;->y:Ljava/lang/String;

    iget-boolean v7, p0, Lflipboard/gui/FLSearchView;->w:Z

    new-instance v10, Lflipboard/objs/UsageEventV2;

    sget-object v11, Lflipboard/objs/UsageEventV2$EventAction;->y:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v12, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v10, v11, v12}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v11, Lflipboard/objs/UsageEventV2$CommonEventData;->w:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v10, v11, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->c:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v10, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v10, v0, v5}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v4, Lflipboard/objs/UsageEventV2$CommonEventData;->B:Lflipboard/objs/UsageEventV2$CommonEventData;

    if-eqz v7, :cond_20

    const/4 v0, 0x1

    :goto_7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v10, v4, v0}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->z:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v10, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->G:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v10, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    const-string v1, "top_result_offered"

    if-eqz v2, :cond_21

    const/4 v0, 0x1

    :goto_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v10, v1, v0}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v0, "number_categories"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v10, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v10}, Lflipboard/objs/UsageEventV2;->a()V

    goto/16 :goto_0

    .line 775
    :cond_1e
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v0, v0, Lflipboard/service/FLSearchManager;->d:Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->c:Lflipboard/service/Flap$SearchType;

    if-ne v0, v1, :cond_1f

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_1f

    .line 776
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->S:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 777
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lflipboard/service/Flap$SearchType;->a:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    const/4 v4, 0x0

    const/4 v5, -0x1

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    goto/16 :goto_6

    .line 779
    :cond_1f
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v0, v0, Lflipboard/service/FLSearchManager;->d:Lflipboard/service/Flap$SearchType;

    sget-object v1, Lflipboard/service/Flap$SearchType;->a:Lflipboard/service/Flap$SearchType;

    if-ne v0, v1, :cond_1d

    .line 780
    sget-object v0, Lflipboard/gui/FLSearchView$ResultType;->c:Lflipboard/gui/FLSearchView$ResultType;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView$ResultType;->name()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->x:Ljava/lang/String;

    .line 781
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLSearchView;->v:Z

    .line 782
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->d:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLSearchView;->t:I

    goto/16 :goto_6

    .line 785
    :cond_20
    const/4 v0, 0x0

    goto :goto_7

    :cond_21
    const/4 v0, 0x0

    goto :goto_8

    .line 792
    :pswitch_6
    const/4 v0, 0x0

    aget-object v0, p2, v0

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->q:Lflipboard/service/FLSearchManager$LocalSearchObserver;

    if-ne v0, v1, :cond_0

    .line 793
    const/4 v0, 0x1

    aget-object v0, p2, v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    .line 794
    const/4 v0, 0x2

    aget-object v0, p2, v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->G:Ljava/util/HashSet;

    .line 797
    iget-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    if-nez v0, :cond_0

    .line 798
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->H:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_22

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 800
    iget-object v2, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    iget v3, p0, Lflipboard/gui/FLSearchView;->J:I

    iget-object v4, v2, Lflipboard/gui/SearchListAdapter;->a:Ljava/util/List;

    invoke-interface {v4, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-virtual {v2}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    .line 801
    iget v0, p0, Lflipboard/gui/FLSearchView;->J:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/FLSearchView;->J:I

    goto :goto_9

    .line 803
    :cond_22
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    invoke-virtual {v0}, Lflipboard/gui/SearchListAdapter;->notifyDataSetChanged()V

    goto/16 :goto_0

    .line 819
    :cond_23
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->d()V

    goto/16 :goto_1

    .line 526
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1074
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 439
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLSearchView:onSearchClicked"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 440
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    sget-object v1, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    if-ne v0, v1, :cond_0

    .line 441
    invoke-direct {p0}, Lflipboard/gui/FLSearchView;->e()V

    .line 442
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 446
    :cond_0
    return-void
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 45
    check-cast p1, Lflipboard/objs/SearchResultItem;

    check-cast p2, Lflipboard/objs/SearchResultItem;

    iget v2, p1, Lflipboard/objs/SearchResultItem;->E:F

    iget v3, p2, Lflipboard/objs/SearchResultItem;->E:F

    cmpl-float v4, v3, v2

    if-eqz v4, :cond_2

    sub-float v2, v3, v2

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p2, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget v2, p1, Lflipboard/objs/SearchResultItem;->H:F

    iget v3, p2, Lflipboard/objs/SearchResultItem;->H:F

    cmpl-float v4, v3, v2

    if-eqz v4, :cond_3

    sub-float v2, v3, v2

    cmpl-float v2, v2, v5

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p2, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v3, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p1, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    iget-object v3, p2, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    if-eqz v2, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_4
    invoke-static {p1}, Lflipboard/gui/FLSearchView;->a(Lflipboard/objs/SearchResultItem;)F

    move-result v2

    invoke-static {p2}, Lflipboard/gui/FLSearchView;->a(Lflipboard/objs/SearchResultItem;)F

    move-result v3

    sub-float v4, v3, v2

    cmpl-float v4, v4, v5

    if-gtz v4, :cond_0

    cmpl-float v0, v3, v2

    if-nez v0, :cond_5

    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public getActiveTimeInMills()J
    .locals 2

    .prologue
    .line 409
    iget-wide v0, p0, Lflipboard/gui/FLSearchView;->c:J

    return-wide v0
.end method

.method public getNewSearchAdapter()Lflipboard/gui/SearchListAdapter;
    .locals 1

    .prologue
    .line 1398
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    return-object v0
.end method

.method public getSearchViewListener()Lflipboard/gui/FLSearchView$FLSearchViewListener;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->M:Lflipboard/gui/FLSearchView$FLSearchViewListener;

    return-object v0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1062
    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 1064
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1065
    iget-object v2, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1066
    sget-object v0, Lflipboard/gui/FLSearchView$Input;->b:Lflipboard/gui/FLSearchView$Input;

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLSearchView;->b(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 1067
    const/4 v0, 0x1

    .line 1069
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 10

    .prologue
    const v9, 0x7f0300e9

    const/4 v8, 0x0

    .line 275
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 277
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->r:Lflipboard/activities/FlipboardActivity;

    .line 279
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 280
    new-instance v1, Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-direct {v1, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    .line 281
    new-instance v1, Lflipboard/gui/SearchListAdapter;

    invoke-direct {v1, v0}, Lflipboard/gui/SearchListAdapter;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, p0, Lflipboard/gui/FLSearchView;->F:Lflipboard/gui/SearchListAdapter;

    .line 283
    const v0, 0x7f0a02d4

    invoke-virtual {p0, v0}, Lflipboard/gui/FLSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    .line 284
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    iget-object v1, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 285
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 288
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_0

    .line 289
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLSearchView;->Q:Z

    .line 291
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    new-instance v1, Lflipboard/gui/FLSearchView$4;

    invoke-direct {v1, p0}, Lflipboard/gui/FLSearchView$4;-><init>(Lflipboard/gui/FLSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 312
    const v0, 0x7f0a0139

    invoke-virtual {p0, v0}, Lflipboard/gui/FLSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    .line 315
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->e:Ljava/util/Map;

    .line 316
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->P:Ljava/util/List;

    .line 317
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->d:Ljava/util/HashSet;

    .line 319
    new-instance v0, Lflipboard/objs/SearchResultItem;

    invoke-direct {v0}, Lflipboard/objs/SearchResultItem;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->f:Lflipboard/objs/SearchResultItem;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->f:Lflipboard/objs/SearchResultItem;

    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0259

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->r:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->f:Lflipboard/objs/SearchResultItem;

    sget-object v1, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    new-instance v0, Lflipboard/objs/SearchResultItem;

    invoke-direct {v0}, Lflipboard/objs/SearchResultItem;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->g:Lflipboard/objs/SearchResultItem;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->g:Lflipboard/objs/SearchResultItem;

    sget-object v1, Lflipboard/objs/SearchResultItem;->p:Ljava/lang/String;

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    new-instance v0, Lflipboard/gui/FLSearchView$SearchResultAction;

    invoke-direct {v0, p0}, Lflipboard/gui/FLSearchView$SearchResultAction;-><init>(Lflipboard/gui/FLSearchView;)V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->i:Lflipboard/gui/FLSearchView$SearchResultAction;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->i:Lflipboard/gui/FLSearchView$SearchResultAction;

    const/4 v1, 0x0

    iput v1, v0, Lflipboard/gui/FLSearchView$SearchResultAction;->k:F

    new-instance v0, Lflipboard/objs/SearchResultItem;

    invoke-direct {v0}, Lflipboard/objs/SearchResultItem;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d025a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->s:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    const-string v1, "search_result_fl_icon"

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->t:Ljava/lang/String;

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->h:Lflipboard/objs/SearchResultItem;

    const-string v1, "flipsearch"

    iput-object v1, v0, Lflipboard/objs/SearchResultItem;->v:Ljava/lang/String;

    .line 321
    const v0, 0x7f0a02cd

    invoke-virtual {p0, v0}, Lflipboard/gui/FLSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->m:Landroid/widget/ProgressBar;

    .line 323
    const v0, 0x7f0a0138

    invoke-virtual {p0, v0}, Lflipboard/gui/FLSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    .line 324
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 325
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    new-instance v1, Lflipboard/gui/FLSearchView$5;

    invoke-direct {v1, p0}, Lflipboard/gui/FLSearchView$5;-><init>(Lflipboard/gui/FLSearchView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 336
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 337
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 340
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_6

    .line 341
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->l:Landroid/widget/ViewFlipper;

    const v1, 0x7f0a02a1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    .line 342
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->V:Lflipboard/objs/ConfigPopularSearches;

    if-nez v1, :cond_1

    new-instance v1, Lflipboard/objs/ConfigPopularSearches;

    invoke-direct {v1}, Lflipboard/objs/ConfigPopularSearches;-><init>()V

    iput-object v1, v0, Lflipboard/service/FlipboardManager;->V:Lflipboard/objs/ConfigPopularSearches;

    :cond_1
    iget-object v2, v0, Lflipboard/service/FlipboardManager;->V:Lflipboard/objs/ConfigPopularSearches;

    .line 343
    iget-object v0, v2, Lflipboard/objs/ConfigPopularSearches;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 344
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    const v1, 0x7f0a004f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    .line 345
    iget-object v1, v2, Lflipboard/objs/ConfigPopularSearches;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 347
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    const v1, 0x7f0a02a2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 348
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 349
    iget-object v1, p0, Lflipboard/gui/FLSearchView;->k:Landroid/view/View;

    const v3, 0x7f0a02a3

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 350
    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 353
    iget-object v3, v2, Lflipboard/objs/ConfigPopularSearches;->b:Ljava/util/List;

    if-eqz v3, :cond_6

    .line 354
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 355
    iget-object v2, v2, Lflipboard/objs/ConfigPopularSearches;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/ConfigPopularSearches$Category;

    .line 357
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLLabelTextView;

    .line 359
    iget-object v4, v2, Lflipboard/objs/ConfigPopularSearches$Category;->a:Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 360
    iget-object v4, v2, Lflipboard/objs/ConfigPopularSearches$Category;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 362
    :cond_3
    const v4, 0x7f080069

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 363
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 366
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0300e8

    invoke-static {v3, v4, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 367
    iget-object v4, v2, Lflipboard/objs/ConfigPopularSearches$Category;->b:Ljava/util/List;

    if-eqz v4, :cond_5

    .line 368
    iget-object v2, v2, Lflipboard/objs/ConfigPopularSearches$Category;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 369
    invoke-virtual {p0}, Lflipboard/gui/FLSearchView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v9, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lflipboard/gui/FLLabelTextView;

    .line 370
    if-eqz v2, :cond_4

    .line 371
    invoke-virtual {v4, v2}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    const v2, 0x7f0201c7

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v4, v2}, Lflipboard/gui/FLLabelTextView;->setTextColor(I)V

    .line 373
    new-instance v2, Lflipboard/gui/FLSearchView$6;

    invoke-direct {v2, p0}, Lflipboard/gui/FLSearchView$6;-><init>(Lflipboard/gui/FLSearchView;)V

    invoke-virtual {v4, v2}, Lflipboard/gui/FLLabelTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    :cond_4
    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 386
    :cond_5
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 390
    :cond_6
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 221
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 223
    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_0

    .line 224
    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 226
    :cond_0
    instance-of v0, v0, Lflipboard/gui/SearchListAdapter;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflipboard/objs/SearchResultItem;

    .line 229
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 230
    iget-object v0, v4, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/SearchResultItem;->n:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    sget-object v0, Lflipboard/gui/FLSearchView$State;->c:Lflipboard/gui/FLSearchView$State;

    iput-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    .line 232
    iput v3, p0, Lflipboard/gui/FLSearchView;->u:I

    .line 233
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    sget-object v2, Lflipboard/service/Flap$SearchType;->d:Lflipboard/service/Flap$SearchType;

    iget-object v3, p0, Lflipboard/gui/FLSearchView;->p:Lflipboard/service/Flap$SearchObserver;

    iget-object v4, v4, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lflipboard/service/FLSearchManager;->a(Ljava/lang/String;Lflipboard/service/Flap$SearchType;Lflipboard/service/Flap$SearchObserver;Ljava/lang/String;I)V

    .line 271
    :cond_1
    :goto_0
    return-void

    .line 234
    :cond_2
    iget-object v0, v4, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/SearchResultItem;->g:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    check-cast v4, Lflipboard/gui/FLSearchView$CollapsedItemList;

    invoke-virtual {v4}, Lflipboard/gui/FLSearchView$CollapsedItemList;->a()V

    goto :goto_0

    .line 236
    :cond_3
    iget-object v0, v4, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/SearchResultItem;->f:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, v4, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    sget-object v5, Lflipboard/objs/SearchResultItem;->e:Ljava/lang/String;

    .line 237
    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 238
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 239
    const-string v6, "search"

    .line 240
    const-string v0, "source"

    invoke-virtual {v5, v0, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v0, "originSectionIdentifier"

    iget-object v7, p0, Lflipboard/gui/FLSearchView;->T:Ljava/lang/String;

    invoke-virtual {v5, v0, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    new-instance v7, Lflipboard/service/Section;

    invoke-direct {v7, v4}, Lflipboard/service/Section;-><init>(Lflipboard/objs/SearchResultItem;)V

    .line 245
    new-instance v8, Lflipboard/objs/UsageEventV2;

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->z:Lflipboard/objs/UsageEventV2$EventAction;

    sget-object v9, Lflipboard/objs/UsageEventV2$EventCategory;->i:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v8, v0, v9}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 246
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->G:Lflipboard/objs/UsageEventV2$CommonEventData;

    invoke-virtual {v8, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 247
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v1, v4, Lflipboard/objs/SearchResultItem;->w:Ljava/lang/Object;

    invoke-virtual {v8, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 248
    const-string v0, "category"

    iget-object v1, v4, Lflipboard/objs/SearchResultItem;->x:Ljava/lang/String;

    invoke-virtual {v8, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    if-ne p3, v2, :cond_5

    .line 253
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/SearchResultItem;

    .line 254
    if-eqz v0, :cond_5

    iget-object v1, v0, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v0, v0, Lflipboard/objs/SearchResultItem;->F:Ljava/lang/String;

    sget-object v1, Lflipboard/objs/SearchResultItem;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    .line 258
    :goto_1
    const-string v1, "top_result_selected"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v1, v0}, Lflipboard/objs/UsageEventV2;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 259
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    const-string v1, "main_search"

    invoke-virtual {v8, v0, v1}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 260
    invoke-virtual {v8}, Lflipboard/objs/UsageEventV2;->a()V

    .line 263
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->a:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_4

    .line 264
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->r:Lflipboard/activities/FlipboardActivity;

    check-cast v0, Lflipboard/activities/ContentDrawerActivity;

    invoke-virtual {v0, v7, v5}, Lflipboard/activities/ContentDrawerActivity;->a(Lflipboard/service/Section;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 265
    :cond_4
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->R:Lflipboard/service/FlipboardManager$RootScreenStyle;

    sget-object v1, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v1, :cond_1

    .line 266
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->r:Lflipboard/activities/FlipboardActivity;

    invoke-static {v0, v7, v6}, Lflipboard/util/ActivityUtil;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1078
    return-void
.end method

.method public setActiveTimeInMills(J)V
    .locals 1

    .prologue
    .line 405
    iput-wide p1, p0, Lflipboard/gui/FLSearchView;->c:J

    .line 406
    return-void
.end method

.method public setDefaultSearchList(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1387
    iput-object p1, p0, Lflipboard/gui/FLSearchView;->I:Ljava/util/List;

    .line 1389
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLSearchView;->z:Lflipboard/gui/FLSearchView$State;

    sget-object v1, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    if-ne v0, v1, :cond_0

    .line 1390
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 1391
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->E:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1393
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->O:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->setSelectionAfterHeaderView()V

    .line 1395
    :cond_0
    return-void
.end method

.method public setOriginSectionId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1379
    iput-object p1, p0, Lflipboard/gui/FLSearchView;->T:Ljava/lang/String;

    .line 1380
    return-void
.end method

.method public setSearchQuery(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 449
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLSearchView:onSearchClicked"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 450
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lflipboard/gui/FLSearchView;->j:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 453
    :cond_0
    return-void
.end method

.method public setSearchViewListener(Lflipboard/gui/FLSearchView$FLSearchViewListener;)V
    .locals 0

    .prologue
    .line 456
    iput-object p1, p0, Lflipboard/gui/FLSearchView;->M:Lflipboard/gui/FLSearchView$FLSearchViewListener;

    .line 457
    return-void
.end method
