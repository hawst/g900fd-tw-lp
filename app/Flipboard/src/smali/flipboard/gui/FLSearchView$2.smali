.class Lflipboard/gui/FLSearchView$2;
.super Ljava/lang/Object;
.source "FLSearchView.java"

# interfaces
.implements Lflipboard/service/Flap$SearchObserver;


# instance fields
.field final synthetic a:Lflipboard/gui/FLSearchView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLSearchView;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 189
    sget-object v0, Lflipboard/gui/FLSearchView;->b:Lflipboard/util/Log;

    new-array v0, v4, [Ljava/lang/Object;

    iget-object v1, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    iget-object v1, v1, Lflipboard/gui/FLSearchView;->o:Lflipboard/service/FLSearchManager;

    iget-object v1, v1, Lflipboard/service/FLSearchManager;->c:Ljava/lang/String;

    aput-object v1, v0, v3

    .line 190
    iget-object v0, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->f:Lflipboard/gui/FLSearchView$Input;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    aput-object p1, v2, v4

    const/4 v3, 0x2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchView;->a(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 191
    return-void
.end method

.method public final a(Ljava/lang/String;Lflipboard/objs/SearchResultItem;)V
    .locals 4

    .prologue
    .line 174
    iget-object v0, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->d:Lflipboard/gui/FLSearchView$Input;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchView;->a(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 175
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;IJ)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;IJ)V"
        }
    .end annotation

    .prologue
    .line 184
    iget-object v0, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->e:Lflipboard/gui/FLSearchView$Input;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchView;->a(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/SearchResultCategory;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 179
    iget-object v0, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->e:Lflipboard/gui/FLSearchView$Input;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchView;->a(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 180
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Ljava/lang/String;J)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 195
    sget-object v0, Lflipboard/gui/FLSearchView;->b:Lflipboard/util/Log;

    const-string v1, "search error: %s"

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 196
    iget-object v0, p0, Lflipboard/gui/FLSearchView$2;->a:Lflipboard/gui/FLSearchView;

    sget-object v1, Lflipboard/gui/FLSearchView$Input;->f:Lflipboard/gui/FLSearchView$Input;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    aput-object p2, v2, v4

    const/4 v3, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/gui/FLSearchView;->a(Lflipboard/gui/FLSearchView$Input;[Ljava/lang/Object;)V

    .line 197
    return-void
.end method
