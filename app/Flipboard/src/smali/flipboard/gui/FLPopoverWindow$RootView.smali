.class Lflipboard/gui/FLPopoverWindow$RootView;
.super Landroid/widget/FrameLayout;
.source "FLPopoverWindow.java"


# instance fields
.field a:Lflipboard/gui/FLPopoverWindow$TriangleView;

.field final synthetic b:Lflipboard/gui/FLPopoverWindow;

.field private c:Z

.field private d:I

.field private e:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(Lflipboard/gui/FLPopoverWindow;Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, -0x2

    .line 336
    iput-object p1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    .line 337
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 340
    const v0, 0x7f030130

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    .line 341
    iget-object v0, p1, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLPopoverWindow$RootView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 344
    new-instance v0, Lflipboard/gui/FLPopoverWindow$TriangleView;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lflipboard/gui/FLPopoverWindow$TriangleView;-><init>(Lflipboard/gui/FLPopoverWindow;Landroid/content/Context;B)V

    iput-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    .line 345
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p1, Lflipboard/gui/FLPopoverWindow;->f:I

    iget v3, p1, Lflipboard/gui/FLPopoverWindow;->g:I

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLPopoverWindow$RootView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 346
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 393
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v2}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 394
    iget-boolean v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->c:Z

    if-eqz v1, :cond_0

    .line 395
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v2}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    .line 397
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    const/16 v2, 0xb4

    iput v2, v1, Lflipboard/gui/FLPopoverWindow$TriangleView;->a:I

    .line 398
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow;->d:Lflipboard/gui/FLPopoverWindow$RootView;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget v2, v2, Lflipboard/gui/FLPopoverWindow;->h:I

    invoke-virtual {v1, v2}, Lflipboard/gui/FLPopoverWindow$TriangleView;->a(I)V

    .line 405
    :goto_0
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    iget v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    iget-object v3, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v3}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v0

    iget v4, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    iget-object v5, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v5}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Lflipboard/gui/FLPopoverWindow$TriangleView;->layout(IIII)V

    .line 408
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v1}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/FLPopoverWindow$RootView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 409
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v0, v0, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 410
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 411
    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v2, v2, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v3

    if-ge v2, v3, :cond_1

    move v0, v1

    .line 412
    :goto_1
    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 413
    iget-boolean v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v2, v2, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v2}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    :goto_2
    iput v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    .line 414
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v0, v0, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    iget v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    iget-object v3, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v3, v3, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget v4, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    iget-object v5, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v5, v5, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 415
    return-void

    .line 401
    :cond_0
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->d:I

    .line 402
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    iput v6, v1, Lflipboard/gui/FLPopoverWindow$TriangleView;->a:I

    .line 403
    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow;->d:Lflipboard/gui/FLPopoverWindow$RootView;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget v2, v2, Lflipboard/gui/FLPopoverWindow;->i:I

    invoke-virtual {v1, v2}, Lflipboard/gui/FLPopoverWindow$TriangleView;->a(I)V

    goto/16 :goto_0

    .line 411
    :cond_1
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_1

    .line 413
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v2}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v8, -0x80000000

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 366
    iget-object v2, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    iget-object v3, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v3}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    iget v3, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v4}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLPopoverWindow$TriangleView;->measure(II)V

    .line 368
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 369
    iget-object v3, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v3, v3, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 371
    new-instance v3, Landroid/graphics/Rect;

    aget v4, v2, v1

    aget v5, v2, v0

    aget v6, v2, v1

    iget-object v7, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v7, v7, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    aget v2, v2, v0

    iget-object v7, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v7, v7, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v7

    add-int/2addr v2, v7

    invoke-direct {v3, v4, v5, v6, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    .line 373
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 374
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 376
    iget-object v4, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v5, v5, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    div-int/lit8 v5, v3, 0x2

    if-lt v4, v5, :cond_0

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->c:Z

    .line 379
    iget-boolean v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->c:Z

    if-eqz v0, :cond_1

    .line 380
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v1}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 384
    :goto_1
    invoke-virtual {p0}, Lflipboard/gui/FLPopoverWindow$RootView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f09009a

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 385
    iget-object v4, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v4, v4, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v4, v1, v0}, Landroid/view/View;->measure(II)V

    .line 387
    invoke-virtual {p0, v2, v3}, Lflipboard/gui/FLPopoverWindow$RootView;->setMeasuredDimension(II)V

    .line 388
    return-void

    :cond_0
    move v0, v1

    .line 376
    goto :goto_0

    .line 382
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v3, v0

    iget-object v1, p0, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v1}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v0, v0, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    invoke-static {p1, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 352
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    invoke-virtual {v0}, Lflipboard/gui/FLPopoverWindow;->dismiss()V

    .line 353
    const/4 v0, 0x1

    .line 355
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
