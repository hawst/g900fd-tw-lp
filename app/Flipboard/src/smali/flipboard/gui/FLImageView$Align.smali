.class public final enum Lflipboard/gui/FLImageView$Align;
.super Ljava/lang/Enum;
.source "FLImageView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLImageView$Align;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLImageView$Align;

.field public static final enum b:Lflipboard/gui/FLImageView$Align;

.field private static final synthetic c:[Lflipboard/gui/FLImageView$Align;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 80
    new-instance v0, Lflipboard/gui/FLImageView$Align;

    const-string v1, "FILL"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLImageView$Align;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    .line 81
    new-instance v0, Lflipboard/gui/FLImageView$Align;

    const-string v1, "FIT"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLImageView$Align;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    .line 79
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/FLImageView$Align;

    sget-object v1, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/FLImageView$Align;->c:[Lflipboard/gui/FLImageView$Align;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLImageView$Align;
    .locals 1

    .prologue
    .line 79
    const-class v0, Lflipboard/gui/FLImageView$Align;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView$Align;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLImageView$Align;
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lflipboard/gui/FLImageView$Align;->c:[Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v0}, [Lflipboard/gui/FLImageView$Align;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLImageView$Align;

    return-object v0
.end method
