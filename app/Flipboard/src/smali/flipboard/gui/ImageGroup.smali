.class public Lflipboard/gui/ImageGroup;
.super Ljava/lang/Object;
.source "ImageGroup.java"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Image;",
            ">;"
        }
    .end annotation
.end field

.field b:Lflipboard/gui/ImageGroup$ImageGroupLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    .line 22
    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->a:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    iput-object v0, p0, Lflipboard/gui/ImageGroup;->b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 85
    return-void
.end method

.method static a(IZ)Lflipboard/gui/ImageGroup$ImageGroupLayout;
    .locals 1

    .prologue
    .line 56
    packed-switch p0, :pswitch_data_0

    .line 65
    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->f:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    :goto_0
    return-object v0

    .line 58
    :pswitch_0
    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->a:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    goto :goto_0

    .line 60
    :pswitch_1
    if-eqz p1, :cond_0

    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->c:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    goto :goto_0

    :cond_0
    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    goto :goto_0

    .line 62
    :pswitch_2
    if-eqz p1, :cond_1

    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->e:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    goto :goto_0

    :cond_1
    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->d:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    goto :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lflipboard/objs/Image;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Image;

    return-object v0
.end method

.method public final b(I)Lflipboard/gui/ImageGroup$ImageLayout;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lflipboard/gui/ImageGroup;->b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    invoke-static {v0, p1}, Lflipboard/gui/ImageGroup$ImageGroupLayout;->a(Lflipboard/gui/ImageGroup$ImageGroupLayout;I)Lflipboard/gui/ImageGroup$ImageLayout;

    move-result-object v0

    return-object v0
.end method
