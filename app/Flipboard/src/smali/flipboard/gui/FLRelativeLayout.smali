.class public Lflipboard/gui/FLRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "FLRelativeLayout.java"


# static fields
.field static n:Z


# instance fields
.field private a:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ar"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lflipboard/gui/FLRelativeLayout;->n:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method private static a(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/16 v6, 0x9

    const/4 v5, 0x7

    const/4 v4, 0x5

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    instance-of v0, p0, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v0, :cond_5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_5

    sget-boolean v0, Lflipboard/gui/FLRelativeLayout;->n:Z

    if-eqz v0, :cond_5

    .line 62
    check-cast p0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 63
    invoke-virtual {p0}, Landroid/widget/RelativeLayout$LayoutParams;->getRules()[I

    move-result-object v0

    .line 66
    aget v1, v0, v6

    if-eqz v1, :cond_0

    const/16 v1, 0x14

    aget v1, v0, v1

    if-eqz v1, :cond_0

    .line 67
    invoke-virtual {p0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 69
    :cond_0
    const/16 v1, 0xb

    aget v1, v0, v1

    if-eqz v1, :cond_1

    const/16 v1, 0x15

    aget v1, v0, v1

    if-eqz v1, :cond_1

    .line 70
    const/16 v1, 0xb

    invoke-virtual {p0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 72
    :cond_1
    aget v1, v0, v2

    if-eqz v1, :cond_2

    const/16 v1, 0x10

    aget v1, v0, v1

    if-eqz v1, :cond_2

    .line 73
    invoke-virtual {p0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 75
    :cond_2
    aget v1, v0, v3

    if-eqz v1, :cond_3

    const/16 v1, 0x11

    aget v1, v0, v1

    if-eqz v1, :cond_3

    .line 76
    invoke-virtual {p0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 78
    :cond_3
    aget v1, v0, v4

    if-eqz v1, :cond_4

    const/16 v1, 0x12

    aget v1, v0, v1

    if-eqz v1, :cond_4

    .line 79
    invoke-virtual {p0, v4}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 81
    :cond_4
    aget v1, v0, v5

    if-eqz v1, :cond_5

    const/16 v1, 0x13

    aget v0, v0, v1

    if-eqz v0, :cond_5

    .line 82
    invoke-virtual {p0, v5}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    .line 85
    :cond_5
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p3}, Lflipboard/gui/FLRelativeLayout;->a(Landroid/view/ViewGroup$LayoutParams;)V

    .line 41
    invoke-super {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 42
    return-void
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1

    .prologue
    .line 47
    invoke-static {p3}, Lflipboard/gui/FLRelativeLayout;->a(Landroid/view/ViewGroup$LayoutParams;)V

    .line 48
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 97
    iget-object v0, p0, Lflipboard/gui/FLRelativeLayout;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lflipboard/gui/FLRelativeLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 100
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->draw(Landroid/graphics/Canvas;)V

    .line 101
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 102
    return-void
.end method

.method public setClipRect(Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lflipboard/gui/FLRelativeLayout;->a:Landroid/graphics/Rect;

    .line 90
    invoke-virtual {p0}, Lflipboard/gui/FLRelativeLayout;->invalidate()V

    .line 91
    return-void
.end method
