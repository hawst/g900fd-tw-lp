.class Lflipboard/gui/FLBitmapView$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "FLBitmapView.java"


# instance fields
.field final synthetic a:Lflipboard/gui/FLBitmapView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLBitmapView;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lflipboard/gui/FLBitmapView$1;->a:Lflipboard/gui/FLBitmapView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 150
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 151
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$1;->a:Lflipboard/gui/FLBitmapView;

    sget-object v1, Lflipboard/gui/FLBitmapView$LayerType;->a:Lflipboard/gui/FLBitmapView$LayerType;

    invoke-static {v0, v1}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView;Lflipboard/gui/FLBitmapView$LayerType;)V

    .line 152
    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->b()V

    .line 153
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 143
    iget-object v0, p0, Lflipboard/gui/FLBitmapView$1;->a:Lflipboard/gui/FLBitmapView;

    sget-object v1, Lflipboard/gui/FLBitmapView$LayerType;->a:Lflipboard/gui/FLBitmapView$LayerType;

    invoke-static {v0, v1}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView;Lflipboard/gui/FLBitmapView$LayerType;)V

    .line 144
    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->b()V

    .line 145
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 135
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 136
    invoke-static {}, Lflipboard/gui/flipping/FlipUtil;->a()V

    .line 137
    return-void
.end method
