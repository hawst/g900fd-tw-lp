.class public Lflipboard/gui/EditableListView;
.super Landroid/widget/ListView;
.source "EditableListView.java"


# instance fields
.field public a:Z

.field b:Lflipboard/gui/EditableListView$DragListener;

.field c:Z

.field d:I

.field e:I

.field f:I

.field g:I

.field h:Landroid/widget/ImageView;

.field i:Landroid/view/View;

.field j:Ljava/util/TimerTask;

.field public k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 45
    const/16 v0, 0x35

    iput v0, p0, Lflipboard/gui/EditableListView;->k:I

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    const/16 v0, 0x35

    iput v0, p0, Lflipboard/gui/EditableListView;->k:I

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    const/16 v0, 0x35

    iput v0, p0, Lflipboard/gui/EditableListView;->k:I

    .line 60
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->setVerticalFadingEdgeEnabled(Z)V

    .line 61
    return-void
.end method

.method static synthetic a(Lflipboard/gui/EditableListView;)I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getMinTopY()I

    move-result v0

    return v0
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 266
    iget-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 267
    iget-object v0, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    if-eqz v0, :cond_0

    .line 269
    iget v0, p0, Lflipboard/gui/EditableListView;->d:I

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 270
    iget-object v0, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    iget v2, p0, Lflipboard/gui/EditableListView;->d:I

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/EditableListView$DragListener;->b(Ljava/lang/Object;)V

    .line 273
    :cond_0
    iget-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 275
    iget-object v1, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 277
    iput-object v3, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    .line 279
    :cond_1
    iget-object v0, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 284
    iget-object v0, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->destroyDrawingCache()V

    .line 285
    iget-object v0, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 286
    iput-object v3, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    .line 288
    :cond_2
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 252
    iget-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .line 254
    iget v1, p0, Lflipboard/gui/EditableListView;->f:I

    sub-int v1, p1, v1

    iget v2, p0, Lflipboard/gui/EditableListView;->g:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 255
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    .line 256
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    .line 257
    iget-object v2, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 259
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/EditableListView;I)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lflipboard/gui/EditableListView;->a(I)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    .line 351
    :cond_0
    return-void
.end method

.method static synthetic b(Lflipboard/gui/EditableListView;)Z
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lflipboard/gui/EditableListView;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->b()V

    return-void
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 358
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getVisibleTop()I

    move-result v1

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    if-le v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method static synthetic d(Lflipboard/gui/EditableListView;)I
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getMaxTopY()I

    move-result v0

    return v0
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 362
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getLastVisiblePosition()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getVisibleBottom()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lflipboard/gui/EditableListView;)Z
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->d()Z

    move-result v0

    return v0
.end method

.method private getMaxTopY()I
    .locals 3

    .prologue
    .line 385
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getVisibleBottom()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 386
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_0

    .line 387
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getLastVisiblePosition()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 389
    :cond_0
    return v0
.end method

.method private getMinTopY()I
    .locals 2

    .prologue
    .line 377
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getVisibleTop()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 378
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_0

    .line 379
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 381
    :cond_0
    return v0
.end method

.method private getVisibleBottom()I
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getHeight()I

    move-result v0

    return v0
.end method

.method private getVisibleTop()I
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    .line 100
    iget-boolean v0, p0, Lflipboard/gui/EditableListView;->a:Z

    if-nez v0, :cond_0

    .line 101
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 172
    :goto_0
    return v0

    .line 104
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 105
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 106
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 109
    if-nez v0, :cond_1

    int-to-double v4, v1

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getWidth()I

    move-result v3

    int-to-double v6, v3

    const-wide v8, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v6, v8

    cmpl-double v3, v4, v6

    if-lez v3, :cond_1

    .line 110
    const/4 v3, 0x1

    iput-boolean v3, p0, Lflipboard/gui/EditableListView;->c:Z

    .line 113
    :cond_1
    iget-boolean v3, p0, Lflipboard/gui/EditableListView;->c:Z

    if-nez v3, :cond_2

    .line 114
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 117
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 161
    :pswitch_0
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->b()V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/EditableListView;->c:Z

    .line 163
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->a()V

    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v2}, Lflipboard/gui/EditableListView;->pointToPosition(II)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getVisibleTop()I

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-gt v2, v1, :cond_a

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v0

    :cond_3
    :goto_1
    iput v0, p0, Lflipboard/gui/EditableListView;->e:I

    .line 166
    iget-object v0, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    if-eqz v0, :cond_4

    iget v0, p0, Lflipboard/gui/EditableListView;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    iget v0, p0, Lflipboard/gui/EditableListView;->e:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 167
    iget-object v0, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    iget v1, p0, Lflipboard/gui/EditableListView;->d:I

    iget v2, p0, Lflipboard/gui/EditableListView;->e:I

    invoke-interface {v0, v1, v2}, Lflipboard/gui/EditableListView$DragListener;->a(II)V

    .line 172
    :cond_4
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 119
    :pswitch_1
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->b()V

    .line 120
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/EditableListView;->pointToPosition(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/EditableListView;->d:I

    .line 121
    iget v0, p0, Lflipboard/gui/EditableListView;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 122
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    iget v1, p0, Lflipboard/gui/EditableListView;->d:I

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 123
    instance-of v1, v0, Lflipboard/objs/ContentDrawerListItem;

    if-eqz v1, :cond_5

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->x()Z

    move-result v0

    if-nez v0, :cond_5

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/EditableListView;->c:Z

    goto :goto_2

    .line 128
    :cond_5
    iget v0, p0, Lflipboard/gui/EditableListView;->d:I

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int v0, v2, v0

    iput v0, p0, Lflipboard/gui/EditableListView;->f:I

    .line 129
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v0, v0

    sub-int/2addr v0, v2

    iput v0, p0, Lflipboard/gui/EditableListView;->g:I

    .line 130
    iget v0, p0, Lflipboard/gui/EditableListView;->d:I

    invoke-direct {p0}, Lflipboard/gui/EditableListView;->a()V

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, v0, v1

    invoke-virtual {p0, v1}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    iget-object v1, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    iget-object v1, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Lflipboard/gui/EditableListView$DragListener;->a(Ljava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lflipboard/gui/EditableListView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iget v3, p0, Lflipboard/gui/EditableListView;->k:I

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    const/high16 v3, 0x3f000000    # 0.5f

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->alpha:F

    const/4 v3, 0x0

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iget v3, p0, Lflipboard/gui/EditableListView;->f:I

    sub-int v3, v2, v3

    iget v4, p0, Lflipboard/gui/EditableListView;->g:I

    add-int/2addr v3, v4

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    const/4 v3, -0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    const/4 v3, -0x2

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    const/16 v3, 0x398

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    const/4 v3, -0x3

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->format:I

    const/4 v3, 0x0

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Landroid/widget/ImageView;

    invoke-direct {v4, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const-string v0, "window"

    invoke-virtual {v3, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0, v4, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v4, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    .line 131
    :cond_7
    invoke-direct {p0, v2}, Lflipboard/gui/EditableListView;->a(I)V

    goto/16 :goto_2

    .line 136
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 138
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getMinTopY()I

    move-result v0

    .line 139
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getMaxTopY()I

    move-result v1

    .line 141
    iget v3, p0, Lflipboard/gui/EditableListView;->f:I

    add-int/2addr v3, v0

    if-le v3, v2, :cond_8

    .line 143
    iget v1, p0, Lflipboard/gui/EditableListView;->f:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lflipboard/gui/EditableListView;->a(I)V

    .line 144
    iget-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lflipboard/gui/EditableListView;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lflipboard/gui/EditableListView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/EditableListView$1;-><init>(Lflipboard/gui/EditableListView;)V

    iput-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto/16 :goto_2

    .line 146
    :cond_8
    iget v0, p0, Lflipboard/gui/EditableListView;->f:I

    add-int/2addr v0, v1

    if-ge v0, v2, :cond_9

    .line 148
    iget v0, p0, Lflipboard/gui/EditableListView;->f:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lflipboard/gui/EditableListView;->a(I)V

    .line 149
    iget-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    if-nez v0, :cond_4

    invoke-direct {p0}, Lflipboard/gui/EditableListView;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lflipboard/gui/EditableListView$2;

    invoke-direct {v0, p0}, Lflipboard/gui/EditableListView$2;-><init>(Lflipboard/gui/EditableListView;)V

    iput-object v0, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->E:Ljava/util/Timer;

    iget-object v1, p0, Lflipboard/gui/EditableListView;->j:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto/16 :goto_2

    .line 152
    :cond_9
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->b()V

    .line 153
    invoke-direct {p0, v2}, Lflipboard/gui/EditableListView;->a(I)V

    goto/16 :goto_2

    .line 165
    :cond_a
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->getVisibleBottom()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getFirstVisiblePosition()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0, v3}, Lflipboard/gui/EditableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-lt v2, v1, :cond_3

    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getLastVisiblePosition()I

    move-result v0

    goto/16 :goto_1

    .line 117
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setDragListener(Lflipboard/gui/EditableListView$DragListener;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lflipboard/gui/EditableListView;->b:Lflipboard/gui/EditableListView$DragListener;

    .line 96
    return-void
.end method

.method public setEditing(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-boolean v0, p0, Lflipboard/gui/EditableListView;->a:Z

    if-eq p1, v0, :cond_0

    .line 69
    iput-boolean p1, p0, Lflipboard/gui/EditableListView;->a:Z

    .line 70
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lflipboard/gui/EditableAdapter;

    .line 71
    invoke-interface {v0, p1}, Lflipboard/gui/EditableAdapter;->a(Z)V

    .line 73
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->a()V

    .line 74
    invoke-direct {p0}, Lflipboard/gui/EditableListView;->b()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/EditableListView;->h:Landroid/widget/ImageView;

    .line 76
    iput-boolean v1, p0, Lflipboard/gui/EditableListView;->c:Z

    .line 77
    iput v1, p0, Lflipboard/gui/EditableListView;->d:I

    .line 78
    iput v1, p0, Lflipboard/gui/EditableListView;->e:I

    .line 79
    iput v1, p0, Lflipboard/gui/EditableListView;->f:I

    .line 80
    iput v1, p0, Lflipboard/gui/EditableListView;->g:I

    .line 81
    invoke-virtual {p0}, Lflipboard/gui/EditableListView;->invalidateViews()V

    .line 83
    :cond_0
    return-void
.end method
