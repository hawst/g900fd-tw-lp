.class public Lflipboard/gui/FLEditText;
.super Landroid/widget/EditText;
.source "FLEditText.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lflipboard/gui/FLTextIntf;


# instance fields
.field final a:I

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Z

.field private e:Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 35
    invoke-direct {p0, p1, v2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/high16 v0, 0x41300000    # 11.0f

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLEditText;->a:I

    .line 36
    invoke-direct {p0, p1, v2}, Lflipboard/gui/FLEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    const/high16 v0, 0x41300000    # 11.0f

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLEditText;->a:I

    .line 42
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    const/high16 v0, 0x41300000    # 11.0f

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLEditText;->a:I

    .line 48
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 59
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/graphics/Paint;)V

    .line 60
    if-eqz p2, :cond_2

    .line 61
    sget-object v0, Lflipboard/app/R$styleable;->FLTextView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "normal"

    if-eqz p2, :cond_1

    const-string v2, "http://schemas.android.com/apk/res/android"

    const-string v3, "textStyle"

    invoke-interface {p2, v2, v3}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "0x1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "0x3"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const-string v0, "bold"

    :cond_1
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLEditText;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    sget-object v0, Lflipboard/app/R$styleable;->FLEditText:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lflipboard/gui/FLEditText;->d:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 65
    :cond_2
    new-instance v0, Lflipboard/gui/FLEditText$1;

    invoke-direct {v0, p0}, Lflipboard/gui/FLEditText$1;-><init>(Lflipboard/gui/FLEditText;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/FLEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 81
    invoke-virtual {p0, p0}, Lflipboard/gui/FLEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 84
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->a()V

    .line 85
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 120
    iget-object v0, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020148

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    .line 122
    iget-object v0, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 123
    iget-object v1, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3, v3, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 124
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020149

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/FLEditText;->c:Landroid/graphics/drawable/Drawable;

    .line 125
    iget-object v1, p0, Lflipboard/gui/FLEditText;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3, v3, v0, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 127
    :cond_0
    iget v0, p0, Lflipboard/gui/FLEditText;->a:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLEditText;->setCompoundDrawablePadding(I)V

    .line 129
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v1, v0, v3

    .line 130
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v2, 0x1

    aget-object v2, v0, v2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLEditText;->c:Landroid/graphics/drawable/Drawable;

    .line 132
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    .line 128
    invoke-virtual {p0, v1, v2, v0, v3}, Lflipboard/gui/FLEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 133
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 146
    iget-boolean v0, p0, Lflipboard/gui/FLEditText;->d:Z

    if-eqz v0, :cond_0

    .line 147
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 148
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v0, v0, v2

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lflipboard/gui/FLEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    invoke-direct {p0, v2}, Lflipboard/gui/FLEditText;->a(Z)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 115
    int-to-float v0, p2

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLEditText;->setTextSize(F)V

    .line 116
    return-void
.end method

.method public getTextColor()I
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCurrentTextColor()I

    move-result v0

    return v0
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 159
    iget-object v0, p0, Lflipboard/gui/FLEditText;->e:Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lflipboard/gui/FLEditText;->e:Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;

    invoke-interface {v0}, Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;->a()V

    .line 163
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 169
    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 172
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getHeight()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lflipboard/gui/FLEditText;->a:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLEditText;->getHeight()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/FLEditText;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget v2, p0, Lflipboard/gui/FLEditText;->a:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 176
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    .line 178
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/gui/FLEditText;->a(Z)V

    .line 187
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 183
    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lflipboard/gui/FLEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setKeyBoardCloseListener(Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lflipboard/gui/FLEditText;->e:Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;

    .line 54
    return-void
.end method
