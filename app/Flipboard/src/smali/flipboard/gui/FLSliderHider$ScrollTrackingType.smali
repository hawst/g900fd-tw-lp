.class public final enum Lflipboard/gui/FLSliderHider$ScrollTrackingType;
.super Ljava/lang/Enum;
.source "FLSliderHider.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLSliderHider$ScrollTrackingType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

.field public static final enum b:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

.field private static final synthetic c:[Lflipboard/gui/FLSliderHider$ScrollTrackingType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    new-instance v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    const-string v1, "DIFFERENTIAL"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLSliderHider$ScrollTrackingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->a:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    .line 60
    new-instance v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    const-string v1, "LINEAR"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLSliderHider$ScrollTrackingType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->b:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    .line 56
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    sget-object v1, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->a:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->b:Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->c:[Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLSliderHider$ScrollTrackingType;
    .locals 1

    .prologue
    .line 56
    const-class v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLSliderHider$ScrollTrackingType;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lflipboard/gui/FLSliderHider$ScrollTrackingType;->c:[Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    invoke-virtual {v0}, [Lflipboard/gui/FLSliderHider$ScrollTrackingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLSliderHider$ScrollTrackingType;

    return-object v0
.end method
