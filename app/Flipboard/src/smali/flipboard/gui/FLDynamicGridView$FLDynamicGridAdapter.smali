.class public Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;
.super Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;
.source "FLDynamicGridView.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic a:Lflipboard/gui/FLDynamicGridView;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<*>;"
        }
    .end annotation
.end field

.field private c:Landroid/view/View;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflipboard/gui/FLDynamicGridView;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 99
    iput-object p1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    .line 100
    invoke-direct {p0, p2}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;-><init>(Landroid/content/Context;)V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->f:Ljava/util/List;

    .line 101
    return-void
.end method

.method public static synthetic a(Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;)Landroid/view/View;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic a(Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    iput-object p1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    invoke-virtual {p1, p0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->b:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a(Ljava/util/List;)V

    :cond_1
    return-void
.end method

.method static synthetic b(Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;)Ljava/util/List;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->f:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 135
    iput-object p1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->b:Ljava/util/List;

    .line 136
    invoke-super {p0, p1}, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->a(Ljava/util/List;)V

    .line 137
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 139
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 140
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, v1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c(Ljava/lang/Object;)V

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 161
    if-nez p1, :cond_0

    .line 162
    const v0, -0x188233

    .line 168
    :goto_0
    return v0

    .line 163
    :cond_0
    const/4 v0, 0x3

    if-ge p1, v0, :cond_1

    .line 164
    const v0, -0x188232

    goto :goto_0

    .line 167
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-static {v0}, Lflipboard/gui/FLDynamicGridView;->b(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-static {v0}, Lflipboard/gui/FLDynamicGridView;->b(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    move-result-object v0

    invoke-virtual {p0, p1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLDynamicGridView$ViewAdapter;->a(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 185
    invoke-virtual {p0, p1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 198
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-static {v0}, Lflipboard/gui/FLDynamicGridView;->b(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-static {v1}, Lflipboard/gui/FLDynamicGridView;->a(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;

    move-result-object v1

    invoke-virtual {v1, p1}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lflipboard/gui/FLDynamicGridView$ViewAdapter;->a(Ljava/lang/Object;Landroid/view/View;)Landroid/view/View;

    move-result-object p2

    .line 201
    :goto_0
    return-object p2

    .line 189
    :pswitch_0
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v0}, Lflipboard/gui/FLDynamicGridView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v1}, Lflipboard/gui/FLDynamicGridView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-virtual {v1}, Lflipboard/gui/FLDynamicGridView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 190
    iget-object v1, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v0, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    iget-object p2, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    goto :goto_0

    .line 194
    :pswitch_1
    if-nez p2, :cond_0

    new-instance p2, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter$FillerView;

    iget-object v0, p0, Lorg/askerov/dynamicgrid/BaseDynamicGridAdapter;->e:Landroid/content/Context;

    invoke-direct {p2, p0, v0}, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter$FillerView;-><init>(Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;Landroid/content/Context;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch -0x188233
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-static {v0}, Lflipboard/gui/FLDynamicGridView;->b(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 174
    :goto_0
    add-int/lit8 v0, v0, 0x2

    return v0

    .line 173
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->a:Lflipboard/gui/FLDynamicGridView;

    invoke-static {v0}, Lflipboard/gui/FLDynamicGridView;->b(Lflipboard/gui/FLDynamicGridView;)Lflipboard/gui/FLDynamicGridView$ViewAdapter;

    const/4 v0, 0x2

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 128
    iget-object v2, p0, Lflipboard/gui/FLDynamicGridView$FLDynamicGridAdapter;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_0

    .line 131
    :cond_0
    return-void
.end method
