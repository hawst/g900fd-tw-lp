.class Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;
.super Ljava/lang/Object;
.source "SlidingTabLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/tabs/SlidingTabLayout;


# direct methods
.method private constructor <init>(Lflipboard/gui/tabs/SlidingTabLayout;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/tabs/SlidingTabLayout;B)V
    .locals 0

    .prologue
    .line 339
    invoke-direct {p0, p1}, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;-><init>(Lflipboard/gui/tabs/SlidingTabLayout;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 342
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;

    move-result-object v1

    invoke-virtual {v1}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 343
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_2

    .line 344
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1}, Lflipboard/gui/tabs/SlidingTabLayout;->c(Lflipboard/gui/tabs/SlidingTabLayout;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 345
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1}, Lflipboard/gui/tabs/SlidingTabLayout;->c(Lflipboard/gui/tabs/SlidingTabLayout;)Landroid/support/v4/view/ViewPager;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 354
    :cond_0
    :goto_1
    return-void

    .line 347
    :cond_1
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 348
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;->c(I)V

    goto :goto_1

    .line 342
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
