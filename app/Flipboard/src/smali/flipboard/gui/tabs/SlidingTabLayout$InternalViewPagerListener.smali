.class Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;
.super Ljava/lang/Object;
.source "SlidingTabLayout.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field final synthetic a:Lflipboard/gui/tabs/SlidingTabLayout;

.field private b:I


# direct methods
.method private constructor <init>(Lflipboard/gui/tabs/SlidingTabLayout;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lflipboard/gui/tabs/SlidingTabLayout;B)V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0, p1}, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;-><init>(Lflipboard/gui/tabs/SlidingTabLayout;)V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 327
    iget v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->b:I

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lflipboard/gui/tabs/SlidingTabStrip;->a(IF)V

    .line 329
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;II)V

    .line 332
    :cond_0
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 333
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;->a(I)V

    .line 335
    :cond_1
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildCount()I

    move-result v0

    .line 298
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lflipboard/gui/tabs/SlidingTabStrip;->a(IF)V

    .line 304
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;

    move-result-object v0

    invoke-virtual {v0, p1}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 305
    if-eqz v0, :cond_2

    .line 306
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    .line 308
    :goto_1
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v1, p1, v0}, Lflipboard/gui/tabs/SlidingTabLayout;->a(Lflipboard/gui/tabs/SlidingTabLayout;II)V

    .line 310
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 311
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;->a(IFI)V

    goto :goto_0

    .line 306
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 318
    iput p1, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->b:I

    .line 320
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;->a:Lflipboard/gui/tabs/SlidingTabLayout;

    invoke-static {v0}, Lflipboard/gui/tabs/SlidingTabLayout;->b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;->b(I)V

    .line 323
    :cond_0
    return-void
.end method
