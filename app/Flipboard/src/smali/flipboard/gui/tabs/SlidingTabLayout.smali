.class public Lflipboard/gui/tabs/SlidingTabLayout;
.super Landroid/widget/HorizontalScrollView;
.source "SlidingTabLayout.java"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/support/v4/view/ViewPager;

.field private g:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

.field private h:Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

.field private final i:Lflipboard/gui/tabs/SlidingTabStrip;

.field private j:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/tabs/SlidingTabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/tabs/SlidingTabLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 109
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    iput v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->e:I

    .line 112
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/tabs/SlidingTabLayout;->setHorizontalScrollBarEnabled(Z)V

    .line 114
    invoke-virtual {p0, v1}, Lflipboard/gui/tabs/SlidingTabLayout;->setFillViewport(Z)V

    .line 116
    const/high16 v0, 0x41c00000    # 24.0f

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->a:I

    .line 118
    new-instance v0, Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-direct {v0, p1}, Lflipboard/gui/tabs/SlidingTabStrip;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    .line 119
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lflipboard/gui/tabs/SlidingTabStrip;->setGravity(I)V

    .line 120
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {p0, v0, v2, v2}, Lflipboard/gui/tabs/SlidingTabLayout;->addView(Landroid/view/View;II)V

    .line 121
    return-void
.end method

.method static synthetic a(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabStrip;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildCount()I

    move-result v0

    .line 275
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v0, p1}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 280
    if-eqz v0, :cond_0

    .line 281
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int/2addr v0, p2

    .line 283
    if-gtz p1, :cond_2

    if-lez p2, :cond_3

    .line 285
    :cond_2
    iget v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->a:I

    sub-int/2addr v0, v1

    .line 288
    :cond_3
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/tabs/SlidingTabLayout;->scrollTo(II)V

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/tabs/SlidingTabLayout;II)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lflipboard/gui/tabs/SlidingTabLayout;->a(II)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/tabs/SlidingTabLayout;)Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->h:Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/tabs/SlidingTabLayout;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->f:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 393
    iget v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->e:I

    if-nez v0, :cond_0

    .line 394
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    iget-object v0, v0, Lflipboard/gui/tabs/SlidingTabStrip;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 398
    :goto_0
    return-object v0

    .line 396
    :cond_0
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    iget-object v0, v0, Lflipboard/gui/tabs/SlidingTabStrip;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    goto :goto_0
.end method

.method public final a(ILandroid/support/v4/view/ViewPager;Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;)V
    .locals 9

    .prologue
    .line 170
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabStrip;->removeAllViews()V

    .line 172
    iput p1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->e:I

    .line 173
    iput-object p2, p0, Lflipboard/gui/tabs/SlidingTabLayout;->f:Landroid/support/v4/view/ViewPager;

    .line 174
    iput-object p3, p0, Lflipboard/gui/tabs/SlidingTabLayout;->g:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    .line 175
    if-eqz p2, :cond_7

    .line 176
    new-instance v0, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lflipboard/gui/tabs/SlidingTabLayout$InternalViewPagerListener;-><init>(Lflipboard/gui/tabs/SlidingTabLayout;B)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 177
    new-instance v4, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;

    const/4 v0, 0x0

    invoke-direct {v4, p0, v0}, Lflipboard/gui/tabs/SlidingTabLayout$TabClickListener;-><init>(Lflipboard/gui/tabs/SlidingTabLayout;B)V

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->g:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    invoke-virtual {v0}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->c()I

    move-result v0

    if-ge v2, v0, :cond_7

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    iget v5, p0, Lflipboard/gui/tabs/SlidingTabLayout;->b:I

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->b:I

    iget-object v3, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    const/4 v5, 0x0

    invoke-virtual {v0, v1, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    iget v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->c:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->d:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLMultiColorTextView;

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :cond_0
    if-nez v3, :cond_1

    iget v5, p0, Lflipboard/gui/tabs/SlidingTabLayout;->e:I

    if-nez v5, :cond_4

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    invoke-direct {v3, v5, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    move-object v3, v1

    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    iget-object v5, p0, Lflipboard/gui/tabs/SlidingTabLayout;->g:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    invoke-virtual {v5, v2}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->c(I)I

    move-result v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-static {}, Lflipboard/util/ColorFilterUtil;->a()Landroid/graphics/ColorFilter;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    iget-object v5, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    iget-object v5, v5, Lflipboard/gui/tabs/SlidingTabStrip;->c:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->g:Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;

    invoke-virtual {v1, v2}, Lflipboard/gui/tabs/FragmentPagerAdapterWithIcon;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLMultiColorTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    iget-object v1, v1, Lflipboard/gui/tabs/SlidingTabStrip;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v0, v3}, Lflipboard/gui/tabs/SlidingTabStrip;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_4
    iget v5, p0, Lflipboard/gui/tabs/SlidingTabLayout;->e:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_1

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v0, Lflipboard/gui/FLMultiColorTextView;

    invoke-direct {v0, v3}, Lflipboard/gui/FLMultiColorTextView;-><init>(Landroid/content/Context;)V

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Lflipboard/gui/FLMultiColorTextView;->setGravity(I)V

    const/4 v3, 0x2

    const/16 v5, 0xc

    invoke-virtual {v0, v3, v5}, Lflipboard/gui/FLMultiColorTextView;->a(II)V

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLMultiColorTextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f080083

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLMultiColorTextView;->setTextColor(I)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xb

    if-lt v3, v5, :cond_5

    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    const v6, 0x101030e

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v3, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v3, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v3}, Lflipboard/gui/FLMultiColorTextView;->setBackgroundResource(I)V

    :cond_5
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-lt v3, v5, :cond_6

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lflipboard/gui/FLMultiColorTextView;->setAllCaps(Z)V

    :cond_6
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x2

    const/4 v6, -0x1

    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Lflipboard/gui/FLMultiColorTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v3, v0

    goto/16 :goto_1

    .line 179
    :cond_7
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 377
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 379
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/HorizontalScrollView;->draw(Landroid/graphics/Canvas;)V

    .line 380
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 266
    invoke-super {p0}, Landroid/widget/HorizontalScrollView;->onAttachedToWindow()V

    .line 268
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->f:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->f:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lflipboard/gui/tabs/SlidingTabLayout;->a(II)V

    .line 271
    :cond_0
    return-void
.end method

.method public setClipTop(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 358
    if-ltz p1, :cond_2

    .line 359
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    .line 360
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v1}, Lflipboard/gui/tabs/SlidingTabStrip;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getMeasuredHeight()I

    move-result v2

    invoke-direct {v0, v3, p1, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    .line 361
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->invalidate()V

    .line 372
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    if-eq v0, p1, :cond_0

    .line 363
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v1}, Lflipboard/gui/tabs/SlidingTabStrip;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, p1, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 364
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    invoke-virtual {v1}, Lflipboard/gui/tabs/SlidingTabStrip;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0, v3, v0, v1, v2}, Lflipboard/gui/tabs/SlidingTabLayout;->invalidate(IIII)V

    goto :goto_0

    .line 367
    :cond_2
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->j:Landroid/graphics/Rect;

    .line 369
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabLayout;->invalidate()V

    goto :goto_0
.end method

.method public setCustomTabColorizer(Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    iput-object p1, v0, Lflipboard/gui/tabs/SlidingTabStrip;->a:Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabStrip;->invalidate()V

    .line 131
    return-void
.end method

.method public setOnPageChangeListener(Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lflipboard/gui/tabs/SlidingTabLayout;->h:Lflipboard/gui/tabs/SlidingTabLayout$OnPageChangeListener;

    .line 150
    return-void
.end method

.method public varargs setSelectedIndicatorColors([I)V
    .locals 2

    .prologue
    .line 138
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabLayout;->i:Lflipboard/gui/tabs/SlidingTabStrip;

    const/4 v1, 0x0

    iput-object v1, v0, Lflipboard/gui/tabs/SlidingTabStrip;->a:Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;

    iget-object v1, v0, Lflipboard/gui/tabs/SlidingTabStrip;->b:Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;

    iput-object p1, v1, Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;->a:[I

    invoke-virtual {v0}, Lflipboard/gui/tabs/SlidingTabStrip;->invalidate()V

    .line 139
    return-void
.end method
