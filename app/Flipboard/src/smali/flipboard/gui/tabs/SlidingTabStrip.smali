.class Lflipboard/gui/tabs/SlidingTabStrip;
.super Landroid/widget/LinearLayout;
.source "SlidingTabStrip.java"


# instance fields
.field a:Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;

.field final b:Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/FLMultiColorTextView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:Landroid/graphics/Paint;

.field private final g:I

.field private final h:Landroid/graphics/Paint;

.field private final i:I

.field private j:I

.field private k:I

.field private l:F

.field private final m:Landroid/graphics/ColorFilter;

.field private final n:Landroid/graphics/ColorFilter;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/tabs/SlidingTabStrip;-><init>(Landroid/content/Context;B)V

    .line 66
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->k:I

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->c:Ljava/util/List;

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->d:Ljava/util/ArrayList;

    .line 70
    invoke-virtual {p0, v6}, Lflipboard/gui/tabs/SlidingTabStrip;->setWillNotDraw(Z)V

    .line 72
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    .line 75
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x1010030

    invoke-virtual {v3, v4, v2, v7}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 77
    iget v2, v2, Landroid/util/TypedValue;->data:I

    .line 79
    const/16 v3, 0xb

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v4

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v5

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    invoke-static {v3, v4, v5, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    iput v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->i:I

    .line 82
    invoke-static {}, Lflipboard/util/ColorFilterUtil;->a()Landroid/graphics/ColorFilter;

    move-result-object v2

    iput-object v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->m:Landroid/graphics/ColorFilter;

    .line 83
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 84
    invoke-static {v2}, Lflipboard/util/ColorFilterUtil;->b(I)Landroid/graphics/ColorFilter;

    move-result-object v3

    iput-object v3, p0, Lflipboard/gui/tabs/SlidingTabStrip;->n:Landroid/graphics/ColorFilter;

    .line 86
    new-instance v3, Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;

    invoke-direct {v3, v6}, Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;-><init>(B)V

    iput-object v3, p0, Lflipboard/gui/tabs/SlidingTabStrip;->b:Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;

    .line 87
    iget-object v3, p0, Lflipboard/gui/tabs/SlidingTabStrip;->b:Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;

    new-array v4, v7, [I

    aput v2, v4, v6

    iput-object v4, v3, Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;->a:[I

    .line 89
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->e:I

    .line 90
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->f:Landroid/graphics/Paint;

    .line 91
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->f:Landroid/graphics/Paint;

    iget v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->i:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 93
    const v1, 0x7f090123

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->g:I

    .line 94
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->h:Landroid/graphics/Paint;

    .line 95
    return-void
.end method


# virtual methods
.method final a(IF)V
    .locals 3

    .prologue
    .line 110
    iput p1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->j:I

    .line 111
    iput p2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    .line 114
    int-to-float v0, p1

    add-float/2addr v0, p2

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v1, v0

    .line 115
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->k:I

    if-eq v1, v0, :cond_1

    .line 116
    iget v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->k:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 117
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->c:Ljava/util/List;

    iget v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->k:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 118
    iget-object v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->m:Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 120
    :cond_0
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 121
    iget-object v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->n:Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 122
    iput v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->k:I

    .line 125
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->invalidate()V

    .line 126
    return-void
.end method

.method public getLayoutDirection()I
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v1, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 156
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getHeight()I

    move-result v8

    .line 157
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildCount()I

    move-result v7

    .line 158
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->a:Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->a:Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;

    move-object v6, v0

    .line 163
    :goto_0
    iget v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->e:I

    sub-int v0, v8, v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v8

    iget-object v5, p0, Lflipboard/gui/tabs/SlidingTabStrip;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 166
    if-lez v7, :cond_4

    .line 167
    iget v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->j:I

    invoke-virtual {p0, v0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 169
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    .line 170
    iget v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->j:I

    invoke-interface {v6, v0}, Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;->a(I)I

    move-result v0

    .line 172
    iget v4, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    cmpl-float v1, v4, v1

    if-lez v1, :cond_5

    iget v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->j:I

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v1, v4, :cond_5

    .line 173
    iget v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v6, v1}, Lflipboard/gui/tabs/SlidingTabLayout$TabColorizer;->a(I)I

    move-result v1

    .line 174
    if-eq v0, v1, :cond_0

    .line 175
    iget v4, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    sub-float v5, v10, v4

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v4

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v5

    add-float/2addr v6, v7

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v7, v4

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v9, v5

    add-float/2addr v7, v9

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v5

    add-float/2addr v0, v1

    float-to-int v1, v6

    float-to-int v4, v7

    float-to-int v0, v0

    invoke-static {v1, v4, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    .line 179
    :cond_0
    iget v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 180
    iget v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v1, v5

    iget v5, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    sub-float v5, v10, v5

    int-to-float v2, v2

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 182
    iget v2, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v2, v4

    iget v4, p0, Lflipboard/gui/tabs/SlidingTabStrip;->l:F

    sub-float v4, v10, v4

    int-to-float v3, v3

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    move v6, v1

    move v7, v2

    .line 186
    :goto_1
    iget-object v1, p0, Lflipboard/gui/tabs/SlidingTabStrip;->h:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 188
    int-to-float v1, v6

    iget v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->g:I

    sub-int v0, v8, v0

    int-to-float v2, v0

    int-to-float v3, v7

    int-to-float v4, v8

    iget-object v5, p0, Lflipboard/gui/tabs/SlidingTabStrip;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 192
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLMultiColorTextView;

    .line 193
    invoke-virtual {v0}, Lflipboard/gui/FLMultiColorTextView;->getLeft()I

    move-result v2

    sub-int v2, v6, v2

    .line 194
    invoke-virtual {v0}, Lflipboard/gui/FLMultiColorTextView;->getLeft()I

    move-result v3

    sub-int v3, v7, v3

    .line 195
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08005a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v8, 0x7f08000b

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v4, v0, Lflipboard/gui/FLMultiColorTextView;->b:I

    iput v5, v0, Lflipboard/gui/FLMultiColorTextView;->a:I

    invoke-virtual {v0}, Lflipboard/gui/FLMultiColorTextView;->getWidth()I

    move-result v4

    invoke-static {v2, v11, v4}, Lflipboard/util/JavaUtil;->a(III)I

    move-result v2

    invoke-virtual {v0}, Lflipboard/gui/FLMultiColorTextView;->getWidth()I

    move-result v4

    invoke-static {v3, v11, v4}, Lflipboard/util/JavaUtil;->a(III)I

    move-result v3

    iget v4, v0, Lflipboard/gui/FLMultiColorTextView;->c:I

    if-ne v2, v4, :cond_2

    iget v4, v0, Lflipboard/gui/FLMultiColorTextView;->d:I

    if-eq v3, v4, :cond_1

    :cond_2
    iput v2, v0, Lflipboard/gui/FLMultiColorTextView;->c:I

    iput v3, v0, Lflipboard/gui/FLMultiColorTextView;->d:I

    invoke-virtual {v0}, Lflipboard/gui/FLMultiColorTextView;->invalidate()V

    goto :goto_2

    .line 158
    :cond_3
    iget-object v0, p0, Lflipboard/gui/tabs/SlidingTabStrip;->b:Lflipboard/gui/tabs/SlidingTabStrip$SimpleTabColorizer;

    move-object v6, v0

    goto/16 :goto_0

    .line 198
    :cond_4
    return-void

    :cond_5
    move v6, v2

    move v7, v3

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v0, 0x0

    .line 136
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 137
    invoke-virtual {p0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildCount()I

    move-result v3

    move v1, v0

    move v2, v0

    .line 139
    :goto_0
    if-ge v1, v3, :cond_0

    .line 140
    invoke-virtual {p0, v1}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v2, v4

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 142
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 144
    if-ge v2, v1, :cond_1

    .line 146
    div-int/2addr v1, v3

    .line 147
    :goto_1
    if-ge v0, v3, :cond_1

    .line 148
    invoke-virtual {p0, v0}, Lflipboard/gui/tabs/SlidingTabStrip;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 149
    invoke-static {v1, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/view/View;->measure(II)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 152
    :cond_1
    return-void
.end method
