.class Lflipboard/gui/ContentDrawerView$1;
.super Ljava/lang/Object;
.source "ContentDrawerView.java"

# interfaces
.implements Lflipboard/gui/EditableListView$DragListener;


# instance fields
.field final synthetic a:Lflipboard/gui/ContentDrawerView;


# direct methods
.method constructor <init>(Lflipboard/gui/ContentDrawerView;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 127
    if-eq p1, p2, :cond_0

    .line 129
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->getCount()I

    move-result v0

    if-lt p2, v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, p2}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v1, v1, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v1, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ContentDrawerListItem;)I

    .line 138
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v1, v1, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v1, p2, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(ILflipboard/objs/ContentDrawerListItem;)V

    .line 141
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 142
    const/4 v2, 0x4

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    if-ne v2, v0, :cond_5

    .line 143
    iget-object v0, v1, Lflipboard/service/User;->n:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-virtual {v1}, Lflipboard/service/User;->r()V

    goto :goto_0

    :cond_3
    iget-object v0, v1, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    iget-object v2, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    const/4 v0, 0x0

    if-le p2, v3, :cond_4

    iget-object v0, v1, Lflipboard/service/User;->n:Ljava/util/List;

    add-int/lit8 v3, p2, -0x1

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    iget-object v0, v0, Lflipboard/objs/Magazine;->c:Ljava/lang/String;

    :cond_4
    invoke-static {v2, v0}, Lflipboard/util/ShareHelper;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Magazine;

    iget-object v2, v1, Lflipboard/service/User;->n:Ljava/util/List;

    invoke-interface {v2, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, v1, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lflipboard/service/User;->c:Lflipboard/service/User$MagazineChangeListener;

    invoke-interface {v0}, Lflipboard/service/User$MagazineChangeListener;->a()V

    goto :goto_0

    .line 145
    :cond_5
    invoke-virtual {v1, p1, p2, v3}, Lflipboard/service/User;->a(IIZ)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 115
    check-cast p1, Lflipboard/objs/ContentDrawerListItem;

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lflipboard/objs/ContentDrawerListItem;->c(Z)V

    .line 116
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0}, Lflipboard/gui/EditableListView;->invalidateViews()V

    .line 117
    return-void
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 121
    check-cast p1, Lflipboard/objs/ContentDrawerListItem;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lflipboard/objs/ContentDrawerListItem;->c(Z)V

    .line 122
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$1;->a:Lflipboard/gui/ContentDrawerView;

    iget-object v0, v0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0}, Lflipboard/gui/EditableListView;->invalidateViews()V

    .line 123
    return-void
.end method
