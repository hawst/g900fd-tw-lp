.class public Lflipboard/gui/RoadBlock;
.super Landroid/widget/FrameLayout;
.source "RoadBlock.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/util/MeterView;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lflipboard/util/MeteringHelper$ExitPath;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const-string v0, "nytimes"

    iput-object v0, p0, Lflipboard/gui/RoadBlock;->a:Ljava/lang/String;

    .line 28
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    iput-object v0, p0, Lflipboard/gui/RoadBlock;->b:Lflipboard/util/MeteringHelper$ExitPath;

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-string v0, "nytimes"

    iput-object v0, p0, Lflipboard/gui/RoadBlock;->a:Ljava/lang/String;

    .line 28
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    iput-object v0, p0, Lflipboard/gui/RoadBlock;->b:Lflipboard/util/MeteringHelper$ExitPath;

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const-string v0, "nytimes"

    iput-object v0, p0, Lflipboard/gui/RoadBlock;->a:Ljava/lang/String;

    .line 28
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    iput-object v0, p0, Lflipboard/gui/RoadBlock;->b:Lflipboard/util/MeteringHelper$ExitPath;

    .line 44
    return-void
.end method


# virtual methods
.method public final a(ZI)V
    .locals 1

    .prologue
    .line 109
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/RoadBlock;->c:Z

    .line 112
    :cond_0
    return-void
.end method

.method public getDebugString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    const-string v0, ""

    return-object v0
.end method

.method public getViewType()Lflipboard/util/MeteringHelper$MeteringViewUsageType;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->a:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 100
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 101
    iget-boolean v0, p0, Lflipboard/gui/RoadBlock;->c:Z

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lflipboard/gui/RoadBlock;->a:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/RoadBlock;->b:Lflipboard/util/MeteringHelper$ExitPath;

    invoke-static {p0, v0, v1}, Lflipboard/util/MeteringHelper;->a(Lflipboard/util/MeterView;Ljava/lang/String;Lflipboard/util/MeteringHelper$ExitPath;)V

    .line 104
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 49
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 50
    const v0, 0x7f0a028f

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    .line 51
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 53
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "nytimes"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 54
    const v0, 0x7f0a028e

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 55
    iget-object v2, v1, Lflipboard/objs/ConfigService;->bL:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 57
    const v0, 0x7f0a0292

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 58
    invoke-static {v1}, Lflipboard/util/MeteringHelper;->b(Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v2

    .line 59
    if-eqz v2, :cond_0

    .line 60
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, v1, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget v4, v1, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    :cond_0
    const v0, 0x7f0a0297

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    .line 63
    invoke-static {v1}, Lflipboard/util/MeteringHelper;->a(Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v2

    .line 64
    if-eqz v2, :cond_1

    .line 65
    new-array v3, v6, [Ljava/lang/Object;

    iget v1, v1, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    :cond_1
    const v0, 0x7f0a0295

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 69
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPaintFlags(I)V

    .line 70
    invoke-virtual {v0, p0}, Lflipboard/gui/FLTextView;->setTag(Ljava/lang/Object;)V

    .line 72
    const v0, 0x7f0a0296

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 73
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPaintFlags(I)V

    .line 74
    invoke-virtual {v0, p0}, Lflipboard/gui/FLTextView;->setTag(Ljava/lang/Object;)V

    .line 75
    sget-boolean v1, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v1, :cond_2

    .line 76
    const v1, 0x7f0d023c

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 77
    const/high16 v1, 0x41d00000    # 26.0f

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setTextSize(F)V

    .line 78
    const v0, 0x7f0a0298

    invoke-virtual {p0, v0}, Lflipboard/gui/RoadBlock;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 79
    const v1, 0x7f0d023a

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 80
    const/high16 v1, 0x41c00000    # 24.0f

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setTextSize(F)V

    .line 82
    :cond_2
    return-void
.end method

.method public setExitPath(Lflipboard/util/MeteringHelper$ExitPath;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lflipboard/gui/RoadBlock;->b:Lflipboard/util/MeteringHelper$ExitPath;

    .line 92
    return-void
.end method
