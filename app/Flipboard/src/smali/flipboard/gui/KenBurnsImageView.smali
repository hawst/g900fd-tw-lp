.class public Lflipboard/gui/KenBurnsImageView;
.super Landroid/widget/FrameLayout;
.source "KenBurnsImageView.java"


# instance fields
.field a:I

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Image;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/lang/Runnable;

.field private d:Lflipboard/gui/KenBurnsImageView$AnimationType;

.field private e:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/KenBurnsImageView;->b:Ljava/util/List;

    .line 87
    new-instance v0, Lflipboard/gui/KenBurnsImageView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/KenBurnsImageView$1;-><init>(Lflipboard/gui/KenBurnsImageView;)V

    iput-object v0, p0, Lflipboard/gui/KenBurnsImageView;->c:Ljava/lang/Runnable;

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/KenBurnsImageView;->b:Ljava/util/List;

    .line 87
    new-instance v0, Lflipboard/gui/KenBurnsImageView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/KenBurnsImageView$1;-><init>(Lflipboard/gui/KenBurnsImageView;)V

    iput-object v0, p0, Lflipboard/gui/KenBurnsImageView;->c:Ljava/lang/Runnable;

    .line 48
    return-void
.end method

.method static synthetic a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$AnimationType;)Lflipboard/gui/KenBurnsImageView$AnimationType;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lflipboard/gui/KenBurnsImageView;->d:Lflipboard/gui/KenBurnsImageView$AnimationType;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/KenBurnsImageView;Lflipboard/gui/KenBurnsImageView$TranslationDirection;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lflipboard/gui/KenBurnsImageView;->e:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    return-object p1
.end method

.method static synthetic a(Lflipboard/gui/KenBurnsImageView;)Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lflipboard/gui/KenBurnsImageView;->f:Z

    return v0
.end method

.method static synthetic b(Lflipboard/gui/KenBurnsImageView;)Lflipboard/gui/KenBurnsImageView$AnimationType;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView;->d:Lflipboard/gui/KenBurnsImageView$AnimationType;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/KenBurnsImageView;)Lflipboard/gui/KenBurnsImageView$TranslationDirection;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView;->e:Lflipboard/gui/KenBurnsImageView$TranslationDirection;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 75
    iget v0, p0, Lflipboard/gui/KenBurnsImageView;->a:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lflipboard/gui/KenBurnsImageView;->a:I

    .line 76
    iget-object v0, p0, Lflipboard/gui/KenBurnsImageView;->b:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/KenBurnsImageView;->a:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Image;

    .line 79
    new-instance v1, Lflipboard/gui/KenBurnsImageView$CoverImageView;

    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lflipboard/gui/KenBurnsImageView$CoverImageView;-><init>(Lflipboard/gui/KenBurnsImageView;Landroid/content/Context;)V

    .line 80
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->setRecycle(Z)V

    .line 81
    const/4 v2, 0x4

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 82
    invoke-virtual {v1, v0}, Lflipboard/gui/KenBurnsImageView$CoverImageView;->setImage(Lflipboard/objs/Image;)V

    .line 83
    invoke-virtual {p0, v1}, Lflipboard/gui/KenBurnsImageView;->addView(Landroid/view/View;)V

    .line 85
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 62
    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView;->a()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/KenBurnsImageView;->f:Z

    .line 64
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 69
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->P:Landroid/os/Handler;

    iget-object v1, p0, Lflipboard/gui/KenBurnsImageView;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/KenBurnsImageView;->f:Z

    .line 71
    return-void
.end method

.method public setImages(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    iput-object p1, p0, Lflipboard/gui/KenBurnsImageView;->b:Ljava/util/List;

    .line 52
    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView;->removeAllViews()V

    .line 53
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/KenBurnsImageView;->a:I

    .line 54
    iget-boolean v0, p0, Lflipboard/gui/KenBurnsImageView;->f:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lflipboard/gui/KenBurnsImageView;->a()V

    .line 57
    :cond_0
    return-void
.end method
