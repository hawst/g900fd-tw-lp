.class Lflipboard/gui/FLButton$1;
.super Ljava/lang/Object;
.source "FLButton.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/widget/Button;

.field final synthetic b:I

.field final synthetic c:Lflipboard/gui/FLButton;


# direct methods
.method constructor <init>(Lflipboard/gui/FLButton;Landroid/widget/Button;I)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lflipboard/gui/FLButton$1;->c:Lflipboard/gui/FLButton;

    iput-object p2, p0, Lflipboard/gui/FLButton$1;->a:Landroid/widget/Button;

    iput p3, p0, Lflipboard/gui/FLButton$1;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 75
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 76
    iget-object v1, p0, Lflipboard/gui/FLButton$1;->a:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->getHitRect(Landroid/graphics/Rect;)V

    .line 77
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lflipboard/gui/FLButton$1;->b:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 78
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, p0, Lflipboard/gui/FLButton$1;->b:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 79
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, p0, Lflipboard/gui/FLButton$1;->b:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 80
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, Lflipboard/gui/FLButton$1;->b:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 81
    new-instance v1, Landroid/view/TouchDelegate;

    iget-object v2, p0, Lflipboard/gui/FLButton$1;->a:Landroid/widget/Button;

    invoke-direct {v1, v0, v2}, Landroid/view/TouchDelegate;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    .line 82
    iget-object v0, p0, Lflipboard/gui/FLButton$1;->a:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 83
    return-void
.end method
