.class public Lflipboard/gui/MetricBar$MetricItemViewHolder;
.super Ljava/lang/Object;
.source "MetricBar.java"


# instance fields
.field protected a:Landroid/view/ViewGroup;

.field protected b:Landroid/widget/TextView;

.field protected c:Landroid/widget/TextView;

.field final synthetic d:Lflipboard/gui/MetricBar;


# direct methods
.method private constructor <init>(Lflipboard/gui/MetricBar;Ljava/lang/String;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->d:Lflipboard/gui/MetricBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput-object p3, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a:Landroid/view/ViewGroup;

    .line 157
    invoke-virtual {p3, p2}, Landroid/view/ViewGroup;->setTag(Ljava/lang/Object;)V

    .line 158
    invoke-static {p0, p3}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 159
    return-void
.end method

.method public synthetic constructor <init>(Lflipboard/gui/MetricBar;Ljava/lang/String;Landroid/view/ViewGroup;B)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/MetricBar$MetricItemViewHolder;-><init>(Lflipboard/gui/MetricBar;Ljava/lang/String;Landroid/view/ViewGroup;)V

    return-void
.end method

.method public static synthetic a(Lflipboard/gui/MetricBar$MetricItemViewHolder;II)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public static synthetic a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public static synthetic a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public static synthetic a(Lflipboard/gui/MetricBar$MetricItemViewHolder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->b:Landroid/widget/TextView;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, p1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->c:Landroid/widget/TextView;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1, p2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method public static synthetic b(Lflipboard/gui/MetricBar$MetricItemViewHolder;II)V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->b:Landroid/widget/TextView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v0, p0, Lflipboard/gui/MetricBar$MetricItemViewHolder;->c:Landroid/widget/TextView;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    return-void
.end method
