.class public Lflipboard/gui/FLBusyView;
.super Landroid/widget/ProgressBar;
.source "FLBusyView.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/FLBusyView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 95
    new-instance v0, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;

    invoke-direct {v0, p0}, Lflipboard/gui/FLBusyView$FLStroopwafelDrawable;-><init>(Lflipboard/gui/FLBusyView;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/FLBusyView;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 96
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lflipboard/gui/FLBusyView;->setMax(I)V

    .line 97
    return-void
.end method


# virtual methods
.method public final a(ZI)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public setProgressPercent(F)V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0}, Lflipboard/gui/FLBusyView;->getMax()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 110
    invoke-virtual {p0}, Lflipboard/gui/FLBusyView;->getProgress()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 111
    invoke-virtual {p0, v0}, Lflipboard/gui/FLBusyView;->setProgress(I)V

    .line 112
    invoke-virtual {p0}, Lflipboard/gui/FLBusyView;->invalidate()V

    .line 114
    :cond_0
    return-void
.end method
