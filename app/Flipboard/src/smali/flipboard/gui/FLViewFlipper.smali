.class public Lflipboard/gui/FLViewFlipper;
.super Landroid/widget/ViewFlipper;
.source "FLViewFlipper.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lflipboard/gui/FLViewFlipper;->addView(Landroid/view/View;)V

    .line 60
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040019

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 61
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04001c

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 62
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->showNext()V

    .line 63
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 30
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04001a

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLViewFlipper;->setInAnimation(Landroid/content/Context;I)V

    .line 31
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04001b

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLViewFlipper;->setOutAnimation(Landroid/content/Context;I)V

    .line 32
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getCurrentView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLViewFlipper;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 33
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->showPrevious()V

    .line 34
    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->getChildCount()I

    move-result v1

    sub-int/2addr v1, v0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLViewFlipper;->removeViews(II)V

    .line 35
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 68
    :try_start_0
    invoke-super {p0}, Landroid/widget/ViewFlipper;->onDetachedFromWindow()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 72
    :goto_0
    return-void

    .line 70
    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lflipboard/gui/FLViewFlipper;->stopFlipping()V

    goto :goto_0
.end method

.method public setDisplayedChild(I)V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 51
    invoke-virtual {p0, p1}, Lflipboard/gui/FLViewFlipper;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 52
    instance-of v1, v0, Lflipboard/gui/ContentDrawerView;

    if-eqz v1, :cond_0

    .line 53
    check-cast v0, Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->d()V

    .line 55
    :cond_0
    return-void
.end method
