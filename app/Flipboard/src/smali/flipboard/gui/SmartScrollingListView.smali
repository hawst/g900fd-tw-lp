.class public Lflipboard/gui/SmartScrollingListView;
.super Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;
.source "SmartScrollingListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field private static b:F

.field private static d:F


# instance fields
.field public a:Landroid/util/SparseIntArray;

.field private e:F

.field private f:Landroid/widget/AbsListView$OnScrollListener;

.field private g:Landroid/widget/Scroller;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;)V

    .line 34
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/SmartScrollingListView;->a:Landroid/util/SparseIntArray;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/SmartScrollingListView;->h:I

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/SmartScrollingListView;->a:Landroid/util/SparseIntArray;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/SmartScrollingListView;->h:I

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lflipboard/gui/SmartScrollingListView;->a:Landroid/util/SparseIntArray;

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lflipboard/gui/SmartScrollingListView;->h:I

    .line 49
    return-void
.end method

.method private static a(Landroid/widget/ListView;)I
    .locals 2

    .prologue
    .line 165
    :try_start_0
    const-class v0, Landroid/widget/AbsListView;

    const-string v1, "mVelocityTracker"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 166
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 167
    invoke-virtual {v0, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/VelocityTracker;

    .line 168
    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    float-to-int v0, v0

    .line 173
    :goto_0
    return v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_0

    .line 171
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 173
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 57
    invoke-super {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->onAttachedToWindow()V

    .line 58
    invoke-super {p0, p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 59
    new-instance v0, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    .line 61
    :try_start_0
    const-class v0, Landroid/widget/Scroller;

    const-string v1, "DECELERATION_RATE"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 62
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 63
    iget-object v1, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getFloat(Ljava/lang/Object;)F

    move-result v0

    sput v0, Lflipboard/gui/SmartScrollingListView;->b:F

    .line 65
    const-class v0, Landroid/widget/Scroller;

    const-string v1, "INFLEXION"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 66
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 67
    iget-object v1, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getFloat(Ljava/lang/Object;)F

    move-result v0

    sput v0, Lflipboard/gui/SmartScrollingListView;->d:F

    .line 69
    const-class v0, Landroid/widget/Scroller;

    const-string v1, "mPhysicalCoeff"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 70
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 71
    iget-object v1, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Field;->getFloat(Ljava/lang/Object;)F

    move-result v0

    iput v0, p0, Lflipboard/gui/SmartScrollingListView;->e:F

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/SmartScrollingListView;->i:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    :cond_0
    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v1, v1, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v1, :cond_0

    .line 75
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 82
    invoke-super {p0}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->onDetachedFromWindow()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    .line 84
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 195
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 12

    .prologue
    const v6, 0x7fffffff

    const/4 v9, 0x1

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 96
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/SmartScrollingListView;->i:Z

    if-eqz v0, :cond_9

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v2, "snap_scrolling"

    invoke-interface {v0, v2, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 97
    const/4 v0, 0x2

    if-ne p2, v0, :cond_b

    move v0, v1

    move v2, v1

    .line 99
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_1

    .line 100
    iget-object v3, p0, Lflipboard/gui/SmartScrollingListView;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 99
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 103
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v2, v0

    .line 104
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/SmartScrollingListView;->a(Landroid/widget/ListView;)I

    move-result v10

    .line 105
    if-eqz v10, :cond_9

    .line 106
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/Scroller;->setFriction(F)V

    .line 108
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 109
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    neg-int v4, v10

    move v3, v1

    move v5, v1

    move v7, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    .line 113
    iget v0, p0, Lflipboard/gui/SmartScrollingListView;->h:I

    neg-int v4, v0

    .line 115
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getCount()I

    move-result v6

    .line 116
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getFinalY()I

    move-result v7

    move v0, v1

    move v5, v1

    .line 118
    :goto_1
    if-ge v5, v6, :cond_8

    if-nez v0, :cond_8

    .line 119
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->a:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 120
    sub-int v8, v7, v4

    .line 121
    add-int v3, v4, v0

    .line 122
    sub-int v0, v7, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 123
    if-gez v10, :cond_4

    .line 125
    if-le v4, v2, :cond_3

    if-le v0, v8, :cond_3

    move v0, v9

    .line 126
    :goto_2
    if-nez v0, :cond_6

    .line 136
    :cond_2
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v3

    .line 137
    goto :goto_1

    :cond_3
    move v0, v1

    .line 125
    goto :goto_2

    .line 131
    :cond_4
    if-gt v3, v2, :cond_5

    if-le v0, v8, :cond_7

    :cond_5
    move v0, v9

    .line 132
    :goto_4
    if-eqz v0, :cond_2

    :cond_6
    move v3, v4

    goto :goto_3

    :cond_7
    move v0, v1

    .line 131
    goto :goto_4

    .line 138
    :cond_8
    sub-int v0, v2, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 139
    sub-int v3, v2, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 143
    sub-int v3, v0, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    int-to-float v0, v0

    div-float v0, v3, v0

    .line 144
    const v3, 0x3f4ccccd    # 0.8f

    cmpg-float v0, v0, v3

    if-gez v0, :cond_a

    .line 145
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v0

    sget v1, Lflipboard/gui/SmartScrollingListView;->b:F

    sget v3, Lflipboard/gui/SmartScrollingListView;->b:F

    sub-float/2addr v3, v11

    div-float/2addr v1, v3

    sget v3, Lflipboard/gui/SmartScrollingListView;->d:F

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    float-to-double v2, v3

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    float-to-double v4, v0

    div-double/2addr v2, v4

    sub-float v0, v1, v11

    div-float v0, v11, v0

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget v2, p0, Lflipboard/gui/SmartScrollingListView;->e:F

    float-to-double v2, v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 146
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setFriction(F)V

    .line 156
    :cond_9
    :goto_5
    return-void

    .line 148
    :cond_a
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFriction(F)V

    goto :goto_5

    .line 151
    :cond_b
    if-nez p2, :cond_9

    .line 152
    iget-object v0, p0, Lflipboard/gui/SmartScrollingListView;->g:Landroid/widget/Scroller;

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFriction(F)V

    .line 153
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getWrappedList()Landroid/widget/ListView;

    move-result-object v0

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setFriction(F)V

    goto :goto_5
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lflipboard/gui/SmartScrollingListView;->getFirstVisiblePosition()I

    move-result v0

    .line 202
    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/SmartScrollingListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-eqz v0, :cond_1

    .line 203
    :cond_0
    invoke-super {p0, p1}, Lse/emilsjolander/stickylistheaders/StickyListHeadersListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 205
    :cond_1
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lflipboard/gui/SmartScrollingListView;->f:Landroid/widget/AbsListView$OnScrollListener;

    .line 89
    return-void
.end method

.method public setTopOffset(I)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lflipboard/gui/SmartScrollingListView;->h:I

    .line 53
    return-void
.end method
