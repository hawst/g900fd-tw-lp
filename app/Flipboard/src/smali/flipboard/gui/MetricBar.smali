.class public Lflipboard/gui/MetricBar;
.super Landroid/widget/LinearLayout;
.source "MetricBar.java"


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lflipboard/gui/MetricBar$MetricItemViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Landroid/graphics/Paint;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    .line 31
    iput v2, p0, Lflipboard/gui/MetricBar;->d:I

    .line 32
    iput v2, p0, Lflipboard/gui/MetricBar;->e:I

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    .line 35
    const-string v0, "normal"

    iput-object v0, p0, Lflipboard/gui/MetricBar;->f:Ljava/lang/String;

    .line 36
    const-string v0, "normal"

    iput-object v0, p0, Lflipboard/gui/MetricBar;->g:Ljava/lang/String;

    .line 40
    invoke-direct {p0, p1}, Lflipboard/gui/MetricBar;->a(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 44
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    .line 31
    iput v2, p0, Lflipboard/gui/MetricBar;->d:I

    .line 32
    iput v2, p0, Lflipboard/gui/MetricBar;->e:I

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    .line 35
    const-string v0, "normal"

    iput-object v0, p0, Lflipboard/gui/MetricBar;->f:Ljava/lang/String;

    .line 36
    const-string v0, "normal"

    iput-object v0, p0, Lflipboard/gui/MetricBar;->g:Ljava/lang/String;

    .line 45
    invoke-direct {p0, p1}, Lflipboard/gui/MetricBar;->a(Landroid/content/Context;)V

    .line 46
    invoke-direct {p0, p1, p2}, Lflipboard/gui/MetricBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/16 v2, 0xb

    .line 50
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    .line 31
    iput v2, p0, Lflipboard/gui/MetricBar;->d:I

    .line 32
    iput v2, p0, Lflipboard/gui/MetricBar;->e:I

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    .line 35
    const-string v0, "normal"

    iput-object v0, p0, Lflipboard/gui/MetricBar;->f:Ljava/lang/String;

    .line 36
    const-string v0, "normal"

    iput-object v0, p0, Lflipboard/gui/MetricBar;->g:Ljava/lang/String;

    .line 51
    invoke-direct {p0, p1}, Lflipboard/gui/MetricBar;->a(Landroid/content/Context;)V

    .line 52
    invoke-direct {p0, p1, p2}, Lflipboard/gui/MetricBar;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/MetricBar;->setOrientation(I)V

    .line 57
    iget-object v0, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 58
    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {v0, p1}, Lflipboard/util/AndroidUtil;->a(FLandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/MetricBar;->i:I

    .line 59
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    .line 62
    sget-object v0, Lflipboard/app/R$styleable;->MetricBar:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 63
    invoke-virtual {p0}, Lflipboard/gui/MetricBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f080083

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 64
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 65
    const/4 v3, 0x1

    invoke-virtual {v1, v3, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    .line 66
    iput v2, p0, Lflipboard/gui/MetricBar;->b:I

    iput v3, p0, Lflipboard/gui/MetricBar;->c:I

    iget-object v0, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    const/16 v4, 0x5a

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MetricBar$MetricItemViewHolder;

    invoke-static {v0, v2, v3}, Lflipboard/gui/MetricBar$MetricItemViewHolder;->a(Lflipboard/gui/MetricBar$MetricItemViewHolder;II)V

    goto :goto_0

    .line 67
    :cond_0
    const/4 v0, 0x2

    iget v2, p0, Lflipboard/gui/MetricBar;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/MetricBar;->d:I

    .line 68
    const/4 v0, 0x3

    iget v2, p0, Lflipboard/gui/MetricBar;->e:I

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lflipboard/gui/MetricBar;->e:I

    .line 69
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_1

    .line 71
    iput-object v0, p0, Lflipboard/gui/MetricBar;->f:Ljava/lang/String;

    .line 73
    :cond_1
    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    iput-object v0, p0, Lflipboard/gui/MetricBar;->g:Ljava/lang/String;

    .line 77
    :cond_2
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 78
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lflipboard/gui/MetricBar;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 126
    invoke-virtual {p0}, Lflipboard/gui/MetricBar;->removeAllViews()V

    .line 127
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 138
    invoke-virtual {p0}, Lflipboard/gui/MetricBar;->getChildCount()I

    move-result v7

    .line 139
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    add-int/lit8 v0, v7, -0x1

    if-ge v6, v0, :cond_0

    .line 140
    invoke-virtual {p0, v6}, Lflipboard/gui/MetricBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 141
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lflipboard/gui/MetricBar;->i:I

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lflipboard/gui/MetricBar;->getMeasuredHeight()I

    move-result v0

    iget v4, p0, Lflipboard/gui/MetricBar;->i:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lflipboard/gui/MetricBar;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 139
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 143
    :cond_0
    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lflipboard/gui/MetricBar;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    const/4 v2, -0x2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    return-object v0
.end method
