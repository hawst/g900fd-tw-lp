.class public Lflipboard/gui/discovery/DiscoveryFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "DiscoveryFragment.java"


# instance fields
.field a:Lflipboard/gui/FLSearchView;

.field b:Landroid/widget/ListView;

.field c:Lflipboard/gui/FLEditText;

.field d:Landroid/view/View;

.field private e:Lflipboard/service/RemoteWatchedFile$Observer;

.field private f:Lflipboard/service/RemoteWatchedFile;

.field private g:Landroid/widget/BaseAdapter;

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/discovery/DiscoveryFragment;)Landroid/widget/BaseAdapter;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->g:Landroid/widget/BaseAdapter;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigSection;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ConfigSection;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x2

    const/4 v10, 0x4

    const/4 v9, 0x3

    .line 212
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 214
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_8

    .line 215
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 217
    add-int/lit8 v1, v4, 0x1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 218
    add-int/lit8 v1, v4, 0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/ContentDrawerListItem;

    move-object v3, v1

    .line 223
    :goto_1
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v10, :cond_1

    move-object v1, v0

    .line 224
    check-cast v1, Lflipboard/objs/ConfigSection;

    .line 226
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_1

    .line 227
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v2, v2, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v2, v11, :cond_4

    .line 229
    new-instance v7, Lflipboard/objs/DualBrick;

    invoke-direct {v7}, Lflipboard/objs/DualBrick;-><init>()V

    .line 230
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v7, v2}, Lflipboard/objs/DualBrick;->a(Lflipboard/objs/ConfigBrick;)V

    .line 232
    if-eqz v3, :cond_0

    invoke-interface {v3}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v10, :cond_0

    move-object v2, v3

    .line 233
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 234
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v8, :cond_0

    .line 235
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v8, v8, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v8, v11, :cond_0

    .line 236
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iput-object v8, v7, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    .line 237
    iget-object v8, v7, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iput-object v2, v8, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    .line 238
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    add-int/lit8 v4, v4, 0x1

    .line 243
    :cond_0
    iput-object v7, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    .line 276
    :cond_1
    :goto_2
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    if-ne v1, v9, :cond_2

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 277
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lflipboard/service/ContentDrawerHandler;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/objs/ContentDrawerListItem;->a(Ljava/util/List;)V

    .line 214
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    move-object v3, v5

    .line 220
    goto :goto_1

    .line 244
    :cond_4
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v2, v2, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v2, v9, :cond_1

    .line 246
    new-instance v7, Lflipboard/objs/TripleBrick;

    invoke-direct {v7}, Lflipboard/objs/TripleBrick;-><init>()V

    .line 247
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v7, v2}, Lflipboard/objs/TripleBrick;->a(Lflipboard/objs/ConfigBrick;)V

    .line 248
    if-eqz v3, :cond_5

    invoke-interface {v3}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v10, :cond_5

    move-object v2, v3

    .line 249
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 250
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v8, :cond_5

    .line 251
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v8, v8, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v8, v9, :cond_5

    .line 252
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iput-object v8, v7, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    .line 253
    iget-object v8, v7, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iput-object v2, v8, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    .line 254
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    add-int/lit8 v4, v4, 0x1

    .line 259
    :cond_5
    add-int/lit8 v2, v4, 0x1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    add-int/lit8 v2, v4, 0x1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/ConfigSection;

    move-object v3, v2

    .line 260
    :goto_3
    if-eqz v3, :cond_6

    invoke-interface {v3}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v2

    if-ne v2, v10, :cond_6

    move-object v2, v3

    .line 261
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 262
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v8, :cond_6

    .line 263
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v8, v8, Lflipboard/objs/ConfigBrick;->g:I

    if-ne v8, v9, :cond_6

    .line 264
    iget-object v8, v2, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iput-object v8, v7, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    .line 265
    iget-object v8, v7, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iput-object v2, v8, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    .line 266
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    add-int/lit8 v4, v4, 0x1

    .line 271
    :cond_6
    iput-object v7, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    goto/16 :goto_2

    :cond_7
    move-object v3, v5

    .line 259
    goto :goto_3

    .line 280
    :cond_8
    invoke-interface {p0, v6}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 281
    return-object p0
.end method

.method static synthetic a(Lflipboard/gui/discovery/DiscoveryFragment;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 51
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigFolder;

    new-instance v1, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v4, v0, Lflipboard/objs/ConfigFolder;->bP:Ljava/lang/String;

    invoke-direct {v1, v4, v5}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/ConfigSection;

    iput-object v5, v1, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    goto :goto_1

    :cond_0
    iget-object v0, v0, Lflipboard/objs/ConfigFolder;->d:Ljava/util/List;

    invoke-static {v0}, Lflipboard/gui/discovery/DiscoveryFragment;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lflipboard/objs/GuideSwitch;

    invoke-direct {v0}, Lflipboard/objs/GuideSwitch;-><init>()V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->a:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLSearchView;->setDefaultSearchList(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->g:Landroid/widget/BaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->g:Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 178
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->a:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLSearchView;->setSearchQuery(Ljava/lang/String;)V

    .line 369
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 183
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lflipboard/activities/FlipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 184
    iget-object v1, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    invoke-virtual {v1}, Lflipboard/gui/FLEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 185
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 186
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 75
    const v0, 0x7f0300fc

    invoke-virtual {p1, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 76
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 78
    new-instance v0, Lflipboard/gui/discovery/DiscoveryFragment$1;

    invoke-direct {v0, p0}, Lflipboard/gui/discovery/DiscoveryFragment$1;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 85
    new-instance v0, Lflipboard/gui/discovery/DiscoveryFragment$6;

    invoke-direct {v0, p0}, Lflipboard/gui/discovery/DiscoveryFragment$6;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->e:Lflipboard/service/RemoteWatchedFile$Observer;

    .line 86
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v2, "contentGuide.json"

    iget-object v3, p0, Lflipboard/gui/discovery/DiscoveryFragment;->e:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v2, v3}, Lflipboard/service/FlipboardManager;->a(Ljava/lang/String;Lflipboard/service/RemoteWatchedFile$Observer;)Lflipboard/service/RemoteWatchedFile;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->f:Lflipboard/service/RemoteWatchedFile;

    .line 90
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->a:Lflipboard/gui/FLSearchView;

    invoke-virtual {v0}, Lflipboard/gui/FLSearchView;->getNewSearchAdapter()Lflipboard/gui/SearchListAdapter;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->g:Landroid/widget/BaseAdapter;

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/discovery/DiscoveryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f09007b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->h:I

    .line 93
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v2, "key_top_offset"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lflipboard/activities/FlipboardActivity;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->i:I

    .line 95
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->l()Lflipboard/service/FlipboardManager$RootScreenStyle;

    move-result-object v0

    .line 96
    sget-object v2, Lflipboard/service/FlipboardManager$RootScreenStyle;->b:Lflipboard/service/FlipboardManager$RootScreenStyle;

    if-ne v0, v2, :cond_0

    .line 97
    const v0, 0x7f0a02cd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 99
    invoke-virtual {p0}, Lflipboard/gui/discovery/DiscoveryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0900b8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v5, v5, v3, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 100
    invoke-virtual {v2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 101
    invoke-virtual {p0}, Lflipboard/gui/discovery/DiscoveryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f02021b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 102
    invoke-static {}, Lflipboard/util/ColorFilterUtil;->a()Landroid/graphics/ColorFilter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 103
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 104
    iget-object v2, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    invoke-virtual {v2, v0, v6, v6, v6}, Lflipboard/gui/FLEditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 106
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    new-instance v2, Lflipboard/gui/discovery/DiscoveryFragment$2;

    invoke-direct {v2, p0}, Lflipboard/gui/discovery/DiscoveryFragment$2;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 118
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    new-instance v2, Lflipboard/gui/discovery/DiscoveryFragment$3;

    invoke-direct {v2, p0}, Lflipboard/gui/discovery/DiscoveryFragment$3;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 142
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    new-instance v2, Lflipboard/gui/discovery/DiscoveryFragment$4;

    invoke-direct {v2, p0}, Lflipboard/gui/discovery/DiscoveryFragment$4;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLEditText;->setKeyBoardCloseListener(Lflipboard/gui/FLEditText$OnKeyBoardCloseListener;)V

    .line 152
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    new-instance v2, Lflipboard/gui/discovery/DiscoveryFragment$5;

    invoke-direct {v2, p0}, Lflipboard/gui/discovery/DiscoveryFragment$5;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 164
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/discovery/DiscoveryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 165
    const-string v2, "key_search_text"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 166
    const-string v2, "key_search_text"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 167
    invoke-virtual {p0, v2}, Lflipboard/gui/discovery/DiscoveryFragment;->a(Ljava/lang/String;)V

    .line 170
    const-string v2, "key_search_text"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    .line 173
    :cond_1
    return-object v1
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 347
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    .line 348
    iget-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->f:Lflipboard/service/RemoteWatchedFile;

    iget-object v1, p0, Lflipboard/gui/discovery/DiscoveryFragment;->e:Lflipboard/service/RemoteWatchedFile$Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/RemoteWatchedFile;->a(Lflipboard/service/RemoteWatchedFile$Observer;)V

    .line 349
    iput-object v2, p0, Lflipboard/gui/discovery/DiscoveryFragment;->f:Lflipboard/service/RemoteWatchedFile;

    .line 350
    iput-object v2, p0, Lflipboard/gui/discovery/DiscoveryFragment;->e:Lflipboard/service/RemoteWatchedFile$Observer;

    .line 351
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroy()V

    .line 352
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 356
    invoke-virtual {p0}, Lflipboard/gui/discovery/DiscoveryFragment;->b()V

    .line 357
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onStop()V

    .line 359
    return-void
.end method
