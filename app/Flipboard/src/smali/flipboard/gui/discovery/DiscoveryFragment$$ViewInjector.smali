.class public Lflipboard/gui/discovery/DiscoveryFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "DiscoveryFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/discovery/DiscoveryFragment;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a010b

    const-string v1, "field \'searchView\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Lflipboard/gui/FLSearchView;

    iput-object v0, p1, Lflipboard/gui/discovery/DiscoveryFragment;->a:Lflipboard/gui/FLSearchView;

    .line 12
    const v0, 0x7f0a02d4

    const-string v1, "field \'searchList\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p1, Lflipboard/gui/discovery/DiscoveryFragment;->b:Landroid/widget/ListView;

    .line 14
    const v0, 0x7f0a0138

    const-string v1, "field \'searchBox\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/FLEditText;

    iput-object v0, p1, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    .line 16
    const v0, 0x7f0a02d5

    const-string v1, "field \'searchListMask\' and method \'onMaskClicked\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    iput-object v0, p1, Lflipboard/gui/discovery/DiscoveryFragment;->d:Landroid/view/View;

    .line 18
    new-instance v1, Lflipboard/gui/discovery/DiscoveryFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lflipboard/gui/discovery/DiscoveryFragment$$ViewInjector$1;-><init>(Lflipboard/gui/discovery/DiscoveryFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    return-void
.end method

.method public static reset(Lflipboard/gui/discovery/DiscoveryFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->a:Lflipboard/gui/FLSearchView;

    .line 30
    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->b:Landroid/widget/ListView;

    .line 31
    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->c:Lflipboard/gui/FLEditText;

    .line 32
    iput-object v0, p0, Lflipboard/gui/discovery/DiscoveryFragment;->d:Landroid/view/View;

    .line 33
    return-void
.end method
