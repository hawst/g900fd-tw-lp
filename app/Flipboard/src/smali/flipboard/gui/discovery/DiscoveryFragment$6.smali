.class Lflipboard/gui/discovery/DiscoveryFragment$6;
.super Ljava/lang/Object;
.source "DiscoveryFragment.java"

# interfaces
.implements Lflipboard/service/RemoteWatchedFile$Observer;


# instance fields
.field final synthetic a:Lflipboard/gui/discovery/DiscoveryFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/discovery/DiscoveryFragment;)V
    .locals 0

    .prologue
    .line 286
    iput-object p1, p0, Lflipboard/gui/discovery/DiscoveryFragment$6;->a:Lflipboard/gui/discovery/DiscoveryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 333
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "fail loading sections.json: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 334
    return-void
.end method

.method public final a(Ljava/lang/String;[BZ)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 291
    :try_start_0
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, p2}, Lflipboard/json/JSONParser;-><init>([B)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->g()Lflipboard/objs/ConfigContentGuide;

    move-result-object v3

    .line 292
    if-eqz v3, :cond_0

    iget-object v0, v3, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    if-nez v0, :cond_1

    .line 293
    :cond_0
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Failed to load valid content guide, it or its sections is null"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 327
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v4, v3, Lflipboard/objs/ConfigContentGuide;->a:Ljava/util/List;

    .line 299
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v3, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->d(Ljava/util/List;)V

    .line 301
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 302
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 303
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ConfigFolder;

    .line 304
    iget-object v6, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v3, Lflipboard/objs/ConfigContentGuide;->b:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v8, :cond_2

    .line 306
    iget-object v6, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    const-string v7, "new"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 307
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    :cond_2
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 309
    :cond_3
    iget-object v6, v0, Lflipboard/objs/ConfigFolder;->a:Ljava/lang/String;

    const-string v7, "simplifiedui"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 310
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 323
    :catch_0
    move-exception v0

    .line 324
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v3, "failed to parse sections.json: %-E"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v2

    invoke-virtual {v1, v3, v4}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 326
    throw v0

    .line 316
    :cond_4
    :try_start_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/discovery/DiscoveryFragment$6$1;

    invoke-direct {v1, p0, v5}, Lflipboard/gui/discovery/DiscoveryFragment$6$1;-><init>(Lflipboard/gui/discovery/DiscoveryFragment$6;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 339
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "fail loading sections.json, maintenance: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 340
    return-void
.end method
