.class Lflipboard/gui/FLStaticTextView$Segment;
.super Ljava/lang/Object;
.source "FLStaticTextView.java"


# instance fields
.field c:I

.field d:I

.field e:F

.field f:F

.field g:F

.field h:Z

.field final synthetic i:Lflipboard/gui/FLStaticTextView;


# direct methods
.method constructor <init>(Lflipboard/gui/FLStaticTextView;IIFFF)V
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    iput p2, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    .line 397
    iput p3, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    .line 398
    iput p4, p0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    .line 399
    iput p5, p0, Lflipboard/gui/FLStaticTextView$Segment;->f:F

    .line 400
    iput p6, p0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    .line 401
    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 424
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v1}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 405
    sget-boolean v0, Lflipboard/service/FlipboardManager;->r:Z

    if-eqz v0, :cond_1

    .line 406
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v2}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v2

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v5, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    .line 408
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v1}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iget-object v1, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v1}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    sub-float/2addr v1, v2

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    sub-float/2addr v1, v2

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->f:F

    iget-object v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 420
    :goto_0
    return-void

    .line 410
    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v1}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int/2addr v3, v4

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/String;-><init>([CII)V

    iget v1, p0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->f:F

    iget-object v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v3}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 413
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->a(Lflipboard/gui/FLStaticTextView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 414
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v2}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v2

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v5, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int/2addr v4, v5

    invoke-direct {v1, v2, v3, v4}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    .line 415
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int v3, v0, v3

    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    sub-float/2addr v0, v4

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    sub-float v4, v0, v4

    iget v5, p0, Lflipboard/gui/FLStaticTextView$Segment;->f:F

    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v6

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 417
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->b(Lflipboard/gui/FLStaticTextView;)[C

    move-result-object v1

    iget v2, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    iget v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    sub-int v3, v0, v3

    iget v4, p0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    iget v5, p0, Lflipboard/gui/FLStaticTextView$Segment;->f:F

    iget-object v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->i:Lflipboard/gui/FLStaticTextView;

    invoke-static {v0}, Lflipboard/gui/FLStaticTextView;->c(Lflipboard/gui/FLStaticTextView;)Landroid/graphics/Paint;

    move-result-object v6

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText([CIIFFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 429
    const-string v1, "%d-%d,%f,%f,%f,%s%s"

    const/4 v0, 0x7

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->e:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->f:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    iget v3, p0, Lflipboard/gui/FLStaticTextView$Segment;->g:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x5

    iget-boolean v0, p0, Lflipboard/gui/FLStaticTextView$Segment;->h:Z

    if-eqz v0, :cond_0

    const-string v0, "#,"

    :goto_0
    aput-object v0, v2, v3

    const/4 v0, 0x6

    invoke-virtual {p0}, Lflipboard/gui/FLStaticTextView$Segment;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
