.class Lflipboard/gui/CommentaryList$7$1;
.super Lflipboard/gui/dialog/FLDialogAdapter;
.source "CommentaryList.java"


# instance fields
.field final synthetic a:Lflipboard/gui/CommentaryList$7;


# direct methods
.method constructor <init>(Lflipboard/gui/CommentaryList$7;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Landroid/support/v4/app/DialogFragment;)V
    .locals 4

    .prologue
    .line 671
    iget-object v0, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    iget-object v0, v0, Lflipboard/gui/CommentaryList$7;->c:Lflipboard/gui/CommentaryList;

    iget-object v1, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    iget-object v1, v1, Lflipboard/gui/CommentaryList$7;->a:Lflipboard/gui/CommentaryList$Comment;

    const-string v2, "reportComment"

    invoke-static {v0, v1, v2}, Lflipboard/gui/CommentaryList;->a(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$Comment;Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    iget-object v0, v0, Lflipboard/gui/CommentaryList$7;->c:Lflipboard/gui/CommentaryList;

    iget-object v0, v0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    .line 674
    if-eqz v0, :cond_1

    .line 675
    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    .line 676
    if-eqz v0, :cond_0

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v3, "comment"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    iget-object v3, v3, Lflipboard/gui/CommentaryList$7;->a:Lflipboard/gui/CommentaryList$Comment;

    iget-object v3, v3, Lflipboard/gui/CommentaryList$Comment;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 677
    iget-object v2, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    iget-object v2, v2, Lflipboard/gui/CommentaryList$7;->b:Landroid/view/ViewGroup;

    iget-object v3, p0, Lflipboard/gui/CommentaryList$7$1;->a:Lflipboard/gui/CommentaryList$7;

    iget-object v3, v3, Lflipboard/gui/CommentaryList$7;->b:Landroid/view/ViewGroup;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->h:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 682
    :cond_1
    return-void
.end method
