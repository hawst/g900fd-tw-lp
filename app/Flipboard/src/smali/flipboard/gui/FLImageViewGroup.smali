.class public Lflipboard/gui/FLImageViewGroup;
.super Landroid/view/ViewGroup;
.source "FLImageViewGroup.java"


# instance fields
.field private final a:Lflipboard/gui/ImageGroup;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/FLImageView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 22
    new-instance v0, Lflipboard/gui/ImageGroup;

    invoke-direct {v0}, Lflipboard/gui/ImageGroup;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    .line 30
    invoke-virtual {p0}, Lflipboard/gui/FLImageViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0900d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLImageViewGroup;->c:I

    .line 31
    invoke-virtual {p0}, Lflipboard/gui/FLImageViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lflipboard/gui/FLImageViewGroup;->d:I

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/FLImageViewGroup;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Lflipboard/gui/ImageGroup;

    invoke-direct {v0}, Lflipboard/gui/ImageGroup;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    .line 36
    iget v0, p2, Lflipboard/gui/FLImageViewGroup;->c:I

    iput v0, p0, Lflipboard/gui/FLImageViewGroup;->c:I

    .line 37
    iget v0, p2, Lflipboard/gui/FLImageViewGroup;->d:I

    iput v0, p0, Lflipboard/gui/FLImageViewGroup;->d:I

    .line 38
    iget-object v0, p2, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    iget-object v0, v0, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLImageViewGroup;->setImages(Ljava/util/List;)V

    .line 39
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 62
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v0}, Lflipboard/gui/ImageGroup;->a()I

    move-result v2

    .line 63
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 64
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 65
    iget-object v3, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v3, v1}, Lflipboard/gui/ImageGroup;->a(I)Lflipboard/objs/Image;

    move-result-object v3

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImageBestFit(Lflipboard/objs/Image;)V

    .line 63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 110
    sub-int v4, p4, p2

    .line 111
    sub-int v5, p5, p3

    .line 112
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v0}, Lflipboard/gui/ImageGroup;->a()I

    move-result v6

    .line 113
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v6, :cond_2

    .line 114
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 115
    iget-object v1, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v1, v3}, Lflipboard/gui/ImageGroup;->b(I)Lflipboard/gui/ImageGroup$ImageLayout;

    move-result-object v7

    .line 116
    iget v1, v7, Lflipboard/gui/ImageGroup$ImageLayout;->a:F

    int-to-float v2, v4

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 117
    iget v2, v7, Lflipboard/gui/ImageGroup$ImageLayout;->a:F

    cmpl-float v2, v2, v9

    if-lez v2, :cond_0

    .line 118
    iget v2, p0, Lflipboard/gui/FLImageViewGroup;->c:I

    add-int/2addr v1, v2

    .line 120
    :cond_0
    iget v2, v7, Lflipboard/gui/ImageGroup$ImageLayout;->b:F

    int-to-float v8, v5

    mul-float/2addr v2, v8

    float-to-int v2, v2

    .line 121
    iget v7, v7, Lflipboard/gui/ImageGroup$ImageLayout;->b:F

    cmpl-float v7, v7, v9

    if-lez v7, :cond_1

    .line 122
    iget v7, p0, Lflipboard/gui/FLImageViewGroup;->c:I

    add-int/2addr v2, v7

    .line 124
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v2

    invoke-virtual {v0, v1, v2, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 113
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 126
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 71
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 72
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 73
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 74
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 76
    iget-object v3, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v3}, Lflipboard/gui/ImageGroup;->a()I

    move-result v6

    .line 77
    const/4 v3, 0x1

    if-ne v6, v3, :cond_0

    .line 78
    iget-object v3, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 79
    invoke-static {v4, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v5, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 80
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lflipboard/gui/FLImageViewGroup;->setMeasuredDimension(II)V

    .line 106
    :goto_0
    return-void

    :cond_0
    move v3, v0

    .line 82
    :goto_1
    if-ge v3, v6, :cond_2

    .line 83
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v0, v3}, Lflipboard/gui/ImageGroup;->b(I)Lflipboard/gui/ImageGroup$ImageLayout;

    move-result-object v7

    .line 84
    iget v0, v7, Lflipboard/gui/ImageGroup$ImageLayout;->c:F

    int-to-float v1, v4

    mul-float/2addr v0, v1

    float-to-int v0, v0

    .line 85
    iget v1, v7, Lflipboard/gui/ImageGroup$ImageLayout;->d:F

    int-to-float v2, v5

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 87
    iget v2, v7, Lflipboard/gui/ImageGroup$ImageLayout;->a:F

    int-to-float v8, v4

    mul-float/2addr v2, v8

    float-to-int v2, v2

    .line 88
    iget v8, v7, Lflipboard/gui/ImageGroup$ImageLayout;->b:F

    int-to-float v9, v5

    mul-float/2addr v8, v9

    float-to-int v8, v8

    .line 91
    sub-int v2, v4, v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 92
    sub-int v2, v5, v8

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 95
    iget v1, v7, Lflipboard/gui/ImageGroup$ImageLayout;->a:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_3

    .line 96
    iget v1, p0, Lflipboard/gui/FLImageViewGroup;->c:I

    sub-int/2addr v0, v1

    move v1, v0

    .line 98
    :goto_2
    iget v0, v7, Lflipboard/gui/ImageGroup$ImageLayout;->b:F

    cmpl-float v0, v0, v10

    if-lez v0, :cond_1

    .line 99
    iget v0, p0, Lflipboard/gui/FLImageViewGroup;->c:I

    sub-int v0, v2, v0

    move v2, v0

    .line 101
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 102
    invoke-static {v1, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v2, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 82
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 104
    :cond_2
    invoke-virtual {p0, v4, v5}, Lflipboard/gui/FLImageViewGroup;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_3
    move v1, v0

    goto :goto_2
.end method

.method public setImages(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 43
    invoke-virtual {p0}, Lflipboard/gui/FLImageViewGroup;->removeAllViews()V

    .line 44
    iget-object v3, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    iget-object v0, v3, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    iget-object v0, v3, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, v3, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    iget-object v0, v3, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->h()F

    move-result v0

    const v5, 0x3fcccccd    # 1.6f

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-static {v4, v0}, Lflipboard/gui/ImageGroup;->a(IZ)Lflipboard/gui/ImageGroup$ImageGroupLayout;

    move-result-object v0

    iput-object v0, v3, Lflipboard/gui/ImageGroup;->b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 45
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v0}, Lflipboard/gui/ImageGroup;->a()I

    move-result v0

    .line 46
    :goto_2
    if-ge v2, v0, :cond_5

    .line 47
    new-instance v3, Lflipboard/gui/FLImageView;

    invoke-virtual {p0}, Lflipboard/gui/FLImageViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {v3, v1}, Lflipboard/gui/FLImageView;->setFade(Z)V

    .line 49
    invoke-virtual {v3, v1}, Lflipboard/gui/FLImageView;->setRecycle(Z)V

    .line 50
    iget v4, p0, Lflipboard/gui/FLImageViewGroup;->d:I

    invoke-virtual {v3, v4}, Lflipboard/gui/FLImageView;->setBackgroundColor(I)V

    .line 51
    invoke-virtual {p0, v3}, Lflipboard/gui/FLImageViewGroup;->addView(Landroid/view/View;)V

    .line 52
    iget-object v4, p0, Lflipboard/gui/FLImageViewGroup;->b:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    iget-object v4, p0, Lflipboard/gui/FLImageViewGroup;->a:Lflipboard/gui/ImageGroup;

    invoke-virtual {v4, v2}, Lflipboard/gui/ImageGroup;->a(I)Lflipboard/objs/Image;

    move-result-object v4

    .line 54
    invoke-virtual {v4}, Lflipboard/objs/Image;->c()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 55
    sget-object v5, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v3, v5}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 57
    :cond_1
    iget v5, v4, Lflipboard/objs/Image;->f:I

    iget v4, v4, Lflipboard/objs/Image;->g:I

    invoke-virtual {v3, v5, v4}, Lflipboard/gui/FLImageView;->a(II)V

    .line 46
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 44
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v5, :cond_3

    iget-object v0, v3, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {p1, v2, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_3
    iget-object v0, v3, Lflipboard/gui/ImageGroup;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    .line 59
    :cond_5
    return-void
.end method
