.class public Lflipboard/gui/SocialCardMainItem;
.super Lflipboard/gui/FLRelativeLayout;
.source "SocialCardMainItem.java"


# instance fields
.field public a:Lflipboard/service/Section;

.field public b:Lflipboard/objs/FeedItem;

.field public c:Lflipboard/gui/FLImageView;

.field public d:Lflipboard/gui/FLStaticTextView;

.field public e:Lflipboard/gui/FLLabelTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lflipboard/gui/SocialCardMainItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/SocialCardMainItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 36
    const v1, 0x7f030128

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 38
    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 53
    const v0, 0x7f0a008e

    invoke-virtual {p0, v0}, Lflipboard/gui/SocialCardMainItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/SocialCardMainItem;->c:Lflipboard/gui/FLImageView;

    .line 54
    const v0, 0x7f0a00c0

    invoke-virtual {p0, v0}, Lflipboard/gui/SocialCardMainItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/SocialCardMainItem;->d:Lflipboard/gui/FLStaticTextView;

    .line 55
    const v0, 0x7f0a00c3

    invoke-virtual {p0, v0}, Lflipboard/gui/SocialCardMainItem;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, p0, Lflipboard/gui/SocialCardMainItem;->e:Lflipboard/gui/FLLabelTextView;

    .line 56
    new-instance v0, Lflipboard/gui/SocialCardMainItem$1;

    invoke-direct {v0, p0}, Lflipboard/gui/SocialCardMainItem$1;-><init>(Lflipboard/gui/SocialCardMainItem;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/SocialCardMainItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    return-void
.end method
