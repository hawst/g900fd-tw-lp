.class public final enum Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;
.super Ljava/lang/Enum;
.source "LightBoxFragment.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

.field public static final enum b:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

.field private static final synthetic d:[Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;


# instance fields
.field c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 76
    new-instance v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    const-string v1, "firstFlip_noAccount"

    const-string v2, "firstFlip-noAccount"

    invoke-direct {v0, v1, v3, v2}, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->a:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    new-instance v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    const-string v1, "learn_more"

    const-string v2, "learn-more"

    invoke-direct {v0, v1, v4, v2}, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->b:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    .line 75
    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    sget-object v1, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->a:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->b:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->d:[Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput-object p3, p0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->c:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;
    .locals 1

    .prologue
    .line 75
    const-class v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->d:[Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-virtual {v0}, [Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    return-object v0
.end method
