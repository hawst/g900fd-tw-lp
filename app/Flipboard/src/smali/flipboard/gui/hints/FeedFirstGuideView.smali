.class public Lflipboard/gui/hints/FeedFirstGuideView;
.super Lflipboard/gui/FLRelativeLayout;
.source "FeedFirstGuideView.java"

# interfaces
.implements Lflipboard/gui/item/TabletItem;


# instance fields
.field a:Lflipboard/gui/FLImageView;

.field b:Lflipboard/gui/FLTextView;

.field c:Lflipboard/gui/FLTextView;

.field d:Lflipboard/gui/FLTextView;

.field e:Lflipboard/gui/FLTextView;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lflipboard/gui/hints/FeedFirstGuideView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    iput-object p1, p0, Lflipboard/gui/hints/FeedFirstGuideView;->f:Landroid/content/Context;

    .line 43
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V
    .locals 6

    .prologue
    .line 62
    const-string v2, "https://cdn.flipboard.com/assets/feed_first_guide_background.jpg"

    .line 64
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    const-string v1, "feed_first_guide_new_user_key"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 65
    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->f:Landroid/content/Context;

    const v1, 0x7f0d0172

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->f:Landroid/content/Context;

    const v3, 0x7f0d0171

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 70
    const v3, 0x7f0a0184

    invoke-virtual {p0, v3}, Lflipboard/gui/hints/FeedFirstGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 71
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 80
    :goto_0
    const-string v3, "FLIP"

    .line 82
    iget-object v4, p0, Lflipboard/gui/hints/FeedFirstGuideView;->a:Lflipboard/gui/FLImageView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setDisableLoadingView(Z)V

    .line 83
    iget-object v4, p0, Lflipboard/gui/hints/FeedFirstGuideView;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v4, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 86
    iget-object v2, p0, Lflipboard/gui/hints/FeedFirstGuideView;->c:Lflipboard/gui/FLTextView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    iget-object v1, p0, Lflipboard/gui/hints/FeedFirstGuideView;->d:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->e:Lflipboard/gui/FLTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->f:Landroid/content/Context;

    const v1, 0x7f0d016f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 74
    iget-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->f:Landroid/content/Context;

    const v1, 0x7f0d016e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 75
    iget-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->f:Landroid/content/Context;

    const v4, 0x7f0d0170

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 77
    iget-object v4, p0, Lflipboard/gui/hints/FeedFirstGuideView;->b:Lflipboard/gui/FLTextView;

    invoke-virtual {v4, v3}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public getItem()Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    const v0, 0x7f0a0182

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/FeedFirstGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->a:Lflipboard/gui/FLImageView;

    .line 49
    const v0, 0x7f0a0185

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/FeedFirstGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->b:Lflipboard/gui/FLTextView;

    .line 50
    const v0, 0x7f0a0183

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/FeedFirstGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->c:Lflipboard/gui/FLTextView;

    .line 51
    const v0, 0x7f0a0186

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/FeedFirstGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->d:Lflipboard/gui/FLTextView;

    .line 52
    const v0, 0x7f0a0189

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/FeedFirstGuideView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/hints/FeedFirstGuideView;->e:Lflipboard/gui/FLTextView;

    .line 53
    return-void
.end method
