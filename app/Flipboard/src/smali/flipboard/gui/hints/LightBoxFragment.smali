.class public Lflipboard/gui/hints/LightBoxFragment;
.super Lflipboard/gui/dialog/FLDialogFragment;
.source "LightBoxFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field j:I

.field k:I

.field public l:Landroid/view/View$OnClickListener;

.field private m:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/widget/ViewFlipper;

.field private p:I

.field private q:I

.field private r:Ljava/lang/StringBuffer;

.field private s:Lflipboard/gui/FLButton;

.field private t:Lflipboard/gui/FLTextView;

.field private u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/LightBoxes$Page;",
            ">;"
        }
    .end annotation
.end field

.field private v:Lflipboard/objs/LightBoxes;

.field private w:Landroid/view/View;

.field private x:I

.field private final y:Ljava/lang/String;

.field private final z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Lflipboard/gui/dialog/FLDialogFragment;-><init>()V

    .line 66
    const-string v0, "extra_lightbox_type"

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->y:Ljava/lang/String;

    .line 68
    const-string v0, "extra_lightbox_page_index"

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->z:Ljava/lang/String;

    .line 72
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/LightBoxFragment;->a(Z)V

    .line 73
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 312
    if-nez p2, :cond_0

    .line 322
    :goto_0
    return-object v0

    .line 316
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "string"

    invoke-virtual {v2, v3, v4, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 317
    :catch_0
    move-exception v1

    .line 318
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private a(Landroid/view/LayoutInflater;Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;)Z
    .locals 10

    .prologue
    const v9, 0x7f0a0246

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 211
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    iget-object v0, v0, Lflipboard/objs/LightBoxes;->a:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 213
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lflipboard/gui/hints/LightBoxFragment;->f()Lflipboard/objs/LightBoxes;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    .line 214
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    iget-object v0, v0, Lflipboard/objs/LightBoxes;->a:Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v0, :cond_2

    :cond_1
    move v0, v4

    .line 307
    :goto_0
    return v0

    .line 218
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    move v0, v4

    .line 220
    goto :goto_0

    .line 224
    :cond_2
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    iget-object v0, v0, Lflipboard/objs/LightBoxes;->a:Ljava/util/Map;

    iget-object v1, p2, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$LightBox;

    .line 225
    if-eqz v0, :cond_3

    iget-object v1, v0, Lflipboard/objs/LightBoxes$LightBox;->b:Lflipboard/objs/LightBoxes$Device;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lflipboard/objs/LightBoxes$LightBox;->a:Lflipboard/objs/LightBoxes$Device;

    if-nez v1, :cond_4

    :cond_3
    move v0, v4

    .line 226
    goto :goto_0

    .line 228
    :cond_4
    sget-object v1, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v1, v1, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v1, :cond_5

    .line 229
    iget-object v0, v0, Lflipboard/objs/LightBoxes$LightBox;->b:Lflipboard/objs/LightBoxes$Device;

    iget-object v0, v0, Lflipboard/objs/LightBoxes$Device;->a:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    .line 234
    :goto_1
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    if-nez v0, :cond_6

    move v0, v4

    .line 238
    goto :goto_0

    .line 231
    :cond_5
    iget-object v0, v0, Lflipboard/objs/LightBoxes$LightBox;->a:Lflipboard/objs/LightBoxes$Device;

    iget-object v0, v0, Lflipboard/objs/LightBoxes$Device;->a:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    goto :goto_1

    .line 241
    :cond_6
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->j:I

    .line 242
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->r:Ljava/lang/StringBuffer;

    .line 243
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->x:I

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->j:I

    if-lt v0, v1, :cond_7

    .line 244
    iput v4, p0, Lflipboard/gui/hints/LightBoxFragment;->x:I

    .line 246
    :cond_7
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->x:I

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    .line 247
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->j:I

    if-le v0, v5, :cond_9

    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->j:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_9

    .line 248
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    :goto_2
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    .line 256
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    .line 257
    if-eqz v0, :cond_8

    .line 258
    iget-object v0, v0, Lflipboard/objs/LightBoxes$Page;->b:Ljava/lang/String;

    invoke-direct {p0, v6, v0}, Lflipboard/gui/hints/LightBoxFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 259
    iget-object v1, p0, Lflipboard/gui/hints/LightBoxFragment;->t:Lflipboard/gui/FLTextView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    :cond_8
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    .line 263
    iget-object v1, v0, Lflipboard/objs/LightBoxes$Page;->f:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 264
    const v1, 0x7f0300c5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 265
    const v2, 0x7f0a0245

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 266
    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLButton;

    .line 267
    iget-object v8, p0, Lflipboard/gui/hints/LightBoxFragment;->l:Landroid/view/View$OnClickListener;

    if-eqz v8, :cond_a

    .line 268
    iget-object v8, p0, Lflipboard/gui/hints/LightBoxFragment;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v8}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 278
    :goto_4
    const v3, 0x7f0a0247

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lflipboard/gui/FLTextView;

    .line 280
    iget-object v8, v0, Lflipboard/objs/LightBoxes$Page;->a:Ljava/lang/String;

    invoke-direct {p0, v6, v8}, Lflipboard/gui/hints/LightBoxFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 281
    invoke-virtual {v3, v8}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v3, v0, Lflipboard/objs/LightBoxes$Page;->c:Ljava/lang/String;

    invoke-direct {p0, v6, v3}, Lflipboard/gui/hints/LightBoxFragment;->b(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 285
    if-nez v3, :cond_b

    move v0, v4

    .line 287
    goto/16 :goto_0

    .line 251
    :cond_9
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->n:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 270
    :cond_a
    new-instance v8, Lflipboard/gui/hints/LightBoxFragment$2;

    invoke-direct {v8, p0}, Lflipboard/gui/hints/LightBoxFragment$2;-><init>(Lflipboard/gui/hints/LightBoxFragment;)V

    invoke-virtual {v3, v8}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_4

    .line 290
    :cond_b
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 292
    iget-object v2, v0, Lflipboard/objs/LightBoxes$Page;->e:Ljava/lang/String;

    if-eqz v2, :cond_c

    .line 293
    invoke-virtual {v1, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLButton;

    .line 295
    iget-object v0, v0, Lflipboard/objs/LightBoxes$Page;->e:Ljava/lang/String;

    invoke-direct {p0, v6, v0}, Lflipboard/gui/hints/LightBoxFragment;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 296
    invoke-virtual {v2, v0}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    .line 297
    invoke-virtual {v2, v4}, Lflipboard/gui/FLButton;->setVisibility(I)V

    .line 299
    :cond_c
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 302
    :cond_d
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->r:Ljava/lang/StringBuffer;

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto/16 :goto_3

    .line 304
    :cond_e
    invoke-direct {p0, v4}, Lflipboard/gui/hints/LightBoxFragment;->b(I)V

    .line 305
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    move v0, v5

    .line 307
    goto/16 :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 328
    :try_start_0
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "drawable"

    invoke-virtual {v2, v3, v4, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 353
    :goto_0
    return-object v0

    .line 334
    :catch_0
    move-exception v1

    .line 335
    sget-object v2, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v2, v1}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 340
    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 344
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 347
    :try_start_1
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "drawable"

    invoke-virtual {v2, v3, v4, p1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    goto :goto_0

    .line 348
    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 390
    new-instance v0, Landroid/text/SpannableString;

    iget-object v1, p0, Lflipboard/gui/hints/LightBoxFragment;->r:Ljava/lang/StringBuffer;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 391
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v2, p1, 0x1

    const/16 v3, 0x21

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 392
    iget-object v1, p0, Lflipboard/gui/hints/LightBoxFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 393
    return-void
.end method

.method private f()Lflipboard/objs/LightBoxes;
    .locals 4

    .prologue
    .line 188
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    .line 206
    :goto_0
    return-object v0

    .line 193
    :cond_0
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    .line 196
    :try_start_0
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const-string v1, "lightboxes.json"

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 198
    :try_start_1
    new-instance v0, Lflipboard/json/JSONParser;

    invoke-direct {v0, v1}, Lflipboard/json/JSONParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lflipboard/json/JSONParser;->o()Lflipboard/objs/LightBoxes;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 206
    :goto_1
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->v:Lflipboard/objs/LightBoxes;

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 205
    :catch_0
    move-exception v0

    .line 203
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v2, "Exception loading first launch LightBoxes"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lflipboard/util/Log;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 204
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private g()V
    .locals 5

    .prologue
    const v4, 0x7f040001

    .line 372
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lflipboard/activities/ShareActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 373
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 374
    const-string v2, "create_magazine_object"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 375
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 376
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 377
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v4, v4}, Landroid/support/v4/app/FragmentActivity;->overridePendingTransition(II)V

    .line 378
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->a()V

    .line 379
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 383
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 384
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 385
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 386
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/dialog/FLDialogFragment;->C:Z

    .line 93
    iput-object p1, p0, Lflipboard/gui/hints/LightBoxFragment;->m:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    .line 94
    return-void
.end method

.method final e()V
    .locals 6

    .prologue
    .line 161
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->j:I

    if-ge v0, v1, :cond_0

    .line 162
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    .line 163
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    iget-object v0, v0, Lflipboard/objs/LightBoxes$Page;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 164
    invoke-direct {p0}, Lflipboard/gui/hints/LightBoxFragment;->g()V

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    invoke-direct {p0}, Lflipboard/gui/hints/LightBoxFragment;->h()V

    .line 168
    iget-object v1, p0, Lflipboard/gui/hints/LightBoxFragment;->t:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    iget v4, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    iget-object v0, v0, Lflipboard/objs/LightBoxes$Page;->b:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "string"

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f040019

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 170
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f04001c

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 171
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showNext()V

    .line 172
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-direct {p0, v0}, Lflipboard/gui/hints/LightBoxFragment;->b(I)V

    .line 173
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->j:I

    if-ge v0, v1, :cond_2

    .line 174
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 176
    :cond_2
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 98
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 99
    const v0, 0x7f0e0023

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/LightBoxFragment;->a(I)V

    .line 100
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 105
    if-eqz p3, :cond_0

    .line 106
    const-string v0, "extra_lightbox_type"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->valueOf(Ljava/lang/String;)Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->m:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    .line 107
    const-string v0, "extra_lightbox_page_index"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->x:I

    .line 109
    :cond_0
    const v0, 0x7f0300c4

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 110
    const v0, 0x7f0a0242

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ViewFlipper;

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    .line 111
    const v0, 0x7f0a0243

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->n:Landroid/widget/TextView;

    .line 112
    const v0, 0x7f0a023f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->w:Landroid/view/View;

    .line 113
    const v0, 0x7f0a0240

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->t:Lflipboard/gui/FLTextView;

    .line 114
    const v0, 0x7f0a0241

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLButton;

    iput-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    .line 115
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    new-instance v2, Lflipboard/gui/hints/LightBoxFragment$1;

    invoke-direct {v2, p0}, Lflipboard/gui/hints/LightBoxFragment$1;-><init>(Lflipboard/gui/hints/LightBoxFragment;)V

    invoke-virtual {v0, v2}, Lflipboard/gui/FLButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p0}, Landroid/widget/ViewFlipper;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->m:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-direct {p0, p1, v0}, Lflipboard/gui/hints/LightBoxFragment;->a(Landroid/view/LayoutInflater;Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;)Z

    move-result v0

    .line 125
    if-nez v0, :cond_1

    .line 126
    const/4 v0, 0x0

    .line 127
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->a()V

    .line 130
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 135
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->x:I

    .line 136
    const-string v0, "extra_lightbox_type"

    iget-object v1, p0, Lflipboard/gui/hints/LightBoxFragment;->m:Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;

    invoke-virtual {v1}, Lflipboard/gui/hints/LightBoxFragment$LightBoxTypes;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    const-string v0, "extra_lightbox_page_index"

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->x:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 138
    invoke-super {p0, p1}, Lflipboard/gui/dialog/FLDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 139
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/16 v2, 0x19

    .line 406
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 407
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 419
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 409
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->p:I

    goto :goto_0

    .line 412
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->q:I

    .line 413
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->q:I

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->p:I

    sub-int/2addr v0, v1

    if-le v0, v2, :cond_2

    .line 414
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    if-lez v0, :cond_0

    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    iget-object v0, v0, Lflipboard/objs/LightBoxes$Page;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lflipboard/gui/hints/LightBoxFragment;->g()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lflipboard/gui/hints/LightBoxFragment;->h()V

    iget-object v1, p0, Lflipboard/gui/hints/LightBoxFragment;->t:Lflipboard/gui/FLTextView;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->u:Ljava/util/List;

    iget v4, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/LightBoxes$Page;

    iget-object v0, v0, Lflipboard/objs/LightBoxes$Page;->b:Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "string"

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v0, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f04001a

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f04001b

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->o:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->showPrevious()V

    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->k:I

    invoke-direct {p0, v0}, Lflipboard/gui/hints/LightBoxFragment;->b(I)V

    iget-object v0, p0, Lflipboard/gui/hints/LightBoxFragment;->s:Lflipboard/gui/FLButton;

    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d021c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLButton;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 415
    :cond_2
    iget v0, p0, Lflipboard/gui/hints/LightBoxFragment;->p:I

    iget v1, p0, Lflipboard/gui/hints/LightBoxFragment;->q:I

    sub-int/2addr v0, v1

    if-le v0, v2, :cond_0

    .line 416
    invoke-virtual {p0}, Lflipboard/gui/hints/LightBoxFragment;->e()V

    goto/16 :goto_0

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
