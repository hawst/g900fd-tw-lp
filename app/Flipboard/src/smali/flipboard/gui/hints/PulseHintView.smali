.class public Lflipboard/gui/hints/PulseHintView;
.super Landroid/view/ViewGroup;
.source "PulseHintView.java"


# instance fields
.field public a:Landroid/view/View;

.field public b:Lflipboard/objs/ConfigHints$Hint;

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;

.field public e:Lflipboard/gui/FLStaticTextView;

.field public f:Lflipboard/gui/FLStaticTextView;

.field public final g:F

.field public h:I

.field public i:Lflipboard/service/HintManager;

.field private final j:Lflipboard/activities/FlipboardActivity;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 45
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 38
    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->k:Z

    .line 41
    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->n:Z

    .line 46
    check-cast p1, Lflipboard/activities/FlipboardActivity;

    iput-object p1, p0, Lflipboard/gui/hints/PulseHintView;->j:Lflipboard/activities/FlipboardActivity;

    .line 47
    invoke-virtual {p0}, Lflipboard/gui/hints/PulseHintView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lflipboard/gui/hints/PulseHintView;->g:F

    .line 48
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/PulseHintView;->setWillNotDraw(Z)V

    .line 49
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->l:Z

    .line 251
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 252
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    const v1, 0x7f020115

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 253
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/high16 v1, 0x437f0000    # 255.0f

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    iget v2, v2, Lflipboard/objs/ConfigHints$Hint;->w:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 255
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->j:Lflipboard/activities/FlipboardActivity;

    const v1, 0x7f04000b

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 256
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 258
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v2

    new-instance v0, Lflipboard/gui/hints/PulseHintView$3;

    invoke-direct {v0, p0}, Lflipboard/gui/hints/PulseHintView$3;-><init>(Lflipboard/gui/hints/PulseHintView;)V

    invoke-virtual {v1, v2, v3, v0}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 268
    :goto_0
    return-void

    .line 266
    :cond_0
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->b()V

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 286
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getLeft()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getRight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getTop()I

    move-result v1

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getBottom()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lflipboard/gui/hints/PulseHintView;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->m:Z

    return v0
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 272
    iput-boolean v1, p0, Lflipboard/gui/hints/PulseHintView;->m:Z

    .line 273
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "PulseHintView:dismiss"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0}, Lflipboard/gui/hints/PulseHintView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 275
    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {p0}, Lflipboard/gui/hints/PulseHintView;->clearAnimation()V

    .line 277
    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 280
    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->i:Lflipboard/service/HintManager;

    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v3, v0}, Lflipboard/service/HintManager;->a(Lflipboard/objs/ConfigHints$Hint;Z)V

    .line 282
    :cond_0
    return-void

    .line 280
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lflipboard/gui/hints/PulseHintView;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->n:Z

    return v0
.end method

.method static synthetic c(Lflipboard/gui/hints/PulseHintView;)Lflipboard/objs/ConfigHints$Hint;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lflipboard/gui/hints/PulseHintView;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->j:Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method

.method static synthetic f(Lflipboard/gui/hints/PulseHintView;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->k:Z

    return v0
.end method

.method static synthetic g(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic h(Lflipboard/gui/hints/PulseHintView;)Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->l:Z

    return v0
.end method

.method static synthetic i(Lflipboard/gui/hints/PulseHintView;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->a()V

    return-void
.end method

.method static synthetic j(Lflipboard/gui/hints/PulseHintView;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->b()V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 140
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 141
    const v0, 0x7f0a02bb

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/PulseHintView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    .line 142
    const v0, 0x7f0a02bc

    invoke-virtual {p0, v0}, Lflipboard/gui/hints/PulseHintView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    .line 143
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    const v1, 0x7f0a02bd

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/hints/PulseHintView;->e:Lflipboard/gui/FLStaticTextView;

    .line 144
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    const v1, 0x7f0a02be

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/hints/PulseHintView;->f:Lflipboard/gui/FLStaticTextView;

    .line 145
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 176
    sub-int v2, p4, p2

    .line 177
    sub-int v3, p5, p3

    .line 179
    iget-object v4, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-static {v4}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v4

    .line 180
    iget-object v5, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    iget-object v6, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingLeft()I

    move-result v6

    sub-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    .line 181
    iget-object v6, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    iget-object v7, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    sub-int/2addr v6, v7

    iget-object v7, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getPaddingBottom()I

    move-result v7

    sub-int/2addr v6, v7

    .line 182
    aget v7, v4, v1

    iget-object v8, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getPaddingLeft()I

    move-result v8

    add-int/2addr v7, v8

    div-int/lit8 v5, v5, 0x2

    add-int/2addr v5, v7

    iget-object v7, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v5, v7

    .line 183
    aget v4, v4, v0

    iget-object v7, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    add-int/2addr v4, v7

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v4, v6

    iget-object v6, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    .line 184
    iget-object v6, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    iget-object v7, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v5

    iget-object v8, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    invoke-virtual {v6, v5, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 187
    div-int/lit8 v6, v2, 0x2

    if-ge v5, v6, :cond_0

    :goto_0
    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->k:Z

    .line 188
    iget-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->k:Z

    if-eqz v0, :cond_1

    .line 193
    :goto_1
    div-int/lit8 v0, v3, 0x2

    if-ge v4, v0, :cond_2

    .line 194
    iget v0, p0, Lflipboard/gui/hints/PulseHintView;->h:I

    add-int/2addr v0, v4

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 198
    :goto_2
    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/view/View;->layout(IIII)V

    .line 199
    return-void

    :cond_0
    move v0, v1

    .line 187
    goto :goto_0

    .line 188
    :cond_1
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v1, v2, v0

    goto :goto_1

    .line 196
    :cond_2
    iget v0, p0, Lflipboard/gui/hints/PulseHintView;->h:I

    sub-int v0, v4, v0

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v0, v2

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 150
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 151
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 153
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/hints/PulseHintView;->setMeasuredDimension(II)V

    .line 156
    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {p0, v3, p1, p2}, Lflipboard/gui/hints/PulseHintView;->measureChild(Landroid/view/View;II)V

    .line 159
    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-static {v3}, Lflipboard/util/AndroidUtil;->c(Landroid/view/View;)[I

    move-result-object v3

    .line 160
    iget-object v4, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 161
    aget v3, v3, v0

    iget-object v5, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    add-int/2addr v3, v5

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iget-object v4, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    .line 162
    div-int/lit8 v4, v1, 0x2

    if-ge v3, v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->k:Z

    .line 163
    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    iget-boolean v0, p0, Lflipboard/gui/hints/PulseHintView;->k:Z

    if-eqz v0, :cond_1

    const v0, 0x7f020111

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 165
    invoke-virtual {p0, v1, v2}, Lflipboard/gui/hints/PulseHintView;->setMeasuredDimension(II)V

    .line 166
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-virtual {p0, v0, p1, p2}, Lflipboard/gui/hints/PulseHintView;->measureChild(Landroid/view/View;II)V

    .line 169
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 170
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {p0, v1, v0, p2}, Lflipboard/gui/hints/PulseHintView;->measureChild(Landroid/view/View;II)V

    .line 171
    return-void

    .line 163
    :cond_1
    const v0, 0x7f020113

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 206
    invoke-virtual {p0, v1}, Lflipboard/gui/hints/PulseHintView;->requestDisallowInterceptTouchEvent(Z)V

    .line 207
    iget-boolean v2, p0, Lflipboard/gui/hints/PulseHintView;->l:Z

    if-eqz v2, :cond_0

    .line 244
    :goto_0
    return v0

    .line 210
    :cond_0
    iget-boolean v2, p0, Lflipboard/gui/hints/PulseHintView;->n:Z

    if-eqz v2, :cond_1

    .line 212
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->b()V

    goto :goto_0

    .line 216
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_8

    .line 218
    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-static {v2, p1}, Lflipboard/gui/hints/PulseHintView;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v2

    .line 219
    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-static {v3, p1}, Lflipboard/gui/hints/PulseHintView;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v3

    .line 220
    if-nez v2, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    move v0, v1

    .line 221
    :cond_3
    iget-object v3, p0, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    iget-boolean v3, v3, Lflipboard/objs/ConfigHints$Hint;->m:Z

    if-eqz v3, :cond_4

    if-eqz v0, :cond_5

    .line 222
    :cond_4
    iput-boolean v1, p0, Lflipboard/gui/hints/PulseHintView;->l:Z

    .line 223
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->a()V

    .line 225
    if-eqz v2, :cond_7

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->E:Lflipboard/objs/UsageEventV2$EventAction;

    .line 226
    :goto_1
    new-instance v3, Lflipboard/objs/UsageEventV2;

    sget-object v4, Lflipboard/objs/UsageEventV2$EventCategory;->h:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v3, v0, v4}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    .line 227
    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v4, p0, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    iget-object v4, v4, Lflipboard/objs/ConfigHints$Hint;->k:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    .line 228
    invoke-virtual {v3}, Lflipboard/objs/UsageEventV2;->c()V

    .line 231
    :cond_5
    if-eqz v2, :cond_6

    .line 232
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    :cond_6
    :goto_2
    move v0, v1

    .line 244
    goto :goto_0

    .line 225
    :cond_7
    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->D:Lflipboard/objs/UsageEventV2$EventAction;

    goto :goto_1

    .line 235
    :cond_8
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_9

    .line 236
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->b()V

    goto :goto_2

    .line 237
    :cond_9
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->b:Lflipboard/objs/ConfigHints$Hint;

    iget-boolean v0, v0, Lflipboard/objs/ConfigHints$Hint;->m:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->c:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/gui/hints/PulseHintView;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 238
    invoke-direct {p0}, Lflipboard/gui/hints/PulseHintView;->a()V

    .line 241
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView;->d:Landroid/view/View;

    invoke-static {v0, p1}, Lflipboard/gui/hints/PulseHintView;->a(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
