.class public Lflipboard/gui/hints/PulseHintView$1;
.super Ljava/lang/Object;
.source "PulseHintView.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lflipboard/gui/hints/PulseHintView;


# direct methods
.method public constructor <init>(Lflipboard/gui/hints/PulseHintView;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const v0, 0x7f040019

    const/4 v9, 0x1

    const/high16 v8, 0x437f0000    # 255.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 79
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->a(Lflipboard/gui/hints/PulseHintView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 80
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->b(Lflipboard/gui/hints/PulseHintView;)Z

    .line 81
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->c(Lflipboard/gui/hints/PulseHintView;)Lflipboard/objs/ConfigHints$Hint;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/objs/ConfigHints$Hint;->A:Z

    if-nez v1, :cond_0

    .line 82
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v7, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 83
    const-wide/16 v2, 0x226

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 84
    invoke-virtual {v1, v9}, Landroid/view/animation/AlphaAnimation;->setFillBefore(Z)V

    .line 87
    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v2}, Lflipboard/gui/hints/PulseHintView;->d(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 88
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Landroid/view/animation/AlphaAnimation;->getDuration()J

    move-result-wide v4

    new-instance v1, Lflipboard/gui/hints/PulseHintView$1$1;

    invoke-direct {v1, p0}, Lflipboard/gui/hints/PulseHintView$1$1;-><init>(Lflipboard/gui/hints/PulseHintView$1;)V

    invoke-virtual {v2, v4, v5, v1}, Lflipboard/service/FlipboardManager;->a(JLjava/lang/Runnable;)V

    .line 94
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->d(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 95
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->d(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v2}, Lflipboard/gui/hints/PulseHintView;->c(Lflipboard/gui/hints/PulseHintView;)Lflipboard/objs/ConfigHints$Hint;

    move-result-object v2

    iget v2, v2, Lflipboard/objs/ConfigHints$Hint;->w:F

    mul-float/2addr v2, v8

    float-to-int v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 99
    :cond_0
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->e(Lflipboard/gui/hints/PulseHintView;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v2}, Lflipboard/gui/hints/PulseHintView;->f(Lflipboard/gui/hints/PulseHintView;)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_0
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 105
    :goto_1
    const-wide/16 v2, 0x226

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 106
    invoke-virtual {v0, v9}, Landroid/view/animation/Animation;->setFillBefore(Z)V

    .line 107
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->g(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 108
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v0}, Lflipboard/gui/hints/PulseHintView;->g(Lflipboard/gui/hints/PulseHintView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v0}, Lflipboard/gui/hints/PulseHintView;->c(Lflipboard/gui/hints/PulseHintView;)Lflipboard/objs/ConfigHints$Hint;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/objs/ConfigHints$Hint;->m:Z

    if-eqz v0, :cond_5

    .line 111
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-virtual {v1}, Lflipboard/gui/hints/PulseHintView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080098

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/hints/PulseHintView;->setBackgroundColor(I)V

    .line 117
    :cond_1
    :goto_2
    return-void

    .line 100
    :cond_2
    const v0, 0x7f04001a

    goto :goto_0

    .line 102
    :cond_3
    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->e(Lflipboard/gui/hints/PulseHintView;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v2}, Lflipboard/gui/hints/PulseHintView;->f(Lflipboard/gui/hints/PulseHintView;)Z

    move-result v2

    if-eqz v2, :cond_4

    const v0, 0x7f04001a

    :cond_4
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_1

    .line 112
    :cond_5
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v0}, Lflipboard/gui/hints/PulseHintView;->c(Lflipboard/gui/hints/PulseHintView;)Lflipboard/objs/ConfigHints$Hint;

    move-result-object v0

    iget v0, v0, Lflipboard/objs/ConfigHints$Hint;->z:F

    cmpl-float v0, v0, v7

    if-lez v0, :cond_1

    .line 114
    iget-object v0, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    iget-object v1, p0, Lflipboard/gui/hints/PulseHintView$1;->a:Lflipboard/gui/hints/PulseHintView;

    invoke-static {v1}, Lflipboard/gui/hints/PulseHintView;->c(Lflipboard/gui/hints/PulseHintView;)Lflipboard/objs/ConfigHints$Hint;

    move-result-object v1

    iget v1, v1, Lflipboard/objs/ConfigHints$Hint;->z:F

    mul-float/2addr v1, v8

    float-to-int v1, v1

    invoke-static {v1, v6, v6, v6}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/hints/PulseHintView;->setBackgroundColor(I)V

    goto :goto_2
.end method
