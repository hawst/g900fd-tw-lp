.class Lflipboard/gui/CommentaryList$11;
.super Ljava/lang/Object;
.source "CommentaryList.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Landroid/view/ViewGroup;

.field final synthetic c:Landroid/view/View;

.field final synthetic d:Landroid/view/ViewGroup;

.field final synthetic e:Lflipboard/gui/CommentaryList;


# direct methods
.method constructor <init>(Lflipboard/gui/CommentaryList;Ljava/util/List;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 809
    iput-object p1, p0, Lflipboard/gui/CommentaryList$11;->e:Lflipboard/gui/CommentaryList;

    iput-object p2, p0, Lflipboard/gui/CommentaryList$11;->a:Ljava/util/List;

    iput-object p3, p0, Lflipboard/gui/CommentaryList$11;->b:Landroid/view/ViewGroup;

    iput-object p4, p0, Lflipboard/gui/CommentaryList$11;->c:Landroid/view/View;

    iput-object p5, p0, Lflipboard/gui/CommentaryList$11;->d:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 812
    iget-object v0, p0, Lflipboard/gui/CommentaryList$11;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 814
    iget-object v2, p0, Lflipboard/gui/CommentaryList$11;->e:Lflipboard/gui/CommentaryList;

    invoke-static {v2, v0}, Lflipboard/gui/CommentaryList;->a(Lflipboard/gui/CommentaryList;Lflipboard/objs/FeedSectionLink;)Landroid/view/View;

    move-result-object v0

    .line 815
    iget-object v2, p0, Lflipboard/gui/CommentaryList$11;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 818
    :cond_0
    iget-object v0, p0, Lflipboard/gui/CommentaryList$11;->c:Landroid/view/View;

    const v1, 0x7f0a0326

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iget-object v1, p0, Lflipboard/gui/CommentaryList$11;->e:Lflipboard/gui/CommentaryList;

    invoke-virtual {v1}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0306

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 820
    iget-object v0, p0, Lflipboard/gui/CommentaryList$11;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lflipboard/gui/CommentaryList$11;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 821
    iget-object v0, p0, Lflipboard/gui/CommentaryList$11;->c:Landroid/view/View;

    const v1, 0x7f0a0328

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 822
    iget-object v0, p0, Lflipboard/gui/CommentaryList$11;->c:Landroid/view/View;

    const v1, 0x7f0a0327

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 823
    iget-object v0, p0, Lflipboard/gui/CommentaryList$11;->c:Landroid/view/View;

    const v1, 0x7f0a0329

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v3}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    .line 824
    return-void
.end method
