.class Lflipboard/gui/ContentDrawerView$2;
.super Ljava/lang/Object;
.source "ContentDrawerView.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/Section;",
        "Lflipboard/service/Section$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/ContentDrawerView;


# direct methods
.method constructor <init>(Lflipboard/gui/ContentDrawerView;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView$2;->a:Lflipboard/gui/ContentDrawerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 191
    check-cast p1, Lflipboard/service/Section;

    check-cast p2, Lflipboard/service/Section$Message;

    invoke-virtual {p1}, Lflipboard/service/Section;->m()Ljava/util/List;

    move-result-object v1

    sget-object v0, Lflipboard/service/Section$Message;->d:Lflipboard/service/Section$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->e:Lflipboard/service/Section$Message;

    if-eq p2, v0, :cond_0

    sget-object v0, Lflipboard/service/Section$Message;->c:Lflipboard/service/Section$Message;

    if-ne p2, v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, p0}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$2;->a:Lflipboard/gui/ContentDrawerView;

    invoke-static {v0}, Lflipboard/gui/ContentDrawerView;->a(Lflipboard/gui/ContentDrawerView;)Lflipboard/util/Observer;

    :cond_1
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$2;->a:Lflipboard/gui/ContentDrawerView;

    invoke-static {v0, v1}, Lflipboard/gui/ContentDrawerView;->a(Lflipboard/gui/ContentDrawerView;Ljava/util/List;)V

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {p1, v1}, Lflipboard/service/FlipboardManager;->a(Lflipboard/service/Section;Ljava/util/List;)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
