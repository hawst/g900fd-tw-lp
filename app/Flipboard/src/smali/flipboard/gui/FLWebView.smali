.class public Lflipboard/gui/FLWebView;
.super Landroid/webkit/WebView;
.source "FLWebView.java"


# static fields
.field public static final a:Lflipboard/util/Log;

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final d:Lflipboard/service/FlipboardManager;

.field private e:Lflipboard/gui/FLWebView$TouchListener;

.field private f:F

.field private g:F

.field private h:Z

.field private i:Lflipboard/gui/FLWebView$PostDrawListener;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 23
    const-string v0, "FLWebView"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLWebView;->a:Lflipboard/util/Log;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "javascript"

    aput-object v1, v0, v3

    const-string v1, "flipboard"

    aput-object v1, v0, v4

    const/4 v1, 0x2

    const-string v2, "flipmag"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "http"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "https"

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLWebView;->b:Ljava/util/List;

    .line 27
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "market"

    aput-object v1, v0, v3

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLWebView;->c:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lflipboard/gui/FLWebView;-><init>(Landroid/content/Context;Z)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    .line 51
    invoke-direct {p0}, Lflipboard/gui/FLWebView;->a()V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 28
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    .line 44
    iput-boolean p2, p0, Lflipboard/gui/FLWebView;->h:Z

    .line 45
    invoke-direct {p0}, Lflipboard/gui/FLWebView;->a()V

    .line 46
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 321
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->AdvertiseUserAgent:Z

    if-eqz v0, :cond_0

    .line 322
    const-string v0, "%s Flipboard/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v3}, Lflipboard/app/FlipboardApplication;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 326
    :cond_0
    const-string v0, "Android"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Android"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 330
    :cond_1
    return-object p0
.end method

.method private a()V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 56
    new-instance v0, Lflipboard/gui/FLWebChromeClient;

    invoke-direct {v0}, Lflipboard/gui/FLWebChromeClient;-><init>()V

    invoke-virtual {p0, v0}, Lflipboard/gui/FLWebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 57
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v1

    .line 58
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 59
    invoke-virtual {v1, v2}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 62
    invoke-virtual {v1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/gui/FLWebView;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 64
    sget-object v0, Lflipboard/gui/FLWebView;->a:Lflipboard/util/Log;

    new-array v0, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 71
    :try_start_0
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 72
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v2

    .line 73
    invoke-virtual {v2}, Landroid/webkit/CookieManager;->removeExpiredCookie()V

    .line 76
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v0, v0, Lflipboard/service/User;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/service/Account;

    .line 77
    iget-object v0, v0, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v0, v0, Lflipboard/objs/UserService;->o:Ljava/util/List;

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserService$Cookie;

    .line 80
    iget-object v5, v0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 81
    const-string v5, "%s=%s; domain=%s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Lflipboard/objs/UserService$Cookie;->a:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, v0, Lflipboard/objs/UserService$Cookie;->c:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-object v8, v0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 82
    iget-object v0, v0, Lflipboard/objs/UserService$Cookie;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v5}, Landroid/webkit/CookieManager;->setCookie(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 89
    :catch_0
    move-exception v0

    .line 90
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 91
    iget-boolean v0, p0, Lflipboard/gui/FLWebView;->h:Z

    if-eqz v0, :cond_2

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0349

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 94
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 116
    :goto_2
    return-void

    .line 85
    :cond_3
    :try_start_1
    invoke-static {}, Landroid/webkit/CookieSyncManager;->getInstance()Landroid/webkit/CookieSyncManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieSyncManager;->sync()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 98
    :cond_4
    invoke-virtual {p0, v1}, Lflipboard/gui/FLWebView;->setTextScale(Landroid/webkit/WebSettings;)V

    .line 103
    new-instance v0, Lflipboard/gui/FLWebView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/FLWebView$1;-><init>(Lflipboard/gui/FLWebView;)V

    invoke-virtual {p0, v0}, Lflipboard/gui/FLWebView;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    goto :goto_2
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 301
    invoke-super {p0, p1}, Landroid/webkit/WebView;->draw(Landroid/graphics/Canvas;)V

    .line 302
    iget-object v0, p0, Lflipboard/gui/FLWebView;->i:Lflipboard/gui/FLWebView$PostDrawListener;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lflipboard/gui/FLWebView;->i:Lflipboard/gui/FLWebView$PostDrawListener;

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getScrollX()I

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getScrollY()I

    .line 305
    :cond_0
    return-void
.end method

.method public loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 190
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lflipboard/gui/FLWebView;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v7, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    new-instance v0, Lflipboard/gui/FLWebView$3;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lflipboard/gui/FLWebView$3;-><init>(Lflipboard/gui/FLWebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->N:Lflipboard/io/DownloadManager;

    invoke-static {p1}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-super/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    iget-boolean v0, p0, Lflipboard/gui/FLWebView;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0349

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public loadUrl(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 127
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v0}, Lflipboard/io/NetworkManager;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "javascript:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 128
    iget-boolean v0, p0, Lflipboard/gui/FLWebView;->h:Z

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 186
    :cond_0
    :goto_0
    return-void

    .line 134
    :cond_1
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 137
    sget-object v1, Lflipboard/gui/FLWebView;->b:Ljava/util/List;

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 139
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 140
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 141
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 146
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 148
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLWebView$2;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/FLWebView$2;-><init>(Lflipboard/gui/FLWebView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 159
    :cond_3
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->N:Lflipboard/io/DownloadManager;

    invoke-static {p1}, Lflipboard/io/DownloadManager;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 160
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 163
    const-string v2, "X-Flipboard-App"

    const-string v3, "%s-%s-%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-virtual {v5}, Lflipboard/app/FlipboardApplication;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    sget-object v5, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    const/4 v5, 0x2

    sget-object v6, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    invoke-static {}, Lflipboard/app/FlipboardApplication;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 165
    const-string v2, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 170
    :try_start_0
    iget-object v2, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    iget-boolean v2, v2, Lflipboard/model/ConfigSetting;->AdvertiseReferer:Z

    if-eqz v2, :cond_4

    .line 171
    const-string v2, "Referer"

    iget-object v3, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    invoke-virtual {v3}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v3

    invoke-virtual {v3}, Lflipboard/model/ConfigSetting;->getWebViewRefererString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Lflipboard/util/HttpUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    invoke-super {p0, v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 179
    :catch_0
    move-exception v0

    .line 181
    sget-object v1, Lflipboard/gui/FLWebView;->a:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 182
    iget-boolean v0, p0, Lflipboard/gui/FLWebView;->h:Z

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0349

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 174
    :cond_4
    :try_start_1
    invoke-super {p0, v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 264
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-nez v0, :cond_0

    .line 265
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 286
    :cond_0
    :goto_0
    :try_start_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 290
    :goto_1
    return v0

    .line 267
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lflipboard/gui/FLWebView;->f:F

    .line 268
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lflipboard/gui/FLWebView;->g:F

    goto :goto_0

    .line 271
    :pswitch_1
    iput v1, p0, Lflipboard/gui/FLWebView;->f:F

    .line 272
    iput v1, p0, Lflipboard/gui/FLWebView;->g:F

    goto :goto_0

    .line 278
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/FLWebView;->e:Lflipboard/gui/FLWebView$TouchListener;

    if-eqz v0, :cond_0

    iget v0, p0, Lflipboard/gui/FLWebView;->f:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 279
    iget-object v0, p0, Lflipboard/gui/FLWebView;->e:Lflipboard/gui/FLWebView$TouchListener;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, Lflipboard/gui/FLWebView;->f:F

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lflipboard/gui/FLWebView;->g:F

    sub-float/2addr v2, v3

    invoke-interface {v0, p0, v1, v2}, Lflipboard/gui/FLWebView$TouchListener;->a(Lflipboard/gui/FLWebView;FF)V

    goto :goto_0

    .line 287
    :catch_0
    move-exception v0

    .line 289
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V

    .line 290
    const/4 v0, 0x1

    goto :goto_1

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onWindowVisibilityChanged(I)V
    .locals 1

    .prologue
    .line 232
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onWindowVisibilityChanged(I)V

    .line 233
    const/16 v0, 0x8

    if-ne p1, v0, :cond_1

    .line 234
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->onPause()V

    .line 235
    sget-object v0, Lflipboard/gui/FLWebView;->a:Lflipboard/util/Log;

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    if-nez p1, :cond_0

    .line 237
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->onResume()V

    .line 238
    sget-object v0, Lflipboard/gui/FLWebView;->a:Lflipboard/util/Log;

    goto :goto_0
.end method

.method public setPostDrawListener(Lflipboard/gui/FLWebView$PostDrawListener;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lflipboard/gui/FLWebView;->i:Lflipboard/gui/FLWebView$PostDrawListener;

    .line 297
    return-void
.end method

.method protected setTextScale(Landroid/webkit/WebSettings;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->F:Landroid/content/SharedPreferences;

    .line 121
    invoke-virtual {p0}, Lflipboard/gui/FLWebView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0007

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 122
    const-string v2, "font_size"

    const/16 v3, 0x64

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    mul-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x64

    invoke-virtual {p1, v0}, Landroid/webkit/WebSettings;->setTextZoom(I)V

    .line 123
    return-void
.end method

.method public setTouchListener(Lflipboard/gui/FLWebView$TouchListener;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lflipboard/gui/FLWebView;->e:Lflipboard/gui/FLWebView$TouchListener;

    .line 259
    return-void
.end method

.method public stopLoading()V
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lflipboard/gui/FLWebView;->d:Lflipboard/service/FlipboardManager;

    const-string v0, "FLWebView:stopLoading"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 245
    invoke-super {p0}, Landroid/webkit/WebView;->stopLoading()V

    .line 246
    return-void
.end method
