.class public Lflipboard/gui/ContentDrawerView;
.super Lflipboard/gui/FLRelativeLayout;
.source "ContentDrawerView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# static fields
.field public static a:Lflipboard/util/Log;


# instance fields
.field public b:Lflipboard/gui/EditableListView;

.field public c:Lflipboard/gui/ContentDrawerListItemAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field d:Landroid/widget/TextView;

.field e:Lflipboard/gui/FLBusyView;

.field public f:Z

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public volatile j:Ljava/lang/String;

.field public k:Landroid/widget/Button;

.field public l:Z

.field public m:Z

.field private o:Lflipboard/gui/FLTextIntf;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Lflipboard/service/Section;

.field private s:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/Section;",
            "Lflipboard/service/Section$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private t:Lflipboard/util/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/Observer",
            "<",
            "Lflipboard/service/User;",
            "Lflipboard/service/User$Message;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "contentdrawer"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/ContentDrawerView;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/ContentDrawerView;->f:Z

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/ContentDrawerView;->f:Z

    .line 85
    return-void
.end method

.method static synthetic a(Lflipboard/gui/ContentDrawerView;)Lflipboard/util/Observer;
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->s:Lflipboard/util/Observer;

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Lflipboard/service/Section;Lflipboard/gui/ContentDrawerListItemAdapter;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Lflipboard/service/Section;",
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 525
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 526
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 530
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v0

    iget-boolean v0, v0, Lflipboard/model/ConfigSetting;->DisablePeerToPeerSharing:Z

    if-nez v0, :cond_0

    .line 532
    new-instance v0, Lflipboard/objs/ContentDrawerListItemHeader;

    const v1, 0x7f0d02f0

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 534
    new-instance v0, Lflipboard/objs/ContentDrawerListItemHeader;

    const v1, 0x7f0d001b

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lflipboard/objs/ContentDrawerListItemHeader;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 539
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 540
    if-eqz p3, :cond_3

    .line 541
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    .line 543
    iget-object v4, p1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v4}, Lflipboard/service/User;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 544
    iget-object v4, v0, Lflipboard/objs/FeedItem;->ce:Ljava/lang/String;

    const-string v8, "sharedwith"

    invoke-virtual {v4, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 545
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 547
    :cond_2
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 554
    :cond_3
    new-instance v8, Lflipboard/objs/ConfigSection;

    invoke-direct {v8}, Lflipboard/objs/ConfigSection;-><init>()V

    .line 555
    const-string v0, "content_guide_shared_with_you"

    iput-object v0, v8, Lflipboard/objs/ConfigSection;->bR:Ljava/lang/String;

    .line 556
    const-string v1, "auth/flipboard/curator/magazine/sharedwithyou"

    .line 557
    iput-object v1, v8, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    .line 558
    const v0, 0x7f0d0344

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Lflipboard/objs/ContentDrawerListItemBase;->a(Ljava/lang/String;)V

    .line 561
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 562
    if-nez v0, :cond_4

    .line 564
    new-instance v0, Lflipboard/service/Section;

    const-string v2, ""

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 565
    iput-boolean v5, v0, Lflipboard/service/Section;->k:Z

    .line 566
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v0}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    .line 569
    :cond_4
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 572
    invoke-virtual {p1}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-nez v0, :cond_5

    .line 573
    new-instance v0, Lflipboard/gui/ContentDrawerView$8;

    invoke-direct {v0, p1}, Lflipboard/gui/ContentDrawerView$8;-><init>(Lflipboard/service/Section;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 586
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 587
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v1}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v1

    iget-boolean v1, v1, Lflipboard/model/ConfigSetting;->DisablePeerToPeerSharing:Z

    if-nez v1, :cond_6

    .line 588
    invoke-interface {v0, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 590
    :cond_6
    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 591
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/ContentDrawerView$9;

    invoke-direct {v2, p2, v0}, Lflipboard/gui/ContentDrawerView$9;-><init>(Lflipboard/gui/ContentDrawerListItemAdapter;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 600
    return-void
.end method

.method static synthetic a(Lflipboard/gui/ContentDrawerView;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lflipboard/gui/ContentDrawerView;->setContentDrawerNotificationsItems(Ljava/util/List;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/ContentDrawerView;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    return-object v0
.end method

.method private setContentDrawerNotificationsItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/FeedItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 512
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    if-nez v0, :cond_1

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 516
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-static {v0, v1, v2, p1}, Lflipboard/gui/ContentDrawerView;->a(Landroid/content/res/Resources;Lflipboard/service/Section;Lflipboard/gui/ContentDrawerListItemAdapter;Ljava/util/List;)V

    .line 517
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    invoke-virtual {v0}, Lflipboard/service/Section;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    :cond_2
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->c()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 282
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    const v1, 0x7f0d00cb

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 283
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/ContentDrawerView;->l:Z

    .line 284
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 178
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView;->g:Ljava/lang/String;

    .line 179
    iput-object p2, p0, Lflipboard/gui/ContentDrawerView;->h:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->o:Lflipboard/gui/FLTextIntf;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->o:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, p1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 184
    :cond_0
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    if-nez v0, :cond_1

    .line 188
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 189
    invoke-virtual {v0}, Lflipboard/service/User;->d()Lflipboard/service/Section;

    move-result-object v1

    iput-object v1, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    .line 190
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    if-eqz v1, :cond_1

    .line 191
    new-instance v1, Lflipboard/gui/ContentDrawerView$2;

    invoke-direct {v1, p0}, Lflipboard/gui/ContentDrawerView$2;-><init>(Lflipboard/gui/ContentDrawerView;)V

    iput-object v1, p0, Lflipboard/gui/ContentDrawerView;->s:Lflipboard/util/Observer;

    .line 206
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    iget-object v2, p0, Lflipboard/gui/ContentDrawerView;->s:Lflipboard/util/Observer;

    invoke-virtual {v1, v2}, Lflipboard/service/Section;->b(Lflipboard/util/Observer;)V

    .line 207
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lflipboard/service/Section;->d(Z)Z

    .line 209
    new-instance v1, Lflipboard/gui/ContentDrawerView$3;

    invoke-direct {v1, p0}, Lflipboard/gui/ContentDrawerView$3;-><init>(Lflipboard/gui/ContentDrawerView;)V

    iput-object v1, p0, Lflipboard/gui/ContentDrawerView;->t:Lflipboard/util/Observer;

    .line 222
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->t:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 227
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 228
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v2, Lflipboard/gui/ContentDrawerView$4;

    invoke-direct {v2, p0}, Lflipboard/gui/ContentDrawerView$4;-><init>(Lflipboard/gui/ContentDrawerView;)V

    new-instance v3, Lflipboard/service/Flap$SectionListRequest;

    invoke-direct {v3, v0, v1}, Lflipboard/service/Flap$SectionListRequest;-><init>(Lflipboard/service/Flap;Lflipboard/service/User;)V

    invoke-virtual {v3, v2}, Lflipboard/service/Flap$SectionListRequest;->a(Lflipboard/service/Flap$SectionListObserver;)Lflipboard/service/Flap$SectionListRequest;

    .line 272
    :cond_2
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 288
    iput-boolean v0, p0, Lflipboard/gui/ContentDrawerView;->m:Z

    .line 289
    iget-object v2, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 290
    iget-object v2, p0, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    const-string v3, "googlereader"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->u()Lflipboard/model/ConfigSetting;

    move-result-object v2

    iget-boolean v2, v2, Lflipboard/model/ConfigSetting;->GoogleReaderDisabled:Z

    if-eqz v2, :cond_0

    .line 291
    :goto_0
    if-eqz v0, :cond_1

    .line 292
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d026a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 296
    :goto_1
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    new-instance v2, Lflipboard/gui/ContentDrawerView$5;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/ContentDrawerView$5;-><init>(Lflipboard/gui/ContentDrawerView;Z)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 361
    return-void

    :cond_0
    move v0, v1

    .line 290
    goto :goto_0

    .line 294
    :cond_1
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0d02f4

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final c()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 463
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0223

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->setEmptyMessage$505cbf4b(Ljava/lang/String;)V

    .line 464
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 501
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->p:Landroid/view/View;

    const-string v2, "CGSearch"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 502
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->q:Landroid/view/View;

    const-string v2, "nanoCGCompose"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 505
    iget-boolean v0, p0, Lflipboard/gui/ContentDrawerView;->l:Z

    if-eqz v0, :cond_0

    .line 506
    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    const-string v2, "CGEditFlipboard"

    invoke-virtual {v0, v1, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    .line 508
    :cond_0
    return-void
.end method

.method public getContentDrawerListItemAdapter()Lflipboard/gui/ContentDrawerListItemAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lflipboard/gui/ContentDrawerListItemAdapter",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 496
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    return-object v0
.end method

.method public getMenuList()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 366
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onDetachedFromWindow()V

    .line 367
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->s:Lflipboard/util/Observer;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->s:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->c(Lflipboard/util/Observer;)V

    .line 369
    iput-object v2, p0, Lflipboard/gui/ContentDrawerView;->r:Lflipboard/service/Section;

    .line 370
    iput-object v2, p0, Lflipboard/gui/ContentDrawerView;->s:Lflipboard/util/Observer;

    .line 372
    :cond_0
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->t:Lflipboard/util/Observer;

    if-eqz v0, :cond_1

    .line 373
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->t:Lflipboard/util/Observer;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 374
    iput-object v2, p0, Lflipboard/gui/ContentDrawerView;->t:Lflipboard/util/Observer;

    .line 376
    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 90
    invoke-super {p0}, Lflipboard/gui/FLRelativeLayout;->onFinishInflate()V

    .line 95
    const v0, 0x7f0a0109

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/EditableListView;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    .line 96
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    if-nez v0, :cond_0

    .line 97
    const v0, 0x7f0a0103

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/EditableListView;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    .line 100
    :cond_0
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    const v1, 0x7f0a00ba

    invoke-virtual {p0, v1}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setEmptyView(Landroid/view/View;)V

    .line 101
    const v0, 0x7f0a00bc

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->d:Landroid/widget/TextView;

    .line 102
    const v0, 0x7f0a00bb

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLBusyView;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->e:Lflipboard/gui/FLBusyView;

    .line 103
    new-instance v1, Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-direct {v1, v0}, Lflipboard/gui/ContentDrawerListItemAdapter;-><init>(Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, p0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    .line 104
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    const v0, 0x7f0a00fe

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->o:Lflipboard/gui/FLTextIntf;

    .line 109
    const v0, 0x7f0a00ff

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->k:Landroid/widget/Button;

    .line 111
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    new-instance v1, Lflipboard/gui/ContentDrawerView$1;

    invoke-direct {v1, p0}, Lflipboard/gui/ContentDrawerView$1;-><init>(Lflipboard/gui/ContentDrawerView;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/EditableListView;->setDragListener(Lflipboard/gui/EditableListView$DragListener;)V

    .line 153
    const v0, 0x7f0a0107

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->p:Landroid/view/View;

    .line 154
    const v0, 0x7f0a0106

    invoke-virtual {p0, v0}, Lflipboard/gui/ContentDrawerView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/ContentDrawerView;->q:Landroid/view/View;

    .line 155
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    .line 383
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 384
    if-lez p4, :cond_1

    add-int v0, p2, p3

    if-lt v0, p4, :cond_1

    const/4 v0, 0x1

    .line 385
    :goto_0
    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->j:Ljava/lang/String;

    .line 389
    const/4 v1, 0x0

    iput-object v1, p0, Lflipboard/gui/ContentDrawerView;->j:Ljava/lang/String;

    .line 392
    iget-object v1, p0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    .line 393
    invoke-virtual {v1}, Lflipboard/gui/ContentDrawerListItemAdapter;->b()V

    .line 396
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v2}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v2

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v5, p0, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v4

    new-instance v5, Lflipboard/gui/ContentDrawerView$6;

    invoke-direct {v5, p0, v1, v0}, Lflipboard/gui/ContentDrawerView$6;-><init>(Lflipboard/gui/ContentDrawerView;Lflipboard/gui/ContentDrawerListItemAdapter;Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4, v0, v5}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/objs/ConfigService;Ljava/lang/String;Lflipboard/service/Flap$SectionListObserver;)V

    .line 437
    :cond_0
    return-void

    .line 384
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 442
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 485
    invoke-super {p0, p1}, Lflipboard/gui/FLRelativeLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 486
    const/4 v0, 0x1

    return v0
.end method

.method public final setEmptyMessage$505cbf4b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 468
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/ContentDrawerView$7;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/ContentDrawerView$7;-><init>(Lflipboard/gui/ContentDrawerView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 481
    return-void
.end method

.method public setHasContent(Z)V
    .locals 0

    .prologue
    .line 173
    iput-boolean p1, p0, Lflipboard/gui/ContentDrawerView;->f:Z

    .line 174
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 276
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->c:Lflipboard/gui/ContentDrawerListItemAdapter;

    invoke-virtual {v0, p1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Ljava/util/List;)V

    .line 277
    return-void
.end method

.method public setPageKey(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 164
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView;->j:Ljava/lang/String;

    .line 167
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 168
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView;->b:Lflipboard/gui/EditableListView;

    invoke-virtual {v0, p0}, Lflipboard/gui/EditableListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 170
    :cond_0
    return-void
.end method

.method public setServiceId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    .line 160
    return-void
.end method
