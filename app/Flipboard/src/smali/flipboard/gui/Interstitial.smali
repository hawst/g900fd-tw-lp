.class public Lflipboard/gui/Interstitial;
.super Landroid/view/ViewGroup;
.source "Interstitial.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/util/MeterView;


# instance fields
.field public a:Lflipboard/gui/FLStaticTextView;

.field public b:Lflipboard/gui/FLStaticTextView;

.field public c:Lflipboard/gui/FLStaticTextView;

.field public d:I

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Lflipboard/gui/actionbar/FLActionBar;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Lflipboard/gui/FLImageView;

.field private k:I

.field private l:Lflipboard/gui/FLStaticTextView;

.field private m:Landroid/view/View;

.field private n:Lflipboard/util/MeteringHelper$ExitPath;

.field private o:Ljava/lang/String;

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 41
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->n:Lflipboard/util/MeteringHelper$ExitPath;

    .line 43
    const-string v0, "nytimes"

    iput-object v0, p0, Lflipboard/gui/Interstitial;->o:Ljava/lang/String;

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->n:Lflipboard/util/MeteringHelper$ExitPath;

    .line 43
    const-string v0, "nytimes"

    iput-object v0, p0, Lflipboard/gui/Interstitial;->o:Ljava/lang/String;

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    sget-object v0, Lflipboard/util/MeteringHelper$ExitPath;->b:Lflipboard/util/MeteringHelper$ExitPath;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->n:Lflipboard/util/MeteringHelper$ExitPath;

    .line 43
    const-string v0, "nytimes"

    iput-object v0, p0, Lflipboard/gui/Interstitial;->o:Ljava/lang/String;

    .line 60
    return-void
.end method


# virtual methods
.method public final a(ZI)V
    .locals 1

    .prologue
    .line 218
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/Interstitial;->p:Z

    .line 221
    :cond_0
    return-void
.end method

.method public getDebugString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 224
    const-string v0, ""

    return-object v0
.end method

.method public getViewType()Lflipboard/util/MeteringHelper$MeteringViewUsageType;
    .locals 1

    .prologue
    .line 204
    sget-object v0, Lflipboard/util/MeteringHelper$MeteringViewUsageType;->b:Lflipboard/util/MeteringHelper$MeteringViewUsageType;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 209
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 210
    iget-boolean v0, p0, Lflipboard/gui/Interstitial;->p:Z

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lflipboard/gui/Interstitial;->o:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/Interstitial;->n:Lflipboard/util/MeteringHelper$ExitPath;

    invoke-static {p0, v0, v1}, Lflipboard/util/MeteringHelper;->a(Lflipboard/util/MeterView;Ljava/lang/String;Lflipboard/util/MeteringHelper$ExitPath;)V

    .line 213
    :cond_0
    return-void
.end method

.method protected onFinishInflate()V
    .locals 8

    .prologue
    const v7, 0x7f0a0296

    const/4 v6, 0x1

    .line 79
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 80
    const v0, 0x7f0a028f

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    .line 81
    iget-object v0, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0, v6}, Lflipboard/gui/actionbar/FLActionBar;->setInverted(Z)V

    .line 82
    iget-object v0, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->f()Landroid/view/View;

    .line 83
    iget-object v0, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBar;->e()V

    .line 85
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "nytimes"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 86
    const v0, 0x7f0a028e

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->j:Lflipboard/gui/FLImageView;

    .line 87
    iget-object v0, p0, Lflipboard/gui/Interstitial;->j:Lflipboard/gui/FLImageView;

    iget-object v2, v1, Lflipboard/objs/ConfigService;->bK:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 89
    const v0, 0x7f0a0292

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    .line 90
    invoke-static {v1}, Lflipboard/util/MeteringHelper;->b(Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v0

    .line 91
    if-eqz v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lflipboard/gui/Interstitial;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "nytimes"

    invoke-static {v2, v3}, Lflipboard/util/MeteringHelper;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lflipboard/gui/Interstitial;->d:I

    .line 93
    iget-object v2, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lflipboard/gui/Interstitial;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    iget v1, v1, Lflipboard/objs/ConfigService;->bI:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    :cond_0
    const v0, 0x7f0a0295

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 97
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPaintFlags(I)V

    .line 98
    invoke-virtual {v0, p0}, Lflipboard/gui/FLTextView;->setTag(Ljava/lang/Object;)V

    .line 100
    invoke-virtual {p0, v7}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextView;

    .line 101
    invoke-virtual {v0}, Lflipboard/gui/FLTextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setPaintFlags(I)V

    .line 102
    invoke-virtual {v0, p0}, Lflipboard/gui/FLTextView;->setTag(Ljava/lang/Object;)V

    .line 103
    sget-boolean v1, Lflipboard/service/FlipboardManager;->q:Z

    if-eqz v1, :cond_1

    .line 104
    const v1, 0x7f0d0234

    invoke-virtual {v0, v1}, Lflipboard/gui/FLTextView;->setText(I)V

    .line 107
    :cond_1
    const v0, 0x7f0a0290

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->b:Lflipboard/gui/FLStaticTextView;

    .line 108
    const v0, 0x7f0a004f

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    .line 109
    const v0, 0x7f0a0224

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView;

    iput-object v0, p0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    .line 111
    const v0, 0x7f0a022d

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    .line 112
    const v0, 0x7f0a0291

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/Interstitial;->f:Landroid/view/View;

    .line 113
    const v0, 0x7f0a0293

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/Interstitial;->h:Landroid/view/View;

    .line 114
    invoke-virtual {p0, v7}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/Interstitial;->i:Landroid/view/View;

    .line 115
    invoke-virtual {p0}, Lflipboard/gui/Interstitial;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090080

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lflipboard/gui/Interstitial;->k:I

    .line 116
    const v0, 0x7f0a012a

    invoke-virtual {p0, v0}, Lflipboard/gui/Interstitial;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/Interstitial;->m:Landroid/view/View;

    .line 117
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 146
    sub-int v1, p4, p2

    .line 147
    sub-int v2, p5, p3

    .line 148
    iget-object v0, p0, Lflipboard/gui/Interstitial;->j:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, v5, v5, v1, v2}, Lflipboard/gui/FLImageView;->layout(IIII)V

    .line 149
    iget-object v0, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    iget-object v3, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v3}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v4}, Lflipboard/gui/actionbar/FLActionBar;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Lflipboard/gui/actionbar/FLActionBar;->layout(IIII)V

    .line 151
    div-int/lit8 v0, v2, 0x2

    iget-object v3, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 152
    iget-object v3, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int v4, v1, v4

    iget-object v5, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v3, v4, v0, v1, v5}, Landroid/view/View;->layout(IIII)V

    .line 154
    div-int/lit8 v0, v2, 0x2

    iget-object v3, p0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 155
    iget-object v3, p0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 156
    iget-object v4, p0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    iget v5, p0, Lflipboard/gui/Interstitial;->k:I

    iget v6, p0, Lflipboard/gui/Interstitial;->k:I

    iget-object v7, p0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v7}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v6, v7

    invoke-virtual {v4, v5, v0, v6, v3}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 158
    iget v4, p0, Lflipboard/gui/Interstitial;->k:I

    sub-int v4, v0, v4

    .line 159
    iget-object v0, p0, Lflipboard/gui/Interstitial;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v0}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v4, v0

    .line 162
    invoke-virtual {p0}, Lflipboard/gui/Interstitial;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070002

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 163
    iget-object v5, p0, Lflipboard/gui/Interstitial;->b:Lflipboard/gui/FLStaticTextView;

    iget v6, p0, Lflipboard/gui/Interstitial;->k:I

    iget v7, p0, Lflipboard/gui/Interstitial;->k:I

    iget-object v8, p0, Lflipboard/gui/Interstitial;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v0, v7, v4}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 164
    iget v4, p0, Lflipboard/gui/Interstitial;->k:I

    sub-int/2addr v0, v4

    .line 167
    :cond_0
    iget-object v4, p0, Lflipboard/gui/Interstitial;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    .line 168
    iget-object v5, p0, Lflipboard/gui/Interstitial;->f:Landroid/view/View;

    iget v6, p0, Lflipboard/gui/Interstitial;->k:I

    iget v7, p0, Lflipboard/gui/Interstitial;->k:I

    iget-object v8, p0, Lflipboard/gui/Interstitial;->f:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v4, v7, v0}, Landroid/view/View;->layout(IIII)V

    .line 170
    iget v0, p0, Lflipboard/gui/Interstitial;->k:I

    sub-int v0, v4, v0

    .line 171
    iget-object v4, p0, Lflipboard/gui/Interstitial;->m:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    .line 172
    iget-object v5, p0, Lflipboard/gui/Interstitial;->m:Landroid/view/View;

    iget v6, p0, Lflipboard/gui/Interstitial;->k:I

    iget v7, p0, Lflipboard/gui/Interstitial;->k:I

    iget-object v8, p0, Lflipboard/gui/Interstitial;->m:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v4, v7, v0}, Landroid/view/View;->layout(IIII)V

    .line 174
    iget v0, p0, Lflipboard/gui/Interstitial;->k:I

    sub-int v0, v4, v0

    .line 175
    iget-object v4, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    .line 176
    iget-object v5, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    iget v6, p0, Lflipboard/gui/Interstitial;->k:I

    iget v7, p0, Lflipboard/gui/Interstitial;->k:I

    iget-object v8, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v8}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    invoke-virtual {v5, v6, v4, v7, v0}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 178
    iget v0, p0, Lflipboard/gui/Interstitial;->k:I

    add-int/2addr v0, v3

    .line 179
    iget-object v3, p0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    iget v4, p0, Lflipboard/gui/Interstitial;->k:I

    iget v5, p0, Lflipboard/gui/Interstitial;->k:I

    iget-object v6, p0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v5, v6

    iget-object v6, p0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v6}, Lflipboard/gui/FLStaticTextView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v3, v4, v0, v5, v6}, Lflipboard/gui/FLStaticTextView;->layout(IIII)V

    .line 181
    iget-object v0, p0, Lflipboard/gui/Interstitial;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    .line 182
    iget v3, p0, Lflipboard/gui/Interstitial;->k:I

    sub-int/2addr v2, v3

    .line 183
    iget-object v3, p0, Lflipboard/gui/Interstitial;->h:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int v3, v2, v3

    .line 184
    iget-object v4, p0, Lflipboard/gui/Interstitial;->h:Landroid/view/View;

    iget-object v5, p0, Lflipboard/gui/Interstitial;->h:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v0

    invoke-virtual {v4, v0, v3, v5, v2}, Landroid/view/View;->layout(IIII)V

    .line 186
    iget-object v0, p0, Lflipboard/gui/Interstitial;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    .line 187
    iget v1, p0, Lflipboard/gui/Interstitial;->k:I

    sub-int v1, v3, v1

    .line 188
    iget-object v2, p0, Lflipboard/gui/Interstitial;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v2, v1, v2

    .line 189
    iget-object v3, p0, Lflipboard/gui/Interstitial;->i:Landroid/view/View;

    iget-object v4, p0, Lflipboard/gui/Interstitial;->i:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v3, v0, v2, v4, v1}, Landroid/view/View;->layout(IIII)V

    .line 190
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/high16 v7, 0x40000000    # 2.0f

    .line 122
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 123
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 125
    iget v2, p0, Lflipboard/gui/Interstitial;->k:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 126
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 127
    iget-object v4, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    invoke-virtual {v4, v2, v3}, Landroid/view/View;->measure(II)V

    .line 128
    iget-object v4, p0, Lflipboard/gui/Interstitial;->a:Lflipboard/gui/FLStaticTextView;

    iget v5, p0, Lflipboard/gui/Interstitial;->k:I

    mul-int/lit8 v5, v5, 0x3

    sub-int v5, v0, v5

    iget-object v6, p0, Lflipboard/gui/Interstitial;->e:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    div-int/lit8 v6, v1, 0x2

    invoke-static {v6, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 129
    invoke-virtual {p0}, Lflipboard/gui/Interstitial;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f070002

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 130
    iget-object v4, p0, Lflipboard/gui/Interstitial;->b:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4, v2, v3}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 132
    :cond_0
    iget-object v4, p0, Lflipboard/gui/Interstitial;->c:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4, v2, v3}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 133
    iget-object v4, p0, Lflipboard/gui/Interstitial;->f:Landroid/view/View;

    invoke-virtual {v4, v2, v3}, Landroid/view/View;->measure(II)V

    .line 134
    iget-object v4, p0, Lflipboard/gui/Interstitial;->g:Lflipboard/gui/actionbar/FLActionBar;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v4, v5, v3}, Lflipboard/gui/actionbar/FLActionBar;->measure(II)V

    .line 135
    iget-object v4, p0, Lflipboard/gui/Interstitial;->h:Landroid/view/View;

    invoke-virtual {v4, v2, v3}, Landroid/view/View;->measure(II)V

    .line 136
    iget-object v4, p0, Lflipboard/gui/Interstitial;->i:Landroid/view/View;

    invoke-virtual {v4, v2, v3}, Landroid/view/View;->measure(II)V

    .line 137
    iget-object v4, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v4, v2, v3}, Lflipboard/gui/FLStaticTextView;->measure(II)V

    .line 138
    iget-object v2, p0, Lflipboard/gui/Interstitial;->m:Landroid/view/View;

    iget-object v3, p0, Lflipboard/gui/Interstitial;->l:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v3}, Lflipboard/gui/FLStaticTextView;->getMeasuredWidth()I

    move-result v3

    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/Interstitial;->m:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 139
    iget-object v2, p0, Lflipboard/gui/Interstitial;->j:Lflipboard/gui/FLImageView;

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lflipboard/gui/FLImageView;->measure(II)V

    .line 140
    invoke-virtual {p0, v0, v1}, Lflipboard/gui/Interstitial;->setMeasuredDimension(II)V

    .line 141
    return-void
.end method

.method public setExitPath(Lflipboard/util/MeteringHelper$ExitPath;)V
    .locals 0

    .prologue
    .line 200
    iput-object p1, p0, Lflipboard/gui/Interstitial;->n:Lflipboard/util/MeteringHelper$ExitPath;

    .line 201
    return-void
.end method
