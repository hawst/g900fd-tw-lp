.class final enum Lflipboard/gui/FLSearchView$ResultType;
.super Ljava/lang/Enum;
.source "FLSearchView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLSearchView$ResultType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLSearchView$ResultType;

.field public static final enum b:Lflipboard/gui/FLSearchView$ResultType;

.field public static final enum c:Lflipboard/gui/FLSearchView$ResultType;

.field private static final synthetic d:[Lflipboard/gui/FLSearchView$ResultType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 99
    new-instance v0, Lflipboard/gui/FLSearchView$ResultType;

    const-string v1, "initial_search"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLSearchView$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$ResultType;->a:Lflipboard/gui/FLSearchView$ResultType;

    new-instance v0, Lflipboard/gui/FLSearchView$ResultType;

    const-string v1, "more_results"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLSearchView$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$ResultType;->b:Lflipboard/gui/FLSearchView$ResultType;

    new-instance v0, Lflipboard/gui/FLSearchView$ResultType;

    const-string v1, "third_party_search"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/FLSearchView$ResultType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$ResultType;->c:Lflipboard/gui/FLSearchView$ResultType;

    .line 98
    const/4 v0, 0x3

    new-array v0, v0, [Lflipboard/gui/FLSearchView$ResultType;

    sget-object v1, Lflipboard/gui/FLSearchView$ResultType;->a:Lflipboard/gui/FLSearchView$ResultType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLSearchView$ResultType;->b:Lflipboard/gui/FLSearchView$ResultType;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/FLSearchView$ResultType;->c:Lflipboard/gui/FLSearchView$ResultType;

    aput-object v1, v0, v4

    sput-object v0, Lflipboard/gui/FLSearchView$ResultType;->d:[Lflipboard/gui/FLSearchView$ResultType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLSearchView$ResultType;
    .locals 1

    .prologue
    .line 98
    const-class v0, Lflipboard/gui/FLSearchView$ResultType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchView$ResultType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLSearchView$ResultType;
    .locals 1

    .prologue
    .line 98
    sget-object v0, Lflipboard/gui/FLSearchView$ResultType;->d:[Lflipboard/gui/FLSearchView$ResultType;

    invoke-virtual {v0}, [Lflipboard/gui/FLSearchView$ResultType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLSearchView$ResultType;

    return-object v0
.end method
