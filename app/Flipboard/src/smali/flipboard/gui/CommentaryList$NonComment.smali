.class Lflipboard/gui/CommentaryList$NonComment;
.super Lflipboard/gui/CommentaryList$AuthorDetails;
.source "CommentaryList.java"


# direct methods
.method constructor <init>(Lflipboard/objs/CommentaryResult$Item$Commentary;)V
    .locals 4

    .prologue
    .line 315
    invoke-direct {p0}, Lflipboard/gui/CommentaryList$AuthorDetails;-><init>()V

    .line 316
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->a:Ljava/lang/String;

    .line 317
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->b:Ljava/lang/String;

    .line 318
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->c:Ljava/lang/String;

    .line 319
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->g:Lflipboard/objs/Image;

    invoke-virtual {v0}, Lflipboard/objs/Image;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->d:Ljava/lang/String;

    .line 321
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 322
    iget-object v0, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 323
    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v3, "author"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->f:Lflipboard/objs/FeedSectionLink;

    .line 330
    :cond_1
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList$NonComment;->a()V

    .line 331
    return-void

    .line 319
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lflipboard/objs/FeedSectionLink;)V
    .locals 2

    .prologue
    .line 305
    invoke-direct {p0}, Lflipboard/gui/CommentaryList$AuthorDetails;-><init>()V

    .line 306
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->b:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->a:Ljava/lang/String;

    .line 307
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->b:Ljava/lang/String;

    .line 308
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->h:Ljava/lang/String;

    const-string v1, "user"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    :goto_0
    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->c:Ljava/lang/String;

    .line 309
    iget-object v0, p1, Lflipboard/objs/FeedSectionLink;->f:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/CommentaryList$NonComment;->d:Ljava/lang/String;

    .line 310
    iput-object p1, p0, Lflipboard/gui/CommentaryList$NonComment;->f:Lflipboard/objs/FeedSectionLink;

    .line 311
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList$NonComment;->a()V

    .line 312
    return-void

    .line 308
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
