.class final enum Lflipboard/gui/ImageGroup$ImageGroupLayout;
.super Ljava/lang/Enum;
.source "ImageGroup.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/ImageGroup$ImageGroupLayout;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/ImageGroup$ImageGroupLayout;

.field public static final enum b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

.field public static final enum c:Lflipboard/gui/ImageGroup$ImageGroupLayout;

.field public static final enum d:Lflipboard/gui/ImageGroup$ImageGroupLayout;

.field public static final enum e:Lflipboard/gui/ImageGroup$ImageGroupLayout;

.field public static final enum f:Lflipboard/gui/ImageGroup$ImageGroupLayout;

.field private static final synthetic i:[Lflipboard/gui/ImageGroup$ImageGroupLayout;


# instance fields
.field final g:[Lflipboard/gui/ImageGroup$ImageLayout;

.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x4

    .line 86
    new-instance v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    const-string v1, "SINGLE"

    new-array v2, v7, [[F

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    aput-object v3, v2, v6

    invoke-direct {v0, v1, v6, v2}, Lflipboard/gui/ImageGroup$ImageGroupLayout;-><init>(Ljava/lang/String;I[[F)V

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->a:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 87
    new-instance v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    const-string v1, "DOUBLE_PORT"

    new-array v2, v8, [[F

    new-array v3, v5, [F

    fill-array-data v3, :array_1

    aput-object v3, v2, v6

    new-array v3, v5, [F

    fill-array-data v3, :array_2

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v7, v2}, Lflipboard/gui/ImageGroup$ImageGroupLayout;-><init>(Ljava/lang/String;I[[F)V

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 88
    new-instance v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    const-string v1, "DOUBLE_LAND"

    new-array v2, v8, [[F

    new-array v3, v5, [F

    fill-array-data v3, :array_3

    aput-object v3, v2, v6

    new-array v3, v5, [F

    fill-array-data v3, :array_4

    aput-object v3, v2, v7

    invoke-direct {v0, v1, v8, v2}, Lflipboard/gui/ImageGroup$ImageGroupLayout;-><init>(Ljava/lang/String;I[[F)V

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->c:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 89
    new-instance v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    const-string v1, "TRIPLE_ONE_PORT_TWO_LAND"

    new-array v2, v9, [[F

    new-array v3, v5, [F

    fill-array-data v3, :array_5

    aput-object v3, v2, v6

    new-array v3, v5, [F

    fill-array-data v3, :array_6

    aput-object v3, v2, v7

    new-array v3, v5, [F

    fill-array-data v3, :array_7

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v9, v2}, Lflipboard/gui/ImageGroup$ImageGroupLayout;-><init>(Ljava/lang/String;I[[F)V

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->d:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 90
    new-instance v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    const-string v1, "TRIPLE_ONE_LAND_TWO_PORT"

    new-array v2, v9, [[F

    new-array v3, v5, [F

    fill-array-data v3, :array_8

    aput-object v3, v2, v6

    new-array v3, v5, [F

    fill-array-data v3, :array_9

    aput-object v3, v2, v7

    new-array v3, v5, [F

    fill-array-data v3, :array_a

    aput-object v3, v2, v8

    invoke-direct {v0, v1, v5, v2}, Lflipboard/gui/ImageGroup$ImageGroupLayout;-><init>(Ljava/lang/String;I[[F)V

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->e:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 91
    new-instance v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    const-string v1, "QUAD"

    const/4 v2, 0x5

    new-array v3, v5, [[F

    new-array v4, v5, [F

    fill-array-data v4, :array_b

    aput-object v4, v3, v6

    new-array v4, v5, [F

    fill-array-data v4, :array_c

    aput-object v4, v3, v7

    new-array v4, v5, [F

    fill-array-data v4, :array_d

    aput-object v4, v3, v8

    new-array v4, v5, [F

    fill-array-data v4, :array_e

    aput-object v4, v3, v9

    invoke-direct {v0, v1, v2, v3}, Lflipboard/gui/ImageGroup$ImageGroupLayout;-><init>(Ljava/lang/String;I[[F)V

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->f:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    .line 85
    const/4 v0, 0x6

    new-array v0, v0, [Lflipboard/gui/ImageGroup$ImageGroupLayout;

    sget-object v1, Lflipboard/gui/ImageGroup$ImageGroupLayout;->a:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    aput-object v1, v0, v6

    sget-object v1, Lflipboard/gui/ImageGroup$ImageGroupLayout;->b:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    aput-object v1, v0, v7

    sget-object v1, Lflipboard/gui/ImageGroup$ImageGroupLayout;->c:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    aput-object v1, v0, v8

    sget-object v1, Lflipboard/gui/ImageGroup$ImageGroupLayout;->d:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    aput-object v1, v0, v9

    sget-object v1, Lflipboard/gui/ImageGroup$ImageGroupLayout;->e:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    aput-object v1, v0, v5

    const/4 v1, 0x5

    sget-object v2, Lflipboard/gui/ImageGroup$ImageGroupLayout;->f:Lflipboard/gui/ImageGroup$ImageGroupLayout;

    aput-object v2, v0, v1

    sput-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->i:[Lflipboard/gui/ImageGroup$ImageGroupLayout;

    return-void

    .line 86
    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 87
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    .line 88
    :array_3
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    .line 89
    :array_5
    .array-data 4
        0x0
        0x0
        0x3f000000    # 0.5f
        0x3f800000    # 1.0f
    .end array-data

    :array_6
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    :array_7
    .array-data 4
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    .line 90
    :array_8
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    :array_a
    .array-data 4
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    .line 91
    :array_b
    .array-data 4
        0x0
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    :array_c
    .array-data 4
        0x3f000000    # 0.5f
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    :array_d
    .array-data 4
        0x0
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data

    :array_e
    .array-data 4
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[[F)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[F)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 96
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 97
    array-length v0, p3

    iput v0, p0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->h:I

    .line 98
    iget v0, p0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->h:I

    new-array v0, v0, [Lflipboard/gui/ImageGroup$ImageLayout;

    iput-object v0, p0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->g:[Lflipboard/gui/ImageGroup$ImageLayout;

    move v6, v5

    .line 99
    :goto_0
    iget v0, p0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->h:I

    if-ge v6, v0, :cond_0

    .line 100
    aget-object v4, p3, v6

    .line 101
    iget-object v7, p0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->g:[Lflipboard/gui/ImageGroup$ImageLayout;

    new-instance v0, Lflipboard/gui/ImageGroup$ImageLayout;

    aget v1, v4, v5

    const/4 v2, 0x1

    aget v2, v4, v2

    const/4 v3, 0x2

    aget v3, v4, v3

    const/4 v8, 0x3

    aget v4, v4, v8

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/ImageGroup$ImageLayout;-><init>(FFFFB)V

    aput-object v0, v7, v6

    .line 99
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/ImageGroup$ImageGroupLayout;I)Lflipboard/gui/ImageGroup$ImageLayout;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->g:[Lflipboard/gui/ImageGroup$ImageLayout;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/ImageGroup$ImageGroupLayout;
    .locals 1

    .prologue
    .line 85
    const-class v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/ImageGroup$ImageGroupLayout;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lflipboard/gui/ImageGroup$ImageGroupLayout;->i:[Lflipboard/gui/ImageGroup$ImageGroupLayout;

    invoke-virtual {v0}, [Lflipboard/gui/ImageGroup$ImageGroupLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/ImageGroup$ImageGroupLayout;

    return-object v0
.end method
