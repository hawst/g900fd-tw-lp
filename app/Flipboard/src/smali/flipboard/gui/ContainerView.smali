.class public abstract Lflipboard/gui/ContainerView;
.super Landroid/widget/FrameLayout;
.source "ContainerView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    return-void
.end method

.method public static b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 51
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 52
    instance-of v1, v0, Lflipboard/gui/ContainerView;

    if-eqz v1, :cond_0

    .line 53
    check-cast v0, Lflipboard/gui/ContainerView;

    invoke-virtual {v0, p0}, Lflipboard/gui/ContainerView;->a(Landroid/view/View;)I

    move-result v0

    .line 58
    :goto_1
    return v0

    .line 55
    :cond_0
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_1

    .line 56
    check-cast v0, Landroid/view/View;

    move-object p0, v0

    goto :goto_0

    .line 58
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getPageOffset()I

    move-result v0

    return v0
.end method

.method public getPageOffset()I
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getWindowVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, -0x80000000

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lflipboard/gui/ContainerView;->b(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 29
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 30
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 31
    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 32
    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getPaddingTop()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 33
    invoke-virtual {p0}, Lflipboard/gui/ContainerView;->getChildCount()I

    move-result v0

    :goto_0
    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    .line 34
    invoke-virtual {p0, v0}, Lflipboard/gui/ContainerView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Landroid/view/View;->measure(II)V

    goto :goto_0

    .line 36
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 37
    return-void
.end method
