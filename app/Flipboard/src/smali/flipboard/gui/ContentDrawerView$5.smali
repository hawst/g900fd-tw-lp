.class Lflipboard/gui/ContentDrawerView$5;
.super Ljava/lang/Object;
.source "ContentDrawerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lflipboard/gui/ContentDrawerView;


# direct methods
.method constructor <init>(Lflipboard/gui/ContentDrawerView;Z)V
    .locals 0

    .prologue
    .line 297
    iput-object p1, p0, Lflipboard/gui/ContentDrawerView$5;->b:Lflipboard/gui/ContentDrawerView;

    iput-boolean p2, p0, Lflipboard/gui/ContentDrawerView$5;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 300
    sget-object v0, Lflipboard/gui/ContentDrawerView;->a:Lflipboard/util/Log;

    .line 301
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    :goto_0
    return-void

    .line 305
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerView$5;->b:Lflipboard/gui/ContentDrawerView;

    iget-object v1, v1, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Lflipboard/objs/ConfigService;->b()Ljava/lang/String;

    move-result-object v1

    .line 307
    iget-object v0, p0, Lflipboard/gui/ContentDrawerView$5;->b:Lflipboard/gui/ContentDrawerView;

    invoke-virtual {v0}, Lflipboard/gui/ContentDrawerView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/ContentDrawerActivity;

    .line 309
    new-instance v2, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 310
    const v3, 0x7f0d004a

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 312
    iget-boolean v3, p0, Lflipboard/gui/ContentDrawerView$5;->a:Z

    if-eqz v3, :cond_1

    .line 313
    const v1, 0x7f0d00a2

    invoke-virtual {v0, v1}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 314
    const v1, 0x7f0d00a1

    invoke-virtual {v0, v1}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 315
    const v1, 0x7f0d026a

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 316
    new-instance v1, Lflipboard/gui/ContentDrawerView$5$1;

    invoke-direct {v1, p0, v0}, Lflipboard/gui/ContentDrawerView$5$1;-><init>(Lflipboard/gui/ContentDrawerView$5;Lflipboard/activities/ContentDrawerActivity;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 358
    :goto_1
    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v1, "sign_out"

    invoke-virtual {v2, v0, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 326
    :cond_1
    const v3, 0x7f0d00a5

    invoke-virtual {v0, v3}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lflipboard/gui/dialog/FLAlertDialogFragment;->j:Ljava/lang/String;

    .line 327
    const v3, 0x7f0d02f4

    invoke-virtual {v2, v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 328
    iget-object v3, p0, Lflipboard/gui/ContentDrawerView$5;->b:Lflipboard/gui/ContentDrawerView;

    iget-object v3, v3, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lflipboard/gui/ContentDrawerView$5;->b:Lflipboard/gui/ContentDrawerView;

    iget-object v3, v3, Lflipboard/gui/ContentDrawerView;->i:Ljava/lang/String;

    const-string v4, "flipboard"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 329
    const v1, 0x7f0d00a3

    invoke-virtual {v2, v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 330
    new-instance v1, Lflipboard/gui/ContentDrawerView$5$2;

    invoke-direct {v1, p0, v0}, Lflipboard/gui/ContentDrawerView$5$2;-><init>(Lflipboard/gui/ContentDrawerView$5;Lflipboard/activities/ContentDrawerActivity;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto :goto_1

    .line 346
    :cond_2
    const v3, 0x7f0d00a4

    invoke-virtual {v0, v3}, Lflipboard/activities/ContentDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    .line 347
    new-instance v1, Lflipboard/gui/ContentDrawerView$5$3;

    invoke-direct {v1, p0, v0}, Lflipboard/gui/ContentDrawerView$5$3;-><init>(Lflipboard/gui/ContentDrawerView$5;Lflipboard/activities/ContentDrawerActivity;)V

    iput-object v1, v2, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    goto :goto_1
.end method
