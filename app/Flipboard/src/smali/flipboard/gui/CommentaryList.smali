.class public Lflipboard/gui/CommentaryList;
.super Landroid/widget/ScrollView;
.source "CommentaryList.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# static fields
.field private static f:I


# instance fields
.field a:Lflipboard/objs/ConfigService;

.field b:Lflipboard/objs/FeedItem;

.field c:Z

.field d:Lflipboard/service/Section;

.field e:Lflipboard/activities/FlipboardActivity;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/UserState$MutedAuthor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lflipboard/app/FlipboardApplication;->a:Lflipboard/app/FlipboardApplication;

    iget-boolean v0, v0, Lflipboard/app/FlipboardApplication;->f:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030126

    :goto_0
    sput v0, Lflipboard/gui/CommentaryList;->f:I

    return-void

    :cond_0
    const v0, 0x7f030127

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;Lflipboard/objs/FeedSectionLink;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 56
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03011d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/MagazineThumbView;

    const v1, 0x7f0a02b5

    invoke-virtual {v0, v1}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    const v2, 0x7f0a004f

    invoke-virtual {v0, v2}, Lflipboard/gui/MagazineThumbView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v3, 0x7f090117

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    const v3, 0x7f090116

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v0}, Lflipboard/gui/MagazineThumbView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v3, :cond_3

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    :goto_0
    invoke-virtual {v0, v3}, Lflipboard/gui/MagazineThumbView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v5, 0x7f090115

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v7, v7, v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    iget-object v3, v3, Lflipboard/objs/Image;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p1, Lflipboard/objs/FeedSectionLink;->l:Lflipboard/objs/Image;

    iget-object v3, v3, Lflipboard/objs/Image;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/gui/MagazineThumbView;->setAuthor(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Lflipboard/gui/MagazineThumbView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Lflipboard/gui/MagazineThumbView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0

    :cond_3
    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto :goto_0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 790
    sget-boolean v0, Lflipboard/service/FlipboardManager;->o:Z

    if-eqz v0, :cond_1

    .line 827
    :cond_0
    :goto_0
    return-void

    .line 794
    :cond_1
    iget-object v1, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    if-nez v1, :cond_3

    move-object v2, v3

    .line 796
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 798
    const v0, 0x7f0a032d

    invoke-virtual {p0, v0}, Lflipboard/gui/CommentaryList;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 799
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lflipboard/gui/CommentaryList;->f:I

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 800
    const v0, 0x7f0a0327

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 801
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 808
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v0, Lflipboard/gui/CommentaryList$11;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/CommentaryList$11;-><init>(Lflipboard/gui/CommentaryList;Ljava/util/List;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;)V

    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 794
    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, v1, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    const-string v5, "magazine"

    iget-object v6, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->a()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    iget-object v5, v1, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    iput-object v5, v0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v0, v1, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v0, :cond_7

    iget-object v0, v1, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    const-string v5, "magazine"

    iget-object v6, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->a()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    iget-object v5, v1, Lflipboard/objs/FeedItem;->ch:Lflipboard/objs/FeedItem;

    iget-object v5, v5, Lflipboard/objs/FeedItem;->aq:Ljava/lang/String;

    iput-object v5, v0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_7
    iget-object v0, v1, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, v1, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    iget-object v0, v0, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v0, "share"

    iget-object v5, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    if-eqz v0, :cond_8

    iget-object v0, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_9
    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    const-string v6, "magazine"

    iget-object v7, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v0}, Lflipboard/objs/FeedSectionLink;->a()Lflipboard/objs/FeedSectionLink;

    move-result-object v0

    iget-object v6, v1, Lflipboard/objs/CommentaryResult$Item$Commentary;->c:Ljava/lang/String;

    iput-object v6, v0, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v6, 0x3

    if-ge v0, v6, :cond_8

    goto :goto_3
.end method

.method private a(Lflipboard/gui/CommentaryList$Comment;II)V
    .locals 9

    .prologue
    .line 568
    iget-object v0, p0, Lflipboard/gui/CommentaryList;->g:Ljava/util/List;

    if-nez v0, :cond_1

    .line 569
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->k()Lflipboard/objs/UserState;

    move-result-object v0

    iget-object v0, v0, Lflipboard/objs/UserState;->j:Lflipboard/objs/UserState$State;

    .line 571
    if-nez v0, :cond_0

    iget-object v1, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    if-eqz v1, :cond_1

    .line 573
    :cond_0
    iget-object v1, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v1, v1, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 574
    iget-object v0, v0, Lflipboard/objs/UserState$State;->d:Lflipboard/objs/UserState$Data;

    iget-object v0, v0, Lflipboard/objs/UserState$Data;->b:Ljava/util/List;

    iput-object v0, p0, Lflipboard/gui/CommentaryList;->g:Ljava/util/List;

    .line 579
    :cond_1
    const/4 v0, 0x0

    .line 580
    iget-object v1, p0, Lflipboard/gui/CommentaryList;->g:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p1, Lflipboard/gui/CommentaryList$Comment;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 582
    iget-object v1, p0, Lflipboard/gui/CommentaryList;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/UserState$MutedAuthor;

    .line 583
    iget-object v3, p1, Lflipboard/gui/CommentaryList$Comment;->c:Ljava/lang/String;

    iget-object v0, v0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 584
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 586
    goto :goto_0

    :cond_2
    move v0, v1

    .line 588
    :cond_3
    if-eqz v0, :cond_4

    .line 718
    :goto_2
    return-void

    .line 591
    :cond_4
    iget-object v0, p1, Lflipboard/gui/CommentaryList$Comment;->b:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lflipboard/gui/CommentaryList$Comment;->b:Ljava/lang/String;

    move-object v2, v0

    .line 592
    :goto_3
    iget-object v0, p1, Lflipboard/gui/CommentaryList$Comment;->d:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lflipboard/gui/CommentaryList$Comment;->d:Ljava/lang/String;

    move-object v3, v0

    .line 593
    :goto_4
    iget-object v0, p1, Lflipboard/gui/CommentaryList$Comment;->g:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lflipboard/gui/CommentaryList$Comment;->g:Ljava/lang/String;

    move-object v4, v0

    .line 594
    :goto_5
    iget-wide v6, p1, Lflipboard/gui/CommentaryList$Comment;->i:J

    .line 596
    const v0, 0x7f0a032d

    invoke-virtual {p0, v0}, Lflipboard/gui/CommentaryList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 597
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    .line 598
    if-nez v1, :cond_5

    .line 599
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v5, Lflipboard/gui/CommentaryList;->f:I

    const/4 v8, 0x0

    invoke-static {v1, v5, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 600
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 601
    const v1, 0x7f0a0326

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    .line 602
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v1, v8}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 603
    const v1, 0x7f0a0329

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 604
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move-object v1, v5

    .line 606
    :cond_5
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v0

    const v5, 0x7f03012a

    const/4 v8, 0x0

    invoke-static {v0, v5, v8}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 608
    const v0, 0x7f0a0332

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLImageView;

    .line 609
    if-eqz v3, :cond_b

    .line 610
    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 615
    :goto_6
    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 616
    invoke-virtual {v0, p0}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 618
    const v0, 0x7f0a0333

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 619
    const-string v0, ""

    .line 620
    if-eqz v4, :cond_e

    .line 624
    :goto_7
    const/16 v0, 0x5dc

    invoke-static {v4, v0}, Lflipboard/util/JavaUtil;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 625
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 626
    const-string v0, "source"

    const-string v4, "sectionLink"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const-string v0, "originSectionIdentifier"

    iget-object v4, p0, Lflipboard/gui/CommentaryList;->d:Lflipboard/service/Section;

    iget-object v4, v4, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v4, v4, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    const-string v0, "linkType"

    const-string v4, "magazine"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    const v0, 0x7f0a0335

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p1, Lflipboard/gui/CommentaryList$Comment;->h:Ljava/util/List;

    invoke-static {v0, v2, v4, v3}, Lflipboard/util/SocialHelper;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/util/List;Landroid/os/Bundle;)V

    .line 630
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v7}, Lflipboard/util/JavaUtil;->d(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v2

    .line 631
    const v0, 0x7f0a0334

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLTextIntf;

    invoke-interface {v0, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 633
    const v0, 0x7f0a0328

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 634
    const v2, 0x7f0a0327

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 636
    iget-object v1, p1, Lflipboard/gui/CommentaryList$Comment;->j:Ljava/lang/String;

    invoke-virtual {v5, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 637
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 639
    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Landroid/view/View;->setLongClickable(Z)V

    .line 641
    new-instance v3, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v3, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    .line 642
    const v1, 0x7f0a026f

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    .line 643
    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    .line 644
    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 645
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09007b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 646
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09007d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 647
    invoke-virtual {v1, v2, v4, v2, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(IIII)V

    .line 649
    iget-object v2, p0, Lflipboard/gui/CommentaryList;->e:Lflipboard/activities/FlipboardActivity;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    .line 652
    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->a:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->a:Ljava/lang/String;

    const-string v4, "flipboard"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    .line 654
    :goto_8
    if-eqz v2, :cond_d

    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->k:Lflipboard/objs/CommentaryResult$Item$Commentary;

    iget-object v2, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v4, v4, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v4, v4, Lflipboard/service/User;->d:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x1

    .line 655
    :goto_9
    if-eqz v2, :cond_6

    .line 656
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    .line 657
    const v2, 0x7f0d012d

    invoke-virtual {v3, v2}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v2

    new-instance v4, Lflipboard/gui/CommentaryList$7;

    invoke-direct {v4, p0, p1, v0}, Lflipboard/gui/CommentaryList$7;-><init>(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$Comment;Landroid/view/ViewGroup;)V

    iput-object v4, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 687
    const/4 v0, 0x1

    iput-boolean v0, v2, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 691
    :cond_6
    iget-boolean v0, p1, Lflipboard/gui/CommentaryList$Comment;->l:Z

    if-eqz v0, :cond_7

    .line 692
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lflipboard/gui/actionbar/FLActionBar;->setVisibility(I)V

    .line 693
    const v0, 0x7f0d0019

    invoke-virtual {v3, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 694
    new-instance v2, Lflipboard/gui/CommentaryList$8;

    invoke-direct {v2, p0, p1}, Lflipboard/gui/CommentaryList$8;-><init>(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$Comment;)V

    iput-object v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 714
    const/4 v2, 0x1

    iput-boolean v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 715
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f0d026a

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 717
    :cond_7
    invoke-virtual {v1, v3}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    goto/16 :goto_2

    .line 591
    :cond_8
    const/4 v0, 0x0

    move-object v2, v0

    goto/16 :goto_3

    .line 592
    :cond_9
    const/4 v0, 0x0

    move-object v3, v0

    goto/16 :goto_4

    .line 593
    :cond_a
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_5

    .line 613
    :cond_b
    const v3, 0x7f02005a

    invoke-virtual {v0, v3}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_6

    .line 652
    :cond_c
    const/4 v2, 0x0

    goto :goto_8

    .line 654
    :cond_d
    const/4 v2, 0x0

    goto :goto_9

    :cond_e
    move-object v4, v0

    goto/16 :goto_7

    :cond_f
    move v0, v1

    goto/16 :goto_1
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;)V
    .locals 20

    .prologue
    .line 56
    const v1, 0x7f0a032d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/CommentaryList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    :goto_0
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    const v1, 0x7f0a032d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/CommentaryList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    :goto_1
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    invoke-direct/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->a()V

    invoke-direct/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->b()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v2, v2, Lflipboard/objs/FeedItem;->aP:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v14, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const/4 v7, 0x0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_3
    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_17

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    iget-object v12, v1, Lflipboard/objs/FeedItem;->cu:Lflipboard/objs/CommentaryResult$Item;

    if-eqz v12, :cond_3

    iget v11, v12, Lflipboard/objs/CommentaryResult$Item;->d:I

    iget v2, v12, Lflipboard/objs/CommentaryResult$Item;->c:I

    iget v9, v12, Lflipboard/objs/CommentaryResult$Item;->b:I

    add-int/2addr v9, v7

    iget-object v7, v12, Lflipboard/objs/CommentaryResult$Item;->h:Ljava/util/List;

    if-eqz v7, :cond_4

    iget-object v7, v12, Lflipboard/objs/CommentaryResult$Item;->h:Ljava/util/List;

    invoke-interface {v8, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_4
    const/4 v10, 0x0

    iget-object v7, v12, Lflipboard/objs/CommentaryResult$Item;->g:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v12, v11

    move v11, v2

    :cond_5
    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/objs/CommentaryResult$Item$Commentary;

    sget-object v7, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v13, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    invoke-virtual {v7, v13}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v7

    if-nez v10, :cond_6

    move-object v10, v7

    :cond_6
    iget-object v13, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v17, "comment"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_7
    iget-object v13, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v17, "like"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    iget-object v13, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    const-string v17, "googlereader"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    iget-object v13, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->a:Ljava/lang/String;

    iget-object v0, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->e:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-object v0, v14, Lflipboard/service/User;->i:Ljava/util/Map;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v19, ":"

    move-object/from16 v0, v19

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    if-eqz v13, :cond_c

    const/4 v13, 0x1

    :goto_4
    if-nez v13, :cond_5

    :cond_8
    const/4 v13, 0x0

    iget-object v0, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "like"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_e

    move-object/from16 v0, p0

    iget-object v13, v0, Lflipboard/gui/CommentaryList;->a:Lflipboard/objs/ConfigService;

    iget-boolean v13, v13, Lflipboard/objs/ConfigService;->ai:Z

    if-eqz v13, :cond_d

    const/4 v7, 0x0

    :goto_5
    move-object v13, v7

    :cond_9
    :goto_6
    if-eqz v13, :cond_5

    invoke-interface {v4, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4, v13, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v13, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    if-nez v7, :cond_a

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v17

    invoke-interface {v3, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_a
    invoke-interface {v7, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    invoke-interface {v4, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/List;

    new-instance v17, Lflipboard/gui/CommentaryList$NonComment;

    move-object/from16 v0, v17

    invoke-direct {v0, v2}, Lflipboard/gui/CommentaryList$NonComment;-><init>(Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    move-object/from16 v0, v17

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v5, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v5, v13, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v7, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v13, "like"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_10

    add-int/lit8 v2, v11, -0x1

    move v11, v2

    goto/16 :goto_3

    :cond_c
    const/4 v13, 0x0

    goto/16 :goto_4

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v13, v7}, Lflipboard/gui/SocialFormatter;->c(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v7

    goto :goto_5

    :cond_e
    iget-object v0, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    move-object/from16 v17, v0

    const-string v18, "share"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_f

    invoke-virtual {v7}, Lflipboard/objs/ConfigService;->g()Ljava/lang/String;

    move-result-object v7

    move-object v13, v7

    goto/16 :goto_6

    :cond_f
    iget-object v7, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v17, "sharedwith"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v13, 0x7f0d030a

    invoke-virtual {v7, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object v13, v7

    goto/16 :goto_6

    :cond_10
    iget-object v2, v2, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    const-string v7, "share"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    add-int/lit8 v2, v12, -0x1

    :goto_7
    move v12, v2

    goto/16 :goto_3

    :cond_11
    if-nez v10, :cond_12

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v10

    :cond_12
    if-lez v12, :cond_13

    invoke-virtual {v10}, Lflipboard/objs/ConfigService;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_13
    :goto_8
    if-lez v11, :cond_16

    iget-boolean v1, v10, Lflipboard/objs/ConfigService;->ai:Z

    if-nez v1, :cond_16

    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v10}, Lflipboard/gui/SocialFormatter;->c(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v7, v9

    goto/16 :goto_2

    :cond_14
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    :cond_15
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, v11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v5, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_16
    move v7, v9

    goto/16 :goto_2

    :cond_17
    new-instance v1, Lflipboard/gui/CommentaryList$4;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lflipboard/gui/CommentaryList$4;-><init>(Lflipboard/gui/CommentaryList;)V

    invoke-static {v8, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v1, Lflipboard/gui/CommentaryList$5;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lflipboard/gui/CommentaryList$5;-><init>(Lflipboard/gui/CommentaryList;)V

    invoke-static {v6, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    sget-object v9, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/CommentaryList$6;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v8}, Lflipboard/gui/CommentaryList$6;-><init>(Lflipboard/gui/CommentaryList;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ILjava/util/List;)V

    invoke-virtual {v9, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    return-void

    :cond_18
    move v2, v12

    goto/16 :goto_7
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$Comment;)V
    .locals 4

    .prologue
    .line 56
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->j:Ljava/lang/String;

    new-instance v3, Lflipboard/gui/CommentaryList$9;

    invoke-direct {v3, p0, p1}, Lflipboard/gui/CommentaryList$9;-><init>(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$Comment;)V

    invoke-virtual {v0, v1, v2, v3}, Lflipboard/service/FlipboardManager;->a(Lflipboard/objs/FeedItem;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$Comment;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 56
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->t()Lflipboard/service/Flap;

    move-result-object v0

    iget-object v2, p0, Lflipboard/gui/CommentaryList;->d:Lflipboard/service/Section;

    iget-object v3, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v4, p1, Lflipboard/gui/CommentaryList$Comment;->k:Lflipboard/objs/CommentaryResult$Item$Commentary;

    new-instance v6, Lflipboard/gui/CommentaryList$10;

    invoke-direct {v6, p0}, Lflipboard/gui/CommentaryList$10;-><init>(Lflipboard/gui/CommentaryList;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lflipboard/service/Flap;->a(Lflipboard/service/User;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/CommentaryResult$Item$Commentary;Ljava/lang/String;Lflipboard/service/Flap$JSONResultObserver;)V

    new-instance v0, Lflipboard/objs/UserState$MutedAuthor;

    invoke-direct {v0}, Lflipboard/objs/UserState$MutedAuthor;-><init>()V

    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->c:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->c:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->b:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->e:Ljava/lang/String;

    iget-object v2, p1, Lflipboard/gui/CommentaryList$Comment;->a:Ljava/lang/String;

    iput-object v2, v0, Lflipboard/objs/UserState$MutedAuthor;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lflipboard/service/User;->a(Ljava/util/List;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;Ljava/lang/String;Ljava/util/List;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/CommentaryList;->a(Ljava/lang/String;Ljava/util/List;I)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 56
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedItem;

    new-instance v2, Lflipboard/gui/CommentaryList$Comment;

    invoke-direct {v2, v0}, Lflipboard/gui/CommentaryList$Comment;-><init>(Lflipboard/objs/FeedItem;)V

    const/4 v0, 0x2

    const v3, 0x7f0d006f

    invoke-direct {p0, v2, v0, v3}, Lflipboard/gui/CommentaryList;->a(Lflipboard/gui/CommentaryList$Comment;II)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lflipboard/gui/CommentaryList;Ljava/util/List;I)V
    .locals 4

    .prologue
    .line 56
    if-lez p2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/CommentaryResult$Item$Commentary;

    const-string v2, "comment"

    iget-object v3, v0, Lflipboard/objs/CommentaryResult$Item$Commentary;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lflipboard/gui/CommentaryList$Comment;

    invoke-direct {v2, v0}, Lflipboard/gui/CommentaryList$Comment;-><init>(Lflipboard/objs/CommentaryResult$Item$Commentary;)V

    const/4 v0, 0x0

    const v3, 0x7f0d0301

    invoke-direct {p0, v2, v0, v3}, Lflipboard/gui/CommentaryList;->a(Lflipboard/gui/CommentaryList$Comment;II)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/util/List;I)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/CommentaryList$NonComment;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 853
    if-lez p3, :cond_6

    .line 854
    if-eqz p1, :cond_0

    invoke-static/range {p1 .. p1}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 856
    :goto_0
    const v1, 0x7f0a032d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lflipboard/gui/CommentaryList;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 857
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lflipboard/gui/CommentaryList;->f:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 858
    const v2, 0x7f0a0327

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 860
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 861
    new-instance v12, Landroid/widget/FrameLayout$LayoutParams;

    const v4, 0x7f09001f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v6, 0x7f09001f

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-direct {v12, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 862
    const/4 v4, 0x0

    const/4 v6, 0x0

    const v7, 0x7f090115

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const/4 v7, 0x0

    invoke-virtual {v12, v4, v6, v3, v7}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 864
    const/4 v9, 0x0

    .line 865
    const/4 v8, 0x0

    .line 866
    const/4 v6, 0x0

    .line 867
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lflipboard/gui/CommentaryList$NonComment;

    .line 868
    iget-object v4, v3, Lflipboard/gui/CommentaryList$NonComment;->b:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v7, v3, Lflipboard/gui/CommentaryList$NonComment;->b:Ljava/lang/String;

    .line 869
    :goto_2
    iget-object v4, v3, Lflipboard/gui/CommentaryList$NonComment;->d:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lflipboard/gui/CommentaryList$NonComment;->d:Ljava/lang/String;

    move-object v10, v4

    .line 870
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v4

    const v14, 0x7f030028

    const/4 v15, 0x0

    invoke-static {v4, v14, v15}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lflipboard/gui/FLImageView;

    .line 874
    invoke-virtual {v4, v12}, Lflipboard/gui/FLImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 875
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 876
    if-eqz v10, :cond_3

    .line 877
    invoke-virtual {v4, v10}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 881
    :goto_4
    invoke-virtual {v4, v3}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 882
    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lflipboard/gui/FLImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 883
    if-eqz v7, :cond_c

    .line 884
    if-nez v9, :cond_4

    move-object v3, v6

    .line 889
    :goto_5
    add-int/lit8 v4, v9, 0x1

    :goto_6
    move-object v6, v3

    move-object v8, v7

    move v9, v4

    .line 891
    goto :goto_1

    .line 854
    :cond_0
    const-string v1, ""

    move-object v5, v1

    goto/16 :goto_0

    .line 868
    :cond_1
    const/4 v7, 0x0

    goto :goto_2

    .line 869
    :cond_2
    const/4 v4, 0x0

    move-object v10, v4

    goto :goto_3

    .line 879
    :cond_3
    const v10, 0x7f02005a

    invoke-virtual {v4, v10}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto :goto_4

    .line 886
    :cond_4
    const/4 v3, 0x1

    if-ne v9, v3, :cond_b

    move-object v3, v7

    move-object v7, v8

    .line 887
    goto :goto_5

    .line 897
    :cond_5
    if-lez v9, :cond_6

    .line 899
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 901
    const v2, 0x7f0a0326

    invoke-virtual {v11, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    invoke-interface {v2, v5}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 903
    invoke-virtual {v1, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 904
    const v1, 0x7f0a0328

    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 905
    const v1, 0x7f0a0327

    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 908
    const/4 v1, 0x0

    .line 909
    const/4 v2, 0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_7

    const/4 v2, 0x1

    if-ne v9, v2, :cond_7

    .line 910
    const v1, 0x7f0d0243

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lflipboard/gui/CommentaryList;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v8, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 925
    :goto_7
    const v1, 0x7f0a0329

    invoke-virtual {v11, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 928
    :cond_6
    return-void

    .line 911
    :cond_7
    const/4 v2, 0x2

    move/from16 v0, p3

    if-ne v0, v2, :cond_8

    const/4 v2, 0x2

    if-ne v9, v2, :cond_8

    .line 913
    const v1, 0x7f0d0330

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lflipboard/gui/CommentaryList;->a(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v8, v2, v3

    const/4 v3, 0x1

    aput-object v6, v2, v3

    invoke-static {v1, v2}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_7

    .line 914
    :cond_8
    const/4 v2, 0x2

    move/from16 v0, p3

    if-lt v0, v2, :cond_a

    .line 918
    add-int/lit8 v1, p3, -0x1

    .line 919
    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    .line 920
    const v2, 0x7f0d0209

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lflipboard/gui/CommentaryList;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_7

    .line 922
    :cond_9
    const v2, 0x7f0d0208

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lflipboard/gui/CommentaryList;->a(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v8, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_7

    :cond_a
    move-object v2, v1

    goto :goto_7

    :cond_b
    move-object v3, v6

    move-object v7, v8

    goto/16 :goto_5

    :cond_c
    move-object v3, v6

    move-object v7, v8

    move v4, v9

    goto/16 :goto_6
.end method

.method private b()V
    .locals 5

    .prologue
    .line 832
    iget-object v0, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->aJ:Lflipboard/objs/FeedItem;

    .line 835
    :goto_0
    iget-object v1, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    if-eqz v1, :cond_4

    .line 836
    const/4 v1, 0x0

    .line 837
    iget-object v0, v0, Lflipboard/objs/FeedItem;->aI:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 838
    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lflipboard/objs/FeedSectionLink;->a:Ljava/lang/String;

    const-string v4, "textLink"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 839
    if-nez v1, :cond_1

    .line 840
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 842
    :cond_1
    new-instance v3, Lflipboard/gui/CommentaryList$NonComment;

    invoke-direct {v3, v0}, Lflipboard/gui/CommentaryList$NonComment;-><init>(Lflipboard/objs/FeedSectionLink;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 832
    :cond_2
    iget-object v0, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    goto :goto_0

    .line 845
    :cond_3
    if-eqz v1, :cond_4

    .line 846
    const v0, 0x7f0d0308

    invoke-direct {p0, v0}, Lflipboard/gui/CommentaryList;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lflipboard/gui/CommentaryList;->a(Ljava/lang/String;Ljava/util/List;I)V

    .line 849
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 153
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/CommentaryList$2;

    invoke-direct {v1, p0}, Lflipboard/gui/CommentaryList$2;-><init>(Lflipboard/gui/CommentaryList;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 160
    return-void
.end method

.method public final a(Lflipboard/service/Section;Lflipboard/objs/ConfigService;Lflipboard/objs/FeedItem;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 97
    sget-object v0, Lflipboard/gui/SocialBarTablet;->a:Lflipboard/util/Log;

    new-array v0, v3, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p2, Lflipboard/objs/ConfigService;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    .line 99
    iput-object p2, p0, Lflipboard/gui/CommentaryList;->a:Lflipboard/objs/ConfigService;

    .line 100
    iput-object p3, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    .line 101
    iput-object p1, p0, Lflipboard/gui/CommentaryList;->d:Lflipboard/service/Section;

    .line 102
    iput-boolean v3, p0, Lflipboard/gui/CommentaryList;->c:Z

    .line 104
    iget-object v0, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 105
    invoke-direct {p0}, Lflipboard/gui/CommentaryList;->b()V

    .line 108
    invoke-direct {p0}, Lflipboard/gui/CommentaryList;->a()V

    .line 111
    const v0, 0x7f0a032e

    invoke-virtual {p0, v0}, Lflipboard/gui/CommentaryList;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 112
    sget-object v1, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v1}, Lflipboard/io/NetworkManager;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    iget-object v1, p0, Lflipboard/gui/CommentaryList;->e:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f0d0218

    invoke-direct {p0, v2}, Lflipboard/gui/CommentaryList;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 114
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 149
    :goto_0
    return-void

    .line 118
    :cond_0
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v2, Lflipboard/gui/CommentaryList$3;

    invoke-direct {v2, p0}, Lflipboard/gui/CommentaryList$3;-><init>(Lflipboard/gui/CommentaryList;)V

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 119
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 120
    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v3, Lflipboard/gui/CommentaryList$1;

    invoke-direct {v3, p0, v0}, Lflipboard/gui/CommentaryList$1;-><init>(Lflipboard/gui/CommentaryList;Landroid/view/View;)V

    invoke-virtual {v2, v1, v3}, Lflipboard/service/FlipboardManager;->c(Ljava/util/List;Lflipboard/service/Flap$CommentaryObserver;)V

    goto :goto_0
.end method

.method public onAttachedToWindow()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Landroid/widget/ScrollView;->onAttachedToWindow()V

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/CommentaryList;->c:Z

    .line 166
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x0

    const/16 v12, 0x8

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1030
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-virtual {v0}, Lflipboard/service/FlipboardManager;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1052
    :cond_0
    :goto_0
    return-void

    .line 1033
    :cond_1
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1037
    instance-of v0, p1, Lflipboard/gui/FLImageView;

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lflipboard/gui/CommentaryList$AuthorDetails;

    if-eqz v0, :cond_b

    .line 1038
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/CommentaryList$AuthorDetails;

    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v5

    new-instance v7, Lflipboard/gui/FLPopoverWindow;

    invoke-direct {v7, p1}, Lflipboard/gui/FLPopoverWindow;-><init>(Landroid/view/View;)V

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f080004

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v1, v7, Lflipboard/gui/FLPopoverWindow;->h:I

    iput v2, v7, Lflipboard/gui/FLPopoverWindow;->i:I

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090098

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v8, 0x7f090099

    invoke-virtual {v2, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v1, v7, Lflipboard/gui/FLPopoverWindow;->f:I

    iput v2, v7, Lflipboard/gui/FLPopoverWindow;->g:I

    iget-object v1, v7, Lflipboard/gui/FLPopoverWindow;->d:Lflipboard/gui/FLPopoverWindow$RootView;

    iget-object v1, v1, Lflipboard/gui/FLPopoverWindow$RootView;->a:Lflipboard/gui/FLPopoverWindow$TriangleView;

    invoke-virtual {v1}, Lflipboard/gui/FLPopoverWindow$TriangleView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, v7, Lflipboard/gui/FLPopoverWindow;->f:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v2, v7, Lflipboard/gui/FLPopoverWindow;->g:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v8, v7, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    const v1, 0x7f0a0339

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iget-object v2, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :goto_1
    iget-object v1, v5, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    if-eqz v1, :cond_2

    const v1, 0x7f0a033b

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iget-object v2, v5, Lflipboard/objs/ConfigService;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    :cond_2
    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->b:Ljava/lang/String;

    if-eqz v1, :cond_3

    const v1, 0x7f0a033a

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iget-object v2, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    const v1, 0x7f0a0338

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->f:Lflipboard/objs/FeedSectionLink;

    if-eqz v1, :cond_e

    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->f:Lflipboard/objs/FeedSectionLink;

    iget-object v2, p0, Lflipboard/gui/CommentaryList;->d:Lflipboard/service/Section;

    invoke-virtual {v2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/objs/FeedSectionLink;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const v1, 0x7f0a033d

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v1, Lflipboard/gui/CommentaryList$12;

    invoke-direct {v1, p0, v9, v7, v0}, Lflipboard/gui/CommentaryList$12;-><init>(Lflipboard/gui/CommentaryList;Landroid/view/View;Lflipboard/gui/FLPopoverWindow;Lflipboard/gui/CommentaryList$AuthorDetails;)V

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move v2, v3

    :goto_2
    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->c:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-boolean v1, v5, Lflipboard/objs/ConfigService;->M:Z

    if-eqz v1, :cond_5

    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->c:Ljava/lang/String;

    const-string v10, "@"

    invoke-virtual {v1, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    :cond_4
    iget-object v10, v5, Lflipboard/objs/ConfigService;->N:Ljava/lang/String;

    if-eqz v10, :cond_9

    iget-object v5, v5, Lflipboard/objs/ConfigService;->N:Ljava/lang/String;

    const-string v10, "%@"

    const-string v11, "%s"

    invoke-virtual {v5, v10, v11}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    :goto_3
    new-array v10, v3, [Ljava/lang/Object;

    aput-object v1, v10, v4

    invoke-static {v5, v10}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const v1, 0x7f0a033c

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    invoke-interface {v1, v5}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->e:Ljava/lang/String;

    if-eqz v1, :cond_a

    const v1, 0x7f0a033f

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iget-object v5, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->e:Ljava/lang/String;

    invoke-interface {v1, v5}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f0a0340

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/View;->setVisibility(I)V

    :goto_4
    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->f:Lflipboard/objs/FeedSectionLink;

    if-eqz v1, :cond_d

    iget-object v1, v0, Lflipboard/gui/CommentaryList$AuthorDetails;->f:Lflipboard/objs/FeedSectionLink;

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->j:Ljava/lang/String;

    if-eqz v1, :cond_d

    const v1, 0x7f0a004c

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBar;

    invoke-virtual {v1, v4, v4}, Lflipboard/gui/actionbar/FLActionBar;->a(ZZ)Landroid/view/View;

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->d()V

    iget-object v5, p0, Lflipboard/gui/CommentaryList;->e:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1, v5, v6}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/activities/FlipboardActivity;Lflipboard/gui/dialog/FLDialogFragment;)V

    new-instance v5, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x5

    const v8, 0x7f0d0346

    invoke-virtual {v5, v4, v6, v4, v8}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v6

    const v8, 0x7f0201cd

    invoke-virtual {v6, v8}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v6

    new-instance v8, Lflipboard/gui/CommentaryList$13;

    invoke-direct {v8, p0, v0, v7}, Lflipboard/gui/CommentaryList$13;-><init>(Lflipboard/gui/CommentaryList;Lflipboard/gui/CommentaryList$AuthorDetails;Lflipboard/gui/FLPopoverWindow;)V

    iput-object v8, v6, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    iput-boolean v3, v6, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    invoke-virtual {v1, v5}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    :goto_5
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLPopoverWindow:show"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-boolean v3, v0, Lflipboard/service/FlipboardManager;->am:Z

    iget-object v0, v7, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v7, v0, v4, v4, v4}, Lflipboard/gui/FLPopoverWindow;->showAtLocation(Landroid/view/View;III)V

    invoke-virtual {v7, v3}, Lflipboard/gui/FLPopoverWindow;->setFocusable(Z)V

    iget-object v0, v7, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v3, v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v3, :cond_6

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, v7}, Lflipboard/activities/FlipboardActivity;->a(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V

    :cond_6
    if-eqz v2, :cond_7

    sget-object v0, Lflipboard/service/HintManager;->b:Lflipboard/service/HintManager;

    const-string v2, "nanoPopoverViewSection"

    invoke-virtual {v0, v9, v2}, Lflipboard/service/HintManager;->b(Landroid/view/View;Ljava/lang/String;)V

    :cond_7
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lflipboard/gui/actionbar/FLActionBar;->i()V

    goto/16 :goto_0

    :cond_8
    const v2, 0x7f02005a

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    goto/16 :goto_1

    :cond_9
    const-string v5, "@%s"

    goto/16 :goto_3

    :cond_a
    const v1, 0x7f0a033e

    invoke-virtual {v8, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v12}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    .line 1039
    :cond_b
    instance-of v0, p1, Lflipboard/gui/MagazineThumbView;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1040
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/FeedSectionLink;

    .line 1041
    iget-object v1, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1042
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v2, v0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    .line 1043
    if-nez v1, :cond_c

    .line 1044
    new-instance v1, Lflipboard/service/Section;

    invoke-direct {v1, v0}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    move-object v0, v1

    .line 1046
    :goto_6
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1047
    const-string v2, "source"

    const-string v3, "authorPopover"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1048
    const-string v2, "originSectionIdentifier"

    iget-object v3, p0, Lflipboard/gui/CommentaryList;->d:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1049
    invoke-virtual {p0}, Lflipboard/gui/CommentaryList;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lflipboard/util/SocialHelper;->a(Lflipboard/service/Section;Landroid/content/Context;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    goto :goto_6

    :cond_d
    move-object v1, v6

    goto/16 :goto_5

    :cond_e
    move v2, v4

    goto/16 :goto_2
.end method

.method public onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 170
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/CommentaryList;->c:Z

    .line 172
    iget-object v0, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lflipboard/gui/CommentaryList;->b:Lflipboard/objs/FeedItem;

    invoke-virtual {v0}, Lflipboard/objs/FeedItem;->E()Lflipboard/objs/FeedItem;

    move-result-object v0

    invoke-virtual {v0, p0}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 175
    :cond_0
    return-void
.end method

.method public setActivity(Lflipboard/activities/FlipboardActivity;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lflipboard/gui/CommentaryList;->e:Lflipboard/activities/FlipboardActivity;

    .line 85
    return-void
.end method
