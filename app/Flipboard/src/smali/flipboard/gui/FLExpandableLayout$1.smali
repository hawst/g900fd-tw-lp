.class Lflipboard/gui/FLExpandableLayout$1;
.super Ljava/lang/Object;
.source "FLExpandableLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/FLExpandableLayout;


# direct methods
.method constructor <init>(Lflipboard/gui/FLExpandableLayout;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 120
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->a(Lflipboard/gui/FLExpandableLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->b(Lflipboard/gui/FLExpandableLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->c(Lflipboard/gui/FLExpandableLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 123
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->c(Lflipboard/gui/FLExpandableLayout;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0, v1}, Lflipboard/gui/FLExpandableLayout;->a(Lflipboard/gui/FLExpandableLayout;Z)Z

    .line 125
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->d(Lflipboard/gui/FLExpandableLayout;)Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->d(Lflipboard/gui/FLExpandableLayout;)Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->c(Lflipboard/gui/FLExpandableLayout;)Landroid/view/View;

    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->e(Lflipboard/gui/FLExpandableLayout;)I

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->a(Lflipboard/gui/FLExpandableLayout;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->f(Lflipboard/gui/FLExpandableLayout;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->c(Lflipboard/gui/FLExpandableLayout;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lflipboard/gui/FLExpandableLayout;->a(Lflipboard/gui/FLExpandableLayout;Z)Z

    .line 132
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->d(Lflipboard/gui/FLExpandableLayout;)Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->d(Lflipboard/gui/FLExpandableLayout;)Lflipboard/gui/FLExpandableLayout$OnExpandClickListener;

    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->c(Lflipboard/gui/FLExpandableLayout;)Landroid/view/View;

    iget-object v0, p0, Lflipboard/gui/FLExpandableLayout$1;->a:Lflipboard/gui/FLExpandableLayout;

    invoke-static {v0}, Lflipboard/gui/FLExpandableLayout;->e(Lflipboard/gui/FLExpandableLayout;)I

    goto :goto_0
.end method
