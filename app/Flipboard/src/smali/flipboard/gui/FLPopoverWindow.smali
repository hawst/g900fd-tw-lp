.class public Lflipboard/gui/FLPopoverWindow;
.super Landroid/widget/PopupWindow;
.source "FLPopoverWindow.java"

# interfaces
.implements Lflipboard/activities/FlipboardActivity$OnBackPressedListener;


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field b:Landroid/view/WindowManager;

.field c:Landroid/view/View;

.field d:Lflipboard/gui/FLPopoverWindow$RootView;

.field e:Landroid/view/View;

.field f:I

.field g:I

.field h:I

.field i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "item"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLPopoverWindow;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 63
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 54
    const/16 v0, 0x28

    iput v0, p0, Lflipboard/gui/FLPopoverWindow;->f:I

    .line 55
    const/16 v0, 0x14

    iput v0, p0, Lflipboard/gui/FLPopoverWindow;->g:I

    .line 65
    const v0, 0x7f0e0014

    invoke-virtual {p0, v0}, Lflipboard/gui/FLPopoverWindow;->setAnimationStyle(I)V

    .line 66
    iput-object p1, p0, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    .line 67
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lflipboard/gui/FLPopoverWindow;->b:Landroid/view/WindowManager;

    .line 70
    new-instance v0, Lflipboard/gui/FLPopoverWindow$RootView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lflipboard/gui/FLPopoverWindow$RootView;-><init>(Lflipboard/gui/FLPopoverWindow;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/FLPopoverWindow;->d:Lflipboard/gui/FLPopoverWindow$RootView;

    .line 71
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow;->d:Lflipboard/gui/FLPopoverWindow$RootView;

    iget-object v0, v0, Lflipboard/gui/FLPopoverWindow$RootView;->b:Lflipboard/gui/FLPopoverWindow;

    iget-object v0, v0, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    iput-object v0, p0, Lflipboard/gui/FLPopoverWindow;->e:Landroid/view/View;

    .line 75
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLPopoverWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 76
    invoke-virtual {p0, v2}, Lflipboard/gui/FLPopoverWindow;->setWidth(I)V

    .line 77
    invoke-virtual {p0, v2}, Lflipboard/gui/FLPopoverWindow;->setHeight(I)V

    .line 78
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow;->d:Lflipboard/gui/FLPopoverWindow$RootView;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLPopoverWindow;->setContentView(Landroid/view/View;)V

    .line 80
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 131
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLPopoverWindow:dismiss"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 132
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-boolean v1, v0, Lflipboard/service/FlipboardManager;->am:Z

    .line 134
    invoke-virtual {p0, v1}, Lflipboard/gui/FLPopoverWindow;->setFocusable(Z)V

    .line 135
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 136
    if-eqz p1, :cond_0

    .line 137
    iget-object v0, p0, Lflipboard/gui/FLPopoverWindow;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 138
    instance-of v1, v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v1, :cond_0

    .line 139
    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0, p0}, Lflipboard/activities/FlipboardActivity;->b(Lflipboard/activities/FlipboardActivity$OnBackPressedListener;)V

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 2

    .prologue
    .line 152
    invoke-virtual {p0}, Lflipboard/gui/FLPopoverWindow;->isShowing()Z

    move-result v0

    .line 153
    if-eqz v0, :cond_0

    .line 154
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lflipboard/gui/FLPopoverWindow;->a(Z)V

    .line 156
    :cond_0
    return v0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lflipboard/gui/FLPopoverWindow;->a(Z)V

    .line 127
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 148
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
