.class public Lflipboard/gui/IconBar$ToggleImageButton;
.super Lflipboard/gui/FLImageView;
.source "IconBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic f:Lflipboard/gui/IconBar;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lflipboard/gui/IconBar;Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    .line 117
    invoke-direct {p0, p2}, Lflipboard/gui/FLImageView;-><init>(Landroid/content/Context;)V

    .line 118
    invoke-virtual {p0, p0}, Lflipboard/gui/IconBar$ToggleImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iput-object p3, p0, Lflipboard/gui/IconBar$ToggleImageButton;->g:Ljava/lang/String;

    .line 120
    iput-object p5, p0, Lflipboard/gui/IconBar$ToggleImageButton;->h:Ljava/lang/String;

    .line 121
    iput-object p6, p0, Lflipboard/gui/IconBar$ToggleImageButton;->i:Ljava/lang/String;

    .line 122
    invoke-virtual {p0, p4}, Lflipboard/gui/IconBar$ToggleImageButton;->setSelected(Z)V

    .line 123
    return-void
.end method

.method public synthetic constructor <init>(Lflipboard/gui/IconBar;Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 108
    invoke-direct/range {p0 .. p6}, Lflipboard/gui/IconBar$ToggleImageButton;-><init>(Lflipboard/gui/IconBar;Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 134
    invoke-virtual {p0}, Lflipboard/gui/IconBar$ToggleImageButton;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 135
    :goto_0
    iget-object v1, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-static {v1}, Lflipboard/gui/IconBar;->a(Lflipboard/gui/IconBar;)Lflipboard/gui/IconBar$OnChangeObserver;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 136
    if-eqz v0, :cond_2

    .line 137
    iget-object v1, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-static {v1}, Lflipboard/gui/IconBar;->b(Lflipboard/gui/IconBar;)I

    .line 138
    iget-object v1, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-static {v1}, Lflipboard/gui/IconBar;->a(Lflipboard/gui/IconBar;)Lflipboard/gui/IconBar$OnChangeObserver;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/IconBar$ToggleImageButton;->g:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-virtual {v3}, Lflipboard/gui/IconBar;->getNumSelectedIcons()I

    invoke-interface {v1, v2}, Lflipboard/gui/IconBar$OnChangeObserver;->a(Ljava/lang/String;)V

    .line 144
    :cond_0
    :goto_1
    invoke-virtual {p0, v0}, Lflipboard/gui/IconBar$ToggleImageButton;->setSelected(Z)V

    .line 145
    return-void

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_2
    iget-object v1, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-static {v1}, Lflipboard/gui/IconBar;->c(Lflipboard/gui/IconBar;)I

    .line 141
    iget-object v1, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-static {v1}, Lflipboard/gui/IconBar;->a(Lflipboard/gui/IconBar;)Lflipboard/gui/IconBar$OnChangeObserver;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/IconBar$ToggleImageButton;->g:Ljava/lang/String;

    iget-object v3, p0, Lflipboard/gui/IconBar$ToggleImageButton;->f:Lflipboard/gui/IconBar;

    invoke-virtual {v3}, Lflipboard/gui/IconBar;->getNumSelectedIcons()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lflipboard/gui/IconBar$OnChangeObserver;->a(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public setSelected(Z)V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lflipboard/gui/IconBar$ToggleImageButton;->a()V

    .line 128
    if-eqz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/IconBar$ToggleImageButton;->h:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lflipboard/gui/IconBar$ToggleImageButton;->setImage(Ljava/lang/String;)V

    .line 129
    invoke-super {p0, p1}, Lflipboard/gui/FLImageView;->setSelected(Z)V

    .line 130
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lflipboard/gui/IconBar$ToggleImageButton;->i:Ljava/lang/String;

    goto :goto_0
.end method
