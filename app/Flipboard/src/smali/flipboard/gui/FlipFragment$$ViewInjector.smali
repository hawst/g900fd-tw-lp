.class public Lflipboard/gui/FlipFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "FlipFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lflipboard/gui/FlipFragment;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 10
    const v0, 0x7f0a01a1

    const-string v1, "field \'magazineGrid\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p1, Lflipboard/gui/FlipFragment;->a:Landroid/widget/GridView;

    .line 12
    const v0, 0x7f0a01a2

    const-string v1, "field \'commentEditText\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p1, Lflipboard/gui/FlipFragment;->b:Landroid/widget/EditText;

    .line 14
    const v0, 0x7f0a01a0

    const-string v1, "field \'actionBar\'"

    invoke-virtual {p0, p2, v0, v1}, Lbutterknife/ButterKnife$Finder;->a(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    check-cast v0, Lflipboard/gui/actionbar/FLActionBar;

    iput-object v0, p1, Lflipboard/gui/FlipFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    .line 16
    return-void
.end method

.method public static reset(Lflipboard/gui/FlipFragment;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 19
    iput-object v0, p0, Lflipboard/gui/FlipFragment;->a:Landroid/widget/GridView;

    .line 20
    iput-object v0, p0, Lflipboard/gui/FlipFragment;->b:Landroid/widget/EditText;

    .line 21
    iput-object v0, p0, Lflipboard/gui/FlipFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    .line 22
    return-void
.end method
