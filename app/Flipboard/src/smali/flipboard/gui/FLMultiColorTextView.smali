.class public Lflipboard/gui/FLMultiColorTextView;
.super Lflipboard/gui/FLTextView;
.source "FLMultiColorTextView.java"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 60
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->c:I

    if-gtz v0, :cond_0

    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->d:I

    invoke-virtual {p0}, Lflipboard/gui/FLMultiColorTextView;->getWidth()I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->c:I

    iget v1, p0, Lflipboard/gui/FLMultiColorTextView;->d:I

    if-eq v0, v1, :cond_1

    .line 62
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 63
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->b:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLMultiColorTextView;->setTextColor(I)V

    .line 64
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->c:I

    invoke-virtual {p0}, Lflipboard/gui/FLMultiColorTextView;->getHeight()I

    move-result v1

    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 65
    invoke-super {p0, p1}, Lflipboard/gui/FLTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 66
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 69
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 70
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->a:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLMultiColorTextView;->setTextColor(I)V

    .line 71
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->c:I

    iget v1, p0, Lflipboard/gui/FLMultiColorTextView;->d:I

    invoke-virtual {p0}, Lflipboard/gui/FLMultiColorTextView;->getHeight()I

    move-result v2

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 72
    invoke-super {p0, p1}, Lflipboard/gui/FLTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 73
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 76
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 77
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->b:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLMultiColorTextView;->setTextColor(I)V

    .line 78
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->d:I

    invoke-virtual {p0}, Lflipboard/gui/FLMultiColorTextView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLMultiColorTextView;->getHeight()I

    move-result v2

    invoke-virtual {p1, v0, v3, v1, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 79
    invoke-super {p0, p1}, Lflipboard/gui/FLTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 80
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 90
    :goto_0
    return-void

    .line 83
    :cond_1
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->c:I

    if-nez v0, :cond_2

    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->d:I

    invoke-virtual {p0}, Lflipboard/gui/FLMultiColorTextView;->getWidth()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 84
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->a:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLMultiColorTextView;->setTextColor(I)V

    .line 88
    :goto_1
    invoke-super {p0, p1}, Lflipboard/gui/FLTextView;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 86
    :cond_2
    iget v0, p0, Lflipboard/gui/FLMultiColorTextView;->b:I

    invoke-virtual {p0, v0}, Lflipboard/gui/FLMultiColorTextView;->setTextColor(I)V

    goto :goto_1
.end method
