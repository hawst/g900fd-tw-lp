.class public final enum Lflipboard/gui/FLStaticTextView$BlockType;
.super Ljava/lang/Enum;
.source "FLStaticTextView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLStaticTextView$BlockType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLStaticTextView$BlockType;

.field public static final enum b:Lflipboard/gui/FLStaticTextView$BlockType;

.field private static final synthetic c:[Lflipboard/gui/FLStaticTextView$BlockType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 351
    new-instance v0, Lflipboard/gui/FLStaticTextView$BlockType;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLStaticTextView$BlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLStaticTextView$BlockType;->a:Lflipboard/gui/FLStaticTextView$BlockType;

    new-instance v0, Lflipboard/gui/FLStaticTextView$BlockType;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLStaticTextView$BlockType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLStaticTextView$BlockType;->b:Lflipboard/gui/FLStaticTextView$BlockType;

    const/4 v0, 0x2

    new-array v0, v0, [Lflipboard/gui/FLStaticTextView$BlockType;

    sget-object v1, Lflipboard/gui/FLStaticTextView$BlockType;->a:Lflipboard/gui/FLStaticTextView$BlockType;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLStaticTextView$BlockType;->b:Lflipboard/gui/FLStaticTextView$BlockType;

    aput-object v1, v0, v3

    sput-object v0, Lflipboard/gui/FLStaticTextView$BlockType;->c:[Lflipboard/gui/FLStaticTextView$BlockType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLStaticTextView$BlockType;
    .locals 1

    .prologue
    .line 351
    const-class v0, Lflipboard/gui/FLStaticTextView$BlockType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLStaticTextView$BlockType;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLStaticTextView$BlockType;
    .locals 1

    .prologue
    .line 351
    sget-object v0, Lflipboard/gui/FLStaticTextView$BlockType;->c:[Lflipboard/gui/FLStaticTextView$BlockType;

    invoke-virtual {v0}, [Lflipboard/gui/FLStaticTextView$BlockType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLStaticTextView$BlockType;

    return-object v0
.end method
