.class public Lflipboard/gui/AtomicFitListView;
.super Landroid/view/ViewGroup;
.source "AtomicFitListView.java"


# instance fields
.field protected a:I

.field protected b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 17
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 20
    iput v0, p0, Lflipboard/gui/AtomicFitListView;->a:I

    .line 21
    iput v0, p0, Lflipboard/gui/AtomicFitListView;->b:I

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 18
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    iput v0, p0, Lflipboard/gui/AtomicFitListView;->a:I

    .line 21
    iput v0, p0, Lflipboard/gui/AtomicFitListView;->b:I

    .line 18
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingLeft()I

    move-result v0

    .line 54
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingTop()I

    move-result v3

    .line 56
    invoke-static {}, Lflipboard/util/AndroidUtil;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getWidth()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v2, 0x1

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingRight()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    const/4 v2, 0x2

    iget v4, p0, Lflipboard/gui/AtomicFitListView;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    .line 58
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    iget v2, p0, Lflipboard/gui/AtomicFitListView;->b:I

    sub-int/2addr v0, v2

    :cond_0
    move v2, v0

    .line 63
    :goto_0
    iget v0, p0, Lflipboard/gui/AtomicFitListView;->a:I

    if-ge v1, v0, :cond_1

    .line 64
    invoke-virtual {p0, v1}, Lflipboard/gui/AtomicFitListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 65
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 66
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v5, v2

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v6, v3

    iget v7, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v7, v2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v7, v8

    iget v8, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v8, v3

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 67
    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v5

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v0, v4

    add-int/2addr v2, v0

    .line 63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 69
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 25
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 28
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getChildCount()I

    move-result v0

    iput v0, p0, Lflipboard/gui/AtomicFitListView;->a:I

    move v6, v5

    move v7, v5

    move v3, v5

    .line 29
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getChildCount()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 30
    invoke-virtual {p0, v6}, Lflipboard/gui/AtomicFitListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v0, p0

    move v2, p1

    move v4, p2

    .line 33
    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/AtomicFitListView;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 35
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 36
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v2, v4

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v4

    add-int/2addr v3, v2

    .line 38
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingRight()I

    move-result v4

    add-int/2addr v2, v4

    if-le v2, v8, :cond_1

    .line 40
    iput v6, p0, Lflipboard/gui/AtomicFitListView;->a:I

    .line 41
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    sub-int/2addr v3, v0

    .line 47
    :cond_0
    iput v3, p0, Lflipboard/gui/AtomicFitListView;->b:I

    .line 48
    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingLeft()I

    move-result v0

    add-int/2addr v0, v3

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingRight()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingTop()I

    move-result v1

    add-int/2addr v1, v7

    invoke-virtual {p0}, Lflipboard/gui/AtomicFitListView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/AtomicFitListView;->setMeasuredDimension(II)V

    .line 49
    return-void

    .line 44
    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    invoke-static {v7, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 29
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v7, v1

    goto :goto_0
.end method
