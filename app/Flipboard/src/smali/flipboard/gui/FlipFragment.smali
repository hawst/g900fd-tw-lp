.class public Lflipboard/gui/FlipFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "FlipFragment.java"


# instance fields
.field a:Landroid/widget/GridView;

.field b:Landroid/widget/EditText;

.field c:Lflipboard/gui/actionbar/FLActionBar;

.field private d:Ljava/lang/String;

.field private e:Lflipboard/service/Section;

.field private f:Lflipboard/objs/FeedItem;

.field private g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

.field private h:Lflipboard/gui/FlipFragment$MagazineAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 276
    return-void
.end method

.method public static a(Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/objs/UsageEventV2$FlipItemNavFrom;)Lflipboard/gui/FlipFragment;
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v1, "section_id"

    invoke-virtual {p0}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    const-string v1, "item_id"

    iget-object v2, p1, Lflipboard/objs/FeedItem;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v1, "nav_from"

    invoke-virtual {p2}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    new-instance v1, Lflipboard/gui/FlipFragment;

    invoke-direct {v1}, Lflipboard/gui/FlipFragment;-><init>()V

    .line 63
    invoke-virtual {v1, v0}, Lflipboard/gui/FlipFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v1
.end method

.method static synthetic a(Lflipboard/gui/FlipFragment;)Lflipboard/service/Section;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->e:Lflipboard/service/Section;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/FlipFragment;)Lflipboard/objs/UsageEventV2$FlipItemNavFrom;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    return-object v0
.end method

.method static synthetic c(Lflipboard/gui/FlipFragment;)Lflipboard/objs/FeedItem;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->f:Lflipboard/objs/FeedItem;

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/FlipFragment;)Lflipboard/activities/FlipboardActivity;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 78
    invoke-super {p0, p1}, Lflipboard/activities/FlipboardFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lflipboard/gui/FlipFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 80
    if-eqz v3, :cond_3

    move v2, v1

    :goto_0
    if-eqz v2, :cond_5

    const-string v2, "flip_url"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "section_id"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "item_id"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    .line 81
    const-string v0, "section_id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 82
    const-string v1, "item_id"

    invoke-virtual {v3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    if-eqz v0, :cond_4

    if-eqz v1, :cond_4

    .line 84
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v0}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FlipFragment;->e:Lflipboard/service/Section;

    .line 85
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->e:Lflipboard/service/Section;

    invoke-virtual {v0, v1}, Lflipboard/service/Section;->a(Ljava/lang/String;)Lflipboard/objs/FeedItem;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FlipFragment;->f:Lflipboard/objs/FeedItem;

    .line 86
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->f:Lflipboard/objs/FeedItem;

    iget-object v0, v0, Lflipboard/objs/FeedItem;->al:Ljava/lang/String;

    iput-object v0, p0, Lflipboard/gui/FlipFragment;->d:Ljava/lang/String;

    .line 90
    :goto_2
    const-string v0, "nav_from"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/objs/UsageEventV2$FlipItemNavFrom;->valueOf(Ljava/lang/String;)Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FlipFragment;->g:Lflipboard/objs/UsageEventV2$FlipItemNavFrom;

    .line 92
    :cond_2
    return-void

    :cond_3
    move v2, v0

    .line 80
    goto :goto_0

    .line 88
    :cond_4
    invoke-virtual {p0}, Lflipboard/gui/FlipFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "flip_url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FlipFragment;->d:Ljava/lang/String;

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 105
    const v1, 0x7f030072

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 106
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 107
    new-instance v2, Lflipboard/gui/FlipFragment$MagazineAdapter;

    iget-object v3, p0, Lflipboard/gui/FlipFragment;->d:Ljava/lang/String;

    invoke-direct {v2, p0, v0, v3}, Lflipboard/gui/FlipFragment$MagazineAdapter;-><init>(Lflipboard/gui/FlipFragment;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lflipboard/gui/FlipFragment;->h:Lflipboard/gui/FlipFragment$MagazineAdapter;

    .line 108
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->h:Lflipboard/gui/FlipFragment$MagazineAdapter;

    new-instance v2, Ljava/util/ArrayList;

    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v3, v3, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v3}, Lflipboard/service/User;->q()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v0, Lflipboard/gui/FlipFragment$MagazineAdapter;->a:Ljava/util/List;

    invoke-virtual {v0}, Lflipboard/gui/FlipFragment$MagazineAdapter;->notifyDataSetChanged()V

    .line 109
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->a:Landroid/widget/GridView;

    iget-object v2, p0, Lflipboard/gui/FlipFragment;->h:Lflipboard/gui/FlipFragment$MagazineAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 110
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->a:Landroid/widget/GridView;

    iget-object v2, p0, Lflipboard/gui/FlipFragment;->h:Lflipboard/gui/FlipFragment$MagazineAdapter;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 111
    iget-object v0, p0, Lflipboard/gui/FlipFragment;->c:Lflipboard/gui/actionbar/FLActionBar;

    sget-object v2, Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;->d:Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;

    invoke-virtual {v0, v2}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar$HomeButtonStyle;)Landroid/view/View;

    .line 112
    return-object v1
.end method

.method public onDestroyView()V
    .locals 0

    .prologue
    .line 117
    invoke-super {p0}, Lflipboard/activities/FlipboardFragment;->onDestroyView()V

    .line 118
    invoke-static {p0}, Lbutterknife/ButterKnife;->a(Ljava/lang/Object;)V

    .line 119
    return-void
.end method
