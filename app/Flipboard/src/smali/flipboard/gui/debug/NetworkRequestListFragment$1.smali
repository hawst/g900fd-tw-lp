.class Lflipboard/gui/debug/NetworkRequestListFragment$1;
.super Landroid/widget/BaseAdapter;
.source "NetworkRequestListFragment.java"


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lflipboard/gui/debug/NetworkRequestListFragment;


# direct methods
.method constructor <init>(Lflipboard/gui/debug/NetworkRequestListFragment;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lflipboard/gui/debug/NetworkRequestListFragment$1;->b:Lflipboard/gui/debug/NetworkRequestListFragment;

    iput-object p2, p0, Lflipboard/gui/debug/NetworkRequestListFragment$1;->a:Ljava/util/List;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private a(I)Lflipboard/io/RequestLogEntry;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lflipboard/gui/debug/NetworkRequestListFragment$1;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/io/RequestLogEntry;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lflipboard/gui/debug/NetworkRequestListFragment$1;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lflipboard/gui/debug/NetworkRequestListFragment$1;->a(I)Lflipboard/io/RequestLogEntry;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 57
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 61
    .line 63
    if-nez p2, :cond_0

    .line 64
    iget-object v0, p0, Lflipboard/gui/debug/NetworkRequestListFragment$1;->b:Lflipboard/gui/debug/NetworkRequestListFragment;

    invoke-virtual {v0}, Lflipboard/gui/debug/NetworkRequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f030117

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 65
    new-instance v1, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;

    invoke-direct {v1, v3}, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;-><init>(B)V

    .line 66
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 67
    const v0, 0x7f0a0308

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, v1, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;->a:Lflipboard/gui/FLLabelTextView;

    .line 68
    iget-object v0, v1, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;->a:Lflipboard/gui/FLLabelTextView;

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Lflipboard/gui/FLLabelTextView;->setTextSize(I)V

    .line 69
    const v0, 0x7f0a0309

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLLabelTextView;

    iput-object v0, v1, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;->b:Lflipboard/gui/FLLabelTextView;

    .line 70
    iget-object v0, v1, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;->b:Lflipboard/gui/FLLabelTextView;

    invoke-virtual {v0, v3}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    move-object v0, v1

    .line 75
    :goto_0
    invoke-direct {p0, p1}, Lflipboard/gui/debug/NetworkRequestListFragment$1;->a(I)Lflipboard/io/RequestLogEntry;

    move-result-object v1

    .line 76
    iget-object v2, v0, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;->a:Lflipboard/gui/FLLabelTextView;

    iget-object v3, v1, Lflipboard/io/RequestLogEntry;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    iget-object v0, v0, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;->b:Lflipboard/gui/FLLabelTextView;

    iget-object v2, p0, Lflipboard/gui/debug/NetworkRequestListFragment$1;->b:Lflipboard/gui/debug/NetworkRequestListFragment;

    invoke-virtual {v2}, Lflipboard/gui/debug/NetworkRequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-wide v4, v1, Lflipboard/io/RequestLogEntry;->a:J

    invoke-static {v2, v4, v5}, Lflipboard/util/JavaUtil;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    return-object p2

    .line 72
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/debug/NetworkRequestListFragment$Holder;

    goto :goto_0
.end method
