.class public Lflipboard/gui/debug/NetworkRequestListFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "NetworkRequestListFragment.java"


# instance fields
.field private a:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    .line 132
    return-void
.end method


# virtual methods
.method final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/io/RequestLogEntry;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 45
    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 46
    new-instance v1, Lflipboard/gui/debug/NetworkRequestListFragment$1;

    invoke-direct {v1, p0, v0}, Lflipboard/gui/debug/NetworkRequestListFragment$1;-><init>(Lflipboard/gui/debug/NetworkRequestListFragment;Ljava/util/List;)V

    .line 82
    iget-object v2, p0, Lflipboard/gui/debug/NetworkRequestListFragment;->a:Landroid/widget/ListView;

    invoke-virtual {v2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 83
    iget-object v1, p0, Lflipboard/gui/debug/NetworkRequestListFragment;->a:Landroid/widget/ListView;

    new-instance v2, Lflipboard/gui/debug/NetworkRequestListFragment$2;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/debug/NetworkRequestListFragment$2;-><init>(Lflipboard/gui/debug/NetworkRequestListFragment;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 92
    iget-object v0, p0, Lflipboard/gui/debug/NetworkRequestListFragment;->a:Landroid/widget/ListView;

    new-instance v1, Lflipboard/gui/debug/NetworkRequestListFragment$3;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/debug/NetworkRequestListFragment$3;-><init>(Lflipboard/gui/debug/NetworkRequestListFragment;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 100
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 38
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0300c0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lflipboard/gui/debug/NetworkRequestListFragment;->a:Landroid/widget/ListView;

    .line 39
    sget-object v0, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    iget-object v0, v0, Lflipboard/io/NetworkManager;->d:Ljava/util/List;

    invoke-virtual {p0, v0}, Lflipboard/gui/debug/NetworkRequestListFragment;->a(Ljava/util/List;)V

    .line 40
    iget-object v0, p0, Lflipboard/gui/debug/NetworkRequestListFragment;->a:Landroid/widget/ListView;

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 104
    invoke-super {p0, p1, p2}, Lflipboard/activities/FlipboardFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lflipboard/activities/FlipboardFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 106
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->l()Lflipboard/gui/actionbar/FLActionBar;

    move-result-object v2

    .line 108
    invoke-virtual {v2}, Lflipboard/gui/actionbar/FLActionBar;->getMenu()Lflipboard/gui/actionbar/FLActionBarMenu;

    move-result-object v1

    .line 109
    if-nez v1, :cond_1

    .line 110
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu;

    invoke-direct {v1, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;-><init>(Landroid/content/Context;)V

    move-object v0, v1

    .line 112
    :goto_0
    const-string v1, "Show Flipboard api requests"

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 113
    new-instance v3, Lflipboard/gui/debug/NetworkRequestListFragment$4;

    invoke-direct {v3, p0}, Lflipboard/gui/debug/NetworkRequestListFragment$4;-><init>(Lflipboard/gui/debug/NetworkRequestListFragment;)V

    iput-object v3, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 120
    const-string v1, "Show other requests"

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v1

    .line 121
    new-instance v3, Lflipboard/gui/debug/NetworkRequestListFragment$5;

    invoke-direct {v3, p0}, Lflipboard/gui/debug/NetworkRequestListFragment$5;-><init>(Lflipboard/gui/debug/NetworkRequestListFragment;)V

    iput-object v3, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 128
    invoke-virtual {v2, v0}, Lflipboard/gui/actionbar/FLActionBar;->setMenu(Lflipboard/gui/actionbar/FLActionBarMenu;)V

    .line 130
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
