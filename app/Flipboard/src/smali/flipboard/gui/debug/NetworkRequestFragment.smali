.class public Lflipboard/gui/debug/NetworkRequestFragment;
.super Lflipboard/activities/FlipboardFragment;
.source "NetworkRequestFragment.java"


# instance fields
.field a:Lflipboard/io/RequestLogEntry;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lflipboard/activities/FlipboardFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 30
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09007b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 32
    new-instance v2, Landroid/widget/ScrollView;

    invoke-direct {v2, v0}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/ScrollView;->setPadding(IIII)V

    .line 34
    invoke-virtual {v2, v4}, Landroid/widget/ScrollView;->setFillViewport(Z)V

    .line 35
    new-instance v1, Landroid/widget/HorizontalScrollView;

    invoke-direct {v1, v0}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {v2, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 37
    new-instance v3, Lflipboard/gui/FLTextView;

    invoke-direct {v3, v0}, Lflipboard/gui/FLTextView;-><init>(Landroid/content/Context;)V

    .line 38
    const-string v0, "Parsing and formatting json..."

    invoke-virtual {v3, v0}, Lflipboard/gui/FLTextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    invoke-virtual {v1, v3}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;)V

    .line 40
    new-instance v0, Lflipboard/gui/debug/NetworkRequestFragment$1;

    invoke-direct {v0, p0, v3}, Lflipboard/gui/debug/NetworkRequestFragment$1;-><init>(Lflipboard/gui/debug/NetworkRequestFragment;Lflipboard/gui/FLTextView;)V

    new-array v1, v4, [Ljava/lang/Void;

    const/4 v3, 0x0

    const/4 v4, 0x0

    aput-object v4, v1, v3

    .line 53
    invoke-virtual {v0, v1}, Lflipboard/gui/debug/NetworkRequestFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 55
    return-object v2
.end method
