.class Lflipboard/gui/ProfilePage$1;
.super Ljava/lang/Object;
.source "ProfilePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/gui/ProfilePage;


# direct methods
.method constructor <init>(Lflipboard/gui/ProfilePage;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lflipboard/gui/ProfilePage$1;->a:Lflipboard/gui/ProfilePage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 91
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflipboard/objs/FeedItem;

    .line 92
    if-eqz v4, :cond_1

    .line 93
    iget-object v6, p0, Lflipboard/gui/ProfilePage$1;->a:Lflipboard/gui/ProfilePage;

    const/4 v0, 0x0

    iget-object v1, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v4, Lflipboard/objs/FeedItem;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_4

    new-instance v0, Lflipboard/service/Section;

    iget-object v1, v4, Lflipboard/objs/FeedItem;->d:Ljava/lang/String;

    iget-object v2, v4, Lflipboard/objs/FeedItem;->y:Ljava/lang/String;

    iget-object v3, v4, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    iget-object v4, v4, Lflipboard/objs/FeedItem;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lflipboard/service/Section;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v1, v0

    :goto_0
    invoke-virtual {v6}, Lflipboard/gui/ProfilePage;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    if-eqz v0, :cond_1

    sget-object v2, Lflipboard/io/NetworkManager;->c:Lflipboard/io/NetworkManager;

    invoke-virtual {v2}, Lflipboard/io/NetworkManager;->a()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v6}, Lflipboard/gui/ProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0218

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 95
    :cond_1
    :goto_1
    return-void

    .line 93
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "source"

    const-string v3, "profile"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "originSectionIdentifier"

    iget-object v3, v6, Lflipboard/gui/ProfilePage;->k:Lflipboard/service/Section;

    iget-object v3, v3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "linkType"

    const-string v3, "magazine"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lflipboard/gui/ProfilePage;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v3, v1, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v3, v3, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v2

    if-nez v2, :cond_3

    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v2, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    :cond_3
    invoke-virtual {v6}, Lflipboard/gui/ProfilePage;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method
