.class public abstract Lflipboard/gui/FLViewGroup;
.super Landroid/view/ViewGroup;
.source "FLViewGroup.java"


# instance fields
.field private a:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    .line 25
    return-void
.end method

.method public static a(Landroid/view/View;III)I
    .locals 6

    .prologue
    .line 206
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 207
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 208
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 209
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 210
    const/4 v3, 0x1

    invoke-static {v0, v1, p2, p3, v3}, Lflipboard/gui/FLViewGroup;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)I

    move-result v3

    .line 211
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v4, p1, v4

    .line 212
    sub-int v5, v4, v2

    .line 213
    add-int/2addr v1, v3

    invoke-virtual {p0, v3, v5, v1, v4}, Landroid/view/View;->layout(IIII)V

    .line 214
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    .line 216
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;IIII)I
    .locals 6

    .prologue
    .line 182
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 183
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 184
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 185
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 186
    invoke-static {v0, v1, p2, p3, p4}, Lflipboard/gui/FLViewGroup;->a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)I

    move-result v3

    .line 187
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, p1

    .line 188
    add-int v5, v4, v2

    .line 189
    add-int/2addr v1, v3

    invoke-virtual {p0, v3, v4, v1, v5}, Landroid/view/View;->layout(IIII)V

    .line 190
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    .line 192
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/ViewGroup$MarginLayoutParams;IIII)I
    .locals 2

    .prologue
    .line 222
    and-int/lit8 v0, p4, 0x7

    packed-switch v0, :pswitch_data_0

    .line 231
    :pswitch_0
    sub-int v0, p3, p2

    iget v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v0, v1

    sub-int/2addr v0, p1

    iget v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p2

    iget v1, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, v1

    .line 233
    :goto_0
    return v0

    .line 224
    :pswitch_1
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v0, p2

    .line 225
    goto :goto_0

    .line 227
    :pswitch_2
    iget v0, p0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v0, p3, v0

    sub-int/2addr v0, p1

    .line 228
    goto :goto_0

    .line 222
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static varargs a([Landroid/view/View;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 101
    .line 102
    array-length v4, p0

    move v2, v1

    move v3, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, p0, v2

    .line 103
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v6, 0x8

    if-eq v0, v6, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v6

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v5

    :goto_1
    add-int/2addr v3, v0

    .line 102
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 103
    goto :goto_1

    .line 105
    :cond_1
    return v3
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 29
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 241
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 242
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 44
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lflipboard/gui/FLViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public measureChildWithMargins(Landroid/view/View;IIII)V
    .locals 2

    .prologue
    .line 49
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 50
    invoke-super/range {p0 .. p5}, Landroid/view/ViewGroup;->measureChildWithMargins(Landroid/view/View;IIII)V

    .line 52
    :cond_0
    return-void
.end method

.method public setClipRect(Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 255
    if-eqz p1, :cond_3

    .line 256
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    if-nez v0, :cond_2

    .line 260
    invoke-virtual {p0}, Lflipboard/gui/FLViewGroup;->invalidate()V

    .line 261
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iput-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    goto :goto_0

    .line 263
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->left:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v2, p1, Landroid/graphics/Rect;->top:I

    .line 264
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    .line 265
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget v4, p1, Landroid/graphics/Rect;->bottom:I

    .line 266
    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 263
    invoke-virtual {p0, v0, v1, v2, v3}, Lflipboard/gui/FLViewGroup;->invalidate(IIII)V

    .line 267
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_0

    .line 270
    :cond_3
    iget-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lflipboard/gui/FLViewGroup;->invalidate()V

    .line 272
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLViewGroup;->a:Landroid/graphics/Rect;

    goto :goto_0
.end method
