.class final enum Lflipboard/gui/FLSearchBar$Input;
.super Ljava/lang/Enum;
.source "FLSearchBar.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLSearchBar$Input;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLSearchBar$Input;

.field public static final enum b:Lflipboard/gui/FLSearchBar$Input;

.field public static final enum c:Lflipboard/gui/FLSearchBar$Input;

.field public static final enum d:Lflipboard/gui/FLSearchBar$Input;

.field private static final synthetic e:[Lflipboard/gui/FLSearchBar$Input;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lflipboard/gui/FLSearchBar$Input;

    const-string v1, "ENTER_PRESSED"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLSearchBar$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchBar$Input;->a:Lflipboard/gui/FLSearchBar$Input;

    new-instance v0, Lflipboard/gui/FLSearchBar$Input;

    const-string v1, "TEXT_CHANGED"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLSearchBar$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchBar$Input;->b:Lflipboard/gui/FLSearchBar$Input;

    new-instance v0, Lflipboard/gui/FLSearchBar$Input;

    const-string v1, "DELAY_TIMER_FIRED"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/FLSearchBar$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchBar$Input;->c:Lflipboard/gui/FLSearchBar$Input;

    new-instance v0, Lflipboard/gui/FLSearchBar$Input;

    const-string v1, "SEARCH_RESULTS"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/FLSearchBar$Input;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchBar$Input;->d:Lflipboard/gui/FLSearchBar$Input;

    .line 29
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/gui/FLSearchBar$Input;

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->a:Lflipboard/gui/FLSearchBar$Input;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->b:Lflipboard/gui/FLSearchBar$Input;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->c:Lflipboard/gui/FLSearchBar$Input;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/FLSearchBar$Input;->d:Lflipboard/gui/FLSearchBar$Input;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/gui/FLSearchBar$Input;->e:[Lflipboard/gui/FLSearchBar$Input;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLSearchBar$Input;
    .locals 1

    .prologue
    .line 29
    const-class v0, Lflipboard/gui/FLSearchBar$Input;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchBar$Input;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLSearchBar$Input;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lflipboard/gui/FLSearchBar$Input;->e:[Lflipboard/gui/FLSearchBar$Input;

    invoke-virtual {v0}, [Lflipboard/gui/FLSearchBar$Input;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLSearchBar$Input;

    return-object v0
.end method
