.class public Lflipboard/gui/swipe/SwipeDismissTouchListener;
.super Ljava/lang/Object;
.source "SwipeDismissTouchListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Landroid/view/View;

.field private e:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

.field private f:I

.field private g:F

.field private h:F

.field private i:Z

.field private j:I

.field private k:Landroid/view/VelocityTracker;

.field private l:F


# direct methods
.method public constructor <init>(Landroid/view/View;Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->a:I

    .line 67
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    mul-int/lit8 v1, v1, 0x10

    iput v1, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->b:I

    .line 68
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->c:I

    .line 69
    iput-object p1, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    .line 70
    iput-object p2, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->e:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

    .line 71
    return-void
.end method

.method static synthetic a(Lflipboard/gui/swipe/SwipeDismissTouchListener;)Landroid/view/View;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lflipboard/gui/swipe/SwipeDismissTouchListener;)Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->e:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

    return-object v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 76
    iget v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->l:F

    invoke-virtual {p2, v0, v8}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 78
    iget v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    const/4 v3, 0x2

    if-ge v0, v3, :cond_0

    .line 79
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    .line 82
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 201
    :cond_1
    :goto_0
    return v2

    .line 85
    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->g:F

    .line 86
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->h:F

    .line 87
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->e:Lflipboard/gui/swipe/SwipeDismissTouchListener$DismissCallback;

    .line 88
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    .line 89
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 95
    :pswitch_1
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 96
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->g:F

    sub-float/2addr v0, v3

    .line 100
    iget-object v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v3, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 101
    iget-object v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    const/16 v4, 0x3e8

    invoke-virtual {v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 102
    iget-object v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    .line 103
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    .line 104
    iget-object v5, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v5}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 107
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v7, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    cmpl-float v6, v6, v7

    if-lez v6, :cond_4

    iget-boolean v6, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    if-eqz v6, :cond_4

    .line 109
    cmpl-float v0, v0, v8

    if-lez v0, :cond_3

    move v0, v1

    .line 117
    :goto_1
    if-eqz v1, :cond_a

    .line 119
    iget-object v1, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    if-eqz v0, :cond_9

    iget v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    int-to-float v0, v0

    .line 120
    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 121
    invoke-virtual {v0, v8}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x258

    .line 122
    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lflipboard/gui/swipe/SwipeDismissTouchListener$1;

    invoke-direct {v1, p0}, Lflipboard/gui/swipe/SwipeDismissTouchListener$1;-><init>(Lflipboard/gui/swipe/SwipeDismissTouchListener;)V

    .line 123
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 140
    :cond_2
    :goto_3
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 141
    iput-object v10, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    .line 142
    iput v8, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->l:F

    .line 143
    iput v8, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->g:F

    .line 144
    iput v8, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->h:F

    .line 145
    iput-boolean v2, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 109
    goto :goto_1

    .line 110
    :cond_4
    iget v6, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->b:I

    int-to-float v6, v6

    cmpg-float v6, v6, v4

    if-gtz v6, :cond_d

    iget v6, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->c:I

    int-to-float v6, v6

    cmpg-float v6, v4, v6

    if-gtz v6, :cond_d

    cmpg-float v6, v5, v4

    if-gez v6, :cond_d

    cmpg-float v4, v5, v4

    if-gez v4, :cond_d

    iget-boolean v4, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    if-eqz v4, :cond_d

    .line 114
    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    move v3, v1

    :goto_4
    cmpg-float v0, v0, v8

    if-gez v0, :cond_6

    move v0, v1

    :goto_5
    if-ne v3, v0, :cond_7

    move v0, v1

    .line 115
    :goto_6
    iget-object v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    cmpl-float v3, v3, v8

    if-lez v3, :cond_8

    :goto_7
    move v11, v1

    move v1, v0

    move v0, v11

    goto :goto_1

    :cond_5
    move v3, v2

    .line 114
    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    :cond_7
    move v0, v2

    goto :goto_6

    :cond_8
    move v1, v2

    .line 115
    goto :goto_7

    .line 119
    :cond_9
    iget v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    neg-int v0, v0

    int-to-float v0, v0

    goto :goto_2

    .line 132
    :cond_a
    iget-boolean v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    if-eqz v0, :cond_2

    .line 134
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 135
    invoke-virtual {v0, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 136
    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x258

    .line 137
    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 138
    invoke-virtual {v0, v10}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto :goto_3

    .line 150
    :pswitch_2
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 155
    invoke-virtual {v0, v8}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 156
    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0x258

    .line 157
    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 158
    invoke-virtual {v0, v10}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 159
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 160
    iput-object v10, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    .line 161
    iput v8, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->l:F

    .line 162
    iput v8, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->g:F

    .line 163
    iput v8, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->h:F

    .line 164
    iput-boolean v2, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    goto/16 :goto_0

    .line 169
    :pswitch_3
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 170
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->k:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 174
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->g:F

    sub-float v3, v0, v3

    .line 175
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iget v4, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->h:F

    sub-float/2addr v0, v4

    .line 176
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->a:I

    int-to-float v5, v5

    cmpl-float v4, v4, v5

    if-lez v4, :cond_b

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    cmpg-float v0, v0, v4

    if-gez v0, :cond_b

    .line 177
    iput-boolean v1, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    .line 178
    cmpl-float v0, v3, v8

    if-lez v0, :cond_c

    iget v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->a:I

    :goto_8
    iput v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->j:I

    .line 179
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 182
    invoke-static {p2}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 184
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x3

    .line 183
    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->setAction(I)V

    .line 186
    iget-object v4, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 187
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 190
    :cond_b
    iget-boolean v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->i:Z

    if-eqz v0, :cond_1

    .line 191
    iput v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->l:F

    .line 192
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    iget v2, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->j:I

    int-to-float v2, v2

    sub-float v2, v3, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 194
    iget-object v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->d:Landroid/view/View;

    const/high16 v2, 0x40000000    # 2.0f

    .line 195
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    mul-float/2addr v2, v3

    iget v3, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->f:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float v2, v9, v2

    .line 194
    invoke-static {v9, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v8, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    move v2, v1

    .line 196
    goto/16 :goto_0

    .line 178
    :cond_c
    iget v0, p0, Lflipboard/gui/swipe/SwipeDismissTouchListener;->a:I

    neg-int v0, v0

    goto :goto_8

    :cond_d
    move v0, v2

    move v1, v2

    goto/16 :goto_1

    .line 82
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
