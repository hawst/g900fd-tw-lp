.class public Lflipboard/gui/FollowButton;
.super Landroid/widget/FrameLayout;
.source "FollowButton.java"

# interfaces
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lflipboard/util/Observer",
        "<",
        "Lflipboard/service/User;",
        "Lflipboard/service/User$Message;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/widget/TextView;

.field public b:Lflipboard/gui/FLCameleonImageView;

.field private c:Lflipboard/objs/FeedSectionLink;

.field private d:Landroid/view/View$OnClickListener;

.field private e:Lflipboard/objs/UsageEventV2$FollowFrom;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 46
    invoke-direct {p0, p1}, Lflipboard/gui/FollowButton;->a(Landroid/content/Context;)V

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    invoke-direct {p0, p1}, Lflipboard/gui/FollowButton;->a(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    invoke-direct {p0, p1}, Lflipboard/gui/FollowButton;->a(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FollowButton;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lflipboard/gui/FollowButton;->d:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 61
    const v1, 0x7f030073

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 63
    const v0, 0x7f0a01a4

    invoke-virtual {p0, v0}, Lflipboard/gui/FollowButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0a01a5

    invoke-virtual {p0, v0}, Lflipboard/gui/FollowButton;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLCameleonImageView;

    iput-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    .line 66
    iget-object v0, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    new-instance v1, Lflipboard/gui/FollowButton$1;

    invoke-direct {v1, p0}, Lflipboard/gui/FollowButton$1;-><init>(Lflipboard/gui/FollowButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    new-instance v1, Lflipboard/gui/FollowButton$2;

    invoke-direct {v1, p0}, Lflipboard/gui/FollowButton$2;-><init>(Lflipboard/gui/FollowButton;)V

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FollowButton;Lflipboard/service/Section;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lflipboard/gui/FollowButton;->a(ZLflipboard/service/Section;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/FollowButton;Z)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lflipboard/gui/FollowButton;->setFollowing(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 9

    .prologue
    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 224
    if-eqz p1, :cond_0

    iget-object v4, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    .line 225
    :goto_0
    if-eqz p1, :cond_1

    iget-object v2, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    .line 226
    :goto_1
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v3, v8, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 229
    invoke-virtual {v3, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 230
    new-instance v0, Lflipboard/gui/FollowButton$5;

    invoke-direct {v0, p0, v2}, Lflipboard/gui/FollowButton$5;-><init>(Lflipboard/gui/FollowButton;Landroid/view/View;)V

    invoke-virtual {v3, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 243
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v5, v1, v8}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 244
    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 245
    invoke-virtual {v5, v6, v7}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 246
    new-instance v0, Lflipboard/gui/FollowButton$6;

    invoke-direct {v0, p0, v4}, Lflipboard/gui/FollowButton$6;-><init>(Lflipboard/gui/FollowButton;Landroid/view/View;)V

    invoke-virtual {v5, v0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 258
    sget-object v6, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v0, Lflipboard/gui/FollowButton$7;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/FollowButton$7;-><init>(Lflipboard/gui/FollowButton;Landroid/view/View;Landroid/view/animation/AlphaAnimation;Landroid/view/View;Landroid/view/animation/AlphaAnimation;)V

    invoke-virtual {v6, v0}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 265
    return-void

    .line 224
    :cond_0
    iget-object v4, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    goto :goto_0

    .line 225
    :cond_1
    iget-object v2, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    goto :goto_1
.end method

.method private a(ZLflipboard/service/Section;)V
    .locals 3

    .prologue
    .line 160
    new-instance v1, Lflipboard/objs/UsageEventV2;

    if-eqz p1, :cond_1

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->l:Lflipboard/objs/UsageEventV2$EventAction;

    :goto_0
    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->a:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 161
    invoke-virtual {p2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 162
    iget-object v2, p2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 163
    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FollowButton;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 164
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 167
    invoke-virtual {p2}, Lflipboard/service/Section;->z()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    new-instance v1, Lflipboard/objs/UsageEventV2;

    if-eqz p1, :cond_2

    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->A:Lflipboard/objs/UsageEventV2$EventAction;

    :goto_1
    sget-object v2, Lflipboard/objs/UsageEventV2$EventCategory;->j:Lflipboard/objs/UsageEventV2$EventCategory;

    invoke-direct {v1, v0, v2}, Lflipboard/objs/UsageEventV2;-><init>(Lflipboard/objs/UsageEventV2$EventAction;Lflipboard/objs/UsageEventV2$EventCategory;)V

    sget-object v0, Lflipboard/objs/UsageEventV2$CommonEventData;->a:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 169
    invoke-virtual {p2}, Lflipboard/service/Section;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->k:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 170
    iget-object v2, p2, Lflipboard/service/Section;->r:Lflipboard/service/Section$Meta;

    iget-object v2, v2, Lflipboard/service/Section$Meta;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->r:Lflipboard/objs/UsageEventV2$CommonEventData;

    .line 171
    iget-object v2, p2, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, v2, Lflipboard/objs/TOCSection;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    sget-object v1, Lflipboard/objs/UsageEventV2$CommonEventData;->C:Lflipboard/objs/UsageEventV2$CommonEventData;

    iget-object v2, p0, Lflipboard/gui/FollowButton;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 172
    invoke-virtual {v0, v1, v2}, Lflipboard/objs/UsageEventV2;->a(Lflipboard/objs/UsageEventV2$CommonEventData;Ljava/lang/Object;)Lflipboard/objs/UsageEventV2;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lflipboard/objs/UsageEventV2;->c()V

    .line 175
    :cond_0
    return-void

    .line 160
    :cond_1
    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->m:Lflipboard/objs/UsageEventV2$EventAction;

    goto :goto_0

    .line 168
    :cond_2
    sget-object v0, Lflipboard/objs/UsageEventV2$EventAction;->B:Lflipboard/objs/UsageEventV2$EventAction;

    goto :goto_1
.end method

.method private static a(Lflipboard/objs/FeedSectionLink;)Z
    .locals 2

    .prologue
    .line 198
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, p0, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 199
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lflipboard/service/Section;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setFollowing(Z)V
    .locals 8

    .prologue
    const v7, 0x7f0d004a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 109
    iget-object v2, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    .line 110
    invoke-virtual {v2}, Lflipboard/objs/FeedSectionLink;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0}, Lflipboard/service/User;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    .line 112
    new-instance v1, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v1}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    .line 113
    const v2, 0x7f0d01f0

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    .line 114
    const v2, 0x7f0d01e9

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->f(I)V

    .line 115
    const v2, 0x7f0d023f

    invoke-virtual {v1, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    .line 116
    invoke-virtual {v1, v7}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    .line 117
    new-instance v2, Lflipboard/gui/FollowButton$3;

    invoke-direct {v2, p0, v0}, Lflipboard/gui/FollowButton$3;-><init>(Lflipboard/gui/FollowButton;Lflipboard/activities/FlipboardActivity;)V

    iput-object v2, v1, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    .line 123
    iget-object v0, v0, Landroid/support/v4/app/FragmentActivity;->b:Landroid/support/v4/app/FragmentManagerImpl;

    const-string v2, "login"

    invoke-virtual {v1, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 141
    :goto_0
    return-void

    .line 125
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    iget-object v1, v2, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v0

    .line 126
    if-nez v0, :cond_2

    .line 127
    new-instance v0, Lflipboard/service/Section;

    invoke-direct {v0, v2}, Lflipboard/service/Section;-><init>(Lflipboard/objs/FeedSectionLink;)V

    move-object v1, v0

    .line 129
    :goto_1
    iput-boolean p1, v2, Lflipboard/objs/FeedSectionLink;->o:Z

    .line 130
    if-eqz p1, :cond_1

    .line 131
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v5, v2}, Lflipboard/service/User;->a(Lflipboard/service/Section;ZLjava/lang/String;)Lflipboard/service/Section;

    .line 133
    sget-object v0, Lflipboard/io/UsageManager;->b:Lflipboard/io/UsageManager;

    const-string v2, "sectionAddButtonTapped"

    invoke-virtual {v0, v2}, Lflipboard/io/UsageManager;->a(Ljava/lang/String;)V

    .line 134
    invoke-direct {p0, v6, v1}, Lflipboard/gui/FollowButton;->a(ZLflipboard/service/Section;)V

    goto :goto_0

    .line 136
    :cond_1
    new-instance v3, Lflipboard/gui/dialog/FLAlertDialogFragment;

    invoke-direct {v3}, Lflipboard/gui/dialog/FLAlertDialogFragment;-><init>()V

    const v0, 0x7f0d026f

    invoke-virtual {v3, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->b(I)V

    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d026e

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v2, v2, Lflipboard/objs/FeedSectionLink;->d:Ljava/lang/String;

    aput-object v2, v4, v5

    invoke-static {v0, v4}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lflipboard/gui/dialog/FLDialogFragment;->A:Ljava/lang/String;

    const v0, 0x7f0d0335

    invoke-virtual {v3, v0}, Lflipboard/gui/dialog/FLAlertDialogFragment;->c(I)V

    invoke-virtual {v3, v7}, Lflipboard/gui/dialog/FLAlertDialogFragment;->d(I)V

    new-instance v0, Lflipboard/gui/FollowButton$4;

    invoke-direct {v0, p0, v1}, Lflipboard/gui/FollowButton$4;-><init>(Lflipboard/gui/FollowButton;Lflipboard/service/Section;)V

    iput-object v0, v3, Lflipboard/gui/dialog/FLDialogFragment;->B:Lflipboard/gui/dialog/FLDialogResponse;

    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    const-string v2, "unfollow_confirmation"

    invoke-virtual {v3, v0, v2}, Lflipboard/gui/dialog/FLAlertDialogFragment;->a(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    .line 138
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, v1}, Lflipboard/service/User;->b(Lflipboard/service/Section;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v0}, Lflipboard/gui/FLCameleonImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 292
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 293
    iget-object v1, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    invoke-virtual {v1, v0}, Lflipboard/gui/FLCameleonImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 294
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 37
    check-cast p2, Lflipboard/service/User$Message;

    sget-object v0, Lflipboard/gui/FollowButton$8;->a:[I

    invoke-virtual {p2}, Lflipboard/service/User$Message;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    check-cast p3, Lflipboard/service/Section;

    if-eqz p3, :cond_0

    iget-object v0, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    invoke-virtual {p3, v0}, Lflipboard/service/Section;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lflipboard/service/Section;->s()Z

    move-result v0

    invoke-direct {p0, v0}, Lflipboard/gui/FollowButton;->a(Z)V

    goto :goto_0

    :pswitch_1
    check-cast p3, Lflipboard/service/Section;

    if-eqz p3, :cond_0

    iget-object v0, p3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v0, v0, Lflipboard/objs/TOCSection;->k:Ljava/lang/String;

    iget-object v1, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    iget-object v1, v1, Lflipboard/objs/FeedSectionLink;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p3, Lflipboard/service/Section;->q:Lflipboard/objs/TOCSection;

    iget-object v2, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    iget-object v0, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    iget-boolean v0, v0, Lflipboard/objs/FeedSectionLink;->o:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lflipboard/objs/FeedSectionLink;->o:Z

    iput-boolean v0, v1, Lflipboard/objs/TOCSection;->N:Z

    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lflipboard/activities/FlipboardActivity;

    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0166

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lflipboard/gui/FLToast;->b(Lflipboard/activities/FlipboardActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 183
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 185
    iget-object v0, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    invoke-static {v0}, Lflipboard/gui/FollowButton;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v0

    invoke-direct {p0, v0}, Lflipboard/gui/FollowButton;->a(Z)V

    .line 188
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->b(Lflipboard/util/Observer;)V

    .line 189
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 178
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 179
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v0, p0}, Lflipboard/service/User;->c(Lflipboard/util/Observer;)V

    .line 180
    return-void
.end method

.method public setFrom(Lflipboard/objs/UsageEventV2$FollowFrom;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lflipboard/gui/FollowButton;->e:Lflipboard/objs/UsageEventV2$FollowFrom;

    .line 105
    return-void
.end method

.method public setInverted(Z)V
    .locals 3

    .prologue
    .line 90
    if-eqz p1, :cond_0

    .line 91
    iget-object v0, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    const v1, 0x7f0201bf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 92
    iget-object v0, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080083

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 93
    iget-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f0201b0

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setBackgroundResource(I)V

    .line 94
    iget-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f0800ac

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setDefaultColor(I)V

    .line 101
    :goto_0
    return-void

    .line 96
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    const v1, 0x7f0201c0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 97
    iget-object v0, p0, Lflipboard/gui/FollowButton;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lflipboard/gui/FollowButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080089

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 98
    iget-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f0201b1

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setBackgroundResource(I)V

    .line 99
    iget-object v0, p0, Lflipboard/gui/FollowButton;->b:Lflipboard/gui/FLCameleonImageView;

    const v1, 0x7f080038

    invoke-virtual {v0, v1}, Lflipboard/gui/FLCameleonImageView;->setDefaultColor(I)V

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 204
    if-eqz p1, :cond_0

    .line 206
    iput-object p1, p0, Lflipboard/gui/FollowButton;->d:Landroid/view/View$OnClickListener;

    .line 208
    :cond_0
    return-void
.end method

.method public setSectionLink(Lflipboard/objs/FeedSectionLink;)V
    .locals 1

    .prologue
    .line 192
    iput-object p1, p0, Lflipboard/gui/FollowButton;->c:Lflipboard/objs/FeedSectionLink;

    .line 193
    invoke-static {p1}, Lflipboard/gui/FollowButton;->a(Lflipboard/objs/FeedSectionLink;)Z

    move-result v0

    .line 194
    invoke-direct {p0, v0}, Lflipboard/gui/FollowButton;->a(Z)V

    .line 195
    return-void
.end method
