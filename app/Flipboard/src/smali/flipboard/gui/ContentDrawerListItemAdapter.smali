.class public Lflipboard/gui/ContentDrawerListItemAdapter;
.super Landroid/widget/BaseAdapter;
.source "ContentDrawerListItemAdapter.java"

# interfaces
.implements Lflipboard/gui/EditableAdapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;",
        "Lflipboard/gui/EditableAdapter;"
    }
.end annotation


# static fields
.field public static g:Lflipboard/util/Log;


# instance fields
.field protected a:Lflipboard/activities/FlipboardActivity;

.field protected b:Landroid/view/LayoutInflater;

.field protected c:Lflipboard/objs/ContentDrawerListItem;

.field protected d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation
.end field

.field protected final e:Ljava/lang/String;

.field public f:Z

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    const-string v0, "contentdrawer"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/ContentDrawerListItemAdapter;->g:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Lflipboard/activities/FlipboardActivity;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflipboard/activities/FlipboardActivity;",
            ")V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 75
    iput-object p1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    .line 76
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Lflipboard/activities/FlipboardActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    .line 77
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    .line 81
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->G:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->e:Ljava/lang/String;

    .line 82
    return-void
.end method

.method private a(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1046
    iget-boolean v0, p1, Lflipboard/objs/ConfigBrick;->i:Z

    if-eqz v0, :cond_0

    .line 1047
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    iget-object v0, v0, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    if-eqz v0, :cond_0

    .line 1048
    iget-object v0, p1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    iget-object v0, v0, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    iget-object v0, v0, Lflipboard/objs/Author;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0324

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    iget-object v3, v3, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    iget-object v3, v3, Lflipboard/objs/Author;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1054
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static b(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1068
    iget-boolean v0, p0, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v0, :cond_0

    .line 1069
    iget-object v0, p0, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    if-eqz v0, :cond_0

    .line 1070
    iget-object v0, p0, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    iget-object v0, v0, Lflipboard/objs/ConfigSection;->bP:Ljava/lang/String;

    .line 1074
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method private static d()V
    .locals 2

    .prologue
    .line 1214
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 1215
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only the UI thread is allowed to do this"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1217
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Lflipboard/objs/ContentDrawerListItem;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    return-object v0
.end method

.method public final a(ILflipboard/objs/ContentDrawerListItem;)V
    .locals 1

    .prologue
    .line 1156
    invoke-static {}, Lflipboard/gui/ContentDrawerListItemAdapter;->d()V

    .line 1157
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1158
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1159
    return-void
.end method

.method public final a(Lflipboard/objs/ContentDrawerListItem;)V
    .locals 1

    .prologue
    .line 1120
    invoke-static {}, Lflipboard/gui/ContentDrawerListItemAdapter;->d()V

    .line 1121
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1122
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1123
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lflipboard/objs/ContentDrawerListItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1198
    invoke-static {}, Lflipboard/gui/ContentDrawerListItemAdapter;->d()V

    .line 1199
    if-eqz p1, :cond_2

    .line 1200
    iget-boolean v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->f:Z

    if-eqz v0, :cond_1

    .line 1201
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    instance-of v1, v0, Lflipboard/objs/ConfigFolder;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lflipboard/objs/ConfigFolder;

    iget-object v1, v1, Lflipboard/objs/ConfigFolder;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1203
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1204
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1205
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1210
    :goto_1
    return-void

    .line 1207
    :cond_2
    sget-object v0, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    const-string v1, "Tried to setItems to a null list in content drawer, ignoring"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/Log;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1221
    iput-boolean p1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->h:Z

    .line 1222
    return-void
.end method

.method public final b(Lflipboard/objs/ContentDrawerListItem;)I
    .locals 2

    .prologue
    .line 1141
    invoke-static {}, Lflipboard/gui/ContentDrawerListItemAdapter;->d()V

    .line 1142
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1143
    if-ltz v0, :cond_0

    .line 1144
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1145
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1147
    :cond_0
    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1079
    invoke-static {}, Lflipboard/gui/ContentDrawerListItemAdapter;->d()V

    .line 1080
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    if-nez v0, :cond_0

    .line 1081
    new-instance v0, Lflipboard/objs/SectionListItem;

    invoke-direct {v0}, Lflipboard/objs/SectionListItem;-><init>()V

    iput-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    .line 1082
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f0d01e4

    invoke-virtual {v1, v2}, Lflipboard/activities/FlipboardActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lflipboard/objs/ContentDrawerListItem;->a(Ljava/lang/String;)V

    .line 1083
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1084
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1086
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1090
    invoke-static {}, Lflipboard/gui/ContentDrawerListItemAdapter;->d()V

    .line 1091
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    if-eqz v0, :cond_0

    .line 1092
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1093
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    .line 1094
    invoke-virtual {p0}, Lflipboard/gui/ContentDrawerListItemAdapter;->notifyDataSetChanged()V

    .line 1096
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(I)Lflipboard/objs/ContentDrawerListItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 98
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 7

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 214
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    .line 244
    :goto_0
    return v0

    .line 218
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 219
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v6

    .line 221
    if-ne v6, v2, :cond_2

    move v0, v1

    .line 222
    goto :goto_0

    .line 223
    :cond_2
    if-ne v6, v1, :cond_3

    move v0, v2

    .line 224
    goto :goto_0

    .line 225
    :cond_3
    if-ne v6, v4, :cond_4

    .line 226
    check-cast v0, Lflipboard/objs/ConfigSection;

    iget-object v0, v0, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v0, :cond_a

    .line 227
    const/4 v0, 0x3

    goto :goto_0

    .line 229
    :cond_4
    const/16 v1, 0xa

    if-ne v6, v1, :cond_5

    move v0, v4

    .line 230
    goto :goto_0

    .line 231
    :cond_5
    const/16 v1, 0xb

    if-ne v6, v1, :cond_6

    .line 232
    const/4 v0, 0x5

    goto :goto_0

    .line 233
    :cond_6
    const/16 v1, 0xc

    if-ne v6, v1, :cond_7

    .line 234
    const/4 v0, 0x6

    goto :goto_0

    .line 235
    :cond_7
    if-ne v6, v5, :cond_8

    move v0, v5

    .line 236
    goto :goto_0

    .line 237
    :cond_8
    const/16 v1, 0xe

    if-ne v6, v1, :cond_a

    .line 238
    check-cast v0, Lflipboard/objs/SearchResultHeader;

    iget-object v0, v0, Lflipboard/objs/SearchResultHeader;->c:Lflipboard/objs/SearchResultHeader$HeaderType;

    sget-object v1, Lflipboard/objs/SearchResultHeader$HeaderType;->a:Lflipboard/objs/SearchResultHeader$HeaderType;

    if-ne v0, v1, :cond_9

    .line 239
    const/16 v0, 0x8

    goto :goto_0

    .line 241
    :cond_9
    const/16 v0, 0x9

    goto :goto_0

    :cond_a
    move v0, v3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    .prologue
    .line 250
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1028
    :cond_0
    :goto_0
    return-object p2

    .line 257
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 260
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/16 v2, 0xb

    if-ne v1, v2, :cond_6

    move-object v1, v0

    .line 262
    check-cast v1, Lflipboard/objs/FeedItem;

    .line 266
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 268
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;

    .line 269
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->a()V

    .line 270
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v3}, Lflipboard/gui/FLImageView;->a()V

    .line 280
    :goto_1
    iget-object v3, v1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    if-eqz v3, :cond_2

    .line 281
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->a:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/FeedItem;->M:Lflipboard/objs/Image;

    invoke-virtual {v3, v4}, Lflipboard/gui/FLImageView;->setImage(Lflipboard/objs/Image;)V

    .line 283
    :cond_2
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->b:Lflipboard/gui/FLTextIntf;

    iget-object v4, v1, Lflipboard/objs/FeedItem;->z:Ljava/lang/String;

    invoke-interface {v3, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 284
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->c:Lflipboard/gui/FLTextIntf;

    iget-object v4, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    iget-wide v6, v1, Lflipboard/objs/FeedItem;->Z:J

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-static {v4, v6, v7}, Lflipboard/util/JavaUtil;->b(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v3, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 285
    const/4 v3, 0x0

    .line 286
    iget-object v4, v1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    if-eqz v4, :cond_65

    iget-object v4, v1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_65

    .line 287
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->d:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 288
    iget-object v1, v1, Lflipboard/objs/FeedItem;->aC:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/objs/FeedItem;

    invoke-virtual {v1}, Lflipboard/objs/FeedItem;->f()Ljava/lang/String;

    move-result-object v1

    .line 290
    :goto_2
    if-eqz v1, :cond_5

    .line 291
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->d:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 292
    iget-object v2, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->d:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 1023
    :cond_3
    :goto_3
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->v()Z

    move-result v0

    if-eqz v0, :cond_63

    .line 1024
    const/4 v0, 0x4

    invoke-static {p2, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 272
    :cond_4
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    const v3, 0x7f030051

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 273
    new-instance v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;

    invoke-direct {v3}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;-><init>()V

    .line 274
    const v2, 0x7f0a012e

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->a:Lflipboard/gui/FLImageView;

    .line 275
    const v2, 0x7f0a012f

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->b:Lflipboard/gui/FLTextIntf;

    .line 276
    const v2, 0x7f0a0130

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->c:Lflipboard/gui/FLTextIntf;

    .line 277
    const v2, 0x7f0a0131

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->d:Lflipboard/gui/FLImageView;

    .line 278
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v3

    goto/16 :goto_1

    .line 294
    :cond_5
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderNotification;->d:Lflipboard/gui/FLImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto :goto_3

    .line 296
    :cond_6
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_7

    .line 297
    if-nez p2, :cond_3

    .line 300
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    const v2, 0x7f030050

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_3

    .line 302
    :cond_7
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    .line 304
    if-eqz p2, :cond_a

    .line 306
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;

    move-object v2, v1

    :goto_4
    move-object v1, v0

    .line 314
    check-cast v1, Lflipboard/objs/ContentDrawerListItemHeader;

    .line 315
    iget-object v1, v1, Lflipboard/objs/ContentDrawerListItemHeader;->a:Ljava/lang/String;

    .line 316
    if-eqz v1, :cond_c

    iget-object v3, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d0024

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 317
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    .line 318
    const-string v3, "flipboard"

    invoke-virtual {v1, v3}, Lflipboard/service/User;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 319
    const-string v3, "flipboard"

    invoke-virtual {v1, v3}, Lflipboard/service/User;->b(Ljava/lang/String;)Lflipboard/service/Account;

    move-result-object v3

    .line 320
    iget-object v1, v3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->b:Ljava/lang/String;

    .line 321
    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_9

    .line 322
    :cond_8
    iget-object v1, v3, Lflipboard/service/Account;->b:Lflipboard/objs/UserService;

    iget-object v1, v1, Lflipboard/objs/UserService;->e:Ljava/lang/String;

    .line 324
    :cond_9
    iget-object v2, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 308
    :cond_a
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03004f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 309
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;

    invoke-direct {v2}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;-><init>()V

    .line 310
    const v1, 0x7f0a004f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    .line 311
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_4

    .line 326
    :cond_b
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    const/16 v2, 0x8

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_3

    .line 329
    :cond_c
    iget-object v2, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderHeader;->a:Lflipboard/gui/FLTextIntf;

    move-object v1, v0

    check-cast v1, Lflipboard/objs/ContentDrawerListItemHeader;

    iget-object v1, v1, Lflipboard/objs/ContentDrawerListItemHeader;->bP:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 331
    :cond_d
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_f

    .line 333
    if-eqz p2, :cond_e

    .line 335
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    .line 336
    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    move-object v2, v1

    .line 346
    :goto_5
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    .line 347
    sget-object v3, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 348
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 349
    sget-object v3, Lflipboard/gui/ContentDrawerListItemAdapter;->g:Lflipboard/util/Log;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 350
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 351
    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 352
    iget-object v3, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090040

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 353
    iget-object v3, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v3}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090040

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 355
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->c:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 356
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->e:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 358
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    new-instance v3, Lflipboard/gui/ContentDrawerListItemAdapter$1;

    invoke-direct {v3, p0}, Lflipboard/gui/ContentDrawerListItemAdapter$1;-><init>(Lflipboard/gui/ContentDrawerListItemAdapter;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 366
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->a:Landroid/view/ViewGroup;

    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$2;

    invoke-direct {v2, p0}, Lflipboard/gui/ContentDrawerListItemAdapter$2;-><init>(Lflipboard/gui/ContentDrawerListItemAdapter;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_3

    .line 338
    :cond_e
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03004b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 339
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    invoke-direct {v2}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 340
    const v1, 0x7f0a00be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    .line 341
    const v1, 0x7f0a00c0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->c:Lflipboard/gui/FLTextIntf;

    .line 342
    const v1, 0x7f0a00c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->e:Lflipboard/gui/FLTextIntf;

    .line 343
    const v1, 0x7f0a0126

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->l:Landroid/view/ViewGroup;

    .line 344
    const v1, 0x7f0a0124

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    goto/16 :goto_5

    .line 376
    :cond_f
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2d

    move-object v1, v0

    check-cast v1, Lflipboard/objs/ConfigSection;

    iget-object v1, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v1, :cond_2d

    .line 379
    if-eqz p2, :cond_15

    .line 381
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;

    move-object v3, v1

    :goto_6
    move-object v1, v0

    .line 413
    check-cast v1, Lflipboard/objs/ConfigSection;

    .line 415
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_16

    .line 416
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->m:Lflipboard/gui/FLLabelTextView;

    iget-object v4, v1, Lflipboard/objs/ConfigSection;->h:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->m:Lflipboard/gui/FLLabelTextView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    .line 418
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->n:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 424
    :goto_7
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 425
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v4}, Lflipboard/objs/ConfigBrick;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 426
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->g:Lflipboard/gui/FLStaticTextView;

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_10

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->i:Z

    if-eqz v2, :cond_17

    :cond_10
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    if-eqz v2, :cond_17

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    iget-object v2, v2, Lflipboard/objs/Author;->c:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0d0324

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v1, Lflipboard/objs/ConfigSection;->l:Lflipboard/objs/Author;

    iget-object v7, v7, Lflipboard/objs/Author;->c:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v2, v5}, Lflipboard/util/Format;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_8
    invoke-virtual {v4, v2}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 427
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->d:Lflipboard/gui/FLStaticTextView;

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_18

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->h:Z

    if-nez v2, :cond_18

    const-string v2, ""

    :goto_9
    invoke-virtual {v4, v2}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 429
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->i:Z

    if-nez v2, :cond_11

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v2, :cond_19

    :cond_11
    const/4 v2, 0x1

    .line 430
    :goto_a
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->o:Landroid/view/View;

    if-eqz v2, :cond_1a

    const/4 v2, 0x0

    :goto_b
    invoke-static {v4, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 432
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-lez v2, :cond_12

    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v2, v2, Lflipboard/objs/ConfigBrick;->e:I

    if-lez v2, :cond_12

    .line 433
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 435
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v5

    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v6}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v6

    add-int/2addr v5, v6

    sub-int/2addr v4, v5

    int-to-float v4, v4

    .line 436
    iget-object v5, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v5, v5, Lflipboard/objs/ConfigBrick;->e:I

    int-to-float v5, v5

    .line 437
    iget-object v6, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    iget v6, v6, Lflipboard/objs/ConfigBrick;->f:I

    int-to-float v6, v6

    .line 438
    div-float/2addr v4, v5

    .line 439
    if-nez v2, :cond_1b

    .line 440
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    mul-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    mul-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-direct {v2, v5, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 445
    :goto_c
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v4, v2}, Lflipboard/gui/FLImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 447
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 448
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->g:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 449
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->d:Lflipboard/gui/FLStaticTextView;

    invoke-virtual {v2, v0}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 452
    :cond_12
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v2}, Lflipboard/objs/ConfigBrick;->a()Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 453
    iget-object v1, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    check-cast v1, Lflipboard/objs/DualBrick;

    .line 454
    iget-object v2, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_1e

    .line 455
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 456
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 458
    if-lez v2, :cond_13

    if-lez v4, :cond_13

    .line 459
    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    div-int/lit8 v6, v2, 0x2

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 460
    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    div-int/lit8 v6, v4, 0x2

    iput v6, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 461
    sget-object v5, Lflipboard/gui/ContentDrawerListItemAdapter;->g:Lflipboard/util/Log;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v7}, Lflipboard/gui/FLImageView;->getPaddingLeft()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    .line 462
    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    div-int/lit8 v2, v2, 0x2

    iput v2, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 463
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    div-int/lit8 v4, v4, 0x2

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 465
    :cond_13
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 466
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v4}, Lflipboard/objs/ConfigBrick;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 468
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 469
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    const/16 v4, 0x8

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 471
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-static {v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    iget-object v2, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->i:Z

    if-nez v2, :cond_14

    iget-object v2, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v2, :cond_1c

    :cond_14
    const/4 v2, 0x1

    .line 475
    :goto_d
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->p:Landroid/view/View;

    if-eqz v2, :cond_1d

    const/4 v2, 0x0

    :goto_e
    invoke-static {v4, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 478
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 479
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 480
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v1, v1, Lflipboard/objs/DualBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v1, v1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 383
    :cond_15
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03004c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 384
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;

    invoke-direct {v2}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 385
    const v1, 0x7f0a0121

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f4

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    .line 386
    const v1, 0x7f0a0122

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f4

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    .line 387
    const v1, 0x7f0a0123

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f4

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    .line 389
    const v1, 0x7f0a0121

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f6

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->d:Lflipboard/gui/FLStaticTextView;

    .line 390
    const v1, 0x7f0a0122

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f6

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    .line 391
    const v1, 0x7f0a0123

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f6

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->f:Lflipboard/gui/FLStaticTextView;

    .line 393
    const v1, 0x7f0a0121

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f7

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->g:Lflipboard/gui/FLStaticTextView;

    .line 394
    const v1, 0x7f0a0122

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f7

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    .line 395
    const v1, 0x7f0a0123

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f7

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLStaticTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->i:Lflipboard/gui/FLStaticTextView;

    .line 397
    const v1, 0x7f0a0111

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->j:Landroid/widget/LinearLayout;

    .line 398
    const v1, 0x7f0a0122

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    .line 399
    const v1, 0x7f0a0123

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    .line 401
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    sget-object v3, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 402
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    sget-object v3, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 403
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    sget-object v3, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v1, v3}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 405
    const v1, 0x7f0a0120

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->n:Landroid/view/View;

    .line 406
    const v1, 0x7f0a011f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLLabelTextView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->m:Lflipboard/gui/FLLabelTextView;

    .line 408
    const v1, 0x7f0a0121

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f5

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->o:Landroid/view/View;

    .line 409
    const v1, 0x7f0a0122

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f5

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->p:Landroid/view/View;

    .line 410
    const v1, 0x7f0a0123

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const v3, 0x7f0a00f5

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->q:Landroid/view/View;

    move-object v3, v2

    goto/16 :goto_6

    .line 420
    :cond_16
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->n:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 421
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->m:Lflipboard/gui/FLLabelTextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lflipboard/gui/FLLabelTextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 426
    :cond_17
    const-string v2, ""

    goto/16 :goto_8

    .line 427
    :cond_18
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->bP:Ljava/lang/String;

    goto/16 :goto_9

    .line 429
    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 430
    :cond_1a
    const/16 v2, 0x8

    goto/16 :goto_b

    .line 442
    :cond_1b
    mul-float/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 443
    mul-float/2addr v4, v6

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto/16 :goto_c

    .line 474
    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_d

    .line 475
    :cond_1d
    const/16 v2, 0x8

    goto/16 :goto_e

    .line 482
    :cond_1e
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 483
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 485
    :cond_1f
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v2}, Lflipboard/objs/ConfigBrick;->c()Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 489
    const/4 v2, 0x0

    invoke-virtual {p2, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 491
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 492
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 495
    if-lez v2, :cond_20

    if-lez v4, :cond_20

    .line 496
    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    div-int/lit8 v2, v2, 0x3

    iput v2, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 497
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    div-int/lit8 v4, v4, 0x3

    iget-object v5, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v5}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f090061

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    sub-int/2addr v4, v5

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 498
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 499
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v4}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v5, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 502
    :cond_20
    iget-object v1, v1, Lflipboard/objs/ConfigSection;->b:Lflipboard/objs/ConfigBrick;

    check-cast v1, Lflipboard/objs/TripleBrick;

    .line 503
    iget-object v2, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_27

    iget-object v2, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_27

    .line 506
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 507
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v4}, Lflipboard/objs/ConfigBrick;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 508
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 509
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v4}, Lflipboard/objs/ConfigBrick;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 512
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 513
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 514
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 515
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 517
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 518
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-static {v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 519
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->i:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 520
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->f:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    invoke-static {v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 522
    iget-object v2, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->i:Z

    if-nez v2, :cond_21

    iget-object v2, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v2, :cond_23

    :cond_21
    const/4 v2, 0x1

    .line 523
    :goto_f
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->p:Landroid/view/View;

    if-eqz v2, :cond_24

    const/4 v2, 0x0

    :goto_10
    invoke-static {v4, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 525
    iget-object v2, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->i:Z

    if-nez v2, :cond_22

    iget-object v2, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v2, :cond_25

    :cond_22
    const/4 v2, 0x1

    .line 526
    :goto_11
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->q:Landroid/view/View;

    if-eqz v2, :cond_26

    const/4 v2, 0x0

    :goto_12
    invoke-static {v4, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 529
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 530
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 531
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 532
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 533
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->i:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 534
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->f:Lflipboard/gui/FLStaticTextView;

    iget-object v1, v1, Lflipboard/objs/TripleBrick;->m:Lflipboard/objs/ConfigBrick;

    iget-object v1, v1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 522
    :cond_23
    const/4 v2, 0x0

    goto :goto_f

    .line 523
    :cond_24
    const/16 v2, 0x8

    goto :goto_10

    .line 525
    :cond_25
    const/4 v2, 0x0

    goto :goto_11

    .line 526
    :cond_26
    const/16 v2, 0x8

    goto :goto_12

    .line 536
    :cond_27
    iget-object v2, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    if-eqz v2, :cond_2b

    .line 539
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    .line 540
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-virtual {v4}, Lflipboard/objs/ConfigBrick;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 543
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 544
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 545
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 546
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 549
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-direct {p0, v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->a(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 550
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    invoke-static {v4}, Lflipboard/gui/ContentDrawerListItemAdapter;->b(Lflipboard/objs/ConfigBrick;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 552
    iget-object v2, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->i:Z

    if-nez v2, :cond_28

    iget-object v2, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-boolean v2, v2, Lflipboard/objs/ConfigBrick;->h:Z

    if-eqz v2, :cond_29

    :cond_28
    const/4 v2, 0x1

    .line 553
    :goto_13
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->p:Landroid/view/View;

    if-eqz v2, :cond_2a

    const/4 v2, 0x0

    :goto_14
    invoke-static {v4, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 556
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setTag(Ljava/lang/Object;)V

    .line 557
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->h:Lflipboard/gui/FLStaticTextView;

    iget-object v4, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v4, v4, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v4}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    .line 558
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->e:Lflipboard/gui/FLStaticTextView;

    iget-object v1, v1, Lflipboard/objs/TripleBrick;->l:Lflipboard/objs/ConfigBrick;

    iget-object v1, v1, Lflipboard/objs/ConfigBrick;->k:Lflipboard/objs/ConfigSection;

    invoke-virtual {v2, v1}, Lflipboard/gui/FLStaticTextView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 552
    :cond_29
    const/4 v2, 0x0

    goto :goto_13

    .line 553
    :cond_2a
    const/16 v2, 0x8

    goto :goto_14

    .line 565
    :cond_2b
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    .line 566
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/gui/FLImageView;->a()V

    .line 569
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->b:Lflipboard/gui/FLImageView;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 570
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->c:Lflipboard/gui/FLImageView;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 571
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 572
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 575
    :cond_2c
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->k:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 576
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderBricks;->l:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-static {v1, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_3

    .line 578
    :cond_2d
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_30

    .line 580
    sget-object v1, Lflipboard/gui/ContentDrawerListItemAdapter;->g:Lflipboard/util/Log;

    const/4 v1, 0x2

    new-array v2, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v2, v1

    const/4 v3, 0x1

    if-eqz p2, :cond_2e

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    :goto_15
    aput-object v1, v2, v3

    .line 583
    if-eqz p2, :cond_2f

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    iget-object v1, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    if-lez v1, :cond_2f

    .line 585
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    .line 594
    :goto_16
    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->c:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->n()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 595
    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->e:Lflipboard/gui/FLTextIntf;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 597
    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    new-instance v3, Lflipboard/gui/ContentDrawerListItemAdapter$3;

    invoke-direct {v3, p0}, Lflipboard/gui/ContentDrawerListItemAdapter$3;-><init>(Lflipboard/gui/ContentDrawerListItemAdapter;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 606
    sget-object v2, Lflipboard/gui/ContentDrawerListItemAdapter;->g:Lflipboard/util/Log;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    .line 608
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-lez v2, :cond_3

    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    if-nez v2, :cond_3

    .line 609
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 610
    iget-object v3, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 611
    iget-object v1, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    goto/16 :goto_3

    .line 580
    :cond_2e
    const/4 v1, 0x0

    goto :goto_15

    .line 587
    :cond_2f
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03004e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 588
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    invoke-direct {v2}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 589
    const v1, 0x7f0a00c0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->c:Lflipboard/gui/FLTextIntf;

    .line 590
    const v1, 0x7f0a00c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->e:Lflipboard/gui/FLTextIntf;

    .line 591
    const v1, 0x7f0a012b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    move-object v1, v2

    goto/16 :goto_16

    .line 615
    :cond_30
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/16 v2, 0xe

    if-ne v1, v2, :cond_35

    move-object v1, v0

    .line 617
    check-cast v1, Lflipboard/objs/SearchResultHeader;

    .line 618
    iget-object v2, v1, Lflipboard/objs/SearchResultHeader;->c:Lflipboard/objs/SearchResultHeader$HeaderType;

    sget-object v3, Lflipboard/objs/SearchResultHeader$HeaderType;->a:Lflipboard/objs/SearchResultHeader$HeaderType;

    if-ne v2, v3, :cond_33

    .line 620
    if-eqz p2, :cond_31

    .line 622
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;

    .line 632
    :goto_17
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->d:Lflipboard/gui/FLTextIntf;

    iget-object v4, v1, Lflipboard/objs/SearchResultHeader;->bP:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 634
    sget-object v3, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/objs/SearchResultHeader;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 636
    if-eqz v1, :cond_32

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_32

    .line 637
    iget-object v3, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->c:Lflipboard/gui/FLImageView;

    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 638
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->c:Lflipboard/gui/FLImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 624
    :cond_31
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f030052

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 625
    new-instance v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;

    invoke-direct {v3}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;-><init>()V

    .line 626
    const v2, 0x7f0a0132

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->a:Landroid/view/ViewGroup;

    .line 627
    const v2, 0x7f0a012a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->b:Landroid/view/View;

    .line 628
    const v2, 0x7f0a0133

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLImageView;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->c:Lflipboard/gui/FLImageView;

    .line 629
    const v2, 0x7f0a004f

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->d:Lflipboard/gui/FLTextIntf;

    .line 630
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v3

    goto :goto_17

    .line 640
    :cond_32
    iget-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultHeader;->c:Lflipboard/gui/FLImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_3

    .line 645
    :cond_33
    if-eqz p2, :cond_34

    .line 647
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultSubHeader;

    .line 655
    :goto_18
    iget-object v3, v1, Lflipboard/objs/SearchResultHeader;->bP:Ljava/lang/String;

    .line 656
    iget-object v4, v1, Lflipboard/objs/SearchResultHeader;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_64

    .line 657
    iget-object v1, v1, Lflipboard/objs/SearchResultHeader;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 659
    :goto_19
    iget-object v2, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultSubHeader;->a:Lflipboard/gui/FLTextIntf;

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 649
    :cond_34
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v3, 0x7f030054

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 650
    new-instance v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultSubHeader;

    invoke-direct {v3}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultSubHeader;-><init>()V

    .line 651
    const v2, 0x7f0a004f

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLTextIntf;

    iput-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResultSubHeader;->a:Lflipboard/gui/FLTextIntf;

    .line 652
    invoke-virtual {p2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v2, v3

    goto :goto_18

    .line 663
    :cond_35
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_49

    .line 665
    if-eqz p2, :cond_3c

    .line 667
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;

    .line 668
    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->a:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    move-object v3, v1

    :goto_1a
    move-object v1, v0

    .line 685
    check-cast v1, Lflipboard/objs/SearchResult;

    .line 686
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->b:Lflipboard/gui/FLTextIntf;

    .line 687
    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->d:Lflipboard/gui/FLTextIntf;

    .line 688
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->a:Lflipboard/gui/FLImageView;

    .line 689
    sget-object v2, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v6, v2}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 690
    if-eqz v4, :cond_36

    .line 691
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 692
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v2

    if-eqz v2, :cond_3d

    const v2, 0x7f080022

    :goto_1b
    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-interface {v4, v2}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    .line 694
    :cond_36
    if-eqz v5, :cond_37

    .line 695
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3e

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v2

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3e

    .line 696
    const/4 v2, 0x0

    invoke-static {v5, v2}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 697
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 702
    :cond_37
    :goto_1c
    if-eqz v6, :cond_43

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_43

    .line 703
    const/4 v2, 0x0

    invoke-static {v6, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 706
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v4

    const-string v5, "drawable"

    iget-object v7, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->e:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 708
    if-eqz v2, :cond_3f

    .line 709
    invoke-virtual {v6, v2}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 729
    :cond_38
    :goto_1d
    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 730
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->s()Z

    move-result v4

    if-eqz v4, :cond_42

    .line 731
    iget-object v4, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v4}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09010f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 732
    iget-object v4, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v4}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09010f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 743
    :goto_1e
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->c:Lflipboard/gui/FLTextIntf;

    .line 744
    if-eqz v2, :cond_39

    .line 745
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->p()Ljava/lang/String;

    move-result-object v4

    .line 746
    if-eqz v4, :cond_44

    .line 747
    invoke-interface {v2, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 753
    :cond_39
    :goto_1f
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 756
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->e:Lflipboard/gui/FLImageView;

    .line 757
    if-eqz v4, :cond_3a

    .line 758
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->r()Z

    move-result v5

    if-eqz v5, :cond_45

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_45

    .line 760
    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 761
    const/4 v5, 0x0

    invoke-static {v4, v5}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 768
    :cond_3a
    :goto_20
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    if-eqz v4, :cond_3b

    .line 770
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 772
    iget-object v4, v1, Lflipboard/objs/SearchResult;->h:Lflipboard/service/FLSearchManager$ResultType;

    sget-object v5, Lflipboard/service/FLSearchManager$ResultType;->d:Lflipboard/service/FLSearchManager$ResultType;

    if-ne v4, v5, :cond_46

    .line 773
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 774
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    .line 794
    :cond_3b
    :goto_21
    iget-boolean v1, v1, Lflipboard/objs/SearchResult;->A:Z

    if-eqz v1, :cond_48

    .line 795
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->g:Lflipboard/gui/FLTextIntf;

    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0d030b

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 796
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->g:Lflipboard/gui/FLTextIntf;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_3

    .line 670
    :cond_3c
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f030053

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 671
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;

    invoke-direct {v2}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 672
    const v1, 0x7f0a00c0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->b:Lflipboard/gui/FLTextIntf;

    .line 673
    const v1, 0x7f0a00c1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->c:Lflipboard/gui/FLTextIntf;

    .line 674
    const v1, 0x7f0a00c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->d:Lflipboard/gui/FLTextIntf;

    .line 675
    const v1, 0x7f0a00c2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->e:Lflipboard/gui/FLImageView;

    .line 676
    const v1, 0x7f0a00be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->a:Lflipboard/gui/FLImageView;

    .line 677
    const v1, 0x7f0a0136

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->i:Lflipboard/gui/FLImageView;

    .line 678
    const v1, 0x7f0a0134

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    .line 679
    const v1, 0x7f0a0135

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->g:Lflipboard/gui/FLTextIntf;

    .line 680
    const v1, 0x7f0a011d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->h:Landroid/view/View;

    .line 681
    const v1, 0x7f0a0126

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->j:Landroid/view/ViewGroup;

    .line 682
    const v1, 0x7f0a0124

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->k:Landroid/view/ViewGroup;

    move-object v3, v2

    goto/16 :goto_1a

    .line 692
    :cond_3d
    const v2, 0x7f080023

    goto/16 :goto_1b

    .line 699
    :cond_3e
    const/16 v2, 0x8

    invoke-static {v5, v2}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_1c

    .line 711
    :cond_3f
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v2

    .line 712
    if-eqz v2, :cond_40

    const-string v4, "null"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_40

    .line 713
    invoke-virtual {v6, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 716
    :cond_40
    instance-of v2, v0, Lflipboard/objs/ConfigSection;

    if-eqz v2, :cond_38

    move-object v2, v0

    .line 717
    check-cast v2, Lflipboard/objs/ConfigSection;

    .line 719
    iget-object v4, v2, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    if-eqz v4, :cond_41

    .line 720
    sget-object v4, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v2, v2, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v2

    .line 721
    invoke-virtual {v2}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_1d

    .line 723
    :cond_41
    invoke-virtual {v6}, Lflipboard/gui/FLImageView;->a()V

    goto/16 :goto_1d

    .line 734
    :cond_42
    iget-object v4, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v4}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 735
    iget-object v4, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v4}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0900b7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_1e

    .line 739
    :cond_43
    const/16 v2, 0x8

    invoke-static {v6, v2}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_1e

    .line 749
    :cond_44
    const/4 v4, 0x0

    invoke-interface {v2, v4}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1f

    .line 763
    :cond_45
    const/16 v5, 0x8

    invoke-static {v4, v5}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_20

    .line 775
    :cond_46
    iget-object v2, v1, Lflipboard/objs/SearchResult;->h:Lflipboard/service/FLSearchManager$ResultType;

    sget-object v4, Lflipboard/service/FLSearchManager$ResultType;->e:Lflipboard/service/FLSearchManager$ResultType;

    if-ne v2, v4, :cond_47

    .line 778
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v4, "search_result_fl_icon"

    const-string v5, "drawable"

    iget-object v6, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->e:Ljava/lang/String;

    invoke-virtual {v2, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 779
    if-eqz v2, :cond_3b

    .line 780
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    invoke-virtual {v4, v2}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 781
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->f:Lflipboard/gui/FLImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setVisibility(I)V

    goto/16 :goto_21

    .line 783
    :cond_47
    iget-object v2, v1, Lflipboard/objs/SearchResult;->o:Ljava/lang/String;

    goto/16 :goto_21

    .line 798
    :cond_48
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderSearchResult;->g:Lflipboard/gui/FLTextIntf;

    const/16 v2, 0x8

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setVisibility(I)V

    goto/16 :goto_3

    .line 803
    :cond_49
    if-eqz p2, :cond_52

    .line 805
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    .line 806
    iget-object v2, v1, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v2}, Lflipboard/gui/FLImageView;->a()V

    move-object v3, v1

    .line 824
    :goto_22
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->c:Lflipboard/gui/FLTextIntf;

    .line 825
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->e:Lflipboard/gui/FLTextIntf;

    .line 826
    iget-object v5, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    .line 827
    sget-object v1, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v5, v1}, Lflipboard/gui/FLImageView;->setAlign(Lflipboard/gui/FLImageView$Align;)V

    .line 828
    if-eqz v2, :cond_4a

    .line 829
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 830
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v1

    if-eqz v1, :cond_53

    const v1, 0x7f080022

    :goto_23
    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-interface {v2, v1}, Lflipboard/gui/FLTextIntf;->setTextColor(I)V

    .line 832
    :cond_4a
    if-eqz v4, :cond_4b

    .line 833
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_54

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_54

    .line 834
    const/4 v1, 0x0

    invoke-static {v4, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 835
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->o()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 840
    :cond_4b
    :goto_24
    if-eqz v5, :cond_59

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_59

    .line 841
    const/4 v1, 0x0

    invoke-static {v5, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 844
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v2

    const-string v4, "drawable"

    iget-object v6, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->e:Ljava/lang/String;

    invoke-virtual {v1, v2, v4, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 846
    if-eqz v1, :cond_55

    .line 847
    invoke-virtual {v5, v1}, Lflipboard/gui/FLImageView;->setBitmap(I)V

    .line 868
    :cond_4c
    :goto_25
    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 869
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->s()Z

    move-result v2

    if-eqz v2, :cond_58

    .line 870
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09010f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 871
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f09010f

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 882
    :goto_26
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->d:Lflipboard/gui/FLTextIntf;

    .line 883
    if-eqz v1, :cond_4d

    .line 884
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->p()Ljava/lang/String;

    move-result-object v2

    .line 885
    if-eqz v2, :cond_5a

    .line 886
    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 892
    :cond_4d
    :goto_27
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 895
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->f:Lflipboard/gui/FLImageView;

    .line 896
    if-eqz v2, :cond_4e

    .line 897
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->r()Z

    move-result v4

    if-eqz v4, :cond_5b

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->q()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5b

    .line 899
    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    .line 900
    const/4 v4, 0x0

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 907
    :cond_4e
    :goto_28
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->h:Landroid/view/View;

    .line 908
    iget-object v4, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->k:Lflipboard/gui/FLTextIntf;

    .line 909
    iget-boolean v1, v1, Lflipboard/objs/ConfigService;->J:Z

    if-eqz v1, :cond_5e

    .line 910
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->w()I

    move-result v1

    if-lez v1, :cond_5d

    .line 911
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->w()I

    move-result v1

    const/16 v6, 0x3e8

    if-lt v1, v6, :cond_5c

    const-string v1, "999+"

    :goto_29
    invoke-interface {v4, v1}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    .line 912
    const/4 v1, 0x0

    invoke-static {v4, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 916
    :goto_2a
    const/16 v1, 0x8

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 947
    :goto_2b
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->t()Z

    move-result v1

    if-eqz v1, :cond_4f

    .line 948
    const/16 v1, 0x8

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 950
    :cond_4f
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->u()Z

    move-result v1

    if-eqz v1, :cond_50

    .line 951
    const/16 v1, 0x8

    invoke-static {v5, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 954
    :cond_50
    iget-boolean v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->h:Z

    if-eqz v1, :cond_62

    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->x()Z

    move-result v1

    if-eqz v1, :cond_62

    .line 955
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_51

    .line 956
    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 958
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 959
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 961
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    neg-int v6, v5

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v2, v6, v7, v8, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 962
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 963
    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 964
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 965
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 967
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 968
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 969
    new-instance v7, Landroid/view/animation/TranslateAnimation;

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v6

    int-to-float v2, v2

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v2, v6, v8, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 970
    const-wide/16 v8, 0xc8

    invoke-virtual {v7, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 971
    invoke-virtual {v7, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 972
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 973
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 976
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    neg-int v6, v5

    iget v7, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v2, v6, v7, v8, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 977
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 978
    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 979
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->l:Landroid/view/ViewGroup;

    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 980
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_51

    .line 981
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    neg-int v5, v5

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v1, v5, v1

    int-to-float v1, v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v2, v1, v5, v6, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 982
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 983
    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 984
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 987
    :cond_51
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 808
    :cond_52
    iget-object v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->b:Landroid/view/LayoutInflater;

    const v2, 0x7f03004d

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 809
    new-instance v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;

    invoke-direct {v2}, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;-><init>()V

    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 810
    const v1, 0x7f0a00c0

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->c:Lflipboard/gui/FLTextIntf;

    .line 811
    const v1, 0x7f0a00c1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->d:Lflipboard/gui/FLTextIntf;

    .line 812
    const v1, 0x7f0a00c3

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->e:Lflipboard/gui/FLTextIntf;

    .line 813
    const v1, 0x7f0a00c2

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->f:Lflipboard/gui/FLImageView;

    .line 814
    const v1, 0x7f0a00be

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    .line 815
    const v1, 0x7f0a011d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->g:Landroid/view/View;

    .line 816
    const v1, 0x7f0a0127

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->h:Landroid/view/View;

    .line 817
    const v1, 0x7f0a0125

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    .line 818
    const v1, 0x7f0a0129

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    .line 819
    const v1, 0x7f0a0128

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lflipboard/gui/FLTextIntf;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->k:Lflipboard/gui/FLTextIntf;

    .line 820
    const v1, 0x7f0a0126

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->l:Landroid/view/ViewGroup;

    .line 821
    const v1, 0x7f0a0124

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, v2, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->m:Landroid/view/ViewGroup;

    move-object v3, v2

    goto/16 :goto_22

    .line 830
    :cond_53
    const v1, 0x7f080023

    goto/16 :goto_23

    .line 837
    :cond_54
    const/16 v1, 0x8

    invoke-static {v4, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_24

    .line 849
    :cond_55
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v1

    .line 850
    if-eqz v1, :cond_56

    const-string v2, "null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_56

    .line 851
    invoke-virtual {v5, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    move-object v1, v0

    .line 852
    check-cast v1, Lflipboard/objs/ContentDrawerListItemBase;

    iget-boolean v1, v1, Lflipboard/objs/ContentDrawerListItemBase;->bV:Z

    invoke-virtual {v5, v1}, Lflipboard/gui/FLImageView;->setClipRound(Z)V

    goto/16 :goto_25

    .line 855
    :cond_56
    instance-of v1, v0, Lflipboard/objs/ConfigSection;

    if-eqz v1, :cond_4c

    move-object v1, v0

    .line 856
    check-cast v1, Lflipboard/objs/ConfigSection;

    .line 858
    iget-object v2, v1, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    if-eqz v2, :cond_57

    .line 859
    sget-object v2, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/objs/ConfigSection;->p:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v1

    .line 860
    invoke-virtual {v1}, Lflipboard/objs/ConfigService;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lflipboard/gui/FLImageView;->setImage(Ljava/lang/String;)V

    goto/16 :goto_25

    .line 862
    :cond_57
    invoke-virtual {v5}, Lflipboard/gui/FLImageView;->a()V

    goto/16 :goto_25

    .line 873
    :cond_58
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0900b7

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 874
    iget-object v2, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->a:Lflipboard/activities/FlipboardActivity;

    invoke-virtual {v2}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0900b7

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_26

    .line 878
    :cond_59
    const/16 v1, 0x8

    invoke-static {v5, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_26

    .line 888
    :cond_5a
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lflipboard/gui/FLTextIntf;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_27

    .line 902
    :cond_5b
    const/16 v4, 0x8

    invoke-static {v2, v4}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_28

    .line 911
    :cond_5c
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->w()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_29

    .line 914
    :cond_5d
    const/16 v1, 0x8

    invoke-static {v4, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    goto/16 :goto_2a

    .line 918
    :cond_5e
    const/16 v1, 0x8

    invoke-static {v4, v1}, Lflipboard/util/AndroidUtil;->a(Lflipboard/gui/FLTextIntf;I)V

    .line 920
    const/4 v1, 0x0

    .line 921
    if-eqz v0, :cond_5f

    .line 922
    invoke-static {v0}, Lflipboard/service/Section;->b(Lflipboard/objs/ContentDrawerListItem;)Ljava/lang/String;

    move-result-object v4

    .line 924
    if-eqz v4, :cond_5f

    .line 926
    sget-object v1, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, v1, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-virtual {v1, v4}, Lflipboard/service/User;->d(Ljava/lang/String;)Lflipboard/service/Section;

    move-result-object v1

    .line 927
    if-eqz v1, :cond_60

    invoke-virtual {v1}, Lflipboard/service/Section;->r()Z

    move-result v4

    if-nez v4, :cond_60

    invoke-virtual {v1}, Lflipboard/service/Section;->q()Z

    move-result v1

    if-eqz v1, :cond_60

    const/4 v1, 0x1

    .line 931
    :cond_5f
    :goto_2c
    if-eqz v1, :cond_61

    iget-boolean v1, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->h:Z

    if-nez v1, :cond_61

    .line 934
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 935
    const/4 v1, 0x0

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    .line 936
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_2b

    .line 927
    :cond_60
    const/4 v1, 0x0

    goto :goto_2c

    .line 941
    :cond_61
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    check-cast v1, Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 942
    const/16 v1, 0x8

    invoke-static {v2, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_2b

    .line 989
    :cond_62
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 990
    new-instance v4, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 992
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 993
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v2}, Landroid/widget/ImageButton;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 995
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    const/4 v6, 0x0

    neg-int v7, v5

    iget v8, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v2, v6, v7, v8, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 996
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 997
    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 998
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    invoke-virtual {v6, v2}, Landroid/widget/ImageButton;->startAnimation(Landroid/view/animation/Animation;)V

    .line 999
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->i:Landroid/widget/ImageButton;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1001
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1002
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    .line 1003
    new-instance v7, Landroid/view/animation/TranslateAnimation;

    const/4 v8, 0x0

    iget v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v2, v6

    int-to-float v2, v2

    const/4 v6, 0x0

    const/4 v9, 0x0

    invoke-direct {v7, v8, v2, v6, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1004
    const-wide/16 v8, 0xc8

    invoke-virtual {v7, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1005
    invoke-virtual {v7, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1006
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1007
    iget-object v2, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->j:Landroid/widget/ImageView;

    const/16 v6, 0x8

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1010
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v6, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v6, v5

    int-to-float v6, v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v2, v6, v7, v8, v9}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1011
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1012
    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1013
    iget-object v6, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->l:Landroid/view/ViewGroup;

    invoke-virtual {v6, v2}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1014
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1015
    new-instance v2, Landroid/view/animation/TranslateAnimation;

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v5

    int-to-float v1, v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v2, v1, v5, v6, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    .line 1016
    const-wide/16 v6, 0xc8

    invoke-virtual {v2, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1017
    invoke-virtual {v2, v4}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1018
    iget-object v1, v3, Lflipboard/gui/ContentDrawerListItemAdapter$ViewHolderDefault;->b:Lflipboard/gui/FLImageView;

    invoke-virtual {v1, v2}, Lflipboard/gui/FLImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto/16 :goto_3

    .line 1026
    :cond_63
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    :cond_64
    move-object v1, v3

    goto/16 :goto_19

    :cond_65
    move-object v1, v3

    goto/16 :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 208
    const/16 v0, 0xa

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1101
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1115
    :goto_0
    return v0

    .line 1105
    :cond_1
    iget-object v0, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/objs/ContentDrawerListItem;

    .line 1106
    iget-object v3, p0, Lflipboard/gui/ContentDrawerListItemAdapter;->c:Lflipboard/objs/ContentDrawerListItem;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 1108
    goto :goto_0

    .line 1109
    :cond_2
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v3

    if-ne v3, v2, :cond_3

    move v0, v1

    .line 1111
    goto :goto_0

    .line 1112
    :cond_3
    invoke-interface {v0}, Lflipboard/objs/ContentDrawerListItem;->a()I

    move-result v0

    const/16 v3, 0xe

    if-ne v0, v3, :cond_4

    move v0, v1

    .line 1113
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1115
    goto :goto_0
.end method
