.class Lflipboard/gui/CommentaryList$5;
.super Ljava/lang/Object;
.source "CommentaryList.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lflipboard/objs/CommentaryResult$Item$Commentary;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lflipboard/gui/CommentaryList;


# direct methods
.method constructor <init>(Lflipboard/gui/CommentaryList;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lflipboard/gui/CommentaryList$5;->a:Lflipboard/gui/CommentaryList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 517
    check-cast p1, Lflipboard/objs/CommentaryResult$Item$Commentary;

    check-cast p2, Lflipboard/objs/CommentaryResult$Item$Commentary;

    iget-wide v2, p1, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    iget-wide v4, p2, Lflipboard/objs/CommentaryResult$Item$Commentary;->k:J

    sub-long v4, v2, v4

    iget-object v2, p0, Lflipboard/gui/CommentaryList$5;->a:Lflipboard/gui/CommentaryList;

    iget-object v2, v2, Lflipboard/gui/CommentaryList;->a:Lflipboard/objs/ConfigService;

    iget-boolean v2, v2, Lflipboard/objs/ConfigService;->h:Z

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    cmp-long v3, v4, v6

    if-lez v3, :cond_1

    :goto_1
    mul-int/2addr v0, v2

    return v0

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1
.end method
