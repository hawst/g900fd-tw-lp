.class public Lflipboard/gui/PaginatedPagerTransformer;
.super Ljava/lang/Object;
.source "PaginatedPagerTransformer.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$PageTransformer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 15
    instance-of v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;

    if-eqz v0, :cond_1

    .line 16
    check-cast p1, Lflipboard/gui/item/PaginatedMagazineTile;

    .line 17
    invoke-virtual {p1}, Lflipboard/gui/item/PaginatedMagazineTile;->getWidth()I

    move-result v0

    .line 18
    iget-object v1, p1, Lflipboard/gui/item/PaginatedMagazineTile;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 19
    sub-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    .line 21
    const/4 v1, 0x0

    cmpg-float v1, p2, v1

    if-gez v1, :cond_2

    .line 25
    int-to-float v0, v0

    neg-float v1, p2

    mul-float/2addr v0, v1

    .line 26
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v1

    sub-float v1, v2, v1

    .line 32
    :goto_0
    invoke-virtual {p1, v0}, Lflipboard/gui/item/PaginatedMagazineTile;->setTranslationX(F)V

    .line 35
    iget-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->e:Lflipboard/gui/FLTextView;

    const/high16 v2, 0x3e800000    # 0.25f

    sub-float v2, v1, v2

    invoke-virtual {v0, v2}, Lflipboard/gui/FLTextView;->setAlpha(F)V

    .line 39
    :cond_0
    iget-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    if-eqz v0, :cond_1

    .line 40
    iget-object v0, p1, Lflipboard/gui/item/PaginatedMagazineTile;->f:Lflipboard/gui/FollowButton;

    invoke-virtual {v0, v1}, Lflipboard/gui/FollowButton;->setAlpha(F)V

    .line 45
    :cond_1
    return-void

    .line 28
    :cond_2
    int-to-float v0, v0

    neg-float v1, p2

    mul-float/2addr v0, v1

    .line 29
    sub-float v1, v2, p2

    goto :goto_0
.end method
