.class final enum Lflipboard/gui/FLSearchView$State;
.super Ljava/lang/Enum;
.source "FLSearchView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lflipboard/gui/FLSearchView$State;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lflipboard/gui/FLSearchView$State;

.field public static final enum b:Lflipboard/gui/FLSearchView$State;

.field public static final enum c:Lflipboard/gui/FLSearchView$State;

.field public static final enum d:Lflipboard/gui/FLSearchView$State;

.field private static final synthetic e:[Lflipboard/gui/FLSearchView$State;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 90
    new-instance v0, Lflipboard/gui/FLSearchView$State;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lflipboard/gui/FLSearchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    new-instance v0, Lflipboard/gui/FLSearchView$State;

    const-string v1, "DELAYING"

    invoke-direct {v0, v1, v3}, Lflipboard/gui/FLSearchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$State;->b:Lflipboard/gui/FLSearchView$State;

    new-instance v0, Lflipboard/gui/FLSearchView$State;

    const-string v1, "SEARCHING"

    invoke-direct {v0, v1, v4}, Lflipboard/gui/FLSearchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$State;->c:Lflipboard/gui/FLSearchView$State;

    new-instance v0, Lflipboard/gui/FLSearchView$State;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v5}, Lflipboard/gui/FLSearchView$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lflipboard/gui/FLSearchView$State;->d:Lflipboard/gui/FLSearchView$State;

    .line 89
    const/4 v0, 0x4

    new-array v0, v0, [Lflipboard/gui/FLSearchView$State;

    sget-object v1, Lflipboard/gui/FLSearchView$State;->a:Lflipboard/gui/FLSearchView$State;

    aput-object v1, v0, v2

    sget-object v1, Lflipboard/gui/FLSearchView$State;->b:Lflipboard/gui/FLSearchView$State;

    aput-object v1, v0, v3

    sget-object v1, Lflipboard/gui/FLSearchView$State;->c:Lflipboard/gui/FLSearchView$State;

    aput-object v1, v0, v4

    sget-object v1, Lflipboard/gui/FLSearchView$State;->d:Lflipboard/gui/FLSearchView$State;

    aput-object v1, v0, v5

    sput-object v0, Lflipboard/gui/FLSearchView$State;->e:[Lflipboard/gui/FLSearchView$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lflipboard/gui/FLSearchView$State;
    .locals 1

    .prologue
    .line 89
    const-class v0, Lflipboard/gui/FLSearchView$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lflipboard/gui/FLSearchView$State;

    return-object v0
.end method

.method public static values()[Lflipboard/gui/FLSearchView$State;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lflipboard/gui/FLSearchView$State;->e:[Lflipboard/gui/FLSearchView$State;

    invoke-virtual {v0}, [Lflipboard/gui/FLSearchView$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lflipboard/gui/FLSearchView$State;

    return-object v0
.end method
