.class public Lflipboard/gui/FLBitmapView;
.super Lflipboard/gui/ContainerView;
.source "FLBitmapView.java"

# interfaces
.implements Lflipboard/gui/FLViewIntf;
.implements Lflipboard/util/Observer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lflipboard/gui/ContainerView;",
        "Lflipboard/gui/FLViewIntf;",
        "Lflipboard/util/Observer",
        "<",
        "Ljava/lang/Object;",
        "Lflipboard/gui/flipping/FlipUtil$FlippingMessages;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static V:I

.field private static W:I

.field public static a:Lflipboard/util/Log;

.field static final b:Landroid/graphics/PointF;

.field static final c:Landroid/graphics/Paint;

.field static final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lflipboard/io/Download;",
            "Ljava/lang/ref/Reference",
            "<",
            "Landroid/graphics/Movie;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final v:I


# instance fields
.field private A:Landroid/graphics/RectF;

.field private B:Landroid/graphics/drawable/Drawable;

.field private C:Landroid/graphics/drawable/Drawable;

.field private D:Landroid/graphics/Matrix;

.field private E:Landroid/graphics/PointF;

.field private F:Lflipboard/gui/FLImageView$Align;

.field private G:Z

.field private H:Z

.field private I:Lflipboard/gui/FLImageView;

.field private J:Landroid/graphics/RectF;

.field private K:Landroid/graphics/Paint;

.field private L:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private M:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Z

.field private S:J

.field private T:I

.field private final U:Landroid/animation/Animator$AnimatorListener;

.field e:Landroid/graphics/Point;

.field f:Landroid/graphics/Point;

.field g:Landroid/graphics/Point;

.field h:Z

.field i:Z

.field j:Z

.field protected k:I

.field l:Z

.field m:Landroid/graphics/Movie;

.field n:Lflipboard/io/Download;

.field o:Ljava/util/concurrent/atomic/AtomicInteger;

.field private p:Landroid/animation/TimeInterpolator;

.field private final q:Landroid/graphics/Paint;

.field private final r:Landroid/graphics/Matrix;

.field private final s:Landroid/graphics/RectF;

.field private final t:Landroid/graphics/RectF;

.field private u:F

.field private w:Lflipboard/io/BitmapManager$Handle;

.field private x:Lflipboard/io/BitmapManager$Handle;

.field private y:Landroid/graphics/Rect;

.field private z:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const-string v0, "image"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    .line 62
    new-instance v0, Landroid/graphics/PointF;

    const/high16 v1, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lflipboard/gui/FLBitmapView;->b:Landroid/graphics/PointF;

    .line 63
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 65
    sput-object v0, Lflipboard/gui/FLBitmapView;->c:Landroid/graphics/Paint;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lflipboard/gui/FLBitmapView;->d:Ljava/util/HashMap;

    .line 74
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput v0, Lflipboard/gui/FLBitmapView;->v:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lflipboard/gui/FLImageView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 126
    invoke-direct {p0, p1}, Lflipboard/gui/ContainerView;-><init>(Landroid/content/Context;)V

    .line 60
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->p:Landroid/animation/TimeInterpolator;

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->q:Landroid/graphics/Paint;

    .line 68
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->r:Landroid/graphics/Matrix;

    .line 69
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->s:Landroid/graphics/RectF;

    .line 70
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->t:Landroid/graphics/RectF;

    .line 88
    sget-object v0, Lflipboard/gui/FLImageView$Align;->a:Lflipboard/gui/FLImageView$Align;

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    .line 89
    iput-boolean v1, p0, Lflipboard/gui/FLBitmapView;->h:Z

    .line 90
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->G:Z

    .line 94
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    .line 103
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 127
    iput-object p2, p0, Lflipboard/gui/FLBitmapView;->I:Lflipboard/gui/FLImageView;

    .line 128
    invoke-virtual {p0, v1}, Lflipboard/gui/FLBitmapView;->setWillNotDraw(Z)V

    .line 130
    new-instance v0, Lflipboard/gui/FLBitmapView$1;

    invoke-direct {v0, p0}, Lflipboard/gui/FLBitmapView$1;-><init>(Lflipboard/gui/FLBitmapView;)V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->U:Landroid/animation/Animator$AnimatorListener;

    .line 155
    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLBitmapView;)Lflipboard/io/BitmapManager$Handle;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    return-object v0
.end method

.method private a(Lflipboard/gui/FLBitmapView$LayerType;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1028
    const/4 v0, 0x0

    .line 1029
    sget-object v1, Lflipboard/gui/FLBitmapView$11;->b:[I

    invoke-virtual {p1}, Lflipboard/gui/FLBitmapView$LayerType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1033
    :goto_0
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getLayerType()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1034
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLBitmapView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1036
    :cond_0
    return-void

    .line 1030
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1031
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 1029
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lflipboard/gui/FLBitmapView;Lflipboard/gui/FLBitmapView$LayerType;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView$LayerType;)V

    return-void
.end method

.method static synthetic a(Lflipboard/gui/FLBitmapView;Lflipboard/io/BitmapManager$Handle;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lflipboard/gui/FLBitmapView;->b(Lflipboard/io/BitmapManager$Handle;)V

    return-void
.end method

.method static synthetic b(Lflipboard/gui/FLBitmapView;)Lflipboard/io/BitmapManager$Handle;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    return-object v0
.end method

.method private declared-synchronized b(Lflipboard/io/BitmapManager$Handle;)V
    .locals 2

    .prologue
    .line 927
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->H:Z

    if-nez v0, :cond_0

    .line 928
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->H:Z

    .line 929
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLBitmapView$7;

    invoke-direct {v1, p0, p1}, Lflipboard/gui/FLBitmapView$7;-><init>(Lflipboard/gui/FLBitmapView;Lflipboard/io/BitmapManager$Handle;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 938
    :cond_0
    monitor-exit p0

    return-void

    .line 927
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lflipboard/gui/FLBitmapView;)Lflipboard/io/BitmapManager$Handle;
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lflipboard/gui/FLBitmapView;->e()Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lflipboard/gui/FLBitmapView;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private declared-synchronized e()Lflipboard/io/BitmapManager$Handle;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 544
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_2

    .line 545
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    :cond_1
    :goto_0
    monitor-exit p0

    return-object v0

    .line 547
    :cond_2
    :try_start_1
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    aput-object v2, v0, v1

    .line 549
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    .line 550
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingBottom()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v1, v2

    .line 552
    if-lez v0, :cond_3

    if-gtz v1, :cond_4

    .line 553
    :cond_3
    sget-object v2, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v0

    .line 554
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    goto :goto_0

    .line 558
    :cond_4
    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v2}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v2

    .line 559
    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v3}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v3

    .line 560
    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->f:Landroid/graphics/Point;

    if-nez v4, :cond_9

    .line 561
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v4, p0, Lflipboard/gui/FLBitmapView;->f:Landroid/graphics/Point;

    .line 565
    :goto_1
    new-instance v4, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    .line 566
    new-instance v4, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v7

    add-int/2addr v7, v0

    int-to-float v7, v7

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v8

    add-int/2addr v8, v1

    int-to-float v8, v8

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    .line 568
    sget-object v4, Lflipboard/gui/FLBitmapView$11;->a:[I

    iget-object v5, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    invoke-virtual {v5}, Lflipboard/gui/FLImageView$Align;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 603
    :goto_2
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    .line 608
    iget-boolean v1, p0, Lflipboard/gui/FLBitmapView;->G:Z

    if-nez v1, :cond_5

    sget v1, Lflipboard/gui/FLBitmapView;->V:I

    if-gt v2, v1, :cond_5

    sget v1, Lflipboard/gui/FLBitmapView;->W:I

    if-le v3, v1, :cond_6

    .line 609
    :cond_5
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_c

    .line 610
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    const v2, 0x47435000    # 50000.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    .line 612
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0, v1}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/BitmapManager$Handle;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    .line 613
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lflipboard/io/BitmapManager$Handle;->a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 614
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v5, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    .line 624
    :cond_6
    :goto_3
    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    .line 626
    if-nez v0, :cond_7

    .line 627
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0, v1}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/BitmapManager$Handle;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 628
    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    .line 630
    :cond_7
    if-eqz v0, :cond_1

    .line 631
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->g:Landroid/graphics/Point;

    if-nez v1, :cond_d

    .line 632
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v2

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    iput-object v1, p0, Lflipboard/gui/FLBitmapView;->g:Landroid/graphics/Point;

    .line 636
    :goto_4
    iget-boolean v1, p0, Lflipboard/gui/FLBitmapView;->h:Z

    if-eqz v1, :cond_8

    .line 637
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->s:Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    iput v1, p0, Lflipboard/gui/FLBitmapView;->u:F

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->t:Landroid/graphics/RectF;

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->r:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->r:Landroid/graphics/Matrix;

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->t:Landroid/graphics/RectF;

    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    sget-object v4, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    new-instance v1, Landroid/graphics/BitmapShader;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    move-result-object v2

    sget-object v3, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v4, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v1, v2, v3, v4}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->r:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->q:Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->q:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 639
    :cond_8
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-direct {p0, v1}, Lflipboard/gui/FLBitmapView;->b(Lflipboard/io/BitmapManager$Handle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 544
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 563
    :cond_9
    :try_start_2
    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->f:Landroid/graphics/Point;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_1

    .line 570
    :pswitch_0
    int-to-float v4, v2

    int-to-float v5, v3

    div-float/2addr v4, v5

    .line 571
    int-to-float v5, v0

    int-to-float v6, v1

    div-float/2addr v5, v6

    .line 572
    cmpl-float v4, v4, v5

    if-lez v4, :cond_a

    .line 574
    mul-int/2addr v0, v3

    div-int/2addr v0, v1

    .line 575
    const/4 v4, 0x0

    sub-int v5, v2, v0

    int-to-float v5, v5

    sub-int v6, v2, v0

    int-to-float v6, v6

    iget-object v7, p0, Lflipboard/gui/FLBitmapView;->E:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->x:F

    mul-float/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    float-to-int v4, v4

    .line 576
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    add-int/2addr v0, v4

    invoke-direct {v5, v4, v6, v0, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    .line 577
    new-instance v0, Landroid/graphics/RectF;

    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    neg-int v4, v4

    mul-int/2addr v4, v1

    div-int/2addr v4, v3

    int-to-float v4, v4

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    neg-int v6, v6

    add-int/2addr v6, v2

    mul-int/2addr v6, v1

    div-int/2addr v6, v3

    int-to-float v6, v6

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v7

    add-int/2addr v1, v7

    int-to-float v1, v1

    invoke-direct {v0, v4, v5, v6, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->A:Landroid/graphics/RectF;

    goto/16 :goto_2

    .line 580
    :cond_a
    mul-int/2addr v1, v2

    div-int/2addr v1, v0

    .line 581
    const/4 v4, 0x0

    sub-int v5, v3, v1

    int-to-float v5, v5

    sub-int v6, v3, v1

    int-to-float v6, v6

    iget-object v7, p0, Lflipboard/gui/FLBitmapView;->E:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    mul-float/2addr v6, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    float-to-int v4, v4

    .line 582
    new-instance v5, Landroid/graphics/Rect;

    const/4 v6, 0x0

    add-int/2addr v1, v4

    invoke-direct {v5, v6, v4, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v5, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    .line 583
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    neg-int v5, v5

    mul-int/2addr v5, v0

    div-int/2addr v5, v2

    int-to-float v5, v5

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v6

    add-int/2addr v6, v0

    int-to-float v6, v6

    iget-object v7, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    neg-int v7, v7

    add-int/2addr v7, v3

    mul-int/2addr v0, v7

    div-int/2addr v0, v2

    int-to-float v0, v0

    invoke-direct {v1, v4, v5, v6, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lflipboard/gui/FLBitmapView;->A:Landroid/graphics/RectF;

    goto/16 :goto_2

    .line 588
    :pswitch_1
    int-to-float v4, v2

    int-to-float v5, v3

    div-float/2addr v4, v5

    .line 589
    int-to-float v5, v0

    int-to-float v6, v1

    div-float/2addr v5, v6

    .line 591
    cmpg-float v4, v4, v5

    if-gez v4, :cond_b

    .line 593
    int-to-float v4, v1

    int-to-float v5, v3

    div-float/2addr v4, v5

    .line 594
    int-to-float v5, v2

    mul-float/2addr v5, v4

    float-to-int v5, v5

    sub-int/2addr v0, v5

    div-int/lit8 v0, v0, 0x2

    .line 595
    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, v0

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v7

    int-to-float v7, v7

    int-to-float v0, v0

    int-to-float v8, v2

    mul-float/2addr v4, v8

    add-float/2addr v0, v4

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingTop()I

    move-result v4

    add-int/2addr v1, v4

    int-to-float v1, v1

    invoke-direct {v5, v6, v7, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    .line 602
    :goto_5
    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->A:Landroid/graphics/RectF;

    goto/16 :goto_2

    .line 598
    :cond_b
    int-to-float v4, v0

    int-to-float v5, v2

    div-float/2addr v4, v5

    .line 599
    int-to-float v5, v3

    mul-float/2addr v5, v4

    float-to-int v5, v5

    sub-int/2addr v1, v5

    div-int/lit8 v1, v1, 0x2

    .line 600
    new-instance v5, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v6

    int-to-float v6, v6

    int-to-float v7, v1

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getPaddingLeft()I

    move-result v8

    add-int/2addr v0, v8

    int-to-float v0, v0

    int-to-float v1, v1

    int-to-float v8, v3

    mul-float/2addr v4, v8

    add-float/2addr v1, v4

    invoke-direct {v5, v6, v7, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v5, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    goto :goto_5

    .line 617
    :cond_c
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0, v1}, Lflipboard/io/BitmapManager;->a(Lflipboard/io/BitmapManager$Handle;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    .line 618
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Lflipboard/io/BitmapManager$Handle;->a(Landroid/graphics/Rect;F)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    .line 619
    if-eqz v0, :cond_6

    .line 620
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v4

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    goto/16 :goto_3

    .line 634
    :cond_d
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->g:Landroid/graphics/Point;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v2

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Point;->set(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_4

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic e(Lflipboard/gui/FLBitmapView;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    return-object v0
.end method

.method static synthetic f(Lflipboard/gui/FLBitmapView;)Lflipboard/io/Download;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    return-object v0
.end method

.method private declared-synchronized f()V
    .locals 3

    .prologue
    .line 675
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    const/4 v1, -0x4

    if-lt v0, v1, :cond_1

    iget v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    const/4 v1, 0x6

    if-gt v0, v1, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 676
    :cond_0
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lflipboard/gui/FLBitmapView;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    .line 677
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    new-instance v1, Lflipboard/gui/FLBitmapView$4;

    invoke-direct {v1, p0}, Lflipboard/gui/FLBitmapView$4;-><init>(Lflipboard/gui/FLBitmapView;)V

    iget-object v0, v0, Lflipboard/io/BitmapManager;->e:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 692
    :goto_0
    monitor-exit p0

    return-void

    .line 690
    :cond_1
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-direct {p0, v0}, Lflipboard/gui/FLBitmapView;->b(Lflipboard/io/BitmapManager$Handle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic g(Lflipboard/gui/FLBitmapView;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->M:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    .line 1107
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FlBitmapView:reanimateMovie"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 1108
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->R:Z

    if-eqz v0, :cond_0

    .line 1109
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->R:Z

    .line 1110
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    if-eqz v0, :cond_0

    .line 1111
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1112
    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    invoke-virtual {v2}, Landroid/graphics/Movie;->duration()I

    move-result v2

    int-to-long v2, v2

    rem-long/2addr v0, v2

    long-to-int v0, v0

    .line 1113
    iget v1, p0, Lflipboard/gui/FLBitmapView;->T:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    iput-wide v0, p0, Lflipboard/gui/FLBitmapView;->S:J

    .line 1114
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->invalidate()V

    .line 1117
    :cond_0
    return-void
.end method

.method static synthetic h(Lflipboard/gui/FLBitmapView;)Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->O:Z

    return v0
.end method

.method static synthetic i(Lflipboard/gui/FLBitmapView;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lflipboard/gui/FLBitmapView;->g()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 282
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->o:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 283
    invoke-virtual {p0, v2, v2, v2}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/io/BitmapManager$Handle;Landroid/graphics/PointF;Landroid/graphics/Point;)V

    .line 284
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->q:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 285
    iput-object v2, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    .line 286
    iput-object v2, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    .line 287
    sget-object v0, Lflipboard/gui/FLBitmapView$LayerType;->a:Lflipboard/gui/FLBitmapView$LayerType;

    invoke-direct {p0, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView$LayerType;)V

    .line 288
    iput-object v2, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    .line 289
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->L:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->L:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 291
    iput-object v2, p0, Lflipboard/gui/FLBitmapView;->L:Landroid/os/AsyncTask;

    .line 293
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 270
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->J:Landroid/graphics/RectF;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->K:Landroid/graphics/Paint;

    if-eq v0, p2, :cond_1

    .line 271
    :cond_0
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->J:Landroid/graphics/RectF;

    .line 272
    iput-object p2, p0, Lflipboard/gui/FLBitmapView;->K:Landroid/graphics/Paint;

    .line 273
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->postInvalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :cond_1
    monitor-exit p0

    return-void

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Lflipboard/io/BitmapManager$Handle;)V
    .locals 3

    .prologue
    .line 941
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lflipboard/gui/FLBitmapView;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 942
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->I:Lflipboard/gui/FLImageView;

    invoke-virtual {v0, p1}, Lflipboard/gui/FLImageView;->a(Lflipboard/io/BitmapManager$Handle;)V

    .line 944
    return-void
.end method

.method public final declared-synchronized a(Lflipboard/io/BitmapManager$Handle;Landroid/graphics/PointF;Landroid/graphics/Point;)V
    .locals 3

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLBitmapView:setBitmap"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 314
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 315
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    if-ne v0, p1, :cond_0

    .line 316
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    :goto_0
    monitor-exit p0

    return-void

    .line 319
    :cond_0
    :try_start_1
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_1

    .line 320
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 321
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    .line 323
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 325
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    .line 328
    :cond_2
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    .line 329
    if-eqz p2, :cond_4

    :goto_1
    iput-object p2, p0, Lflipboard/gui/FLBitmapView;->E:Landroid/graphics/PointF;

    .line 330
    if-nez p3, :cond_3

    if-nez p1, :cond_5

    :cond_3
    :goto_2
    invoke-virtual {p0, p3}, Lflipboard/gui/FLBitmapView;->setSize(Landroid/graphics/Point;)V

    .line 331
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->H:Z

    .line 333
    if-nez p1, :cond_6

    .line 335
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->postInvalidate()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 329
    :cond_4
    :try_start_2
    sget-object p2, Lflipboard/gui/FLBitmapView;->b:Landroid/graphics/PointF;

    goto :goto_1

    .line 330
    :cond_5
    new-instance p3, Landroid/graphics/Point;

    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->h()I

    move-result v0

    invoke-virtual {p1}, Lflipboard/io/BitmapManager$Handle;->i()I

    move-result v1

    invoke-direct {p3, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_2

    .line 337
    :cond_6
    invoke-direct {p0}, Lflipboard/gui/FLBitmapView;->f()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final a(Lflipboard/io/Download;Landroid/graphics/Point;)V
    .locals 2

    .prologue
    .line 1040
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->M:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1041
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    .line 1042
    iput-object p2, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    .line 1043
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->d()V

    .line 1044
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->N:Z

    if-eqz v0, :cond_1

    .line 1045
    iget v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    if-ltz v0, :cond_0

    iget v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    sget v1, Lflipboard/gui/FLBitmapView;->v:I

    if-gt v0, v1, :cond_0

    .line 1046
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->b()V

    .line 1048
    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->P:Z

    if-nez v0, :cond_1

    .line 1049
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->P:Z

    .line 1050
    const-class v0, Lflipboard/gui/flipping/FlippingContainer;

    new-instance v1, Lflipboard/gui/FLBitmapView$8;

    invoke-direct {v1, p0}, Lflipboard/gui/FLBitmapView$8;-><init>(Lflipboard/gui/FLBitmapView;)V

    invoke-static {p0, v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Ljava/lang/Class;Lflipboard/util/Callback;)V

    .line 1061
    :cond_1
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 57
    check-cast p2, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    sget-object v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->a:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    if-ne p2, v0, :cond_1

    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->R:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->R:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lflipboard/gui/flipping/FlipUtil$FlippingMessages;->b:Lflipboard/gui/flipping/FlipUtil$FlippingMessages;

    if-ne p2, v0, :cond_0

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/FLBitmapView$10;

    invoke-direct {v1, p0}, Lflipboard/gui/FLBitmapView$10;-><init>(Lflipboard/gui/FLBitmapView;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final declared-synchronized a(ZI)V
    .locals 3

    .prologue
    .line 498
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    .line 499
    iput p2, p0, Lflipboard/gui/FLBitmapView;->k:I

    .line 501
    iput-boolean p1, p0, Lflipboard/gui/FLBitmapView;->O:Z

    .line 503
    if-eqz p1, :cond_5

    .line 504
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    if-nez v0, :cond_0

    .line 505
    invoke-direct {p0}, Lflipboard/gui/FLBitmapView;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 537
    :goto_0
    monitor-exit p0

    return-void

    .line 507
    :cond_0
    :try_start_1
    iget v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    if-nez v0, :cond_1

    .line 508
    invoke-direct {p0}, Lflipboard/gui/FLBitmapView;->g()V

    .line 509
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v0

    if-lez v0, :cond_1

    .line 510
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->invalidate()V

    .line 513
    :cond_1
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    sget v1, Lflipboard/gui/FLBitmapView;->v:I

    if-gt v0, v1, :cond_3

    .line 514
    if-nez p2, :cond_2

    .line 515
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 517
    :cond_2
    :try_start_2
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const/16 v1, 0x1f4

    new-instance v2, Lflipboard/gui/FLBitmapView$3;

    invoke-direct {v2, p0}, Lflipboard/gui/FLBitmapView$3;-><init>(Lflipboard/gui/FLBitmapView;)V

    invoke-virtual {v0, v1, v2}, Lflipboard/service/FlipboardManager;->a(ILjava/lang/Runnable;)V

    goto :goto_0

    .line 526
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    .line 527
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    if-eqz v0, :cond_4

    .line 528
    invoke-static {p0}, Lflipboard/gui/flipping/FlipUtil;->b(Lflipboard/util/Observer;)V

    .line 529
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    .line 531
    :cond_4
    sget-object v0, Lflipboard/gui/FLBitmapView$LayerType;->a:Lflipboard/gui/FLBitmapView$LayerType;

    invoke-direct {p0, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView$LayerType;)V

    goto :goto_0

    .line 535
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method final b()V
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->L:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->M:Ljava/util/concurrent/atomic/AtomicBoolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->M:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 697
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->L:Landroid/os/AsyncTask;

    invoke-static {v0}, Lflipboard/util/AndroidUtil;->a(Landroid/os/AsyncTask;)V

    .line 699
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d()V
    .locals 1

    .prologue
    .line 1065
    new-instance v0, Lflipboard/gui/FLBitmapView$9;

    invoke-direct {v0, p0}, Lflipboard/gui/FLBitmapView$9;-><init>(Lflipboard/gui/FLBitmapView;)V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->L:Landroid/os/AsyncTask;

    .line 1086
    return-void
.end method

.method public getAlign()Lflipboard/gui/FLImageView$Align;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    return-object v0
.end method

.method public getForeground()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->C:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getImageMatrix()Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public getImageRect()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->A:Landroid/graphics/RectF;

    return-object v0
.end method

.method public getPlaceholder()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getScaling()Z
    .locals 1

    .prologue
    .line 202
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->G:Z

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 952
    const/4 v0, 0x0

    return v0
.end method

.method protected declared-synchronized onAttachedToWindow()V
    .locals 3

    .prologue
    .line 436
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lflipboard/gui/FLBitmapView;->k:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 437
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->N:Z

    .line 439
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    if-nez v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;

    if-eqz v0, :cond_2

    :cond_0
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->P:Z

    if-nez v0, :cond_2

    .line 440
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->P:Z

    .line 441
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    if-nez v0, :cond_1

    .line 442
    invoke-static {p0}, Lflipboard/gui/flipping/FlipUtil;->a(Lflipboard/util/Observer;)V

    .line 443
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    .line 445
    :cond_1
    const-class v0, Lflipboard/gui/flipping/FlippingContainer;

    new-instance v1, Lflipboard/gui/FLBitmapView$2;

    invoke-direct {v1, p0}, Lflipboard/gui/FLBitmapView$2;-><init>(Lflipboard/gui/FLBitmapView;)V

    invoke-static {p0, v0, v1}, Lflipboard/util/AndroidUtil;->a(Landroid/view/View;Ljava/lang/Class;Lflipboard/util/Callback;)V

    .line 455
    :cond_2
    invoke-super {p0}, Lflipboard/gui/ContainerView;->onAttachedToWindow()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    monitor-exit p0

    return-void

    .line 436
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onDetachedFromWindow()V
    .locals 3

    .prologue
    .line 466
    monitor-enter p0

    :try_start_0
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    .line 467
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->N:Z

    .line 469
    invoke-super {p0}, Lflipboard/gui/ContainerView;->onDetachedFromWindow()V

    .line 470
    const/high16 v0, -0x80000000

    iput v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    .line 472
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_1

    .line 473
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->j:Z

    if-eqz v0, :cond_0

    .line 474
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0, v1}, Lflipboard/io/BitmapManager;->c(Lflipboard/io/BitmapManager$Handle;)V

    .line 476
    :cond_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 477
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    .line 479
    :cond_1
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_3

    .line 480
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->j:Z

    if-eqz v0, :cond_2

    .line 481
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0, v1}, Lflipboard/io/BitmapManager;->c(Lflipboard/io/BitmapManager$Handle;)V

    .line 483
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 484
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    .line 486
    :cond_3
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    if-eqz v0, :cond_4

    .line 487
    invoke-static {p0}, Lflipboard/gui/flipping/FlipUtil;->b(Lflipboard/util/Observer;)V

    .line 488
    const/4 v0, 0x0

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    :cond_4
    monitor-exit p0

    return-void

    .line 466
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 785
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 787
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMaximumBitmapWidth()I

    move-result v1

    sput v1, Lflipboard/gui/FLBitmapView;->V:I

    .line 788
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMaximumBitmapHeight()I

    move-result v1

    sput v1, Lflipboard/gui/FLBitmapView;->W:I

    .line 791
    :cond_0
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v1

    if-nez v1, :cond_1

    .line 792
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    .line 793
    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 794
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 797
    :cond_1
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    if-eqz v1, :cond_7

    .line 798
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    invoke-virtual {v0}, Landroid/graphics/Movie;->duration()I

    move-result v0

    .line 799
    if-lez v0, :cond_3

    .line 800
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 801
    iget-boolean v2, p0, Lflipboard/gui/FLBitmapView;->R:Z

    if-nez v2, :cond_2

    .line 802
    iget-wide v2, p0, Lflipboard/gui/FLBitmapView;->S:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    invoke-virtual {v2}, Landroid/graphics/Movie;->duration()I

    move-result v2

    int-to-long v2, v2

    rem-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lflipboard/gui/FLBitmapView;->T:I

    .line 804
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    iget v1, p0, Lflipboard/gui/FLBitmapView;->T:I

    invoke-virtual {v0, v1}, Landroid/graphics/Movie;->setTime(I)Z

    .line 805
    iget v0, p0, Lflipboard/gui/FLBitmapView;->k:I

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->O:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->R:Z

    if-nez v0, :cond_3

    .line 808
    const-wide/16 v0, 0x2c

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLBitmapView;->postInvalidateDelayed(J)V

    .line 811
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 812
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-lez v0, :cond_4

    .line 813
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 814
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 815
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v3

    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 817
    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    sget-object v3, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    if-ne v2, v3, :cond_6

    .line 818
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 822
    :goto_0
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    invoke-virtual {v1}, Landroid/graphics/Movie;->width()I

    .line 823
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->scale(FFFF)V

    .line 825
    :cond_4
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Movie;->draw(Landroid/graphics/Canvas;FF)V

    .line 826
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 903
    :cond_5
    :goto_1
    monitor-exit p0

    return-void

    .line 820
    :cond_6
    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0

    .line 827
    :cond_7
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->n:Lflipboard/io/Download;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_5

    .line 829
    :try_start_2
    iget-boolean v1, p0, Lflipboard/gui/FLBitmapView;->i:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v1}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v1

    if-nez v1, :cond_8

    .line 830
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v1}, Lflipboard/io/BitmapManager$Handle;->b()Landroid/graphics/Bitmap;

    .line 831
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-direct {p0, v1}, Lflipboard/gui/FLBitmapView;->b(Lflipboard/io/BitmapManager$Handle;)V

    .line 836
    :cond_8
    iget-boolean v1, p0, Lflipboard/gui/FLBitmapView;->l:Z

    if-eqz v1, :cond_c

    .line 837
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v1}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v1

    if-nez v1, :cond_a

    .line 838
    :cond_9
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    .line 839
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 840
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 895
    :catch_0
    move-exception v0

    .line 896
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_14

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    move-result-object v1

    const-string v2, "recycled bitmap"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 897
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->b(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 785
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 846
    :cond_a
    const/4 v1, 0x0

    :try_start_4
    iput-boolean v1, p0, Lflipboard/gui/FLBitmapView;->l:Z

    iget v1, p0, Lflipboard/gui/FLBitmapView;->k:I

    if-nez v1, :cond_b

    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v1

    if-eqz v1, :cond_b

    sget-object v0, Lflipboard/gui/FLBitmapView$LayerType;->c:Lflipboard/gui/FLBitmapView$LayerType;

    invoke-direct {p0, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView$LayerType;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLBitmapView;->setAlpha(F)V

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->p:Landroid/animation/TimeInterpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->U:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    const/4 v0, 0x1

    :cond_b
    if-eqz v0, :cond_c

    .line 847
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_5

    .line 848
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 849
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_1

    .line 856
    :cond_c
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->l:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    sget-object v1, Lflipboard/gui/FLImageView$Align;->b:Lflipboard/gui/FLImageView$Align;

    if-eq v0, v1, :cond_d

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->e()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 857
    :cond_d
    invoke-super {p0, p1}, Lflipboard/gui/ContainerView;->onDraw(Landroid/graphics/Canvas;)V

    .line 860
    :cond_e
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->j()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 861
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->h:Z

    if-eqz v0, :cond_11

    .line 862
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->s:Landroid/graphics/RectF;

    iget v1, p0, Lflipboard/gui/FLBitmapView;->u:F

    iget v2, p0, Lflipboard/gui/FLBitmapView;->u:F

    iget-object v3, p0, Lflipboard/gui/FLBitmapView;->q:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 876
    :cond_f
    :goto_2
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->C:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_10

    .line 877
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->C:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 878
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->C:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 881
    :cond_10
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->J:Landroid/graphics/RectF;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->K:Landroid/graphics/Paint;

    if-eqz v0, :cond_5

    .line 882
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->J:Landroid/graphics/RectF;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->K:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 864
    :cond_11
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->y:Landroid/graphics/Rect;

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->z:Landroid/graphics/RectF;

    sget-object v3, Lflipboard/gui/FLBitmapView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v1, v2, v3}, Lflipboard/io/BitmapManager$Handle;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_2

    .line 867
    :cond_12
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_13

    .line 868
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 869
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 871
    :cond_13
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_f

    .line 872
    invoke-direct {p0}, Lflipboard/gui/FLBitmapView;->f()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 899
    :cond_14
    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 417
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    .line 418
    if-eqz p1, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {v0}, Lflipboard/io/BitmapManager$Handle;->d()V

    .line 422
    const/4 v0, 0x0

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->x:Lflipboard/io/BitmapManager$Handle;

    .line 424
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    if-eqz v0, :cond_1

    .line 426
    invoke-static {p0}, Lflipboard/util/AndroidUtil;->d(Landroid/view/View;)Z

    move-result v0

    iget v1, p0, Lflipboard/gui/FLBitmapView;->k:I

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/FLBitmapView;->a(ZI)V

    .line 428
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, -0x80000000

    .line 347
    sget-object v0, Lflipboard/gui/FLBitmapView;->a:Lflipboard/util/Log;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    aput-object v2, v0, v1

    .line 348
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-boolean v0, v0, Lflipboard/service/FlipboardManager;->ag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    if-nez v0, :cond_1

    .line 351
    :cond_0
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v1, "ImageView was measured before setting how big it was be"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 354
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 355
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 356
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 357
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 360
    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    if-eqz v4, :cond_2

    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    if-nez v4, :cond_3

    .line 361
    :cond_2
    invoke-virtual {p0, v2, v0}, Lflipboard/gui/FLBitmapView;->setMeasuredDimension(II)V

    .line 409
    :goto_0
    return-void

    .line 367
    :cond_3
    iget-object v4, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget-object v5, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    .line 368
    if-ne v1, v7, :cond_5

    .line 371
    int-to-float v1, v2

    div-float/2addr v1, v4

    float-to-int v1, v1

    .line 372
    if-ne v3, v7, :cond_4

    move v1, v2

    .line 408
    :goto_1
    invoke-virtual {p0, v1, v0}, Lflipboard/gui/FLBitmapView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 374
    :cond_4
    if-ne v3, v6, :cond_c

    .line 375
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v2

    goto :goto_1

    .line 377
    :cond_5
    if-ne v1, v6, :cond_9

    .line 379
    if-ne v3, v7, :cond_6

    .line 381
    int-to-float v1, v2

    int-to-float v2, v0

    mul-float/2addr v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    float-to-int v1, v1

    goto :goto_1

    .line 382
    :cond_6
    if-ne v3, v6, :cond_8

    .line 383
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    if-le v0, v1, :cond_7

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    if-le v2, v1, :cond_7

    .line 385
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/Point;->x:I

    .line 386
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    goto :goto_1

    .line 389
    :cond_7
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    int-to-float v2, v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 390
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    div-float/2addr v1, v0

    float-to-int v1, v1

    .line 391
    iget-object v2, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float v0, v2, v0

    float-to-int v0, v0

    .line 392
    goto :goto_1

    .line 395
    :cond_8
    int-to-float v0, v2

    div-float/2addr v0, v4

    float-to-int v0, v0

    move v1, v2

    goto :goto_1

    .line 399
    :cond_9
    if-ne v3, v7, :cond_a

    .line 406
    :goto_2
    int-to-float v1, v0

    mul-float/2addr v1, v4

    float-to-int v1, v1

    goto :goto_1

    .line 401
    :cond_a
    if-ne v3, v6, :cond_b

    .line 402
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_2

    .line 404
    :cond_b
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    goto :goto_2

    :cond_c
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public postInvalidate()V
    .locals 2

    .prologue
    .line 962
    :try_start_0
    invoke-super {p0}, Lflipboard/gui/ContainerView;->postInvalidate()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 966
    :goto_0
    return-void

    .line 963
    :catch_0
    move-exception v0

    .line 964
    sget-object v1, Lflipboard/util/Log;->b:Lflipboard/util/Log;

    invoke-virtual {v1, v0}, Lflipboard/util/Log;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public setAlign(Lflipboard/gui/FLImageView$Align;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->F:Lflipboard/gui/FLImageView$Align;

    .line 178
    return-void
.end method

.method public setAllowPreloadOnUIThread(Z)V
    .locals 0

    .prologue
    .line 159
    iput-boolean p1, p0, Lflipboard/gui/FLBitmapView;->i:Z

    .line 160
    return-void
.end method

.method public setBitmap(I)V
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0, p1}, Lflipboard/io/BitmapManager;->a(I)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLBitmapView;->setBitmap(Lflipboard/io/BitmapManager$Handle;)V

    .line 302
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 305
    sget-object v0, Lflipboard/io/BitmapManager;->b:Lflipboard/io/BitmapManager;

    invoke-virtual {v0, p1}, Lflipboard/io/BitmapManager;->a(Landroid/graphics/Bitmap;)Lflipboard/io/BitmapManager$Handle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lflipboard/gui/FLBitmapView;->setBitmap(Lflipboard/io/BitmapManager$Handle;)V

    .line 306
    return-void
.end method

.method public setBitmap(Lflipboard/io/BitmapManager$Handle;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 309
    invoke-virtual {p0, p1, v0, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/io/BitmapManager$Handle;Landroid/graphics/PointF;Landroid/graphics/Point;)V

    .line 310
    return-void
.end method

.method public setClipRound(Z)V
    .locals 0

    .prologue
    .line 185
    iput-boolean p1, p0, Lflipboard/gui/FLBitmapView;->h:Z

    .line 186
    return-void
.end method

.method setFade(Z)V
    .locals 0

    .prologue
    .line 983
    iput-boolean p1, p0, Lflipboard/gui/FLBitmapView;->l:Z

    .line 984
    return-void
.end method

.method public declared-synchronized setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 255
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->C:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 256
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->C:Landroid/graphics/drawable/Drawable;

    .line 257
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->postInvalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :cond_0
    monitor-exit p0

    return-void

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setImageMatrix(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 211
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/Matrix;->isIdentity()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object p1, v0

    .line 214
    :cond_0
    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    if-eq v1, p1, :cond_2

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    iget-object v1, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    invoke-virtual {v1, p1}, Landroid/graphics/Matrix;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 215
    :cond_1
    if-nez p1, :cond_3

    :goto_0
    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->D:Landroid/graphics/Matrix;

    .line 216
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->postInvalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :cond_2
    monitor-exit p0

    return-void

    .line 215
    :cond_3
    :try_start_1
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0, p1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 211
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method setMovie(Landroid/graphics/Movie;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 996
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLBitmapView:setMovie"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 997
    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lflipboard/gui/FLBitmapView;->N:Z

    if-eqz v0, :cond_0

    .line 998
    invoke-static {p0}, Lflipboard/gui/flipping/FlipUtil;->a(Lflipboard/util/Observer;)V

    .line 999
    const/4 v0, 0x1

    iput-boolean v0, p0, Lflipboard/gui/FLBitmapView;->Q:Z

    .line 1001
    :cond_0
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->m:Landroid/graphics/Movie;

    .line 1002
    invoke-virtual {p1, v3}, Landroid/graphics/Movie;->setTime(I)Z

    .line 1003
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    if-nez v0, :cond_2

    .line 1004
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/graphics/Movie;->width()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Movie;->height()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    .line 1005
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->requestLayout()V

    .line 1013
    :cond_1
    :goto_0
    iput-boolean v3, p0, Lflipboard/gui/FLBitmapView;->l:Z

    .line 1015
    sget-object v0, Lflipboard/gui/FLBitmapView$LayerType;->b:Lflipboard/gui/FLBitmapView$LayerType;

    invoke-direct {p0, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/gui/FLBitmapView$LayerType;)V

    .line 1017
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->w:Lflipboard/io/BitmapManager$Handle;

    invoke-virtual {p0, v0}, Lflipboard/gui/FLBitmapView;->a(Lflipboard/io/BitmapManager$Handle;)V

    .line 1018
    return-void

    .line 1007
    :cond_2
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Landroid/graphics/Movie;->width()I

    move-result v1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1}, Landroid/graphics/Movie;->height()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1008
    :cond_3
    new-instance v0, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/graphics/Movie;->width()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Movie;->height()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    .line 1009
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->requestLayout()V

    .line 1010
    const-string v0, "unwanted.wrong_image_size_gif"

    invoke-static {v0}, Lflipboard/io/UsageEvent;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized setPlaceholder(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 239
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 240
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->B:Landroid/graphics/drawable/Drawable;

    .line 241
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->postInvalidate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    :cond_0
    monitor-exit p0

    return-void

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setRecycle(Z)V
    .locals 0

    .prologue
    .line 168
    iput-boolean p1, p0, Lflipboard/gui/FLBitmapView;->j:Z

    .line 169
    return-void
.end method

.method public setScaling(Z)V
    .locals 0

    .prologue
    .line 198
    iput-boolean p1, p0, Lflipboard/gui/FLBitmapView;->G:Z

    .line 199
    return-void
.end method

.method setSize(Landroid/graphics/Point;)V
    .locals 1

    .prologue
    .line 987
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v0, "FLBitmapView:setSize"

    invoke-static {v0}, Lflipboard/service/FlipboardManager;->h(Ljava/lang/String;)V

    .line 988
    if-eqz p1, :cond_1

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    invoke-virtual {p1, v0}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 989
    :cond_0
    iput-object p1, p0, Lflipboard/gui/FLBitmapView;->e:Landroid/graphics/Point;

    .line 990
    invoke-virtual {p0}, Lflipboard/gui/FLBitmapView;->requestLayout()V

    .line 992
    :cond_1
    return-void
.end method
