.class public abstract Lflipboard/gui/CarouselView$CarouselAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "CarouselView.java"


# instance fields
.field private final b:Lflipboard/util/RecycleBin;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lflipboard/util/RecycleBin",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 207
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 209
    new-instance v0, Lflipboard/util/RecycleBin;

    invoke-direct {v0}, Lflipboard/util/RecycleBin;-><init>()V

    iput-object v0, p0, Lflipboard/gui/CarouselView$CarouselAdapter;->b:Lflipboard/util/RecycleBin;

    return-void
.end method


# virtual methods
.method public abstract a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public abstract a(I)Ljava/lang/Object;
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lflipboard/gui/CarouselView$CarouselAdapter;->b:Lflipboard/util/RecycleBin;

    invoke-virtual {p0, p2}, Lflipboard/gui/CarouselView$CarouselAdapter;->c(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-class v2, Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lflipboard/util/RecycleBin;->a(Ljava/lang/Object;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 241
    invoke-virtual {p0, p2, v0, p1}, Lflipboard/gui/CarouselView$CarouselAdapter;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 242
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 243
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 248
    move-object v0, p3

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 249
    iget-object v0, p0, Lflipboard/gui/CarouselView$CarouselAdapter;->b:Lflipboard/util/RecycleBin;

    invoke-virtual {p0, p2}, Lflipboard/gui/CarouselView$CarouselAdapter;->c(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lflipboard/util/RecycleBin;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 250
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 254
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c(I)I
.end method
