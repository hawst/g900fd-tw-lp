.class public Lflipboard/gui/actionbar/FLActionBarMenu;
.super Ljava/lang/Object;
.source "FLActionBarMenu.java"

# interfaces
.implements Landroid/view/Menu;
.implements Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;


# instance fields
.field private a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lflipboard/gui/actionbar/FLActionBarMenuItem;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/content/Context;

.field private final c:Lflipboard/service/FlipboardManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->c:Lflipboard/service/FlipboardManager;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    .line 41
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 51
    invoke-virtual {p0, v0, v0, v0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    invoke-virtual {v0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move v2, p1

    move v3, v1

    move v5, v1

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    invoke-virtual {v0, p4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 6

    .prologue
    .line 69
    const/4 v5, -0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 6

    .prologue
    .line 56
    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenuItem;-><init>(Landroid/content/Context;IIILjava/lang/CharSequence;)V

    .line 57
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 58
    const/4 v1, -0x1

    if-ne p5, v1, :cond_0

    .line 59
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :goto_0
    return-object v0

    .line 61
    :cond_0
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p5, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-virtual {p0, v0, v0, v0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;ZLflipboard/objs/FeedItem;)V
    .locals 8

    .prologue
    .line 192
    invoke-virtual {p3}, Lflipboard/objs/FeedItem;->ag()Z

    move-result v0

    .line 194
    if-nez v0, :cond_0

    invoke-virtual {p3}, Lflipboard/objs/FeedItem;->ah()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 195
    :cond_0
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    iget-object v1, p3, Lflipboard/objs/FeedItem;->T:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    move-object v6, v0

    .line 200
    :goto_0
    invoke-virtual {p3, p0}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/HasCommentaryItem$CommentaryChangedObserver;)V

    .line 202
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->c:Lflipboard/service/FlipboardManager;

    iget-object v0, v0, Lflipboard/service/FlipboardManager;->M:Lflipboard/service/User;

    invoke-static {}, Lflipboard/service/User;->u()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz p5, :cond_7

    const/4 v0, 0x1

    move v7, v0

    .line 204
    :goto_1
    if-eqz v7, :cond_1

    invoke-virtual {p6, v6}, Lflipboard/objs/FeedItem;->b(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    invoke-virtual {v6}, Lflipboard/objs/ConfigService;->f()Ljava/lang/String;

    move-result-object v0

    .line 206
    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 207
    invoke-static {v6}, Lflipboard/util/AndroidUtil;->b(Lflipboard/objs/ConfigService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 208
    invoke-virtual {v0, p4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 209
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu$1;

    invoke-direct {v1, p0, p1, p2, p6}, Lflipboard/gui/actionbar/FLActionBarMenu$1;-><init>(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 216
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v1, 0x1

    .line 217
    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 218
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    const v2, 0x7f0d02e7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 221
    :cond_1
    iget-boolean v0, p6, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v0, :cond_2

    .line 224
    const/4 v0, 0x0

    const v1, 0x7f0d02e7

    invoke-virtual {p0, v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(II)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 225
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    const v3, 0x7f0d02e7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 226
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu$2;

    invoke-direct {v1, p0, p6, p2, p1}, Lflipboard/gui/actionbar/FLActionBarMenu$2;-><init>(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/service/Section;Lflipboard/activities/FlipboardActivity;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 234
    invoke-virtual {v0, p4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 235
    const v1, 0x7f020016

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 236
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 237
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 241
    :cond_2
    if-eqz v7, :cond_3

    invoke-virtual {p3, v6}, Lflipboard/objs/FeedItem;->a(Lflipboard/objs/ConfigService;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 242
    invoke-static {p1, v6}, Lflipboard/gui/SocialFormatter;->d(Landroid/content/Context;Lflipboard/objs/ConfigService;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 243
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 244
    invoke-static {v6}, Lflipboard/util/AndroidUtil;->a(Lflipboard/objs/ConfigService;)I

    move-result v1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 245
    invoke-virtual {v0, p4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 246
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->p:Z

    .line 247
    iget-boolean v1, p3, Lflipboard/objs/FeedItem;->af:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Z)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 248
    const/4 v1, 0x1

    iput-boolean v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    .line 249
    invoke-virtual {v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    const v3, 0x7f0d01df

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 250
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 251
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu$3;

    invoke-direct {v1, p0, p3, p1, p2}, Lflipboard/gui/actionbar/FLActionBarMenu$3;-><init>(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/objs/FeedItem;Lflipboard/activities/FlipboardActivity;Lflipboard/service/Section;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 263
    :cond_3
    iget-boolean v0, p6, Lflipboard/objs/FeedItem;->aj:Z

    if-eqz v0, :cond_4

    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    invoke-static {}, Lflipboard/service/FlipboardManager;->L()Z

    move-result v0

    if-nez v0, :cond_4

    .line 266
    const/4 v1, 0x0

    const/4 v2, 0x6

    const/4 v3, 0x0

    invoke-virtual {p1}, Lflipboard/activities/FlipboardActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f0d001f

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lflipboard/util/JavaUtil;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v6

    .line 267
    invoke-virtual {v6}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->getActionView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->b:Landroid/content/Context;

    const v2, 0x7f0d001f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 268
    const v0, 0x7f020006

    invoke-virtual {v6, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 269
    invoke-virtual {v6, p4}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Lflipboard/gui/actionbar/FLActionBar$FLActionBarButtonStyle;)V

    .line 270
    new-instance v0, Lflipboard/gui/actionbar/FLActionBarMenu$4;

    move-object v1, p0

    move v2, p5

    move-object v3, p1

    move-object v4, p2

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lflipboard/gui/actionbar/FLActionBarMenu$4;-><init>(Lflipboard/gui/actionbar/FLActionBarMenu;ZLflipboard/activities/FlipboardActivity;Lflipboard/service/Section;Lflipboard/objs/FeedItem;)V

    iput-object v0, v6, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, v6, Lflipboard/gui/actionbar/FLActionBarMenuItem;->n:Z

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->setShowAsAction(I)V

    .line 284
    :cond_4
    invoke-virtual {p6}, Lflipboard/objs/FeedItem;->ak()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 285
    const v0, 0x7f0d0288

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 286
    new-instance v1, Lflipboard/gui/actionbar/FLActionBarMenu$5;

    invoke-direct {v1, p0, p1, p6, p2}, Lflipboard/gui/actionbar/FLActionBarMenu$5;-><init>(Lflipboard/gui/actionbar/FLActionBarMenu;Lflipboard/activities/FlipboardActivity;Lflipboard/objs/FeedItem;Lflipboard/service/Section;)V

    iput-object v1, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->t:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 297
    :cond_5
    return-void

    .line 197
    :cond_6
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    const-string v1, "flipboard"

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->e(Ljava/lang/String;)Lflipboard/objs/ConfigService;

    move-result-object v0

    move-object v6, v0

    goto/16 :goto_0

    .line 202
    :cond_7
    const/4 v0, 0x0

    move v7, v0

    goto/16 :goto_1
.end method

.method public final a(Lflipboard/objs/HasCommentaryItem;)V
    .locals 2

    .prologue
    .line 301
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 302
    if-eqz v0, :cond_0

    instance-of v1, p1, Lflipboard/objs/FeedItem;

    if-eqz v1, :cond_0

    .line 303
    check-cast p1, Lflipboard/objs/FeedItem;

    iget-boolean v1, p1, Lflipboard/objs/FeedItem;->af:Z

    invoke-virtual {v0, v1}, Lflipboard/gui/actionbar/FLActionBarMenuItem;->a(Z)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 305
    :cond_0
    return-void
.end method

.method public synthetic add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2, p3, p4}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIII)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1, p2, p3, p4}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(IIILjava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public synthetic add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->a(Ljava/lang/CharSequence;)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addSubMenu(I)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 148
    iget v2, v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;->b:I

    if-ne v2, p1, :cond_0

    .line 152
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 123
    return-void
.end method

.method public close()V
    .locals 2

    .prologue
    .line 167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic findItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->c(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    return-object v0
.end method

.method public hasVisibleItems()Z
    .locals 2

    .prologue
    .line 142
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 177
    const/4 v0, 0x0

    return v0
.end method

.method public performIdentifierAction(II)Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public removeGroup(I)V
    .locals 2

    .prologue
    .line 117
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeItem(I)V
    .locals 2

    .prologue
    .line 109
    invoke-virtual {p0, p1}, Lflipboard/gui/actionbar/FLActionBarMenu;->b(I)Lflipboard/gui/actionbar/FLActionBarMenuItem;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_0

    .line 111
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 113
    :cond_0
    return-void
.end method

.method public setGroupCheckable(IZZ)V
    .locals 2

    .prologue
    .line 127
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setGroupEnabled(IZ)V
    .locals 2

    .prologue
    .line 137
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setGroupVisible(IZ)V
    .locals 2

    .prologue
    .line 132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported yet."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setQwertyMode(Z)V
    .locals 0

    .prologue
    .line 188
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarMenu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
