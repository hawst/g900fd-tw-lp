.class Lflipboard/gui/actionbar/FLActionBar$4;
.super Ljava/lang/Object;
.source "FLActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lflipboard/service/Section;

.field final synthetic b:Landroid/os/Bundle;

.field final synthetic c:Lflipboard/gui/actionbar/FLActionBar;


# direct methods
.method constructor <init>(Lflipboard/gui/actionbar/FLActionBar;Lflipboard/service/Section;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 869
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBar$4;->c:Lflipboard/gui/actionbar/FLActionBar;

    iput-object p2, p0, Lflipboard/gui/actionbar/FLActionBar$4;->a:Lflipboard/service/Section;

    iput-object p3, p0, Lflipboard/gui/actionbar/FLActionBar$4;->b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 872
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar$4;->a:Lflipboard/service/Section;

    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar$4;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-static {v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBar$4;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lflipboard/service/Section;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 873
    const-string v1, "launched_by_flipboard_activity"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 874
    const-string v1, "launched_by_sstream"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 875
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBar$4;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-static {v1}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar;)Lflipboard/activities/FlipboardActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Lflipboard/activities/FlipboardActivity;->startActivity(Landroid/content/Intent;)V

    .line 876
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar$4;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-static {v0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    const v1, 0x7f040013

    const v2, 0x7f040014

    invoke-virtual {v0, v1, v2}, Lflipboard/activities/FlipboardActivity;->overridePendingTransition(II)V

    .line 877
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBar$4;->c:Lflipboard/gui/actionbar/FLActionBar;

    invoke-static {v0}, Lflipboard/gui/actionbar/FLActionBar;->a(Lflipboard/gui/actionbar/FLActionBar;)Lflipboard/activities/FlipboardActivity;

    move-result-object v0

    invoke-virtual {v0}, Lflipboard/activities/FlipboardActivity;->finish()V

    .line 878
    return-void
.end method
