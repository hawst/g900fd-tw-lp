.class public Lflipboard/gui/actionbar/FLActionBarPopover;
.super Landroid/widget/PopupWindow;
.source "FLActionBarPopover.java"


# static fields
.field public static final a:Lflipboard/util/Log;


# instance fields
.field b:Landroid/view/View;

.field c:Lflipboard/gui/actionbar/FLActionBar;

.field d:Lflipboard/gui/actionbar/FLActionBarPopover$RootView;

.field e:Landroid/widget/ScrollView;

.field f:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "actionbar"

    invoke-static {v0}, Lflipboard/util/Log;->a(Ljava/lang/String;)Lflipboard/util/Log;

    move-result-object v0

    sput-object v0, Lflipboard/gui/actionbar/FLActionBarPopover;->a:Lflipboard/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lflipboard/gui/actionbar/FLActionBar;Landroid/view/View;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lflipboard/gui/actionbar/FLActionBar;",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Lflipboard/gui/actionbar/FLActionBarMenuItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    const/4 v3, -0x1

    const/4 v2, -0x2

    const/4 v6, 0x0

    .line 39
    invoke-direct {p0, p1}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;)V

    .line 40
    iput-object p1, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->f:Landroid/app/Activity;

    .line 42
    const v0, 0x7f0e0014

    invoke-virtual {p0, v0}, Lflipboard/gui/actionbar/FLActionBarPopover;->setAnimationStyle(I)V

    .line 43
    iput-object p3, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->b:Landroid/view/View;

    .line 44
    iput-object p2, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->c:Lflipboard/gui/actionbar/FLActionBar;

    .line 47
    new-instance v0, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;

    invoke-direct {v0, p0, p1}, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;-><init>(Lflipboard/gui/actionbar/FLActionBarPopover;Landroid/content/Context;)V

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->d:Lflipboard/gui/actionbar/FLActionBarPopover$RootView;

    .line 49
    const v0, 0x7f030007

    invoke-static {p1, v0, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    .line 50
    iget-object v0, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    const v1, 0x7f0a0046

    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 51
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 52
    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-virtual {v2, v1}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-virtual {v1, v7}, Landroid/widget/ScrollView;->setFillViewport(Z)V

    .line 54
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->d:Lflipboard/gui/actionbar/FLActionBarPopover$RootView;

    iget-object v2, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->e:Landroid/widget/ScrollView;

    invoke-virtual {v1, v2}, Lflipboard/gui/actionbar/FLActionBarPopover$RootView;->addView(Landroid/view/View;)V

    .line 57
    invoke-virtual {p0, v3}, Lflipboard/gui/actionbar/FLActionBarPopover;->setWidth(I)V

    .line 58
    invoke-virtual {p0, v3}, Lflipboard/gui/actionbar/FLActionBarPopover;->setHeight(I)V

    .line 59
    invoke-virtual {p0, v6}, Lflipboard/gui/actionbar/FLActionBarPopover;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lflipboard/gui/actionbar/FLActionBarPopover;->setTouchable(Z)V

    .line 61
    iget-object v1, p0, Lflipboard/gui/actionbar/FLActionBarPopover;->d:Lflipboard/gui/actionbar/FLActionBarPopover$RootView;

    invoke-virtual {p0, v1}, Lflipboard/gui/actionbar/FLActionBarPopover;->setContentView(Landroid/view/View;)V

    .line 63
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;

    .line 64
    const v2, 0x7f03000a

    invoke-static {p1, v2, v6}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 65
    const v2, 0x7f0a0049

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLStaticTextView;

    .line 66
    iget-object v5, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    invoke-virtual {v2, v6}, Lflipboard/gui/FLStaticTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 69
    new-instance v2, Lflipboard/gui/actionbar/FLActionBarPopover$1;

    invoke-direct {v2, p0, p1, v1}, Lflipboard/gui/actionbar/FLActionBarPopover$1;-><init>(Lflipboard/gui/actionbar/FLActionBarPopover;Landroid/app/Activity;Lflipboard/gui/actionbar/FLActionBarMenuItem;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iget-object v2, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->e:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    .line 78
    const v2, 0x7f0a004a

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lflipboard/gui/FLStaticTextView;

    .line 79
    iget-object v5, v1, Lflipboard/gui/actionbar/FLActionBarMenuItem;->e:Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Lflipboard/gui/FLStaticTextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    invoke-virtual {v2, v7}, Lflipboard/gui/FLStaticTextView;->setVisibility(I)V

    .line 82
    :cond_1
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 83
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {p4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 84
    const v1, 0x7f0a004b

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 87
    :cond_2
    return-void
.end method

.method static synthetic a(Lflipboard/gui/actionbar/FLActionBarPopover;)V
    .locals 0

    .prologue
    .line 24
    invoke-super {p0}, Landroid/widget/PopupWindow;->dismiss()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 95
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/actionbar/FLActionBarPopover$2;

    invoke-direct {v1, p0}, Lflipboard/gui/actionbar/FLActionBarPopover$2;-><init>(Lflipboard/gui/actionbar/FLActionBarPopover;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 102
    return-void
.end method

.method public dismiss()V
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lflipboard/service/FlipboardManager;->u:Lflipboard/service/FlipboardManager;

    new-instance v1, Lflipboard/gui/actionbar/FLActionBarPopover$3;

    invoke-direct {v1, p0}, Lflipboard/gui/actionbar/FLActionBarPopover$3;-><init>(Lflipboard/gui/actionbar/FLActionBarPopover;)V

    invoke-virtual {v0, v1}, Lflipboard/service/FlipboardManager;->b(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 117
    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
